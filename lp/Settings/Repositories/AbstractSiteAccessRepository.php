<?php
namespace LP\Settings\Repositories;

use LP\Settings\Contracts\SiteAccessInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Exception;

abstract class AbstractSiteAccessRepository implements SiteAccessInterface{
	/**
     * @var
     */
	protected $model, $assignedApps;
	
	 /**
     * @var App
     */
    private $app;
 
    /**
     * @param App $app
     * 
     */
    public function __construct(App $app) {
        $this->app = $app;
        $this->makeModel();
    }
 
    /**
     * Specify Model class name
     * 
     * @return mixed
     */
    abstract function model();
 
    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());
		
        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
 
        return $this->model = $model;
    }
	
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	
	abstract public function getPackageID(Authenticatable $user);
	
	abstract public function getPackageName(Authenticatable $user);
	
	abstract public function getAssignedAppID(Authenticatable $user);
	
	abstract public function getAssignedModules(Authenticatable $user);
	
	protected function _fetchSubscriptionData($user){
		$this->assignedApps = (count($user->apps) && $user->apps->count())  ? $user->apps: null;
		//dd($this->subcription->toArray());
	}
}