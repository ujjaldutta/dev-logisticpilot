<?php
namespace LP\Settings\Repositories;

use LP\Settings\Repositories\AbstractSiteAccessRepository;
use Illuminate\Contracts\Auth\Authenticatable;

class SiteAccessRepository extends AbstractSiteAccessRepository {
	public function model(){
		return 'App\User';
	}
	
	public function getPackageID(Authenticatable $user){
		
	}
	
	public function getPackageName(Authenticatable $user){
		
	}
	
	public function getAssignedModules(Authenticatable $user){
		
	}
	
	public function getAssignedAppID(Authenticatable $user){
		if(!count($this->assignedApps) || !(count($this->assignedApps) && $this->assignedApps->count())){
			$this->_fetchSubscriptionData($user);
		}
		
		return $this->assignedApps;
	}
	
	public function getAppGroupswithApps($user){
		$appsWithGroups = $this->getAssignedAppID($user);
		//now getting all groups from the assigned apps
		$groups = [];
		if(count($appsWithGroups) && $appsWithGroups->count()){
			$appsWithGroups->each(function($data) use(& $groups){
				$dataArr =  array('id' => $data->Id, 'name' => $data->appName, 'action' => $data->appTargetURL, 'appType' => $data->appTargetURLType, 'icon' => $data->icon);
				if(isset($groups[$data->appGroupID]) && isset($groups[$data->appGroupID]['apps'])){
					$groups[$data->appGroupID]['apps'][] = $dataArr;
				}else{
					$groups[$data->appGroupID] = array(
						'name' => isset($data->group->GroupName) ? $data->group->GroupName : 'Unknown',
						'icon' => isset($data->group->icon) ? $data->group->icon : null,
						'apps' => array($dataArr),
					);
				}
			});
		}
		
		return $groups;
	}
}