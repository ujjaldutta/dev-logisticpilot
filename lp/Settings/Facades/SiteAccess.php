<?php
namespace LP\Settings\Facades;

use Illuminate\Support\Facades\Facade;

class SiteAccess extends Facade{
	protected static function getFacadeAccessor(){
		return 'LP\Settings\Repositories\SiteAccessRepository';
	}
}