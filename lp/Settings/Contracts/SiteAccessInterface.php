<?php
namespace LP\Settings\Contracts;
use Illuminate\Contracts\Auth\Authenticatable;

interface SiteAccessInterface{
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	
	public function getPackageID(Authenticatable $user);
	
	public function getPackageName(Authenticatable $user);
	
	public function getAssignedAppID(Authenticatable $user);
	
	public function getAssignedModules(Authenticatable $user);
}