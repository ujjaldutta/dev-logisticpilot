<?php
namespace LP\Settings\Contracts;

interface SettingsInterface{
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	public function getLogo();
	
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	public function getName();
	
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	public function getTagline();
	
	 /**
     * Determine if an item exists in the cache.
     *
     * @param  string  $key
     * @return bool
     */
	public function getCredits();
}