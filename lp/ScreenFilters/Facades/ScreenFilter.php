<?php
namespace LP\ScreenFilters\Facades;

use Illuminate\Support\Facades\Facade;

class ScreenFilter extends Facade{
	protected static function getFacadeAccessor(){
		return 'LP\ScreenFilters\Repositories\ScreenFilterRepository';
	}
}