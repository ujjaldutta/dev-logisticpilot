<?php
namespace LP\ScreenFilters\Repositories;

use LP\ScreenFilters\Repositories\AbstractScreenFilterRepository;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Builder;

class ScreenFilterRepository extends AbstractScreenFilterRepository {
	public static $allowedOperators = ['=', '<>', '<', '>'];
	
	public function model(){
		return 'App\UserFilter';
	}
	
	/**
	 * Get the screen config and set the applicable filters 
	 * for the display and its attributes or columns
	 *
	 * @param  Illuminate\Contracts\Config\Repository  $screenConfig
	 * @param  Illuminate\Contracts\Auth\Guard  $auth
	 * @param  Illuminate\Database\Eloquent\Builder  $query
	 * @return Illuminate\Database\Eloquent\Builder;
	*/
	
	public function applyFilters($screenId, Guard $auth, Builder $query){
		//getting user filters specific to this user
		$userScreenFilters = $this->model->where('userId', $auth->id())
			->where('layoutId', $screenId)
			->with([
					'attribute' => function($query){
						//$query->select(['id', 'layoutName']);
					},
					'operator' => function($query){
						//$query->select(['id', 'layoutName']);
					},
				])
				//->groupBy('layoutId')
				->get();
		//for the time allowed operators
		//$allowedOperators = ['=', '<>', '<', '>'];
		foreach($userScreenFilters as $filter){
			if(isset($filter->attribute->displayField) && 
				isset($filter->operator->sqloperator) && 
				isset($filter->filterValue)
			){
				if(in_array($filter->operator->sqloperator, self::$allowedOperators)){
					$query->where($filter->attribute->displayField, $filter->operator->sqloperator, $filter->filterValue);
				}
			}
		}
		
		return $query;
	}
}