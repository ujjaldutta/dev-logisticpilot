<?php
namespace LP\ScreenFilters\Repositories;

use LP\ScreenFilters\Contracts\ScreenFilterInterface;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Exception;

abstract class AbstractScreenFilterRepository implements ScreenFilterInterface{
	/**
     * @var
     */
	protected $model;
	
	 /**
     * @var App
     */
    private $app;
 
    /**
     * @param App $app
     *
     */
    public function __construct(App $app) {
        $this->app = $app;
        $this->makeModel();
    }
 
    /**
     * Specify Model class name
     * 
     * @return mixed
     */
    abstract function model();
 
    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());
		
        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
 
        return $this->model = $model;
    }
	
	 /**
	 * Get the screen config and set the applicable filters 
	 * for the display and its attributes or columns
	 *
	 * @param  Illuminate\Contracts\Config\Repository  $screenConfig
	 * @param  Illuminate\Contracts\Auth\Guard  $auth
	 * @param  Illuminate\Database\Eloquent\Builder  $query
	 * @return Illuminate\Database\Eloquent\Builder;
	 */
	
	abstract public function applyFilters($screenId, Guard $auth, Builder $query);
}