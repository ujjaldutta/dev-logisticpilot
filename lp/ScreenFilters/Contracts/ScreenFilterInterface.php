<?php
namespace LP\ScreenFilters\Contracts;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Builder;

interface ScreenFilterInterface{
	 /**
	 * Get the screen config and set the applicable filters 
	 * for the display and its attributes or columns
	 *
	 * @param  int $screenId
	 * @param  Illuminate\Contracts\Auth\Guard  $auth
	 * @param  Illuminate\Database\Eloquent\Builder  $query
	 * @return Illuminate\Database\Eloquent\Builder;
	 */
	public function applyFilters($screenId, Guard $auth, Builder $query);
}