<?php
namespace LP\Customers\Facades;

use Illuminate\Support\Facades\Facade;

class CustomerRepository extends Facade{
	protected static function getFacadeAccessor(){
		return 'LP\Customers\Repositories\CustomerRepository';
	}
}