<?php
namespace LP\Customers\Repositories;

use LP\Customers\Contracts\CustomerRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Exception;

abstract class AbstractCustomerRepository implements CustomerRepositoryInterface{
	/**
     * @var
     */
	protected $model, $assignedApps;
	
	 /**
     * @var App
     */
    private $app;
 
    /**
     * @param App $app
     * 
     */
    public function __construct(App $app) {
        $this->app = $app;
        $this->makeModel();
    }
 
    /**
     * Specify Model class name
     * 
     * @return mixed
     */
    abstract function model();
 
    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());
		
        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
 
        return $this->model = $model;
    }
	
	abstract public function getCurrentCustomerID($key = 'custID');
	
	abstract public function setCurrentCustomer(array $custInfo);
	
	abstract public function getCurrentCustomer();
	
	abstract public function getCustomerLocale();
	
	abstract public function getCustomerTheme();
	
	abstract public function getCustomerLogo();
	
	abstract public function getCustomerTagline();
	
	abstract public function removeCurrentCustomer();
}