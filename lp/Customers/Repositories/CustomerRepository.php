<?php
namespace LP\Customers\Repositories;

use LP\Customers\Repositories\AbstractCustomerRepository;
use LP\Customers\Exceptions\CustomerRepositoryException;
use LP\Customers\Logics\CustomerStore;
use LP\Customers\Logics\CustomerFilters;

class CustomerRepository extends AbstractCustomerRepository {
	const CUSTOMER_KEY_NAME = 'customerId',
		CUSTOMER_LOCALE_KEY_NAME = 'locale',
		CUSTOMER_THEME_KEY_NAME = 'theme',
		CUSTOMER_LOCALE_LOGO_NAME = 'logo',
		CUSTOMER_LOCALE_TAGLINE_NAME = 'tagline',
		DEFAULT_LOCALE = 'en';
		
	protected $store;
	
	public function model(){
		return 'App\Customer';
	}
	
	public function getCurrentCustomerID($key = 'custID'){
		if(CustomerStore::has(self::CUSTOMER_KEY_NAME)){
			$customer = CustomerStore::get(self::CUSTOMER_KEY_NAME);
			if(isset($customer[$key]))
				return $customer[$key];
			else
				return $this->__getLoggedInCustomerID();
		}else
			return $this->__getLoggedInCustomerID();
	}
	
	public function getCurrentCustomer(){
		return CustomerStore::get(self::CUSTOMER_KEY_NAME);
	}
	
	public function getCustomerLocale(){
		return CustomerStore::get(self::CUSTOMER_LOCALE_KEY_NAME);
	}
	
	public function getCustomerTheme(){
		return CustomerStore::get(self::CUSTOMER_THEME_KEY_NAME);
	}
	
	public function getCustomerLogo(){
		return CustomerStore::get(self::CUSTOMER_LOCALE_LOGO_NAME);
	}
	
	public function getCustomerTagline(){
		return CustomerStore::get(self::CUSTOMER_LOCALE_TAGLINE_NAME);
	}
	
	public function setCurrentCustomer(array $custInfo){
		if(!isset($custInfo['id']) || 
			!isset($custInfo['client_firstname']) || 
			!isset($custInfo['client_lastname'])
		)
			throw new CustomerRepositoryException('Required information are missing to switch customer');
		
		$custInfo = [
			'custID' => $custInfo['id'], 
			'client_firstname' => $custInfo['client_firstname'], 
			'client_lastname' => $custInfo['client_lastname'], 
			'client_compname' => isset($custInfo['client_compname']) ? $custInfo['client_compname'] : null,
			'client_addr1' => isset($custInfo['client_adr1']) ? $custInfo['client_adr1'] : null,
			'client_adr2' => isset($custInfo['client_adr2']) ? $custInfo['client_adr2'] : null,
			'client_city' => isset($custInfo['client_city']) ? $custInfo['client_city'] : null,
			'client_state' => isset($custInfo['client_state']) ? $custInfo['client_state'] : null,
			'client_country' => isset($custInfo['client_country']) ? $custInfo['client_country'] : null,
			'client_postal' => isset($custInfo['client_postal']) ? $custInfo['client_postal'] : null,
			'client_email' => isset($custInfo['client_email']) ? $custInfo['client_email'] : null,
			'client_phone' => isset($custInfo['client_phone']) ? $custInfo['client_phone'] : null,
		];
		
		CustomerStore::put(self::CUSTOMER_KEY_NAME, $custInfo);
		
		//setting current locale and other pref for this user
		$customerBranding = $this->__getCustomerBranding();
		
		//dd($customerBranding);
		
		CustomerStore::put(self::CUSTOMER_LOCALE_KEY_NAME, $customerBranding['locale']);
		CustomerStore::put(self::CUSTOMER_THEME_KEY_NAME, $customerBranding['theme']);
		CustomerStore::put(self::CUSTOMER_LOCALE_LOGO_NAME, $customerBranding['logo']);
		CustomerStore::put(self::CUSTOMER_LOCALE_TAGLINE_NAME, $customerBranding['tagline']);
		
		return true;
	}
	
	public function removeCurrentCustomer(){
		CustomerStore::forget(self::CUSTOMER_LOCALE_KEY_NAME);
		CustomerStore::forget(self::CUSTOMER_THEME_KEY_NAME);
		CustomerStore::forget(self::CUSTOMER_LOCALE_LOGO_NAME);
		CustomerStore::forget(self::CUSTOMER_LOCALE_TAGLINE_NAME);
		CustomerStore::forget(self::CUSTOMER_KEY_NAME);
		
		return true;
	}
	
	private function __getLoggedInCustomerID(){
		return \Auth::user()->clientID;
	}
	
	private function __getCustomerBranding(){
		$locale = self::DEFAULT_LOCALE;
		$theme = '';
		$logo = '';
		$tagline = '';
		$clientId = $this->getCurrentCustomerID();
		$returnArr = [];
		
		if(is_numeric($clientId)){
			//getting wl culture if any
			$wl_culture = $this->model->where('id', $clientId)
				->with([
					'wishList' => function($query){
						$query->select(['id', 'clientID', 'wl_culture', 'wl_theme', 'wl_message', 'wl_logo']);
					},
					'wishList.culture' => function($query){
						$query->select(['id', 'code']);
					},
					'wishList.theme' => function($query){
						$query->select(['id', 'theme_path']);
					},
				])->first(['id']);
			
			//print '<pre>';dd($wl_culture->toArray());
			//dd($wl_culture->wishList->culture->code);
			
			if(isset($wl_culture->wishList->culture->code))
				$locale = $wl_culture->wishList->culture->code;
			
			if(isset($wl_culture->wishList->theme->theme_path))
				$theme = $wl_culture->wishList->theme->theme_path;
			
			if(isset($wl_culture->wishList->wl_logo))
				$logo = $wl_culture->wishList->wl_logo;
			
			if(isset($wl_culture->wishList->wl_message))
				$tagline = $wl_culture->wishList->wl_message;
		}
		
		return $returnArr = ['locale' => $locale, 'theme' => $theme, 'logo' => $logo, 'tagline' => $tagline];
	}
	/*
	@return Illuminate\Database\Eloquent\Builder
	*/
	public function getChildren($parent_id = null){
		if(empty($parent_id))
			$parent_id = $this->getCurrentCustomerID();
		
		//$clients = $this->model->where('parent_id', $parent_id);
		//$clients = $this->model->where('active', 1);
		$clients = $this->model->whereIn('active', array('0','1'));
		
		//filtering customer based on dynamic logics
		$customerLogic = new CustomerFilters();
		
		//return $customerLogic->applyAssociationMapping($clients);
		return $customerLogic->applyAssociationMapping($clients, $parent_id);
	}
	
	public function getAvailableChildrenIds(){
		$children = $this->getChildren();
		return $children->lists('id');
	}
}