<?php
namespace LP\Customers\Contracts;

interface CustomerRepositoryInterface{
	 /**
     * Determine the selected customer ID
     *
     * @param  void
     * @return int
     */
	public function getCurrentCustomerID($key = 'custID');
	
	/**
     * Determine the selected customer and his/her information
     *
     * @param  void
     * @return array
     */
	public function getCurrentCustomer();
	
	/**
     * gets a customer's set locale
     *
     * @param  void
     * @return mixed string|null
     */
	public function getCustomerLocale();
	
	/**
     * gets a customer's set theme from the available theme list
     *
     * @param  void
     * @return mixed string|null
     */
	public function getCustomerTheme();
	
	/**
     * Removes an customer details
     *
     * @param  void
     * @return array
     */
	public function getCustomerLogo();
	
	/**
     * Removes an customer details
     *
     * @param  void
     * @return array
     */
	public function getCustomerTagline();
	
	/**
     * Determine the selected customer
     *
     * @param  array $custInfo
     * @return bool
     */
	public function setCurrentCustomer(array $custInfo);
	
	
	/**
     * Removes an customer details
     *
     * @param  void
     * @return bool
     */
	public function removeCurrentCustomer();
	
	
}