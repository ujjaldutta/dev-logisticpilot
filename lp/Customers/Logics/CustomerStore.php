<?php
namespace LP\Customers\Logics; 

class CustomerStore{
	public function __construct(){
		
	}
	
	public static function put($key, $value){
		\Session::put($key, $value);
		return true;
	}
	
	public static function get($key){
		return \Session::get($key);
	}
	
	public static function has($key){
		return \Session::has($key);
	}
	
	public static function forget($key){
		\Session::forget($key);
		return true;
	}
	
	public static function overWrite($key, $value){
		$oldValue = \Session::pull($key);
		\Session::put($key, $value);
		return $oldValue;
	}
}