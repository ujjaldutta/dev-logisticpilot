<?php
namespace LP\Customers\Logics;
use Illuminate\Database\Eloquent\Builder;

use App\UserClientMapping;

class CustomerFilters{
	
	public function applyAssociationMapping(Builder $customers, $parent_id){
		//if super admin logs in he/she should get all customers below his/her company
		if(isset(\Auth::user()->usr_type_id) &&  \Auth::user()->usr_type_id == \Config::get('app.superAdminID')){
			
		}else if(isset(\Auth::user()->usr_type_id) &&  \Auth::user()->usr_type_id == \Config::get('app.userAdminType')){
			$customers->where('parent_id', $parent_id);
		}else{
			$customers->where('parent_id', $parent_id);
			$mappedClients = $this::getMappedClients(\Auth::id());
			$customers->whereIn('id', $mappedClients);
		}
		
		return $customers;
	}
	
	/*
	* method to get list of assigned or mapped client id against a user id
	*/
	public static function getMappedClients($userID){
		$clients = UserClientMapping::where('userID', $userID)->lists('clientID');
		return $clients;
	}
}