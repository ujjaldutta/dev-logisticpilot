<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipStatus extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'v_shipstatus';
	protected $revisionEnabled = true;
	//protected $fillable = ['id', 'shipId', 'ship_b2bId', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function shipheaders(){
		return $this->hasMany('App\ShipHeader', 'id');
	}		
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
}
