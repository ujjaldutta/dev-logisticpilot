<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	//
	protected $table = 'm_country';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function states(){
		return $this->hasMany('App\State', 'country_code');
	}
}
