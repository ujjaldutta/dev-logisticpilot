<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerType extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'v_ownerType';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}