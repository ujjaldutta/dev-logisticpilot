<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AppModule extends Model {

	//
	protected $table = 'm_apps';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	public $timestamps = false; 
	
	protected $fillable = [
		'appName',
		'appGroupID',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function group(){
		return $this->belongsTo('App\AppModuleGroup', 'appGroupID');
	}
}