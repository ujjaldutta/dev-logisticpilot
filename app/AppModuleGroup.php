<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AppModuleGroup extends Model {

	//
	protected $table = 'm_appgroup';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'GroupName',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}