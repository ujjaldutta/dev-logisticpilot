<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTemplateType extends Model {

	protected $table = 'v_invoiceTemplateType';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}
