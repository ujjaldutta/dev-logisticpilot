<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model{
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'quote_master';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'clientId',
		'shp_pickupdate',
		'shp_pickupcity',
		'shp_pickupstate',
		'shp_pickuppostal',
		'shp_pickupcountry',
		'shp_deliverycity',
		'shp_deliverystate',
		'shp_deliverypostal',
		'shp_deliverycountry',
		'shp_equipmenttype',
		'shp_quotenumber',
		'shp_quotedate',
		'shp_transitdays',
		'shp_terminalId',
		'shp_loadnumber',
		'shp_servicelevelId',
		'shp_distance',
		'shp_statusId',
		'shp_createduserId',
	];
	
	protected $guared = [
		'id',
	];
	
	protected $hidden = [
		
	];
	
	public function costs(){
		return $this->hasMany('App\QuoteCost', 'quoteId');
	}
	
	public function products(){
		return $this->hasMany('App\QuoteProduct', 'quoteId');
	}
}