<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierEquipment extends Model {

    use \Venturecraft\Revisionable\RevisionableTrait;

    //
    protected $table = 'm_carrierequipment';
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
    protected $fillable = [
        'carrierId', // FK
        'equipId', // FK}
        'ownerId',
        'equip_description',        
        'equip_no',
        'equip_weight',
        'equip_tareweight',
        'equip_interiorvolume',
        'equip_refridgerate', 
        'equip_purchasedate',
        'equip_year',
        'equip_make',
        'equip_registrationnumber',        
        'equip_registrationdate',
        'equip_registeredamount',
        'equip_licenseplateno',
        'equip_licensestate',
        'equip_licenseexpirydate',
        'equip_tag1',
        'equip_tag2',
        'equip_tag3',
        'equip_chasisnumber',
        'equip_vin',
        'equip_ownername',        
        'equip_ownerphone',
        'equip_owneradr1',
        'equip_owneremail',
        'equip_ownercity',
        'equip_ownerstate',
        'equip_ownerpostal',
        'equip_ownercountry',
        
    ];
    protected $guared = [
    ];
    protected $hidden = [
    ];

    public function carrier() {
        return $this->belongsTo('App\Carrier', 'carrierID');
    }

    public function equipmentType() {
        return $this->belongsTo('App\EquipmentType', 'equipementTypeId');
    }

}
