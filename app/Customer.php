<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_clients';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'client_compname',
		'client_firstname',
		'client_lastname',
		'client_adr1',
		'client_adr2',
		'client_city',
		'client_state',
		'client_postal',
		'client_country',
		'client_email',
		'client_fax',
		'client_phone',
		'client_actdate',
	];
	
	protected $guared = [
		'client_code','client_password',
	];
	
	protected $hidden = [
		'created_at', 'updated_at'
	];
	
	public static function getGeneralValidationRules(array $fields = array()){
		return [
			//
			'client_email' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|email|unique:m_clients,client_email,". $fields['id'] : "required|email|unique:m_clients,client_email",
			'client_compname' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|unique:m_clients,client_compname,". $fields['id'] : "required|unique:m_clients,client_compname",
		];
	}
	
	public function user(){
		return $this->hasOne('App\User', 'clintID');
	}
	
	public function wishList(){
		return $this->hasOne('App\CustomerWishList', 'clientID');
	}
	
	public function billTo(){
		return $this->hasOne('App\ClientBillingInfo', 'clientID');
	}
	
	public function defaultSettings(){
		return $this->hasOne('App\ClientDefaultSettings', 'clientID');
	}
	
	public function carriers(){
		return $this->belongsToMany('App\Carrier', 'm_clientcarrier', 'clientID', 'carrierID')
			->withTimestamps()
			->withPivot(['id', 'acc_ruletariff', 'fuel_ruletariff']);
	}
	
	public function carrierApi(){
		return $this->hasMany('App\CarrierAPI', 'clientID');
	}
	
	public function subClients(){
		return $this->hasMany('App\Customer', 'parent_id');
	}
	
	public function users(){
		return $this->belongsToMany('App\User', 'm_userclientmap', 'clientID', 'userID');
	}
	
    public function accessorial()
    {
        return $this->belongsToMany('App\Accessorial', 'm_clientaccessorial', 'clientId', 'accsId');
    }	
}
