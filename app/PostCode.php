<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCode extends Model {

	//
	protected $table = 'm_postal';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function country(){
		return $this->belongsTo('App\Country', 'country_code');
	}
	
	public function state(){
		return $this->belongsTo('App\State', 'state_code');
	}
	
	public function city(){
		return $this->belongsTo('App\City', 'cityID');
	}
}
