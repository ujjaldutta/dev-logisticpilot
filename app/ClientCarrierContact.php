<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCarrierContact extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_cc_contact';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'cc_contactname',
		'cc_email',
		'cc_contactphone',
		'cc_contacttype',
		'cc_emailtype',
		'cc_adr1',
		'cc_adr2',
		'cc_city',
		'cc_state',
		'cc_postal',
		'cc_country',
	];
	
	protected $guared = [
		
	];
}