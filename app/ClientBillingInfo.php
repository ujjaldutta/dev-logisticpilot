<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBillingInfo extends Model {

	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'm_clientbillto';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'billto_name',
		'billto_adr1',
		'billto_adr2',
		'billto_city',
		'billto_state',
		'billto_postal',
		'billto_country',
		'billto_phone',
		'billto_email',
		'billto_contact',
	];
	
	protected $guarded = [
	
	];
}
