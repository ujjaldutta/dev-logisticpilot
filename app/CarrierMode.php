<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierMode extends Model {

    use \Venturecraft\Revisionable\RevisionableTrait;

    //
    protected $table = 'm_carriermode';
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
    protected $fillable = [
        'carrierId', // FK
        'modeId', // FK
        'minweight',
        'maxweight',
        'comments',
    ];
    protected $guared = [
    ];
    protected $hidden = [
    ];

    public function carrier() {
        return $this->belongsTo('App\Carrier', 'carrierID');
    }

    public function modeType() {
        return $this->belongsTo('App\ModeType', 'modeId');
    }

}
