<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierFuelTariff extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_carrierfuel';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'carrierID',
		'clientID',
		'fuelType',
		'rangefrom',
		'rangeto',
		'LTLRate',
		'TLRate',
		'effectivefrom',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function fuelType(){
		return $this->belongsTo('App\CarrierFuelType', 'fuelType');
	}
	
	public function carrier(){
		return $this->belongsTo('App\Carrier', 'carrierID');
	}
	
	/*
	* static function to execute mysql stored proc to validate and insert data 
	* @param array values
	* @return array
	* @access public static
	*/
	public static function callUploadCsvProc(array $values){
		try{
			//dd($values);
			//getting db manager PDO object
			$pdo = \DB::getPDO();
			
			//setting up Mysql stored proc and its in out params
			$outVar = 'lId';
			$stmt = $pdo->prepare("call sp_importFuelCSV(?,?,?,?,?,?,?,?,@lId)");
			$stmt->bindParam(1, $values['carrierID']);
			$stmt->bindParam(2, $values['clientID']);
			$stmt->bindParam(3, $values['fuel_code']);
			$stmt->bindParam(4, $values['rangefrom']);
			$stmt->bindParam(5, $values['rangeto']);
			$stmt->bindParam(6, $values['LTLRate']);
			$stmt->bindParam(7, $values['TLRate']);
			$stmt->bindParam(8, $values['effectivefrom']);
			$stmt->bindParam(9, $outVar);
			
			//executing the statement and throwing error if occurred
			if(!$stmt->execute())
				throw new \Exception("Error in executing sql statement");
				//throw new \Exception("Error code: ". implode(',', $stmt->errorCode()). " - Error: ". implode(',', $stmt->errorInfo());
			
			//relaesing current resource
			$stmt->closeCursor();
			
			//quering again using PDO statement to get the last insert id
			$queryStmt = $pdo->query("select @". $outVar ." as lastInsertID");
			if(!$queryStmt->execute())
				throw new \Exception("Error in executing sql statement to get last insert id");
			
			$result['success'] = $queryStmt->fetch(\PDO::FETCH_ASSOC);
			//var_dump($result);
			return $result;
		}catch(\Exception $e){
			return array('error' => $e->getMessage());
		}
	}
}