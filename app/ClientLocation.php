<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientLocation extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_clientlocation';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'clientId',
		'location_typeId',
		'location_name',
		'location_code',
		'location_adr1',
		'location_adr2',
		'location_city',
		'location_state',
		'location_postal',
		'location_email',
		'location_notes',
		'location_contact',
		'location_timezone',
		'location_hoursfrom',
		'location_hoursto',
		'location_refcode1',
		'location_refcode2',
		'location_refcode3',
		'location_refcode4',
		'location_refcode5',
		'location_refvalue1',
		'location_refvalue2',
		'location_refvalue3',
		'location_refvalue4',
		'location_refvalue5',
		'location_liftgate',
		'location_apptrequired',
		'location_other',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		'created_at', 'updated_at'
	];
	
	
}