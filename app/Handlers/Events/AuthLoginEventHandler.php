<?php namespace App\Handlers\Events;

use App\LoginDetail;
use App\User;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class AuthLoginEventHandler {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth, Request $request)
	{
		//
		$this->auth = $auth;
		$this->request = $request;
	}

	/**
	 * Handle the event.
	 *
	 * @param  Events  $event
	 * @return void
	 */
	public function handle(User $user, $remember)
	{
		//saving login date time and other information after when a successful login attempt is made
		$loginDetail = new LoginDetail;
		
		//fetching geolocation to know about the user and his/her country and other
		$location = \GeoIP::getLocation();
		
		$locationDetails = isset($location['city']) ? $location['city'] : '';
		if(isset($location['state']))
			$locationDetails .= ', '. $location['state'];
		if(isset($location['postal_code']))
			$locationDetails .= ', '. $location['postal_code'];
		
		$loginDetail->userID = $this->auth->id();
		$loginDetail->adt_logindatetime = \Carbon\Carbon::now()->toDateTimeString();
		$loginDetail->adt_ipaddres = isset($location['ip']) ? $location['ip'] : 'Unknown';
		$loginDetail->adt_location = $locationDetails;
		$loginDetail->adt_country = isset($location['country']) ? $location['country'] : 'Unknown';
		$loginDetail->adt_browsertype = $this->request->server('HTTP_USER_AGENT');
		
		if($loginDetail->save())
			session()->put('adt_logindetails_id', $loginDetail->id);
	}

}
