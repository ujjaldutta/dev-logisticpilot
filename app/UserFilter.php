<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFilter extends Model {
	
	//
	protected $table = 'm_userfilter';
	
	public $timestamps = false;
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function layoutType(){
		return $this->belongsTo('App\LayoutType', 'layoutId');
	}
	
	public function attribute(){
		return $this->belongsTo('App\Layout', 'attributeId');
	}
	
	public function operator(){
		return $this->belongsTo('App\SqlOperator', 'sqloperatorId');
	}
}