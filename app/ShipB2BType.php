<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipB2BType extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'v_b2btype';
	protected $revisionEnabled = true;
	//protected $fillable = ['id', 'shipId', 'ship_b2bId', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function viewb2btypes(){
		return $this->hasMany('App\ShipB2b', 'ship_b2bId');
	}	
	
}
