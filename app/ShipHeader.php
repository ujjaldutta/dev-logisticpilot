<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipHeader extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipheader';
	protected $revisionEnabled = true;
	
	protected $fillable = [
		'shp_paymentermId','shp_paymentermId','shp_loadstatusId','shp_pickupname','shp_shipnumber','shp_pickupadr1','shp_pickupadr2','shp_pickupcity','shp_pickupstate','shp_pickuppostal','shp_pickupcountry', 'shp_deliveryname', 'shp_deliveryadr1', 'shp_deliveryadr2', 'shp_deliverycity', 'shp_deliverystate', 'shp_deliverypostal', 'shp_deliverycountry', 'shp_deliverynotes','shp_deliverytimefrom','shp_deliverytimeto','shp_carrierbilltoname','shp_carrierbilltoadr1','shp_carrierbilltoadr2','shp_carrierbilltocity','shp_carrierbilltostate','shp_carrierbilltocountry','shp_carrierbilltopostal','shp_pickuptimefrom','shp_pickuptimeto','shp_pickupnotes','shp_shipdate','clientId','shp_deliverydate','shp_pickdate','shp_ponumber','shp_sonumber','shp_pronumber','shp_shipmentvalue','shp_valueperpound','shp_billweight','shp_quotenumber','shp_deliverycontact','shp_deliveryphone','shp_pickupcontact','shp_pickupphone','shp_modeId','shp_distance',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function shipstops(){
		return $this->hasMany('App\ShipStop', 'shipId');
	}
	
	public function shipproducts(){
		return $this->hasMany('App\ShipProduct', 'shipId');
	}	
	
	public function shipnotes(){
		return $this->hasMany('App\ShipNote', 'shipId');
	}	
	
	public function shipaccessorials(){
		return $this->hasMany('App\ShipAccessorial', 'shipId');
	}		
	
	public function customers(){
		return $this->belongsTo('App\Customer', 'clientId');
	}
	
	public function carriers(){
		return $this->belongsTo('App\Carrier', 'shp_scac');
	}	
	
	public function shipb2btypes(){
		return $this->hasMany('App\ShipB2b', 'shipId');
	}	
	
	public function shipuserstatus(){
		return $this->hasMany('App\ShipHeaderStatus', 'shipId');
	}		
	
	public function shipdocuments(){
		return $this->hasMany('App\ShipDocument', 'shipId');
	}

	public function shiptracking(){
		return $this->hasMany('App\ShipTracking', 'shipId');
	}	
	
	public function shipStatus(){
		return $this->belongsTo('App\ShipStatus', 'shp_loadstatusId');
	}		
	
	public static function getGeneralValidationRules(array $fields = array()){
		return [
			//
			'shp_equipmentId' => 'required',
			'shp_paymentermId' => 'required',
			'shp_modeId' => 'required',
			'pick_location' => 'required',
			/*'client_email' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|email|unique:m_clients,client_email,". $fields['id'] : "required|email|unique:m_clients,client_email",
			'client_compname' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|unique:m_clients,client_compname,". $fields['id'] : "required|unique:m_clients,client_compname",*/
		];
	}

	public static function getPartialValidationRules(array $fields = array()){
		return [
			//
			'id' => 'required',
			/*'shp_paymentermId' => 'required',
			'shp_loadstatusId' => 'required',
			'pick_location' => 'required',
			'client_email' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|email|unique:m_clients,client_email,". $fields['id'] : "required|email|unique:m_clients,client_email",
			'client_compname' => (isset($fields['id']) && is_numeric($fields['id'])) ? "required|unique:m_clients,client_compname,". $fields['id'] : "required|unique:m_clients,client_compname",*/
		];
	}
}
