<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipTracking extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shiptracking';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId', 'track_date', 'track_time', 'track_statusId', 'track_text', 'track_city', 'track_state', 'track_trailer', 'track_createdbyId', 'track_createdDate', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	public function users(){
		return $this->belongsTo('App\User', 'track_createdbyId');
	}

	public function shipstatus(){
		return $this->belongsTo('App\ShipStatus', 'track_statusId');
	}		
}
