<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerWishList extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'm_custwl';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'wl_message',
		'wl_logo',
		'wl_theme',
		'wl_culture',
	];
	
	public function culture(){
		return $this->belongsTo('App\Locale', 'wl_culture');
	}
	
	public function theme(){
		return $this->belongsTo('App\Theme', 'wl_theme');
	}
}
