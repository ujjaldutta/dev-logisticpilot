<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClientMapping extends Model {
	
	protected $table = 'm_userclientmap';
	
	public $timestamps = false;
	
	protected $hidden = [];
	
	protected $fillable = [];
	
	
}