<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'v_equipmentType';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}