<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierInsurance extends Model {

    use \Venturecraft\Revisionable\RevisionableTrait;

    //
    protected $table = 'm_carrierinsurance';
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
    protected $fillable = [
        'carrierId', // FK
        'insuranceTypeId', // FK
        'insurance_companyname',
        'insurance_agentname',
        'insurance_policyname',
        'insurance_address1',
        'insurance_address2',
        'insurance_city',
        'insurance_state',
        'insurance_postal',
        'insurance_country',
        'insurance_amount',
        'insurance_efffrom',
        'insurance_effto',
    ];
    protected $guared = [
    ];
    protected $hidden = [
    ];

    public function carrier() {
        return $this->belongsTo('App\Carrier', 'carrierID');
    }

    public function insuranceType() {
        return $this->belongsTo('App\InsuranceType', 'insuranceTypeId');
    }

}
