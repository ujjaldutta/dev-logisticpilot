<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCarrier extends Model{
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_clientcarrier';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'acc_ruletariff',
		'fuel_ruletariff',
	];
	
	protected $guared = [
	];
	
	protected $hidden = [
		
	];
}