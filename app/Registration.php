<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'm_registrations';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'reg_usrfirstname', 
		'reg_usrlastname',
		'reg_usremail',
		'reg_company',
		'reg_cno',
		'reg_subsciption_plan_id',
	];
	
	protected $hidden = [
		'id', 
	];
	
	protected $guarded = [
	];
	
	public function user(){
		return $this->belongsTo('App\User');
	}
	
	public function plan(){
		return $this->belongsTo('App\SubscriptionPlan', 'reg_subsciption_plan_id');
	}
}
