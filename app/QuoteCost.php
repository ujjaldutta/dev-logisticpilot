<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteCost extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'quote_cost';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'cost_srlno',
		'cost_carrierid',
		'cost_code',
		'cost_quotedcost',
		'cost_carrierid',
		'cost_ratetype',
	];
	
	protected $guared = [
		'id',
	];
	
	protected $hidden = [
		
	];
}