<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Handlers\Commands\SetLocale as Locale;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		/* \Queue::push(function(){
			//registering logging handler such that it can also log to MongoDB
			$mongoHandler = new \Monolog\Handler\MongoDBHandler(
				new \MongoClient('mongodb://'. env('MONGO_HOST_WITH_AUTH', 'localhost')),
				env('MONGO_LOG_DB', 'logisticspilot'), // you may use default one 'test'
				env('MONGO_LOG_COLLECTION', 'applog') // that's a table or collection in MongoDB terms. Please create one with robomongo
			);
			
			\Log::getMonolog()->pushHandler($mongoHandler);
		}); */
		
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
		
		//global log event listeners to record every log to the db
		/* \Log::listen(function($level, $message, $context){
			\Queue::push(function() use($level, $message, $context){
				$mongoHandler = new \Monolog\Handler\MongoDBHandler(
					new \MongoClient('mongodb://'. env('MONGO_HOST_WITH_AUTH', 'localhost')),
					env('MONGO_LOG_DB', 'logisticspilot'), // you may use default one 'test'
					env('MONGO_LOG_COLLECTION', 'applog') // that's a table or collection in MongoDB terms. Please create one with robomongo
				);
			
				\Log::getMonolog()->pushHandler($mongoHandler);
			});
		}); */
	}

}
