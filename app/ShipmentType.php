<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentType extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	protected $table = 'v_shipmentType';
	protected $revisionEnabled = true;
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}
