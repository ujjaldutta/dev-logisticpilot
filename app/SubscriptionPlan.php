<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model {

	//
	public function apps(){
		return $this->belongsToMany('App\AppModule', 'm_subscription', 'regsubsID', 'appID');
	}
}
