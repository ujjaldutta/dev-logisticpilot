<?php namespace App\Models\Distancecalc;

use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Profile\RateresultModel;

class Distancemile  {
	
	public $request;
	public $contract;
	public $type=3;
     
     function __construct($request,$contract) {
     
       $this->request=$request;
       $this->contract=$contract;
   }

	
	public function rate_calculation(){
		
		$source=$this->request->input('source');
		$destination=$this->request->input('destination');
		
		
		$sourceloc=$source['geometry']['location'];
		$destloc=$destination['geometry']['location'];
		
		$distance=$this->distance($sourceloc['lat'],$sourceloc['lng'],$destloc['lat'],$destloc['lng'],'k');
		
		$contract= RateresultModel::where('cId', '=', $this->contract->_id)->first();
		if($contract){
		$contract->Distance =$distance;
		$contract->save();	
		}
	}
	
	private function distance($lat1, $lon1, $lat2, $lon2, $unit) { 
 
	  $theta = $lon1 - $lon2; 
	  $dist = SIN(DEG2RAD($lat1)) * SIN(DEG2RAD($lat2)) +  COS(DEG2RAD($lat1)) * COS(DEG2RAD($lat2)) * COS(DEG2RAD($theta)); 
	  $dist = ACOS($dist); 
	  $dist = RAD2DEG($dist); 
	  $miles = $dist * 60 * 1.1515;
	  $unit = STRTOUPPER($unit);
	 
	  if ($unit == "K") {
		return ($miles * 1.609344); 
	  } elseif($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
  
  private function location($address){
	  $sourceadress=array('locality'=>'','area'=>'','state'=>'','country'=>'','postal_code'=>'');
	  
	  
	  foreach($address as $data){
			if($data['types'][0]=='locality'){
			$sourceadress['locality']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_2'){
			$sourceadress['area']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_1'){
			$sourceadress['state']=$data['short_name'];
			}
			if($data['types'][0]=='country'){
			$sourceadress['country']=$data['long_name'];
			}
			if($data['types'][0]=='postal_code'){
			$sourceadress['postal_code']=$data['long_name'];
			}
			
		}
		
		return $sourceadress;
  } 	
}
