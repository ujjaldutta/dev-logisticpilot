<?php namespace App\Models\Profile;

use Jenssegers\Mongodb\Model as Eloquent;

class ContractModel extends Eloquent {
	protected $collection = 'm_contract';
	protected $connection = 'mongodb';
	protected $guarded = [];
	protected $primaryKey = '_id';
	
	public function contract(){
		return $this->belongsTo('App\Models\Profile\ProfileModel', 'RateProfileCode');
	}
	
	public function ratelane(){
		
		return $this->hasMany('App\Models\Profile\LaneModel' , 'cId', '_id');
	}
	public function matrixratelane(){
		
		return $this->hasMany('App\Models\Profile\MatrixRatelaneModel' , 'cId', '_id');
	}
	
}
