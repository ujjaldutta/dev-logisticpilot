<?php namespace App\Models\Profile;

use Jenssegers\Mongodb\Model as Eloquent;

class ProfileModel extends Eloquent {
	protected $collection = 'm_profile';
	protected $connection = 'mongodb';
	protected $guarded = [];
	protected $primaryKey = 'RateProfileCode';
	public function Contractors(){
		return $this->hasMany('App\Models\Profile\ContractModel', 'ProfileId','RateProfileCode')->select(array('carrierId','ProfileId'));
	}
	
}
