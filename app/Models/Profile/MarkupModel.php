<?php namespace App\Models\Profile;

use Jenssegers\Mongodb\Model as Eloquent;

class MarkupModel extends Eloquent {
	protected $collection = 'm_rateupcharge';
	protected $connection = 'mongodb';
	protected $guarded = [];
	protected $primaryKey = '_id';
	
	
}
