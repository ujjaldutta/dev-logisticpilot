<?php namespace App\Models\Profile;

use Jenssegers\Mongodb\Model as Eloquent;

class LaneModel extends Eloquent {
	protected $collection = 'm_ratelanes';
	protected $connection = 'mongodb';
	protected $guarded = [];
	
	public function contract(){
		return $this->belongsTo('App\Models\Profile\ContractModel', 'cId');
	}
	
}
