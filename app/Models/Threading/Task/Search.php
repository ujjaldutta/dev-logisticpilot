<?php
/**
 * File:        Task.php
 * Project:     PHP Multi threading
 *
 * @author     Ujjal Dutta
 */
namespace App\Models\Threading\Task;
use App\Models\Threading\Task\Base;
use Session;
class Search extends Base
{
    /**
     * Initialize (called first by the task manager
     * 
     * @return mixed
     */
     private $rateObj;
     
     function __construct($rateObj) {
    
       $this->rateObj=$rateObj;
   }
   
   
    public function initialize() 
    {
		
        return true;
    }

    /**
     * Called by the task manager upon sucess (when the process method returned true)
     * 
     * @return mixed
     */
    public function onSuccess()
    {
		 sleep(1);
		
		 
			  
			  
					  
					  
					   
		
		  //echo '[Pid:' . getmypid() . '] Task executed at ' . date('Y-m-d H:i:s') . PHP_EOL;
        return true;
    }

    /**
     * Called by the task manager upon failure (when the process method returned false)
     * 
     * @return mixed
     */
    public function onFailure() 
    {
        return false;
    }

    /**
     * Main method containing the logic to be executed by the task
     * 
     * @param $params array Assoc array of params
     *
     * @return boolean True upon success, false otherwise
     */
    public function process(array $params = array())
    {
        sleep(1);
   
       foreach($this->rateObj as $obj){
			
		
			$obj->rate_calculation();
			
			
		}
		/*
		$prid2=Session::get('endedpid');
		  $prid2[]=$pid;
		  Session::put('endedpid',$prid2);
		  
		if(getmypid()>0){
				  $prid=Session::get('endpid');
					  array_push($prid, getmypid());
					  Session::put('endpid',$prid);
					  
					  $fp = fopen('data.txt', 'a+');
						fwrite($fp, '<pre>'.getmypid().print_r($prid,true).'</pre>');
						fclose($fp);
				  }*/
      //  echo '[Pid:' . getmypid() . '] Task executed at ' . date('Y-m-d H:i:s') . PHP_EOL;
      
        return true;
    }
}
