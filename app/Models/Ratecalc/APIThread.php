<?php namespace App\Models\Ratecalc;


use App\Models\Profile\RateresultModel;

class APIThread  {
	
	public $request;
	public $contract;
	public $type=3;
     
     function __construct($request,$contract) {
     
       $this->request=$request;
       $this->contract=$contract;
   }
  
  private function location($address){
	  $sourceadress=array('locality'=>'','area'=>'','state'=>'','country'=>'','postal_code'=>'');
	  
	  
	  foreach($address as $data){
			if($data['types'][0]=='locality'){
			$sourceadress['locality']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_2'){
			$sourceadress['area']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_1'){
			$sourceadress['state']=$data['short_name'];
			}
			if($data['types'][0]=='country'){
			$sourceadress['country']=$data['long_name'];
			}
			if($data['types'][0]=='postal_code'){
			$sourceadress['postal_code']=$data['long_name'];
			}
			
		}
		
		return $sourceadress;
  } 
	
	public function rate_calculation(){
		
		$source=$this->request->input('source');
		$source_address=$this->location($source['address_components']);
		
		
		$destination=$this->request->input('destination');
		$destination_address=$this->location($destination['address_components']);
		
		
		$url='http://23.253.41.5:8080/testingapiservicesv2/getRateData?UserID=318578192&DeliveryCity=bloomington&ServiceLevel=Priority&Password=1537917838&DeliveryState=mn&Accs1=&meterno=&DeliveryPostal=55420&type=&Accountnumber=174264350&DeliveryCountry=US&Clientaddress=&PickupCity=tobyhanna&ScacCode=FDEX&Clientcity=&PickupState=PA&PaymentTerm=prepaid&Clientstate=&PickupPostal=18466&ShipDate=&ClientPostal=&PickupCountry=US&Accounttype=shipper&clientcountry=&accountname=&isdebugmode=&IsTestMode=&products=&carrierrateserviceproduct=&usebrokersrv=&Class1=70&Weight1=1000&Height1=&Length1=&NMFC1=&Pallet1=1&Width1=&Class2=&Weight2=&Height2=&Length2=&NMFC2=&Pallet2=&Width2=&Class3=&Height3=&Length3=&NMFC3=&Pallet3=&Weight3=&Width3=&Class4=&Height4=&Length4=&NMFC4=&Pallet4=&Weight4=&Width4=&Class5=&Height5=&Length5=&NMFC5=&Pallet5=&Weight5=&Width5';
		$ch = curl_init();
		$timeout = 10;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$data1 = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode(json_encode((array) ($data1)),1);
		$result=json_decode($result[0]);
		
		/*$fp = fopen('data.txt', 'a+');
		
		fwrite($fp, '<pre>'.$this->contract->carrier['scac'].'</pre>');
		
		fwrite($fp, date("Y-m-d H:s:i")."rateware");
		fclose($fp);
		return;*/
		
		
		
			if(is_object($result) && $result->Result=='Success'){
		
				$ratesearch = new RateresultModel();
				$data['searchId']=$this->request->input('_token');
				$data['clientID']=\CustomerRepository::getCurrentCustomerID();
				$data['rateType']='API';
				$data['cId']=$this->contract->carrier['id'];
				
				$data['ProfileId']=$this->request->input('profile');
				$data['Carrier']=$this->contract->carrier['scac'];
				$data['Rate']= $result->GetRate->TotalShipmentCharge;
				$data['Distance']='';
				$data['Telephone']='';
		
				$data['Fuel']='';
				$data['Accessorial']='';
				
				$data['RateDetails']=(array)$result->GetRate;
								
				$ratesearch->create($data);
				
						
		}
		
		
	}
	
}
