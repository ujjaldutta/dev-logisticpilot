<?php namespace App\Models\Ratecalc;

use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\DesignModel;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Profile\RateresultModel;

class Ratematrix  {
	
	public $request;
	public $contract;
	public $type=6;
	
	function __construct($request,$contract) {
     
       $this->request=$request;
       $this->contract=$contract;
   }
	
	public function rate_calculation(){
		
		$weight=$this->request->input('weight');
		$pieces=$this->request->input('pieces');
		
		$result=$this->contract['matrixratelane'];
		
		
		
		if($result){
		
		foreach($result as $rec){
				$ratesearch = new RateresultModel();
				$data['searchId']=$this->request->input('_token');
				$data['clientID']=\CustomerRepository::getCurrentCustomerID();
				$data['rateType']=$this->type;
				$data['cId']=$rec->cId;
				$data['ratematrixlaneId']=$rec->_id;
				$data['ProfileId']=$this->request->input('profile');
				$data['Carrier']=$this->contract['carrierId'];
				$data['Rate']=$rec['TLRate'];
				$data['Distance']='';
				$data['Telephone']='';
				$data['Carrier']='';
				$data['Fuel']='';
				$data['Accessorial']='';
				
				$data['RateDetails']=$rec->toArray();
								
				$ratesearch->create($data);
			}
		}

	}
	
}
