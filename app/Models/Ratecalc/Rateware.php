<?php namespace App\Models\Ratecalc;

use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\DesignModel;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Profile\RateresultModel;

class Rateware  {
	
	public $request;
	public $contract;
	public $type=3;
     
     function __construct($request,$contract) {
     
       $this->request=$request;
       $this->contract=$contract;
   }
  
  private function location($address){
	  $sourceadress=array('locality'=>'','area'=>'','state'=>'','country'=>'','postal_code'=>'');
	  
	  
	  foreach($address as $data){
			if($data['types'][0]=='locality'){
			$sourceadress['locality']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_2'){
			$sourceadress['area']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_1'){
			$sourceadress['state']=$data['short_name'];
			}
			if($data['types'][0]=='country'){
			$sourceadress['country']=$data['long_name'];
			}
			if($data['types'][0]=='postal_code'){
			$sourceadress['postal_code']=$data['long_name'];
			}
			
		}
		
		return $sourceadress;
  } 
	
	public function rate_calculation(){
		
		/*
		 * 
			$source=$this->request->input('source');
			$source_address=$this->location($source['address_components']);
			
			$destination=$this->request->input('destination');
			$destination_address=$this->location($destination['address_components']);
		
		 * 
		 * $lanerate = LaneModel::with('contract')->where('ProfileId', '=', $this->request->input('profile'))->where('cId', '=', $this->cid);
	
		
		if(!empty($source_address['postal_code'])){
			$name=$source_address['postal_code'];
			$query = array('OriginalZipcode'=> "{$name}" );
			$lanerate->where($query, 'exists', true);
			}
		if(!empty($destination_address['postal_code'])){
			$name=$destination_address['postal_code'];
			$query = array('DestinationZipcode'=> "{$name}" );
			$lanerate->where($query, 'exists', true);
			}	
			*/
		
		
		
		$weight=$this->request->input('weight');
		$pieces=$this->request->input('pieces');
		
		
		
		$result=$this->contract['ratelane'];
		
		
		
		if($result){
		
		foreach($result as $rec){
				$ratesearch = new RateresultModel();
				$data['searchId']=$this->request->input('_token');
				$data['clientID']=\CustomerRepository::getCurrentCustomerID();
				$data['rateType']=$this->type;
				$data['cId']=$rec->cId;
				$data['ratelaneId']=$rec->_id;
				$data['ProfileId']=$this->request->input('profile');
				$data['Carrier']=$this->contract['carrierId'];
				$data['Rate']=$rec['Rateware']['Rate'];
				$data['Distance']='';
				$data['Telephone']='';
				$data['Carrier']='';
				$data['Fuel']='';
				$data['Accessorial']='';
				$data['RateDetails']=$rec->toArray();
								
				$ratesearch->create($data);
			}
		}
		
	
		/*$lanerate = LaneModel::with('contract')->where('ProfileId', '=', $this->request->input('profile'))->where('cId', '=', $this->cid);
		$result=$lanerate->first();
		
		$fp = fopen('data.txt', 'a+');
		fwrite($fp, '<pre>'.print_r($result->Rateware,true).'</pre>');
		
		fwrite($fp, date("Y-m-d H:s:i")."rateware");
		fclose($fp);*/
	}
	
}
