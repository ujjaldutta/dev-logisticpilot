<?php namespace App\Models\Ratecalc;

use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\DesignModel;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Profile\RateresultModel;


class Costperhunderd  {
	public $request;
	public $contract;
	public $type=8;
	function __construct($request,$contract) {
     
       $this->request=$request;
       $this->contract=$contract;
   }
   
	public function rate_calculation(){
		
		$weight=$this->request->input('weight');
		$pieces=$this->request->input('pieces');
		
		$result=$this->contract['ratelane'];
		
		
		
		if($result){
		
		foreach($result as $rec){
				$ratesearch = new RateresultModel();
				$data['searchId']=$this->request->input('_token');
				$data['clientID']=\CustomerRepository::getCurrentCustomerID();
				$data['rateType']=$this->type;
				$data['cId']=$rec->cId;
				$data['ratelaneId']=$rec->_id;
				$data['ProfileId']=$this->request->input('profile');
				$data['Carrier']=$this->contract['carrierId'];
				$data['Rate']=$rec['CostPerHundredRate']['Rate'];
				$data['Distance']='';
				$data['Telephone']='';
				$data['Carrier']='';
				$data['Fuel']='';
				$data['Accessorial']='';
				
				$data['RateDetails']=$rec->toArray();
								
				$ratesearch->create($data);
			}
		}
		
		/*
		$lanerate = LaneModel::with('contract')->where('ProfileId', '=', $this->request->input('profile'))->where('cId', '=', $this->cid);
		
		//$lanerate->where('clientID',\CustomerRepository::getCurrentCustomerID());
		$result=$lanerate->first();
		
		//RateresultModel::where("searchId",$this->request->input('_token'))->delete();
		if($result){
		$ratesearch = new RateresultModel();
		$data['searchId']=$this->request->input('_token');
		$data['clientID']=\CustomerRepository::getCurrentCustomerID();
		$data['rateType']=$this->type;
		$data['cId']=$this->cid;
		$data['ProfileId']=$this->request->input('profile');
		$data['RateDetails']=$result->toArray();
		
		$ratesearch->create($data);
		}
		* */
		/*
		$fp = fopen('data.txt', 'a+');
		fwrite($fp, '<pre>'.print_r($this->request->all(),true).'</pre>');
		fwrite($fp, date("Y-m-d H:s:i")."Costperhundred");
		fclose($fp);
		*/
	}
	
}
