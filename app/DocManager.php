<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DocManager extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'doc_manager';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'doc_filename',
		'doc_path',
		'doc_filetype',
	];
	
	protected $guared = [
	
	];
	
	protected $hidden = [
		
	];
}
