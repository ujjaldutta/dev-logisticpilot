<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	//
	protected $table = 'm_city';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function postCodes(){
		return $this->hasMany('App\PostCode', 'cityId');
	}
	
	public function country(){
		return $this->belongsTo('App\Country', 'country_code');
	}
	
	public function state(){
		return $this->belongsTo('App\State', 'state_code');
	}
}
