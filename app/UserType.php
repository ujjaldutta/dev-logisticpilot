<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'v_usertype';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}