<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model {
	
	//
	protected $table = 'm_layout';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function layoutType(){
		return $this->belongsTo('App\LayoutType', 'layoutId');
	}
	
	/**
	* @Returns Illuminate\Support\Collection
	*/
	public static function getDisplayAttributes($userId, $layoutTypeId){
		$allFields = self::where('userId', $userId)
			->where('layoutId', $layoutTypeId)
			->orderBy('displayOrder','ASC')
			->get(['id', 'displayName', 'displayField']);

		if(!count($allFields) || (count($allFields) && !$allFields->count())){
			//dd($allFields);
			$allFields = self::whereNull('userId')
				->where('layoutId', $layoutTypeId)
				->orderBy('displayOrder','ASC')
				->get(['id', 'displayName', 'displayField']);	
		}
		
		return $allFields;
	}
}