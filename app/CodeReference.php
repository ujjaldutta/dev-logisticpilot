<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeReference extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_codereference';
	protected $revisionEnabled = true;
        protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'codeId',            
                'codeType',
                'codeValue',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		'created_at', 'updated_at'
	];	
}