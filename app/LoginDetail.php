<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginDetail extends Model {

	//
	protected $table = 'adt_logindetails';
	
	protected $fillable = [
		'adt_logindatetime',
		'adt_logoutdatetime',
		'adt_ipaddres',
		'adt_location',
		'adt_country',
		'adt_browsertype',
	];
	
	public $timestamps = false;
}
