<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InvHeader extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'inv_header';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'clientId','loadId','inv_number','inv_date','inv_billto_custId','inv_billto_name','inv_billto_address1','inv_billto_address2','inv_billto_city','inv_billto_state','inv_billto_postal','inv_billto_country','inv_email','inv_amount','inv_paytermsId','inv_invtermsId','inv_notes','inv_manifestId','inv_statusId',
	'inv_emaileddate','inv_remitname','inv_remitaddress1','inv_remitaddress2','inv_remitcity','inv_remitstate','inv_remitpostal','inv_remitcountry',
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		//return $this->belongsTo('App\ShipHeader', 'shipId');
	}
}
