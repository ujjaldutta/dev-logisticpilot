<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipProduct extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipproduct';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId','prod_srlno','prod_itemcode','prod_itemname','prod_itempieces','prod_itemqty','prod_itempkgtypeId','prod_itemclass','prod_itemweight','prod_itemwidth','prod_itemheight','prod_itemlength','prod_stopId','prod_itemhazmat','prod_itemnmfc','prod_cubefeet','prod_orderId','prod_accountcode',
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
}
