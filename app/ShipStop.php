<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipStop extends Model {
   use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipstop';
	protected $revisionEnabled = true;
	protected $historyLimit = 500;
	protected $fillable = ['id', 'shipId','stopId','stop_name','stop_adr1','stop_adr2','stop_city','stop_state','stop_postal','stop_type',
		'stop_country','stop_email','stop_contactname','stop_contactphone','stop_prodqty','stop_ponumber','stop_prodId',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
}
