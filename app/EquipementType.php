<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model {
	
	protected $table = 'v_equipmenttype';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
}