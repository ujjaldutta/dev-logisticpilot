<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipAccessorial extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipaccessorial';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId','accs_srlno','accs_scac','accs_code','accs_quotedcost','accs_billedcost','accs_ratedcost','accs_approvedcost','accs_ratetype', 'accs_customercost',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	public function carriers(){
		return $this->belongsTo('App\Carrier', 'scac');
	}	
}
