<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierRemit extends Model {

    use \Venturecraft\Revisionable\RevisionableTrait;

    //
    protected $table = 'm_carrierremit';
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
    protected $fillable = [
        'carrierId', // FK
        'remit_name',        
        'remit_address1',
        'remit_address2',
        'remit_city',
        'remit_state',
        'remit_postal',
        'remit_country',
        'remit_currency',
        
    ];
    protected $guared = [
    ];
    protected $hidden = [
    ];

    public function carrier() {
        return $this->belongsTo('App\Carrier', 'carrierID');
    }


}
