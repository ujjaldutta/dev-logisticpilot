<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipNote extends Model {

	//
	protected $table = 'trn_shipnotes';
	
	protected $fillable = ['id', 'shipId','stopId','ship_notestypeId','ship_notes','ship_createdbyId','ship_createddate',];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	public function users(){
		return $this->belongsTo('App\User', 'ship_createdbyId');
	}	
}
