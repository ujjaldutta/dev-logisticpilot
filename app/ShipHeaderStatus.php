<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipHeaderStatus extends Model {
   use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipstatus';
	protected $revisionEnabled = true;
	protected $historyLimit = 500;
	protected $fillable = ['id', 'shipId', 'status_statusId', 'status_changeddate', 'status_changedbyId', 'status_notes',];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
    public function carrier() {
        return $this->belongsTo('App\ShipHeader', 'shipId');
    }	
	
	/*public function shipheaders(){
		return $this->hasMany('App\ShipHeader', 'id');
	}	*/	
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
}
