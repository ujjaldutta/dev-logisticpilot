<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipNote extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipnotes';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId', 'ship_notestypeId', 'ship_notes', 'ship_createdbyId', 'ship_createddate', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	public function users(){
		return $this->belongsTo('App\User', 'ship_createdbyId');
	}	
}
