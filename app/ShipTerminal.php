<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipTerminal extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipterminal';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId', 'trm_orginname', 'trm_origintype', 'trm_originadr1', 'trm_originadr2', 'trm_origincity', 'trm_originstate', 'trm_originpostal', 'trm_originemail', 'trm_originphone', 'trm_origincontactname', 'trm_deliveryname', 'trm_deliverytype', 'trn_deliveryadr1', 'trn_deliveryadr2', 'trn_deliverycity', 'trn_deliverystate', 'trn_deliverypostal', 'trn_deliverycontact', 'trn_deliveryphone', 'trn_deliveryemail',];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	/*public function users(){
		return $this->belongsTo('App\User', 'ship_createdbyId');
	}	*/
}
