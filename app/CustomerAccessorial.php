<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAccessorial extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_clientaccessorial';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'clientId',            
                'accsId',
                'created_date',
                'createdbyId',       
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		'created_at', 'updated_at'
	];
	
	
}