<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model {

    use \Venturecraft\Revisionable\RevisionableTrait;

    //
    protected $table = 'm_carrier';
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
    protected $fillable = [
        'scac',
        'carrier_name',
        'carrier_postal',
        'carrier_adr1',
        'carrier_adr2',
        'carrier_city',
        'carrier_state',
        'carrier_country',
        'carrier_email',
        'carrier_phone',
        'carrier_mc',
        'carrier_xrefcode',
        'carrier_rateapi',
        'carrier_bookingapi',
        'carrier_trackingapi',
        'carrier_imageapi',        
        'carrier_type',
        'carrier_smartwaypay',
        'carrier_mexicoauth',
        'carrier_canauth',
        'carrier_acceptbid',
        'carrier_safetyrating',
        'carrier_hazmat',
        'carrier_hazmatcontact',
        'carrier_hazmatphone',
        'carrier_currencycode',
    ];
    protected $guared = [
    ];
    protected $hidden = [
    ];

    public function customers() {
        return $this->belongsToMany('App\Customer', 'm_clientcarrier', 'carrierID', 'clientID')
                        ->withTimestamps();
    }

    public function insurances() {
        return $this->hasMany('App\CarrierInsurance', 'carrierId');
    }
    
    public function modes() {
        return $this->hasMany('App\CarrierMode', 'carrierId');
    }
    
    public function equipments() {
        return $this->hasMany('App\CarrierEquipment', 'carrierId');
    }

    public function shpaccess() {
        return $this->hasMany('App\ShipAccessorial', 'accs_scac');
    }	
	
}
        