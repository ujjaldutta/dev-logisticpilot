<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTermsType extends Model {

	protected $table = 'v_invoiceTermsType';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}
