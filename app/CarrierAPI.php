<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierAPI extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'rate_carrierapi';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'carrierID',
		'rate_apipwd',
		'rate_apiuserid',
		'rate_apishiptype',
		'rate_apiaccount',
		'rate_apipaytype',
		'rate_apiactivated',
		'rate_usercrrfuel',
		'rate_usrcrraccs',
		'rate_enablepickup',
		'rate_enablerate',
		'rate_enabletracking',
		'rate_apidirectonly',
		'contract_profilecode',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function contract(){
		return $this->belongsTo('App\DocManager', 'rate_docID');
	}
	
	public function zipCodes(){
		return $this->hasMany('App\ApiDetail', 'rateapidetailID');
	}
	public function carrier(){
		
		return $this->hasOne('App\Carrier', 'id','carrierID')->select(['id','scac','carrier_name']);
	}
}
