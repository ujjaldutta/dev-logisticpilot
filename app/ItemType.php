<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//

	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.

	protected $table = 'v_itemtype';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}
