<?php namespace App\Http\Controllers;

use LP\Settings\Repositories\SiteAccessRepository;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(SiteAccessRepository $site)
	{
		$this->middleware('auth');
		$this->middleware('terms');
		$this->middleware('regsteps', ['except' => ['getRegistrationSteps', 'getAjaxGetRegistrationSteps']]);
		//$this->middleware('setLocale');
		
		$this->site = $site;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//print $this->site->getPackageID(\Auth::user());
		//print $this->site->getPackageName(\Auth::user());
		//dd( \SiteAccess::getAppGroupswithApps(\Auth::user()));
		
		
		return view('home');
	}
	
	public function getRegistrationSteps(){
		return view('regsteps.steps');
	}
	
	public function getAjaxGetRegistrationSteps($step = 1){
		switch($step){
			case 1:
				//fetching details if submitted previously i.e. continuing from last saved session
				$profile = \App\Customer::find(\Auth::user()->clientID);
				//echo '<pre>';var_dump($profile->toArray());
				return view('regsteps.step1')->withProfile($profile);
			case 2:
				//fetching details if submitted previously i.e. continuing from last saved session
				$billingInfo = \App\ClientBillingInfo::where('clientID', \Auth::user()->clientID)->first();
				//echo '<pre>';var_dump($billingInfo->toArray());
				return view('regsteps.step2')->withBillingInfo($billingInfo);
			case 3:
				return view('regsteps.step3');
			case 4:
				return view('regsteps.step4');
			case 5:
				return view('regsteps.step5');
			default:
				return view('regsteps.form');
		}
	}
}
