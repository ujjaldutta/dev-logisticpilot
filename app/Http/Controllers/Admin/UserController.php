<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Admin\Controller;
use App\User;
use App\RandomPasswordGenerator;

class UserController extends Controller{
	
	/*
	|--------------------------------------------------------------------------
	| Admin User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders admin dashboard once after logged in as an admin
	|
	*/
	
	const PAGELIMIT = 10;
	
	public function __construct(Guard $auth, Request $request){
		$this->request = $request;
		$this->auth = $auth;
	}
	
	public function getDashboard(){
		$users = new \stdClass;		
		return \View::make('admin.users.dashboard')
			->withUsers($users);
	}
	
	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		//dd($request->all());
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password') + ['usr_type_id' => \Config::get('app.superAdminID')];

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			return redirect()->to(\Config::get('app.adminPrefix'). '/users/dashboard');
		}

		return redirect()->back()
				->withInput()
				->withErrors(new MessageBag(
					['auth' => [
						'Either your email or password maybe wrong or your account is inactive.'
					]]
				));
	}
	
	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'These credentials do not match our records.';
	}
	
	public function anyLogout(){
		if(\Auth::check())
			\Auth::logout();
		
		return redirect()->to(\Config::get('app.adminPrefix'). '/users/login');
	}
	
	public function getChangePassword(){
		return view('admin.users.change_password');
	}
	
	public function postChangePassword(){
		//validating form
		$v = \Validator::make($this->request->all(), [
			'old_password' => 'required',
			'password' => 'required|confirmed',
		]);
		
		if($v->fails())
			return redirect()->back()->withErrors($v->messages()->all());
		
		$oldPass = $this->request->get('old_password');
		$newPass = $this->request->get('password');
		
		//checking old password matches with the currently given password
		//dd(\Hash::check($oldPass, $this->auth->user()->password));
		
		if(\Hash::check($oldPass, $this->auth->user()->password)){
			$user = User::find($this->auth->id());
			$user->password = \Hash::make($newPass);
			if($user->save())
				return redirect(\Config::get('app.adminPrefix'). '/users/change-password')->with('success', 'The Password has been updated successfully');
			else
				return redirect(\Config::get('app.adminPrefix'). '/users/change-password')->with('error', 'The Password has not been updated successfully');
		}else
			return redirect(\Config::get('app.adminPrefix'). '/users/change-password')->with('error', 'The Old Password does not match with our records');
	}
}