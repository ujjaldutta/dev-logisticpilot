<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;
use App\Registration;
use App\User;

use App\RandomPasswordGenerator;

use Illuminate\Http\Request;

class RegistrationController extends Controller {
	
	public function __construct(Request $request){
		$this->request = $request;
	}
	
	/* 
	* Method to get list of registered members
	*
	*/
	public function getList(){
		$registrations = Registration::with([
			'plan' => function($query){
				$query->select(['id', 'name']);
			}
		])
		->select([
			'id',
			\DB::raw("CONCAT(`reg_usrfirstname`, ' ', `reg_usrlastname`) as reg_username"),
			'reg_usremail',
			'reg_subsciption_plan_id',
			'userID',
			'created_at',
		])
		->get();
		//iterating through rows and formatting as per the requirement
		$data = [];
		foreach($registrations as $registration){
			$data[] = array(
				'id' => $registration->id, 
				'name' => $registration->reg_username,
				'email' => $registration->reg_usremail,
				'plan' => $registration->plan->name,
				'status' => empty($registration->userID) ? '<span class="links activate">Inactive</span>' : 'Active',
				'created_on' => $registration->created_at->toIso8601String(),
				'actions' => null,
				'token' => csrf_token(),
			);
		}
		//print_r($registrations->toArray());
		return response()->json(['data' => $data]);
	}
	
	public function postSetAction(){
		$id = $this->request->get('id');
		$status = $this->request->get('status');
		
		//toggling status
		//$status = !$status;
		
		//reading registration data and creating user record with new password
		$registration = Registration::findorFail($id);
		
		//updating and generating mail with a new password when activated
		$newPassword = RandomPasswordGenerator::generate();
		
		//validating data to prevent duplicate user records specially the user id/email
		$v = \Validator::make(
			['email' => $registration->reg_usremail], 
			['email' => 'required|email|unique:m_users,email']
		);
		
		//throwing error if validation fails
		if($v->fails())
			return response()->json(['error' => $v->errors()->all()]);
		
		$user = new User;
		$user->fill([
			'usr_firstname' => $registration->reg_usrfirstname,
			'usr_lastname' => $registration->reg_usrlastname,
			'email' => $registration->reg_usremail,
			'usr_email' => $registration->reg_usremail,
			'usr_phone' => $registration->reg_cno,
			
		]);
		
		$user->usr_type_id = \Config::get('app.userAdminType');
		$user->password = \Hash::make($newPassword);
		
		//saving new user
		if($user->save()){
			//now updating the registration table with the user id
			$registration->userID = $user->id;
			$verifyToken = md5(uniqid());
			$registration->reg_verify_token = $verifyToken;
			
			if($registration->save()){
				//notifying the admin about this signup via an email
				\Mail::send('emails.user_activated', 
					[
						'fname' => $registration->reg_usrfirstname,
						'loginid' => $registration->reg_usremail, 
						'password' => $newPassword,
						'token' => $verifyToken,
						'plan' => $registration->plan->name,
					],
					function($message) use($registration){
							$message->to($registration->reg_usremail, $registration->reg_usrfirstname. ' '. $registration->reg_usrlastname)
								->subject('Your account has been approved, please verify this email to activate your account');
					}
				);
				
				return response()->json(['success' => 'Registration activated successfully']);
			}else
				return response()->json(['error' => 'Registration not activated successfully']);
		}else
			return response()->json(['error' => 'user creation error']);
	}
	
	/*
	* method to view a registration
	*/
	public function getView($id){
		$registration = Registration::findorFail($id);
		
		return \View::make('admin.registrations.ajax_reg_details')
			->withRegistration($registration);
	}
	
	/*
	* method to delete a registration record
	*/
	public function postDelete(){
		$id = $this->request->get('id');
		if(Registration::destroy($id))
			return response()->json(['success' => 'The User deleted successfully']);
		else
			return response()->json(['error' => 'The User not deleted successfully']);
	}
}