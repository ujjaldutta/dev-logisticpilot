<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Registration;
use App\SubscriptionPlan;

use Illuminate\Http\Request;

class RegistrationController extends Controller {

	public function __construct(Request $request){
		//parent::__construct();
		
		$this->request = $request;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return response()->json(['success' => 'Here']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		/*if(!$this->request->ajax())
			return abort(405, 'Bad Request to access this method');
		*/
		$v = \Validator::make($this->request->all(), [
			'reg_usrfirstname' => 'required',
			'reg_usrlastname' => 'required',
			'reg_usremail' => 'required|email|unique:m_registrations,reg_usremail',
			'reg_company' => 'required',
			'reg_cno' => 'required',
			'captcha' => 'required|captcha',
		],['captcha' => 'Captcha image is invalid']);
		
		if($v->fails()){
			return response()->json(['error' => $v->errors()->all()]);
		}
		
		$userData = $this->request->except(['_token']);
		$registration = new Registration();
		$registration->fill($userData);
		
		//fetching geolocation to know about the user and his/her country and other
		$location = \GeoIP::getLocation();
		
		$registration->reg_userip = isset($location['ip']) ? $location['ip'] : 'Unknown';
		
		//getting location info for this registration
		$locInfo = ''; 
		if(isset($location['city']))
			$locInfo .= $location['city'];
		if(isset($location['state']))
			$locInfo .= ', '. $location['state'];
		if(isset($location['country']))
			$locInfo .= ', '. $location['country'];
		
		$registration->reg_usrlocation = $locInfo;
		$registration->reg_usrbrowser = $this->request->server('HTTP_USER_AGENT');
		
		if($registration->save()){
			//notifying the admin about this signup via an email
			\Mail::send('emails.user_registered_admin', 
				$userData + ['plan_name' => SubscriptionPlan::where('id', $this->request->get('subscription_plan_id'))->pluck('name')],
				function($message){
						$message->to(env('ADMIN_EMAIL_ADDRESS'), 'Logistics Pilot Admin')
							->subject('New user signed up on Logistics Pilot');
						
				}
			);
			return response()->json(['success' => 'User registered successfully', 'captcha' => captcha_src()]);
		}else
			return response()->json(['error' => 'Error occurred in registering the user']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
