<?php namespace App\Http\Controllers;

use App\Country;
use App\PostCode;
use App\City;
use App\State;

use Illuminate\Http\Request;

class LocationController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Location Controller
	|--------------------------------------------------------------------------
	|
	| 
	|
	*/
	public function getCountryList(Request $request){
		$hint = $request->get('q');
		
		$country = Country::select([
			\DB::raw('country_name as name'), \DB::raw('country_code as code')
		]);
		
		if(!empty($hint))
			$country->where('country_name', 'LIKE', '%'.$hint.'%');
		
		return response()->json($country->get()->toArray());
	}
	
	public function getPostCodeList(Request $request){
		$hint = $request->get('q');
		$ccode = $request->get('ccode');
		$scode = $request->get('scode');
		$cityName = $request->get('cityName');
		
		if(!empty($hint) && strlen($hint) >= 2){
			$zipcodes = PostCode::where('postal_code', 'LIKE',  $hint.'%')
			->with([
				'country' => function($query) use($ccode){
					$query->select(['id', 'country_name', 'country_code']);
					if(!empty($ccode)){
						$query->where('country_code', $ccode);
					}
				},
				'state' => function($query) use($scode){
					$query->select(['id', 'state_name', 'state_code']);
					if(!empty($scode)){
						$query->where('state_code', $scode);
					}
				},
				'city' => function($query) use($cityName){
					$query->select(['id', 'city_name']);
					if(!empty($cityName)){
						$query->where('city_name', 'LIKE', $cityName);
					}
				},
			])
			->get()
			->toArray();
			
			$filteredZipcodes = array();
			
			foreach($zipcodes as $zipcode){
				if(count($zipcode['country']) && count($zipcode['state']) && count($zipcode['city']))
					$filteredZipcodes[] = [
						'postal_code' => $zipcode['postal_code'],
						'country_code' => $zipcode['country']['country_code'],
						'state_code' => $zipcode['state']['state_code'],
						'city_name' => $zipcode['city']['city_name'],
						'description' => $zipcode['city']['city_name'].', '.$zipcode['state']['state_name'].', '. $zipcode['country']['country_name']
					];
			}
			
			return response()->json($filteredZipcodes);
		}
		
		return response()->json([]);
	}
	
	public function getStateList(Request $request){
		$hint = $request->get('q');
		$ccode = $request->get('ccode');
		
		$states = State::where('state_name', 'LIKE', '%'. $hint. '%')
			->with([
				'country' => function($query) use($ccode){
					$query->select(['id', 'country_name']);
					if(!empty($ccode)){
						$query->where('country_code', $ccode);
					}
				},
			])->get()->toArray();
			
		$filtered_states = array();
		
		foreach($states as $state){
			if(count($state['country']))
				$filtered_states[] = [
					'name' => $state['state_name'],
					'code' => $state['state_code'],
				];
		}
		
		return response()->json($filtered_states);
	}
	
	public function getCityList(Request $request){
		$hint = $request->get('q');
		$ccode = $request->get('ccode');
		$scode = $request->get('scode');
		
		$cities = City::where('city_name', 'LIKE', '%'. $hint. '%')
			->with([
				'country' => function($query) use($ccode){
					$query->select(['id', 'country_name']);
					if(!empty($ccode)){
						$query->where('country_code', $ccode);
					}
				},
				'state' => function($query) use($scode){
					$query->select(['id', 'state_name']);
					if(!empty($scode)){
						$query->where('state_code', $scode);
					}
				},
			])->get()->toArray();
			
		$filtered_cities = array();
		
		foreach($cities as $city){
			if(count($city['country']) && count($city['state']))
				$filtered_cities[] = [
					'name' => $city['city_name'],
					'description' => $city['city_name']. ', '. $city['state']['state_name']. ', '. $city['country']['country_name'],
				];
		}
		
		return response()->json($filtered_cities);
	}
}