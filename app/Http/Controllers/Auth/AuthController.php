<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use LP\Customers\Exceptions\CustomerRepositoryException;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;

use App\User;
use App\SubscriptionPlan;
use App\RandomPasswordGenerator;
use App\Registration;
use App\LoginDetail;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar, Request $request)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->request = $request;

		$this->middleware('guest', ['except' => ['anyLogWarn', 'getLogout','getChangePassword', 'postChangePassword']]);
		$this->middleware('auth', ['only' => ['getChangePassword', 'postChangePassword']]);
	}
	
	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		return view('auth.login');
	}
	
	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		//dd($request->all());
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password') + ['usr_completereg' => true];

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			try{
			//setting the current customer id initially for the logged in user
			//dd($this->auth->user()->client);
			if($this->auth->user()->client != null)
			{
				if($this->auth->user()->client->count()){
					\CustomerRepository::setCurrentCustomer($this->auth->user()->client->toArray());
				}
			}
			
			return redirect()->intended($this->redirectPath());
			
			}catch(CustomerRepositoryException $e){
				return redirect()->back()
					->withInput($request->only('usr_password', 'password'))
					->withErrors([
						'usr_password' => $e->getMessage(),
					]);
			}
		}

		return redirect($this->loginPath())
			->withInput($request->only('usr_password', 'password'))
			->withErrors([
				'usr_password' => $this->getFailedLoginMessage(),
			]);
	}
	
	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister($subscription_plan_id)
	{
		//checking for valid subscription plan
		if(SubscriptionPlan::where('id', $subscription_plan_id)->exists())
			return view('auth.register')
				->withSubscriptionPlanId($subscription_plan_id);
		else
			throw new \Exception('The requested plan does not exists');
	}
	
	/**
	 * method to register users via the registration ajax method
	 * @param void
	 * @return \Illuminate\Http\Response
	 */
	public function postAjaxRegister(){
		/*if(!$this->request->ajax())
			return abort(405, 'Bad Request to access this method');
		*/
		$v = \Validator::make($this->request->all(), [
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required|email|unique:users,email',
			'company_name' => 'required',
			'cno' => 'required|numeric|digits_between:10,10',
		]);
		
		if($v->fails()){
			return response()->json(['error' => $v->errors()->all()]);
		}
		
		$userData = $this->request->except(['_token']);
		$registration = new Registration();
		$registration->fill($userData);
		if($registration->save()){
			//notifying the admin about this signup via an email
			\Mail::send('emails.user_registered_admin', 
				$userData + ['plan_name' => SubscriptionPlan::where('id', $this->request->get('subscription_plan_id'))->pluck('name')],
				function($message){
						$message->to(env('ADMIN_EMAIL_ADDRESS'), 'Logistics Pilot Admin')
							->subject('New user signed up on Logistics Pilot');
						
				}
			);
			return response()->json(['success' => 'User registered successfully']);
		}else
			return response()->json(['error' => 'Error occurred in registering the user']);
	}
	
	public function getActivate($token){
		$registration = Registration::select(['id', 'userID', 'reg_verify_token'])->where('reg_verify_token', $token)->first();
		//finding the user with the verify token
		if(count($registration) && $registration->count()){
			$user = User::findOrFail($registration->userID);
			$user->usr_completereg = true;
			//saving after verifying the user
			if($user->save()){
				//now updating registration parameters to finalize the activations
				$registration->reg_verify_token = null;
				$registration->reg_emailverified = true;
				
				if($registration->save())
					return redirect('/')->with('success', 'Your account has been activated successfully');
				else
					return redirect('/')->withErrors(['Your account has not been activated successfully']);
			}else
				return redirect('/')->withErrors(['Your account has not been activated successfully']);
		}else
			return \App::abort(500, 'There is something went wrong with the activation process');
	}
	
	/**
	 * Get the post register / login redirect path.
	 *
	 * @return string
	 */
	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
	}

	/**
	 * Get the path to the login route.
	 *
	 * @return string
	 */
	public function loginPath()
	{
		return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
	}
	
	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{
		//recording logout time whenever possible
		if(session()->has('adt_logindetails_id')){
			//updating logout time
			$loginDetail = LoginDetail::find(session()->get('adt_logindetails_id'));
			$loginDetail->adt_logoutdatetime = \Carbon\Carbon::now()->toDateTimeString();
			
			$loginDetail->save();
			
			//destroying session data for login details
			session()->forget('adt_logindetails_id');
			
			//destroying customer repository obj
			\CustomerRepository::removeCurrentCustomer();
		}
		
		//clearing locale setting on every logout
		if(session()->has('locale'))
			session()->forget('locale');
		
		//logging out the user
		$this->auth->logout();
		
		return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
	}
	
	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'These credentials do not match our records.';
	}
	
	/**
		
	**/
	public function getChangePassword(){
		return view('auth.change_password');
	}
	
	public function postChangePassword(){
		//validating form
		$v = \Validator::make($this->request->all(), [
			'old_password' => 'required',
			'password' => 'required|confirmed',
		]);
		
		if($v->fails())
			return redirect()->back()->withErrors($v->messages()->all());
		
		$oldPass = $this->request->get('old_password');
		$newPass = $this->request->get('password');
		
		//checking old password matches with the currently given password
		//dd(\Hash::check($oldPass, $this->auth->user()->password));
		
		if(\Hash::check($oldPass, $this->auth->user()->password)){
			$user = User::find($this->auth->id());
			$user->password = \Hash::make($newPass);
			if($user->save())
				return redirect('/auth/change-password')->with('success', 'The Password has been updated successfully');
			else
				return redirect('/auth/change-password')->with('error', 'The Password has not been updated successfully');
		}else
			return redirect('/auth/change-password')->with('error', 'The Old Password does not match with our records');
	}
}
