<?php namespace App\Http\Controllers;

use App\Carrier;


use Illuminate\Http\Request;

class CarrierController extends Controller {
	
	/*
	* method to supply carrier names with partial details
	*/
	public function getAjaxCarrierList(Request $request, Carrier $carrier){
		$query = $request->get('q');
		$alreadySelected = $request->get('selected');
		
		if(!empty($query)){
			$foundCarriers = $carrier->select([
					'id', 
					'carrier_name', 
					'carrier_logo', 
					'carrier_adr1', 
					'carrier_adr2', 
					'carrier_city',
					'carrier_state',
					'carrier_postal',
					'carrier_country',
				])
				->where('carrier_name', 'LIKE', "%{$query}%")
				->where('carrier_status', 1);
			
			//code to exclude already selected carriers 	
			if(!empty($alreadySelected)){
				$alreadySelected = explode(',', $alreadySelected);
				$alreadySelected = count($alreadySelected) ? $alreadySelected : [];
				//print_r($alreadySelected);
				$foundCarriers->whereNotIn('id', $alreadySelected);
			}
			
			//finally fetching the records with all filters
			$foundCarriers = $foundCarriers->get();
			$processedCarriers = array();	
			foreach($foundCarriers as $carrier){	
				$processedCarriers[] = array(
					'id' => $carrier->id, 
					'name' => $carrier->carrier_name,
					'logo' => asset('/uploads/carriers/'. $carrier->carrier_logo),
					'address1' => $carrier->carrier_adr1,
					'address2' => $carrier->carrier_adr2,
					'city' => $carrier->carrier_city,
					'state' => $carrier->carrier_state,
					'postal' => $carrier->carrier_postal,
					'country' => $carrier->carrier_country,
				);
			}
			
			return response()->json($processedCarriers);
		}else
			return response()->json([]);
	}
}