<?php

namespace App\Http\Controllers\Front;


use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Front\Controller;
use App\User;
use MongoClient;
use App\Layout;
use App\Traits\UtilityTrait;
use App\Traits\LookupServiceTrait;
use Illuminate\Pagination\Paginator;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;
use App\Models\Profile\MarkupModel;
use App\Models\Profile\RatematrixModel;
use App\Models\Profile\MatrixRatelaneModel;
use DB;
use App\Country;

class ProfileController extends Controller {
	const UPLOAD_DIRECTORY = 'uploads',
		BRAND_IMAGE_DIRECTORY = 'brands',
		CARRIER_IMAGE_DIRECTORY = 'carriers',
		DOCMANAGER_DIRECTORY = 'docmanager',
		CONTRACT_DIRECTORY = 'contract';
		
      use LookupServiceTrait;

  public function __construct() {
      $this->middleware('auth');

      }

   
   
    public function anyIndex() {
	
       $userId = \Auth::user()->id;
        $checkLayout = Layout::where('userId', $userId)
                ->where('layoutId', \Config::get('AppLayout.customerApp'))
                ->count();
    
        if (!$checkLayout) {
            $allFields = Layout::whereNull('userId')->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        } else {
            $allFields = Layout::where('userId', $userId)->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        }
		///dd($checkLayout);
		//exit;
		
        return view('profiles.index')->with('allFields', $allFields);
    }
    
    
    public function postProfileAjaxbyid(Request $request){
		$profile=ProfileModel::where('_id', '=', $request->input('cid'))->get();
	 
	   if(is_object($profile)){
	   $profile=$profile[0];
	   
	   return response()->json(['success' => true,"profile"=>$profile]);
	}else{
		return response()->json(['success' => false,"profile"=>array()]);
	}
	}
	
	
	
	 public function postProfileAjaxbycode(Request $request){
		$profile=ProfileModel::where('RateProfileCode', '=', $request->input('id'))->first();
	 
	   if(is_object($profile)){
	   $profile=$profile[0];
	   
	   return response()->json(['success' => true,"profile"=>$profile]);
	}else{
		return response()->json(['success' => false,"profile"=>array()]);
	}
	}
	
	
	
    public function postSaveProfile(Request $request){
	
	$profile = ProfileModel::where('RateProfileCode', '=', $request->input('RateProfileCode'))->count();
	if($profile<=0){
	
	$profile = new ProfileModel;
	
	$Fuel=array();
	$Accessorial=array();
  
	$profile->RateProfileId = 0;
	$profile->clientID=(int)\CustomerRepository::getCurrentCustomerID();
	$profile->RateProfileCode =$request->input('RateProfileCode');
	$profile->RateProfileDescription =$request->input('RateProfileDescription');
	$profile->Active =$request->input('Active');
	$profile->CustomerReferenceCode =$request->input('CustomerReferenceCode');
	$profile->Fuel=$Fuel;
	$profile->Accessorial=$Accessorial;
	
	$profile->save();
	return response()->json(['success' => true,"message"=>"Profile is created successfully"]);
	}elseif($request->input('_id')!=''){
		$data=$request->all();
	    unset($data["_token"]);
		$profile = ProfileModel::where("_id",$data["_id"]);
		unset($data["_id"]);
		$profile->update($data);
	}
	else{
	  return response()->json(['errors' => true,"message"=>"Profile already exists. Please try with another profile code."]); 
	}
	
	
    }
    
    
    
    public function postCopyProfile(Request $request){
		$data=$request->all();
		unset($data["_token"]);
	
		$data=$data['profile'];
		//unset($data["_id"]);
		
	$profileid=$data['RateProfileCode'];
	
	$data=ProfileModel::where('_id', '=', $data["_id"])->first()->toArray();
	$newprofileid=$data['RateProfileCode']=$data['RateProfileCode'].'-copy';
	$data['RateProfileDescription']=$data['RateProfileDescription'].'-copy';
	
	$profile = new ProfileModel($data);
	$profile->save();
	
	/********* copy other related data to profile **********/
	$contract=ContractModel::where('ProfileId', '=', $profileid)->get()->toArray();
	
	foreach($contract as $rec){
		$rec['ProfileId']=$data['RateProfileCode'];
		
		$oldcid=$rec["_id"];
		unset($rec["_id"]);
		$contract = new ContractModel();
		$contract=$contract->create($rec);
		
		$cid=$contract->_id;
		
		$rate = LaneModel::where("ProfileId", $profileid)->where("cId", $oldcid)->get()->toArray();

		foreach($rate as $data){
				$lanerate = new LaneModel();
				$data['cId']=$cid;
				$data['ProfileId']=$newprofileid;
				$lanerate->create($data);
			}
		
	}	
	
	
	return response()->json(['success' => true,"message"=>"Profile is copied successfully"]);
		
	}
    
    public function postProfileList(Request $request){
	 $fields=array("fields"=>explode(",",$request->get('fields')));	
    
    $profiles=$this->ProfileList($request,$fields);
    
	$ratetype=DB::table('m_codereference')->select('codeId as id','codeValue as name')->where('codeType', 'RATETYPE')->get();
	$servicetype=DB::table('m_codereference')->select('codeValue as id','codeValue as name')->where('codeType', 'SERVICETYPE')->get();
	$DistanceApp=DB::table('m_codereference')->select('codeId as id','codeValue as name')->where('codeType', 'DISTANCEAPP')->get();
	
	return response()->json(['success' => true,"profilelist"=>$profiles,"RateTypeOptions"=>$ratetype,"ServiceLevelOptions"=>$servicetype,"DistanceAppOptions"=>$DistanceApp]);
    }
    
    
    
    public function postDeleteProfile(Request $request){
    
	
	ProfileModel::where('RateProfileCode', '=', $request->input('RateProfileCode'))->delete();
	$contract = ContractModel::where('ProfileId', '=', $request->input('RateProfileCode'))->get()->toArray();
	ContractModel::where('ProfileId', '=', $request->input('RateProfileCode'))->delete();
	
	foreach($contract as $ratedel){
	LaneModel::where('cId', '=', $ratedel['_id'])->delete();
		}
	
	$markuplist = MarkupModel::all();
	
	foreach($markuplist as $markup){
		$profiles=$markup['MappedProfile'];
		for($i=0;$i<count($profiles);$i++){
			if(isset($profiles[$i]['ProfileId']) && $profiles[$i]['ProfileId']==$request->input('RateProfileCode')){
				unset($profiles[$i]);
			}
		}
		$rec['MappedProfile']=array_values(array_unique($profiles, SORT_NUMERIC ));
		
		$markup->update($rec);
	}
	
	$profiles=ProfileModel::all();
	return response()->json(['success' => true,"profilelist"=>$profiles]);
    }

   
   
   public function postContractList(Request $request){
	  
	  $fields=array("fields"=>explode(",",$request->get('fields')));	
	  
	  $contract=$this->ContractList($request,$fields);
	 
	 $count = ContractModel::where('ProfileId', '=', $request->input('RateProfileCode'))->count();
	 
	 $profile_fields=array('_id','RateProfileCode','RateProfileDescription','CustomerReferenceCode');
	 
	 $profile = $this->ProfileById($request->input('RateProfileCode'),$profile_fields);
	  
	   return response()->json(['success' => true,"contracts"=>$contract,"count"=>$count,"profile"=>$profile[0]]);
	   
   }
   
   public function postDeleteContract(Request $request){
	 
	ContractModel::where('_id', '=', $request->input('cid'))->delete();
	LaneModel::where('cId', '=', $request->input('cid'))->delete();
	return response()->json(['success' => true]);
   }
   public function postCopyContract(Request $request){
	 
	
	$contract=ContractModel::where('_id', '=', $request->input('cid'))->get()->toArray();
	
	foreach($contract as $rec){
		//$rec['ProfileId']=$data['RateProfileCode'];
		
		$oldcid=$rec["_id"];
		unset($rec["_id"]);
		$contract = new ContractModel();
		$contract=$contract->create($rec);
		
		$cid=$contract->_id;
		
		$rate = LaneModel::where("cId", $oldcid)->get()->toArray();

		foreach($rate as $data){
				$lanerate = new LaneModel();
				$data['cId']=$cid;
				//$data['ProfileId']=$newprofileid;
				$lanerate->create($data);
			}
		
	}
	
	
	return response()->json(['success' => true]);
   }
   
   
   public function postSaveContract(Request $request){
	   $data=$request->all();
	   unset($data["_token"]);
	   
	   //$data['DistanceApp']=$data['DistanceApp']['name'];
	   $data['EquipmentType']=$data['EquipmentType']['name'];
	   //$data['RateType']=$data['RateType']['name'];
	   //$data['RateTypeId']=$data['RateType']['id'];
	   $data['ServiceLevel']=$data['ServiceLevel']['name'];
	   //$data['ServiceLevelId']=$data['ServiceLevel']['id'];
	   $data['clientID']=\CustomerRepository::getCurrentCustomerID();
	   //$data['EffectiveFrom']=strtotime($data['EffectiveFrom']);
	   //$data['EffectiveTo']=strtotime($data['EffectiveTo']);
	   
	  if(!empty($data['_id'])){
		$contract = ContractModel::where("_id",$data["_id"]);
		unset($data["_id"]);
		$contract->update($data);
		return response()->json(['success' => true,"cmessage"=>"Contract is updated successfully"]);
	  }else{
	   $contract = new ContractModel();
	
		$contract->create($data);
		return response()->json(['success' => true,"cmessage"=>"Contract is created successfully"]);
	}
		
   }
   
  
   
   
   public function postContractAjaxbyid(Request $request){
	   
	  
	   
	   $contract=$this->ContractById($request->input('cid'));
	  
	   if(is_array($contract)){
		   
	 
	   $contract['ServiceLevel']=array("id"=>$contract['ServiceLevel'],"name"=>$contract['ServiceLevel']);
	   
	   $contract['EquipmentType']=array("id"=>$contract['EquipmentType'],"name"=>$contract['EquipmentType']);
	   return response()->json(['success' => true,"contract"=>$contract]);
	}else{
		return response()->json(['success' => false,"contract"=>array()]);
	}
   }
   
   
   
   public function postCountry(){
	   
	   $country=Country::with("states")->select("country_name as id","country_name as name")->get();
	  
	   $weighttype=DB::table('m_codereference')->select('codeValue as id','codeValue as name')->where('codeType', 'WEIGHTTYPE')->get();
	   $EqpType=DB::table('m_codereference')->select('codeValue as id','codeValue as name')->where('codeType', 'EQPTYPE')->get();
	   $Loc=DB::table('m_codereference')->select('codeId as id','codeValue as name')->where('codeType', 'RATELOCTYPE')->get();
	   $DistanceType=DB::table('m_codereference')->select('codeValue as id','codeValue as name')->where('codeType', 'DISTANCETYPE')->get();
	   $UnitType=DB::table('m_codereference')->select('codeValue as id','codeValue as name')->where('codeType', 'UNITTYPE')->get();
	   return response()->json(['success' => true,"country"=>$country,"WeightTypeOptions"=>$weighttype,"EqpType"=>$EqpType,"Loc"=>$Loc,"DistanceType"=>$DistanceType,"UnitType"=>$UnitType]);
   }
   
   
   
   
   public function postSaveLane(Request $request){
	    $data=$request->all();
	   unset($data["_token"]);
	   

	   $data['DistanceType']=$data['DistanceType']['name'];
	   $data['CapacityWeightType']=$data['CapacityWeightType']['name'];
	   $data['PackageWeightType']=$data['PackageWeightType']['name'];
	   $data['VolumeWeightType']=$data['VolumeWeightType']['name'];
	   $data['clientID']=\CustomerRepository::getCurrentCustomerID();
	   if(isset($data['CPU']) && is_array($data['CPU']) && count($data['CPU'])>0){
	   //$data['CPU']['UnitType']=$data['CPU']['UnitType']['name'];
		}
		if(isset($data['Rateware']) && is_array($data['Rateware']) && count($data['Rateware'])>0){
			$data=$this->resetarray($data,'Rateware');
		}
		
		elseif( isset($data['DistanceRate']) && is_array($data['DistanceRate']) && count($data['DistanceRate'])>0){
			$data=$this->resetarray($data,'DistanceRate');
			
		}
		
		elseif( isset($data['CPU']) && is_array($data['CPU']) && count($data['CPU'])>0){
			$data=$this->resetarray($data,'CPU');
		}
		
		elseif(isset($data['RateMatrix']) && is_array($data['RateMatrix']) && count($data['RateMatrix'])>0){
			$data=$this->resetarray($data,'RateMatrix');
		}
		elseif(isset($data['CostPerHundredRate']) && is_array($data['CostPerHundredRate'])  && count($data['CostPerHundredRate'])>0){
			$data=$this->resetarray($data,'CostPerHundredRate');
		}
	
		
	  if(!empty($data['_id'])){
		$lanerate = LaneModel::where("_id",$data["_id"]);
		unset($data["_id"]);
		$lanerate->update($data);
	  }else{
	   $lanerate = new LaneModel();
	
		$lanerate->create($data);
	}
		return response()->json(['success' => true,"cmessage"=>"Lane is created successfully"]);
	
   }
   
   
   
   
   private function resetarray($arr,$type){
	   //print_r($arr);
	   foreach($arr as $key=>$set){
		   if($key=='CostPerHundredRate' || $key=='Rateware' || $key=='DistanceRate' || $key=='CPU' || $key=='RateMatrix'){
			   foreach($set as $index=>$val){
				  
				   if($type!=$key){
					    //echo $type;
				   //echo $key;
				   //echo '-----------------';
					   $arr[$key][$index]='';
				   }
			   }
		   }
	   
	   }
	  // print_r($arr);
	   return $arr;
   }
   
   
   
   
   public function postRatelaneList(Request $request){
	   
	 $fields=array("fields"=>explode(",",$request->get('fields')));  
	 $lanerate=$this->RatelaneList($request,$fields);
	 $count = LaneModel::where('ProfileId', '=', $request->input('ProfileId'))->where('cId', '=', $request->input('carrierId'))->where('clientID',\CustomerRepository::getCurrentCustomerID())->count();
	  $limit = $request->get('limit', 1);

	 
	  
	   return response()->json(['success' => true,"ratelist"=>$lanerate,"count"=>round($count/$limit)]);
   }

   
   public function postDeleteRatelane(Request $request){
	 
	   LaneModel::where('_id', '=', $request->input('cid'))->delete();
	
	return response()->json(['success' => true]);
   }
   
   public function postLanerateAjaxbyid(Request $request){
	   $lanerate=LaneModel::where('_id', '=', $request->input('cid'))->get()->toArray();
	   $lanerate=$lanerate[0];
	   
	  
	   $lanerate['DistanceType']=array("id"=>$lanerate['DistanceType'],"name"=>$lanerate['DistanceType']);;
	   $lanerate['CapacityWeightType']=array("id"=>$lanerate['CapacityWeightType'],"name"=>$lanerate['CapacityWeightType']);;
	   $lanerate['PackageWeightType']=array("id"=>$lanerate['PackageWeightType'],"name"=>$lanerate['PackageWeightType']);;
	   $lanerate['VolumeWeightType']=array("id"=>$lanerate['VolumeWeightType'],"name"=>$lanerate['VolumeWeightType']);
	
	   if(isset($lanerate['CPU']) && is_array($lanerate['CPU']) && count($lanerate['CPU'])>0){
	
	  // $lanerate['CPU']['UnitType']=array("id"=>$lanerate['CPU']['UnitType'],"name"=>$lanerate['CPU']['UnitType']);;
		}	
	   
	   //$contract['DistanceType']=array("id"=>$contract['DistanceType'],"name"=>$contract['DistanceType']);
	   return response()->json(['success' => true,"lanerate"=>$lanerate]);
   }
   
  
  
  
  
  
  
  
   
   public function postContractAssocarial(Request $request){
	   
	   $pageSize = $request->get('limit', 1);
		$pageNumber = $request->get('page', 1);
     $filters = $request->get('filters');
	   $profile=ProfileModel::where('RateProfileCode', '=', $request->input('profileid'))->first();
	   $assoc=array();
	   $sasc_assoc=array();
	   
	  
	   $effectivefrom=isset($filters[0]['EffectiveFrom'])?$filters[0]['EffectiveFrom']:'';
	   $effectiveto=isset($filters[0]['EffectiveTo'])?$filters[0]['EffectiveTo']:'';
	   $searchAssoc=isset($filters[0]['searchAssoc'])?$filters[0]['searchAssoc']:'';
		
	   foreach($profile['Accessorial'] as $rec){
			if(empty($rec['scac']) && $rec['cid']!=''){
				
				if(!empty($effectivefrom) || !empty($effectiveto) || !empty($searchAssoc)){
					if(empty($searchAssoc) && !empty($effectivefrom) && empty($effectiveto) && $rec['effective_from']>=$effectivefrom){
						$assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && empty($effectivefrom) && !empty($effectiveto) && $rec['effective_to']<=$effectiveto){
						$assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto){
						$assoc[]=$rec;
					}
					elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}
				}else{
				
				$assoc[]=$rec;
				}
			}
			
			
		}
		
		foreach($profile['Accessorial'] as $rec){
			/*if(!empty($rec['scac']) && $rec['cid']!=''){
				$sasc_assoc[]=$rec;
			}*/
			
			if(!empty($rec['scac']) && $rec['cid']!=''){
				
				if(!empty($effectivefrom) || !empty($effectiveto) || !empty($searchAssoc)){
					if(empty($searchAssoc) && !empty($effectivefrom) && empty($effectiveto) && $rec['effective_from']>=$effectivefrom){
						$sasc_assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && empty($effectivefrom) && !empty($effectiveto) && $rec['effective_to']<=$effectiveto){
						$sasc_assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto){
						$sasc_assoc[]=$rec;
					}
					elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && $rec['accessorial_code']==$searchAssoc){
						$sasc_assoc[]=$rec;
					}
				}else{
				
				$sasc_assoc[]=$rec;
				}
			}
			
			
		}
		
		$start = --$pageNumber * $pageSize;
		
		
		
		$data=array("Accessorial"=>array_slice($assoc, $start, $pageSize),"SCAC_Assoc"=>array_slice($sasc_assoc, $start, $pageSize),"assoc_count"=>round(count($assoc)/$pageSize),"scac_count"=>round(count($sasc_assoc)/$pageSize));
		
		
	   return response()->json($data);
   }
   
   public function postSaveAssoc(Request $request){
	   $data=$request->all();
	    unset($data["_token"]);
	    unset($data['Accessorial']['_token']);
		unset($data['Accessorial']['id']);
	    $assoc=array();
		$profile = ProfileModel::where("RateProfileCode",$data["cid"])->first();
		
		$i=0;
		foreach($profile['Accessorial'] as $rec){
			if(isset($data['Accessorial']['cid']) && $rec['cid']==$data['Accessorial']['cid'] && $data['Accessorial']['cid']!=''){
				$assoc[]=$data['Accessorial'];
			}
			
			else{
				$assoc[]=$rec;
			}
			$i++;
		}
		if(!isset($data['Accessorial']['cid'])){
			
		
			$data['Accessorial']['cid']=$i+1;
			$assoc[]=$data['Accessorial'];
			
		}
		
		
		$data=array("Accessorial"=>$assoc);
		
		$profile->update($data);
	return response()->json(['success' => true]);
   }
   
   
   
    public function postDeleteAssoc(Request $request){
	   $data=$request->all();
	   $assoc=array();
		$profile = ProfileModel::where("RateProfileCode",$data["cid"])->first();
		
		
		$i=0;
		foreach($profile['Accessorial'] as $rec){
			if(isset($data['Accessorial']['cid']) && $rec['cid']==$data['Accessorial']['cid'] && $data['Accessorial']['cid']!=''){
				
			}elseif($rec['cid']!=$data['Accessorial']['cid']){
				$assoc[]=$rec;
			}
			$i++;
		}
		
		$data=array("Accessorial"=>$assoc);
		
		$profile->update($data);
	return response()->json(['success' => true]);
		
	   
   }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    
   public function postContractFuel(Request $request){
	   
	   $pageSize = $request->get('limit', 1);
		$pageNumber = $request->get('page', 1);
     $filters = $request->get('filters');
	   $profile=ProfileModel::where('RateProfileCode', '=', $request->input('profileid'))->first();
	   $assoc=array();
	   $sasc_assoc=array();
	   
	  
	   $effectivefrom=isset($filters[0]['EffectiveFrom'])?$filters[0]['EffectiveFrom']:'';
	   $effectiveto=isset($filters[0]['EffectiveTo'])?$filters[0]['EffectiveTo']:'';
	   $searchAssoc=isset($filters[0]['searchFuel'])?$filters[0]['searchFuel']:'';
		
	   foreach($profile['Fuel'] as $rec){
			if(empty($rec['scac']) && $rec['cid']!=''){
				
				if(!empty($effectivefrom) || !empty($effectiveto) || !empty($searchAssoc)){
					if(empty($searchAssoc) && !empty($effectivefrom) && empty($effectiveto) && $rec['effective_from']>=$effectivefrom){
						$assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && empty($effectivefrom) && !empty($effectiveto) && $rec['effective_to']<=$effectiveto){
						$assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto){
						$assoc[]=$rec;
					}
					elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}
				}else{
				
				$assoc[]=$rec;
				}
			}
			
			
		}
		
		foreach($profile['Fuel'] as $rec){
			/*if(!empty($rec['scac']) && $rec['cid']!=''){
				$sasc_assoc[]=$rec;
			}*/
			
			if(!empty($rec['scac']) && $rec['cid']!=''){
				
				if(!empty($effectivefrom) || !empty($effectiveto) || !empty($searchAssoc)){
					if(empty($searchAssoc) && !empty($effectivefrom) && empty($effectiveto) && $rec['effective_from']>=$effectivefrom){
						$sasc_assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && empty($effectivefrom) && !empty($effectiveto) && $rec['effective_to']<=$effectiveto){
						$sasc_assoc[]=$rec;
					}
					elseif(empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto){
						$sasc_assoc[]=$rec;
					}
					elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && !empty($effectivefrom) && !empty($effectiveto) &&  $rec['effective_from']>=$effectivefrom && $rec['effective_to']<=$effectiveto && $rec['accessorial_code']==$searchAssoc){
						$assoc[]=$rec;
					}elseif(isset($rec['accessorial_code'])&& !empty($searchAssoc) && $rec['accessorial_code']==$searchAssoc){
						$sasc_assoc[]=$rec;
					}
				}else{
				
				$sasc_assoc[]=$rec;
				}
			}
			
			
		}
		
		$start = --$pageNumber * $pageSize;
		
		
		
		$data=array("Fuel"=>array_slice($assoc, $start, $pageSize),"SCAC_Fuel"=>array_slice($sasc_assoc, $start, $pageSize),"fuel_count"=>round(count($assoc)/$pageSize),"scac_count"=>round(count($sasc_assoc)/$pageSize));
		
		
	   return response()->json($data);
   }
   
   public function postSaveFuel(Request $request){
	   $data=$request->all();
	    unset($data["_token"]);
	    unset($data['Fuel']['_token']);
		unset($data['Fuel']['id']);
	    $assoc=array();
		$profile = ProfileModel::where("RateProfileCode",$data["cid"])->first();
		
		$i=0;
		foreach($profile['Fuel'] as $rec){
			if(isset($data['Fuel']['cid']) && $rec['cid']==$data['Fuel']['cid'] && $data['Fuel']['cid']!=''){
				$assoc[]=$data['Fuel'];
			}
			
			else{
				$assoc[]=$rec;
			}
			$i++;
		}
		if(!isset($data['Fuel']['cid'])){
			
		
			$data['Fuel']['cid']=$i+1;
			$assoc[]=$data['Fuel'];
			
		}
		
		
		$data=array("Fuel"=>$assoc);
		
		$profile->update($data);
	return response()->json(['success' => true]);
   }
   
   
   
    public function postDeleteFuel(Request $request){
	   $data=$request->all();
	   $assoc=array();
		$profile = ProfileModel::where("RateProfileCode",$data["cid"])->first();
		
		
		$i=0;
		foreach($profile['Fuel'] as $rec){
			if(isset($data['Fuel']['cid']) && $rec['cid']==$data['Fuel']['cid'] && $data['Fuel']['cid']!=''){
				
			}elseif($rec['cid']!=$data['Fuel']['cid']){
				$assoc[]=$rec;
			}
			$i++;
		}
		
		$data=array("Fuel"=>$assoc);
		
		$profile->update($data);
	return response()->json(['success' => true]);
		
	   
   }
   

   
   
   public function postMarkupList(Request $request){
	  //$code=$request->get("profile");
	   $name=$request->get("markup");
	   
	   $clientID=(int)\CustomerRepository::getCurrentCustomerID();
	   
	   
	   if($name!=''){
		$profile=ProfileModel::where('RateProfileDescription', 'LIKE', "{$name}%")->first();
		}
	    $markup=array();
	    
	   if(isset($profile)){
	   $markup = MarkupModel::where("UpChargeCode",$profile->RateProfileCode)->where("clientID",$clientID)->where("Active",'true');
	
		
		}else{
		
		$markup = MarkupModel::where("clientID",$clientID)->where("Active",'true');
		
		}
		
	 $markup=$markup->get();
	   return response()->json(['success' => true,"markuplist"=>$markup]);
   }
   
   public function postNewmarkupList(Request $request){
	   
	    $markup=array();
	  $clientID=(int)\CustomerRepository::getCurrentCustomerID();
	   //$markup = MarkupModel::where("clientID",$clientID)->where("Active",'true')->where("MappedProfile",'size',0);
	$markup = MarkupModel::where("clientID",$clientID)->where("Active",'true');
		
		
	 $markup=$markup->get();
	   return response()->json(['success' => true,"markuplist"=>$markup]);
   }
   
   
   
   
   public function postSaveMarkup(Request $request){
	   $data=$request->all();
	    $rec=array();
	    
		$markup = new MarkupModel();
		$rec['clientID']=(int)\CustomerRepository::getCurrentCustomerID();
		$rec['UpChargeCode']=$data['profile'];
		$rec['UpChargeDescription']=$data['Markup']['MarkupName'];
		$rec['Active']='true';
		$rec['TopLevelUpCharge']=array("ChargePercent"=>"","MinCharge"=>"","TopChargeType"=>"");
		$rec['UpchargeByCarrier']=array();
		$rec['MappedProfile']=array();
		$markup->create($rec);
	    
	return response()->json(['success' => true]);
   }
   
   
   public function postUpdateMarkup(Request $request){
	   $data=$request->all();
	    $rec=array();
	    $data=$data['Markup'];
	
		$markup = MarkupModel::where("_id",$data["_id"])->first();
		
		
		$rec['TopLevelUpCharge']=$data['TopLevelUpCharge'];
		$rec['UpchargeByCarrier']=$data['UpchargeByCarrier'];
	if(isset($data["ProfileId"])){
		$rec['MappedProfile'][]=array("ProfileId"=>$data["ProfileId"]);
		$rec['MappedProfile']=array_unique($rec['MappedProfile']);
	}
		$markup->update($rec);
	    
	return response()->json(['success' => true]);
   }
   
   
   
   public function postDeleteMarkup(Request $request){
	   $data=$request->all();
	  
		MarkupModel::where("_id",$data["_id"])->delete();
		
	return response()->json(['success' => true]);
		
	   
   }
   
   public function postCarrierMarkup(Request $request){
	    $fields=array("fields"=>explode(",",$request->get('fields')));	
	  
	  $contract=$this->ContractCarrierList($request,$fields);
	  $res=array();
	  if(isset($contract)){
	  foreach($contract as $rec){
			$res[]=array("CarrierScac"=>$rec['carriercode'],"CarrierName"=>$rec['carrierId']);
		  }
	  }
	  $result=array_unique($res, SORT_REGULAR);
	  $profile = ProfileModel::where("RateProfileCode",$request->input('profile'))->first();
	  
	  
	  return response()->json(array("carrier"=>$result,"profile"=>$profile));
   }
   
   public function postAddprofileMarkup(Request $request){
	   $data=$request->all();
	  
		$markup = MarkupModel::where("_id",$data["_id"])->first();
		$profiles=$markup['MappedProfile'];
		$profiles[]=array("ProfileId"=>$data["ProfileId"]);
		$rec['MappedProfile']=array_unique($profiles, SORT_REGULAR);
		
		$markup->update($rec);
		
	return response()->json($rec['MappedProfile']);
		
	   
   }
   
   
   
   public function postExcludeprofileMarkup(Request $request){
	   $data=$request->all();
	  
		$markup = MarkupModel::where("_id",$data["_id"])->first();
		$profiles=$markup['MappedProfile'];
		for($i=0;$i<count($profiles);$i++){
			if(isset($profiles[$i]['ProfileId']) && $profiles[$i]['ProfileId']==$data["ProfileId"]){
				unset($profiles[$i]);
			}
		}
		$rec['MappedProfile']=array_values(array_unique($profiles, SORT_NUMERIC ));
		
		$markup->update($rec);
		
	return response()->json($rec['MappedProfile']);
		
	   
   }
   
   public function postUpdateprofileMarkup(Request $request){
	   $data=$request->all();
	  
	   $upcharge=$data['profile']['UpchargeByCarrier'];
	   $topleveupcharge=$data['profile']['TopLevelUpCharge'];
	   
	   $data['Markup']['TopLevelUpCharge']=array("ChargePercent"=>$topleveupcharge['profileChargePercent'],"MinCharge"=>$topleveupcharge['profilecarrierMinCharge'],"TopChargeType"=>$topleveupcharge['profileTopChargeType']);
	   
	   
	   foreach($upcharge as $cdata){
	   $data['Markup']['UpchargeByCarrier'][]=array("CarrierName"=>$cdata['CarrierName'],"CarrierScac"=>$cdata['CarrierScac'],"ChargePercent"=>$cdata['ChargePercent'],"carrierMinCharge"=>$cdata['carrierMinCharge'],"ChargeType"=>$cdata['ChargeType']);
		}
	   unset($data['profile']);
	   
	   
	   $profile = ProfileModel::where("RateProfileCode",$data["profileId"])->first();
		unset($data["profileId"]);
		$profile->update($data);
		
		return response()->json($profile);
   }
   
   public function postDelprofileMarkup(Request $request){
	    $data=$request->all();
	   $profile = ProfileModel::where("RateProfileCode",$data["profileId"])->first();
		unset($data["profileId"]);
		$data['Markup']=array();
		$profile->update($data);
		
		return response()->json($profile);
   }
   
   public function postSaveMatrix(Request $request){
	   $data=$request->all();
	   unset($data["_token"]);
	   
	   $matrix = new RatematrixModel();
		$matrixcreated=$matrix->create($data);
		
		
		 $matrix=RatematrixModel::get();
	   $MatrixOptions=array();
	   $MatrixOptions[]=array("id"=>"","name"=>"Please Select");
	   foreach($matrix as $data){
		   $MatrixOptions[]=array("id"=>$data['_id'],"name"=>$data['name']);
		   
	   }
	   
	   $selected=array("id"=>$matrixcreated->_id,"name"=>$matrixcreated->name);
	   
	   //upload grid for new created matrix
	   if($data['nomatrix']){
		   
		   	$existsmatrix=RatematrixModel::where("_id",$matrixcreated->_id)->first()->toArray();
			   
			   //adjust column name
			   $col1=preg_replace("/[^a-zA-Z0-9]+/", "", $existsmatrix['OriginLocation']['name']);
			   $col2=preg_replace("/[^a-zA-Z0-9]+/", "", $existsmatrix['DestinationLocation']['name']);
				$head=array($col1,$col2);
				foreach($existsmatrix['matrix'] as $colhead){
						   $head[]=preg_replace("/[^a-zA-Z0-9]+/", "", $colhead['name']);
						 
					   }
					   
				$result=$data['mfile']	;	   
				$upTmpName = $result['path'].$result['fileName'];
				$row = 0;
		 
			 
			if (($handle = fopen($upTmpName, "r")) !== FALSE) {
			 
				while (($rowdata = fgetcsv($handle, 0, ",")) !== FALSE) {
			 
					if($row == 0){
												
						$diff=array_diff($rowdata,$head);
						
						if(count($diff)>0)
						break;
						 
						$row++; 
					} else {
			 
						
						if(!empty($rowdata[0]) && !empty($rowdata[1])) 
						{
							
						   $rec['InterstateOption']='All';
						   $rec['MoveType']='All';
						   $rec['IgnoreCarrierConnect']='None';
						   $rec['UseRoutingGuide']='None';
						   
						   $rec['cId']=$data['cId'];
						   $rec['gridId']=$matrixcreated->_id;
			   
							
							
							for($i=0;$i<count($head);$i++){
								$rec[$head[$i]]= $rowdata[$i];
							}
							
							$matrix = new MatrixRatelaneModel();
							$matrix=$matrix->create($rec);
						}
						
			 
					}
			 
				}
			 @unlink($upTmpName);
			}
			
	   }
	   
	   
		return response()->json(['success' => true,"moptions"=>$MatrixOptions,"selected"=>$selected]);
	   
   }
   
   public function postFetchMatrix(Request $request){
	   $req=$request->all();
	   unset($req["_token"]);
	   
	   $matrix=RatematrixModel::get();
	   $MatrixOptions=array();
	   $MatrixOptions[]=array("id"=>"","name"=>"Please Select");
	   foreach($matrix as $data){
		   $MatrixOptions[]=array("id"=>$data['_id'],"name"=>$data['name']);
		   
	   }
	   $colheading=DB::table('m_codereference')->select('codeId as id','codeValue as name')->where('codeType', 'MATRIXTYPE')->get();
	  
	   $existsmatrix=array();
	   if(!empty($req['matrixform']['matrix']['id'])){
		   $existsmatrix=RatematrixModel::where("_id",$req['matrixform']['matrix']['id'])->first()->toArray();
		 
	   }
	   $grid=array();
	   $ratematrix=array();
	   if(isset($req['cid']) && !empty($req['cid'])){
		   $matrixdata=MatrixRatelaneModel::where("cId",$req['cid'])->get();
		   
		   
		   
		   foreach($matrixdata as $mdata){
			   //seperate grid and matrix data
			   $ratematrix['InterstateOption']=$mdata['InterstateOption'];
			   $ratematrix['MoveType']=$mdata['MoveType'];
			   $ratematrix['IgnoreCarrierConnect']=$mdata['IgnoreCarrierConnect'];
			   $ratematrix['UseRoutingGuide']=$mdata['UseRoutingGuide'];
			   $ratematrix['OriginCountry']=$mdata['OriginCountry'];
			   $ratematrix['DestinationCountry']=$mdata['DestinationCountry'];
			   
			   $ratematrix['OriginCountryCode']=$mdata['OriginCountryCode'];
			   $ratematrix['DestinationCountryCode']=$mdata['DestinationCountryCode'];
			   
			   
			   $ratematrix['cId']=$mdata['cId'];
			   $ratematrix['gridId']=$mdata['gridId'];
			   
			   $existsmatrix=RatematrixModel::where("_id",$mdata['gridId'])->first()->toArray();
			   
			   //adjust column name
			   $col1=$existsmatrix['OriginLocation']['orggrid'];
			   $col2=$existsmatrix['DestinationLocation']['destgrid'];
			   
			   $griditem=array($col1=>$mdata[$col1],$col2=>$mdata[$col2]);
			   foreach($existsmatrix['matrix'] as $colhead){
				   $col=preg_replace("/[^a-zA-Z0-9]+/", "", $colhead['name']);
				   
				   $griditem[$col]=$mdata[$col];
				   $griditem['_id']=$mdata['_id'];
			   }
			   $grid[]=$griditem;
		   }
		   
	   }
	   return response()->json(["matrixoptions"=>$MatrixOptions,"headingoptions"=>$colheading,"existingmatrix"=>$existsmatrix,"griddata"=>$grid,"matrixdata"=>$ratematrix]);
	   
   }
   
   
   
   
   //save matrix grid data and form data
   public function postSaveLanematrix(Request $request){
	   $req=$request->all();
	   unset($req['matrixlane']["_token"]);
	   
	   $lanematrix=MatrixRatelaneModel::where("cId",$req['matrixlane']['cId'])->get();
	   $count=$lanematrix->count();
	   if($count>0 ){
		   $i=0;
		   foreach($lanematrix as $lanedata){
			   $lanecollection=MatrixRatelaneModel::where("_id",$lanedata['_id'])->first();
			   unset($req['grid'][$i]['_id']);
			   $rdata=$req['grid'][$i];
			   $rdata +=$req['matrixlane'];
			   
			    if(isset($rdata['OriginPostalRange']) && !empty(($rdata['OriginPostalRange']))){
					   $zip=explode("-",$rdata['OriginPostalRange']);
					   $rdata['OriginZipcodeFrom']=trim($zip[0]);
					   $rdata['OriginZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($rdata['DestinationPostalRange']) && !empty(($rdata['DestinationPostalRange']))){
					   $zip=explode("-",$rdata['DestinationPostalRange']);
					   $rdata['DestinationZipcodeFrom']=trim($zip[0]);
					   $rdata['DestinationZipcodeTo']=trim($zip[1]);
				   }
				   
				    if(isset($rdata['OriginPostalCode3Range']) && !empty(($rdata['OriginPostalCode3Range']))){
					   $zip=explode("-",$rdata['OriginPostalCode3Range']);
					   $rdata['OriginTripleZipcodeFrom']=trim($zip[0]);
					   $rdata['OriginTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($rdata['DestinationPostalCode3Range']) && !empty(($rdata['DestinationPostalCode3Range']))){
					   $zip=explode("-",$rdata['DestinationPostalCode3Range']);
					   $rdata['DestinationTripleZipcodeFrom']=trim($zip[0]);
					   $rdata['DestinationTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				    
				   if(isset($rdata['OriginStateCity']) && !empty(($rdata['OriginStateCity']))){
					   $loc=explode(",",$rdata['OriginStateCity']);
					   $rdata['OriginState']=trim($loc[0]);
					   $rdata['OriginCity']=trim($loc[1]);
				   }
				   
				   if(isset($rdata['DestinationStateCity']) && !empty(($rdata['DestinationStateCity']))){
					   $loc=explode(",",$data['DestinationStateCity']);
					   $rdata['DestinationState']=trim($loc[0]);
					   $rdata['DestinationCity']=trim($loc[1]);
				   }
				   
			   
			   $lanecollection->update($rdata);
			   $i++;
		   }
		   if( count($req['grid']) > $count ){
			   $matrix = new MatrixRatelaneModel();
			   
			   for(;$i<count($req['grid']);$i++){
				   $data=$req['grid'][$i];
				   
				    if(isset($data['OriginPostalRange']) && !empty(($data['OriginPostalRange']))){
					   $zip=explode("-",$data['OriginPostalRange']);
					   $data['OriginZipcodeFrom']=trim($zip[0]);
					   $data['OriginZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($data['DestinationPostalRange']) && !empty(($data['DestinationPostalRange']))){
					   $zip=explode("-",$data['DestinationPostalRange']);
					   $data['DestinationZipcodeFrom']=trim($zip[0]);
					   $data['DestinationZipcodeTo']=trim($zip[1]);
				   }
				   
				    if(isset($data['OriginPostalCode3Range']) && !empty(($data['OriginPostalCode3Range']))){
					   $zip=explode("-",$data['OriginPostalCode3Range']);
					   $data['OriginTripleZipcodeFrom']=trim($zip[0]);
					   $data['OriginTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($data['DestinationPostalCode3Range']) && !empty(($data['DestinationPostalCode3Range']))){
					   $zip=explode("-",$data['DestinationPostalCode3Range']);
					   $data['DestinationTripleZipcodeFrom']=trim($zip[0]);
					   $data['DestinationTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				    
				   if(isset($data['OriginStateCity']) && !empty(($data['OriginStateCity']))){
					   $loc=explode(",",$data['OriginStateCity']);
					   $data['OriginState']=trim($loc[0]);
					   $data['OriginCity']=trim($loc[1]);
				   }
				   
				   if(isset($data['DestinationStateCity']) && !empty(($data['DestinationStateCity']))){
					   $loc=explode(",",$data['DestinationStateCity']);
					   $data['DestinationState']=trim($loc[0]);
					   $data['DestinationCity']=trim($loc[1]);
				   }


				   $data +=$req['matrixlane'];
				   
				   
				   $matrix=$matrix->create($data);
			   }
		   }
		   
	   }else{
		   
			   $matrix = new MatrixRatelaneModel();
			   
			   
			  
			   foreach($req['grid'] as $data){
				   if(isset($data['OriginPostalRange']) && !empty(($data['OriginPostalRange']))){
					   $zip=explode("-",$data['OriginPostalRange']);
					   $data['OriginZipcodeFrom']=trim($zip[0]);
					   $data['OriginZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($data['DestinationPostalRange']) && !empty(($data['DestinationPostalRange']))){
					   $zip=explode("-",$data['DestinationPostalRange']);
					   $data['DestinationZipcodeFrom']=trim($zip[0]);
					   $data['DestinationZipcodeTo']=trim($zip[1]);
				   }
				   
				   
				   
				   if(isset($data['OriginPostalCode3Range']) && !empty(($data['OriginPostalCode3Range']))){
					   $zip=explode("-",$data['OriginPostalCode3Range']);
					   $data['OriginTripleZipcodeFrom']=trim($zip[0]);
					   $data['OriginTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				   if(isset($data['DestinationPostalCode3Range']) && !empty(($data['DestinationPostalCode3Range']))){
					   $zip=explode("-",$data['DestinationPostalCode3Range']);
					   $data['DestinationTripleZipcodeFrom']=trim($zip[0]);
					   $data['DestinationTripleZipcodeTo']=trim($zip[1]);
				   }
				   
				   
				   
				   if(isset($data['OriginStateCity']) && !empty(($data['OriginStateCity']))){
					   $loc=explode(",",$data['OriginStateCity']);
					   $data['OriginState']=trim($loc[0]);
					   $data['OriginCity']=trim($loc[1]);
				   }
				   
				   if(isset($data['DestinationStateCity']) && !empty(($data['DestinationStateCity']))){
					   $loc=explode(",",$data['DestinationStateCity']);
					   $data['DestinationState']=trim($loc[0]);
					   $data['DestinationCity']=trim($loc[1]);
				   }
				   
				   
				   $data +=$req['matrixlane'];
				   
				   $matrix=$matrix->create($data);
				   
			   }
			   
		   }
	   
		
		return response()->json(['success' => true]);
	   
   }
   
   
   
   //delete matrix data
   public function postDeleteLanematrix(Request $request){
	   $req=$request->all();
	   $lanematrix=MatrixRatelaneModel::where("cId",$req['cId'])->where("_id",$req['_id'])->delete();
	  
	   return response()->json(['success' => true]);
   }
   
   
   
   
   //delete matrix head
	public function postDeleteMatrix(Request $request){
	   $req=$request->all();
	  
	  $lanedata=MatrixRatelaneModel::where("gridId",$req['_id'])->count();
	  
	  if($lanedata<=0){
	  
	   $lanematrix=RatematrixModel::where("_id",$req['_id'])->delete();
	   
	   $matrix=RatematrixModel::get();
	   $MatrixOptions=array();
	   $MatrixOptions[]=array("id"=>"","name"=>"Please Select");
	   foreach($matrix as $data){
		   $MatrixOptions[]=array("id"=>$data['_id'],"name"=>$data['name'] );
		   
	   }
	   return response()->json(['success' => true,"moptions"=>$MatrixOptions]);
	}else{
		
		return response()->json(['success' => false]);
	}
	  
	   
   }
   
   public function postUploadMatrix(Request $request){
	   $req=$request->all();
	  $lanedata=MatrixRatelaneModel::where("cId",$req['cid']);
	  $result = $this->uploadDoc($request, 'contract_doc', null, null, false);
	  
	  if($lanedata->count()>0){
		  
		$fmdata=$lanedata->first()->toArray();
		
		$existsmatrix=RatematrixModel::where("_id",$fmdata['gridId'])->first()->toArray();
			   
			   //adjust column name
			   $col1=$existsmatrix['OriginLocation']['orggrid'];
			   $col2=$existsmatrix['DestinationLocation']['destgrid'];
				$head=array($col1,$col2);
				foreach($existsmatrix['matrix'] as $colhead){
						   $head[]=preg_replace("/[^a-zA-Z0-9]+/", "", $colhead['name']);
						 
					   }
		 
		 $upTmpName = $result['path'].$result['fileName'];
		 $row = 0;
		 
			 
			if (($handle = fopen($upTmpName, "r")) !== FALSE) {
			 
				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
			 
					if($row == 0){
						//print_r($data);
						//print_r($head);
						
						$result=array_diff($data,$head);
						
						if(count($result)>0)
						return response()->json(['success' => false,"msg"=>"Columns are not matching.","nomatrix"=>false]); 
						 
						$row++; 
					} else {
			 
						// $data[0] = first name; $data[1] = last name; $data[2] = email; $data[3] = phone
						/*********************************************************************************************************************/
						if(!empty($data[0]) && !empty($data[1])) 
						{
							//echo $data[0].' - '.$data[1].' - '.$data[2].' - '.$data[3].'<br/>';
							$rec=$fmdata;
							for($i=0;$i<count($head);$i++){
								$rec[$head[$i]]= $data[$i];
							}
							unset($rec['_id']);unset($rec['updated_at']);unset($rec['created_at']);
							$matrix = new MatrixRatelaneModel();
							$matrix=$matrix->create($rec);
						}
						
			 
					}
			 
				}
			 @unlink($upTmpName);
			} else {
			 return response()->json(['success' => false,'msg'=>'File could not be opened.']); 
				
			}	


		
		
		return response()->json(['success' => true,"msg"=>'Grid uploaded successfully']);
	  }else{
		  
		return response()->json(['success' => false,"nomatrix"=>true,"result"=>$result]);  
	  }
	   
	   
   }   

}
