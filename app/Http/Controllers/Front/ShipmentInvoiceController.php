<?php

namespace App\Http\Controllers\Front;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\ShipmentInvoiceActionsTrait;
use App\ClientItem;
use App\Layout;
use App\ShipHeader;

class ShipmentInvoiceController extends Controller {
	const UPLOAD_DIRECTORY = 'uploads',
		BRAND_IMAGE_DIRECTORY = 'brands',
		CARRIER_IMAGE_DIRECTORY = 'carriers',
		DOCMANAGER_DIRECTORY = 'docmanager',
		SHIP_FILE_DIRECTORY = 'shipfiles';
		
    use UtilityTrait, ShipmentInvoiceActionsTrait;

	public function __construct()
	{

	}	
	
	
    /*
     * Method to generate shipment list for a given user
     * @access public
     * @
     */	
    public function getList(){
		//$pdf = \App::make('dompdf.wrapper');
		//$pdf->loadHTML('<h1>Test</h1>');
		//return $pdf->stream();
		//return $pdf->loadFile(DOMPDF_CHROOT.'/public/vics-stand/vics-stand.htm')->save(DOMPDF_CHROOT.'/public/pdf/my_stored_file.pdf')->stream('download.pdf');
		//$pdf->loadFile(DOMPDF_CHROOT.'/public/vics_stand/1.html')->save(DOMPDF_CHROOT.'/public/pdf/my_stored_file.pdf');
	
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.dispatchSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.dispatchSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		
      /* $fields1 = array();
	    foreach($allFields as $item){
		 $fields1[] = $item->displayField;
		}
       echo '<pre>'; print_r($fields1); exit;*/
	
	 return view('shipments.list')->with(['allFields'=>$allFields]);
	}
	
    public function getInvoice(){	
	
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentInvoiceSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentInvoiceSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		
	 return view('shipments.invoice')->with(['allFields'=>$allFields]);
	}	
	
    public function getInv(){	
	
	 return view('shipments.inv');
	}		
	
    /*
     * Method to generate post shipment for a given user
     * @access public
     * @
     */	
    public function addShipment(){
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		//dd($allFields);
	 return view('shipments.edit')->with(['allFields'=>$allFields, 'ship_id' => '']);		 
	 //return view('shipments.add')->with(['allFields'=>$allFields]);
	}	
	
    public function getEdit($id) {
        if (ShipHeader::where('id', $id)->count()){
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		//dd($allFields);
			 
	    return view('shipments.edit')->with(['allFields'=>$allFields, 'ship_id' => $id]);
		}
        else{
            abort(404, 'Shipdata not found');
			}
    }	
	
    /*
     * Method to post shipment information for a given customer
     * @returns json
     * @access public
     * @
     */

    public function postAjaxSubShipmentEdit(Request $request) {
        $shipID = $request->get('shipID'); 
        return $this->handleShipmentPostData($request, $shipID);		  
    }	
	
    public function getAjaxSubShipmentEdit(Request $request) {
        $shipID = $request->get('shipID');
		
		if($request->get('page') == 'list'){
			return $this->getShipmentDetailsForList($request, $shipID, array('clientId', 
			'LoadId', 
			'shp_modeId',  
			'shp_shipnumber',
			'shp_pickupname',
			'shp_scac',
			'shp_pronumber',
			'shp_pickuptimefrom',
			'shp_deliverytimefrom',
			'shp_pickuptimeto',
			'shp_deliverytimeto',
			'shp_expdeliverydate',
			'shp_reqpickupdate',
			'shp_sonumber',
			'shp_quotenumber',
			'shp_shipmentvalue',
			'shp_valueperpound',
			'shp_billweight',
			'shp_pickupcity',
			'shp_pickupstate',
			'shp_pickuppostal',
			'shp_pickupcountry',
			'shp_pickupadr1',
			'shp_pickupadr2',			
			'shp_deliveryname',
			'shp_deliveryadr1',
			'shp_deliveryadr2',
			'shp_deliverycity',
			'shp_deliverystate',
			'shp_deliverypostal',
			'shp_deliverycountry',
            'shp_pickupphone',
            'shp_pickupcontact',
            'shp_deliveryphone',
            'shp_deliverycontact',
            'shp_distance',			
			));
		} else {
		 return $this->getShipmentDetails($request, $shipID, array());
		}
    }

    public function postAjaxPartialShipmentEdit(Request $request, Guard $auth) {
        $shipID = $request->get('shipID'); 
        return $this->handlePartialShipmentPostData($request, $shipID, $auth);		  
    }		
	
    /*
     * @returns json
     *
     */

    public function getAjaxShipmentLists(Request $request, Guard $auth) {
	
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentInvoiceSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['displayField']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentInvoiceSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['displayField']);
        }		
	
      
	    $fields1 = array();
		$pickup_location_array = array();
		$destination_location_array = array();
		$ship_status_array = array();
		
	    foreach($allFields as $item){
		 $fields1[] = $item->displayField;
		}
		
		if(in_array('shp_pickupstate', $fields1)){
		 $pickup_location_array[] = 'shp_pickupname';
		 $pickup_location_array[] = 'shp_pickupcity';
		 $pickup_location_array[] = 'shp_pickupstate';
		 $pickup_location_array[] = 'shp_pickuppostal';
		 $pickup_location_array[] = 'shp_pickupcountry';
		 $pickup_location_array[] = 'shp_pickupcontact';
		 $pickup_location_array[] = 'shp_pickupphone';
		}
		if(in_array('shp_deliverystate', $fields1)){
		 $destination_location_array[] = 'shp_deliveryname';
		 $destination_location_array[] = 'shp_deliverycity';
		 $destination_location_array[] = 'shp_deliverystate';
		 $destination_location_array[] = 'shp_deliverypostal';
		 $destination_location_array[] = 'shp_deliverycountry';	
		 $destination_location_array[] = 'shp_deliverycontact';
		 $destination_location_array[] = 'shp_deliveryphone';			 
		}	
		if(in_array('shp_loadstatusid', $fields1)){
		 $ship_status_array[] = 'shp_loadstatusid';
		 $ship_status_array[] = 'statusname';	
		}		
		$mandatory_fields2 =array(
		'clientId',
		'shp_modeId',		
		);
		//echo '<pre>'; print_r($fields); exit;	
		
        return $this->listShipmentsForCustomer($request, array_merge($fields1,$pickup_location_array,$destination_location_array,$ship_status_array,$mandatory_fields2) , $auth);		
		
       /* return $this->listShipmentsForCustomer($request, array('clientId', 
		'LoadId',
        'shp_modeId',		
		'shp_shipnumber',
		'shp_pickupname',
		'client_firstname',
		'client_lastname',
		'client_phone',
		'client_compname',
		'carrier_name',
		'statusname',
		'shp_pronumber',
		'shp_loadstatusId',
		'shp_pickupcity',
		'shp_pickupstate',
		'shp_pickuppostal',
		'shp_pickupcountry',
		'shp_deliverycity',
		'shp_deliverystate',
		'shp_deliverypostal',
		'shp_deliverycountry',
		'shp_reqpickupdate',
		'shp_deliveryname',
		'shp_deliverycontact',
		'shp_deliveryphone',
		'shp_pickupcontact',
		'shp_pickupphone',		
		), $auth);*/
    }	
	
	
    public function getAjaxShipmentSummaryLists(Request $request, Guard $auth) {		
        return $this->listSummaryShipmentsForCustomer($request, array(), $auth);			
    }	
	
	
    public function getAjaxShipmentTab(Request $request, Guard $auth) {
        return $this->handleShipTabInfo($request, array(), $auth);
    }		
	
    public function getAjaxShipmentNotes(Request $request, Guard $auth) {
        return $this->listShipmentNotes($request, array(), $auth);
    }	
	
    public function getAjaxSuggestedHeadings(Request $request, Guard $auth) {
        return $this->listSuggestedHeadings($request, array(), $auth);
    }	
	
    public function getAjaxShipStatus(Request $request, Guard $auth) {
        return $this->listShipStatus($request, array(), $auth);
    }	
	
	
	public function getCustomerCostDirectoryView(){
	 return view('shipments.customer-cost');
	}
	
	public function getCarrierCostDirectoryView(){
	 return view('shipments.carrier-cost');
	}	
	
	public function getCustomerNotesView(){
	 return view('shipments.customer-notes');
	}

	public function getCarrierNotesView(){
	 return view('shipments.carrier-notes');
	}

	public function getTerminalNotesView(){
	 return view('shipments.terminal-notes');
	}
	
	public function getJscriptsAfterDomLoad(){
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		//dd($allFields);
	 
	 return view('shipments.js-loader')->with(['allFields'=>$allFields,]);
	}
	
	public function getJscriptsInvoiceAfterDomLoad(){
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.shipmentSetup'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }	
		//dd($allFields);
	 
	 return view('shipments.js-loader-invoice')->with(['allFields'=>$allFields,]);
	}	
	

    public function deleteAjaxSubShipmentDelete(Request $request) {
        $shipID = $request->get('shipID');
        return $this->deleteShipment($shipID);
    }
	
    public function postAjaxShipmentStatus(Request $request, Guard $auth) {
        $shipID = $request->get('shipId');
        return $this->updateShipmentStatus($shipID, $request, $auth);
    }	
	
    public function postAjaxShipmentInvoiceStatus(Request $request, Guard $auth) {
        $shipID = $request->get('shipId');
        return $this->updateShipmentInvoiceStatus($shipID, $request, $auth);
    }	
	
    public function getAjaxSourceLocation(Request $request, Guard $auth) {
      return view('shipments.s_location');		
    }	
	
    public function getAjaxSubShipStatusForUser(Request $request) {
        $shipID = $request->get('shipID');

			return $this->getShipStatusForUserList($request, $shipID, array('id', 
			'shipId', 
			'status_statusId',
			'status_changeddate',
			'status_changedbyId',
			'status_notes',
			'created_at',
			'updated_at'	
			));

    }	
	
	
    public function getShipFilesInfo(Request $request) {
        $shipID = $request->get('shipID');

			return $this->getShipFiles($request, $shipID, array('id', 
			'shipId', 
			'doc_typeId',
			'doc_location',
			'doc_createdbyId',
			'doc_createddate',
			'created_at',
			'updated_at'	
			));

    }


    public function getShipTrackList(Request $request) {
        $shipID = $request->get('shipID');

			return $this->getShipTracking($request, $shipID, array('id', 
			'shipId', 
			'track_date',
			'track_time',
			'track_statusId',
			'track_text',
			'track_city',
			'track_state',	
			'track_trailer',	
			'track_createdbyId',	
			'track_createdDate',	
			));

    }	
	
	
    public function getShipTerminalList(Request $request) {
        $shipID = $request->get('shipID');

			return $this->getShipTerminalData($request, $shipID, array('shipId', 
			'trm_orginname',
			'trm_origintype',
			'trm_originadr1',
			'trm_originadr2',
			'trm_origincity',
			'trm_originstate',	
			'trm_originpostal',	
			'trm_originemail',	
			'trm_originphone',	
			'trm_origincontactname',
			'trm_deliveryname',
			'trm_deliverytype',
			'trn_deliveryadr1',
			'trn_deliveryadr2',
			'trn_deliverycity',
			'trn_deliverystate',
			'trn_deliverypostal',
			'trn_deliverycontact',
			'trn_deliveryphone',
			'trn_deliveryemail'	
			));

    }	
	
	
    public function getLastLoadNumber(Request $request) {
        $shipID = $request->get('shipID');

			return $this->getLoadNumber($request, array('client_loadprefix', 
			'client_lastloadno'
			));

    }	
	
    public function getAjaxPrintShipment($id, $for) {
      //return view('shipments.print_shipment');
      return view('shipments.print_shipment')->with(['ship_id' => $id, 'for'=>$for]);	  
    }	
	
	public function postAjaxUploadFile(Request $request){
		return $this->handleBrandFileUpload($request);
	}	
	
    public function getStateList(Request $request, Guard $auth) {
        return $this->listState($request, array(), $auth);
    }		
}
