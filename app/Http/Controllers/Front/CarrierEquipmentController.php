<?php

namespace App\Http\Controllers\Front;

use App\Layout;
use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\CommonCarrierActionsTrait;

class CarrierEquipmentController extends Controller {
    
    use UtilityTrait,
        CommonCarrierActionsTrait;

    public function getAdd($carrierId) {
        return view('carriers.equipments.add')
                        ->with('carrierId', $carrierId);
    }

    public function getEdit($carrierId, $equipmentId) {
        return view('carriers.equipments.edit')
                        ->with('carrierId', $carrierId)
                        ->with('equipmentId', $equipmentId);
    }
    
    public function getAjaxCarrierEquipmentEdit(Request $request){
        $equipID = $request->get('equipID');
	return $this->getEquipmentDetails($equipID);
    }
        
    public function postAjaxCarrierEquipmentEdit(Request $request) {
        $equipID = $request->get('equipID');
        $carrierId = $request->get('carrierId');
        return $this->handleCarrierEquipment($request, $equipID, $carrierId);
    }
    
    public function getAjaxGetEquipmentTypes(){
        return $this->getEquipmentTypes();        
    }
    
     public function getAjaxGetOwnerTypes(){
	return $this->getOwnerTypes();
    }
    

}
