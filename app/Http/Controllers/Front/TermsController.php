<?php
namespace app\Http\Controllers\Front;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Front\Controller;
use App\Term;
use App\Customer;
use App\User;

class TermsController extends Controller{
	public function __construct(){
		
	}
	
	/*
	* method to display system terms and conditions before making a user as client
	* @access public
	* @param Illuminate\Contracts\Auth\Guard
	* @return void
	*/
	public function getSysTerms(Guard $auth){
		
		//if already accepted system terms then redirect to accept application specific terms
		if(!empty($auth->user()->clientID))
			return redirect()->to(\Config::get('app.frontPrefix'). '/terms/app-terms');
		
		$term = Term::select(['id', 'ts_effdate', 'ts_message'])
			->where('clientID', \Config::get('app.tsMasterClientID'))
			->where('ts_termtype', \Config::get('app.tSMasterType'))
			->first();
		return \View::make('terms.show_sys')
			->withTerm($term);
	}
	
	/*
	* method to process a user account after accepting system terms
	* @access public
	* @param Illuminate\Http\Request
	* @param Illuminate\Contracts\Auth\Guard
	* @return void
	*/
	public function postAcceptSysTerms(Request $request, Guard $auth){
		if($request->has('accepted')){
			//creating customer after reading all its properties
			//getting user data for the current user to register as a new customer
			$customer = new Customer;
			$customer->fill([
				'client_firstname' => $auth->user()->usr_firstname,
				'client_lastname' => $auth->user()->usr_lastname,
				'client_adr1' => $auth->user()->usr_adr1,
				'client_adr2' => $auth->user()->usr_adr2,
				'client_city' => $auth->user()->usr_city,
				'client_state' => $auth->user()->usr_state,
				'client_postal' => $auth->user()->usr_postal,
				'client_country' => $auth->user()->usr_country,
				'client_email' => $auth->user()->usr_email,
				'client_actdate' => \Carbon\Carbon::now()->toDateString(),
			]);
			
			if($customer->save()){
				$user = User::findorFail($auth->id());
				$user->clientID = $customer->id;
				if($user->save())
					return redirect()->to(\Config::get('app.frontPrefix'). '/terms/app-terms');
				else
					return redirect()->back()->with('error', 'Error processing your request. Please try again later.');
			}else
				return redirect()->back()->with('error', 'Error processing your request to accept terms. Please try again later.');
		}else
			return redirect()->back()->with('error', 'There is no approval to accept this terms');
	}
	
	/*
	* method to display system terms and conditions before making a user as client
	* @access public
	* @param Illuminate\Contracts\Auth\Guard
	* @return void
	*/
	public function getAppTerms(Guard $auth){
		
		//if already accepted system terms then redirect to accept application specific terms
		if($auth->user()->usr_type == 1 || $auth->user()->usr_acceptedterms)
			return redirect()->to('/');
		
		//getting parent company terms and conditions
		$parentClientID = $auth->user()->client->parent_id;
		
		$term = Term::select(['id', 'ts_effdate', 'ts_message'])
			->where('clientID', $parentClientID ? $parentClientID : \Config::get('app.tsMasterClientID'))
			->where('ts_termtype', \Config::get('app.tSAppType'))
			->orderBy('ts_effdate', 'asc')
			->first();
		return \View::make('terms.show_app')
			->withTerm($term);
	}
	
	public function postAcceptAppTerms(Request $request, Guard $auth){
		if($request->has('accepted')){
			$user = User::findOrFail($auth->id());
			
			$user->usr_acceptedterms = true;
			
			if($user->save())
				return redirect()->to('/');
			else
				return redirect()->back()->with('error', 'The terms and conditions could not be updated');
		}
	}
}