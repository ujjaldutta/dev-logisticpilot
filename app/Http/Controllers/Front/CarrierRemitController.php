<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\CommonCarrierActionsTrait;

class CarrierRemitController extends Controller {
    
    use UtilityTrait,
        CommonCarrierActionsTrait;

    public function getAdd($carrierId) {
        return view('carriers.remits.add')
                        ->with('carrierId', $carrierId);
    }

    public function getEdit($carrierId, $remitId) {
        return view('carriers.remits.edit')
                        ->with('carrierId', $carrierId)
                        ->with('remitId', $remitId);
    }
    
    public function getAjaxCarrierRemitEdit(Request $request){
        $remitID = $request->get('remitID');
	return $this->getRemitDetails($remitID);
    }
        
    public function postAjaxCarrierRemitEdit(Request $request) {
        $remitID = $request->get('remitID');
        $carrierId = $request->get('carrierId');
        return $this->handleCarrierRemit($request, $remitID, $carrierId);
    }
    
    public function getAjaxGetCurrTypes(){
        return $this->getCurrTypes();        
    }    

}
