<?php

namespace App\Http\Controllers\Front;

//namespace Amitavroy\Utils;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\User;
use App\Customer;
use App\Layout;
use App\Traits\UtilityTrait;
use App\Traits\RegistrationSteps;
use App\Traits\CommonUserClientActionsTrait;
use App\Traits\AccessorialServicesTrait;
//To generate random password
use App\RandomPasswordGenerator;

#request validators
use App\Http\Requests\CustomerDefaultSettingsRequest;
use App\Http\Requests\UserRequest;

class AccessorialController extends Controller {

    use UtilityTrait,
        RegistrationSteps,
        CommonUserClientActionsTrait,
		AccessorialServicesTrait;

    const ClienTypeSubClient = 2;

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function anySetClientID($clientID) {
        $customer = Customer::select(['id', 'client_firstname', 
			'client_lastname', 'client_compname', 
			'client_adr1', 'client_adr2',
			'client_city', 'client_state',
			'client_country', 'client_postal',
			'client_email', 'client_phone',
			])->findOrFail($clientID);

        if (\CustomerRepository::setCurrentCustomer($customer->toArray()))
        //return redirect()->back();
            return redirect()->to('/' /*\Config::get('app.frontPrefix')  . '/customers' */);
        else
            throw new \Exception('Error setting customer id');
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function anyResetClientID() {
        if (\CustomerRepository::setCurrentCustomer(\Auth::user()->client->toArray()))
        //return redirect()->back();
            return redirect()->to('/' /* \Config::get('app.frontPrefix') . '/customers' */);
        else
            throw new \Exception('Error setting customer id');
    }

    public function anyIndex() {
        $userId = \Auth::user()->id;
        $checkLayout = Layout::where('userId', $userId)
                ->where('layoutId', \Config::get('AppLayout.customerApp'))
                ->count();
        if (!$checkLayout) {
            $allFields = Layout::whereNull('userId')->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        } else {
            $allFields = Layout::where('userId', $userId)->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        }
//dd($checkLayout);
        return view('customers.index')->with('allFields', $allFields);
    }

    public function getAdd() {
        $randomPassword = RandomPasswordGenerator::generate();

        return view('customers.add')->with('randomPassword', $randomPassword);
    }

    public function getEdit($id) {
        if (Customer::where('id', $id)->count())
            return view('customers.edit')->withCustomerId($id);
        else
            abort(404, 'Customer not found');
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function getDownload(Guard $auth) {

        //passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('client_code', 'client_firstname', 'client_adr1', 'client_city', 'client_state', 'client_postal', 'client_phone', 'client_email');
        $arrFirstRow = array('Customer ID', 'Customer Name', 'Customer Address', 'City', 'State', 'Postal Code', 'Phone', 'Email');

        //$data = \CustomerRepository::getChildren()
        //	->get($arrColumns)
        //->toArray();

        $data = \CustomerRepository::getChildren();

        $data = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $data);

        $data = $data->get($arrColumns)->toArray();
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        return $this->convertToCSV($data, $options);
    }

    /*
     * Method to get profile information for a given customer
     * @returns json
     * @access public
     * @
     */

    public function getAjaxSubClientEdit(Request $request) {
        $clientID = $request->get('custID');
        return $this->getProfileDetails($clientID);
    }

    /*
     * Method to get profile information for a given customer
     * @returns json
     * @access public
     * @
     */

    public function postAjaxSubClientEdit(Request $request) {
        $clientID = $request->get('custID');


        return $this->handleClientProfile($request, $clientID);
    }

    public function postAjaxCreateNewUserWithClientData(Request $request) {
        try {
            if (!$request->has('userId'))
                $user = new User;
            else
                $user = User::findOrFail($request->get('userId'));

            $v = \Validator::make([
                        'usr_firstname' => $request->get('client_firstname'),
                        'usr_lastname' => $request->get('client_lastname'),
                        'email' => $request->get('client_email'),
                            ], [
                        'usr_firstname' => 'required|alpha',
                        'usr_lastname' => 'required|alpha',
                        'email' => !$request->get('userId') ? "required|email|unique:m_users,email" :
                                "required|email|unique:m_users,email," . $request->get('userId'),
            ]);
            //validating before user create
            if ($v->fails())
                return response()->json(['error' => $v->messages(),]);

            $user->usr_firstname = $request->get('client_firstname');
            $user->usr_lastname = $request->get('client_lastname');
            $user->email = $request->get('client_email');
            $user->usr_email = $request->get('client_email');
            $user->password = \Hash::make($request->get('client_password'));
            $user->clientID = $request->get('clientID');
            $user->usr_phone = $request->get('client_phone');
            $user->usr_phone = $request->get('client_phone');
            //$user->usr_fax = $request->get('client_fax');
            $user->usr_adr1 = $request->get('client_adr1');
            $user->usr_adr2 = $request->get('client_adr2');
            $location = $this->_processGoogleLocation($request->get('location'), 'usr_postal', 'usr_city', 'usr_state', 'usr_country');
            $user->usr_postal = isset($location['usr_postal']) ? $location['usr_postal'] : null;
            $user->usr_city = isset($location['usr_city']) ? $location['usr_city'] : null;
            $user->usr_state = isset($location['usr_state']) ? $location['usr_state'] : null;
            $user->usr_country = isset($location['usr_country']) ? $location['usr_country'] : null;
            $user->usr_completereg = 1;

            if ($user->save()) {
                /* now syncing user client map pivot table for this client to map 
                  to this logged in user
                 */

                //firstly detaching if already added this clientID works on when editing
                \Auth::user()->clients()->detach($user->clientID);

                //attaching the current user id with the newly created or updated client id
                \Auth::user()->clients()->attach($user->clientID);

                return response()->json([
                            'success' => 'User created successfully',
                            'userId' => $user->id,
                            'client_password' => $request->get('client_password')
                ]);
            } else
                return response()->json(['error' => 'User not created successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * Method to get profile information for a given customer
     * @returns json
     * @access public
     * @
     */

    public function deleteAjaxSubClientDelete(Request $request) {
        $clientID = $request->get('custID');
        return $this->deleteClient($clientID);
    }

    /*
     * Method to get all default settings for a given customer
     * @returns json
     * @access public
     * @
     */

    public function getAjaxDefaultSettings(Request $request) {
        $clientID = $request->get('custID');
        return $this->getDefaultSettings($clientID);
    }

    /*
     * Method to get all default settings for a given customer
     * @returns json
     * @access public
     * @
     */

    public function postAjaxDefaultSettings(CustomerDefaultSettingsRequest $request) {
        //die('here');
        $clientID = $request->get('custID');
        return $this->postDefaultSettings($request, $clientID);
    }

    /*
     * @returns json
     *
     */

    public function getAjaxSubClientLists(Request $request, Guard $auth) {
        return $this->listCustomersForUser($request, array(), $auth);
    }

    public function getDownloadCsv() {
        $clientData = Customer::where('id', \Auth::user()->clientID)
                        ->with([
                            'subClients',
                        ])->first(['id']);

        $count = (count($clientData) && $clientData->count() && isset($clientData->subClients) && $clientData->subClients->count()) ? $clientData->subClients->count() : 0;

        return response()->$clientData;
    }

    /*
     * method to get the list of clients children 
     * to the currently set customer id
     */

    public function getAjaxSearchClients(Request $request) {
        /* $q = $request->get('q');
          if(!empty($q) && strlen($q) >= 2){
          $user = User::where('id', \Auth::id())
          ->with([
          'clients' => function($query)use ($q){
          $query->where('client_firstname', 'LIKE', "{$q}%");
          $query->orWhere('client_compname', 'LIKE', "{$q}%");
          }
          ])->first(['id', 'clientID']);

          //dd($user->toArray());

          if(count($user) && isset($user->clients) && $user->clients->count()){
          $clients = [];
          foreach($user->clients as $client){
          $description = '';

          if(!empty($client->client_city))
          $description .=', '. $client->client_city;
          if(!empty($client->client_state))
          $description .=', '. $client->client_state;
          if(!empty($client->client_postal))
          $description .=', '. $client->client_postal;
          if(!empty($client->client_country))
          $description .=', '. $client->client_country;
          if(!empty($client->client_phone))
          $description .=', Contact Ph : '. $client->client_phone;

          $clients[] = [
          'companyName' => $client->client_compname,
          'name' => $client->client_firstname. ' '.  $client->client_lastname,
          'description' => $description,
          'clientID' => $client->id,
          ];
          }
          return response()->json($clients);
          }else
          return response()->json([]);

          }
          return response()->json([]);
         */
        return $this->getChildClients($request);
    }

    public function postAjaxSendPassword(Request $request) {
        try {
            $email = $request->email;
            $password = $request->password;

            \Mail::send('emails.send_password', ['password' => $password, 'email' => $email], function ($m) use ($password, $email) {
                $m->to($email, $email)->subject('Password for your account on Logistics Pilot');
            });

            return response()->json(['success' => 'Email sent successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    public function postAjaxSendUpdatePassword(Request $request) {
        try {
            $randomPassword = RandomPasswordGenerator::generate();
            $email = $request->get('email');
            $custId = $request->get('custId', 0);

            $userDetail = User::where('clientID', $custId)->firstOrFail(['id', 'usr_firstname', 'usr_lastname']);

            //now assigning new password
            $userDetail->password = \Hash::make($randomPassword);

            if ($userDetail->save()) {
                \Mail::send(
                        'emails.send_password', ['password' => $randomPassword, 'email' => $email], function ($m) use ($randomPassword, $email, $userDetail) {
                    $m->to($email, $userDetail->usr_firstname . ' ' . $userDetail->usr_lastname)
                            ->subject('Password for your account on Logistics Pilot');
                }
                );
            } else
                throw new \Exception('Error saving new password');

            return response()->json(['success' => 'Email sent successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    public function getAjaxGetLanguages() {
        return response()->json(['success' => 'Locales loaded successfully', 'languages' => \App\Locale::all(['name', 'id'])]);
    }

    public function getAjaxGetTypes() {
        //return response()->json(['success' => 'Locales loaded successfully', 'types' => \App\Locale::all(['usertype', 'id'])]);
        return response()->json(['success' => 'Locales loaded successfully', 'types' => \App\Locale::all(['name', 'id'])]);
    }

    public function getAjaxGetThemes() {
        return response()->json(['success' => 'Themes loaded successfully', 'themes' => \App\Theme::all(['theme_name', 'id'])]);
    }

    public function getAjaxGetCommodity() {
        return response()->json(['success' => 'Themes loaded successfully', 'commodity' => \App\CommodityType::all(['name as label', 'id'])]);
    }

    public function getAjaxGetClasses() {
        return response()->json(['success' => 'Classes loaded successfully', 'classes' => \App\FreeClassType::all(['name as label', 'id'])]);
    }
    
    public function getAjaxGetShipment() {
        return response()->json(['success' => 'Classes loaded successfully', 'shipment' => \App\ShipmentType::all(['name as label', 'id'])]);
    }
    
    public function getAjaxGetBolTemp() {
        return response()->json(['success' => 'Classes loaded successfully', 'bolTemplates' => \App\BolTemplatesType::all(['name as label', 'id'])]);
    }
    
    public function getAjaxGetInvoiceTerms() {
        return response()->json(['success' => 'Classes loaded successfully', 'invoiceTerms' => \App\InvoiceTermsType::all(['name as label', 'id'])]);
    }
    
    public function getAjaxGetInvoiceDays() {
        return response()->json(['success' => 'Classes loaded successfully', 'invoiceDays' => \App\InvoiceDaysType::all(['name as label', 'id'])]);
    }
    
    public function getAjaxGetInvoiceTemp() {
        return response()->json(['success' => 'Classes loaded successfully', 'invoiceTemplates' => \App\InvoiceTemplateType::all(['name as label', 'id'])]);
    }

    public function getAjaxSubAccessorialListsData(Request $request) {
        return $this->listAccessorialForUserData($request);
    }		
}
