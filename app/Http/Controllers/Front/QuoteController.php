<?php namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Http\Controllers\Front\Controller;

use Illuminate\Http\Request;

use App\Traits\QuoteServices;

class QuoteController extends Controller {
	
	use QuoteServices;
	
	public function __construct(){
		$this->middleware('auth.front');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$action = $request->get('action');
		switch($action){
			
			default:
				return response()->json(['error' => 'Required action is missing or not matching']);
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//detecting the request for which step
		$step = $request->get('action');
		
		switch($step){
			case 'saveQuote':
				return $this->saveQuoteForCarrier($request);
			default:
				throw new \Exception('Unknown step to handle');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		$action = $request->get('type');
		//$id = $request->get('id');
		switch($action){
			default:
				throw new \Exception('Unknown step to handle');
		}
	}
}
