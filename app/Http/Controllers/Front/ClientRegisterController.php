<?php namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Http\Controllers\Front\Controller;

use Illuminate\Http\Request;

use App\Traits\UtilityTrait;
use App\Traits\RegistrationSteps;

class ClientRegisterController extends Controller {
	const UPLOAD_DIRECTORY = 'uploads',
		BRAND_IMAGE_DIRECTORY = 'brands',
		CARRIER_IMAGE_DIRECTORY = 'carriers',
		DOCMANAGER_DIRECTORY = 'docmanager';
	
	use UtilityTrait, RegistrationSteps;
	
	public function __construct(){
		$this->middleware('auth.front');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$action = $request->get('action');
		switch($action){
			case 'getProfileDetails':
				return $this->getProfileDetails();
			case 'getBillingDetails':
				return $this->getBillingDetails();
			case 'getSelectedCarriers':
				$withSetupDetails = $request->get('withCarrierSetup');
				$searchString = $request->get('q', null);
				$onlyApiEnabled = $request->get('apiEnabled', false);
				$limit = $request->get('limit');
				$clientID = $request->get('clientID');
				//ujjal
				$ProfileId = $request->get('ProfileId', null);
				
				
				if(!empty($withSetupDetails) && (bool) $withSetupDetails == true)
					return $this->listSelectedCarriers(true, $searchString, $onlyApiEnabled, $limit, $clientID,$ProfileId);
				else
					return $this->listSelectedCarriers(false, $searchString, $onlyApiEnabled, $limit, $clientID,$ProfileId);
			
			case 'getUnassignedCarriers':
			$withSetupDetails = $request->get('withCarrierSetup');
				$searchString = $request->get('q', null);
				$onlyApiEnabled = $request->get('apiEnabled', false);
				$limit = $request->get('limit');
				$clientID = $request->get('clientID');
				
				return $this->listUnassignedCarriers(true, $searchString, $onlyApiEnabled, $limit, $clientID);
				
			case 'getCarrierApiDetails':
				return $this->getCarrierApiDetails($request);
			case 'getCarrierAccTariffDetails':
				return $this->getCarrierAccTariffDetails($request);
			case 'getCarrierAccsTariffData':
				$id = $request->get('id', 0);
				return $this->getAccessorialData($id);
			case 'getCarrierFuelTariffDetails':
				return $this->getCarrierFuelTariffDetails($request);
			case 'getCarrierFuelTariffData':
				$id = $request->get('id', 0);
				return $this->getFuelData($id);
			case 'getCarrierApiConnectivity':
				return $this->getApiStatus($request);
			case 'getCarrierAccessorials':
				return $this->getCarrierAccessorials($request);
			case 'getItemTypes':
				return $this->getItemTypes($request);
			case 'validateAllSteps':
				return $this->validateAllSteps();
			case 'completeRegSteps':
				return $this->updateRegsteps();
			default:
				return response()->json(['error' => 'Required action is missing or not matching']);
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//detecting the request for which step
		$step = $request->get('step');
		
		switch($step){
			case 1:
				return $this->handleClientProfile($request);
			case 2:
				return $this->handleClientBillingInfo($request);
			case 3:
				return $this->handleCarrierList($request);
			case 4:
				return $this->handleCarrierApiSetup($request);
			case 'saveCarrierContact':
				return $this->handleCarrierContact($request);
			case 'saveCarrierContractSetup':
				return $this->handleCarrierSetup($request);
			case 'postCarrierAccTariffCustomDetails':
				return $this->handleCarrierAccTariffCustomDetails($request);
			case 'postCarrierFuelTariffCustomDetails':
				return $this->handleCarrierFuelTariffCustomDetails($request);
			default:
				throw new \Exception('Unknown step to handle');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		$action = $request->get('type');
		//$id = $request->get('id');
		switch($action){
			case 'carrierAssignment':
				return $this->deleteCarrierProvider($id);
			case 'carrierContractFile':
				return $this->deleteCarrierContractFile($id);
			case 'deleteCarrierAccTariffCustomDetails':
				return $this->deleteCarrierAccTariffCustomDetails($id);
			case 'deleteCarrierFuelTariffCustomDetails':
				return $this->deleteCarrierFuelTariffCustomDetails($id);
			default:
				throw new \Exception('Unknown step to handle');
		}
	}
	
	public function uploadImage(Request $request){
		return $this->handleBrandFileUpload($request);
	}
	
	public function postAjaxUploadCarrierImage(Request $request){
		return $this->handleCarrierLogoUpload($request);
	}
	
	public function postAjaxSaveContractFile(Request $request){
		if($request->hasFile('file') /* && $request->has('id') */ && $request->has('carrierID')){
			$docManager = $this->handleUploadToDocManager($request);
			$docManager = json_decode($docManager);
			//return  response()->json($docManager);
			if(isset($docManager->success) && isset($docManager->id) && isset($docManager->url)){
				return $this->handleCarrierContractSetup(
					$request->get('carrierID'), $request->get('id'), $docManager->id, $docManager->url
				);
			}else
				return response()->json(['error' => 'No valid file uploaded']);
		}else
			return response()->json(['error' => 'required information missing']);
	}
	
	public function getAjaxStep3AddCarrier(){
		return view('regsteps.add_carrier');
	}
	
	public function postAjaxStep3AddCarrier(Request $request){
		return $this->handleCarrierCreation($request);
	}
	
}
