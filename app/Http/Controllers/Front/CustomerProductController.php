<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\CustomerProductServicesTrait;
use App\ClientItem;
use App\Layout;

class CustomerProductController extends Controller {

    use CustomerProductServicesTrait, UtilityTrait;
    
    public function anyIndex() {
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.customerProduct'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.customerProduct'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }
        //dd($allFields);
        return view('clientproducts.index')->with('allFields', $allFields);
    }

    public function getAdd() {
        return view('clientproducts.add');
    }

    public function getEdit($id) {
        if (!$this->getProductDetails($id)->count())
            abort(404, 'Customer not found');

        return view('clientproducts.edit')->withId($id);
    }

    /*
     * @returns json
     *
     */

    public function getAjaxListProducts(Request $request) {
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.customerProduct'))
                ->orderBy('displayOrder', 'ASC')
                ->lists('displayField');

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.customerProduct'))
                    ->orderBy('displayOrder', 'ASC')
                    ->lists('displayField');
        }
        
        return $this->listProducts($request, $allFields);
    }
	
    public function getAjaxProductList(Request $request) {
        return $this->listProductdata($request);
    }	

    public function getAjaxWeightTypes() {
        return $this->getWeightTypes();
    }
    
    public function getAjaxCommodityTypes() {
        return $this->getCommodityTypes();
    }
    
    public function getAjaxDimmensionsTypes() {
        return $this->getDimmensionsTypes();
    }
    
    public function getAjaxTemperatureTypes() {
        return $this->getTemperatureTypes();
    }
    
    public function getAjaxClassTypes() {
        return $this->getFreeClassTypes();
    }
    
    public function getAjaxFreightTypes() {
        return $this->getClassTypes();
    }

    public function getAjaxPackageTypes() {
        return $this->getPackageTypes();        
    }
	
	/**** For autocomplete data generation *****/
    public function getAjaxPackageTypeList(Request $request) {
        return $this->getPackageTypeList($request);        
    }	
    /**** For autocomplete data generation *****/
    public function getAjaxClassTypeList(Request $request) {
        return $this->getFreeClassTypeList($request);
    }
	
    public function postAjaxProductEdit(Request $request) {
        try {
            $productId = $this->saveProductDetails($request);
            if ($productId)
                return response()->json(['success' => 'Information saved successfully', 'productId' => $productId]);
            else
                return response()->json(['error' => 'Information not saved successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    public function getAjaxProductEdit(Request $request) {
        try {
            $id = $request->get('id');

            $clientProduct = $this->getProductDetails($id);
            if ($clientProduct)
                return response()->json(['success' => 'Information loaded saved successfully', 'clientProduct' => $clientProduct]);
            else
                return response()->json(['error' => 'Information not saved successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * 
     * @returns json
     * @access public
     * @
     */

    public function deleteAjaxProductDelete(Request $request) {
        try {
            $id = $request->get('id');

            if ($this->deleteClientProduct($id))
                return response()->json(['success' => 'Information deleted successfully']);
            else
                return response()->json(['error' => 'Information not deleted successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function getDownload() {

        //passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('item_name', 'item_weight', 'item_class', 'item_nmfc', 'item_tempmin', 'item_tempmax');

        $arrFirstRow = array('Item Name', 'Weight', 'Class', 'NMFC', 'Temp. Min. ', 'Temp. Min.');

        $data = ClientItem::where('clientId', \CustomerRepository::getCurrentCustomerID());

        $data = $data->get($arrColumns)->toArray();
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
            'fileName' => 'client_products_report.csv'
        );
        return $this->convertToCSV($data, $options);
    }

}
