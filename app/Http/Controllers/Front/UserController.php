<?php
namespace App\Http\Controllers\Front;
//namespace Amitavroy\Utils;

use Illuminate\Http\Request;
use Illuminate\Auth\Guard;
use App\Http\Controllers\Front\Controller;

use App\User;
use App\Layout;

//To generate random password
use App\RandomPasswordGenerator;

#request validators
#use App\Http\Requests\CustomerDefaultSettingsRequest;
#use App\Http\Requests\UserRequest;

use App\Traits\UtilityTrait;
use App\Traits\CommonUserClientActionsTrait;
use App\Traits\CustomizationTrait;

class UserController extends Controller{
    use UtilityTrait, CommonUserClientActionsTrait, CustomizationTrait;
	
	const DefaultUserType = 2;
    
    public function anyIndex(){
		$userId=\Auth::user()->id;
		$checkLayout = Layout::where('userId', $userId)
					   ->where('layoutId', \Config::get('AppLayout.userApp'))
					   ->count();
		if (!$checkLayout) {
		    $allFields = Layout::whereNull('userId')->where('layoutId', \Config::get('AppLayout.userApp'))->orderBy('displayOrder','ASC')->get(['id', 'displayName', 'displayField']);			
		}
		else
		{
			$allFields = Layout::where('userId', $userId)->where('layoutId', \Config::get('AppLayout.userApp'))->orderBy('displayOrder','ASC')->get(['id', 'displayName', 'displayField']);						
		}	
		//dd($checkLayout);
		//echo "<pre>";
		//print_r($allFields);
		//echo "</pre>";
		//die();
        return view('users.index')->with('allFields',$allFields);
       // return view('users.index');
    }
	
	public function getAdd(){
		$randomPassword = RandomPasswordGenerator::generate();
        return view('users.add')->with('randomPassword', $randomPassword);
    }
    
    public function getEdit($id){
        if(User::where('id', $id)->count())
            return view('users.edit')->withUserId($id);
        else
            abort( 404, 'Customer not found');
    }
	
	public function getAjaxManageSubUser(Request $request){
		try{
			$userId = $request->get('userID', 0);
			$userDetails = User::find($userId);
			return response()->json(['success' => 'User Details loaded successfully', 'userDetails' => $userDetails]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	
	public function postAjaxManageSubUser(Request $request){
		//dd($request->all());
		try{
			if(!$request->has('userID')){
				$user = new User;
				$user->password = \Hash::make($request->get('password'));
				$user->clientID = $request->get('clientID');				
				$user->usr_completereg = 1;
			}else
				$user = User::findOrFail($request->get('userID'));
			
			$v = \Validator::make(
				$request->except(['usr_location']), 
				[
					'usr_firstname' => 'required|alpha',
					'usr_lastname' => 'required|alpha',
					'clientID' => 'required|numeric',
					'usr_type_id' => 'required|numeric',
					'email' => !$request->get('userID') ? "required|email|unique:m_users,email" :
						"required|email|unique:m_users,email,". $request->get('userID'),
				]);
			//validating before user create
			if($v->fails())
				return response()->json(['error' => $v->messages(), ]);
			$user->fill($request->except(['usr_location']));
			
			$location = $this->_processGoogleLocation($request->get('usr_location'), 'usr_postal', 'usr_city', 'usr_state', 'usr_country');
			$user->usr_postal = isset($location['usr_postal']) ? $location['usr_postal'] : $user->usr_postal;
			$user->usr_city = isset($location['usr_city']) ? $location['usr_city'] : $user->usr_city;
			$user->usr_state = isset($location['usr_state']) ? $location['usr_state'] : $user->usr_state;
			$user->usr_country = isset($location['usr_country']) ? $location['usr_country'] : $user->usr_country;
			
			
			if($user->save()){
				//now setting up app permission to assign default apps w.r.t. db only when creating new user
				if(!$request->has('userID')){
					$this->setupApps($request->usr_type_id, $user->id);
				}
				
				return response()->json([
					'success' => 'User saved successfully', 
					'userID' => $user->id, 
					'client_password' => $request->get('password')
				]);
			}else
				return response()->json(['error' => 'User not created successfully',]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}	
	
	/*
	* Method to get profile information for a given customer
	* @returns json
	* @access public
	* @
	*/
	public function deleteAjaxSubUserDelete(Request $request){
		try{
			$userID = $request->get('userID', 0);
			$user = User::findOrFail($userID);
			if($user->delete())
				return response()->json(['success' => 'User deleted successfully',]);
			else
				return response()->json(['error' => 'User not deleted successfully',]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	public function anyRegistred(){	
		$randomPassword = RandomPasswordGenerator::generate();	
      
        return view('users.register')->with('randomPassword',$randomPassword);
    }
	
	public function getAjaxGetClientLists(Request $request){
		return $this->getChildClients($request);
	}
	
	public function getAjaxClientMapping(Request $request){
		$userId = $request->get('userID', 0);
		
		return $this->getUserClientMapping($userId);
		//return \CustomerRepository::getAvailableChildrenIds();
	}
	
	public function postAjaxClientMapping(Request $request){
		
		$clientList = $request->get('clientList', []);
		$userId = $request->get('userID', 0);
		
		if($request->has('all') && $request->get('all') == 'true'){
			$clientList = \CustomerRepository::getAvailableChildrenIds();
			//dd($clientList);
		}
		
		return $this->syncUserClientMapping($clientList, $userId);
	}
	
	public function deleteAjaxClientMapping(Request $request){
		$userID = $request->get('userID', 0);
		$clientID = $request->get('clientID', 0);
		
		return $this->detachUserClientMapping($clientID, $userID);
	}
	 /*
    * @returns json
    *
    */
    public function getAjaxSubUserLists(Request $request, Guard $auth){
        return $this->listUsersforCustomers($request, array(), $auth);
    }
	
	/*
    * @returns json
    *
    */
    public function getAjaxLayoutTypes(){
        return $this->getLayoutTypes();
    }
	
	public function getAjaxLayoutAttributes(Request $request){
		$layoutId = $request->get('layoutID', 0);
        return $this->getLayoutAttributes($layoutId);
    }
	
	public function getAjaxConstraintOperators(){
		try{
			$operators = $this->getAllConstraintOperators();
			return response()->json(['success' => 'Operators loaded successfully', 'constraintOperators' => $operators]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	public function getAjaxScreenFilters(Request $request){
		try{
			$userId = $request->get('userId', 0);
			return response()->json(['success' => 'Screen Filters loaded successfully', 'screenFilters' => $this->getAllScreenFilters($userId)]);
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	public function postAjaxScreenFilters(Request $request){
		try{
			$filterId = $this->saveScreenFilter($request);
			if($filterId)
				return response()->json(['success' => 'Screen Filter saved successfully', 'filterId' => $filterId]);
			else
				return response()->json(['error' => 'Screen Filter not saved successfully', ]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	public function deleteAjaxScreenFilters(Request $request){
		try{
			$layoutId = $request->get('layoutId');
			$id = $request->get('id');
			
			if($this->deleteScreenFilter($layoutId, $id))
				return response()->json(['success' => 'Screen Filter deleted successfully', ]);
			else
				return response()->json(['error' => 'Screen Filter not deleted successfully', ]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	public function getAjaxSavedAttributes(Request $request){
		$layoutId = $request->get('layoutID', 0);
		$userId = $request->get('userId', 0);
		
		return $this->getSavedAttributes($userId, $layoutId);
	}
	
	public function getAjaxAllLayoutMappings(Request $request){
		try{
			$userId = $request->get('userId', 0);
			return response()->json(['success' => 'Layouts loaded successfully', 'layoutMappings' => $this->getAllLayoutMappings($userId)]);
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	public function deleteAjaxAllLayoutMappings(Request $request){
		try{
			$userId = $request->get('userId', 0);
			$layoutTypeId = $request->get('layoutTypeId', 0);
			if($this->deleteLayoutMappings($userId, $layoutTypeId))
				return response()->json(['success' => 'Layout assignments deleted successfully', ]);
			else
				return response()->json(['error' => 'Layout assignments not deleted successfully', ]);
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	public function postAjaxSaveAttributes(Request $request){
		try{
			$userId = $request->get('userId', 0);
			$layoutId = $request->get('layoutId', 0);
			$assignments = $request->get('selectedAttributes', []);
			//dd($request->all());
			//saving the new assignments
			$this->saveAttributes($userId, $layoutId, $assignments);
			return response()->json(['success' => 'attributes saved successfully', ]);
				
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	public function getAjaxGetUserTypes(){
		return response()->json([
			'success' => 'User Types loaded successfully', 
			'userTypes' => \App\UserType::where('id', '<>', \Config::get('app.superAdminID'))->get(['usertype', 'id']),
		]);
	}
	
	public function postAjaxSendPassword(Request $request){		
		try{
			$email=$request->email;
			$password=$request->password;
			
			\Mail::send('emails.send_password_user', ['password' => $password, 'email' => $email ], function ($m) use ($password, $email) {
				$m->to($email, $email)->subject('Password for your account on Logistics Pilot');
			});	
			
			return response()->json(['success' => 'Email sent successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
    }
	
	public function postAjaxSendUpdatePassword(Request $request){	
		try{
			
			$randomPassword = RandomPasswordGenerator::generate();
			$email = $request->get('email');
			//$userId = $request->get('userId');			
			$userDetail = User::where('email', $email)->firstOrFail(['id', 'usr_firstname', 'usr_lastname']);
			
			//now assigning new password
			$userDetail->password = \Hash::make($randomPassword);
			
			if($userDetail->save()){
				\Mail::send(
					'emails.send_password_user', 
					['password' => $randomPassword, 'email' =>$email], 
					function ($m) use ($randomPassword, $email, $userDetail) {
						$m->to($email, $userDetail->usr_firstname. ' '. $userDetail->usr_lastname)
							->subject('Password for your account on Logistics Pilot');
					}
				);
			}else
				throw new \Exception('Error saving new password');

			return response()->json(['success' => 'Email sent successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
    }
	
	public function getDownload(Guard $auth){ 
		// passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('id','usr_firstname','usr_adr1','usr_city','usr_state','usr_postal','usr_phone','email');         
        $arrFirstRow = array('User ID','User Name','User Address','City','State','Postal Code','Phone','Email');
		
		//$assignedUsers = $this->getMappedUsers(\CustomerRepository::getCurrentCustomerID());
		
		//$data = User::where('clientID', \Auth::user()->clientID)
		//	->where('id','!=',\Auth::user()->id)		
         //   ->get($arrColumns)
		//	->toArray();  
			
           $data = User::where('clientID', \Auth::user()->clientID)
			->where('id','!=',\Auth::user()->id); 

		   $data = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.userApp'), $auth, $data);
          //  dd($data);
		   $data = $data->get($arrColumns)->toArray();
          // building the options array
          $options = array(
              'columns' => $arrColumns,
              'firstRow' => $arrFirstRow,
          );         
          return $this::convertToCSV($data, $options);  
    }
	
	public function getProfile(){
		$id=\Auth::User()->id;
        if(User::where('id', $id)->count())
            return view('users.profile')->withUserId($id);
        else
            abort( 404, 'Customer not found');
    }
}