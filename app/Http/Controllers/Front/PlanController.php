<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;
use App\SubscriptionPlan;

class PlanController extends Controller{
	public function __construct(){
		
	}
	
	public function anyPricing(){
		$plans = SubscriptionPlan::where('status', 1)
			->get([
				'id', 
				'name', 
				'price', 
				'description', 
				'colorcode'
			]);
		
		return view('plans.pricing')
			->withPlans($plans);
	}
}