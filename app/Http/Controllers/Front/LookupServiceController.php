<?php

namespace App\Http\Controllers\Front;

//namespace Amitavroy\Utils;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Http\Controllers\Front\Controller;
use App\User;
use MongoClient;
use App\Layout;
use App\Models\Profile\ContractModel;
use App\Traits\UtilityTrait;
use App\Traits\LookupServiceTrait;

use DB;
use Illuminate\Routing\UrlGenerator;
use App\Country;

class LookupServiceController extends Controller {

const UPLOAD_DIRECTORY = 'uploads',
		BRAND_IMAGE_DIRECTORY = 'brands',
		CARRIER_IMAGE_DIRECTORY = 'carriers',
		DOCMANAGER_DIRECTORY = 'docmanager',
		CONTRACT_DIRECTORY = 'contract';
		
      use LookupServiceTrait;

  public function __construct() {
      //$this->middleware('auth.front');
		
      }

 public function anyIndex() {
	
      
    }
  
  public function postCarrierAjax(Request $request){
	  
	   return $this->getCarriers($request);
	   
	   
   }
   
  public function postCountryList(Request $request){
	  
	   return $this->GetCountryList($request);
	   
	   
   } 
   
   public function getStateList(Request $request){
	
	  return $this->StateList($request);
	   
	   
   } 
   
   public function postStateList(Request $request){
	 
	  return $this->StateList($request);
	   
	   
   } 
   
   public function postCityList(Request $request){
	  
	   return $this->GetCityList($request);
	   
	   
   } 
    public function postPostalList(Request $request){
	  
	   return $this->fetchPostalList($request);
	   
	   
   } 
   
   public function getPostalList(Request $request){
	  
	   return $this->PostalList($request);
	   
	   
   }
   
   public function getStatecityList(Request $request){
	  
	   return $this->StatecityList($request);
	   
	   
   }
   
   
   
   public function postTest(Request $request){
	    return $this->GetCountryloc($request);
   }
   
   
   
  /******************************************************************************************************/
   public function getCountry(Request $request) {
	  $skip=$request->input('skip');
	  $rec=DB::table('m_country')->take(500)->skip($skip)->get();
	
	if(count($rec)>0){
		
		$this->CreateCountry($rec);
		
	}else{
		exit;
	}
	echo $skip=$skip+500;

	echo '<script>window.location.href="'.url('/front/services/country?skip='.$skip).'"</script>';
	exit;
   }
   
   
    
    
    
    
    public function postUpload(Request $request){
	   //uploading new logo and cleaning the old one if any
	   $contract = ContractModel::where("_id",$request->input('cid'))->first();
	 
                    $result = $this->_handleUploadAndResize($request, 'contract_doc', 282, 53, true,$contract->file);
                    
                    
                    if($result['success']){
						//$contract = ContractModel::where("_id",$request->input('cid'));
						
						$data['file']=$result['fileName'];
						$contract->update($data);
						return $result;
						
					}
					
   }
   
   
   public function postFileurl(Request $request){
	   //uploading new logo and cleaning the old one if any
				$contract = ContractModel::where("_id",$request->input('cid'))->get()->toArray();
				
                    $fileurl=$this->getAssetUrl($contract[0]['file'], 'contract_doc');
                    return json_encode(['fileurl' => $fileurl]);
					
   }
   
    public function postDelfile(Request $request){
	   //uploading new logo and cleaning the old one if any
				$contract = ContractModel::where("_id",$request->input('cid'))->first();
				
				//print_r($contract->file);exit;
				
				$this->_deleteFile($contract->file, 'contract_doc', true); //true to remove that resource from s3 bucket
				
				$data['file']='';
				$contract->update($data);
				
               
                return json_encode(['success' => true]);
					
   }
   
   public function postAssocType(Request $request){
	   
	   
	   return $this->getAssoc($request);
	   
	   
   }
   
   public function postCodeType(Request $request){
	   
	   
	   return $this->getRefcode($request);
	   
	   
   }
   
   public function postFuelType(Request $request){
	   
	   
	   return $this->getFuelType();
	   
	   
   }
   
  
   
   
  public function getDesignApi(Request $request){
	   
	

	   return $this->getDesign($request);
	   
	   
   } 


   ///this is to insert dummy records from mysql to mongodb
   public function getLocations(Request $request) {
	  
	$skip=$request->input('skip');
	
	
	
	 
	$rec=DB::table('USACANZIP')->take(500)->skip($skip)->get();
	
	if(count($rec)>0){
		$this->CreatePostcode($rec);
	}else{
		exit;
	}
	echo $skip=$skip+500;

	echo '<script>window.location.href="'.url('/front/services/locations?skip='.$skip).'"</script>';
	exit;
	
    
    } 
    
     public function postCarrierApi(Request $request){
	  
	  return $this->getCarrierAPI($request);
	   
	   
   }
}
