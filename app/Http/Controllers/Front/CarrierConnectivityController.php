<?php namespace App\Http\Controllers\Front;
	
	use App\Http\Controllers\Front\Controller;
	use App\Layout;
	use App\CarrierAccessorialTariff;
	use App\CarrierFuelTariff;
	use App\Traits\UtilityTrait;
	
	class CarrierConnectivityController extends Controller{
		use UtilityTrait;
		
		public function anyIndex(){
			return view('connectivity.home');
		}
		
		public function getAddCarrier(){
			return view('connectivity.add_carrier');
		}
		
		public function getRates(){
			return view('connectivity.rates');
		}
		
		public function anyEquipmentDashboard(){
			return view('connectivity.equipment_dashboard');
		}
		
		public function anyListCarrierAccessorials($carrierId){
			$allFields = Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierAccessorialSetup'));
			
			//getting carrier info
			$carrier = \App\Carrier::where('id', $carrierId);
			
			if(!$carrier->exists())
				abort(404, 'The requested page for the carrier could not be found');
			
			
			return view('connectivity.index_acccessorial')
				->with('allFields',$allFields)
				->withCarrierId($carrierId)
				->withCarrier($carrier->first(['id', 'carrier_name', 'carrier_logo']));
		}
		
		public function getAddAccessorial($carrierId){
			//getting carrier info
			$carrier = \App\Carrier::where('id', $carrierId);
			
			if(!$carrier->exists())
				abort(404, 'The requested page for the carrier could not be found');
			
			return view('connectivity.add_acccessorial')
				->withCarrierId($carrierId)
				->withCarrier($carrier->first(['id', 'carrier_name', 'carrier_logo']));
		}
		
		public function getEditAccessorial($id){
			$carrier = CarrierAccessorialTariff::where('id', $id);
			if($carrier->exists($id)){
				$carrierId = $carrier->pluck('carrierId'); 
				
				
			return view('connectivity.edit_acccessorial')
				->withCarrierId($carrierId)
				->withId($id)
				->withCarrier(\App\Carrier::find($carrierId, ['id', 'carrier_name', 'carrier_logo']));
			}else
				abort(404, 'The requested record was not found');
		}
		
		public function anyDownloadAccessorials($carrierId){
			//passing the columns which I want from the result set. Useful when we have not selected required fields
			$allFields = Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierAccessorialSetup'));
			
			$arrColumns = (count($allFields) && $allFields->count() && method_exists($allFields, 'lists')) ? 
				$allFields->lists('displayField'): [];          
			//dd($arrColumns);
			
			$arrFirstRow = array('Carrier SCAC', 'Carrier Name', 'Accessorial', 'Min Rate', 'Max Rate', 'Rate Type', 'Effective From');
			
			//$data = \CustomerRepository::getChildren()
				//	->get($arrColumns)
					//->toArray();

			$data = \App\CarrierAccessorialTariff::where('carrierID', $carrierId)
						->where('clientID', \CustomerRepository::getCurrentCustomerID())
						->with([
						'accsType',
						'rate',
						'carrier',
					])->get();
					
			$payload = [];
			foreach($data as $item){
				$row = array();
				foreach($arrColumns as $column){
					if($column == 'rate.ratetype')
						$row[$column] = isset($item->rate->ratetype) ? $item->rate->ratetype : 'Unknown';
					else if($column == 'carrier.scac')
						$row[$column] = isset($item->carrier->scac) ? $item->carrier->scac : 'Unknown';
					else if($column == 'carrier.carrier_name')
						$row[$column] = isset($item->carrier->carrier_name) ? $item->carrier->carrier_name : 'Unknown';
					else if($column == 'accs_type.accsname')
						$row[$column] = isset($item->accsType->accsname) ? $item->accsType->accsname : 'Unknown';
					else
						$row[$column] = isset($item->$column) ? $item->$column : 'Unknown';
				}
				
				$payload[] = $row;
			}
			//dd($payload);
			  
			  // building the options array
			  $options = array(
				  'columns' => $arrColumns,
				  'firstRow' => $arrFirstRow,
				  'fileName' => 'accessorial_rates.csv',
			  );         
			  return $this->convertToCSV($payload, $options); 
		}
		
		
		public function anyListCarrierFuel($carrierId){
			$allFields = Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierFuelSetup'));
			
			//getting carrier info
			$carrier = \App\Carrier::where('id', $carrierId);
			
			if(!$carrier->exists())
				abort(404, 'The requested page for the carrier could not be found');
			
			
			return view('connectivity.index_fuel')
				->with('allFields',$allFields)
				->withCarrierId($carrierId)
				->withCarrier($carrier->first(['id', 'carrier_name', 'carrier_logo']));
		}
		
		public function getAddFuel($carrierId){
			//getting carrier info
			$carrier = \App\Carrier::where('id', $carrierId);
			
			if(!$carrier->exists())
				abort(404, 'The requested page for the carrier could not be found');
			
			return view('connectivity.add_fuel')
				->withCarrierId($carrierId)
				->withCarrier($carrier->first(['id', 'carrier_name', 'carrier_logo']));
		}
		
		public function getEditFuel($id){
			$carrier = CarrierFuelTariff::where('id', $id);
			if($carrier->exists($id)){
				$carrierId = $carrier->pluck('carrierId'); 
				
				
			return view('connectivity.edit_fuel')
				->withCarrierId($carrierId)
				->withId($id)
				->withCarrier(\App\Carrier::find($carrierId, ['id', 'carrier_name', 'carrier_logo']));
			}else
				abort(404, 'The requested record was not found');
		}
		
		public function anyDownloadFuel($carrierId){
			//passing the columns which I want from the result set. Useful when we have not selected required fields
			$allFields = Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierFuelSetup'));
			
			$arrColumns = (count($allFields) && $allFields->count() && method_exists($allFields, 'lists')) ? 
				$allFields->lists('displayField'): [];          
			//dd($arrColumns);
	 	 	
			$arrFirstRow = array('Carrier SCAC', 'Carrier Name', 'Fuel Type', 'From ($)', 'To ($)', 'LTL (%)', 'TL (%)', 'Effective From');
			
			//$data = \CustomerRepository::getChildren()
				//	->get($arrColumns)
					//->toArray();

			$data = CarrierFuelTariff::where('carrierID', $carrierId)
						->where('clientID', \CustomerRepository::getCurrentCustomerID())
						->with([
							'fuelType' => function($query){
								$query->select(['id','fuelType']);
							}, 
							'carrier' => function($query){
								$query->select(['id','scac','carrier_name']);
							}
						])->get();
					
			$payload = [];
			foreach($data as $item){
				$row = array();
				foreach($arrColumns as $column){
					if($column == 'fuel_type.fuelType')
						$row[$column] = isset($item->fuelType->fuelType) ? $item->fuelType->fuelType : 'Unknown';
					else if($column == 'carrier.scac')
						$row[$column] = isset($item->carrier->scac) ? $item->carrier->scac : 'Unknown';
					else if($column == 'carrier.carrier_name')
						$row[$column] = isset($item->carrier->carrier_name) ? $item->carrier->carrier_name : 'Unknown';
					else
						$row[$column] = isset($item->$column) ? $item->$column : 'Unknown';
				}
				
				$payload[] = $row;
			}
			//dd($payload);
			  
			  // building the options array
			  $options = array(
				  'columns' => $arrColumns,
				  'firstRow' => $arrFirstRow,
				  'fileName' => 'fuel_rates.csv',
			  );         
			  return $this->convertToCSV($payload, $options); 
		}
	}