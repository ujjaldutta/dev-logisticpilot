<?php

namespace App\Http\Controllers\Front;

//namespace Amitavroy\Utils;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Front\Controller;
use App\User;
use MongoClient;
use App\Layout;
use App\Traits\UtilityTrait;
use App\Traits\SearchServiceTrait;
use Illuminate\Pagination\Paginator;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;
use App\Models\Profile\MarkupModel;
use App\Models\Profile\RatematrixModel;
use App\Models\Profile\MatrixRatelaneModel;

use App\Models\Profile\RateresultModel;
use Session;

use App\Country;

class ProfilesearchController extends Controller {
	
		
      use SearchServiceTrait;

  public function __construct() {
      $this->middleware('auth');

      }

   
    public function anyIndex() {
	
       $userId = \Auth::user()->id;
        $checkLayout = Layout::where('userId', $userId)
                ->where('layoutId', \Config::get('AppLayout.customerApp'))
                ->count();
    
        if (!$checkLayout) {
            $allFields = Layout::whereNull('userId')->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        } else {
            $allFields = Layout::where('userId', $userId)->where('layoutId', \Config::get('AppLayout.customerApp'))->orderBy('displayOrder', 'ASC')->get(['id', 'displayName', 'displayField']);
        }
		///dd($checkLayout);
		//exit;
		
		RateresultModel::where('clientID',\CustomerRepository::getCurrentCustomerID())->delete();
		
		Session::put('activeThreads',array());
		/*
		$contracts= ContractModel::with(['ratelane' => function ($query) {
			$query->where('MoveType', '=', 'All');
			}])
		->where('ProfileId', '=', 'amit')
				->where('clientID', '=', '9')
				->where('EffectiveFrom', '<=', '04/12/2016')
				->where('EffectiveTo', '>=', '04/12/2016')->get();
		echo '<pre>'.print_r($contracts,true).'</pre>';
		exit;	
		
		$contracts= ContractModel::with(['ratelane' => function ($query)  {
		$query->where('cId','<>','');
			 $query->orWhere(function ($query) {
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "53402" ), 'exists', true)			
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "54618" ), 'exists', true);

                });
				$query->orWhere(function ($query) {
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "55419" ), 'exists', true)			
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "California" ), 'exists', true);

                })->orderBy('OriginalZipcode', 'ASC');

			},
			'matrixratelane' => function ($query) {
			$query->where('MoveType', '=', 'All');
			
			}])->where('ProfileId', '=', 'amit')
				->where('clientID', '=', '9')
				->where('EffectiveFrom', '<=', '04/12/2016')
				->where('EffectiveTo', '>=', '04/12/2016')->select(array('_id','carrierId','carriercode','RateType','ProfileId','ClientID','EffectiveFrom','EffectiveTo'))->get();
				
			echo '<pre>'.print_r($contracts,true).'</pre>';
			exit;*/	
						
        return view('profiles.search')->with('allFields', $allFields);
    }
    
    
    
     public function postSearch(Request $request){
		
		RateresultModel::where('clientID',\CustomerRepository::getCurrentCustomerID())->delete();
		Session::put('activeThreads',array());
		$result=$this->SearchProfile($request);
		
	 
		$request->session()->regenerateToken();
		return response()->json(['success' => true,'result'=>$result,'token'=>csrf_token()]);
		
	}
 
	public function postList(Request $request){
		
		$pid=Session::get('activeThreads');
		$searchId=$request->input('searchId');
		if(count($pid)>0){
			return response()->json(['success' => false,'msg'=>'Search In Progress','rest'=>$pid]);
		}
		
		
		$result=$this->SearchResult($request);
		if(count($result)<=0){
			return response()->json(['success' => true,'msg'=>'Till Now No Result Found!',"count"=>0]);
		}else{
			$result=array();
			
	
			//fuel accessorial call
			$stat=$this->FuelAccessorialStore($searchId);
			if($stat){
				$result=$this->SearchResult($request);
			}
		}
		
		
		return response()->json(['success' => true,'result'=>$result]);
		
	}
	
	
	

}
