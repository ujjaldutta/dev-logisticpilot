<?php

namespace App\Http\Controllers\Front;

use App\Layout;
use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\CarrierServices;
use App\Traits\UtilityTrait;
use App\Traits\CommonCarrierActionsTrait;
use App\Carrier;
use App\CarrierEquipment;
use App\CarrierRemit;

class CarrierController extends Controller {

    const   UPLOAD_DIRECTORY = 'uploads',
            BRAND_IMAGE_DIRECTORY = 'brands',
            CARRIER_IMAGE_DIRECTORY = 'carriers',
            DOCMANAGER_DIRECTORY = 'docmanager';

    use CarrierServices,
        UtilityTrait,
        CommonCarrierActionsTrait;

    const getValidCSVFileExt = 'csv';

    /* public function __construct() {
      $this->middleware('auth.front');
      } */

    public function anyIndex() {
        $userId = \Auth::user()->id;
        $layoutId = \Config::get('app.carriersLayoutID');

        $checkLayout = Layout::where('layoutId', $layoutId)
                        ->where('userId', $userId)->count();
        if (!$checkLayout) {
            $allFields = Layout::where('layoutId', $layoutId)
                    ->whereNull('userId')
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        } else {
            $allFields = Layout::where('layoutId', $layoutId)
                    ->where('userId', $userId)
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }
        return view('carriers.index')->with('allFields', $allFields);
    }

    public function anyAdd() {
        $userId = \Auth::user()->id;
        $layEquipId = \Config::get('app.carriersEquipmentLayoutID');
        $allFields = $this->getAllFields($layEquipId, $userId);

        $layRemitId = \Config::get('app.carriersRemitLayoutID');
        $remitFields = $this->getAllFields($layRemitId, $userId);

        return view('carriers.add')
                        ->with('allFields', $allFields)
                        ->with('remitFields', $remitFields);
    }

    public function getEdit($id) {
        $userId = \Auth::user()->id;
        $layEquipId = \Config::get('app.carriersEquipmentLayoutID');
        $allFields = $this->getAllFields($layEquipId, $userId);

        $layRemitId = \Config::get('app.carriersRemitLayoutID');
        $remitFields = $this->getAllFields($layRemitId, $userId);

        if (Carrier::where('id', $id)->count()) {
            return view('carriers.edit')
                            ->withCarrierId($id)
                            ->with('allFields', $allFields)
                            ->with('remitFields', $remitFields);
        } else {
            abort(404, 'Carrier not found');
        }
    }
	
	public function anyDashboard(){
		return view('carriers.dashboard');
	}

    /*
     * @returns json
     *
     */

    public function getAjaxSubCarrierLists(Request $request) {
        return $this->listCarriersForUser($request);
    }
	
    public function getAjaxSubCarrierListsData(Request $request) {
        return $this->listCarriersForUserData($request);
    }	

    public function deleteAjaxSubCarrierDelete(Request $request) {
        $carrierID = $request->get('carrID');
        return $this->deleteCarrier($carrierID);
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function getDownload() {
        //passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('scac', 'carrier_name', 'carrier_adr1', 'carrier_city', 'carrier_state', 'carrier_postal', 'carrier_email');
        $arrFirstRow = array('SCAC', 'Currier Name', 'Currier Address', 'City', 'State', 'Zip', 'Email');

        /* $data = \CustomerRepository::getChildren()
          ->get($arrColumns)
          ->toArray(); */

        $data = Carrier::get($arrColumns)
                ->toArray();

        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
            'fileName' => 'carriers_report.csv'
        );
        return $this->convertToCSV($data, $options);
    }

    public function postAjaxCreateNewCarrierData(Request $request) {
        try {
            if (!$request->has('carrId'))
                $carrier = new Carrier;
            else
                $carrier = Carrier::findOrFail($request->get('carrId'));

            $v = \Validator::make([
                        'scac' => $request->get('scac'),
                        'carrier_name' => $request->get('carrier_name'),], [
                        'scac' => 'required|alpha',
                        'carrier_name' => 'required|alpha'
            ]);
            //validating before user create
            if ($v->fails())
                return response()->json(['error' => $v->messages(),]);

            $carrier->scac = $request->get('scac');
            $carrier->carrier_name = $request->get('carrier_name');
            $carrier->carrier_adr1 = $request->get('carrier_adr1');
            $carrier->carrier_adr2 = $request->get('carrier_adr2');
            $carrier->carrier_mc = $request->get('carrier_mc');
            $carrier->carrier_email = $request->get('carrier_email');
            $carrier->carrier_phone = $request->get('carrier_phone');
            $carrier->fax = $request->get('fax'); // TODO: No exist in db
            $carrier->carrier_xrefcode = $request->get('carrier_xrefcode');

            $location = $this->_processGoogleLocation($request->get('location'), 'carrier_postal', 'carrier_city', 'carrier_state', 'carrier_country');
            $carrier->carrier_postal = isset($location['carrier_postal']) ? $location['carrier_postal'] : null;
            $carrier->carrier_city = isset($location['carrier_city']) ? $location['carrier_city'] : null;
            $carrier->carrier_state = isset($location['carrier_state']) ? $location['carrier_state'] : null;
            $carrier->carrier_country = isset($location['carrier_country']) ? $location['carrier_country'] : null;

            // TODO: Logo, carrier_rateapi, carrier_bookingapi, carrier_trackingapi, carrier_imageapi

            if ($carrier->save()) {
                /* now syncing user client map pivot table for this client to map 
                  to this logged in user
                 */

                //firstly detaching if already added this clientID works on when editing
                // \Auth::user()->clients()->detach($user->clientID);
                //attaching the current user id with the newly created or updated client id
                // \Auth::user()->clients()->attach($user->clientID);

                return response()->json([
                            'success' => 'Carrier created successfully',
                            'carrId' => $carrier->id
                ]);
            } else
                return response()->json(['error' => 'Carrier not created successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    public function getAjaxSubCarrierEdit(Request $request) {
        $carrierID = $request->get('carrID');
        return $this->getCarrierDetails($carrierID);
    }

    public function postAjaxSubCarrierEdit(Request $request) {
        $carrierID = $request->get('carrID');
        return $this->handleCarrierProfile($request, $carrierID);
    }

    public function postUpload(Request $request) {
        return $this->handleCarrierLogoUpload($request);
    }

    public function getAjaxGetInsuranceTypes() {
        return $this->getInsuranceTypes();
    }

    public function getAjaxGetCurrencyTypes() {
        return $this->getCurrencyTypes();
    }

    public function getAjaxGetCarrierTypes() {
        return $this->getCarrierTypes();
    }

    public function getAjaxGetModeTypes() {
        return $this->getModeTypes();
    }
	
    public function getAjaxGetCarrierList() {
        return $this->getCarriers();
    }	
	

    public function getAjaxCarrierInsurances(Request $request) {
        //$carrierID = $request->get('carrID');
        return $this->getCarrierInsurances($request);
    }

    public function postAjaxSaveInsurances(Request $request) {
        $carrierID = $request->get('carrID');
        return $this->saveInsurancesForCarrier($request, $carrierID);
    }

    public function getAjaxCarrierModes(Request $request) {
        //$carrierID = $request->get('carrID');
        return $this->getCarrierModes($request);
    }

    public function postAjaxSaveModes(Request $request) {
        $carrierID = $request->get('carrID');
        return $this->saveModesForCarrier($request, $carrierID);
    }

    public function getAjaxSubCarrierEquipmentsLists(Request $request) {
        return $this->listEquipments($request);
    }

    public function deleteAjaxSubEquipmentDelete(Request $request) {
        $equipID = $request->get('equipID');
        return $this->deleteEquipment($equipID);
    }

    public function getEquipmentDownload($carrierID) {
        //passing the columns which I want from the result set. Useful when we have not selected required fields
        if (isset($carrierID) && $carrierID != null) {
            $arrColumns = array('equip_no', 'equip_refridgerate', 'equip_weight', 'equip_tareweight', 'equip_ownername');
            $arrFirstRow = array('ISO Eqp. Code', 'Temp. Control', 'Max Gross Weight', 'Tare Weight', 'Owner Name');

            $data = CarrierEquipment::where('carrierId', $carrierID)
                    ->get($arrColumns)
                    ->toArray();
            // building the options array
            $options = array(
                'columns' => $arrColumns,
                'firstRow' => $arrFirstRow,
                'fileName' => 'carriers_equipments_report.csv'
            );
            return $this->convertToCSV($data, $options);
        }
    }

    public function getAjaxGetEquipTypes() {
        return $this->getEquipTypes();
    }

    public function getAjaxGetInsTypes() {
        return $this->getInsTypes();
    }

    public function getAjaxGetModTypes() {
        return $this->getModTypes();
    }
    
    public function getAjaxSubCarrierRemitsLists(Request $request) {
        return $this->listRemits($request);
    }

    public function deleteAjaxSubRemitDelete(Request $request) {
        $remitID = $request->get('remitID');
        return $this->deleteRemit($remitID);
    }
    
    public function getRemitDownload($carrierID) {
        //passing the columns which I want from the result set. Useful when we have not selected required fields
        if (isset($carrierID) && $carrierID != null) {
            $arrColumns = array('remit_name', 'remit_address1', 'remit_address2', 'remit_state', 'remit_city');
            $arrFirstRow = array('Remit Name', 'Address 1', 'Address 2', 'State', 'City');

            $data = CarrierRemit::where('carrierId', $carrierID)
                    ->get($arrColumns)
                    ->toArray();
            // building the options array
            $options = array(
                'columns' => $arrColumns,
                'firstRow' => $arrFirstRow,
                'fileName' => 'carriers_remits_report.csv'
            );
            return $this->convertToCSV($data, $options);
        }
    }

    // ----------

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $action = $request->get('action');
        switch ($action) {
            case 'getCarrierLiablityInsurance':
                return $this->getCarrierLiablityInsurance($request);
            default:
                return response()->json(['error' => 'Required action is missing or not matching']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        //detecting the request for which step
        $step = $request->get('action');

        switch ($step) {
            case 'saveQuote':
                return $this->saveQuoteForCarrier($request);
            case 'uploadAccesCsv':
                return $this->uploadCustomAccsCsv($request);
            case 'uploadFuelCsv':
                return $this->uploadCustomFuelCsv($request);
            default:
                throw new \Exception('Unknown step to handle');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request) {
        $action = $request->get('type');
        //$id = $request->get('id');
        switch ($action) {
            default:
                throw new \Exception('Unknown step to handle');
        }
    }

}
