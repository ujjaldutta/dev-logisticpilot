<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\CodeReferenceServicesTrait;
use App\CodeReference;
use App\Layout;

class CodeReferenceController extends Controller {

    use CodeReferenceServicesTrait, UtilityTrait;
    
    public function anyIndex() {
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.codeReference'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField', 'fieldType']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.codeReference'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }
        return view('codereferences.index')->with('allFields', $allFields);
    }

    public function getAdd() {
        return view('codereferences.add');
    }

    public function getEdit($id) {
        if (!$this->getCodeDetails($id)->count())
            abort(404, 'code Reference not found');

        return view('codereferences.edit')->withId($id);
    }

    /*
     * @returns json
     *
     */

    public function getAjaxListCodes(Request $request) {
        return $this->listReferenceCodes($request, array());
    }

    public function postAjaxCodeEdit(Request $request) {
        try {
            $codeReferenceId = $this->saveCodeDetails($request);
            if ($codeReferenceId)
                return response()->json(['success' => 'Information saved successfully', 'codeReferenceId' => $codeReferenceId]);
            else
                return response()->json(['error' => 'Information not saved successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function getAjaxCodeEdit(Request $request) {
        try {
            $id = $request->get('id');

            $codeReference = $this->getCodeDetails($id);
            if ($codeReference)
                return response()->json(['success' => 'Information loaded saved successfully', 'codeReference' => $codeReference]);
            else
                return response()->json(['error' => 'Information not saved successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }
    
    public function getAjaxCodeTypes() {
        return $this->getCodeTypes();
    }

    /*
     * 
     * @returns json
     * @access public
     * @
     */

    public function deleteAjaxCodeDelete(Request $request) {
        try {
            $id = $request->get('id');

            if ($this->deleteCode($id))
                return response()->json(['success' => 'Information deleted successfully']);
            else
                return response()->json(['error' => 'Information not deleted successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function getDownload() {

        //passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('codeId', 'codeType', 'codeValue');

        $arrFirstRow = array('Code Id', 'Code Type', 'Code Value');

        //$data = ;

        $data = CodeReference::get($arrColumns)->toArray();
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
            'fileName' => 'code_references_report.csv'
        );
        return $this->convertToCSV($data, $options);
    }

}
