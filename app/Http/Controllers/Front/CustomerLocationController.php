<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\Controller;
use App\Traits\UtilityTrait;
use App\Traits\CustomerLocationServicesTrait;
use App\Traits\CustomerBillingServicesTrait;
use App\ClientLocation;
use App\Layout;

class CustomerLocationController extends Controller {

    use CustomerLocationServicesTrait,
        UtilityTrait, CustomerBillingServicesTrait; 

    public function anyIndex() {
        $allFields = Layout::where('userId', \Auth::id())
                ->where('layoutId', \Config::get('AppLayout.customerLocation'))
                ->orderBy('displayOrder', 'ASC')
                ->get(['id', 'displayName', 'displayField']);

        if (!count($allFields) || (count($allFields) && !$allFields->count())) {
            //dd($allFields);
            $allFields = Layout::whereNull('userId')
                    ->where('layoutId', \Config::get('AppLayout.customerLocation'))
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField']);
        }
        //dd($allFields);
        return view('clientlocations.index')->with('allFields', $allFields);
    }

    public function getAdd() {
        return view('clientlocations.add');
    }

    public function getEdit($id) {
        if (!$this->getLocationDetails($id)->count())
            abort(404, 'Customer not found');

        return view('clientlocations.edit')->withId($id);
    }

    /*
     * @returns json
     *
     */

    public function getAjaxListLocations(Request $request) {
        return $this->listLocations($request, array());
    }

	/** function to get customer billing info ******/
    public function getAjaxClientBillingInfo(Request $request) {
        return $this->listBillingInfo($request);
    }		
	
    public function getAjaxLocationTypes() {
        return response()->json(['success' => 'Location Types loaded successfully', 'locationTypes' => \App\LocationType::all(['location_type', 'id'])]);
    }

    public function getAjaxTimezones() {
        return response()->json(['success' => 'Time zones loaded successfully', 'timeZones' => \App\TimeZone::all(['name', 'id'])]);
    }

    public function postAjaxLocationEdit(Request $request) {
        try {
            $locationId = $this->saveLocationDetails($request);
            if ($locationId)
                return response()->json(['success' => 'Information saved successfully', 'locationId' => $locationId]);
            else
                return response()->json(['error' => 'Information not saved successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    public function getAjaxLocationEdit(Request $request) {
        try {
            $id = $request->get('id');

            $clientLocation = $this->getLocationDetails($id);
            if ($clientLocation)
                return response()->json(['success' => 'Information loaded saved successfully', 'clientLocation' => $clientLocation]);
            else
                return response()->json(['error' => 'Information not saved successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * 
     * @returns json
     * @access public
     * @
     */

    public function deleteAjaxLocationDelete(Request $request) {
        try {
            $id = $request->get('id');

            if ($this->deleteClientLocation($id))
                return response()->json(['success' => 'Information deleted successfully']);
            else
                return response()->json(['error' => 'Information not deleted successfully',]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(),]);
        }
    }

    /*
     * Method to create csv file for a given customer
     * @access public
     * @
     */

    public function getDownload() {

        //passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('location_code', 'location_name', 'location_adr1', 'location_city', 'location_state', 'location_postal', 'location_country', 'location_contact', 'location_email', 'location_hoursfrom', 'location_hoursto');

        $arrFirstRow = array('Location Code', 'Location Name', 'Customer Address', 'City', 'State', 'Postal Code', 'Country', 'Contact Name', 'Email', 'Hours From', 'Hours To');

        //$data = \CustomerRepository::getChildren()
        //	->get($arrColumns)
        //->toArray();

        $data = ClientLocation::where('clientId', \CustomerRepository::getCurrentCustomerID());

        $data = $data->get($arrColumns)->toArray();
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        return $this->convertToCSV($data, $options);
    }

}
