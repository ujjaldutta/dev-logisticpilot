<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url('/'));
});

Breadcrumbs::register('dashboard', function($breadcrumbs)
{
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Dashboard', url('/'));
});

// Home > About
Breadcrumbs::register('admin', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Admin', url('/'));
});

// Home > Blog
Breadcrumbs::register('setup_customer_location', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Setup Customer Location', url('/front/customer-locations'));
});
Breadcrumbs::register('setup_customer', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Setup Customer', url('/front/customers'));
});
Breadcrumbs::register('setup_user', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Setup User', url('/front/users'));
});
Breadcrumbs::register('add_customer', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_customer');
    $breadcrumbs->push('Add Customer', url('/front/customers/add'));
});
Breadcrumbs::register('add_user', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_user');
    $breadcrumbs->push('Add user', url('/front/users/add'));
});
Breadcrumbs::register('edit_customer', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_customer');
    $breadcrumbs->push('Edit Customer', url('/front/customers/edit/'));
});

Breadcrumbs::register('setup_carrier', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Setup Carrier', url('/front/carriers'));
});

Breadcrumbs::register('new_carrier', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Add Carrier', url('/front/carriers/add'));
});

Breadcrumbs::register('edit_carrier', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Edit Carrier', url('/front/carriers/edit'));
});

Breadcrumbs::register('new_carrierequipment', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Add Equipment', url('/front/carriers/equipments/add'));
});

Breadcrumbs::register('edit_carrierequipment', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Edit Equipment', url('/front/carriers/equipments/edit'));
});

Breadcrumbs::register('new_carrierremit', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Add Remit', url('/front/carriers/equipments/add'));
});

Breadcrumbs::register('edit_carrierremit', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_carrier');
    $breadcrumbs->push('Edit Remit', url('/front/carriers/equipments/edit'));
});

Breadcrumbs::register('add_customer_location', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_customer_location');
    $breadcrumbs->push('Add Customer Location', url('/front/customer-locations/add'));
});

Breadcrumbs::register('setup_customer_product', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Setup Customer Product', url('/front/customer-products'));
});

Breadcrumbs::register('add_customer_product', function($breadcrumbs)
{
    $breadcrumbs->parent('setup_customer_product');
    $breadcrumbs->push('Add Customer Product', url('/front/customer-products/add'));
});

// Home > Accounting
Breadcrumbs::register('accounting', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Accounting', url('#'));
});
Breadcrumbs::register('shop_rates', function($breadcrumbs)
{
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Shop Rate', url('/front/rates'));
});
Breadcrumbs::register('carrier_connectivity', function($breadcrumbs)
{
    $breadcrumbs->parent('accounting');
    $breadcrumbs->push('Setup Carrier Connectivity', url('/front/setup-connectivity'));
});
Breadcrumbs::register('carrier_equipment_dashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('carrier_connectivity');
    $breadcrumbs->push('Setup Carrier Equipments', url('/front/setup-connectivity/equipment-dashboard'));
});
Breadcrumbs::register('carrier_equipment_accessorial_list', function($breadcrumbs, $param)
{
    $breadcrumbs->parent('carrier_equipment_dashboard');
    $breadcrumbs->push('Setup Carrier Equipment Accessorial', url('/front/setup-connectivity/list-carrier-accessorials/'. $param));
});
Breadcrumbs::register('carrier_equipment_accessorial_add', function($breadcrumbs, $param)
{
    $breadcrumbs->parent('carrier_equipment_accessorial_list', $param);
    $breadcrumbs->push('Setup Carrier Equipments Accessorial Add', url('/front/setup-connectivity/add-accessorial'. $param));
});

Breadcrumbs::register('carrier_equipment_fuel_list', function($breadcrumbs, $param)
{
    $breadcrumbs->parent('carrier_equipment_dashboard');
    $breadcrumbs->push('Setup Carrier Equipment Fuel', url('/front/setup-connectivity/list-carrier-fuel/'. $param));
});
Breadcrumbs::register('carrier_equipment_fuel_add', function($breadcrumbs, $param)
{
    $breadcrumbs->parent('carrier_equipment_fuel_list', $param);
    $breadcrumbs->push('Setup Carrier Equipments Fuel Add', url('/front/setup-connectivity/add-fuel'. $param));
});

Breadcrumbs::register('add_carrier', function($breadcrumbs)
{
    $breadcrumbs->parent('carrier_connectivity');
    $breadcrumbs->push('Add Carrier', url('/front/setup-connectivity/add-carrier'));
});

// Home > Accounting
Breadcrumbs::register('update_profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Update Profile', url('/front/users/profile'));
});


