<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class TermsAuthenticationVerifier {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!$this->auth->user()){
			if($request->ajax())
				return response('Unauthorized.', 401);
			else
				return redirect()->guest('/auth/login');
		}
			
		
		if (empty($this->auth->user()->clientID))
		{
			return redirect()->to(\Config::get('app.frontPrefix'). '/terms/sys-terms');
			
		}else if ($this->auth->user()->usr_type_id != \Config::get('app.userAdminType') && !$this->auth->user()->usr_acceptedterms)
		{
			return redirect()->to(\Config::get('app.frontPrefix'). '/terms/app-terms');
		}

		return $next($request);
	}

}
