<?php namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Closure, Session;
use App\Customer;

class SetLocale {
	
	 public function handle($request, Closure $next)
    {
		 if(!\Auth::guest())
        {
			//var_dump(\CustomerRepository::getCustomerLocale());
			app()->setLocale(\CustomerRepository::getCustomerLocale());	  
        }
		
		

        return $next($request);
    }

}
