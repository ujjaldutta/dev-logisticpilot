<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true; //now all request are authorized for this time being
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$existingId = $this->get('id', null);
		return [
			//
			'usr_firstname' => 'required|alpha',
			'usr_lastname' => 'required|alpha',
			'email' => empty($existingId) ? "required|email|unique:m_users,email" :
				"required|email|unique:m_users,email,". $existingId,
		];
	}

}
