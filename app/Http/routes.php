<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index');

Route::any('/get-captcha', function() {
return response()->json(['captcha' => captcha_src()]);
});

/*
 * list of information provider or route handler for auto complete drop down placed on the registration steps
 */
Route::get('/list-countries', array('as' => 'countrySearch', 'uses' => 'LocationController@getCountryList'));
Route::get('/list-postcodes', array('as' => 'postalSearch', 'uses' => 'LocationController@getPostCodeList'));
Route::get('/list-states', array('as' => 'stateSearch', 'uses' => 'LocationController@getStateList'));
Route::get('/list-cities', array('as' => 'citySearch', 'uses' => 'LocationController@getCityList'));
Route::get('/list-carriers', array('as' => 'carrierSearch', 'uses' => 'CarrierController@getAjaxCarrierList'));

//route config for registration steps for a client from the front-end
Route::get('/reg-steps', ['as' => 'front.client.reg', 'uses' => 'HomeController@getRegistrationSteps']);



Route::resource('register', 'RegistrationController');


Route::controllers([
'auth' => 'Auth\AuthController',
 'password' => 'Auth\PasswordController',
 'plans' => 'Front\PlanController',
]);

//route to accept admin login
Route::get(Config::get('app.adminPrefix') . '/users/login', function() {
return View::make('admin.users.login');
});

Route::post(Config::get('app.adminPrefix') . '/users/login', array('uses' => 'Admin\UserController@postLogin'));

//Routing groups dedicated to handle administration modules
Route::group(array('prefix' => Config::get('app.adminPrefix'), 'middleware' => 'auth.admin'), function() {
Route::controller('users', 'Admin\UserController');
Route::controller('registrations', 'Admin\RegistrationController');
});

Route::group(array('prefix' => Config::get('app.frontPrefix'), 'middleware' => 'auth.front'), function() {
    Route::get('/list-clients', ['uses' => 'Front\CustomerController@getAjaxSearchClients']);
    Route::any('/set-customer/{id}', ['uses' => 'Front\CustomerController@anySetClientID']);
    Route::any('/reset-customer', ['uses' => 'Front\CustomerController@anyResetClientID']);
    Route::controller('terms', 'Front\TermsController');
    Route::get('registration-steps/{id}', ['uses' => 'HomeController@getAjaxGetRegistrationSteps']);
    Route::post('registration-steps/upload', ['uses' => 'Front\ClientRegisterController@uploadImage', 'as' => 'reg-step-upload-logo']);
    Route::resource('client-register', 'Front\ClientRegisterController');
    Route::get('/ajaxStep3AddCarrier', ['uses' => 'Front\ClientRegisterController@getAjaxStep3AddCarrier']);
    Route::post('/ajaxStep3AddCarrier', ['uses' => 'Front\ClientRegisterController@postAjaxStep3AddCarrier']);
    Route::post('/ajaxStep3AddCarrierImage', ['uses' => 'Front\ClientRegisterController@postAjaxUploadCarrierImage']);
    Route::controller('setup-connectivity', 'Front\CarrierConnectivityController');
    Route::controller('rates', 'Front\CarrierRatesController');
    Route::resource('quote-services', 'Front\QuoteController');
    Route::resource('carrier-service', 'Front\CarrierController');
    Route::controller('customers', 'Front\CustomerController');
    Route::controller('users', 'Front\UserController');

    Route::get('/carriers/edit/equipments/add/{carrierId}', ['uses' => 'Front\CarrierEquipmentController@getAdd']);
    Route::get('/carriers/edit/equipments/edit/{carrierId}/{equipmentId}', ['uses' => 'Front\CarrierEquipmentController@getEdit']);
    Route::get('/carriers/ajax-carrier-equipment-edit', ['uses' => 'Front\CarrierEquipmentController@getAjaxCarrierEquipmentEdit']);
    Route::post('/carriers/ajax-carrier-equipment-edit', ['uses' => 'Front\CarrierEquipmentController@postAjaxCarrierEquipmentEdit']);
    Route::get('/carriers/ajax-get-equipment-types', ['uses' => 'Front\CarrierEquipmentController@getAjaxGetEquipmentTypes']);
    Route::get('/carriers/ajax-get-owner-types', ['uses' => 'Front\CarrierEquipmentController@getAjaxGetOwnerTypes']);

    Route::get('/carriers/edit/remits/add/{carrierId}', ['uses' => 'Front\CarrierRemitController@getAdd']);
    Route::get('/carriers/edit/remits/edit/{carrierId}/{remitId}', ['uses' => 'Front\CarrierRemitController@getEdit']);
    Route::get('/carriers/ajax-carrier-remit-edit', ['uses' => 'Front\CarrierRemitController@getAjaxCarrierRemitEdit']);
    Route::post('/carriers/ajax-carrier-remit-edit', ['uses' => 'Front\CarrierRemitController@postAjaxCarrierRemitEdit']);
    Route::get('/carriers/ajax-get-curr-types', ['uses' => 'Front\CarrierRemitController@getAjaxGetCurrTypes']);
    Route::controller('carriers', 'Front\CarrierController');

    Route::controller('customer-locations', 'Front\CustomerLocationController');
    Route::controller('customer-products', 'Front\CustomerProductController');
    Route::controller('code-references', 'Front\CodeReferenceController');

    //route to doc manager upload
    Route::post('/ajaxUploadApiContract', ['uses' => 'Front\ClientRegisterController@postAjaxSaveContractFile']);
	Route::get('/shipment/list', ['uses' => 'Front\ShipmentController@getList']);
	Route::get('/shipment/add', ['uses' => 'Front\ShipmentController@addShipment']);
	


Route::controller('profiles', 'Front\ProfileController');
Route::controller('profilesearch', 'Front\ProfilesearchController');

Route::controller('services', 'Front\LookupServiceController');
});



Route::Controller('/front/user', 'Front\UserController');
