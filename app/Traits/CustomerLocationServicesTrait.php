<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\ClientLocation;
use App\LocationType;

trait CustomerLocationServicesTrait {
		
	protected function listLocations(Request $request, array $fields=array()){
	    if($request->get('autocomplete') == 'true')
	    {
		    $search = $request->get('q');
			$clientID = $request->get('clientID');
			//
			//if the sent client id is blank get the system set customer ID
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			$customerLocations = ClientLocation::where('clientId', $clientID);	
			if(!empty($search) && strlen($search) > 1)
				$customerLocations->where('location_name', 'LIKE', "%{$search}%");	

			$customerlocations = $customerLocations->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'location_name', 'clientId', 
				'location_adr1', 'location_adr2',
				'location_city', 'location_state',
				'location_postal', 'location_hoursfrom',
				'location_hoursto', 'created_at',
				/*'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($customerlocations as $key => $customerlocation){
				$processedData [$key] = [
					'id' => $customerlocation->id,
					'clientId' => $customerlocation->clientId,
					'location_name' => $customerlocation->location_name,
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,*/
					'description' => $customerlocation->location_adr1. ', '. $customerlocation->location_adr2. ', '. $customerlocation->location_city. ', '. $customerlocation->location_state. ', '.  $customerlocation->location_postal,
				] + $customerlocation->toArray();
			}
			
			return response()->json($processedData);
				
		}
	     else {
		//processing filters and limits and other params
		$filters = $request->get('filters');
		$filters = json_decode($filters);
		//print_r($filters);
		//limit
		$orderBy = $request->get('orderBy', null);
		$orderType = $request->get('orderType', null);
		$limit = $request->get('limit', 1);
		$page = $request->get('page', 1);
		$offset = ($page - 1) * $limit;
		
		$clientID = $request->get('clientID');
		//
		//if the sent client id is blank get the system set customer ID
		if(empty($clientID) || !is_numeric($clientID))
			$clientID = \CustomerRepository::getCurrentCustomerID();
		
		$customerLocations = ClientLocation::where('clientId', $clientID);
		
		 foreach($filters as $filter){			
			if(isset($filter->name)){
				$customerLocations->where('location_name', 'LIKE', "%{$filter->name}%");
			}
			if(isset($filter->location)){
				if(isset($filter->location->postal_code)){
					$customerLocations->where('location_postal', $filter->location->postal_code);	
				}
				if(isset($filter->location->city)){
					$customerLocations->where('location_city', $filter->location->city);	
				}
				if(isset($filter->location->state)){
					$customerLocations->where('location_state', $filter->location->state);	
				}
				if(isset($filter->location->country)){
					$customerLocations->where('location_country', $filter->location->country);	
				}
			}
			
			if(isset($filter->status)){
				if((isset($filter->status->active) && $filter->status->active == 1) && 
					(!isset($filter->status->inactive) || (isset($filter->status->inactive) && $filter->status->inactive == ''))
				){
					$customerLocations->where('location_active', 1);
				}else if((isset($filter->status->inactive) && $filter->status->inactive == 1) && 
					(!isset($filter->status->active) || (isset($filter->status->active) && $filter->status->active == ''))
				){
					$customerLocations->where('location_active', 0);
				}
			}
		};
		
		
		$countcustomerLocations =  $customerLocations->count() ;
		
		if(!empty($orderBy) && !empty($orderType))
			$customerLocations->orderBy($orderBy, $orderType);
		else
			$customerLocations->orderBy('id', 'asc');
		
		$clientData = $customerLocations->take($limit)->skip($offset);
		
		if(count($fields))
			$clientData = $clientData->get(array_merge(['id'], $fields));
		else
			$clientData = $clientData->get();
        // dd($clientData->toArray());
		return response()->json(['success' => 'Clients loaded successfully', 'count' => $countcustomerLocations, 'results' => $clientData]);
	  }
	}
	
	protected function saveLocationDetails(Request $request){
		$clientId = $request->get('clientId');
		
		if(empty($clientId) || !is_numeric($clientId))
			throw new \Exception('Invalid client / customer id provided');
		
		if(!$request->has('id')){
			$clientLocation = new ClientLocation;
			$clientLocation->location_code = isset(\CustomerRepository::getCurrentCustomer()['client_compname']) ? 
			$this::generateClientLocationCode(\CustomerRepository::getCurrentCustomer()['client_compname'], $request->get('location_name'), ClientLocation::count()) : null;
		}else
			$clientLocation = ClientLocation::findOrFail($request->get('id'));
		
		$clientLocation->fill($request->except(['clientId']));
		$clientLocation->clientId = $clientId;
		
		$location = $this->_processGoogleLocation($request->get('location_location'), 'location_postal', 'location_city', 'location_state', 'location_country');
		
		$clientLocation->location_postal = isset($location['location_postal']) ? $location['location_postal'] : $clientLocation->location_postal;
		$clientLocation->location_city = isset($location['location_city']) ? $location['location_city'] : $clientLocation->location_city;
		$clientLocation->location_state = isset($location['location_state']) ? $location['location_state'] : $clientLocation->location_state;
		$clientLocation->location_country = isset($location['location_country']) ? $location['location_country'] : $clientLocation->location_country;
		
		$clientLocation->location_createdbyId = \Auth::id();
		
		//processing start and end time
		//$startTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursfrom'));
		$startTime = new \DateTime($request->get('location_hoursfrom'));
		//dd($request->get('location_hoursfrom'));
		$clientLocation->location_hoursfrom = $startTime ? $startTime->format('H:i:s') : null;
		
		//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
		$endTime = new \DateTime($request->get('location_hoursto'));
		$clientLocation->location_hoursto = $endTime ? $endTime->format('H:i:s') : null;
		
		
		return $clientLocation->save() ? $clientLocation->id : false;
	}
	
	protected function getLocationDetails($id){
		$clientLocationDetails = ClientLocation::findOrFail($id);
		
		$startTime = \DateTime::createFromFormat('y-m-d H:i:s', date('y-m-d'). ' '. $clientLocationDetails->location_hoursfrom);
		$endTime = \DateTime::createFromFormat('y-m-d H:i:s', date('y-m-d'). ' '. $clientLocationDetails->location_hoursto);
		
		
		
		$clientLocationDetails->location_hoursfrom = $startTime ? $startTime->format(\DateTime::RFC2822) : null;
		$clientLocationDetails->location_hoursto = $endTime ? $endTime->format(\DateTime::RFC2822) : null;
		
		return $clientLocationDetails;
	}
	
	protected function deleteClientLocation($id){
		$locationDetais = ClientLocation::findOrFail($id);
		
		return $locationDetais->delete();
	}
	
	/* method to generate the client code based on parent company name */
	public static function generateClientLocationCode($parentCompanyName, $currentLocationName, $currentCount){
		$compnameInitial = strtoupper(substr($parentCompanyName, 0, 1));
		$locNameInitial = strtoupper(substr($currentLocationName, 0, 1));
		$nextCount = sprintf("%08d", $currentCount +1);
		return $compnameInitial.$locNameInitial.$nextCount;
	}
}