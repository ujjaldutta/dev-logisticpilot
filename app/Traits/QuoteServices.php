<?php
namespace App\Traits;

use Illuminate\Http\Request;
use App\Quote;
use App\QuoteCost;
use App\QuoteProduct;

trait QuoteServices{
	/*
	* Method to save a quote for a carrier depending on the searched params
	*
	*/
	protected function saveQuoteForCarrier(Request $request){
		try{
			//getting the data and processing them
			
			//getting quote data
			$quoteData = [];
			foreach($request->all() as $key => $value){
				if(preg_match('/^shp_/', $key)){
					$quoteData[$key] = $value;
				}
			}
			
			/* $costData = [];
			foreach($request->all() as $key => $value){
				if(preg_match('/^cost_/', $key)){
					$costData[$key] = $value;
				}
			} */
			
			//now first saving the quote data to quoteMaster
			$quote = new Quote();
			$quote->fill($quoteData);
			$quote->clientId = \Auth::user()->clientID;
			$quote->shp_createduserId = \Auth::id();
			$quote->shp_quotedate = \Auth::id();
			
			if($quote->save()){
				//now processing the cost data
				if($request->get('costs')){
					$costs = [];
					foreach($request->get('costs') as $cost){
						$costs[] = new QuoteCost($cost);
					}
					
					$quote->costs()->saveMany($costs);	
				}
				
				
				//saving product details
				if($request->get('products')){
					$products = [];
					foreach($request->get('products') as $product){
						//dd($product);
						//type converting hazmat items for db saving
						if(isset($product['prd_itemhazmat']) && ($product['prd_itemhazmat']== true || $product['prd_itemhazmat'] == 'true'))
							$product['prd_itemhazmat'] = 1;
						else
							$product['prd_itemhazmat'] = 0;
						//type converting hazmat items for db saving
						if(isset($product['prd_stackable']) && ($product['prd_stackable'] == true || $product['prd_stackable'] == 'true'))
							$product['prd_stackable'] = 1;
						else
							$product['prd_stackable'] = 0;
						
						$products[] = new QuoteProduct($product);
					}
					
					$quote->products()->saveMany($products);
				}
				return response()->json(['success' => 'Saving quote completed successfully', 'id' => $quote->id]);
			}else{
				return response()->json(['error' => 'Saving quote failed']);
			}
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
}