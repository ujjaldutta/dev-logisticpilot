<?php
namespace App\Traits;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\CustomerWishList;
use App\ClientBillingInfo;
use App\ClientDefaultSettings;
use App\ClientCarrierContact;
use App\Carrier;
use App\CarrierAPI;
use App\ApiDetail;
use App\CarrierAccessorialTariff;
use App\CarrierFuelTariff;
use App\DocManager;
use App\SubscriptionPlan;

#request validators
use App\Http\Requests\CustomerDefaultSettingsRequest;

trait RegistrationSteps{
	
	private $_carrierGroupByColumns = ['carrierId', 'fuelType', 'accsID', ];
	
	protected function handleClientProfile(Request $request, $custID = null){
		try{
			//dd($request->only(['wish_list.wl_message']));
			//updating customer information after update
			$clientId = is_numeric($custID) ? $custID : $request->get('clientID');
			
			//preocessing location for this client profile submitted using Google location details
			$location = $request->get('location', []);
			$location = $this->_processGoogleLocation($location); 
			
			$validator = \Validator::make($request->all(), Customer::getGeneralValidationRules(['id' => $clientId]));
			
			if($validator->fails())
				return response()->json(['error' => $validator->messages()]);
			
			if(!is_numeric($clientId) || !Customer::where('id', $clientId)->count()){ 
				//checking if still the client id is not null otherwise creating new record
				//throw new \Exception('Required client ID is missing');
				//firstly generating matching user for this customer
				
				//generating new customer code
				$count = Customer::where('parent_id', \CustomerRepository::getCurrentCustomerID())->count();
				
				$customer = new Customer;
				$customer->parent_id = \CustomerRepository::getCurrentCustomerID();
				$customer->client_type = $this::ClienTypeSubClient;
				
				$customer->client_code = isset(\CustomerRepository::getCurrentCustomer()['client_compname']) ? 
					$this::generateClientCode(\CustomerRepository::getCurrentCustomer()['client_compname'], $count) : null;				
				$customer->client_actdate = new \DateTime('now');				
				
				
			}else{
				//fetching customer object for existing customer
				$customer = Customer::findOrFail($clientId);
			}
			
			
				
			$customer->fill(
				$location +
				$request->except([
					'_token', 'clientID', 
					'clientWLID', 'nextNotAllowed', 
					'saveButtonText', 'step', 
					'wl_message'
				])
			);
			
			//saving
			if(!$customer->save())
				throw new \Exception('Customer not saved successfully');
			
			$wlID = 0;
			//var_dump($request->only(['wish_list']));
			//querying customer wish-list table to see whether record exists
			if($customer->wishList()->count()){
				$wlID = $customer->wishList->id;
				$wishlist = $customer->wishList()->first();
			}else{
				$wishlist = new CustomerWishList;
				//$wl->wl_message = $request->get('wish_list.wl_message')[0];
			}
				$wishlist->fill($request->only(['wish_list'])['wish_list']);
				$customer->wishList()->save($wishlist);
				
				$wlID = $customer->wishList->id;				
			return response()->json(['success' => 'Customer profile updated successfully', 'clientWLID' => $wlID, 'custId' => $customer->id]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function deleteClient($clientID){
		try{
			$client = Customer::findOrFail($clientID);
			if($client->delete())
				return response()->json(['success' => 'Customer profile deleted successfully']);
			else
				return response()->json(['error' => 'Customer profile not deleted successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function handleClientBillingInfo(Request $request){
		try{
			$clientID = $request->get('clientID');
			
			//preocessing location for this client profile submitted using Google location details
			$location = $request->get('location', []);
			$location = $this->_processGoogleLocation($location, 'billto_postal', 'billto_city', 'billto_state', 'billto_country');
			
			$customer = Customer::findOrFail($clientID);
			
			$billInfoID = 0;
			if($customer->billTo()->count()){
				$billInfoID = $customer->billTo->id;
				//updating exisitng records
				$billTo = $customer->billTo()->first();
				$billTo->update($location + $request->except(['_token', 'clientID', 'saveButtonText', 'step', 'location',]));
			}else{
				//creating new records
				$billingInfo = new ClientBillingInfo;
				$billingInfo->fill($location + $request->except(['_token', 'clientID', 'saveButtonText', 'step', 'location',]));
				$customer->billTo()->save($billingInfo);
				
				$billInfoID = $customer->billTo->id;
			}
			return response()->json(['success' => 'Customer billing info updated successfully', 'billInfoID' => $billInfoID]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function handleBrandFileUpload(Request $request){
		if($request->hasFile('file') && $request->has('clientID')){
			if($request->file('file')->isValid()){
				//validating the image file
				$v = \Validator::make(
					$request->all(), 
					['file' => 'required|image', 'clientID' => 'required|numeric']
				);
				//checking
				if($v->fails())
					return response()->json(['error' => $v->errors()->all()]);
				try{
					//finding the customer who is going to update the logo
					$cust = Customer::findorFail($request->get('clientID'));
					$oldLogo = isset($cust->wishList->wl_logo) ? $cust->wishList->wl_logo : null;
					
					//uploading new logo and cleaning the old one if any
					$result = $this->_handleUploadAndResize($request, 'brand_logo', 282, 53, true, $oldLogo);
					
					if(isset($result['success']) && $result['success']){
						if($cust->wishList()->count()){
							$wl = $cust->wishList()->first();
							$wl->update(['wl_logo' => $result['fileName']]);
						}else{
							$wl = new CustomerWishList;
							$wl->fill(['wl_logo' => $result['fileName']]);
							$cust->wishList()->save($wl);
						}
						return response()->json([
							'success' => 'File uploaded successfully', 
							'size' => isset($result['size']) ? $result['size']: 0,
							'fileName' => isset($result['fileName']) ? $result['fileName']: 0, 
							'url' => isset($result['url']) ? $result['url'] : null
						]);
					}else
						return response()->json(['error' => 'File uploaded but not moved and saved']);
				}catch(\Exception $e){
					return response()->json(['error' => $e->getMessage()]);
				}
			}else
				return response()->json(['error' => 'No valid file uploaded']);
		}else
			return response()->json(['error' => 'required information missing']);
	}
	
	protected function handleCarrierLogoUpload(Request $request){
		if($request->hasFile('file') && $request->has('id')){
			if($request->file('file')->isValid()){
				//validating the image file
				$v = \Validator::make(
					$request->all(), 
					['file' => 'required|image',]
				);
				//checking
				if($v->fails())
					return response()->json(['error' => $v->errors()->all()]);
				try{
					//creating or updating the logo
					$id = $request->get('id');
					if(!empty($id)){
						$carrier = Carrier::findOrFail($id);
					}else
						$carrier = new Carrier;
					
					//handling file upload and removal of the old one if any
					$oldFile = isset($carrier->carrier_logo) ? $carrier->carrier_logo : null;
					
					$result = $this->_handleUploadAndResize($request, 'carrier', 64, 64, true, $oldFile);
					
					if(isset($result['success']) && $result['success']){
						$carrier->carrier_logo = $result['fileName'];
						
						if($carrier->save())
							return response()->json(['success' => 'File uploaded and updated successfully', 'id' => $carrier->id]);
						else
							return response()->json(['error' => 'File not uploaded or updated successfully', ]);
					}
				}catch(\Exception $e){
					return response()->json(['error' => $e->getMessage()]);
				}
			}else
				return response()->json(['error' => 'No valid file uploaded']);
		}else
			return response()->json(['error' => 'required information missing']);
	}
	
	protected function handleCarrierCreation(Request $request){
		if($request->has('id')){
			try{
				$id = $request->get('id');
				//validating the image file
				$v = \Validator::make(
					$request->all(), 
					[
						'scac' => empty($id) ? 'required|unique:m_carrier,scac' : 'required|unique:m_carrier,scac,'.$id ,
						'carrier_name' => 'required',
						'carrier_email' => 'required|email',
					]
				);
				
				//preocessing location for this client profile submitted using Google location details
				$location = $request->get('location', []);
				$location = $this->_processGoogleLocation($location, 'carrier_postal', 'carrier_city', 'carrier_state', 'carrier_country');
				
				//checking
				if($v->fails())
					return response()->json(['error' => $v->errors()->all()]);
				
				//creating or updating the logo
				if(!empty($id)){
					$carrier = Carrier::findOrFail($id);
				}else
					$carrier = new Carrier;
				
				$carrier->fill($location + $request->except(['id', 'location']));
				
				if($carrier->save())
					return response()->json(['success' => 'Record inserted successfully', 'id' => $carrier->id]);
				else
					return response()->json(['error' => 'File not uploaded or updated successfully', ]);
				
			}catch(\Exception $e){
					return response()->json(['error' => $e->getMessage()]);
			}
		}else
			return response()->json(['error' => 'required information missing']);
	}
	
	private function _handleUploadAndResize(Request $request, $destinationType, 
		$resizeWidth, $resizeHeight, $removeOld = false, $removeFile = null
	){
		try{
			//generating random file name for this
			$ext = $request->file('file')->getClientOriginalExtension();
			$fileName = uniqid(). '.' . $ext;
			$fileSize = $request->file('file')->getSize();
			//moving the file to its destination
			if($request->file('file')
				->move(self::getUploadPath($destinationType), $fileName)
			){
				
				if(in_array($ext,['jpg', 'jpeg', 'gif', 'png'])){
					//firstly cropping the image to fit using image library façade
					\Image::make(self::getUploadPath($destinationType). $fileName, array(
						'width' => $resizeWidth,
						'height' => $resizeHeight,
						//'grayscale' => true
					))->save(self::getUploadPath($destinationType). $fileName);
				}
				
				//now copying this file to the aws s3 bucket
				if(!$this->copyToS3Bucket(self::getUploadPath($destinationType). $fileName, 
					$fileName, self::getS3UploadFolder($destinationType)
				))
					throw new \Exception('copying to S3 bucket failed');
				
				//removing the old file if any
				if($removeOld && !empty($removeFile)){
					$this->_deleteFile($removeFile, $destinationType, true); //true to remove that resource from s3 bucket
				}
				
				return [
					'success' => true, 
					'size' => $fileSize, 
					'fileName' => $fileName, 
					'url' => self::getAssetUrl($fileName, $destinationType),
					'path' => self::getUploadPath(),
					'filetype' => $ext,
				];
			}else
				return ['success' => false, ];
		}catch(\Exception $ex){
			throw new \Exception ($ex->getMessage());
		}
	}
	
	/*
	* Method to delete files from local file system depending on various location types 
	*
	*/
	private function _deleteFile($removeFileName, $destinationType, $removeFromS3Bucket = false){
		try{
			//removing the old file if any	
			$removeFile = self::getUploadPath($destinationType). $removeFileName;
			
			//deleting from s3 if switched on
			if($removeFromS3Bucket){
				if(!$this->deleteFromS3Bucket($removeFileName, self::getS3UploadFolder($destinationType)))
					throw new \Exception('Deleting from s3 failed');
			}
			
			//deleting
			if(!is_dir($removeFile) && file_exists($removeFile))
				return unlink($removeFile);
			else
				return false;
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	protected function deleteFromS3Bucket($file, $folder = null){
		try{
			$disk = \Storage::disk('s3');
			$path = empty($folder) ? $file : $folder. '/'. $file;
			
			if($disk->delete($path))
				return true;
			else
				return false;
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	protected function copyToS3Bucket($fileLocation, $fileName, $s3BucketFolder = null){
		try{
			$disk = \Storage::disk('s3');
			//copying the file to s3 bucket
			if(!empty($s3BucketFolder))
				$destinationFile = $s3BucketFolder. '/'. $fileName;
			else
				$destinationFile =  $fileName;
			//copying
			//getting contents of the local file
			
			$contents = file_get_contents($fileLocation);
			if($disk->put($destinationFile, $contents))
				if(!$disk->exists($destinationFile))
					throw new \Exception('Uploading to S3 bucket has failed');
				else
					return true;
			else
				return false;
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	/*
	* Method to handle step 3 which saved multiple carriers
	*
	*/
	public function handleCarrierList(Request $request){
		$carrier_ids = $request->get('carriers');
		if(!empty($carrier_ids) && count($carrier_ids)){
			try{
				//checking for a valid client id
			//	$clientId = \Auth::user()->clientID;
				$clientId = \CustomerRepository::getCurrentCustomerID();
				if(empty($clientId))
					throw new \Exception('The required client id is missing for this user');
				
				//finding and attaching the carriers for this client
				$customer = Customer::findOrFail($clientId);
				$customer->carriers()->sync($carrier_ids);
				
				return response()->json(['success' => 'Carriers Saved Successfully', ]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()]);
			}
		}
	}
	
	public function listSelectedCarriers($withSetupDetails = false, $searchString = null, $onlyApiEnabled = false, $limit = null, $clientID=null,$ProfileId=null){
		try{
			
			//checking for a valid client id
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			/*if(!is_numeric($clientID))
				throw new \Exception('The required client id is missing or invalid for this user');
			*/
			
			$customer = Customer::where('id', $clientID)
				->with([
					'carriers' => function($query) use ($searchString, $onlyApiEnabled, $limit){
						
						if(!empty($searchString)){
							$query->where('carrier_name', 'LIKE', "{$searchString}%");
						}
						
						if((bool)$onlyApiEnabled == true){
							//$query->where('carrier_apienabled', 1);
						}
						
						$query->orderBy('carrier_name', 'asc');
						
						/* if(!empty($limit))
							$query->take($limit);
						*/
					},
				])
				->first(['id']);
			
			//processing data to desired format
			$formattedCarriers = array();
			$addedCarriersFlatList = [];
			$carrierCount = 0;
			if(count($customer->carriers) && $customer->carriers->count()){
				$carrierCount = $customer->carriers->count();
				foreach($customer->carriers as $carrier){
					if(isset($carrier->pivot) && $carrier->pivot->count()){
						//getting saved contact information for this carrier
						$contactInfo = [
							'contactName' => '',
							'contactEmail' => '',
							'contactPhone' => '',
							'assignmentId' => 0 
						];
						
						$setupInfo = [];
						
						if(isset($carrier->pivot->id)){
							//if setup details requested then fetched listed setup details for this carrier of this client
							if($withSetupDetails){
								if($ProfileId !=''){
								$carrierSetup = CarrierAPI::where('carrierID', $carrier->id)
									->where('clientID', $clientID)
									->where('contract_profilecode', $ProfileId)
									->with(['contract' => function($query){
										$query->select(['id', 'doc_filename', 'doc_path']);
									}])
									->first(['id', 'rate_docID', 
										'rate_apiuserid', 'rate_apiaccount', 
										'rate_apipwd', 'rate_apiactivated',
										'rate_apishiptype', 'rate_apipaytype',
									]);
									
								}else{
									$carrierSetup = CarrierAPI::where('carrierID', $carrier->id)
									->where('clientID', $clientID)
									
									->with(['contract' => function($query){
										$query->select(['id', 'doc_filename', 'doc_path']);
									}])
									->first(['id', 'rate_docID', 
										'rate_apiuserid', 'rate_apiaccount', 
										'rate_apipwd', 'rate_apiactivated',
										'rate_apishiptype', 'rate_apipaytype',
									]);
								}
								if(count($carrierSetup) && $carrierSetup->count()){
									$setupInfo = [
										'setupId' => $carrierSetup->id,
										'apiUserName' => $carrierSetup->rate_apiuserid,
										'apiPassword' => $carrierSetup->rate_apipwd,
										'accountNumber' => $carrierSetup->rate_apiaccount,
										'apiShipType' => $carrierSetup->rate_apishiptype,
										'apiPayType' => $carrierSetup->rate_apipaytype,
										'connectivity' => $carrierSetup->rate_apiactivated,
										'contractFile' => isset($carrierSetup->contract->doc_filename) ? 
											$carrierSetup->contract->doc_filename : null,
										/* 'contractFilePath' => isset($carrierSetup->contract->doc_path) ?
											$carrierSetup->contract->doc_path : null, */
										'contractFileUrl' => isset($carrierSetup->contract->doc_filename) ?
											self::getAssetUrl($carrierSetup->contract->doc_filename, 'docmanager'): null,
										'contractFileId' => isset($carrierSetup->contract->id) ?
											$carrierSetup->contract->id : null,
									];
								}
							}
							$contactInfo['assignmentId'] = $carrier->pivot->id;
							$carrierClientInfo = ClientCarrierContact::where('clientcarrierID', $carrier->pivot->id)->first();
						
							if(count($carrierClientInfo) && $carrierClientInfo->count()){
								$contactInfo['contactName'] = $carrierClientInfo->cc_contactname;
								$contactInfo['contactEmail'] = $carrierClientInfo->cc_email;
								$contactInfo['contactPhone'] = $carrierClientInfo->cc_contactphone;
							}
						}
						
						//adding to flat list
						$addedCarriersFlatList[] = $carrier->id;
						//calculating the time from
						$timeFrom = \DateTime::createFromFormat('H:i:s', $carrier->carrier_hoursfrom);
						$timeFrom = $timeFrom ? $timeFrom->format('h:i a') : 'Unknown';
						//calculating the time to
						$timeTo = \DateTime::createFromFormat('H:i:s', $carrier->carrier_hoursto);
						$timeTo = $timeTo ? $timeTo->format('h:i a') : 'Unknown';
						
						if($ProfileId!=''){
							
							if(count($carrierSetup) && $carrierSetup->count()){
									$formattedCarriers[] = [
										'id' => $carrier->id,
										'scac' => $carrier->scac,
										'name' => $carrier->carrier_name,
										'logo' => asset('uploads/carriers/'. $carrier->carrier_logo),
										'address1' =>  $carrier->carrier_adr1,
										'address2' => $carrier->carrier_adr2,
										'city' => $carrier->carrier_city,
										'state' => $carrier->carrier_state,
										'postal' => $carrier->carrier_postal,
										'country' => $carrier->carrier_country,
										'startTime' => $timeFrom,
										'endTime' => $timeTo,
										'apiEnabled' => $carrier->carrier_apienabled,
									] + $contactInfo + $setupInfo;
								}
						}else{
							$formattedCarriers[] = [
							'id' => $carrier->id,
							'scac' => $carrier->scac,
							'name' => $carrier->carrier_name,
							'logo' => asset('uploads/carriers/'. $carrier->carrier_logo),
							'address1' =>  $carrier->carrier_adr1,
							'address2' => $carrier->carrier_adr2,
							'city' => $carrier->carrier_city,
							'state' => $carrier->carrier_state,
							'postal' => $carrier->carrier_postal,
							'country' => $carrier->carrier_country,
							'startTime' => $timeFrom,
							'endTime' => $timeTo,
							'apiEnabled' => $carrier->carrier_apienabled,
						] + $contactInfo + $setupInfo;
						}
					}
				}
			}
			return response()->json(['success' => 'Carriers loaded Successfully', 'count' => $carrierCount, 'carriers' => $formattedCarriers, 'carrierlist' => $addedCarriersFlatList]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	public function listUnassignedCarriers($withSetupDetails = false, $searchString = null, $onlyApiEnabled = false, $limit = null, $clientID=null){
		
		try{
			
			//checking for a valid client id
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			/*if(!is_numeric($clientID))
				throw new \Exception('The required client id is missing or invalid for this user');
			*/
			
			
			$customer = Customer::where('id', $clientID)
				->with([
					'carriers' => function($query) use ($searchString, $onlyApiEnabled, $limit){
						
						if(!empty($searchString)){
							$query->where('carrier_name', 'LIKE', "{$searchString}%");
						}
						
						if((bool)$onlyApiEnabled == true){
							$query->where('carrier_apienabled', 1);
						}
						
						$query->orderBy('carrier_name', 'asc');
						
						 if(!empty($limit))
							$query->take($limit);
											},
				])
				->first(['id']);
			
			//processing data to desired format
			$formattedCarriers = array();
			$addedCarriersFlatList = [];
			$carrierCount = 0;
			if(count($customer->carriers) && $customer->carriers->count()){
				$carrierCount = $customer->carriers->count();
				foreach($customer->carriers as $carrier){
					if(isset($carrier->pivot) && $carrier->pivot->count()){
						//getting saved contact information for this carrier
						$contactInfo = [
							'contactName' => '',
							'contactEmail' => '',
							'contactPhone' => '',
							'assignmentId' => 0 
						];
						
						$setupInfo = [];
						
						if(isset($carrier->pivot->id)){
							//if setup details requested then fetched listed setup details for this carrier of this client
							if($withSetupDetails){
								
									$carrierSetup = CarrierAPI::where('carrierID', $carrier->id)
									->where('clientID', $clientID)
									
									->with(['contract' => function($query){
										$query->select(['id', 'doc_filename', 'doc_path']);
									}])
									->first(['id', 'rate_docID', 
										'rate_apiuserid', 'rate_apiaccount', 
										'rate_apipwd', 'rate_apiactivated',
										'rate_apishiptype', 'rate_apipaytype',
									]);
								
								if(count($carrierSetup) && $carrierSetup->count()){
									$setupInfo = [
										'setupId' => $carrierSetup->id,
										'apiUserName' => $carrierSetup->rate_apiuserid,
										'apiPassword' => $carrierSetup->rate_apipwd,
										'accountNumber' => $carrierSetup->rate_apiaccount,
										'apiShipType' => $carrierSetup->rate_apishiptype,
										'apiPayType' => $carrierSetup->rate_apipaytype,
										'connectivity' => $carrierSetup->rate_apiactivated,
										'contractFile' => isset($carrierSetup->contract->doc_filename) ? 
											$carrierSetup->contract->doc_filename : null,
										/* 'contractFilePath' => isset($carrierSetup->contract->doc_path) ?
											$carrierSetup->contract->doc_path : null, */
										'contractFileUrl' => isset($carrierSetup->contract->doc_filename) ?
											self::getAssetUrl($carrierSetup->contract->doc_filename, 'docmanager'): null,
										'contractFileId' => isset($carrierSetup->contract->id) ?
											$carrierSetup->contract->id : null,
									];
								}
							}
							$contactInfo['assignmentId'] = $carrier->pivot->id;
							$carrierClientInfo = ClientCarrierContact::where('clientcarrierID', $carrier->pivot->id)->first();
						
							if(count($carrierClientInfo) && $carrierClientInfo->count()){
								$contactInfo['contactName'] = $carrierClientInfo->cc_contactname;
								$contactInfo['contactEmail'] = $carrierClientInfo->cc_email;
								$contactInfo['contactPhone'] = $carrierClientInfo->cc_contactphone;
							}
						}
						
						//adding to flat list
						$addedCarriersFlatList[] = $carrier->id;
						//calculating the time from
						$timeFrom = \DateTime::createFromFormat('H:i:s', $carrier->carrier_hoursfrom);
						$timeFrom = $timeFrom ? $timeFrom->format('h:i a') : 'Unknown';
						//calculating the time to
						$timeTo = \DateTime::createFromFormat('H:i:s', $carrier->carrier_hoursto);
						$timeTo = $timeTo ? $timeTo->format('h:i a') : 'Unknown';
						
						if(!isset($carrierSetup)){
							
							if(!empty($carrier->carrier_logo) && file_exists('uploads/carriers/'. $carrier->carrier_logo)){
								$logo=asset('uploads/carriers/'. $carrier->carrier_logo);
							}else{
								$logo=asset('images/no_image.png');
							}
							
							$formattedCarriers[] = [
							'id' => $carrier->id,
							'scac' => $carrier->scac,
							'name' => $carrier->carrier_name,
							'logo' => $logo,
							'address1' =>  $carrier->carrier_adr1,
							'address2' => $carrier->carrier_adr2,
							'city' => $carrier->carrier_city,
							'state' => $carrier->carrier_state,
							'postal' => $carrier->carrier_postal,
							'country' => $carrier->carrier_country,
							'startTime' => $timeFrom,
							'endTime' => $timeTo,
							'apiEnabled' => $carrier->carrier_apienabled,
						] + $contactInfo + $setupInfo;
						
					}
						
					}
				}
			}
			return response()->json(['success' => 'Carriers loaded Successfully', 'count' => $carrierCount, 'carriers' => $formattedCarriers, 'carrierlist' => $addedCarriersFlatList]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
		
	}
	
	/*
	* method to save carrier contract setup information
	*
	*/
	
	protected function handleCarrierSetup(Request $request){
		$carrierID = $request->get('carrierID');
		$id = $request->get('id');
		$docId = $request->get('docId');
		$userId = $request->get('username');
		$password = $request->get('password');
		
		//return response()->json(array('id' => $id, 'docID' => $docId, 'rate_apiuserid' => $userId, 'rate_apipwd' =>  $password));
		
		return $this->handleCarrierContractSetup($carrierID, $id, $docId, null, array('rate_apiuserid' => $userId, 'rate_apipwd' =>  $password));
	}
	
	protected function handleCarrierContractSetup($carrierId, $setupId, $docManagerId, $docUrl = null, array $details = array()){
		try{
			
			if(empty($setupId) || !is_numeric($setupId))
				$setupId = 0;
			
			$setupContractDetails = CarrierAPI::find($setupId);
			
			if(!count($setupContractDetails) || !$setupContractDetails->count()){
				$setupContractDetails = new CarrierAPI;
				
				$setupContractDetails->carrierID = $carrierId;
				$setupContractDetails->clientID = $setupContractDetails->rate_crtby = \Auth::user()->clientID;
			}
			
				
			if(!empty($docManagerId) && is_numeric($docManagerId))
				$setupContractDetails->rate_docID = $docManagerId;
			
			if(count($details))
				$setupContractDetails->fill($details);
			
			if($setupContractDetails->save()){
				return response()->json(['success' => 'Contract Details Saved Successfully', 'contractId' => $setupContractDetails->id, 'docId' => $docManagerId, 'docUrl' => $docUrl]);
			}else
				return response()->json(['error' => 'Contract Saving Error']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	/*
	* method to upload files to the destination and entry to doc manager
	*
	*/
	protected function handleUploadToDocManager(Request $request){
		if($request->hasFile('file') /*&& $request->has('id')*/){
				if($request->file('file')->isValid()){
					//validating the image file
					$v = \Validator::make(
						$request->all(), 
						['file' => 'required|mimes:pdf',]
					);
					//checking
					if($v->fails())
						return response()->json(['error' => $v->errors()->all()]);
					try{
						//creating or updating the document
						$id = $request->get('docId');
						if(!empty($id) && is_numeric($id)){
							$docManager = DocManager::findOrFail($id);
						}else
							$docManager = new DocManager;
						
						//handling file upload and removal of the old one if any
						$oldFile = isset($docManager->doc_filename) ? $docManager->doc_filename : null;
						
						$result = $this->_handleUploadAndResize($request, 'docmanager', 64, 64, true, $oldFile);
						
						if(isset($result['success']) && $result['success']){
							$docManager->doc_filename = $result['fileName'];
							$docManager->doc_path = $result['path'];
							$docManager->doc_filetype = $result['filetype'];
							$docManager->doc_size = $result['size'];
							$docManager->created_by = \Auth::id();
							
							if($docManager->save()){
								return json_encode(['success' => 'File uploaded and updated successfully', 'id' => $docManager->id, 'url' => self::getAssetUrl($result['fileName'], 'docmanager')]);
							}else
								return json_encode(['error' => 'File not uploaded or updated successfully', ]);
						}
					}catch(\Exception $e){
						return json_encode(['error' => $e->getMessage()]);
					}
				}else
					return json_encode(['error' => 'No valid file uploaded']);
			}else
				return json_encode(['error' => 'required information missing']);
	}
	
	/*
	* method to save carrier wise contact which has been already selected
	*
	*/
	
	public function handleCarrierContact(Request $request){
		try{
			if($request->has('assignmentId')){
				$assignmentId = $request->get('assignmentId');
				//checking whether the record exists or getting created for the first time
				$contact = ClientCarrierContact::where('clientcarrierID', $assignmentId)->first();
				
				//creating new instance if it a new record
				if(empty($contact)){
					$contact = new ClientCarrierContact;
					$contact->clientcarrierID = $assignmentId;
				}
				//dd($contact);
				$contact->cc_contactname = $request->get('name');
				$contact->cc_email = $request->get('email');
				$contact->cc_contactphone = $request->get('phone');
					
				if($contact->save())
					return response()->json(['success' => 'Contact Saved Successfully', 'contactId' => $contact->id]);
				else
					return response()->json(['error' => 'Contact Saving Error']);
			}else
				throw new \Exception('Required assignment id is missing');
		}catch(Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	/*
	* method to list or create new data for api details of a carrier
	*
	*/
	public function getCarrierApiDetails(Request $request){
		$carrierId = $request->get('carrierId');
		
		if(empty($carrierId) || !is_numeric($carrierId))
			return response()->json(['error' => 'Invalid Carrier id provided']);
		
		$clientId = \Auth::user()->clientID; //getting client id
		
		//checking for carrier rate fuel and accessorial settings
		$ratePreference = \App\ClientCarrier::where('clientID', $clientId)
			->where('carrierID', $carrierId)
			->first(['acc_ruletariff', 'fuel_ruletariff']);
		//dd($ratePreference->toArray());
		
		//checking if already a record for this carrier of this client exists or not
		$carrierApi = CarrierAPI::where('clientID', $clientId)
			->where('carrierID', $carrierId)
			->with([
				'zipCodes' => function($query){
					$query->select(['id', 'rateapidetailID', 'rate_shppostal', 'rate_cnspostal']);
				}
			])
			->first();
		
		$csnZip = ['csnZip' => []];
		$shpZip = ['shpZip' => []];
		if(count($carrierApi) && isset($carrierApi->zipCodes) && $carrierApi->zipCodes->count()){
			foreach($carrierApi->zipCodes as $zip){
				if(!empty($zip->rate_cnspostal))
					$csnZip['csnZip'][] =  ['csnZip' => $zip->rate_cnspostal];
				if(!empty($zip->rate_shppostal))
					$csnZip['shpZip'][] = ['shpZip' => $zip->rate_shppostal];
			}
		}
		
		if(count($carrierApi) && $carrierApi->count()){
			return response()->json(['count' => $carrierApi->count(), 'carrier' => $ratePreference->toArray() + $carrierApi->toArray() + $csnZip + $shpZip]);
		}else
			return response()->json(['count' => 0, 'carrier' => []]);
	}
	
	/* method to curl api endpoint to see api details validity or availability 
	*
	*/
	protected function getCarrierAccTariffDetails(Request $request){
		try{
			if($request->has('carrierId')){
				$returnArr = [];
				if(!$request->has('withNoExtraParams')){
					//getting all accessorial types
					$accsTypes = \App\Accessorial::where('accstype', \Config::get('app.accsViewableType'))->get(['accsname', 'id']);
					//getting all types of calculations
					$calculationTypes = \App\CalulationType::all(['ratetype', 'id']);
					
					$returnArr['calculationTypes'] = $calculationTypes;
					$returnArr['accsTypes'] = $accsTypes;
				}
				
				$carrierId = $request->get('carrierId');
				
				//$clientID = \Auth::user()->clientID;
				$data = [];
				
				if(!$request->has('withNoCustom')){
					$tariffs = CarrierAccessorialTariff::where('carrierID', $carrierId)
						->where('clientID', \CustomerRepository::getCurrentCustomerID());
						
					//processing filters if sent
					$filters = []; 
					
					if($request->has('filter')){
						$filters = $request->get('filter', []);
					}else if($request->has('filterJson')){
						$filtersObjs = json_decode($request->get('filterJson'));
						foreach($filtersObjs as $filtersObj){
							if(isset($filtersObj->fromDate)){
								$fromDate = new \DateTime($filtersObj->fromDate);
								$fromDate = $fromDate ? $fromDate->format('Y-m-d') : null;
								$filters['fromDate'] = $fromDate;
								
								if(!empty($fromDate))
									$filters['fromDate'] = $fromDate;
							}
							
							if(isset($filtersObj->toDate)){
								$toDate = new \DateTime($filtersObj->toDate);
								$toDate = $toDate ? $toDate->format('Y-m-d') : null;
								
								if(!empty($toDate))
									$filters['toDate'] = $toDate;
							}
						}
					}
						
					if(isset($filters['type']) && is_numeric($filters['type'])){
						$tariffs = $tariffs->where('accsID', $filters['type']);
					}
					
					if(isset($filters['fromDate']) && !empty($filters['fromDate']) && $filters['fromDate'] != 'null'){
						$tariffs = $tariffs->where(function($query) use($filters){
							$query->where('crraccs_effectivefrom', '>=', $filters['fromDate']);
								if(isset($filters['toDate']) && !empty($filters['toDate']) && $filters['toDate'] != 'null'){
									$query->where('crraccs_effectivefrom', '<=', $filters['toDate']);	
								}
						});	
					}
					
					$tariffs->with([
						'accsType',
						'rate',
						'carrier',
					]);
					
					if(isset($filters['groupBy']) && !empty($filters['groupBy']) 
						&& in_array($filters['groupBy'], $this->_carrierGroupByColumns)
					){
						$tariffs->groupBy($filters['groupBy']);
					}
					
					//eagerly loading relations
					/*  */
					$data['tariffCustomCount'] = $tariffs->count();
					
					if($request->has('orderBy') && $request->has('orderType')){
						//getting order-able columns or attributes 
						$allSortableAttributes = \App\Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierAccessorialSetup'));
						
						$allSortableAttributes = (count($allSortableAttributes) && 
							$allSortableAttributes->count() &&  method_exists($allSortableAttributes, 'lists')) ? 
								$allSortableAttributes->lists('displayField') : [];
						
						$allSortableAttributes = array_merge(['id'], $allSortableAttributes);
						
						$orderBy = $request->get('orderBy');
						$orderBy = !empty($orderBy) ? $orderBy : 'crraccs_effectivefrom';
						$orderType = $request->get('orderType');
						$orderType = (!empty($orderType) && (strtolower($orderType) == 'asc' || strtolower($orderType) == 'desc'))? $orderType : 'asc';
						
						//checking the "orderby" attributes are existed for the predefined ones
						if(in_array($orderBy, $allSortableAttributes)){
							//changing few orderby columns to morph
							if($orderBy == 'rate.ratetype')
								$orderBy = 'crraccs_type';
							else if($orderBy == 'carrier.scac')
								$orderBy = 'carrierID';
							else if($orderBy == 'carrier.carrier_name')
								$orderBy = 'carrierID';
							else if($orderBy == 'accs_type.accsname')
								$orderBy = 'accsID';
							
							//finally ordering
							$tariffs->orderBy($orderBy, $orderType);	
						}else
							$tariffs->orderBy('crraccs_effectivefrom', 'asc');
					}else
						$tariffs->orderBy('crraccs_effectivefrom', 'asc');
						
					if($request->has('limit') && $request->has('page')){
						$limit = $request->get('limit', 1);
						$page = $request->get('page', 1);
						$offset = ($page - 1) * $limit;
						
						$data['tariffsCustom'] = $tariffs->take($limit)->skip($offset)->get();
					}else
						$data['tariffsCustom'] = $tariffs->get();
				}
				
				if(!$request->has('withNoDefault')){
					$data['tariffsDefault'] = CarrierAccessorialTariff::where('carrierID', $carrierId)
						->where('clientID', NULL)
						->with([
							'accsType',
							'rate',
						])->get();
				}	
				
				//dd($tariffs);
				/* if(count($tariffs) && $tariffs->count()){
					$data['tariffsDefault'] = $tariffs->filter(function($tariff){
						return empty($tariff->clientID);
					});
					$data['tariffsCustom'] = $tariffs->filter(function($tariff) use($clientID){
						return (!empty($tariff->clientID) && $tariff->clientID == $clientID);
					});
				} */
				$returnArr['success'] = 'Information loaded successfully'; 
				$returnArr['rates'] = $data;
				
				return response()->json($returnArr);
			}else
				return response()->json(['error' => 'Required parameters are missing', ]);
		}catch(Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function getFuelData($id){
		try{
			$fuelDetails = CarrierFuelTariff::findOrFail($id);
			return response()->json(['success' => 'Details loaded successfully', 'fuelDetails' => $fuelDetails]);
		}catch(Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function getAccessorialData($id){
		try{
			$accsdetails = CarrierAccessorialTariff::findOrFail($id);
			return response()->json(['success' => 'Details loaded successfully', 'accsDetails' => $accsdetails]);
		}catch(Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	/* method to curl api endpoint to see api details validity or availability 
	*
	*/
	protected function getCarrierFuelTariffDetails(Request $request){
		try{
			if($request->has('carrierId')){
				$returnArr = []; 
				//dd($request->get('filter'));
				//getting all fuel types
				
				if(!$request->has('withNoExtraParams')){
					$returnArr['fuelTypes'] = \App\CarrierFuelType::all();
				}
			
				$data = [];
				if(!$request->has('withNoCustom')){
					$carrierId = $request->get('carrierId');
					$tariffs = CarrierFuelTariff::where('carrierID', $carrierId)
						->where('clientID', \CustomerRepository::getCurrentCustomerID());
					
					//dd($tariffs);
					$filters = [];
					
					if($request->has('filter')){
						$filters = $request->get('filter', []);
					}else if($request->has('filterJson')){
						$filtersObjs = json_decode($request->get('filterJson'));
						foreach($filtersObjs as $filtersObj){
							if(isset($filtersObj->fromDate)){
								$fromDate = new \DateTime($filtersObj->fromDate);
								$fromDate = $fromDate ? $fromDate->format('Y-m-d') : null;
								$filters['fromDate'] = $fromDate;
								
								if(!empty($fromDate))
									$filters['fromDate'] = $fromDate;
							}
							
							if(isset($filtersObj->toDate)){
								$toDate = new \DateTime($filtersObj->toDate);
								$toDate = $toDate ? $toDate->format('Y-m-d') : null;
								
								if(!empty($toDate))
									$filters['toDate'] = $toDate;
							}
						}
					}
					
					//dd($filters);
					if(isset($filters['type']) && is_numeric($filters['type'])){
						$tariffs = $tariffs->where('fuelType', $filters['type']);
					}
					//var_dump($filters['fromDate'] == 'null');
					if(isset($filters['fromDate']) && !empty($filters['fromDate']) && $filters['fromDate'] != 'null'){
						//var_dump(isset($filters['fromDate']) && !empty($filters['fromDate']));
						$tariffs = $tariffs->where(function($query) use($filters){
							$query->where('effectivefrom', '>=', $filters['fromDate']);
								if(isset($filters['toDate']) && !empty($filters['toDate']) && $filters['toDate'] != 'null'){
									$query->where('effectivefrom', '<=', $filters['toDate']);	
								}
						});	
					}
					if(isset($filters['amountFrom']) && !empty($filters['amountFrom']) && $filters['amountFrom'] != 'null'){
						$tariffs = $tariffs->where(function($query) use($filters){
							$query->where('rangefrom', '>=', $filters['amountFrom']);
								if(isset($filters['amountTo']) && !empty($filters['amountTo']) && $filters['amountTo'] != 'null'){
									$query->where('rangeto', '<=', $filters['amountTo']);	
								}
						});
					}
					
					//eagerly loading relationship
					$tariffs->with([
						'fuelType' => function($query){
							$query->select(['id','fuelType']);
						}, 
						'carrier' => function($query){
							$query->select(['id','scac','carrier_name']);
						}
					]);
					
					if(isset($filters['groupBy']) && !empty($filters['groupBy']) 
						&& in_array($filters['groupBy'], $this->_carrierGroupByColumns)
					){
						$tariffs->groupBy($filters['groupBy']);
					}
					
					$data['totalCustomRateCount'] = (count($tariffs) && $tariffs->count()) ? $tariffs->count() : 0;
					
					if($request->has('orderBy') && $request->has('orderType')){
						//getting order-able columns or attributes 
						$allSortableAttributes = \App\Layout::getDisplayAttributes(\Auth::id(), \Config::get('AppLayout.carrierFuelSetup'));
						
						$allSortableAttributes = (count($allSortableAttributes) && 
							$allSortableAttributes->count() &&  method_exists($allSortableAttributes, 'lists')) ? 
								$allSortableAttributes->lists('displayField') : [];
						
						$allSortableAttributes = array_merge(['id'], $allSortableAttributes);
						
						$orderBy = $request->get('orderBy');
						$orderBy = !empty($orderBy) ? $orderBy : 'effectivefrom';
						$orderType = $request->get('orderType');
						$orderType = (!empty($orderType) && (strtolower($orderType) == 'asc' || strtolower($orderType) == 'desc'))? $orderType : 'asc';
						
						//checking the "orderby" attributes are existed for the predefined ones
						if(in_array($orderBy, $allSortableAttributes)){
							//changing few orderby columns to morph
							if($orderBy == 'fuel_type.fuelType')
								$orderBy = 'fuelType';
							else if($orderBy == 'carrier.scac')
								$orderBy = 'carrierID';
							else if($orderBy == 'carrier.carrier_name')
								$orderBy = 'carrierID';
							//finally ordering
							$tariffs->orderBy($orderBy, $orderType);	
						}else
							$tariffs->orderBy('effectivefrom', 'asc');
					}else
						$tariffs->orderBy('effectivefrom', 'asc');
					
					if($request->has('limit') && $request->has('page')){
						$limit = $request->get('limit', 1);
						$page = $request->get('page', 1);
						$offset = ($page - 1) * $limit;
						
						$data['tariffsCustom'] = $tariffs->take($limit)->skip($offset)->get();
						
					}else
						$data['tariffsCustom'] = $tariffs->get();
				}
				
				
				if(!$request->has('withNoDefault')){
					$tariffDefault = CarrierFuelTariff::where('carrierID', $carrierId)
					->where('clientID', NULL)
					->with([
						'fuelType',
					])->get();
					$data['tariffsDefault'] = $tariffDefault;
					$data['totalDefaultCount'] = (count($tariffDefault) && $tariffDefault->count()) ? $tariffDefault->count() : 0;
				}
				
				$returnArr['success'] = 'Information loaded successfully';
				$returnArr['rates'] = $data;
				
				return response()->json($returnArr);
			}else
				return response()->json(['error' => 'Required parameters are missing', ]);
		}catch(Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	/* method to curl api endpoint to see api details validity or availability 
	*
	*/
	public function getApiStatus(Request $request){
		try{
			
		$url = env('CARRIER_API_CHEK_ENDPOINT')
			.'username=' . $request->get('username')
			.'&password=' . $request->get('password') 
			.'&accountnumber=' .$request->get('accountnumber') 
			.'&scaccode=' . $request->get('scaccode');
		
		//return response()->json(['url' => $url]);
		
		// create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
		
		/*$client = new \GuzzleHttp\Client();
		$res = $client->get(env('CARRIER_API_CHEK_ENDPOINT') 
		,[
			'query' =>  [
				'username' => $request->get('username'), 
				'password' => $request->get('password'),
				'accountnumber' => $request->get('accountnumber'),
				'scaccode' => $request->get('scaccode'),
			]
		]
		);
		
		/* echo $res->getStatusCode();
		// "200"
		echo $res->getHeader('content-type');
		// 'application/json; charset=utf8'
		echo $res->getBody();
		
		*/
		//dd($output);
			return response()->json(['success' => true, 'output' => $output]);
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	public function handleCarrierApiSetup(Request $request){
		try{
			$clientId = \Auth::user()->clientID;
			$carrierID = $request->get('carrierID');
				
			//updating carrier rate tariff pref
			
			if($request->has('acc_ruletariff') && $request->has('fuel_ruletariff')){
				//checking for carrier rate fuel and accessorial settings
				$ratePreference = \App\ClientCarrier::where('clientID', $clientId)
					->where('carrierID', $carrierID)
					->first(['id', 'acc_ruletariff', 'fuel_ruletariff']);

				//saving
				$ratePreference->acc_ruletariff = $request->get('acc_ruletariff');
				$ratePreference->fuel_ruletariff = $request->get('fuel_ruletariff');
				$ratePreference->save();
			}
			
			//checking for already saved valid information if found then updating it
			if($request->has('id')){
				$id = $request->get('id');

				if(!empty($id) && is_numeric($id)){
					$carrierDetails = CarrierAPI::find($id);
					if(count($carrierDetails) && $carrierDetails->count()){
						$carrierDetails->clientID = $clientId;
						$carrierDetails->fill($request->except([
							'scac',
						]));
						
						if($carrierDetails->save()){
							//now updating zipcodes
							if($request->has('shpZip') || $request->has('csnZip')){
								$shipZips = $request->get('shpZip', []);
								$csnZips = $request->get('csnZip', []);
								$carrierId = $request->get('carrierID');
								$shipType = $request->get('rate_apishiptype');
								$data = [];
								//iterating through the appropriate zip to insert them to the respective attributes
								if($shipType == ApiDetail::API_SHIP_TYPE_SHIPPING){
									foreach($shipZips as $zip){
										if(isset($zip['shpZip']))
											$data[] = new ApiDetail(['carrierAPIID' => $carrierId, 'rate_shppostal' => $zip['shpZip']]);
									}
								}else if($shipType == ApiDetail::API_SHIP_TYPE_CONSIGNEE){
									foreach($csnZips as $zip){
										if(isset($zip['csnZip']))
											$data[] = new ApiDetail(['carrierAPIID' => $carrierId, 'rate_cnspostal' => $zip['csnZip']]);
									}
								}
								
								//firstly deleting already added zipcodes if any
								ApiDetail::where('rateapidetailID', $carrierDetails->id)
									->delete();
								//now saving the new zip codes with related data
								$carrierDetails->zipCodes()->saveMany($data);
							}
							return response()->json(['success' => 'Details updated Successfully', 'id' => $carrierDetails->id]);
						}else
							return response()->json(['error' => 'Error occurred when saving details']);
					}
				}
			}
			
			//if the record has not been previously saved, creating new row for it
			$carrierDetails = new CarrierAPI;
			$carrierDetails->clientID = \Auth::user()->clientID;
			$carrierDetails->fill($request->except([
				'scac',
			]));
			
			if($carrierDetails->save())
				return response()->json(['success' => 'Details Saved Successfully', 'id' => $carrierDetails->id]);
			else
				return response()->json(['error' => 'Error occurred when saving details']);
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	public function deleteCarrierProvider($id){
		try{
			$clientId = \Auth::user()->clientID;
			$customer = Customer::findOrFail($clientId);
			//deleting the pivot record
			if($customer->carriers()->detach($id)){
				//now deleting the associated carrier setup if any
				$carrierContract = CarrierAPI::where('clientID', $clientId)
					->where('carrierID', $id)
					->delete();
				
				return response()->json(['success' => 'Provider deleted successfully']);
			}else
				return response()->json(['error' => 'Provider not deleted deleted successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function validateAllSteps($response = true){
		$clientID = \Auth::user()->clientID;
		
		if(is_numeric($clientID)){
			$customer = Customer::where('id', $clientID)
				->with([
					'billTo' => function($query){
						$query->select(['id', 'clientID']);
					},
					'carrierApi' => function($query){
						$query->select(['carrierID', 'clientID']);
						$query->limit(1);
					},
					/* 'carriers' => function($query){
						$query->select(\DB::raw("COUNT(id) as carrierCount"));
					},*/
				])
				->first(['id']);
				//return response()->json($customer->toArray());
				
			if(
				(count($customer) && $customer->count()) && 
				(isset($customer->carrierApi) && $customer->carrierApi->count()) &&
				(isset($customer->billTo) && $customer->billTo->count())
			)
				return $response ? response()->json(['success' => 'All steps are valid']) : true;
			else
				return $response ? response()->json(['error' => 'Not all steps are valid']) : false;
		}else
			return $response ? response()->json(['error' => 'Unknown customer / client id found']) : false;
	}

	public function deleteCarrierContractFile($id){
		try{
			$docManagerItem = DocManager::findOrFail($id);
			$removeFile = $docManagerItem->doc_filename;
			//deleting the pivot record
			if($docManagerItem->delete()){
				//deleting the file
				$this->_deleteFile($removeFile, 'docmanager', true);
				
				return response()->json(['success' => 'Contract file deleted successfully']);
			}else
				return response()->json(['error' => 'Contract file not deleted successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function getDefaultSettings($clientID = null){
		try{
			$clientID = is_numeric($clientID) ? $clientID : \Auth::user()->clientID;
			$defaultSettings = Customer::where('id', $clientID)->with('defaultSettings')->first(['id']);
			return response()->json([
				'success' => 'Default settings loaded successfully', 
				'defaultSettings' => $defaultSettings
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function postDefaultSettings(CustomerDefaultSettingsRequest $request, $clientID = null){
		try{
			$clientID = is_numeric($clientID) ? $clientID : \Auth::user()->clientID;
			if($request->has('id'))
				$defaultSettings = ClientDefaultSettings::find($request->get('id'));
			else{
				$defaultSettings = new ClientDefaultSettings();
				$defaultSettings->clientID = $clientID;
			}
			
			//pickup location
			$pickupLocation = $this->_processGoogleLocation($request->get('pickup_location'), 
				'pickup_postal', 'pickup_city', 'pickup_state', 'pickup_country'
			);
			
			//delivery location
			$deliveryLocation = $this->_processGoogleLocation($request->get('delivery_location'), 
				'delivery_postal', 'delivery_city', 'delivery_state', 'delivery_country'
			);
			
			//billing location
			$billingLocation = $this->_processGoogleLocation($request->get('billto_location'), 
				'billto_postal', 'billto_city', 'billto_state', 'billto_country'
			);
			
			//filling data
			$defaultSettings->fill($pickupLocation + $deliveryLocation 
				+ $billingLocation + $request->except(['pickup_location', 'delivery_location', 'billto_location']));
			
			if($defaultSettings->save()){
				return response()->json([
					'success' => 'Default settings saved successfully', 
					'id' => $defaultSettings->id
				]);
			}else
				return response()->json(['error' => 'Settings was not saved successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function getProfileDetails($clientID = null){
		try{
			$clientID = is_numeric($clientID) ? $clientID : \CustomerRepository::getCurrentCustomerID();
			$profile = Customer::find($clientID);
			$additionalData = ['clientWLID' => $profile->wishList->id];
			return response()->json([
				'success' => 'Profile details loaded successfully', 
				'profile' => $additionalData + $profile->toArray()
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	
	protected function getBillingDetails($clientId = null){
		try{
			$clientId = is_numeric($clientId) ? $clientId : \CustomerRepository::getCurrentCustomerID();
			$profile = Customer::where('id', $clientId)
				->with([
					'billTo' => function($query){
						
					}
				])->first(['id']);
			if(count($profile) && isset($profile->billTo) && $profile->billTo->count()){
				return response()->json([
					'success' => 'Billing details loaded successfully', 
					'billing' => $profile->billTo->toArray()
				]);
			}else
				return response()->json(['info' => 'No Billing info found']);
			
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function updateRegsteps(){
		try{
			$userId = \Auth::id();
		
			if($this->validateAllSteps(false)){
				$user = User::findorFail($userId);
			
				$user->usr_completeregsteps = true;
			
				if($user->save()){
					//now copying all app access to the user app map table to load assigned apps on the account for this user
					
					//getting all apps assigned
					$this->copyAppAccess($userId);
					
					return response()->json(['success' => 'Registration process finished successfully']);
				}else
					return response()->json(['error' => 'Registration process not finished successfully']);
			}else
				return response()->json(['error' => 'registration Steps are not valid. Please check']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	//method to get or list all available accessorial options for carriers
	protected function getCarrierAccessorials(Request $request){
		try{
			$accessorials = \App\Accessorial::select(['id', \DB::raw('accsname as label')])->get();
			return response()->json(['success' => 'Accessorial list loaded successfully', 'accessorials' => $accessorials]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	//method to get or list all available accessorial options for carriers
	protected function getItemTypes(Request $request){
		try{
			$itemTypes = \App\ItemType::select([\DB::raw('item_code as id'), \DB::raw('item_name as name')])->get();
			return response()->json(['success' => 'product list loaded successfully', 'itemTypes' => $itemTypes]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function handleCarrierAccTariffCustomDetails(Request $request){
		try{
			$customAcctariff = CarrierAccessorialTariff::find($request->get('id', 0));
			
			//making new record if not found or missing
			if(!count($customAcctariff) || (count($customAcctariff) && !$customAcctariff->count())){
				$customAcctariff = new CarrierAccessorialTariff;
				$customAcctariff->carrierID = $request->get('carrierID');
				$customAcctariff->clientID = \Auth::user()->clientID;
			}
			
			$customAcctariff->accsID = $request->get('accsID');
			$customAcctariff->crraccs_rate = $request->get('crraccs_rate');
			$customAcctariff->crraccs_type = $request->get('crraccs_type');
			$customAcctariff->crraccs_minrate = $request->get('crraccs_minrate');
			$customAcctariff->crraccs_addbydefault = $request->get('crraccs_addbydefault');
			$customAcctariff->crraccs_effectivefrom = $request->get('crraccs_effectivefrom');
			
			if($customAcctariff->save())
				return response()->json(['success' => 'Rate Created / Updated Successfully', 'id' => $customAcctariff->id]);
			else
				return response()->json(['error' => 'Rate not Updated Successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function handleCarrierFuelTariffCustomDetails(Request $request){
		try{
			$customFueltariff = CarrierFuelTariff::find($request->get('id', 0));
			
			//making new record if not found or missing
			if(!count($customFueltariff) || (count($customFueltariff) && !$customFueltariff->count())){
				$customFueltariff = new CarrierFuelTariff;
				$customFueltariff->carrierID = $request->get('carrierID');
				$customFueltariff->clientID = \Auth::user()->clientID;
			}
			
			$customFueltariff->fuelType = $request->get('fuelType');
			$customFueltariff->rangefrom = $request->get('rangefrom');
			$customFueltariff->rangeto = $request->get('rangeto');
			$customFueltariff->LTLRate = $request->get('LTLRate');
			$customFueltariff->TLRate = $request->get('TLRate');
			$customFueltariff->effectivefrom = $request->get('effectivefrom');
			
			if($customFueltariff->save())
				return response()->json(['success' => 'Fuel Rate Created/Updated Successfully', 'id' => $customFueltariff->id]);
			else
				return response()->json(['error' => 'Fuel Rate not Updated Successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function deleteCarrierAccTariffCustomDetails($id){
		try{
			$prefCustomRate = CarrierAccessorialTariff::findOrFail($id);
			if($prefCustomRate->delete())
				return response()->json(['success' => 'Rate deleted Successfully']);
			else
				return response()->json(['error' => 'Rate not deleted Successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function deleteCarrierFuelTariffCustomDetails($id){
		try{
			$prefCustomRate = CarrierFuelTariff::findOrFail($id);
			if($prefCustomRate->delete())
				return response()->json(['success' => 'Rate deleted Successfully']);
			else
				return response()->json(['error' => 'Rate not deleted Successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function copyAppAccess($userId){
		$reg_subsciption_plan_id = isset(\Auth::user()->registration->reg_subsciption_plan_id) ?
				\Auth::user()->registration->reg_subsciption_plan_id : 0;
		//querying plan default app assignments
		$subscriptionDetails = SubscriptionPlan::where('id', $reg_subsciption_plan_id)
					->with(['apps'])
					->first(['id']);
					//->toArray();
		
		if(count($subscriptionDetails) && isset($subscriptionDetails->apps) && $subscriptionDetails->apps->count()){
			$subscriptionApps = [];
			
			foreach($subscriptionDetails->apps as $detail){
				$subscriptionApps[] = $detail->Id;
			}
			//inserting as in coping
			//dd($subscriptionApps);
			//\DB::table('m_userappmap')->insert($subscriptionApps);
			\Auth::user()->apps()->sync($subscriptionApps);
		}
	}
	
	public static function getS3UploadFolder($path = 'default'){
		switch ($path){
			case 'brand_logo':
				return self::BRAND_IMAGE_DIRECTORY;
			case 'carrier':
				return self::CARRIER_IMAGE_DIRECTORY;
			case 'docmanager':
				return self::DOCMANAGER_DIRECTORY;
			default: 
				return null;
		}
	}
	
	public static function getUploadPath($path = 'default'){
		switch ($path){
			case 'brand_logo':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::BRAND_IMAGE_DIRECTORY .DIRECTORY_SEPARATOR;
			case 'carrier':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::CARRIER_IMAGE_DIRECTORY .DIRECTORY_SEPARATOR;
			case 'docmanager':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::DOCMANAGER_DIRECTORY .DIRECTORY_SEPARATOR;
			default: 
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY
					.DIRECTORY_SEPARATOR;
		}
	}
	
	public static function getAssetUrl($file, $path = 'default'){
		switch ($path){
			case 'brand_logo':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::BRAND_IMAGE_DIRECTORY. '/'. $file);
			case 'carrier':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::CARRIER_IMAGE_DIRECTORY. '/'. $file);
			case 'docmanager':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::DOCMANAGER_DIRECTORY. '/'. $file);
			default: 
				return asset(self::UPLOAD_DIRECTORY. '/'. $file);
		}
	}
	
	/* method to generate the client code based on parent company name */
	public static function generateClientCode($parentCompanyName, $currentCount){
		$compnameInitial = strtoupper(substr($parentCompanyName, 0, 2));
		$nextCount = sprintf("%08d", $currentCount +1);
		return $compnameInitial.$nextCount;
	}
	
	/* protected function to process google location details into the 
	* readable and required format for this application
	*/
	/* protected function _processGoogleLocation($location, $postalField = 'client_postal', 
		$cityField = 'client_city', $stateField = 'client_state', 
		$countryField = 'client_country'
	){
		$parsedLocation = [];
		if(isset($location['address_components']) && count($location['address_components'])){
			foreach($location['address_components'] as $address){
				if(isset($address['types'][0])){
					switch($address['types'][0]){
						case 'postal_code':
							$parsedLocation[$postalField] = isset($address['long_name']) ? $address['long_name'] : null;
							break;
						case 'locality':
							$parsedLocation[$cityField] = isset($address['long_name']) ? $address['long_name'] : null;
							break;
						case 'administrative_area_level_1':
							$parsedLocation[$stateField] = isset($address['short_name']) ? $address['short_name'] : null;
							break;
						case 'country':
							$parsedLocation[$countryField] = isset($address['short_name']) ? $address['short_name'] : null;
							break;
					}
				}
			}
		}
		
		return $parsedLocation;
	} */	
}
