<?php
namespace App\Traits;

use Illuminate\Http\Request;

use App\LayoutType;
use App\Layout;
use App\SqlOperator;
use App\UserFilter;

trait CustomizationTrait{
	protected function getLayoutTypes(){
		return response()->json(['success' => 'Layout types loaded successfully', 'layoutTypes' => LayoutType::all(['id', 'layoutName'])]);
	}
	
	protected function getLayoutAttributes($layoutId){
		try{
			$attributes = Layout::where('layoutId', $layoutId)
				->where('userId', NULL)
				->orderBy('displayOrder', 'asc')
				->get(['id', 'displayName', 'displayField', 'displayOrder', 'layoutId']);
			return response()->json($attributes);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	protected function getSavedAttributes($userId, $layoutId){
		try{
			$savedAttributes = Layout::where('layoutId', $layoutId)
				->where('userId', $userId)
				->orderBy('displayOrder', 'asc')
				->get(['displayName', 'displayField', 'displayOrder', 'layoutId']);
			return response()->json(['success' => 'Saved attributes loaded successfully', 'layoutAttributes' => $savedAttributes]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	/*
	* deletes and creates new layout assignment upon create or update
	* @returns void
	*/
	protected function saveAttributes($userId, $layoutId, array $assignments){
		$savedLayouts = [];
		foreach($assignments as $key => $assignment){
			if(isset($assignment['id']))
				unset($assignment['id']);
			$savedLayouts[] = ['displayOrder' => $key, 'userId' => $userId] + $assignment;
		}
		
		if(count($savedLayouts)){
			//firstly clearing all previous saved one to replace with these new ones
			Layout::where('userId', $userId)->where('layoutId', $layoutId)->delete();
			
			//now inserting
			Layout::insert($savedLayouts);
		}
	}
	
	protected function getAllLayoutMappings($userId){
		return $allLayoutMappings = LayoutType::with([
				'layouts' => function($query) use($userId){
					$query->where('userId', $userId);
					$query->select(['id', 'layoutId', 'displayName']);
					$query->orderBy('displayOrder', 'asc');
				}
			])
			->get(['id', 'layoutName', ]);
	}
	
	protected function getAllScreenFilters($userId){
		//getting all filters group by layoutId
		$layouts = UserFilter::where('userId', $userId)
			->distinct()
			->get(['id', 'layoutId']);
		
		//getting layout names
		/* $layoutNames = [];
		foreach($layouts as $layout){
			$layoutNames[$layout->layoutId] = isset($layout->layoutType->layoutName) ? $layout->layoutType->layoutName : 'Unknown';
		} */
		
		//getting layout ids filtered from the above query
		$layoutIds = $layouts->lists('layoutId');
		
		$allLayoutMappings = [];
		
		foreach($layoutIds as $layoutId){
			//if(array_key_exists($layoutId, $layoutNames)){
			//	$allLayoutMappings[$layoutNames[$layoutId]] 
				$allLayoutMappings[$layoutId] = UserFilter::where('userId', $userId)
				->where('layoutId', $layoutId)
				->with([
					'layoutType' => function($query){
						//$query->select(['id', 'layoutName']);
					},
					'attribute' => function($query){
						//$query->select(['id', 'layoutName']);
					},
					'operator' => function($query){
						//$query->select(['id', 'layoutName']);
					},
				])
				//->groupBy('layoutId')
				->get();
			//}
		}
		
		return $allLayoutMappings;
	}
	
	protected function deleteLayoutMappings($userId, $layoutTypeId){
		return Layout::where('layoutId', $layoutTypeId)
			->where('userId', $userId)
			->delete();
	}
	
	protected function getAllConstraintOperators(){
		return SqlOperator::get(['id', \DB::raw("CONCAT(sqldesscription, ' (', sqloperator, ')') AS operatorDesscription")]);
	}
	
	protected function saveScreenFilter(Request $request){
		if($request->has('id'))
			$screenFilter = UserFilter::findOrFail($request->get('id'));
		else
			$screenFilter = new UserFilter;
		
		$screenFilter->userId = $request->get('userId', 0);
		$screenFilter->layoutId = $request->get('layoutId', 0);
		$screenFilter->attributeId = $request->get('attributeId', 0);
		$screenFilter->sqloperatorId = $request->get('screenConstraintOperatorId', 0);
		$screenFilter->filtervalue = $request->get('filterValue');
		
		return $screenFilter->save() ? $screenFilter->id : false;
	}
	
	protected function deleteScreenFilter($layoutId, $id){
		$filter = UserFilter::where('layoutId', $layoutId);
		
		if(!empty($id) && is_numeric($id))
			$filter->where('id', $id);
		
		return $filter->delete();
	}
}