<?php
namespace App\Traits;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\Layout;
use App\UserClientMapping;

trait CommonUserClientActionsTrait{
	
	protected function listCustomersForUser(Request $request, array $fields=array(), Guard $auth){
		//processing filters and limits and other params
		$filters = $request->get('filters');
		$filters = json_decode($filters);
		//print_r($filters);
		//limit
		$orderBy = $request->get('orderBy', null);
		$orderType = $request->get('orderType', null);
		$limit = $request->get('limit', 1);
		$page = $request->get('page', 1);
		$offset = ($page - 1) * $limit;
		
		$subClients = \CustomerRepository::getChildren();
		
		$subClients = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $subClients);
		
		foreach($filters as $filter){			
			if(isset($filter->name)){
				$subClients->where('client_compname', 'LIKE', "%{$filter->name}%");
			}
			if(isset($filter->location)){
				if(isset($filter->location->postal_code)){
					$subClients->where('client_postal', $filter->location->postal_code);	
				}
				if(isset($filter->location->city)){
					$subClients->where('client_city', $filter->location->city);	
				}
				if(isset($filter->location->state)){
					$subClients->where('client_state', $filter->location->state);	
				}
				if(isset($filter->location->country)){
					$subClients->where('client_country', $filter->location->country);	
				}
			}
			
			if(isset($filter->status)){
				if((isset($filter->status->active) && $filter->status->active == 1) && 
					(!isset($filter->status->inactive) || (isset($filter->status->inactive) && $filter->status->inactive == ''))
				){
					$subClients->where('active', 1);
				}else if((isset($filter->status->inactive) && $filter->status->inactive == 1) && 
					(!isset($filter->status->active) || (isset($filter->status->active) && $filter->status->active == ''))
				){
					$subClients->where('active', 0);
				}
			}
		};
		
		
		$countSubClients = $subClients->count();
		
		if(!empty($orderBy) && !empty($orderType))
			$subClients->orderBy($orderBy, $orderType);
		else
			$subClients->orderBy('id', 'asc');
		
		$clientData = $subClients->take($limit)->skip($offset);
		
		if(count($fields))
			$clientData = $clientData->get(array_merge(['id'], $fields));
		else
			$clientData = $clientData->get();
        // dd($clientData->toArray());
		return response()->json(['success' => 'Clients loaded successfully', 'count' => $countSubClients, 'results' => $clientData]);
	}
	
	protected function listUsersforCustomers(Request $request, array $fields=array(), Guard $auth){
		//processing filters and limits and other params
		$filters = $request->get('filters');
		$filters = json_decode($filters);
		//print_r($filters);
		//limit
		
		$orderBy = $request->get('orderBy', null);
		$orderType = $request->get('orderType', null);
		$limit = $request->get('limit', 1);
		$page = $request->get('page', 1);
		$offset = ($page - 1) * $limit;
		//dd("A");
		//$clientID = $request->get('clientID', 0);
		$clientID = $request->get('clientID');
		//
		//if the sent client id is blank get the system set customer ID
		if(empty($clientID) || !is_numeric($clientID))
			$clientID = \CustomerRepository::getCurrentCustomerID();
		
		if(!is_numeric($clientID))
			return response()->json(['error' => 'Required client id is missing or invalid',]);
		
		$userList = User::where('clientID', $clientID)
			->where('id', '<>', $auth->id())
			->where('usr_type_id', '<>', \Config::get('app.superAdminID'));
			//->whereIn('id', $assignedUsers);
		
		//applying screen filter to list data with the constraint imposed by the admin
		$userList = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.userApp'), $auth, $userList);
		
		
			
		foreach($filters as $filter){
			//print_r($filter);
			if(isset($filter->name)){
				$userList->where(function($query) use($filter){
					$query->where('usr_firstname', 'LIKE', "%{$filter->name}%");
					$query->orWhere('usr_lastname', 'LIKE', "%{$filter->name}%");
				});
			}
			if(isset($filter->location)){
				if(isset($filter->location->postal_code)){
					$userList->where('usr_postal', $filter->location->postal_code);	
				}
				if(isset($filter->location->city)){
					$userList->where('usr_city', $filter->location->city);	
				}
				if(isset($filter->location->state)){
					$userList->where('usr_state', $filter->location->state);	
				}
				if(isset($filter->location->country)){
					$userList->where('usr_country', $filter->location->country);	
				}
			}
			
			if(isset($filter->status)){
				if((isset($filter->status->active) && $filter->status->active == 1) && 
					(!isset($filter->status->inactive) || (isset($filter->status->inactive) && $filter->status->inactive == ''))
				){
					$userList->where('usr_completereg', 1);
				}else if((isset($filter->status->inactive) && $filter->status->inactive == 1) && 
					(!isset($filter->status->active) || (isset($filter->status->active) && $filter->status->active == ''))
				){
					$userList->where('usr_completereg', 0);
				}
			}
		};
		
		$userListCount = $userList->count();
		
		
		if(!empty($orderBy) && !empty($orderType))
			$userList->orderBy($orderBy, $orderType);
		else
			$userList->orderBy('id', 'asc');
		
		$userData = $userList->take($limit)->skip($offset);
		
		if(count($fields))
			$userData = $userData->get(array_merge(['id'], $fields));
		else
			$userData = $userData->get();
        
		
		//$clientData = Customer::where('id', \Auth::user()->clientID)
	//	$userList = $userList
		//	->take($limit)
		//	->skip($offset) //pagination
		//	->get();
     //   dd($userData->toArray());
        return response()->json(['success' => 'Users loaded successfully', 'count' => $userListCount, 'results' => $userData]);
	}
	
	public function getChildClients(Request $request){
		try{
			$clientID = \CustomerRepository::getCurrentCustomerID();
			$selected = $request->get('selected');
			$search = $request->get('q');
			$selected = !empty($selected) ? explode(',', $selected) : [];
			
			
			/* $clients = Customer::where('parent_id', $clientID);
			
			//if super admin logs in he/she should get all customers below his/her company
			if(\Auth::user()->usr_type_id != \Config::get('app.superAdminID')){
				$mappedClients = $this->getMappedClients(\Auth::id());
				$clients->whereIn('id', $mappedClients);
			} */
			
			//returns Illuminate\Database\Eloquent\Builder
			$clients = \CustomerRepository::getChildren();
			
			if(count($selected))
				$clients->whereNotIn('id', $selected);
			
			if(!empty($search) && strlen($search) > 1)
				$clients->where('client_compname', 'LIKE', "%{$search}%");
			
			$clients = $clients->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'client_compname', 
				'client_firstname', 'client_lastname',
				'client_adr1', 'client_city',
				'client_state', 'client_postal',
				'client_country', 'client_postal',
				'client_email', 'client_postal',
			]);
			
			$processedData = [];
			foreach($clients as $key => $client){
				$processedData [$key] = [
					'clientID' => $client->id,
					'companyName' => $client->client_compname, 
					'name' => $client->client_firstname. ' '. $client->client_lastname,
					'description' => $client->client_city. ', '. $client->client_state. ', '. $client->client_postal. ', '.  $client->client_country,
				] + $client->toArray();
			}
			
			return response()->json($processedData);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage(), ]);
		}
	}
	
	protected function getUserClientMapping($userId){
		try{
			$userClientMapping = User::where('id', $userId)
				->with([
					'clients' => function($query){
						
					}
				])->first(['id']);
				
				return response()->json(['success' => 'Customer mapping loaded successfully', 'userMappings' => isset($userClientMapping->clients) ? $userClientMapping->clients : []]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function detachUserClientMapping($clientID, $userId){
		try{
			$userClientMapping = User::findOrFail($userId, ['id']);
			if($userClientMapping->clients()->detach($clientID))
				return response()->json(['success' => 'Customer mapping detached successfully',]);
			else
				return response()->json(['error' => 'Customer mapping not detached successfully',]);
				
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function syncUserClientMapping(array $clientList, $userId){
		try{
			$userClientMapping = User::findOrFail($userId, ['id']);
			return response()->json(['success' => 'Customer mapping synced successfully', 'sync' => $userClientMapping->clients()->sync($clientList)]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	protected function setupApps($userTypeId, $userId){
		//getting apps assigned for this userId
		$apps = \App\UserTypeAppMap::where('usertypeId', $userTypeId)->lists('appId');
		
		//now syncing apps got from the above result
		$user = User::findOrFail($userId);
		//syncing
		$user->apps()->sync($apps);
	}
	
	protected function getUserClientMappingList(array $fields=array(), Guard $auth){
		try{
		        $clientID = \CustomerRepository::getCurrentCustomerID();
				$default_client = Customer::where('id', $clientID)->get(['id', 'client_code', 'client_firstname', 'client_lastname', 'client_compname as label'])->toArray();
				//echo "<pre>"; print_r($default_client); exit;
				
				$subClients = \CustomerRepository::getChildren();
				$subClients = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $subClients)->orderBy('client_compname','ASC')->get(['id', 'client_code', 'client_firstname', 'client_lastname', 'client_compname as label'])->toArray();	
			    //echo '<pre>'; print_r($subClients); exit;
				//$first = ['0' => array('label' => 'None', 'id' => 0)];
				$first = [];

                $subClients =  array_merge($first,$subClients,$default_client);  
				return response()->json(['success' => 'Customer mapping loaded successfully', 'clientlist' => $subClients]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
}