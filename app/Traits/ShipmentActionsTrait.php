<?php
namespace App\Traits;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Customer;
use App\Layout;
use App\UserClientMapping;
use App\ShipHeader;
use App\ShipStop;
use App\ShipProduct;
use App\ShipNote;
use App\ShipAccessorial;
use App\ShipB2b;
use App\ShipStatus;
use App\ShipHeaderStatus;
use App\ShipheaderData;
use App\ShipDocument;
use App\ShipTracking;
use App\ShipTerminal;
use App\State;
use App\InvHeader;

trait ShipmentActionsTrait{
	
	protected function listShipmentLocation(Request $request, array $fields=array(), Guard $auth){
		if($request->get('shipID'))
		 $shipID = $request->get('shipID');
		
		$shipheader = new ShipheaderData;
		$shipheader = $shipheader->where('id', $shipID);

		$countShipments = $shipheader->count();
		return response()->json(['success' => 'Shipment loaded successfully', 'count' => $countShipments, 'results' => $shipheader]);		
    }
	
	protected function getLoadNumber(Request $request, array $fields=array()){
		try{
		        //echo '<pre>'; print_r($request->all()); exit;
				if($request->get('shipID'))
				 $shipID = $request->get('shipID');
				else
             		$shipID = $request->get('shipID');		
				 
				if($request->get('clientID'))
				 $clientID = $request->get('clientID');
					 
				if(empty($clientID) || !is_numeric($clientID))
				 $clientID = \CustomerRepository::getCurrentCustomerID();		 
				
				$custobj = new Customer;
				$custobj = $custobj->where('id', $clientID);
				$custdata = array();
				if(count($fields)){
					$custdata = $custobj->get(array_merge(['id'], $fields));
				}	
				
				$countdata = $custdata->count();
				if(!$shipID){
					if($countdata){
					  $custdata = $custdata->toArray();
					  $data = array();
					  $data['id'] = $custdata[0]['id'];
					  $data['client_lastloadno'] = $custdata[0]['client_lastloadno'] + 1;
					  $custobj->update($data);
					  $custdata = $custobj->get(array_merge(['id'], $fields));
					} 
				}
				$custdata = $custdata->toArray();
				return response()->json(['success' => 'loadid loaded successfully', 'count' => $countdata, 'results' => $custdata, 'lastloadno' => $custdata[0]['client_loadprefix'].'00'.$custdata[0]['client_lastloadno']]);	
       }catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}		
    }	
	
	
	protected function listShipmentsForCustomer(Request $request, array $fields=array(), Guard $auth){
		if($request->get('clientID'))
		 $clientID = $request->get('clientID');
			 
		if(empty($clientID) || !is_numeric($clientID))
		 $clientID = \CustomerRepository::getCurrentCustomerID();		
		
		$client_list = array();

		$subClients = \CustomerRepository::getChildren();
		$subClients = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $subClients)->lists('id');
		$subClients[] = $clientID;
		$client_list = $subClients;
		
		$filters = $request->get('filters');
		$filters = json_decode($filters);		 
			 
		$orderBy = $request->get('orderBy', null);
		$orderType = $request->get('orderType', null);
		$limit = $request->get('limit', 1);
		$page = $request->get('page', 1);
		$offset = ($page - 1) * $limit;
		
		$shipheader = new ShipheaderData;
		$shipheader = $shipheader->where('shp_isdeleted', '!=', 1);
		$shipheader = $shipheader->whereIn('clientId', $client_list);
	
		//echo '<pre>'; print_r($request->get('filters')); exit;
		foreach($filters as $filter){	
			if(isset($filter->name)){
				$shipheader->where(function ($query) use($filter) {
                $query->orWhere('shp_ponumber', 'LIKE', $filter->name.'%')
					  ->orWhere('shp_shipnumber', 'LIKE', $filter->name.'%')
					  ->orWhere('shp_deliverypostal', 'LIKE', $filter->name.'%')
					  ->orWhere('shp_pronumber', 'LIKE', $filter->name.'%')
					  ->orWhere('shp_pickuppostal', 'LIKE', $filter->name.'%');
                });
			}		
		
			if(isset($filter->shp_postatusId)){
			   if(sizeof($filter->shp_postatusId)){ 
					$shipheader = $shipheader->whereIn('shp_loadstatusId', $filter->shp_postatusId); 
				}
			}
			
			if(isset($filter->carriers)){
			   if(sizeof($filter->carriers)){
			        //echo '<pre>'; print_r($filter->carriers);
					$shipheader = $shipheader->whereIn('carrier_id', $filter->carriers); 
				}
			}
           
			if(isset($filter->shp_loadstatusId)){
			   if(sizeof($filter->shp_loadstatusId)){ 
					$shipheader = $shipheader->whereIn('shp_modeId', $filter->shp_loadstatusId); 
				}
			}		

			if(isset($filter->shp_equipmentId)){
			   if(sizeof($filter->shp_equipmentId)){
					$shipheader = $shipheader->whereIn('shp_equipmentId', $filter->shp_equipmentId); 
				}
			}

			if(isset($filter->shp_clientlist)){
			   if(sizeof($filter->shp_clientlist)){  
			   //echo '<pre>'; print_r($filter->shp_clientlist); exit;
					$shipheader = $shipheader->whereIn('clientId', $filter->shp_clientlist); 
				}
			}

			if(isset($filter->shp_pickupstate) && $filter->shp_pickupstate != 'Select All'){
			   if(sizeof($filter->shp_pickupstate)){ //print_r($filter->shp_pickupstate); exit;  
					$shipheader = $shipheader->whereIn('shp_pickupstate', [$filter->shp_pickupstate]); 
				}
			}	

			if(isset($filter->shp_deliverystate) && $filter->shp_deliverystate != 'Select All'){
			   if(sizeof($filter->shp_deliverystate)){  
					$shipheader = $shipheader->whereIn('shp_deliverystate', [$filter->shp_deliverystate]); 
				}
			}			
			
		};
			
		if(!empty($orderBy) && !empty($orderType))
			$shipheader->orderBy($orderBy, $orderType);
		else
			$shipheader->orderBy('id', 'asc');
		
		$shipmentData = $shipheader->take($limit)->skip($offset);
		
		if(count($fields)){
			$shipmentData = $shipmentData->get(array_merge(['id'], $fields));
		} else {
			$shipmentData = $shipmentData->get();			
			}	
		
			$countShipments = $shipmentData->count();
		return response()->json(['success' => 'Shipment loaded successfully', 'count' => $countShipments, 'results' => $shipmentData]);
	}	
	
	
	protected function listSummaryShipmentsForCustomer(Request $request, array $fields=array(), Guard $auth){
	        $clientID = \CustomerRepository::getCurrentCustomerID();
			$client_list = array();

			$subClients = \CustomerRepository::getChildren();
			$subClients = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $subClients)->lists('id');
			$subClients[] = $clientID;
			$client_list = $subClients;			
			$client_list_str = implode("','",$client_list);
			
		$query_str = '';	
		$filters = $request->get('filters');
		$filters = json_decode($filters);		
		foreach($filters as $filter){	
			if(isset($filter->name)){
              $query_str .= " AND (a.carrier_name LIKE '".$filter->name."%' OR a.client_compname LIKE '".$filter->name."%') ";
			}		
		
			if(isset($filter->shp_postatusId)){
			   if(sizeof($filter->shp_postatusId)){ 
                    $si = implode(',', $filter->shp_postatusId);	
                    $si = str_replace(",", "','", $si);					
				 	$query_str .= " AND a.shp_loadstatusId IN('".$si."') ";  
				}
			}
			
			if(isset($filter->carriers)){
			   if(sizeof($filter->carriers)){
                    $cr = implode(',', $filter->carriers);	
                    $cr = str_replace(",", "','", $cr);					
				 	$query_str .= " AND a.carrier_id IN('".$cr."') ";               
				}
			}
           
			if(isset($filter->shp_loadstatusId)){
			   if(sizeof($filter->shp_loadstatusId)){ 
                    $sis = implode(',', $filter->shp_loadstatusId);	
                    $sis = str_replace(",", "','", $sis);					
				 	$query_str .= " AND a.shp_modeId IN('".$sis."') "; 	 
				}
			}		

			if(isset($filter->shp_equipmentId)){
			   if(sizeof($filter->shp_equipmentId)){
                    $eqi = implode(',', $filter->shp_equipmentId);	
                    $eqi = str_replace(",", "','", $eqi);					
				 	$query_str .= " AND a.shp_equipmentId IN('".$eqi."') "; 	             
				}
			}

			if(isset($filter->shp_clientlist)){
			   if(sizeof($filter->shp_clientlist)){
                    $cl = implode(',', $filter->shp_clientlist);	
                    $cl = str_replace(",", "','", $cl);					
				 	$query_str .= " AND a.client_id IN('".$cl."') "; 
				}
			}

			if(isset($filter->shp_pickupstate) && $filter->shp_pickupstate != 'Select All'){
			   if(sizeof($filter->shp_pickupstate)){ 
			     $query_str .= " AND a.shp_pickupstate IN('".$filter->shp_pickupstate."') "; 
				}
			}	

			if(isset($filter->shp_deliverystate) && $filter->shp_deliverystate != 'Select All'){
			   if(sizeof($filter->shp_deliverystate)){  
			    $query_str .= " AND a.shp_deliverystate IN('".$filter->shp_deliverystate."') "; 
				}
			}			
			
		}			
			
			//echo "SELECT a.client_compname, a.carrier_name, sum(a.accs_customercost) payable, sum(a.accs_carriercost) receiveablecost, sum(a.accs_customercost)/100 margin, sum(a.accs_carriercost)/100 margin_percent, count(a.id) ship_count from v_shipheaderdata a where a.shp_isdeleted = 0 and a.client_id in('".$client_list_str."') group by a.client_id";
			
 			$sql_shipment = DB::select( DB::raw("SELECT a.client_compname, a.carrier_name, a.carrier_id, sum(a.accs_customercost) payable, sum(a.accs_carriercost) receiveablecost, round(sum(a.accs_customercost-a.accs_carriercost),2) margin, round((sum(a.accs_customercost-a.accs_carriercost)/sum(a.accs_customercost))*100,2) margin_percent, count(a.id) ship_count, a.client_id from v_shipheaderdata a where a.shp_isdeleted = 0 ".$query_str." and a.client_id in('".$client_list_str."') group by a.client_id,a.carrier_id") );
			$sql_shipment = (array)$sql_shipment;       	
		
			//$countData = $sql_shipment->count();
			return response()->json([
			'success' => 'Shipment summary loaded successfully', 
			'results' => [
				'shipment_summary' => $sql_shipment,
				],
			]);
	}	
	
	
	protected function handleShipTabInfo(Request $request, array $fields=array(), Guard $auth){
	  try{
	    if(!$request->get('shipID'))
		  throw new \Exception('Invalid id');
		  
		$shipheader = new ShipHeader; 
		$shipmentData = $shipheader->where('id', $request->get('shipID'))->get(['id']);

		
		$customernc[] =	array(	'name' => 'Mithun',	'age' => '23' );
		$customernc[] =	array(	'name' => 'Mithun',	'age' => '23' );
		$customernc[] =	array(	'name' => 'Mithun',	'age' => '23' );		
		
        $customernc_arr['customernc'] = $customernc;	
									
		$countcnc = sizeof($customernc_arr);
		//$shipmentData = $shipheader->get();
		return response()->json(['success' => 'Shipment loaded successfully', 'count' => $countcnc, 'results' => $customernc_arr]);
	  }catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	  
	}
	
	protected function listShipmentNotes(Request $request, $shipID = null){ 
	 try{   // echo $request->get('shipID'); exit;
	         if(!$request->get('shipID'))
			   throw new \Exception('Invalid ship ID'); 
			 
			 $shipID = $request->get('shipID'); 
			
			$notedata_customer = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '1' and a.shipId=$shipID") );
			$notedata_customer = (array)$notedata_customer;
			//echo "<pre>"; print_r($notedata_customer); exit;			
			
			$notedata_carrier = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '2' and a.shipId=$shipID") );
			$notedata_carrier = (array)$notedata_carrier;

			$notedata_internal = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '3' and a.shipId=$shipID") );
			$notedata_internal = (array)$notedata_internal;			
			
			$additionalData = [];
			
			return response()->json([
			'success' => 'Shipment details loaded successfully', 
			'results' => [
				'notedata_customer' => $notedata_customer,
				'notedata_carrier' => $notedata_carrier,
				'notedata_internal' => $notedata_internal,
				],
			]);	 
	 }catch(\Exception $e){
	  return response()->json(['error' => $e->getMessage()]);
	 }
	}
	
	
	protected function handleShipmentPostData(Request $request, $shipID = null){
		try{  //print_r($request->all()); exit; //print_r($request->all()); exit;
			$shipID = is_numeric($shipID) ? $shipID : $request->get('shipID'); 
			if($request->get('clientID'))
			 $clientID = $request->get('clientID');
			//preocessing location for this client profile submitted using Google location details
			$location = $request->shipment['pick_location']; 
			$location = $this->_processGoogleLocation($location); 
			
			if(isset($location['client_city']))
			$location['shp_pickupcity'] = $location['client_city'];
			
			if(isset($location['client_state']))
			$location['shp_pickupstate'] = $location['client_state'];
			
			if(isset($location['client_country']))
			$location['shp_pickupcountry'] = $location['client_country'];
			
			if(isset($location['client_postal']))
			$location['shp_pickuppostal'] = $location['client_postal'];
			
			//print_r($location); exit;
			
			$location1 = $request->shipment['del_location'];
			$location1 = $this->_processGoogleLocation($location1); 
			
			if(isset($location1['client_city']))
			$location['shp_deliverycity'] = $location1['client_city'];
			
			if(isset($location1['client_state']))
			$location['shp_deliverystate'] = $location1['client_state'];
			
			if(isset($location1['client_country']))
			$location['shp_deliverycountry'] = $location1['client_country'];
			
			if(isset($location1['client_postal']))
			$location['shp_deliverypostal'] = $location1['client_postal'];		

            if(isset($request->shipment['billto_location'])){
			$location2 = $request->shipment['billto_location'];
			$location2 = $this->_processGoogleLocation($location2); 

			
			if(isset($location2['client_city']))
			$location['shp_carrierbilltocity'] = $location2['client_city'];
			
			if(isset($location2['client_state']))
			$location['shp_carrierbilltostate'] = $location2['client_state'];
			
			if(isset($location2['client_country']))
			$location['shp_carrierbilltocountry'] = $location2['client_country'];
			
			if(isset($location2['client_postal']))
			$location['shp_carrierbilltopostal'] = $location2['client_postal'];				
			}
			
			//print_r($request->all()); exit;
			
			$validator = \Validator::make($request->shipment, ShipHeader::getGeneralValidationRules(['id' => $shipID]));
			
			if($validator->fails())
				return response()->json(['error' => $validator->messages()]);
			
			if(!is_numeric($shipID) || !ShipHeader::where('id', $shipID)->count()){ 				
				$shipheader = new ShipHeader;				
				
			}else{
				//fetching shipheader object for existing shipment
				$shipheader = ShipHeader::findOrFail($shipID);
				//print_r($shipheader); exit;
				$stopobj = ShipStop::where('shipId', $shipID)->select('id');
				$stopobj->delete();
				$noteobj = ShipNote::where('shipId', $shipID)->select('id');
				$noteobj->delete();
				$productobj = ShipProduct::where('shipId', $shipID)->select('id');
				$productobj->delete();
				$accssobj = ShipAccessorial::where('shipId', $shipID)->select('id');
				$accssobj->delete();
				$b2bobj = ShipB2b::where('shipId', $shipID)->select('id');
				$b2bobj->delete();				
				
				//exit;
			}
			
			if(empty($clientID) || !is_numeric($clientID))
				  $clientID = \CustomerRepository::getCurrentCustomerID();	
			
			$shipheader->clientId = $clientID; 			
			
			//dd($shipheader);
			$shipheader->fill(
				$location +
                $request->shipment +				
				$request->except([
					'_token', 'shipID'/*, 
					'clientWLID', 'nextNotAllowed', 
					'saveButtonText', 'step', 
					'wl_message'*/
				])
			); 
			//saving
			
			//echo $request->get('shp_pickuptimefrom'); exit;
			$startTime = new \DateTime($request->shipment['shp_pickuptimefrom']);
			//dd($request->get('location_hoursfrom'));
			 $shipheader->shp_pickuptimefrom = $startTime ? $startTime->format('H:i:s') : null; 
			
			//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
			$endTime = new \DateTime($request->shipment['shp_pickuptimeto']);
			 $shipheader->shp_pickuptimeto = $endTime ? $endTime->format('H:i:s') : null;

			$startTime = new \DateTime($request->shipment['shp_deliverytimefrom']);
			//dd($request->get('location_hoursfrom'));
			$shipheader->shp_deliverytimefrom = $startTime ? $startTime->format('H:i:s') : null;
			
			//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
			$endTime = new \DateTime($request->shipment['shp_deliverytimeto']);
			$shipheader->shp_deliverytimeto = $endTime ? $endTime->format('H:i:s') : null;			
											
			$shp_reqpickupdate = new \DateTime($request->shipment['shp_reqpickupdate']); 
			$shipheader->shp_reqpickupdate = $shp_reqpickupdate ? trim($shp_reqpickupdate->format('Y-m-d H:i:s')) : null;	
			
			$shp_expdeliverydate = new \DateTime($request->shipment['shp_expdeliverydate']); 
			$shipheader->shp_expdeliverydate = $shp_expdeliverydate ? trim($shp_expdeliverydate->format('Y-m-d H:i:s')) : null;				
			 
			$shipheader->shp_equipmentId = $request->shipment['shp_equipmentId'];
			//$shipheader->shp_scac = 1;
			$shipheader->shp_loadstatusId = 1;
			
			//print_r($request->all()); exit;
			if(!$shipheader->save())
				throw new \Exception('Shipment data not saved successfully');	
			else{ 
				/*if(!is_numeric($shipID) || !ShipTerminal::where('shipId', $shipID)->count()){ 		
					$shipterminal = new ShipTerminal;				
				}else{	
                  $findbyid = ShipTerminal::where('shipId', $shipID)->get(['id'])->toArray();	
                  //echo $findbyid[0]['id']; exit;				  
			      $shipterminal = ShipTerminal::findOrFail($findbyid[0]['id']);
			    }
			
			    $shipterminal->shipId = $shipheader->id;
			    $shipterminal->trm_orginname = isset($request->shipment['shp_pickupname']) ? $request->shipment['shp_pickupname'] : '';
				$shipterminal->trm_origintype = '1';
				$shipterminal->trm_originadr1 = isset($request->shipment['shp_pickupadr1']) ? $request->shipment['shp_pickupadr1'] : '';
				$shipterminal->trm_originadr2 = isset($request->shipment['shp_pickupadr2']) ? $request->shipment['shp_pickupadr2'] : '';
				
				if(isset($location['shp_pickupcity']))
				$shipterminal->trm_origincity = $location['shp_pickupcity'];
				
				if(isset($location['shp_pickupstate']))
				$shipterminal->trm_originstate = $location['shp_pickupstate'];
				
				if(isset($location['shp_pickuppostal']))
				$shipterminal->trm_originpostal = $location['shp_pickuppostal'];
				
				$shipterminal->trm_originemail = '';
				$shipterminal->trm_originphone = isset($request->shipment['shp_pickupphone']) ? $request->shipment['shp_pickupphone'] : '';
				$shipterminal->trm_origincontactname = isset($request->shipment['shp_pickupcontact']) ? $request->shipment['shp_pickupcontact'] : '';
				
				$shipterminal->trm_deliveryname = isset($request->shipment['shp_deliveryname']) ? $request->shipment['shp_deliveryname'] : '';
				$shipterminal->trm_deliverytype = '2';
				$shipterminal->trn_deliveryadr1 = isset($request->shipment['shp_deliveryadr1']) ? $request->shipment['shp_deliveryadr1'] : '';
				$shipterminal->trn_deliveryadr2 = isset($request->shipment['shp_deliveryadr2']) ? $request->shipment['shp_deliveryadr2'] : '';
				
				if(isset($location['shp_deliverycity']))
				$shipterminal->trn_deliverycity = $location['shp_deliverycity'];
				
				if(isset($location['shp_deliverystate']))
				$shipterminal->trn_deliverystate = $location['shp_deliverystate'];
				
				if(isset($location['shp_deliverypostal']))
				$shipterminal->trn_deliverypostal = $location['shp_deliverypostal'];
				
				$shipterminal->trn_deliverycontact = isset($request->shipment['shp_deliverycontact']) ? $request->shipment['shp_deliverycontact'] : '';
				$shipterminal->trn_deliveryphone = isset($request->shipment['shp_deliveryphone']) ? $request->shipment['shp_deliveryphone'] : '';
				$shipterminal->trn_deliveryemail = '';

				$shipterminal->save();
				*/
			
			    //print_r($request->stops); exit;
				$stop_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($request->stops);$ind++){
				  $shipstops_obj = new ShipStop;
				  $shipstops_obj->stop_name = isset($request->stops[$ind]['stop_name']) ? $request->stops[$ind]['stop_name'] : ''; 
				  $shipstops_obj->stop_prodId = $ind+1;
				  $shipstops_obj->stopId = $ind+1;
				  $shipstops_obj->stop_adr1 = isset($request->stops[$ind]['stop_adr1']) ? $request->stops[$ind]['stop_adr1'] : ''; 
				  $shipstops_obj->stop_adr2 = isset($request->stops[$ind]['stop_adr2']) ? $request->stops[$ind]['stop_adr2'] : ''; 	   
				  $shipstops_obj->stop_city =  isset($request->stops[$ind]['stop_city']) ? $request->stops[$ind]['stop_city'] : ''; 
				  $shipstops_obj->stop_state =  isset($request->stops[$ind]['stop_state']) ? $request->stops[$ind]['stop_state'] : '';
				  $shipstops_obj->stop_postal =  isset($request->stops[$ind]['stop_postal']) ? $request->stops[$ind]['stop_postal'] : '';
				  $shipstops_obj->stop_country = isset($request->stops[$ind]['stop_country']) ? $request->stops[$ind]['stop_country'] : '';
				  $shipstops_obj->stop_email = isset($request->stops[$ind]['stop_email']) ? $request->stops[$ind]['stop_email'] : '';
				  $shipstops_obj->stop_contactname = isset($request->stops[$ind]['stop_contactname']) ? $request->stops[$ind]['stop_contactname'] : '';
				  $shipstops_obj->stop_contactphone = isset($request->stops[$ind]['stop_contactphone']) ? $request->stops[$ind]['stop_contactphone'] : '';
				  $shipstops_obj->stop_prodqty = isset($request->stops[$ind]['stop_prodqty']) ? $request->stops[$ind]['stop_prodqty'] : '';
				  $shipstops_obj->stop_ponumber = isset($request->stops[$ind]['stop_ponumber']) ? $request->stops[$ind]['stop_ponumber'] : '';
				  $shipstops_obj->stop_type = isset($request->stops[$ind]['stop_type']) ? $request->stops[$ind]['stop_type'] : '';	
				  
				  $stop_list[$start_ind] = $shipstops_obj;
                  $start_ind++;			   
				}
			  	
              // print_r($stop_list); exit;				
			  $shipheader->find($shipheader->id)->shipstops()->saveMany($stop_list);
			  //print_r($request->products); exit;
				$shiproduct_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($request->products);$ind++){
				  $shipproduct_obj = new ShipProduct;
				  $shipproduct_obj->prod_itemname =  isset($request->products[$ind]['prod_itemname']) ? $request->products[$ind]['prod_itemname'] : '';
                  $shipproduct_obj->prod_itemcode =  isset($request->products[$ind]['prod_itemcode']) ? $request->products[$ind]['prod_itemcode'] : '';		
				  
				  $shipproduct_obj->prod_srlno = $ind+1;
				  $shipproduct_obj->prod_itempieces = isset($request->products[$ind]['prod_itempieces']) ? $request->products[$ind]['prod_itempieces'] : ''; 
				  $shipproduct_obj->prod_itemqty = isset($request->products[$ind]['prod_itemqty']) ? $request->products[$ind]['prod_itemqty'] : ''; 	   
				  $shipproduct_obj->prod_itempkgtypeId =  isset($request->products[$ind]['prod_itempkgtypeId']) ? $request->products[$ind]['prod_itempkgtypeId'] : '';
           		  $shipproduct_obj->prod_itemclass =  isset($request->products[$ind]['prod_itemclass']) ? $request->products[$ind]['prod_itemclass'] : '';
				  $shipproduct_obj->prod_itemweight =  isset($request->products[$ind]['prod_itemweight']) ? $request->products[$ind]['prod_itemweight'] : '';
				  $shipproduct_obj->prod_itemwidth = isset($request->products[$ind]['prod_itemwidth']) ? $request->products[$ind]['prod_itemwidth'] : '';	
				  $shipproduct_obj->prod_itemheight = isset($request->products[$ind]['prod_itemheight']) ? $request->products[$ind]['prod_itemheight'] : '';
				  $shipproduct_obj->prod_itemlength = isset($request->products[$ind]['prod_itemlength']) ? $request->products[$ind]['prod_itemlength'] : '';
				  $shipproduct_obj->prod_stopId = isset($request->products[$ind]['prod_stopId']) ? $request->products[$ind]['prod_stopId'] : '';
                  $shipproduct_obj->prod_itemhazmat = isset($request->products[$ind]['prod_itemhazmat']) ? $request->products[$ind]['prod_itemhazmat'] : '';
				  $shipproduct_obj->prod_itemnmfc = isset($request->products[$ind]['prod_itemnmfc']) ? $request->products[$ind]['prod_itemnmfc'] : '';
				  $shipproduct_obj->prod_cubefeet = isset($request->products[$ind]['prod_cubefeet']) ? $request->products[$ind]['prod_cubefeet'] : '';
				  $shipproduct_obj->prod_orderId = isset($request->products[$ind]['prod_orderId']) ? $request->products[$ind]['prod_orderId'] : '';
				  $shipproduct_obj->prod_accountcode = isset($request->products[$ind]['prod_accountcode']) ? $request->products[$ind]['prod_accountcode'] : '';
				  
				  $shiproduct_list[$start_ind] = $shipproduct_obj;
                  $start_ind++;			   
				}
			  	
                //print_r($shiproduct_list); exit;				
			    $shipheader->find($shipheader->id)->shipproducts()->saveMany($shiproduct_list);	
				//print_r($request->all());
				$ar1 = isset($request->shipment['customer_notes_fields_elem']) ? $request->shipment['customer_notes_fields_elem'] : array();
				//print_r($request->shipment->customer_notes_fields_elem); exit;
				$ar2 = isset($request->shipment['carrier_notes_fields_elem']) ? $request->shipment['carrier_notes_fields_elem'] : array();
				$ar3 = isset($request->shipment['internal_notes_fields_elem']) ? $request->shipment['internal_notes_fields_elem'] : array();
				
				$merged_array = array_merge($ar1,$ar2,$ar3);
				//print_r($merged_array); exit;
				
				$shipnote_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($merged_array);$ind++){
				    $shipnote_obj = new ShipNote;
					$shipnote_obj->ship_notestypeId = isset($merged_array[$ind]['ship_notestypeId']) ? $merged_array[$ind]['ship_notestypeId'] : '';
					$shipnote_obj->ship_notes = isset($merged_array[$ind]['ship_notes']) ? $merged_array[$ind]['ship_notes'] : '';
					$shipnote_obj->ship_createdbyId = isset($merged_array[$ind]['ship_createdbyId']) ? $merged_array[$ind]['ship_createdbyId'] : '';
					$shipnote_obj->ship_createddate = date('Y-m-d H:i:s');
					
				  $shipnote_list[$start_ind] = $shipnote_obj;
                  $start_ind++;						
				}
               //print_r($shipnote_list); exit;
			  $shipheader->find($shipheader->id)->shipnotes()->saveMany($shipnote_list);		  
			  //print_r($request->accessorials); exit;
			  //print_r($request->suggested_item); exit;
			  $b2b_list = array();
			  $start_ind = 0;			  
			  $b2b = $request->suggested_item;
			  for($ind=0;$ind<sizeof($b2b);$ind++){
			    $b2b_obj = new ShipB2b;
				if(isset($request->suggested_item[$ind]["suggested_list"]) && $request->suggested_item[$ind]["suggested_list"] == 1){
				 $b2b_obj->ship_b2bId = $request->suggested_item[$ind]["id"];
				 $b2b_list[$start_ind] = $b2b_obj;
				}
				
                $start_ind++;
              }			  
			  $shipheader->find($shipheader->id)->shipb2btypes()->saveMany($b2b_list);	
			  
			  $access_list = array();
			  $start_ind = 0;			  
			  $accessorials = $request->accessorials;
			  for($ind=0;$ind<sizeof($accessorials);$ind++){
			      $shipaccess_obj = new ShipAccessorial;
				  $shipaccess_obj->accs_srlno = $ind+1;
				  $shipaccess_obj->accs_scac = isset($request->accessorials[$ind]['accs_scac']) ? $request->accessorials[$ind]['accs_scac'] : '';
				  $shipaccess_obj->accs_code = $request->accessorials[$ind]['id'];
				  $shipaccess_obj->accs_quotedcost = isset($request->accessorials[$ind]['accs_quotedcost']) ? $request->accessorials[$ind]['accs_quotedcost'] : ''; 
				  $shipaccess_obj->accs_billedcost = isset($request->accessorials[$ind]['accs_billedcost']) ? $request->accessorials[$ind]['accs_billedcost'] : '';  
				  $shipaccess_obj->accs_ratedcost =  isset($request->accessorials[$ind]['accs_ratedcost']) ? $request->accessorials[$ind]['accs_ratedcost'] : ''; 
				  $shipaccess_obj->accs_approvedcost =  isset($request->accessorials[$ind]['accs_approvedcost']) ? $request->accessorials[$ind]['accs_approvedcost'] : '';
				  
                  $shipaccess_obj->accs_ratetype =  2;			  
				   
				  $access_list[$start_ind] = $shipaccess_obj;
                  $start_ind++;		   
			  }
			  // print_r($access_list); exit;
			  $shipheader->find($shipheader->id)->shipaccessorials()->saveMany($access_list);
			  
			  
			  $access_list = array();
			  $start_ind = 0;			  
			  $accessorials = $request->accessorials;
			  for($ind=0;$ind<sizeof($accessorials);$ind++){
			      $shipaccess_obj = new ShipAccessorial;
				  $shipaccess_obj->accs_srlno = $ind+1;
				  $shipaccess_obj->accs_scac = isset($request->accessorials[$ind]['accs_scac']) ? $request->accessorials[$ind]['accs_scac'] : '';
				  $shipaccess_obj->accs_code = $request->accessorials[$ind]['id'];
				  $shipaccess_obj->accs_quotedcost = isset($request->accessorials[$ind]['accs_quotedcost']) ? $request->accessorials[$ind]['accs_quotedcost'] : ''; 
				  $shipaccess_obj->accs_billedcost = isset($request->accessorials[$ind]['accs_billedcost']) ? $request->accessorials[$ind]['accs_billedcost'] : '';  
				  $shipaccess_obj->accs_ratedcost =  isset($request->accessorials[$ind]['accs_ratedcost']) ? $request->accessorials[$ind]['accs_ratedcost'] : ''; 
				  $shipaccess_obj->accs_approvedcost =  isset($request->accessorials[$ind]['accs_approvedcost']) ? $request->accessorials[$ind]['accs_approvedcost'] : '';
				  
                  $shipaccess_obj->accs_ratetype =  3;			  
				   
				  $access_list[$start_ind] = $shipaccess_obj;
                  $start_ind++;		   
			  }
			  // print_r($access_list); exit;
			  $shipheader->find($shipheader->id)->shipaccessorials()->saveMany($access_list);			  
			  
			}	
				
			return response()->json(['success' => 'Shipment details updated successfully', 'shipID' => $shipheader->id]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	
	protected function handlePartialShipmentPostData(Request $request, $shipID = null, Guard $auth){
		try{  //print_r($request->all()); exit;
			$shipID = is_numeric($shipID) ? $shipID : $request->get('shipID'); 

			//echo "<pre>"; print_r($request->shipment); exit;
			$validator = \Validator::make($request->shipment, ShipHeader::getPartialValidationRules(['id' => $shipID]));
			
			if($validator->fails())
				return response()->json(['error' => $validator->messages()]);
			
			$location = array();
			if(isset($request->shipment['pick_location'])){
			$location = $request->shipment['pick_location']; 
			$location = $this->_processGoogleLocation($location); 
			
			if(isset($location['client_city']))
			$location['shp_pickupcity'] = $location['client_city'];
			
			if(isset($location['client_state']))
			$location['shp_pickupstate'] = $location['client_state'];
			
			if(isset($location['client_country']))
			$location['shp_pickupcountry'] = $location['client_country'];
			
			if(isset($location['client_postal']))
			$location['shp_pickuppostal'] = $location['client_postal'];
			}
			//print_r($location); exit;
			if(isset($request->shipment['del_location'])){
			$location1 = $request->shipment['del_location'];
			$location1 = $this->_processGoogleLocation($location1); 
			
			if(isset($location1['client_city']))
			$location['shp_deliverycity'] = $location1['client_city'];
			
			if(isset($location1['client_state']))
			$location['shp_deliverystate'] = $location1['client_state'];
			
			if(isset($location1['client_country']))
			$location['shp_deliverycountry'] = $location1['client_country'];
			
			if(isset($location1['client_postal']))
			$location['shp_deliverypostal'] = $location1['client_postal'];		
           }

			/*$location2 = $request->shipment['billto_location'];
			$location2 = $this->_processGoogleLocation($location2); 

			
			if(isset($location2['client_city']))
			$location['shp_carrierbilltocity'] = $location2['client_city'];
			
			if(isset($location2['client_state']))
			$location['shp_carrierbilltostate'] = $location2['client_state'];
			
			if(isset($location2['client_country']))
			$location['shp_carrierbilltocountry'] = $location2['client_country'];
			
			if(isset($location2['client_postal']))
			$location['shp_carrierbilltopostal'] = $location2['client_postal'];				
						
            */
			
			if(!is_numeric($shipID) || !ShipHeader::where('id', $shipID)->count()){ 				
				$shipheader = new ShipHeader;				
				
			}else{
				//fetching shipheader object for existing shipment
				$shipheader = ShipHeader::findOrFail($shipID);
				//print_r($shipheader); exit;
				//echo $request->isseperate; exit;
				if(!$request->isseperate){
				$stopobj = ShipStop::where('shipId', $shipID)->select('id');
				$stopobj->delete();
				/*$b2bobj = ShipB2b::where('shipId', $shipID)->select('id');
				$b2bobj->delete();*/
				$productobj = ShipProduct::where('shipId', $shipID)->select('id');
				$productobj->delete();
				
				/*$accssobj = ShipAccessorial::select('id')
				->whereIn('accs_ratetype', [2,3]);
                $accssobj->delete();*/
				$accssobj = ShipAccessorial::where('shipId', $shipID)->select('id');
                $accssobj->delete();
				
                $ssobj = ShipHeaderStatus::where('shipId', $shipID)->select('id');
				$ssobj->delete();	

                $ssdocobj = ShipDocument::where('shipId', $shipID)->select('id');
				$ssdocobj->delete();	
                $strackobj = ShipTracking::where('shipId', $shipID)->select('id');
				$strackobj->delete();				
				
				$noteobj = ShipNote::where('shipId', $shipID)->select('id');
				$noteobj->delete();	
				
				//exit;
				}
			}		
			
			//dd($shipheader);
			$shipheader->fill(
			    $location +
                $request->shipment +				
				$request->except([
					'_token', 'shipID'/*, 
					'clientWLID', 'nextNotAllowed', 
					'saveButtonText', 'step', 
					'wl_message'*/
				])
			); 
			//saving
			
			//echo $request->get('shp_pickuptimefrom'); exit;
			if(isset($request->shipment['shp_pickuptimefrom'])){
			$startTime = new \DateTime($request->shipment['shp_pickuptimefrom']);
			 $shipheader->shp_pickuptimefrom = $startTime ? $startTime->format('H:i:s') : null; 
			}
			//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
			if(isset($request->shipment['shp_pickuptimeto'])){
			$endTime = new \DateTime($request->shipment['shp_pickuptimeto']);
			 $shipheader->shp_pickuptimeto = $endTime ? $endTime->format('H:i:s') : null;
            }
			
			if(isset($request->shipment['shp_deliverytimefrom'])){
			$startTime = new \DateTime($request->shipment['shp_deliverytimefrom']);
			//dd($request->get('location_hoursfrom'));
			$shipheader->shp_deliverytimefrom = $startTime ? $startTime->format('H:i:s') : null;
			}
			
			if(isset($request->shipment['shp_deliverytimeto'])){
			//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
			$endTime = new \DateTime($request->shipment['shp_deliverytimeto']);
			$shipheader->shp_deliverytimeto = $endTime ? $endTime->format('H:i:s') : null;			
			}

          if(isset($request->shipment['shp_reqpickupdate'])){			
			$shp_reqpickupdate = new \DateTime($request->shipment['shp_reqpickupdate']); 
			$shipheader->shp_reqpickupdate = $shp_reqpickupdate ? trim($shp_reqpickupdate->format('Y-m-d H:i:s')) : null;	
		}
           if(isset($request->shipment['shp_expdeliverydate'])){		
			$shp_expdeliverydate = new \DateTime($request->shipment['shp_expdeliverydate']); 
			$shipheader->shp_expdeliverydate = $shp_expdeliverydate ? trim($shp_expdeliverydate->format('Y-m-d H:i:s')) : null;				
			 }
			//$shipheader->shp_equipmentId = $request->shipment['shp_equipmentId'];
			
			//print_r($request->all()); exit;
			if(!$shipheader->save())
				throw new \Exception('Shipment data not saved successfully');	
			else{ 
			
				/*if(!is_numeric($shipID) || !ShipTerminal::where('shipId', $shipID)->count()){ 		
					$shipterminal = new ShipTerminal;				
				}else{	
                  $findbyid = ShipTerminal::where('shipId', $shipID)->get(['id'])->toArray();	
                  //echo $findbyid[0]['id']; exit;				  
			      $shipterminal = ShipTerminal::findOrFail($findbyid[0]['id']);
			    }
			
			    $shipterminal->shipId = $shipheader->id;
				
				if(isset($location['shp_pickupname']))
			    $shipterminal->trm_orginname = $request->shipment['shp_pickupname'];
				
				$shipterminal->trm_origintype = '1';
				
				if(isset($location['shp_pickupadr1']))
				$shipterminal->trm_originadr1 = $request->shipment['shp_pickupadr1'];
				
				if(isset($location['shp_pickupadr2']))
				$shipterminal->trm_originadr2 = $request->shipment['shp_pickupadr2'];
				
				if(isset($location['shp_pickupcity']))
				$shipterminal->trm_origincity = $location['shp_pickupcity'];
				
				if(isset($location['shp_pickupstate']))
				$shipterminal->trm_originstate = $location['shp_pickupstate'];
				
				if(isset($location['shp_pickuppostal']))
				$shipterminal->trm_originpostal = $location['shp_pickuppostal'];
				
				$shipterminal->trm_originemail = '';
				
				if(isset($request->shipment['shp_pickupphone']))
				$shipterminal->trm_originphone = $request->shipment['shp_pickupphone'];
				
				if(isset($location['shp_pickupcontact']))
				$shipterminal->trm_origincontactname = $request->shipment['shp_pickupcontact'];
				
				if(isset($location['shp_deliveryname']))
				$shipterminal->trm_deliveryname = $request->shipment['shp_deliveryname'];
				
				$shipterminal->trm_deliverytype = '2';
				
				if(isset($location['shp_deliveryadr1']))
				$shipterminal->trn_deliveryadr1 = $request->shipment['shp_deliveryadr1'];
				
				if(isset($location['shp_deliveryadr2']))
				$shipterminal->trn_deliveryadr2 = $request->shipment['shp_deliveryadr2'];
				
				if(isset($location['shp_deliverycity']))
				$shipterminal->trn_deliverycity = $location['shp_deliverycity'];
				
				if(isset($location['shp_deliverystate']))
				$shipterminal->trn_deliverystate = $location['shp_deliverystate'];
				
				if(isset($location['shp_deliverypostal']))
				$shipterminal->trn_deliverypostal = $location['shp_deliverypostal'];
				
				if(isset($location['shp_deliverycontact']))
				$shipterminal->trn_deliverycontact = $request->shipment['shp_deliverycontact'];
				
				if(isset($location['shp_deliveryphone']))
				$shipterminal->trn_deliveryphone = $request->shipment['shp_deliveryphone'];
				
				$shipterminal->trn_deliveryemail = '';

				//dd($shipterminal->all());
				$shipterminal->save();	*/		
			
			    //print_r($request->stops); exit;
				$stop_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($request->stops);$ind++){
				  $shipstops_obj = new ShipStop;
				  $shipstops_obj->stop_name = isset($request->stops[$ind]['stop_name']) ? $request->stops[$ind]['stop_name'] : ''; 
				  $shipstops_obj->stop_prodId = $ind+1;
				  $shipstops_obj->stopId = $ind+1;
				  $shipstops_obj->stop_adr1 = isset($request->stops[$ind]['stop_adr1']) ? $request->stops[$ind]['stop_adr1'] : ''; 
				  $shipstops_obj->stop_adr2 = isset($request->stops[$ind]['stop_adr2']) ? $request->stops[$ind]['stop_adr2'] : ''; 	   
				  $shipstops_obj->stop_city =  isset($request->stops[$ind]['stop_city']) ? $request->stops[$ind]['stop_city'] : ''; 
				  $shipstops_obj->stop_state =  isset($request->stops[$ind]['stop_state']) ? $request->stops[$ind]['stop_state'] : '';
				  $shipstops_obj->stop_postal =  isset($request->stops[$ind]['stop_postal']) ? $request->stops[$ind]['stop_postal'] : '';
				  $shipstops_obj->stop_country = isset($request->stops[$ind]['stop_country']) ? $request->stops[$ind]['stop_country'] : '';
				  $shipstops_obj->stop_email = isset($request->stops[$ind]['stop_email']) ? $request->stops[$ind]['stop_email'] : '';
				  $shipstops_obj->stop_contactname = isset($request->stops[$ind]['stop_contactname']) ? $request->stops[$ind]['stop_contactname'] : '';
				  $shipstops_obj->stop_contactphone = isset($request->stops[$ind]['stop_contactphone']) ? $request->stops[$ind]['stop_contactphone'] : '';
				  $shipstops_obj->stop_prodqty = isset($request->stops[$ind]['stop_prodqty']) ? $request->stops[$ind]['stop_prodqty'] : '';
				  $shipstops_obj->stop_ponumber = isset($request->stops[$ind]['stop_ponumber']) ? $request->stops[$ind]['stop_ponumber'] : '';
				  $shipstops_obj->stop_type = isset($request->stops[$ind]['stop_type']) ? $request->stops[$ind]['stop_type'] : '';	
				  
				  $stop_list[$start_ind] = $shipstops_obj;
                  $start_ind++;			   
				}
			  	
              // print_r($stop_list); exit;				
			  $shipheader->find($shipheader->id)->shipstops()->saveMany($stop_list);
			  //echo "<pre>"; print_r($request->products); exit;
				$shiproduct_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($request->products);$ind++){
				  $shipproduct_obj = new ShipProduct;
				  $shipproduct_obj->prod_itemname =  isset($request->products[$ind]['prod_itemname']) ? $request->products[$ind]['prod_itemname'] : '';
                  $shipproduct_obj->prod_itemcode =  isset($request->products[$ind]['prod_itemcode']) ? $request->products[$ind]['prod_itemcode'] : '';		
				  
				  $shipproduct_obj->prod_srlno = $ind+1;
				  $shipproduct_obj->prod_itempieces = isset($request->products[$ind]['prod_itempieces']) ? $request->products[$ind]['prod_itempieces'] : ''; 
				  $shipproduct_obj->prod_itemqty = isset($request->products[$ind]['prod_itemqty']) ? $request->products[$ind]['prod_itemqty'] : ''; 	   
				  $shipproduct_obj->prod_itempkgtypeId =  isset($request->products[$ind]['prod_itempkgtypeId']) ? $request->products[$ind]['prod_itempkgtypeId'] : '';
           		  $shipproduct_obj->prod_itemclass =  isset($request->products[$ind]['prod_itemclass']) ? $request->products[$ind]['prod_itemclass'] : '';
				  $shipproduct_obj->prod_itemweight =  isset($request->products[$ind]['prod_itemweight']) ? $request->products[$ind]['prod_itemweight'] : '';
				  $shipproduct_obj->prod_itemwidth = isset($request->products[$ind]['prod_itemwidth']) ? $request->products[$ind]['prod_itemwidth'] : '';	
				  $shipproduct_obj->prod_itemheight = isset($request->products[$ind]['prod_itemheight']) ? $request->products[$ind]['prod_itemheight'] : '';
				  $shipproduct_obj->prod_itemlength = isset($request->products[$ind]['prod_itemlength']) ? $request->products[$ind]['prod_itemlength'] : '';
				  $shipproduct_obj->prod_stopId = isset($request->products[$ind]['prod_stopId']) ? $request->products[$ind]['prod_stopId'] : '';
                  $shipproduct_obj->prod_itemhazmat = isset($request->products[$ind]['prod_itemhazmat']) ? $request->products[$ind]['prod_itemhazmat'] : '';
				  $shipproduct_obj->prod_itemnmfc = isset($request->products[$ind]['prod_itemnmfc']) ? $request->products[$ind]['prod_itemnmfc'] : '';
				  $shipproduct_obj->prod_cubefeet = isset($request->products[$ind]['prod_cubefeet']) ? $request->products[$ind]['prod_cubefeet'] : '';
				  $shipproduct_obj->prod_orderId = isset($request->products[$ind]['prod_orderId']) ? $request->products[$ind]['prod_orderId'] : '';
				  $shipproduct_obj->prod_accountcode = isset($request->products[$ind]['prod_accountcode']) ? $request->products[$ind]['prod_accountcode'] : '';
				  
				  $shiproduct_list[$start_ind] = $shipproduct_obj;
                  $start_ind++;			   
				}
			  	
                //print_r($shiproduct_list); exit;				
			    $shipheader->find($shipheader->id)->shipproducts()->saveMany($shiproduct_list);	
				
				$ar1 = $request->customer_notes ? $request->customer_notes : array();
				//print_r($ar1); exit;
				//print_r($request->shipment->customer_notes_fields_elem); exit;
				$ar2 = $request->carrier_notes ? $request->carrier_notes : array();
				$ar3 = $request->internal_notes ? $request->internal_notes : array();
				
				$merged_array = array_merge($ar1,$ar2,$ar3);
				//print_r($merged_array); exit;
				
				$shipnote_list = array();
				$start_ind = 0;
				for($ind=0;$ind<sizeof($merged_array);$ind++){
				    $shipnote_obj = new ShipNote;
					$shipnote_obj->ship_notestypeId = isset($merged_array[$ind]['ship_notestypeId']) ? $merged_array[$ind]['ship_notestypeId'] : '';
					$shipnote_obj->ship_notes = isset($merged_array[$ind]['ship_notes']) ? $merged_array[$ind]['ship_notes'] : '';
					$shipnote_obj->ship_createdbyId = isset($merged_array[$ind]['ship_createdbyId']) ? $merged_array[$ind]['ship_createdbyId'] : '';
					$shipnote_obj->ship_createddate = date('Y-m-d H:i:s');
					
				  $shipnote_list[$start_ind] = $shipnote_obj;
                  $start_ind++;						
				}
              // print_r($shipnote_list); exit;
			  $shipheader->find($shipheader->id)->shipnotes()->saveMany($shipnote_list);		  	
			  
			$acc_ar1 = isset($request->customer_cost['customernc']) ? $request->customer_cost['customernc'] : array();
			$acc_ar2 = isset($request->carrier_cost['carriernc']) ? $request->carrier_cost['carriernc'] : array();

				
		   $acc_merged_array = array_merge($acc_ar1,$acc_ar2);			  
			  
			  //echo '<pre>'; print_r($acc_merged_array); exit;
			  
			 /* $access_list = array();
			  $start_ind = 0;			  
			  $accessorials = $request->accessorials;
			  for($ind=0;$ind<sizeof($acc_merged_array);$ind++){
			      $shipaccess_obj = new ShipAccessorial;
				  $shipaccess_obj->accs_srlno = $ind+1;
				  $shipaccess_obj->accs_scac = isset($acc_merged_array[$ind]['accs_scac']) ? $acc_merged_array[$ind]['accs_scac'] : '';
				  $shipaccess_obj->accs_code = isset($acc_merged_array[$ind]['accs_code']) ? $acc_merged_array[$ind]['accs_code'] : '';
				  $shipaccess_obj->accs_quotedcost = isset($acc_merged_array[$ind]['accs_quotedcost']) ? $acc_merged_array[$ind]['accs_quotedcost'] : ''; 
				  $shipaccess_obj->accs_billedcost = isset($acc_merged_array[$ind]['accs_billedcost']) ? $acc_merged_array[$ind]['accs_billedcost'] : '';  
				  $shipaccess_obj->accs_ratedcost =  isset($acc_merged_array[$ind]['accs_ratedcost']) ? $acc_merged_array[$ind]['accs_ratedcost'] : ''; 
				  $shipaccess_obj->accs_approvedcost =  isset($acc_merged_array[$ind]['accs_approvedcost']) ? $acc_merged_array[$ind]['accs_approvedcost'] : '';
				  $shipaccess_obj->accs_ratetype =  isset($acc_merged_array[$ind]['accs_ratetype']) ? $acc_merged_array[$ind]['accs_ratetype'] : '';
				  $access_list[$start_ind] = $shipaccess_obj;
                  $start_ind++;		   
			  }
			   //print_r($access_list); exit;
			  $shipheader->find($shipheader->id)->shipaccessorials()->saveMany($access_list);
			  */
			  
			  //print_r($acc_ar1); exit; 
			  $access_list = array();
			  $start_ind = 0;	
              $first_scac = '';			  
			  
			  for($ind=0;$ind<sizeof($acc_ar1);$ind++){
			      $shipaccess_obj = new ShipAccessorial;
				  $shipaccess_obj->accs_srlno = $ind+1;
				  
			      	  
				  $shipaccess_obj->accs_scac = isset($acc_ar1[$ind]['accs_scac']) ? $acc_ar1[$ind]['accs_scac'] : '';
				  $first_scac = isset($acc_ar1[0]['accs_scac']) ? $acc_ar1[0]['accs_scac'] : '';
				  $shipaccess_obj->accs_code = isset($acc_ar1[$ind]['accs_code']) ? $acc_ar1[$ind]['accs_code'] : '';
				  $shipaccess_obj->accs_quotedcost = isset($acc_ar1[$ind]['accs_quotedcost']) ? $acc_ar1[$ind]['accs_quotedcost'] : ''; 
				  $shipaccess_obj->accs_billedcost = isset($acc_ar1[$ind]['accs_billedcost']) ? $acc_ar1[$ind]['accs_billedcost'] : '';  
				  $shipaccess_obj->accs_ratedcost =  isset($acc_ar1[$ind]['accs_ratedcost']) ? $acc_ar1[$ind]['accs_ratedcost'] : ''; 
				  $shipaccess_obj->accs_approvedcost =  isset($acc_ar1[$ind]['accs_approvedcost']) ? $acc_ar1[$ind]['accs_approvedcost'] : '';
				  $shipaccess_obj->accs_customercost =  isset($acc_ar1[$ind]['accs_customercost']) ? $acc_ar1[$ind]['accs_customercost'] : '';
				  
				  $shipaccess_obj->accs_ratetype =  isset($acc_ar1[$ind]['accs_ratetype']) ? $acc_ar1[$ind]['accs_ratetype'] : '2';
				  
				  /*if($shipaccess_obj->id){
					$test = ShipAccessorial::findOrFail($shipaccess_obj->id);
					$test->update([
					'accs_scac' => $shipaccess_obj->accs_scac, 
					'accs_code' => $shipaccess_obj->accs_code, 
					'accs_quotedcost' => $shipaccess_obj->accs_quotedcost,
					'accs_billedcost' => $shipaccess_obj->accs_billedcost,
					'accs_ratedcost' => $shipaccess_obj->accs_ratedcost,
					'accs_approvedcost' => $shipaccess_obj->accs_approvedcost,
					'accs_customercost' => $shipaccess_obj->accs_customercost,
					'accs_ratetype' => $shipaccess_obj->accs_ratetype,
					]);					
				  } else {*/
				    $access_list[$start_ind] = $shipaccess_obj;
                    $start_ind++;		   
				  //}				    
			  }
			   //print_r($access_list); exit;
			  $shipheader->id = $shipheader->id;
			  $shipheader->shp_scac = $first_scac;
			  $shipheader->save();
			  
			  $shipheader->find($shipheader->id)->shipaccessorials()->saveMany($access_list);
			  			  
			  
			  $access_list = array();
			  $start_ind = 0;			  
			  //$accessorials = $request->accessorials;
			  for($ind=0;$ind<sizeof($acc_ar2);$ind++){
			      $shipaccess_obj = new ShipAccessorial;
				  $shipaccess_obj->accs_srlno = $ind+1;
				  
				  $shipaccess_obj->accs_scac = isset($acc_ar2[$ind]['accs_scac']) ? $acc_ar2[$ind]['accs_scac'] : '';
				  $shipaccess_obj->accs_code = isset($acc_ar2[$ind]['accs_code']) ? $acc_ar2[$ind]['accs_code'] : '';
				  $shipaccess_obj->accs_quotedcost = isset($acc_ar2[$ind]['accs_quotedcost']) ? $acc_ar2[$ind]['accs_quotedcost'] : ''; 
				  $shipaccess_obj->accs_billedcost = isset($acc_ar2[$ind]['accs_billedcost']) ? $acc_ar2[$ind]['accs_billedcost'] : '';  
				  $shipaccess_obj->accs_ratedcost =  isset($acc_ar2[$ind]['accs_ratedcost']) ? $acc_ar2[$ind]['accs_ratedcost'] : ''; 
				  $shipaccess_obj->accs_approvedcost =  isset($acc_ar2[$ind]['accs_approvedcost']) ? $acc_ar2[$ind]['accs_approvedcost'] : '';
				  $shipaccess_obj->accs_customercost =  isset($acc_ar2[$ind]['accs_customercost']) ? $acc_ar2[$ind]['accs_customercost'] : '';
				  $shipaccess_obj->accs_ratetype =  isset($acc_ar2[$ind]['accs_ratetype']) ? $acc_ar2[$ind]['accs_ratetype'] : '3';
				 /* if($shipaccess_obj->id){ 
					$test = ShipAccessorial::findOrFail($shipaccess_obj->id);
					$test->update([
					'accs_scac' => $shipaccess_obj->accs_scac, 
					'accs_code' => $shipaccess_obj->accs_code, 
					'accs_quotedcost' => $shipaccess_obj->accs_quotedcost,
					'accs_billedcost' => $shipaccess_obj->accs_billedcost,
					'accs_ratedcost' => $shipaccess_obj->accs_ratedcost,
					'accs_approvedcost' => $shipaccess_obj->accs_approvedcost,
					'accs_customercost' => $shipaccess_obj->accs_customercost,
					'accs_ratetype' => $shipaccess_obj->accs_ratetype,
					]);
				  } else {*/
				   $access_list[$start_ind] = $shipaccess_obj;
                   $start_ind++;		   
				 // }
			  }
			   //print_r($access_list); exit;
			  $shipheader->find($shipheader->id)->shipaccessorials()->saveMany($access_list);		  
			  
              $status_notes_list = array();
			  $start_ind = 0;			  
			  $ship_status_user = $request->ship_status_user;
			  for($ind=0;$ind<sizeof($ship_status_user);$ind++){
				    $shisattus_data = ShipStatus::where('id', $ship_status_user[$ind]['status_statusId'])->get(["name"])->toArray();
					//echo '<pre>'; print_r($shisattus_data); exit;
					$shobj = new ShipHeaderStatus;
					$shobj->shipId = isset($ship_status_user[$ind]['shipId']) ? $ship_status_user[$ind]['shipId'] : '';
					$shobj->status_statusId = isset($ship_status_user[$ind]['status_statusId']) ? $ship_status_user[$ind]['status_statusId'] : '';
					$shobj->status_changeddate = isset($ship_status_user[$ind]['status_changeddate']) ? $ship_status_user[$ind]['status_changeddate'] : '';
					$shobj->status_notes = 'Shipment is updated to '.$shisattus_data[0]['name'];
					$shobj->status_changedbyId = isset($ship_status_user[$ind]['status_changedbyId']) ? $ship_status_user[$ind]['status_changedbyId'] : '';				  
				  
				   $status_notes_list[$start_ind] = $shobj;
                   $start_ind++;		   
			  }			  
			  
			  $shipheader->find($shipheader->id)->shipuserstatus()->saveMany($status_notes_list);
			
              $doc_list = array();
			  $start_ind = 0;			  
			  $ship_doc = $request->ship_doc;
			  //dd($ship_doc);
			  for($ind=0;$ind<sizeof($ship_doc);$ind++){
					$shdocobj = new ShipDocument;
					$shdocobj->shipId = $shipheader->id;
					$shdocobj->doc_location = isset($ship_doc[$ind]['doc_location']) ? $ship_doc[$ind]['doc_location'] : '';
					$shdocobj->doc_createdbyId = $auth->user()->id;
					$shdocobj->doc_createddate = date('Y-m-d');
								  
				  
				   $doc_list[$start_ind] = $shdocobj;
                   $start_ind++;		   
			  }			  
			  
			  $shipheader->find($shipheader->id)->shipdocuments()->saveMany($doc_list);		
			  
			  
              $track_list = array();
			  $start_ind = 0;			  
			  $ship_track = $request->ship_track;
			  //dd($ship_track);
			  for($ind=0;$ind<sizeof($ship_track);$ind++){
					$trackobj = new ShipTracking;
					$trackobj->shipId = $shipheader->id;
					$trackobj->track_date = date('Y-m-d'); 
					$trackobj->track_time = isset($ship_track[$ind]['track_time']) ? $ship_track[$ind]['track_time'] : '';
					$trackobj->track_statusId = isset($ship_track[$ind]['track_statusId']) ? $ship_track[$ind]['track_statusId'] : '';
					$trackobj->track_text = isset($ship_track[$ind]['track_text']) ? $ship_track[$ind]['track_text'] : '';
					$trackobj->track_city = isset($ship_track[$ind]['track_city']) ? $ship_track[$ind]['track_city'] : '';
					$trackobj->track_state = isset($ship_track[$ind]['track_state']) ? $ship_track[$ind]['track_state'] : '';
					$trackobj->track_createdbyId = isset($ship_track[$ind]['track_createdbyId']) ? $ship_track[$ind]['track_createdbyId'] : '';
					$trackobj->track_createdDate = isset($ship_track[$ind]['track_createdDate']) ? $ship_track[$ind]['track_createdDate'] : '';
								  
				   $track_list[$start_ind] = $trackobj;
                   $start_ind++;		   
			  }			  
			  
			  $shipheader->find($shipheader->id)->shiptracking()->saveMany($track_list);				  
			  
			}	
				
			return response()->json(['success' => 'Shipment details updated successfully', 'shipID' => $shipheader->id]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	
	
	protected function getShipmentDetails(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';
			$shipmentdata = ShipHeader::find($shipID);
			$suggestedHeadingsdata = ShipHeader::find($shipID)->shipb2btypes()->lists('ship_b2bId');
			//dd($suggestedHeadingsdata);
			$productdata = ShipHeader::find($shipID)->shipproducts()->get();
			$carriersdata = ShipHeader::find($shipID)->carriers()->get(['id', 'carrier_name']);
			
			$notedata_customer = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '1' and a.shipId=$shipID") );
			$notedata_customer = (array)$notedata_customer;
			//echo "<pre>"; print_r($notedata_customer); exit;			
			
			$notedata_carrier = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '2' and a.shipId=$shipID") );
			$notedata_carrier = (array)$notedata_carrier;

			$notedata_internal = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '3' and a.shipId=$shipID") );
			$notedata_internal = (array)$notedata_internal;			
			
			/*$accessorials = ShipHeader::find($shipID)->shipaccessorials()->where('accs_ratetype', '')->get(['id', 'accs_scac', 'accs_code', 'accs_ratedcost', 'accs_customercost', 'accs_ratetype', ]);
			echo '<pre>'; print_r($accessorials->toArray()); exit;*/
			
			$clientID = \CustomerRepository::getCurrentCustomerID();	
            $customer = new Customer;
		    $accessorialData_client = $customer->find($clientID)->accessorial()->lists('accscode');	
			$accessorialData_client = implode(",",$accessorialData_client);
			//echo $accessorialData_client; exit;
			$accessorialData_client = str_replace(",", "','", $accessorialData_client);
			
			$accessorials = DB::select( DB::raw("SELECT a.shipId, a.accs_scac, a.accs_code as id,a.accs_ratedcost,a.accs_customercost,a.accs_ratetype FROM trn_shipaccessorial a 
            where a.shipId=$shipID AND a.accs_code IN('".$accessorialData_client."') GROUP BY a.accs_code") );
			$accessorials = (array)$accessorials;			
			
            //echo '<pre>'; print_r($accessorials); exit;
			
			$stops = ShipHeader::find($shipID)->shipstops()->get();
			//echo '<pre>'; print_r($accessorials_cc); exit;
			$additionalData = [];
			
			return response()->json([
				'success' => 'Shipment details loaded successfully', 
				'shipmentdata' => $additionalData + $shipmentdata->toArray(),
				'productdata' => $productdata->toArray(),
				'accessorials' => $accessorials,
				'stops' => $stops->toArray(),
				'notedata_customer' => $notedata_customer,
				'notedata_carrier' => $notedata_carrier,
				'notedata_internal' => $notedata_internal,
				'suggestedHeadings' => $suggestedHeadingsdata,
				'carriersdata' => $carriersdata->toArray(),
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	
	protected function getShipmentDetailsForList(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';
			$shipmentdata = ShipHeader::where('id', $shipID)->first(array_merge(['id'], $fields));
			$accessorials_ccccr_deafult = ShipHeader::find($shipID)->carriers()->get();
			$productdata = ShipHeader::find($shipID)->shipproducts()->get();
			$stops = ShipHeader::find($shipID)->shipstops()->get();
			/*$b2btypes = ShipHeader::find($shipID)
				->shipb2btypes()
				->with([
					'shipb2btypesname' => function($query){
						$query->select(['id', 'name']);
					}
				])->get();*/
				
			$b2btypes = DB::select( DB::raw("SELECT b.name FROM v_b2btype b, trn_shipb2b a WHERE a.ship_b2bId = b.id AND a.shipId= '".$shipID."'") );	
			$b2btypes = (array)$b2btypes;	
			//dd($b2btypes);
			
			$notedata_customer = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '1' and a.shipId=$shipID") );
			$notedata_customer = (array)$notedata_customer;
			//echo "<pre>"; print_r($notedata_customer); exit;			
			
			$notedata_carrier = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '2' and a.shipId=$shipID") );
			$notedata_carrier = (array)$notedata_carrier;

			$notedata_internal = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),ship_createddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),ship_createddate)), ' day/s ago')) end) as ship_createddate,c.usr_firstname, c.usr_lastname FROM trn_shipnotes a,trn_shipheader b, m_users c  where a.shipId = b.id and a.ship_createdbyId=c.id and a.ship_notestypeId= '3' and a.shipId=$shipID") );
			$notedata_internal = (array)$notedata_internal;			
			
			$accessorials_cc = array();
			$accessorials_crc = array();
			$accessorials = ShipHeader::find($shipID)->shipaccessorials()->where('accs_ratetype', '')->get(['id', 'accs_scac', 'accs_code', 'accs_ratedcost', 'accs_customercost', 'accs_ratetype','shipId' ]);
			
			/*$accessorials_cc["customernc"] = ShipHeader::find($shipID)->shipaccessorials()->where('accs_ratetype', 2)->get(['id', 'accs_scac', 'accs_code', 'accs_ratedcost', 'accs_customercost', 'accs_ratetype', ])->toArray();
			$accessorials_crc["carriernc"] = ShipHeader::find($shipID)->shipaccessorials()->where('accs_ratetype', 3)->get(['id', 'accs_scac', 'accs_code', 'accs_ratedcost', 'accs_customercost', 'accs_ratetype', ])->toArray();	*/
			
			$accessorials_cc["customernc"] = DB::select( DB::raw("SELECT a.id, a.accs_scac, a.accs_ratedcost, a.accs_ratedcost,a.accs_code,a.accs_customercost,a.accs_ratetype, b.accsname,c.carrier_name FROM trn_shipaccessorial a LEFT JOIN m_accessorial b 
			ON (a.accs_code = b.accscode) LEFT JOIN m_carrier c ON (a.accs_scac = c.scac)
			WHERE a.shipId = '".$shipID."' AND a.accs_ratetype = '2'") );
			
			
			$accessorials_crc["carriernc"] = DB::select( DB::raw("SELECT a.id, a.accs_scac, a.accs_ratedcost, a.accs_ratedcost,a.accs_code,a.accs_customercost,a.accs_ratetype, b.accsname,c.carrier_name FROM trn_shipaccessorial a LEFT JOIN m_accessorial b 
			ON (a.accs_code = b.accscode) LEFT JOIN m_carrier c ON (a.accs_scac = c.scac)
			WHERE a.shipId = '".$shipID."' AND a.accs_ratetype = '3'") );
					

			if(empty($accessorials_cc["customernc"])){ 
			 $accessorials_cc["customernc"] = $accessorials->toArray();
            } else {
			 $accessorials_cc["customernc"] = array_merge($accessorials->toArray(),$accessorials_cc["customernc"]);
			}

			
            if(empty($accessorials_crc["carriernc"])){
			 $accessorials_crc["carriernc"] = $accessorials->toArray();
            } else {
			 $accessorials_crc["carriernc"] = array_merge($accessorials->toArray(),$accessorials_crc["carriernc"]);
			}			
			
			$accessorials_cc_ini = [];
			$accessorials_crc_ini = [];
			if(isset($accessorials_cc["customernc"][0]->accs_scac)){
            $accessorials_cc_ini = DB::select( DB::raw("SELECT id, scac, carrier_logo, carrier_name FROM m_carrier WHERE scac = '".$accessorials_cc["customernc"][0]->accs_scac."'") );
			$accessorials_cc_ini = (array)$accessorials_cc_ini;	
			}
           // echo '<pre>';
			//print_r($accessorials_cc_ini); exit;
			if(isset($accessorials_crc["carriernc"][0]->accs_scac)){
            $accessorials_crc_ini = DB::select( DB::raw("SELECT id, scac, carrier_logo, carrier_name FROM m_carrier WHERE scac = '".$accessorials_crc["carriernc"][0]->accs_scac."'") );
			$accessorials_crc_ini = (array)$accessorials_crc_ini;	
			}
			
            // echo '<pre>'; print_r($shipmentdata); exit;
			//echo '<pre>'; print_r($accessorials_cc); exit;
			$additionalData = [];
			
			return response()->json([
				'success' => 'Shipment details loaded successfully', 
				'shipmentdata' => $additionalData + $shipmentdata->toArray(),
				'accessorials' => $accessorials->toArray(),
				'accessorials_cc' => $accessorials_cc,
				'accessorials_crc' => $accessorials_crc,
				'notedata_customer' => $notedata_customer,
				'notedata_carrier' => $notedata_carrier,
				'notedata_internal' => $notedata_internal,
				'accessorials_cc_ini' => $accessorials_cc_ini,
				'accessorials_crc_ini' => $accessorials_crc_ini,
				'accessorials_ccccr_deafult' => $accessorials_ccccr_deafult->toArray(),
				'productdata' => $productdata->toArray(),
				'stops' => $stops->toArray(),
				'b2btypes' => $b2btypes,
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}		
	
	protected function listSuggestedHeadings(){
	 try{
         $headings = \App\B2btype::all(['name as label', 'id']);

         return response()->json(['success' => 'Headings loaded successfully', 'headings' => $headings]);	 
	 } catch(\Exception $e){
	   return response()->json(['error' => $e->getMessage()]);
	 }
	}
	
	protected function listShipStatus(Request $request, array $fields=array(), Guard $auth){
	 try{
		if($request->get('clientID'))
		 $clientID = $request->get('clientID');
			 
		if(empty($clientID) || !is_numeric($clientID))
		 $clientID = \CustomerRepository::getCurrentCustomerID();	
		 
		$client_list = array();

		$subClients = \CustomerRepository::getChildren();
		$subClients = \ScreenFilterRepository::applyFilters(\Config::get('AppLayout.customerApp'), $auth, $subClients)->lists('id');
		$subClients[] = $clientID;
		$client_list = $subClients;
		
		if(!empty($client_list))
		 $client_list = "'".implode("','", $client_list)."','".$clientID."'";
		else
		 $client_list = "'".$clientID."'";
		 
		//echo $client_list; exit;
        //$shipstatus = \App\ShipStatus::all(['name as label', 'id']);
	
		$shipstatus = DB::select( DB::raw("SELECT a.id, a.name as label, count(b.shp_loadstatusId) as items FROM v_shipstatus a left JOIN (SELECT * FROM v_shipheaderdata WHERE (shp_isdeleted='0' OR shp_isdeleted=null) AND clientId IN(".$client_list.")) b ON a.id=b.shp_loadstatusId group by a.id") );
		$notedata_internal = (array)$shipstatus;	
		 //echo '<pre>'; print_r($shipstatus);
		 
         return response()->json(['success' => 'status loaded successfully', 'shipstatus' => $shipstatus]);	 
	 } catch(\Exception $e){
	   return response()->json(['error' => $e->getMessage()]);
	 }
	}	
	
	protected function deleteShipment($shipID){
		try{
			$ship = ShipHeader::findOrFail($shipID);
			$ship->shp_isdeleted = 1;
			if($ship->save())
				return response()->json(['success' => 'Shipment profile deleted successfully']);
			else
				return response()->json(['error' => 'Shipment profile not deleted successfully']);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	protected function updateShipmentStatus($shipID, Request $request, Guard $auth){
		try{ 
			$ship = ShipHeader::where('id', $request->get('shipId'))->update(['shp_loadstatusId' => $request->get('shp_loadstatusId')]);
			if($ship){
			    $shisattus_data = ShipStatus::where('id', $request->get('shp_loadstatusId'))->get(["name"])->toArray();
				//echo "<pre>"; print_r($shisattus_data); exit;
			    $shobj = new ShipHeaderStatus;
				$shobj->shipId = $request->get('shipId');
				$shobj->status_statusId = $request->get('shp_loadstatusId');
				$shobj->status_changeddate = date('Y-m-d');
				$shobj->status_notes = 'Shipment is updated to '.$shisattus_data[0]['name'];
				$shobj->status_changedbyId = $auth->user()->id;
				$shobj->save();
				
				return response()->json(['success' => 'Shipment profile updated successfully']);
			}else{
				return response()->json(['error' => 'Shipment profile not updated successfully']);
				}
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	
	protected function updateShipmentInvoiceStatus($shipID, Request $request, Guard $auth){
		try{ 
			$ship = ShipHeader::where('id', $request->get('shipId'))->get(['id','clientId',
			'LoadId',
			'shp_shipnumber',
			'shp_shipdate',
			'shp_pickupname',
			'shp_carrierbilltoname',
			'shp_carrierbilltoadr1',
			'shp_carrierbilltoadr2',
			'shp_carrierbilltocity',
			'shp_carrierbilltostate',
			'shp_carrierbilltocountry',
			'shp_carrierbilltopostal',			
			]);
			if($ship){
                $ship_info = $ship->toArray();			
			    //echo '<pre>'; print_r($ship_info); exit;
			
				$invobj = InvHeader::where('loadId', $request->get('shipId'))->select('id');
				$invobj->delete();			
			
			    $inv_hdr_obj = new InvHeader;
				$inv_hdr_obj->clientId = $ship_info[0]['clientId'];
				$inv_hdr_obj->loadId = $ship_info[0]['id'];
				$inv_hdr_obj->inv_number = $ship_info[0]['shp_shipnumber'];
				$inv_hdr_obj->inv_date = $ship_info[0]['shp_shipdate'];
				$inv_hdr_obj->inv_billto_custId = $ship_info[0]['clientId'];
				$inv_hdr_obj->inv_billto_name = $ship_info[0]['shp_carrierbilltoname'];
				$inv_hdr_obj->inv_billto_address1 = $ship_info[0]['shp_carrierbilltoadr1'];
				$inv_hdr_obj->inv_billto_address2 = $ship_info[0]['shp_carrierbilltoadr2'];
				$inv_hdr_obj->inv_billto_city = $ship_info[0]['shp_carrierbilltocity'];
				$inv_hdr_obj->inv_billto_state = $ship_info[0]['shp_carrierbilltostate'];
				$inv_hdr_obj->inv_billto_postal = $ship_info[0]['shp_carrierbilltopostal'];
				$inv_hdr_obj->inv_billto_country = $ship_info[0]['shp_carrierbilltocountry'];
				$inv_hdr_obj->inv_paytermsId = '';
				$inv_hdr_obj->inv_statusId = '1';

				$inv_hdr_obj->save();
				
				//$inv_statusdata_ship = DB::select( DB::raw("UPDATE trn_shipheader SET shp_invoicestatusId = '1' WHERE id = '".$request->get('shipId')."'") );
				
				$inv_statusdata_ship = DB::table('trn_shipheader')
				->where('id', $request->get('shipId'))
				->update(['shp_invoicestatusId' => 1]);
				
			    /*$shisattus_data = ShipStatus::where('id', $request->get('shp_loadstatusId'))->get(["name"])->toArray();
			    $shobj = new ShipHeaderStatus;
				$shobj->shipId = $request->get('shipId');
				$shobj->status_statusId = $request->get('shp_loadstatusId');
				$shobj->status_changeddate = date('Y-m-d');
				$shobj->status_notes = 'Shipment is updated to '.$shisattus_data[0]['name'];
				$shobj->status_changedbyId = $auth->user()->id;
				$shobj->save();*/
				
				return response()->json(['success' => 'Shipment profile updated successfully']);
			}else{
				return response()->json(['error' => 'Shipment profile not updated successfully']);
				}
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}		
	
	
	protected function getShipStatusForUserList(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';

			$statusdata_ship = DB::select( DB::raw("SELECT a.*, (case when(DATEDIFF(curdate(),status_changeddate) ) =0 then 'Today'  else (select concat((DATEDIFF(curdate(),status_changeddate)), ' day/s ago')) end) as userstatus_changeddate, c.usr_firstname, c.usr_lastname FROM trn_shipstatus a, m_users c  where a.status_changedbyId=c.id and a.shipId=$shipID ORDER BY a.id DESC") );
			$statusdata_ship = (array)$statusdata_ship;
			//echo "<pre>"; print_r($statusdata_ship); exit;			
			
			$additionalData = [];
			
			return response()->json([
				'success' => 'Shipment status details loaded successfully', 
				'shipment_status_user' => $statusdata_ship
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}		
	
	protected function getShipFiles(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';

			$files_ship = DB::select( DB::raw("SELECT id, shipId, doc_typeId, doc_createdbyId, doc_createddate, doc_location FROM trn_shipdocuments WHERE shipId=$shipID ORDER BY id DESC") );
			$files_ship = (array)$files_ship;
			//echo "<pre>"; print_r($statusdata_ship); exit;			
			
			$additionalData = [];
			
			return response()->json([
				'success' => 'Shipment status details loaded successfully', 
				'shipment_files' => $files_ship
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	
	protected function getShipTracking(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';
            $strack = new ShipTracking; 
            $ship_track = $strack::where('shipId', $shipID)->with([
			 'users' => function($query){
			  $query->select(['id', 'usr_firstname', 'usr_lastname']);
			 },
			 'shipstatus' => function($query){
			  $query->select(['id', 'name as track_statusText']);
			 }
			])->get(array_merge(['id'], $fields))->toArray();
			
			$additionalData = [];
			//dd($ship_track);
			return response()->json([
				'success' => 'Tracking details loaded successfully', 
				'track_list' => $ship_track
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}	
	
	protected function getShipTerminalData(Request $request, $shipID = null, array $fields=array()){
		try{
			$shipID = is_numeric($shipID) ? $shipID : '';
            $sterminal = new ShipTerminal; 
            $ship_trdata = $sterminal::where('shipId', $shipID)->get(array_merge(['id'], $fields))->toArray();
			
			$additionalData = [];
			//dd($$ship_trdata);
			return response()->json([
				'success' => 'Terminal data loaded successfully', 
				'terminal_data' => $ship_trdata
			]);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}		
	
	protected function listState(Request $request, array $fields=array(), Guard $auth){
		try{
            $state = new State; 
			$statelist = $state->get(['id','state_code','state_name','country_code'])->toArray();
			$x = array();
			$x[] = array('id'=>'','state_code'=>'Select All','state_name'=>'','country_code'=>'');
			return response()->json([
				'success' => 'State data loaded successfully', 
				'state_list' => array_merge($x,$statelist)
			]);			
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}

	}	
		
	
    /*
     * Method to upload, delete files  
     */

    protected function handleBrandFileUpload(Request $request) { 
        if ($request->hasFile('file') && $request->has('shipID')) {
            if ($request->file('file')->isValid()) {
                //validating the image file
                /*$v = \Validator::make(
                                $request->all(), ['file' => 'required|image', 'clientID' => 'required|numeric']
                );
                //checking
                if ($v->fails())
                    return response()->json(['error' => $v->errors()->all()]);*/
                try {
                    //finding the customer who is going to update the logo
                    //$cust = Customer::findorFail($request->get('clientID'));
                    //$oldLogo = isset($cust->wishList->wl_logo) ? $cust->wishList->wl_logo : null;

					$oldLogo = null;
                    //uploading new logo and cleaning the old one if any
                    $result = $this->_handleUploadAndResize($request, 'ship_files', 282, 53, true, $oldLogo);

                    if (isset($result['success']) && $result['success']) {
                        /*if ($cust->wishList()->count()) {
                            $wl = $cust->wishList()->first();
                            $wl->update(['wl_logo' => $result['fileName']]);
                        } else {
                            $wl = new CustomerWishList;
                            $wl->fill(['wl_logo' => $result['fileName']]);
                            $cust->wishList()->save($wl);
                        }*/
                        return response()->json([
                                    'success' => 'File uploaded successfully',
                                    'size' => isset($result['size']) ? $result['size'] : 0,
                                    'fileName' => isset($result['fileName']) ? $result['fileName'] : 0,
                                    'url' => isset($result['url']) ? $result['url'] : null
                        ]);
                    } else
                        return response()->json(['error' => 'File uploaded but not moved and saved']);
                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()]);
                }
            } else
                return response()->json(['error' => 'No valid file uploaded']);
        } else
            return response()->json(['error' => 'required information missing']);
    }
	
	
	
    private function _handleUploadAndResize(Request $request, $destinationType, $resizeWidth, $resizeHeight, $removeOld = false, $removeFile = null) {
        try {
            //generating random file name for this
            $ext = $request->file('file')->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $ext;
            $fileSize = $request->file('file')->getSize();
            //moving the file to its destination
            if ($request->file('file')->move(self::getUploadPath($destinationType), $fileName)) {

                if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png', 'txt', 'html', 'doc', 'docx', 'pdf'])) {
                    //firstly cropping the image to fit using image library façade
                    /*\Image::make(self::getUploadPath($destinationType) . $fileName, array(
                        'width' => $resizeWidth,
                        'height' => $resizeHeight,
                            //'grayscale' => true
                    ))->save(self::getUploadPath($destinationType) . $fileName);*/
                }

                //now copying this file to the aws s3 bucket
                if (!$this->copyToS3Bucket(self::getUploadPath($destinationType) . $fileName, $fileName, self::getS3UploadFolder($destinationType)
                        ))
                    throw new \Exception('copying to S3 bucket failed');

                //removing the old file if any
                if ($removeOld && !empty($removeFile)) {
                    $this->_deleteFile($removeFile, $destinationType, true); //true to remove that resource from s3 bucket
                }

                return [
                    'success' => true,
                    'size' => $fileSize,
                    'fileName' => $fileName,
                    'url' => self::getAssetUrl($fileName, $destinationType),
                    'path' => self::getUploadPath(),
                    'filetype' => $ext,
                ];
            } else
                return ['success' => false,];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }	
	
	
    public static function getUploadPath($path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::BRAND_IMAGE_DIRECTORY . DIRECTORY_SEPARATOR;
            case 'carrier':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::CARRIER_IMAGE_DIRECTORY . DIRECTORY_SEPARATOR;
            case 'docmanager':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::DOCMANAGER_DIRECTORY . DIRECTORY_SEPARATOR;
            case 'ship_files':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::SHIP_FILE_DIRECTORY . DIRECTORY_SEPARATOR;						
            default:
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR;
        }
    }	

    protected function copyToS3Bucket($fileLocation, $fileName, $s3BucketFolder = null) {
        try {
            $disk = \Storage::disk('s3');
            //copying the file to s3 bucket
            if (!empty($s3BucketFolder))
                $destinationFile = $s3BucketFolder . '/' . $fileName;
            else
                $destinationFile = $fileName;
            //copying
            //getting contents of the local file

            $contents = file_get_contents($fileLocation);
            if ($disk->put($destinationFile, $contents))
                if (!$disk->exists($destinationFile))
                    throw new \Exception('Uploading to S3 bucket has failed');
                else
                    return true;
            else
                return false;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public static function getS3UploadFolder($path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return self::BRAND_IMAGE_DIRECTORY;
            case 'carrier':
                return self::CARRIER_IMAGE_DIRECTORY;
            case 'docmanager':
                return self::DOCMANAGER_DIRECTORY;
            case 'ship_files':
                return self::SHIP_FILE_DIRECTORY;				
            default:
                return null;
        }
    }


    private function _deleteFile($removeFileName, $destinationType, $removeFromS3Bucket = false) {
        try {
            //removing the old file if any	
            $removeFile = self::getUploadPath($destinationType) . $removeFileName;

            //deleting from s3 if switched on
            if ($removeFromS3Bucket) {
                if (!$this->deleteFromS3Bucket($removeFileName, self::getS3UploadFolder($destinationType)))
                    throw new \Exception('Deleting from s3 failed');
            }

            //deleting
            if (!is_dir($removeFile) && file_exists($removeFile))
                return unlink($removeFile);
            else
                return false;
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }


    public static function getAssetUrl($file, $path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::BRAND_IMAGE_DIRECTORY . '/' . $file);
            case 'carrier':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::CARRIER_IMAGE_DIRECTORY . '/' . $file);
            case 'docmanager':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::DOCMANAGER_DIRECTORY . '/' . $file);
            case 'ship_files':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::SHIP_FILE_DIRECTORY . '/' . $file);				
            default:
                return asset(self::UPLOAD_DIRECTORY . '/' . $file);
        }
    }	
	
}