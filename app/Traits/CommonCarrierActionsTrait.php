<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Carrier;
use App\InsuranceType;
use App\CarrierInsurance;
use App\CarrierMode;
use App\CarrierEquipment;
use App\CarrierRemit;
use App\EquipmentType;
use App\ModeType;
use App\Layout;

trait CommonCarrierActionsTrait {
	
	private $_sortMethod = ['asc', 'desc', ];
	private $_carrierGroupByColumns = ['carrierId', 'equipId', 'modeId', 'insuranceTypeId'];
	private $_carrierInsuranceColumns = ['insurance_policyname', 'insuranceTypeId'];
	private $_carrierRemitColumns = ['remit_name', ];
	private $_carrierModeColumns = ['comments', 'modeId', ];
	private $_carrierEquipmentColumns = ['equip_no', ];

    protected function listCarriersForUser(Request $request, array $fields = array()) {
        //processing filters and limits and other params
        $filters = $request->get('filters');
        $filters = json_decode($filters);
        //print_r($filters);
        //limit
        $orderBy = $request->get('orderBy', null);
        $orderType = $request->get('orderType', null);
        $limit = $request->get('limit', 1);
        $page = $request->get('page', 1);
        $offset = ($page - 1) * $limit;

        $subCarriers = DB::table('m_carrier');

        foreach ($filters as $filter) {
            if (isset($filter->name)) {
                $subCarriers->where('m_carrier.carrier_name', 'LIKE', "%{$filter->name}%");
            }

            if (isset($filter->insurance) && $filter->insurance != 0) {
                $subCarriers->whereExists(function ($query) use($filter) {
                    $query->select(DB::raw(1))
                            ->from('m_carrierinsurance')
                            ->whereRaw('m_carrierinsurance.carrierId = m_carrier.id')
                            ->where('m_carrierinsurance.insuranceTypeId', $filter->insurance);
                });
            }

            if (isset($filter->mode)  && $filter->mode != 0) {
                $subCarriers->whereExists(function ($query) use($filter) {
                    $query->select(DB::raw(1))
                            ->from('m_carriermode')
                            ->whereRaw('m_carriermode.carrierId = m_carrier.id')
                            ->where('m_carriermode.modeId', $filter->mode);
                });
            }
            
            if (isset($filter->equipTypeId) && $filter->equipTypeId != ''  && $filter->equipTypeId != 0) {
                $subCarriers->whereExists(function ($query) use($filter) {
                        $query->select(DB::raw(1))
                                ->from('m_carrierequipment')
                                ->whereRaw('m_carrierequipment.carrierId = m_carrier.id')
                                ->where('m_carrierequipment.equipId', $filter->equipTypeId);
                    });
            }

            if (isset($filter->location)) {
                if (isset($filter->location->postal_code)) {
                    $subCarriers->where('m_carrier.carrier_postal', $filter->location->postal_code);
                }
                if (isset($filter->location->city)) {
                    $subCarriers->where('m_carrier.carrier_city', $filter->location->city);
                }
                if (isset($filter->location->state)) {
                    $subCarriers->where('m_carrier.carrier_state', $filter->location->state);
                }
                if (isset($filter->location->country)) {
                    $subCarriers->where('m_carrier.carrier_country', $filter->location->country);
                }
            }
            if (isset($filter->tempControl)) {
                if ($filter->tempControl == 'enabledTC') {
                    $subCarriers->whereExists(function ($query) use($filter) {
                        $query->select(DB::raw(1))
                                ->from('m_carrierequipment')
                                ->whereRaw('m_carrierequipment.carrierId = m_carrier.id')
                                ->where('m_carrierequipment.equip_refridgerate', true);
                    });
                } elseif ($filter->tempControl == 'disabledTC') {
                    $subCarriers->whereExists(function ($query) use($filter) {
                        $query->select(DB::raw(1))
                                ->from('m_carrierequipment')
                                ->whereRaw('m_carrierequipment.carrierId = m_carrier.id')
                                ->where('m_carrierequipment.equip_refridgerate', false);
                    });
                }
            }
            if (isset($filter->api)) {
                if ($filter->api == 'enabled') {
                    $subCarriers->where('m_carrier.carrier_apienabled', true);
                } elseif ($filter->api == 'disabled') {
                    $subCarriers->where('m_carrier.carrier_apienabled', false);
                }
            }
			
			if(isset($filters['groupBy']) && !empty($filters['groupBy']) 
					&& in_array($filters['groupBy'], $this->_carrierGroupByColumns)
			){
				$subCarriers->groupBy($filters['groupBy']);
			}
        }

        $countSubCarriers = $subCarriers->count();

        if (!empty($orderBy) && !empty($orderType))
            $subCarriers->orderBy($orderBy, $orderType);
        else
            $subCarriers->orderBy('scac', 'asc');

        $carrierData = $subCarriers->take($limit)->skip($offset);

        if (count($fields))
            $carrierData = $carrierData->get(array_merge(['id'], $fields));
        else
            $carrierData = $carrierData->get();        

        return response()->json(['success' => 'Carriers loaded successfully', 'count' => $countSubCarriers, 'results' => $carrierData]);
    }
	
	
    protected function listCarriersForUserData(Request $request, array $fields = array()) {
        $search = $request->get('q');
		
			//if(!empty($search) && strlen($search) > 1)
			//	$customeritems->where('item_name', 'LIKE', "%{$search}%");	
			$carrieritems = Carrier::where('id', '!=', '0');	
            if(!empty($search) && strlen($search) > 1) {
                //$subCarriers->where('m_carrier.carrier_name', 'LIKE', "%{$search}%");
				$carrieritems->where('carrier_name', 'LIKE', "%{$search}%");	
            }

		
			$carrieritems = $carrieritems->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'scac', 'carrier_name', 
				/*'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($carrieritems as $key => $carrieritem){
				$processedData [$key] = [
					'id' => $carrieritem->id,
					'scac' => $carrieritem->scac,
					'item_name' => $carrieritem->carrier_name,
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,*/
					/*'description' => $customeritem->item_weight. ', '. $customeritem->item_length. ', '. $customeritem->item_width. ', '. $customeritem->item_height. ', '.  $customeritem->item_class,*/
				] + $carrieritem->toArray();
			}
			
			return response()->json($processedData);			
		
    }	
	
	

    protected function deleteCarrier($carrierID) {
        try {
            $carrier = Carrier::findOrFail($carrierID);
            if ($carrier->delete())
                return response()->json(['success' => 'Carrier profile deleted successfully']);
            else
                return response()->json(['error' => 'Carrier profile not deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function handleCarrierProfile(Request $request, $carrID = null) {
        try {
            $carrierId = is_numeric($carrID) ? $carrID : $request->get('carrID');

            //processing location for this client profile submitted using Google location details
            $location = $request->get('location', []);
            $location = $this->_processGoogleLocation($location, 'carrier_postal', 'carrier_city', 'carrier_state', 'carrier_country');

            $validator = \Validator::make([
                        'scac' => $request->get('scac'),
                        'carrier_name' => $request->get('carrier_name'),], [
                        'scac' => 'required',
                        'carrier_name' => 'required'
            ]);
            //validating before user create
            if ($validator->fails())
                return response()->json(['error' => $validator->messages(),]);

            if (!is_numeric($carrierId) || !Carrier::where('id', $carrierId)->count()) {
                $carrier = new Carrier;
            } else {
                $carrier = Carrier::findOrFail($carrierId);
            }
            
            $carrier_type = $request->get('carrier_type');
            $carrType = $carrier_type['id'];
            $carrier_currencycode = $request->get('carrier_currencycode');
            $currType = $carrier_currencycode['id'];

            $carrier->fill(
                    $location + $request->except(['carrier_type'],['carrier_currencycode'])
            );

            if ($carrier->carrier_rateapi || $carrier->carrier_bookingapi || $carrier->carrier_trackingapi || $carrier->carrier_imageapi) {
                $carrier->carrier_apienabled = true;
            }
            
            $carrier->carrier_type = $carrType;
            $carrier->carrier_currencycode = $currType;

            //saving
            if (!$carrier->save())
                throw new \Exception('Carrier not saved successfully');

            return response()->json(['success' => 'Carrier created updated successfully', 'carrId' => $carrier->id]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function getCarrierDetails($carrierID = null) {
        try {
            if (!is_numeric($carrierID))
                throw new \Exception('Invalid Carrier ID');

            $carrier = Carrier::find($carrierID);
            return response()->json([
                        'success' => 'Carrier loaded successfully',
                        'carrier' => $carrier->toArray()
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function getCurrencyTypes() {
        try {
            //$currentType = \App\CodeReference::select('codeId', 'codeValue')->where('codeType', 'CARTYPE');
            /*$currencyType = DB::table('m_codereference')->select('id', 'codeValue as label')
                    ->where('codeType', 'CURRENCY')
                    ->get();*/
            
            $currencyType = \App\CurrencyType::all(['name as label', 'id']);

            return response()->json(['success' => 'Currency Type loaded successfully', 'currencyType' => $currencyType]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function getCarrierTypes() {
        try {
            /*$carrierType = DB::table('m_codereference')->select('id', 'codeValue as label')
                    ->where('codeType', 'CARTYPE')
                    ->get();*/
            
            $carrierType = \App\CarrierType::all(['name as label', 'id']);

            return response()->json(['success' => 'Carriers Type loaded successfully', 'carrierType' => $carrierType]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getInsuranceTypes() {
        try {
           /* $types = DB::table('m_codereference')->select('id', 'codeValue')
                    ->where('codeType', 'INSTYPE')
                    ->get();*/

            $types = \App\InsuranceType::all(['insurance_name as codeValue', 'id']);
            
            return response()->json(['success' => 'Insurances loaded successfully', 'insurancestype' => $types]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getModeTypes() {
        try {
            /*$types = DB::table('m_codereference')->select('id', 'codeValue')
                    ->where('codeType', 'MODETYPE')
                    ->get();*/
            
            $types = \App\ModeType::orderBy('name', 'ASC')->get(['name as label', 'id'])->toArray();
			//$first = ['0' => array('label' => 'None', 'id' => 0)];
			$first = [];
            
            $types =  array_merge($first,$types);  
            return response()->json(['success' => 'Mode loaded successfully', 'modestype' => $types]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }    
    
    protected function getEquipTypes() {
        try {
            $types = \App\EquipmentType::all(['name as label', 'id'])->toArray();            
            $first = ['0' => array('label' => 'None', 'id' => 0)];
            
            $equipmentTypes =  array_merge($first,$types);
            
            
            return response()->json([
                        'success' => 'Carrier loaded successfully',
                        'equipmentTypes' => $equipmentTypes
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getInsTypes() {
        try {
            /*$types = DB::table('m_codereference')->select('id', 'codeValue')
                    ->where('codeType', 'INSTYPE')
                    ->get(); */
            $types = \App\InsuranceType::all(['insurance_name as label', 'id'])->toArray();            
            $first = ['0' => array('label' => 'None', 'id' => 0)];
			
            $insTypes =  array_merge($first,$types);            
            
            return response()->json([
                        'success' => 'Carrier loaded successfully',
                        'insuranceTypes' => $insTypes
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getModTypes() {
        try {
            $types = \App\ModeType::all(['name as label', 'id'])->toArray();            
            $first = ['0' => array('label' => 'None', 'id' => 0)];
            
            $modTypes =  array_merge($first,$types);
            
            
            return response()->json([
                        'success' => 'Mode Types loaded successfully',
                        'modeTypes' => $modTypes
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getAllFields($layoutId, $userId) {
        $checkLayout = Layout::where('layoutId', $layoutId)
                        ->where('userId', $userId)->count();
        if (!$checkLayout) {
            $allFields = Layout::where('layoutId', $layoutId)
                    ->whereNull('userId')
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        } else {
            $allFields = Layout::where('layoutId', $layoutId)
                    ->where('userId', $userId)
                    ->orderBy('displayOrder', 'ASC')
                    ->get(['id', 'displayName', 'displayField', 'fieldType']);
        }
        
        return $allFields;
    }
      

    /*
     * Methods Tab 2     
     */

    protected function getCarrierInsurances(Request $request) {
        try {
			$carrierID = $request->get('carrID');
			$groupBy = $request->get('groupBy');
			$orderType = $request->get('orderType');
			$orderBy = $request->get('orderBy');
			
			$limit = $request->get('limit', 1);
			$page = $request->get('page', 1);
			$offset = ($page - 1) * $limit;
            
			if (!is_numeric($carrierID))
                throw new \Exception('Invalid Carrier ID');

            $carrierInsurances = CarrierInsurance::where('carrierId', $carrierID);
			
			if(!empty($groupBy) && in_array($groupBy, $this->_carrierInsuranceColumns)){
				$carrierInsurances->groupBy($groupBy);
			}
			
			if((!empty($orderType) && in_array($orderType, $this->_sortMethod)) &&
				(!empty($orderBy) && in_array($orderBy, $this->_carrierInsuranceColumns))
			){
				$carrierInsurances->orderBy($orderBy, $orderType);
			}
			
			$carrierInsurances->with(['insuranceType']);
			
			//$carrierInsurances->take($limit)->skip($offset);
			
			$carrierInsurances = $carrierInsurances->get();

            return response()->json([
                        'success' => 'Carrier Insurances loaded successfully',
                        'carrierInsurances' => $carrierInsurances
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function saveInsurancesForCarrier(Request $request, $carrID = null) {
        try {
            $carrierId = is_numeric($carrID) ? $carrID : $request->get('carrID');

            $insuranceIds = [];
            foreach ($request->all() as $key => $item) {
                if (is_array($item)) {
                    $insuranceId = isset($item['id']) ? $item['id'] : null;
                    $insuranceTypeId = isset($item['insuranceTypeId']) ? $item['insuranceTypeId'] : null;

                    $insuranceLocation = $item['insurance_location'];
                    $insuranceLocation = $this->_processGoogleLocation($insuranceLocation, 'insurance_postal', 'insurance_city', 'insurance_state', 'insurance_country');

                    $insuranceFields = [
                        'insurance_companyname' => isset($item['insurance_companyname']) ? $item['insurance_companyname'] : null,
                        'insurance_agentname' => isset($item['insurance_agentname']) ? $item['insurance_agentname'] : null,
                        'insurance_policyname' => isset($item['insurance_policyname']) ? $item['insurance_policyname'] : null,
                        'insurance_address1' => isset($item['insurance_address1']) ? $item['insurance_address1'] : null,
                        'insurance_amount' => isset($item['insurance_amount']) ? $item['insurance_amount'] : null,
                        'insurance_effto' => isset($item['insurance_effto']) ? $item['insurance_effto'] : null,
                        'insuranceTypeId' => $insuranceTypeId,
                        'carrierId' => $carrierId];

                    if (!$insuranceId) { // CREATE NEW ITEMS
                        $newInsurance = new CarrierInsurance;
                        $newInsurance->fill($insuranceLocation + $insuranceFields);
                        $newInsurance->save();

                        $insuranceIds[] = $newInsurance->id;
                    } else { // UPDATE EXISTENT ITEMS
                        $carrierInsurance = CarrierInsurance::findOrFail($insuranceId);
                        $carrierInsurance->fill($insuranceLocation + $insuranceFields);
                        $carrierInsurance->save();

                        $insuranceIds[] = $carrierInsurance->id;
                    }
                }
            }
            // DELETE MISSING ITEMS
            CarrierInsurance::where('carrierId', $carrierId)->whereNotIn('id', $insuranceIds)->delete();

            return response()->json(['success' => 'Insurances created updated successfully', 'carrId' => $carrierId]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /*
     * Methods Tab 3     
     */

    protected function getCarrierModes(Request $request) {
        try {
            $carrierID = $request->get('carrID');
			$groupBy = $request->get('groupBy');
			$orderType = $request->get('orderType');
			$orderBy = $request->get('orderBy');
			
			$limit = $request->get('limit', 1);
			$page = $request->get('page', 1);
			$offset = ($page - 1) * $limit;
			
            if (!is_numeric($carrierID))
                throw new \Exception('Invalid Carrier ID');

            $carrierModes = CarrierMode::where('carrierId', $carrierID);
			
			if(!empty($groupBy) && in_array($groupBy, $this->_carrierModeColumns)){
				$carrierModes->groupBy($groupBy);
			}
			
			if((!empty($orderType) && in_array($orderType, $this->_sortMethod)) &&
				(!empty($orderBy) && in_array($orderBy, $this->_carrierModeColumns))
			){
				$carrierModes->orderBy($orderBy, $orderType);
			}
			
			$carrierModes->with(['modeType']);
			
			//$carrierModes->take($limit)->skip($offset);
			
			$carrierModes = $carrierModes->get();

            return response()->json([
                        'success' => 'Carrier Modes loaded successfully',
                        'carrierModes' => $carrierModes
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function saveModesForCarrier(Request $request, $carrID = null) {
        try {
            $carrierId = is_numeric($carrID) ? $carrID : $request->get('carrID');

            $modeIds = [];
            foreach ($request->all() as $key => $item) {
                if (is_array($item)) {
                    $modeId = isset($item['id']) ? $item['id'] : null;
                    $modeTypeId = isset($item['modeId']) ? $item['modeId'] : null;

                    $modeFields = [
                        'minweight' => isset($item['minweight']) ? $item['minweight'] : null,
                        'maxweight' => isset($item['maxweight']) ? $item['maxweight'] : null,
                        'comments' => isset($item['comments']) ? $item['comments'] : null,
                        'modeId' => $modeTypeId,
                        'carrierId' => $carrierId];

                    if (!$modeId) { // CREATE NEW ITEMS
                        $newMode = new CarrierMode;
                        $newMode->fill($modeFields);
                        $newMode->save();

                        $modeIds[] = $newMode->id;
                    } else { // UPDATE EXISTENT ITEMS
                        $carrierMode = CarrierMode::findOrFail($modeId);
                        $carrierMode->fill($modeFields);
                        $carrierMode->save();

                        $modeIds[] = $carrierMode->id;
                    }
                }
            }
            // DELETE MISSING ITEMS
            CarrierMode::where('carrierId', $carrierId)->whereNotIn('id', $modeIds)->delete();

            return response()->json(['success' => 'Modes created updated successfully', 'carrId' => $carrierId]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /*
     * Methods Tab 4     
     */
    
    protected function getEquipmentTypes() {
        try {
            /*$types = DB::table('m_codereference')->select('id', 'codeValue as label')
                    ->where('codeType', 'EQPTYPE')
                    ->get();  */
            
            $types = \App\EquipmentType::orderBy('name', 'ASC')->get(['name as label', 'id'])->toArray();
			//$first = ['0' => array('label' => 'None', 'id' => 0)];
			$first = [];
            
            $types =  array_merge($first,$types);                         
            return response()->json([
                        'success' => 'Equipment loaded successfully',
                        'equipmentTypes' => $types
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function getOwnerTypes() {
        try {
            /*$ownerType = DB::table('m_codereference')->select('id', 'codeValue as label')
                    ->where('codeType', 'OWNTYPE')
                    ->get();*/
            
            $ownerType = \App\OwnerType::all(['name as label', 'id']);

            return response()->json(['success' => 'Owners Type loaded successfully', 'ownerTypes' => $ownerType]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function listEquipments(Request $request, array $fields = array()) {
        //processing filters and limits and other params
        $carrierId = $request->get('carrID', null);
        $orderBy = $request->get('orderBy', null);
        $orderType = $request->get('orderType', null);
        $limit = $request->get('limit', 1);
        $page = $request->get('page', 1);
        $offset = ($page - 1) * $limit;
		$groupBy = $request->get('groupBy');
        $equipmentData = [];
        $countSubEquipments = 0;

        if ($carrierId != null) {
            $subEquipments = \App\CarrierEquipement::where('carrierId', $carrierId);

            $countSubEquipments = $subEquipments->count();
			
			$subEquipments->with(['equipmentType']);

            if (!empty($orderBy) && !empty($orderType))
                $subEquipments->orderBy($orderBy, $orderType);
            else
                $subEquipments->orderBy('equip_no', 'asc');
			
			if(!empty($groupBy) && in_array($groupBy, $this->_carrierEquipmentColumns)){
				$subEquipments->groupBy($groupBy);
			}

            $equipmentData = $subEquipments->take($limit)->skip($offset);

            if (count($fields))
                $equipmentData = $equipmentData->get(array_merge(['id'], $fields));
            else
                $equipmentData = $equipmentData->get();

            return response()->json(['success' => 'Equipment loaded successfully', 'count' => $countSubEquipments, 'results' => $equipmentData]);
        } else {
            return response()->json(['success' => 'Equipment not created', 'count' => $countSubEquipments, 'results' => $equipmentData]);
        }
    }

    protected function deleteEquipment($equipID) {
        try {
            $equipment = CarrierEquipment::findOrFail($equipID);
            if ($equipment->delete())
                return response()->json(['success' => 'Equipment profile deleted successfully']);
            else
                return response()->json(['error' => 'Equipment profile not deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function getEquipmentDetails($equipID = null) {
        try {
            if (!is_numeric($equipID))
                throw new \Exception('Invalid Equipment ID');

            $equip = CarrierEquipment::find($equipID);
            return response()->json([
                        'success' => 'Carrier Equipment loaded successfully',
                        'equipment' => $equip->toArray(),
                        'carrierId' => $equip->carrierId
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function handleCarrierEquipment(Request $request, $equipID = null, $carrierId = null) {
        try {
            $equipId = is_numeric($equipID) ? $equipID : $request->get('$equipID');
            $carrId = is_numeric($carrierId) ? $carrierId : $request->get('carrierId');

            $location = $request->get('location', []);
            $location = $this->_processGoogleLocation($location, 'equip_ownerpostal', 'equip_ownercity', 'equip_ownerstate', 'equip_ownercountry');
            
            if (!CarrierEquipment::where('id', $equipId)->count()) {
                $equip = new CarrierEquipment;
            } else {
                $equip = CarrierEquipment::findOrFail($equipId);
            }
            
            $eqpId = $request->get('equipId');
            $eqpType = $eqpId['id'];
            $ownerId = $request->get('ownerId');
            $ownType = $ownerId['id'];

            $equip->fill($location + $request->except(['equipId'],['ownerId']));
            $equip->carrierId = $carrId;
            $equip->equipId = $eqpType;
            $equip->ownerId = $ownType;

            //saving
            if (!$equip->save())
                throw new \Exception('Carrier Equipment not saved successfully');

            return response()->json(['success' => 'Carrier Equipment created / updated successfully',
                        'equipId' => $equip->id,
                        'carrierId' => $equip->carrierId]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    /*
     * Methods Tab 5     
     */
    protected function getCurrTypes() {
        try {
            /*$types = DB::table('m_codereference')->select('id', 'codeValue as label')
                    ->where('codeType', 'CURRENCY')
                    ->get();*/  
            $types = \App\CurrencyType::all(['name as label', 'id']);
                       
            return response()->json([
                        'success' => 'Currency loaded successfully',
                        'currencyTypes' => $types
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function listRemits(Request $request, array $fields = array()) {
        //processing filters and limits and other params
        $carrierId = $request->get('carrID', null);
        $orderBy = $request->get('orderBy', null);
        $orderType = $request->get('orderType', null);
        $limit = $request->get('limit', 1);
        $page = $request->get('page', 1);
		$groupBy = $request->get('groupBy');
        $offset = ($page - 1) * $limit;
        $remitData = [];
        $countSubRemits = 0;

        if ($carrierId != null) {
            $subRemits = DB::table('m_carrierremit')
                    ->where('carrierId', $carrierId);

            $countSubRemits = $subRemits->count();

            if (!empty($orderBy) && !empty($orderType))
                $subRemits->orderBy($orderBy, $orderType);
            else
                $subRemits->orderBy('remit_name', 'asc');
			
			if(!empty($groupBy) 
					&& in_array($groupBy, $this->_carrierRemitColumns)
			){
				$subRemits->groupBy($groupBy);
			}

            $remitData = $subRemits->take($limit)->skip($offset);

            if (count($fields))
                $remitData = $remitData->get(array_merge(['id'], $fields));
            else
                $remitData = $remitData->get();

            return response()->json(['success' => 'Remit loaded successfully', 'count' => $countSubRemits, 'results' => $remitData]);
        } else {
            return response()->json(['success' => 'Remit not loaded', 'count' => $countSubRemits, 'results' => $remitData]);
        }
    }

    protected function deleteRemit($remitID) {
        try {
            $remit = CarrierRemit::findOrFail($remitID);
            if ($remit->delete())
                return response()->json(['success' => 'Remit deleted successfully']);
            else
                return response()->json(['error' => 'Remit not deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getRemitDetails($remitID = null) {
        try {
            if (!is_numeric($remitID))
                throw new \Exception('Invalid Remit ID');

            $remit = CarrierRemit::find($remitID);
            return response()->json([
                        'success' => 'Carrier Remit loaded successfully',
                        'remit' => $remit->toArray(),
                        'carrierId' => $remit->carrierId
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function handleCarrierRemit(Request $request, $remitID = null, $carrierId = null) {
        try {
            $remId = is_numeric($remitID) ? $remitID : $request->get('$remitID');
            $carrId = is_numeric($carrierId) ? $carrierId : $request->get('carrierId');

            $location = $request->get('location', []);
            $location = $this->_processGoogleLocation($location, 'remit_postal', 'remit_city', 'remit_state', 'remit_country');
             
            if (!CarrierRemit::where('id', $remId)->count()) {
                $remit = new CarrierRemit;
            } else {
                $remit = CarrierRemit::findOrFail($remId);
            }
            
            $remit_currency = $request->get('remit_currency');
            $currType = $remit_currency['id'];

            $remit->fill($location + $request->except(['remit_currency']));
            $remit->carrierId = $carrId;
            $remit->remit_currency = $currType;

            //saving
            if (!$remit->save())
                throw new \Exception('Carrier Remit not saved successfully');

            return response()->json(['success' => 'Carrier Remit created / updated successfully',
                        'remitId' => $remit->id,
                        'carrierId' => $remit->carrierId]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    
    /*
     * Method to upload, delete files  
     */

    protected function handleBrandFileUpload(Request $request) {
        if ($request->hasFile('file') && $request->has('clientID')) {
            if ($request->file('file')->isValid()) {
                //validating the image file
                $v = \Validator::make(
                                $request->all(), ['file' => 'required|image', 'clientID' => 'required|numeric']
                );
                //checking
                if ($v->fails())
                    return response()->json(['error' => $v->errors()->all()]);
                try {
                    //finding the customer who is going to update the logo
                    $cust = Customer::findorFail($request->get('clientID'));
                    $oldLogo = isset($cust->wishList->wl_logo) ? $cust->wishList->wl_logo : null;

                    //uploading new logo and cleaning the old one if any
                    $result = $this->_handleUploadAndResize($request, 'brand_logo', 282, 53, true, $oldLogo);

                    if (isset($result['success']) && $result['success']) {
                        if ($cust->wishList()->count()) {
                            $wl = $cust->wishList()->first();
                            $wl->update(['wl_logo' => $result['fileName']]);
                        } else {
                            $wl = new CustomerWishList;
                            $wl->fill(['wl_logo' => $result['fileName']]);
                            $cust->wishList()->save($wl);
                        }
                        return response()->json([
                                    'success' => 'File uploaded successfully',
                                    'size' => isset($result['size']) ? $result['size'] : 0,
                                    'fileName' => isset($result['fileName']) ? $result['fileName'] : 0,
                                    'url' => isset($result['url']) ? $result['url'] : null
                        ]);
                    } else
                        return response()->json(['error' => 'File uploaded but not moved and saved']);
                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()]);
                }
            } else
                return response()->json(['error' => 'No valid file uploaded']);
        } else
            return response()->json(['error' => 'required information missing']);
    }

    protected function handleCarrierLogoUpload(Request $request) {
        if ($request->hasFile('file') && $request->has('carrierID')) {
            if ($request->file('file')->isValid()) {
                //validating the image file
                $v = \Validator::make(
                                $request->all(), ['file' => 'required|image',]
                );
                //checking
                if ($v->fails())
                    return response()->json(['error' => $v->errors()->all()]);
                try {
                    //creating or updating the logo
                    $id = $request->get('carrierID');
                    if (!empty($id)) {
                        $carrier = Carrier::findOrFail($id);
                    } else
                        $carrier = new Carrier;

                    //handling file upload and removal of the old one if any
                    $oldFile = isset($carrier->carrier_logo) ? $carrier->carrier_logo : null;

                    $result = $this->_handleUploadAndResize($request, 'carrier', 64, 64, true, $oldFile);

                    if (isset($result['success']) && $result['success']) {
                        $carrier->carrier_logo = $result['fileName'];

                        if ($carrier->save())
                            return response()->json(['success' => 'File uploaded and updated successfully', 'id' => $carrier->id]);
                        else
                            return response()->json(['error' => 'File not uploaded or updated successfully',]);
                    }
                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()]);
                }
            } else
                return response()->json(['error' => 'No valid file uploaded']);
        } else
            return response()->json(['error' => 'required information missing']);
    }

    private function _handleUploadAndResize(Request $request, $destinationType, $resizeWidth, $resizeHeight, $removeOld = false, $removeFile = null) {
        try {
            //generating random file name for this
            $ext = $request->file('file')->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $ext;
            $fileSize = $request->file('file')->getSize();
            //moving the file to its destination
            if ($request->file('file')->move(self::getUploadPath($destinationType), $fileName)) {

                if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png'])) {
                    //firstly cropping the image to fit using image library façade
                    \Image::make(self::getUploadPath($destinationType) . $fileName, array(
                        'width' => $resizeWidth,
                        'height' => $resizeHeight,
                            //'grayscale' => true
                    ))->save(self::getUploadPath($destinationType) . $fileName);
                }

                //now copying this file to the aws s3 bucket
                if (!$this->copyToS3Bucket(self::getUploadPath($destinationType) . $fileName, $fileName, self::getS3UploadFolder($destinationType)
                        ))
                    throw new \Exception('copying to S3 bucket failed');

                //removing the old file if any
                if ($removeOld && !empty($removeFile)) {
                    $this->_deleteFile($removeFile, $destinationType, true); //true to remove that resource from s3 bucket
                }

                return [
                    'success' => true,
                    'size' => $fileSize,
                    'fileName' => $fileName,
                    'url' => self::getAssetUrl($fileName, $destinationType),
                    'path' => self::getUploadPath(),
                    'filetype' => $ext,
                ];
            } else
                return ['success' => false,];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    private function _deleteFile($removeFileName, $destinationType, $removeFromS3Bucket = false) {
        try {
            //removing the old file if any	
            $removeFile = self::getUploadPath($destinationType) . $removeFileName;

            //deleting from s3 if switched on
            if ($removeFromS3Bucket) {
                if (!$this->deleteFromS3Bucket($removeFileName, self::getS3UploadFolder($destinationType)))
                    throw new \Exception('Deleting from s3 failed');
            }

            //deleting
            if (!is_dir($removeFile) && file_exists($removeFile))
                return unlink($removeFile);
            else
                return false;
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    protected function deleteFromS3Bucket($file, $folder = null) {
        try {
            $disk = \Storage::disk('s3');
            $path = empty($folder) ? $file : $folder . '/' . $file;

            if ($disk->delete($path))
                return true;
            else
                return false;
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    protected function copyToS3Bucket($fileLocation, $fileName, $s3BucketFolder = null) {
        try {
            $disk = \Storage::disk('s3');
            //copying the file to s3 bucket
            if (!empty($s3BucketFolder))
                $destinationFile = $s3BucketFolder . '/' . $fileName;
            else
                $destinationFile = $fileName;
            //copying
            //getting contents of the local file

            $contents = file_get_contents($fileLocation);
            if ($disk->put($destinationFile, $contents))
                if (!$disk->exists($destinationFile))
                    throw new \Exception('Uploading to S3 bucket has failed');
                else
                    return true;
            else
                return false;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public static function getS3UploadFolder($path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return self::BRAND_IMAGE_DIRECTORY;
            case 'carrier':
                return self::CARRIER_IMAGE_DIRECTORY;
            case 'docmanager':
                return self::DOCMANAGER_DIRECTORY;
            default:
                return null;
        }
    }

    public static function getUploadPath($path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::BRAND_IMAGE_DIRECTORY . DIRECTORY_SEPARATOR;
            case 'carrier':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::CARRIER_IMAGE_DIRECTORY . DIRECTORY_SEPARATOR;
            case 'docmanager':
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR . self::DOCMANAGER_DIRECTORY . DIRECTORY_SEPARATOR;
            default:
                return public_path() . DIRECTORY_SEPARATOR . self::UPLOAD_DIRECTORY
                        . DIRECTORY_SEPARATOR;
        }
    }

    public static function getAssetUrl($file, $path = 'default') {
        switch ($path) {
            case 'brand_logo':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::BRAND_IMAGE_DIRECTORY . '/' . $file);
            case 'carrier':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::CARRIER_IMAGE_DIRECTORY . '/' . $file);
            case 'docmanager':
                return asset(self::UPLOAD_DIRECTORY . '/' . self::DOCMANAGER_DIRECTORY . '/' . $file);
            default:
                return asset(self::UPLOAD_DIRECTORY . '/' . $file);
        }
    }

	
    protected function getCarriers() {
        try {
            
            $carriers = \App\Carrier::orderBy('carrier_name', 'ASC')->get(['carrier_name as label', 'scac', 'id'])->toArray();				
			//$first = ['0' => array('label' => 'None', 'id' => 0)];
			$first = [];
            $carriers =  array_merge($first,$carriers);  
            return response()->json(['success' => 'carriers loaded successfully', 'carriers' => $carriers]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }	
	
}
