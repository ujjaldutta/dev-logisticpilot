<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Carrier;
use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\DesignModel;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Ratecalc\Costperhunderd;
use App\Models\Ratecalc\Distance;
use App\Models\Ratecalc\Rateware;
use App\Models\Ratecalc\Costperunit;
use App\Models\Ratecalc\Ratematrix;
use App\Models\Profile\RateresultModel;

use App\Models\Distancecalc\Randmcnally;
use App\Models\Distancecalc\Pcmiler;
use App\Models\Distancecalc\Distancemile;
use App\Models\Distancecalc\Promile;

use App\Models\Accessorialcalc\Accessoccalc;
use App\Models\Fuelcalc\Fuel;
use App\Models\Carrierterminal\Carrierterminal;


use App\Models\Threading\Task\Search;
use App\Models\Threading\Multiple;
use Session;

use App\ModeType;
use App\Layout;

trait LookupServiceTrait {
	
	private $_sortMethod = ['asc', 'desc', ];
	private $_carrierGroupByColumns = ['carrierId', 'equipId', 'modeId', 'insuranceTypeId'];
	private $_carrierInsuranceColumns = ['insurance_policyname', 'insuranceTypeId'];
	private $_carrierRemitColumns = ['remit_name', ];
	private $_carrierModeColumns = ['comments', 'modeId', ];
	private $_carrierEquipmentColumns = ['equip_no', ];
	private $SECONDS_IN_A_MILLISECOND = 1000;
	private $options;
	
	public static function getMongoUTCDateTimeFromUnixTimestamp($timestamp) {
        return new \MongoDB\BSON\UTCDateTime(intval($timestamp * $this->SECONDS_IN_A_MILLISECOND));
    }

    public static function getUnixTimestampFromMongoUTCDateTime(\MongoDB\BSON\UTCDateTime $utc_date_time) {
        return intval((string) $utc_date_time / $this->SECONDS_IN_A_MILLISECOND);
    }
    
    
    protected function getCarriers($request) {
		$name=$request->get('q');
		
		
		$subCarriers = Carrier::select('scac as id', 'carrier_name as name');
		 if (isset($name)) {
                $subCarriers->where('m_carrier.carrier_name', 'LIKE', "{$name}%");
          
			$subCarriers->take(10)->skip(0);
			$carrierData = $subCarriers->get(); 
				
		
			return response()->json($carrierData);
		  }
	}
	
	protected function CreateCountry($rec){
		foreach($rec as $data){
			$postal = new CountryModel();

			$postal->create((array)$data);
			
		}
	}
	
	
	protected function CreatePostcode($rec){
		foreach($rec as $data){
			$postal = new PostalcodeModel();

			$postal->create((array)$data);
			
		}
	}
	
	
	protected function GetCountryloc(Request $request){
		
		$name=$request->get('q');
		
		
		$countrylist = CountryModel::select();
		 if (isset($name)) {
                $countrylist->where('m_country.country_code', 'LIKE', "{$name}%")->orwhere('country_name', 'LIKE', "{$name}%");
          
				$countrylist->take(10)->skip(0);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->country_code,"name"=>$rec->country_name);
				} 
       
         return response()->json(array("items"=>$clist));
           }
            return response()->json(array()); 
	}
	
	protected function GetCountryList(Request $request){
		
		$name=$request->get('q');
		
		
		$countrylist = CountryModel::select();
		 if (isset($name)) {
                $countrylist->where('m_country.country_code', 'LIKE', "{$name}%")->orwhere('country_name', 'LIKE', "{$name}%");
          
				$countrylist->take(10)->skip(0);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->country_code,"name"=>$rec->country_name,"value"=>$rec->country_name,"label"=>$rec->country_name);
				} 
       
         return response()->json(array("items"=>$clist));
           }
            return response()->json(array()); 
	}
	
	
	protected function StateList(Request $request){
		
		$name=$request->get('q');
		$country=$request->get('countrycode');
		
		$countrylist = PostalcodeModel::select('ProvinceAbbr','ProvinceName')->groupBy('ProvinceAbbr');
		 if (isset($name)) {
				$countrylist->where('ProvinceAbbr', 'LIKE',  "{$name}%")->orwhere('ProvinceName', 'LIKE', "{$name}%");
                $countrylist->where('CountryName', '=', $country);
          
				$countrylist->limit(10);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->ProvinceAbbr,"name"=>$rec->ProvinceName,"value"=>$rec->ProvinceName,"label"=>$rec->ProvinceName);
				} 
       
         return response()->json($clist);
           }
          return response()->json(array()); 
		
	}
	
	protected function GetCityList(Request $request){
		
		$name=$request->get('q');
		$country=$request->get('countrycode');
		$state=$request->get('statecode');
		
		$countrylist = PostalcodeModel::select('CityName')->groupBy('CityName');
		 if (isset($name)) {
				$countrylist->where('CityName', 'LIKE',  "{$name}%");
				if($country)
				 $countrylist->where('CountryName', '=', $country);
				 if($state)
                $countrylist->where('ProvinceName', 'LIKE', $state);
          
				$countrylist->limit(10);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->CityName,"name"=>$rec->CityName);
				} 
       
         return response()->json($clist);
           }
          return response()->json(array()); 
          
		
	}
	
	protected function fetchPostalList(Request $request){
		$name=$request->get('q');
		$country=$request->get('countrycode');
		$state=$request->get('statecode');
		$city=$request->get('city');
		
		$countrylist = PostalcodeModel::select('PostalCode')->groupBy('PostalCode');
		 if (isset($name)) {
				if($city)
				$countrylist->where('CityName', '=',  $city);
				if($country)
				 $countrylist->where('CountryName', '=', $country);
                if($state)
                $countrylist->where('ProvinceAbbr', '=', $state);
                
                $countrylist->where('PostalCode', 'Like',  "{$name}%");
          
				$countrylist->limit(10);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("value"=>$rec->PostalCode,"label"=>$rec->PostalCode,"title"=>$rec->PostalCode,"id"=>$rec->PostalCode,"name"=>$rec->PostalCode);
				} 
       
         return response()->json($clist);
           }
          return response()->json(array()); 
	}
	
	
	protected function PostalList(Request $request){
		$name=$request->get('q');
		$country=$request->get('countrycode');
	
		
		$countrylist = PostalcodeModel::select('PostalCode')->groupBy('PostalCode');
		 if (isset($name)) {
				
				if($country)
				 $countrylist->where('CountryName', '=', $country);
				
				$countrylist->where('PostalCode', 'Like',  "{$name}%");
          
				$countrylist->limit(10);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->PostalCode,"name"=>$rec->PostalCode,"value"=>$rec->PostalCode,"label"=>$rec->PostalCode);
				} 
       
         return response()->json(array("items"=>$clist));
           }
          return response()->json(array()); 
	}
	
		
	protected function StatecityList(Request $request){
		$name=$request->get('q');
		$country=$request->get('countrycode');
	
		
		$countrylist = PostalcodeModel::select('ProvinceName','CityName')->groupBy('CityName');
		 if (isset($name)) {
				
				if($country)
				 $countrylist->where('CountryName', '=', $country);
				
				$countrylist->where('CityName', 'Like',  "{$name}%");
          
				$countrylist->limit(10);
				$Data = $countrylist->get();
				$clist=array();
				foreach($Data as $rec){
				$clist[]=array("id"=>$rec->CityName,"name"=>$rec->CityName."-".$rec->ProvinceName,"value"=>$rec->CityName,"label"=>$rec->CityName."-".$rec->ProvinceName);
				} 
       
         return response()->json(array("items"=>$clist));
           }
          return response()->json(array()); 
	}
	
	 private function uploadDoc(Request $request, $destinationType, $resizeWidth, $resizeHeight, $removeOld = false, $removeFile = null) {
        try {
            //generating random file name for this
            $ext = $request->file('file')->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $ext;
            $fileSize = $request->file('file')->getSize();
            //moving the file to its destination
            if ($request->file('file')->move(self::getUploadPath($destinationType), $fileName)) {

                if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png'])) {
                    //firstly cropping the image to fit using image library façade
                    \Image::make(self::getUploadPath($destinationType) . $fileName, array(
                        'width' => $resizeWidth,
                        'height' => $resizeHeight,
                            //'grayscale' => true
                    ))->save(self::getUploadPath($destinationType) . $fileName);
                }

              

                //removing the old file if any
                if ($removeOld && !empty($removeFile)) {
                    $this->_deleteFile($removeFile, $destinationType, true); //true to remove that resource from s3 bucket
                }

                return [
                    'success' => true,
                    'size' => $fileSize,
                    'fileName' => $fileName,
                    'url' => self::getAssetUrl($fileName, $destinationType),
                    'path' => self::getUploadPath($destinationType),
                    'filetype' => $ext,
                ];
            } else
                return ['success' => false,];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
    
    
	
     private function _handleUploadAndResize(Request $request, $destinationType, $resizeWidth, $resizeHeight, $removeOld = false, $removeFile = null) {
        try {
            //generating random file name for this
            $ext = $request->file('file')->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $ext;
            $fileSize = $request->file('file')->getSize();
            //moving the file to its destination
            if ($request->file('file')->move(self::getUploadPath($destinationType), $fileName)) {

                if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png'])) {
                    //firstly cropping the image to fit using image library façade
                    \Image::make(self::getUploadPath($destinationType) . $fileName, array(
                        'width' => $resizeWidth,
                        'height' => $resizeHeight,
                            //'grayscale' => true
                    ))->save(self::getUploadPath($destinationType) . $fileName);
                }

                //now copying this file to the aws s3 bucket
                if (!$this->copyToS3Bucket(self::getUploadPath($destinationType) . $fileName, $fileName, self::getS3UploadFolder($destinationType)
                        ))
                    throw new \Exception('copying to S3 bucket failed');

                //removing the old file if any
                if ($removeOld && !empty($removeFile)) {
                    $this->_deleteFile($removeFile, $destinationType, true); //true to remove that resource from s3 bucket
                }

                return [
                    'success' => true,
                    'size' => $fileSize,
                    'fileName' => $fileName,
                    'url' => self::getAssetUrl($fileName, $destinationType),
                    'path' => self::getUploadPath(),
                    'filetype' => $ext,
                ];
            } else
                return ['success' => false,];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
    
    
    
  
	/*
	* Method to delete files from local file system depending on various location types 
	*
	*/
	private function _deleteFile($removeFileName, $destinationType, $removeFromS3Bucket = false){
		try{
			//removing the old file if any	
			$removeFile = self::getUploadPath($destinationType). $removeFileName;
			
			//deleting from s3 if switched on
			if($removeFromS3Bucket){
				if(!$this->deleteFromS3Bucket($removeFileName, self::getS3UploadFolder($destinationType)))
					throw new \Exception('Deleting from s3 failed');
			}
			
			//deleting
			if(!is_dir($removeFile) && file_exists($removeFile))
				return unlink($removeFile);
			else
				return false;
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	protected function deleteFromS3Bucket($file, $folder = null){
		try{
			$disk = \Storage::disk('s3');
			$path = empty($folder) ? $file : $folder. '/'. $file;
			
			if($disk->delete($path))
				return true;
			else
				return false;
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	protected function copyToS3Bucket($fileLocation, $fileName, $s3BucketFolder = null){
		try{
			$disk = \Storage::disk('s3');
			//copying the file to s3 bucket
			if(!empty($s3BucketFolder))
				$destinationFile = $s3BucketFolder. '/'. $fileName;
			else
				$destinationFile =  $fileName;
			//copying
			//getting contents of the local file
			
			$contents = file_get_contents($fileLocation);
			if($disk->put($destinationFile, $contents))
				if(!$disk->exists($destinationFile))
					throw new \Exception('Uploading to S3 bucket has failed');
				else
					return true;
			else
				return false;
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	
	
	
		/*
	* method to upload files to the destination and entry to doc manager
	*
	*/
	protected function handleUploadToDocManager(Request $request){
		if($request->hasFile('file') /*&& $request->has('id')*/){
				if($request->file('file')->isValid()){
					//validating the image file
					$v = \Validator::make(
						$request->all(), 
						['file' => 'required|mimes:pdf',]
					);
					//checking
					if($v->fails())
						return response()->json(['error' => $v->errors()->all()]);
					try{
						//creating or updating the document
						$id = $request->get('docId');
						if(!empty($id) && is_numeric($id)){
							$docManager = DocManager::findOrFail($id);
						}else
							$docManager = new DocManager;
						
						//handling file upload and removal of the old one if any
						$oldFile = isset($docManager->doc_filename) ? $docManager->doc_filename : null;
						
						$result = $this->_handleUploadAndResize($request, 'docmanager', 64, 64, true, $oldFile);
						
						if(isset($result['success']) && $result['success']){
							$docManager->doc_filename = $result['fileName'];
							$docManager->doc_path = $result['path'];
							$docManager->doc_filetype = $result['filetype'];
							$docManager->doc_size = $result['size'];
							$docManager->created_by = \Auth::id();
							
							if($docManager->save()){
								return json_encode(['success' => 'File uploaded and updated successfully', 'id' => $docManager->id, 'url' => self::getAssetUrl($result['fileName'], 'docmanager')]);
							}else
								return json_encode(['error' => 'File not uploaded or updated successfully', ]);
						}
					}catch(\Exception $e){
						return json_encode(['error' => $e->getMessage()]);
					}
				}else
					return json_encode(['error' => 'No valid file uploaded']);
			}else
				return json_encode(['error' => 'required information missing']);
	}
	  
    
    
	public static function getS3UploadFolder($path = 'default'){
		switch ($path){
			case 'brand_logo':
				return self::BRAND_IMAGE_DIRECTORY;
			case 'contract_doc':
				return self::CONTRACT_DIRECTORY;
			
			case 'carrier':
				return self::CARRIER_IMAGE_DIRECTORY;
			case 'docmanager':
				return self::DOCMANAGER_DIRECTORY;
			default: 
				return null;
		}
	}
	
	public static function getUploadPath($path = 'default'){
		switch ($path){
			case 'brand_logo':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::BRAND_IMAGE_DIRECTORY .DIRECTORY_SEPARATOR;
			case 'contract_doc':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::CONTRACT_DIRECTORY .DIRECTORY_SEPARATOR;
			case 'carrier':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::CARRIER_IMAGE_DIRECTORY .DIRECTORY_SEPARATOR;
			case 'docmanager':
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY 
					.DIRECTORY_SEPARATOR .self::DOCMANAGER_DIRECTORY .DIRECTORY_SEPARATOR;
			default: 
				return public_path(). DIRECTORY_SEPARATOR .self::UPLOAD_DIRECTORY
					.DIRECTORY_SEPARATOR;
		}
	}
	
	public static function getAssetUrl($file, $path = 'default'){
		switch ($path){
			case 'brand_logo':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::BRAND_IMAGE_DIRECTORY. '/'. $file);
			case 'contract_doc':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::CONTRACT_DIRECTORY. '/'. $file);
			case 'carrier':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::CARRIER_IMAGE_DIRECTORY. '/'. $file);
			case 'docmanager':
				return asset(self::UPLOAD_DIRECTORY. '/'. self::DOCMANAGER_DIRECTORY. '/'. $file);
			default: 
				return asset(self::UPLOAD_DIRECTORY. '/'. $file);
		}
	}
	
	 protected function getAssoc($request) {
		$name=$request->get('q');
		
		
		
		$accsTypes = \App\Accessorial::select('accscode as id', 'accsname as name')
		->where('accstype', \Config::get('app.accsViewableType'));
		
		 if (isset($name)) {
                $accsTypes->where('accsname', 'LIKE', "{$name}%");
          
			$accsTypes->take(10)->skip(0);
			$assocData = $accsTypes->get(); 
				

			return response()->json(array("assoctype"=>$assocData));
		  }
	}
	
	
	 protected function getRefcode($request) {
		$name=$request->get('q');
		
		
		
		$codeTypes = \App\CodeReference::select('codeId as id','codeValue as name')
		->where('codeType', $name);
		
		
			$codeData = $codeTypes->get()->toArray(); 
				

			return response()->json($codeData);
		 
	}
	
	protected function getFuelType() {
	
		$Types = \App\FuelType::select('id','FuelType as name');
		$Types = $Types->get(); 
		return response()->json($Types);
		
	}
	
	
	protected function getDesign($request){
		$name=$request->get('vendor_id');
		
	/*	$File = self::getUploadPath('carrier').'eestes-logo.png';
		$imagedata = file_get_contents($File);
             // alternatively specify an URL, if PHP settings allow
	echo $base64 = base64_encode($imagedata);
	exit;*/
		$design = DesignModel::select('image','color_code','customer_name',"endpoint_url");
		
		 if (isset($name)) {
                $design->where('vendor_id', '=', "{$name}");
          
			
			$designData = $design->get(); 
				

			return response()->json($designData);
		  }
	}
	
	protected function ProfileList($request,$fields){
		$filters = is_object($request)?$request->get('filters'):null;
		$profiles=ProfileModel::with('Contractors')->where('Active','true')->orderBy('created_at', 'desc');
		
		if(is_array($filters)){
				
				foreach($filters as $filter){
					
					if(isset($filter['name'])){
						$name=$filter['name'];
						$profiles->where('RateProfileDescription', 'LIKE', "%".$name."%");
					}
					
				}
			}
			
		
		$select=$fields['fields'];
		$profiles->select($select);
		$clientId=\CustomerRepository::getCurrentCustomerID();
		$profiles->where('clientID',(int)$clientId);
		
		$profiles=$profiles->get();
		return $profiles;
	}
	
	protected function ContractList($request,$fields){
		$orderBy = $request->input('orderBy', null);
	 $orderType = $request->input('orderType', null);
	 
	 $filters = $request->get('filters');
	
	 
	 $limit = $request->get('limit', 1);
     $page = $request->get('page', 1);
     $offset = ($page - 1) * $limit;
        
	   
	   $contract = ContractModel::where('ProfileId', '=', $request->input('RateProfileCode'))->take($limit)->skip($offset);
	   if(!empty($orderBy) && !empty($orderType))
			$contract->orderBy($orderBy, $orderType);
		else
			$contract->orderBy('carrierId', 'asc');
		if(is_array($filters)){
			
			foreach($filters as $filter){
				
				if(isset($filter['carrier'])){
					$name=$filter['carrier'];
					$contract->where('carrierId', 'LIKE', "%".$name."%");
				}
				if(isset($filter['EffectiveFrom'])){
					$name=$filter['EffectiveFrom'];
					$contract->where('EffectiveFrom', '>=',$name );
				}
				if(isset($filter['EffectiveTo'])){
					$name=$filter['EffectiveTo'];
					$contract->where('EffectiveTo', '<=',$name );
				}
				if(isset($filter['Month'])){
					if($filter['Month']=='all'){
						$date=date('m-d-Y', strtotime("+3 months", time()));
						$contract->where('EffectiveTo', '<=',$date );
					}
					elseif($filter['Month']=='1'){
						$date=date('m-d-Y', strtotime("+1 months", time()));
						$contract->where('EffectiveTo', '<=',$date );
					}
					elseif($filter['Month']=='2'){
						$date=date('m-d-Y', strtotime("+2 months", time()));
						$contract->where('EffectiveTo', '<=',$date );
					}
					elseif($filter['Month']=='3'){
						$date=date('m-d-Y', strtotime("+3 months", time()));
						$contract->where('EffectiveTo', '<=',$date );
					}
					
				}
				
			}
		}
		
		$contract->where('clientID',\CustomerRepository::getCurrentCustomerID());
		$select=$fields['fields'];
		
		$contract->select($select);
		//print_r($select);
		//print_r($contract->get());
	   return $contract=$contract->get();
	}
	
	protected function ProfileById($id,$fields){
		$profile=ProfileModel::where('RateProfileCode', '=', $id)->select($fields)->get();
		return $profile->toArray();
	}
	
	protected function ContractById($id){
		$contract=ContractModel::where('_id', '=', $id)->first()->toArray();
		
		return $contract;
	}
	
	protected function RatelaneList($request,$fields){
		$orderBy = $request->input('orderBy', null);
	 $orderType = $request->input('orderType', null);
	 $filters = $request->get('filters');
	 $limit = $request->get('limit', 1);
     $page = $request->get('page', 1);
    
     
     $offset = ($page - 1) * $limit;
        
	   
	   $lanerate = LaneModel::where('ProfileId', '=', $request->input('ProfileId'))->where('cId', '=', $request->input('carrierId'));
	   if(!empty($orderBy) && !empty($orderType))
			$lanerate->orderBy($orderBy, $orderType);
		else
			$lanerate->orderBy('_id', 'asc');
		
		$lanerate->where('clientID',\CustomerRepository::getCurrentCustomerID());
		
		if(is_array($filters)){
			
			foreach($filters as $filter){
				
				if(isset($filter['Statecode']) && !empty($filter['Statecode'])){
					$name=$filter['Statecode'];
					
					
					$query = array('OriginalState'=> "{$name}" );
					$lanerate->where($query, 'exists', true);
					
				}
				if(isset($filter['Statecode']) && !empty($filter['Statecode'])){
					$name=$filter['Statecode'];
					
					
					$query = array('DestinationState'=> "{$name}" );
					///$lanerate->orWhere($query, 'exists', true);
					
				}
				
				if(isset($filter['Citycode']) && !empty($filter['Citycode'])){
					$name=$filter['Citycode'];
					
					$query = array('OriginalCity'=> "{$name}" );
					$lanerate->where($query);
					
				}
				if(isset($filter['Citycode']) && !empty($filter['Citycode'])){
					$name=$filter['Citycode'];
					
					$query = array('DestinationCity'=> "{$name}" );
					//$lanerate->where($query);
					
				}
			}
		}
		$select=$fields['fields'];
		$lanerate->select($select);
		$lanerate->take($limit)->skip($offset);	
	   return $lanerate=$lanerate->get();
	}
	
	protected function getCarrierAPI($request) {
		$name=$request->get('q');
		
		
		$subCarriers = Carrier::with("customers")->whereHas('customers',  function($query)  {
    $query->where('clientID', '=', (int)\CustomerRepository::getCurrentCustomerID());})->select('scac as id', 'carrier_name as name');
		 if (isset($name)) {
                $subCarriers->where('m_carrier.carrier_name', 'LIKE', "{$name}%");
          
			$subCarriers->take(10)->skip(0);
			$carrierData = $subCarriers->get(); 
				

			return response()->json(array("items"=>$carrierData));
		  }
	}
	
	protected function ContractCarrierList($request,$fields){
	
	 $filters = $request->get('filters');
	
	 $ProfileId=$request->input('profile');
	   
	  if($ProfileId=='') return; 
	  
	  $contract = ContractModel::where('ProfileId', '=', $ProfileId);
	
		
	  $contract->where('clientID',\CustomerRepository::getCurrentCustomerID());
	  $select=$fields['fields'];
		
	  $contract->select($select);
		
	   return $contract=$contract->get();
	   
	   
		
	}
	
	
	
	
   }
