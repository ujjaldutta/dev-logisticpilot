<?php
namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Customer;
use App\Accessorial;

trait AccessorialServicesTrait {
    
    protected function listAccessorialForUserData(Request $request, array $fields = array()) {
        $search = $request->get('q');
		
			//if(!empty($search) && strlen($search) > 1)
			//	$customeritems->where('item_name', 'LIKE', "%{$search}%");	
			$accessritems = Accessorial::where('id', '!=', '0');	
            if(!empty($search) && strlen($search) > 1) {
                //$subCarriers->where('m_carrier.accsname', 'LIKE', "%{$search}%");
				$accessritems->where('accsname', 'LIKE', "%{$search}%");	
            }

		
			$accessritems = $accessritems->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'accscode', 'accsname', 
				/*'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($accessritems as $key => $accessitem){
				$processedData [$key] = [
					'id' => $accessitem->id,
					'accscode' => $accessitem->accscode,
					'item_name' => $accessitem->accsname,
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,*/
					/*'description' => $customeritem->item_weight. ', '. $customeritem->item_length. ', '. $customeritem->item_width. ', '. $customeritem->item_height. ', '.  $customeritem->item_class,*/
				] + $accessitem->toArray();
			}
			
			return response()->json($processedData);			
		
    }	
        
}
