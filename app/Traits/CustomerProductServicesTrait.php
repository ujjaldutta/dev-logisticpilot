<?php
namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\ClientItem;

trait CustomerProductServicesTrait {
    
    protected function listProducts(Request $request, array $fields=array()){
		//processing filters and limits and other params
		$filters = $request->get('filters');
		$filters = json_decode($filters);		
		$orderBy = $request->get('orderBy', null);
		$orderType = $request->get('orderType', null);
		$limit = $request->get('limit', 1);
		$page = $request->get('page', 1);
		$offset = ($page - 1) * $limit;
		
		$clientID = $request->get('clientID');
		//
		//if the sent client id is blank get the system set customer ID
		if(empty($clientID) || !is_numeric($clientID))
			$clientID = \CustomerRepository::getCurrentCustomerID();
		
		$customerProducts = DB::table('m_clientitem')
                        ->where('clientId', $clientID);
		
		foreach($filters as $filter){			
                    if(isset($filter->name)){
                        $customerProducts->where('item_name', 'LIKE', "%{$filter->name}%");
                    }
                    
                    if(isset($filter->nmfc)){
                        $customerProducts->where('item_nmfc', 'LIKE', "%{$filter->nmfc}%");
                    }
			
	            if(isset($filter->classTypes) && $filter->classTypes != 0){
                        $customerProducts->where('item_class', $filter->classTypes);
                    }
		};
                
                if (in_array('codeValue', $fields)) {
                    $customerProducts->join('m_codereference', 'm_clientitem.item_class', '=', 'm_codereference.id');
                }
		
		$countcustomerProducts =  $customerProducts->count() ;
		
		if(!empty($orderBy) && !empty($orderType))
                    $customerProducts->orderBy($orderBy, $orderType);
		else
                    $customerProducts->orderBy('id', 'asc');
		
		$productData = $customerProducts->take($limit)->skip($offset);
		
		if(count($fields))
			$productData = $productData->get(array_merge(['m_clientitem.id'], $fields));
		else
			$productData = $productData->get();
        
		return response()->json(['success' => 'Items loaded successfully', 'count' => $countcustomerProducts, 'results' => $productData]);
	}
        
	protected function listProductdata(Request $request){
	    if($request->get('autocomplete') == 'true')
	    {	
		    $search = $request->get('q');
			$clientID = $request->get('clientID');
			//
			//if the sent client id is blank get the system set customer ID
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			$customeritems = ClientItem::where('clientId', $clientID);	
			if(!empty($search) && strlen($search) > 1)
				$customeritems->where('item_name', 'LIKE', "%{$search}%");	

			$customeritems = $customeritems->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'item_name', 'clientId', 
				'item_weight', 'item_length',
				'item_width', 'item_height',
				'item_dimuomtype', 'item_class',
				'item_nmfc', 'item_custnum', 'item_qty',
				/*'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($customeritems as $key => $customeritem){
				$processedData [$key] = [
					'id' => $customeritem->id,
					'clientId' => $customeritem->clientId,
					'item_name' => $customeritem->item_name,
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,*/
					/*'description' => $customeritem->item_weight. ', '. $customeritem->item_length. ', '. $customeritem->item_width. ', '. $customeritem->item_height. ', '.  $customeritem->item_class,*/
				] + $customeritem->toArray();
			}
			
			return response()->json($processedData);	
       }			
	}	
		
		
    protected function getWeightTypes() {
        try {
            $weightTypes = \App\WeightType::all(['name as label', 'id']);

            return response()->json(['success' => 'Weight Type loaded successfully', 'weightTypes' => $weightTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }    
    
    protected function getCommodityTypes() {
        try {
            $commodityTypes = \App\CommodityType::all(['name as label', 'id']);

            return response()->json(['success' => 'Commodity Type loaded successfully', 'commodityTypes' => $commodityTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getDimmensionsTypes() {
        try {
            $dimmensionsTypes = \App\DimmensionsType::all(['name as label', 'id']);

            return response()->json(['success' => 'Dimmensions Type loaded successfully', 'dimmensionsTypes' => $dimmensionsTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getTemperatureTypes() {
        try {
            $temperatureTypes = \App\TemperatureType::all(['name as label', 'id']);

            return response()->json(['success' => 'Temperature Type loaded successfully', 'temperatureTypes' => $temperatureTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getFreeClassTypes() {
        try {
            $freeClassTypes = \App\FreeClassType::all(['name as label', 'id']);

            return response()->json(['success' => 'Class Type loaded successfully', 'freeClassTypes' => $freeClassTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getClassTypes() {
        try {
            $types = \App\FreeClassType::all(['name as label', 'id'])->toArray();            
            $first = ['0' => array('label' => 'None', 'id' => 0)];
            
            $freeClassTypes =  array_merge($first,$types);          
            
            return response()->json(['success' => 'Class Type loaded successfully', 'classTypes' => $freeClassTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    protected function getPackageTypes() {
        try {
           $types = \App\ItemType::all(['item_name as label', 'item_code as id']);
            
            return response()->json(['success' => 'Class Type loaded successfully', 'packageTypes' => $types]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }   
	
    protected function getPackageTypeList(Request $request, array $fields=array()) {
        if($request->get('autocomplete') == 'true')
	    {
		    $search = $request->get('q');
			$clientID = $request->get('clientID');
			//
			//if the sent client id is blank get the system set customer ID
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			$itemtypes = \App\ItemType::where('item_code', '!=', '0');	
			if(!empty($search) && strlen($search) > 1)
				$itemtypes->where('item_name', 'LIKE', "%{$search}%");	

			$itemtypes = $itemtypes->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'item_code', 'item_name',
				/*'client_firstname', 'client_lastname',
				'client_adr1', 'client_city',
				'client_state', 'client_postal',
				'client_country', 'client_postal',
				'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($itemtypes as $key => $itemtype){
				$processedData [$key] = [
					'id' => $itemtype->item_code,
					'billto_name' => $itemtype->item_name, 
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,
					'description' => $client->client_city. ', '. $client->client_state. ', '. $client->client_postal. ', '.  $client->client_country,*/
				] + $itemtype->toArray();
			}
			
			return response()->json($processedData);
				
		}
    } 	
	
    protected function getFreeClassTypeList(Request $request, array $fields=array()) {
        if($request->get('autocomplete') == 'true')
	    {
		    $search = $request->get('q');
			$clientID = $request->get('clientID');
			//
			//if the sent client id is blank get the system set customer ID
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			$classtypes = \App\FreeClassType::where('id', '!=', '0');	
			if(!empty($search) && strlen($search) > 1)
				$classtypes->where('name', 'LIKE', "%{$search}%");	

			$classtypes = $classtypes->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'name',
				/*'client_firstname', 'client_lastname',
				'client_adr1', 'client_city',
				'client_state', 'client_postal',
				'client_country', 'client_postal',
				'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($classtypes as $key => $classtype){
				$processedData [$key] = [
					'id' => $classtype->id,
					'name' => $classtype->name, 
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,
					'description' => $client->client_city. ', '. $client->client_state. ', '. $client->client_postal. ', '.  $client->client_country,*/
				] + $classtype->toArray();
			}
			
			return response()->json($processedData);
				
		}
    } 	   

	  
    protected function saveProductDetails(Request $request){
	$clientId = $request->get('clientId');
        		
	if(empty($clientId) || !is_numeric($clientId))
            throw new \Exception('Invalid client / customer id provided');
		
	if(!$request->has('id')) {
            $clientProduct = new ClientItem;
	} else {
            $clientProduct = ClientItem::findOrFail($request->get('id'));
        }
        
        $item_class = $request->get('item_class');
        $itemClass = $item_class['id'];
        $item_weightType = $request->get('item_weightType');
        $weightType = $item_weightType['id'];        
        $item_packagetypeid = $request->get('item_packagetypeid');
        $packagetype = $item_packagetypeid['id'];
        $item_commodity = $request->get('item_commodity');
        $commodity = $item_commodity['id'];
        $item_temperaturetype = $request->get('item_temperaturetype');
        $temp = $item_temperaturetype['id'];   
        $item_temperaturetype = $request->get('item_temperaturetype');
        $temp = $item_temperaturetype['id']; 
        $item_dimuomtype = $request->get('item_dimuomtype');
        $dim = $item_dimuomtype['id'];
        
        $clientProduct->fill($request->except(['clientId'], ['item_class'], ['item_packagetypeid'], ['item_commodity'], ['item_temperaturetype'], ['item_dimuomtype']));
        $clientProduct->clientId = $clientId;
        $clientProduct->item_class = $itemClass;
        $clientProduct->item_weightType = $weightType;
        $clientProduct->item_packagetypeid = $packagetype;
        $clientProduct->item_commodity = $commodity;
        $clientProduct->item_temperaturetype = $temp;
        $clientProduct->item_dimuomtype = $dim;
        $clientProduct->item_createdby = \Auth::id();
	
        return $clientProduct->save() ? $clientProduct->id : false;  	
    }
    
    protected function getProductDetails($id){
	$clientProductDetails = ClientItem::findOrFail($id);
		
	return $clientProductDetails;
    }
    
    protected function deleteClientProduct($id){
	$productDetails = ClientItem::findOrFail($id);		
	return $productDetails->delete();
    }
}
