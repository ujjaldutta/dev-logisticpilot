<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\CodeReference;

trait CodeReferenceServicesTrait {

    protected function listReferenceCodes(Request $request, array $fields = array()) {
        //processing filters and limits and other params
        $filters = $request->get('filters');
        $filters = json_decode($filters);
        //print_r($filters);
        //limit
        $orderBy = $request->get('orderBy', null);
        $orderType = $request->get('orderType', null);
        $limit = $request->get('limit', 1);
        $page = $request->get('page', 1);
        $offset = ($page - 1) * $limit;

        $codes = DB::table('m_codereference');

        foreach ($filters as $filter) {
            if (isset($filter->code)) {
                $codes->where('codeId', $filter->code);
            }
            
            if (isset($filter->desc)) {
                $codes->where('codeValue', 'LIKE', "%{$filter->desc}%");
            }
            
            if (isset($filter->codetype)) {
                $codes->where('codeType',  $filter->codetype);
            }
            
        };

        $countcodes = $codes->count();

        if (!empty($orderBy) && !empty($orderType))
            $codes->orderBy($orderBy, $orderType);
        else
            $codes->orderBy('Id', 'asc');

        $codeReferenceData = $codes->take($limit)->skip($offset);

        if (count($fields))
            $codeReferenceData = $codeReferenceData->get(array_merge(['Id'], $fields));
        else
            $codeReferenceData = $codeReferenceData->get();
       
        return response()->json(['success' => 'Codes loaded successfully', 'count' => $countcodes, 'results' => $codeReferenceData]);
    }

    protected function getCodeTypes() {
        try {
            $types = DB::table('m_codereference')
                    ->select('codeType')
                    ->distinct()
                    ->get();
            $first = ['0' => ''];

            $codeTypes = array_merge($first, $types);

            return response()->json(['success' => 'Types loaded successfully', 'types' => $codeTypes]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    protected function saveCodeDetails(Request $request) {
        
        if ($request->get('id') == null ) {
            $codeReference = new CodeReference;
        } else {
            $codeReference = CodeReference::findOrFail($request->get('id'));
        }

        $codeReference->fill($request->except(['id', 'Id']));
        if($codeReference->save()) {
            $id = $codeReference->id;
        } else
            $id = 0;
        
        return $id;
    }

    protected function getCodeDetails($id) {
        $codeReferenceDetails = CodeReference::findOrFail($id);

        return $codeReferenceDetails;
    }

    protected function deleteCode($id) {
        
        return DB::table('m_codereference')->where('Id', $id)->delete();
    }

}
