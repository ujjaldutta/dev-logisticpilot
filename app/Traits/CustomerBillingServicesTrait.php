<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\ClientBillingInfo;
//use App\LocationType;

trait CustomerBillingServicesTrait {
		
	protected function listBillingInfo(Request $request, array $fields=array()){
	    if($request->get('autocomplete') == 'true')
	    {
		    $search = $request->get('q');
			$clientID = $request->get('clientID');
			//
			//if the sent client id is blank get the system set customer ID
			if(empty($clientID) || !is_numeric($clientID))
				$clientID = \CustomerRepository::getCurrentCustomerID();
			
			$clientbillinginfo = ClientBillingInfo::where('clientID', $clientID);	
			if(!empty($search) && strlen($search) > 1)
				$clientbillinginfo->where('billto_name', 'LIKE', "%{$search}%");	

			$clientbillinginfo = $clientbillinginfo->with([
				/* 'wishList' => function($query){
					$query->select(['id', 'clientID', 'wl_logo']);
				}, */
			])->get([
				'id', 'billto_name', 'clientID', 
				'billto_adr1', 'billto_adr2',
				'billto_city', 'billto_state',
				'billto_postal', 'billto_country',
				/*'client_country', 'client_postal',
				'client_email', 'client_postal',*/
			]);
			
			$processedData = [];
			foreach($clientbillinginfo as $key => $clientbilling){
				$processedData [$key] = [
					'id' => $clientbilling->id,
					'clientID' => $clientbilling->clientID,
					'billto_name' => $clientbilling->billto_name, 
					/*'name' => $client->client_firstname. ' '. $client->client_lastname,*/
					'description' => $clientbilling->billto_city. ', '. $clientbilling->billto_state. ', '. $clientbilling->billto_postal. ', '.  $clientbilling->billto_country,
				] + $clientbilling->toArray();
			}
			
			return response()->json($processedData);
				
		}
	     else {

		    return response()->json([]);
	  }
	}
	
	/*protected function saveLocationDetails(Request $request){
		$clientId = $request->get('clientId');
		
		if(empty($clientId) || !is_numeric($clientId))
			throw new \Exception('Invalid client / customer id provided');
		
		if(!$request->has('id')){
			$clientLocation = new ClientLocation;
			$clientLocation->location_code = isset(\CustomerRepository::getCurrentCustomer()['client_compname']) ? 
			$this::generateClientLocationCode(\CustomerRepository::getCurrentCustomer()['client_compname'], $request->get('location_name'), ClientLocation::count()) : null;
		}else
			$clientLocation = ClientLocation::findOrFail($request->get('id'));
		
		$clientLocation->fill($request->except(['clientId']));
		$clientLocation->clientId = $clientId;
		
		$location = $this->_processGoogleLocation($request->get('location_location'), 'location_postal', 'location_city', 'location_state', 'location_country');
		
		$clientLocation->location_postal = isset($location['location_postal']) ? $location['location_postal'] : $clientLocation->location_postal;
		$clientLocation->location_city = isset($location['location_city']) ? $location['location_city'] : $clientLocation->location_city;
		$clientLocation->location_state = isset($location['location_state']) ? $location['location_state'] : $clientLocation->location_state;
		$clientLocation->location_country = isset($location['location_country']) ? $location['location_country'] : $clientLocation->location_country;
		
		$clientLocation->location_createdbyId = \Auth::id();
		
		//processing start and end time
		//$startTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursfrom'));
		$startTime = new \DateTime($request->get('location_hoursfrom'));
		//dd($request->get('location_hoursfrom'));
		$clientLocation->location_hoursfrom = $startTime ? $startTime->format('H:i:s') : null;
		
		//$endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $request->get('location_hoursto'));
		$endTime = new \DateTime($request->get('location_hoursto'));
		$clientLocation->location_hoursto = $endTime ? $endTime->format('H:i:s') : null;
		
		
		return $clientLocation->save() ? $clientLocation->id : false;
	}
	
	protected function getLocationDetails($id){
		$clientLocationDetails = ClientLocation::findOrFail($id);
		
		$startTime = \DateTime::createFromFormat('y-m-d H:i:s', date('y-m-d'). ' '. $clientLocationDetails->location_hoursfrom);
		$endTime = \DateTime::createFromFormat('y-m-d H:i:s', date('y-m-d'). ' '. $clientLocationDetails->location_hoursto);
		
		
		
		$clientLocationDetails->location_hoursfrom = $startTime ? $startTime->format(\DateTime::RFC2822) : null;
		$clientLocationDetails->location_hoursto = $endTime ? $endTime->format(\DateTime::RFC2822) : null;
		
		return $clientLocationDetails;
	}
	
	protected function deleteClientLocation($id){
		$locationDetais = ClientLocation::findOrFail($id);
		
		return $locationDetais->delete();
	}
	*/
	/* method to generate the client code based on parent company name */
	/*public static function generateClientLocationCode($parentCompanyName, $currentLocationName, $currentCount){
		$compnameInitial = strtoupper(substr($parentCompanyName, 0, 1));
		$locNameInitial = strtoupper(substr($currentLocationName, 0, 1));
		$nextCount = sprintf("%08d", $currentCount +1);
		return $compnameInitial.$locNameInitial.$nextCount;
	}*/
}