<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Carrier;
use App\CarrierAPI;
use App\Models\Profile\PostalcodeModel;
use App\Models\Profile\CountryModel;
use App\Models\Profile\DesignModel;
use App\Models\Profile\ProfileModel;
use App\Models\Profile\ContractModel;
use App\Models\Profile\LaneModel;

use App\Models\Ratecalc\Costperhunderd;
use App\Models\Ratecalc\Distance;
use App\Models\Ratecalc\Rateware;
use App\Models\Ratecalc\Costperunit;
use App\Models\Ratecalc\Ratematrix;
use App\Models\Profile\RateresultModel;

use App\Models\Distancecalc\Randmcnally;
use App\Models\Distancecalc\Pcmiler;
use App\Models\Distancecalc\Distancemile;
use App\Models\Distancecalc\Promile;

use App\Models\Accessorialcalc\Accessoccalc;
use App\Models\Fuelcalc\Fuel;
use App\Models\Transitcalc\Carrierterminal;
use App\Models\Ratecalc\APIThread;

/*use App\Models\Ratingdata\Tclass\ratrequestproducts;
use App\Models\Rating\Tclass\raterequest;
use App\Models\Rating\switchcasemodel;
use App\Models\Ratingdata\Tclass\ratedatarequest;
use App\Models\Ratingdata\switchcasemodeldata;
use App\Models\Tracking\Tclass\trackingrequest;
use App\Models\Tracking\Trackingswitchcasemodel;
*/

use App\Models\Threading\Task\Search;
use App\Models\Threading\Multiple;
use Session;

use App\ModeType;
use App\Layout;

trait SearchServiceTrait {
	
	
	
	protected function ProfileList($request,$fields){
		$filters = is_object($request)?$request->get('filters'):null;
		$profiles=ProfileModel::with('Contractors')->where('Active','true')->orderBy('created_at', 'desc');
		
		if(is_array($filters)){
				
				foreach($filters as $filter){
					
					if(isset($filter['name'])){
						$name=$filter['name'];
						$profiles->where('RateProfileDescription', 'LIKE', "%".$name."%");
					}
					
				}
			}
			
		
		$select=$fields['fields'];
		$profiles->select($select);
		
		$profiles->where('clientID',\CustomerRepository::getCurrentCustomerID());
		
		$profiles=$profiles->get();
		return $profiles;
	}
	
private function distance($lat1, $lon1, $lat2, $lon2, $unit) { 
 
	  $theta = $lon1 - $lon2; 
	  $dist = SIN(DEG2RAD($lat1)) * SIN(DEG2RAD($lat2)) +  COS(DEG2RAD($lat1)) * COS(DEG2RAD($lat2)) * COS(DEG2RAD($theta)); 
	  $dist = ACOS($dist); 
	  $dist = RAD2DEG($dist); 
	  $miles = $dist * 60 * 1.1515;
	  $unit = STRTOUPPER($unit);
	 
	  if ($unit == "K") {
		return ($miles * 1.609344); 
	  } elseif($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
	
	
	
//Calculate and search engine
	
	protected function SearchProfile($request){
		
	
				
				$ProfileId=$request->input('profile');
				$carriers=array();
				$contracts = $this->searchCarriers($request,$ProfileId);
				
				
			
				 $cid=array();
				
				 
				//return ($contracts);
				foreach($contracts as $contract){
						$ratetype=$contract->RateType['id'];
						$cid=$contract->_id;
						$carriers[]=array('carrierId'=>$contract->carrierId,'carriercode'=>$contract->carriercode);
						
						//$this->calculateDistance($request,$contracts);
						$source=$request->input('source');
						$destination=$request->input('destination');
						$sourceloc=$source['geometry']['location'];
						$destloc=$destination['geometry']['location'];
						
						
						$distanceapp=$contract->DistanceApp['id']; 
					
					switch($distanceapp)
						{
								//Rand McNally
								case 1:
								
								$distance=$this->distance($sourceloc['lat'],$sourceloc['lng'],$destloc['lat'],$destloc['lng'],'k');
								
								break;
								
								//PC Miler
								case 2:
								
								$distance=$this->distance($sourceloc['lat'],$sourceloc['lng'],$destloc['lat'],$destloc['lng'],'k');
								break;
								
								//Distance Mile
								case 3:
								
								
								$distance=$this->distance($sourceloc['lat'],$sourceloc['lng'],$destloc['lat'],$destloc['lng'],'k');
								break;
								
								//Pro Mile
								case 4:
								
								$distance=$this->distance($sourceloc['lat'],$sourceloc['lng'],$destloc['lat'],$destloc['lng'],'k');
								
								break;
								
								default:
								
								break;
								
								
							}
						
						
						switch($ratetype)
						{
								//distance (Mile/km)
								case 2:
									$this->options['process'][]=
										new Distance($request,$contract,$distance);
										
									
									
									break;
															
								//rateware
								case 3:
									$this->options['process'][]=
										new Rateware($request,$contract);
									;
								
								break;
								//cost per unit
								case 4:
									$this->options['process'][]=
										new Costperunit($request,$contract);
									;
									
								break;
								//rate matrix
								case 6:
										$this->options['process'][]=
											new Ratematrix($request,$contract);
										
								
								break;
								//cost per hundred
								case 8:
									$this->options['process'][]=
								
										new Costperhunderd($request,$contract);
									
									
								break;
								
								default:
								
								break;
						
						}
						
			
				
				}
				
				
				$this->getTerminal($request,$contracts);
				$this->searchCarriersAPI($request,$ProfileId);
				
			
				
				
				$task=new Search($this->options['process']);
				$multithreadManager = new Multiple();
				$multithreadManager->start($task);
				
				$this->options['process']=array();
				
				return $carriers;
		
	}
	
	protected function searchCarriers($request,$ProfileId=null){
		/*DB::connection('mongodb')->enableQueryLog();
		DB::connection('mongodb')->listen(function ($sql, $bindings, $time) {
			Log::info($sql . var_export($bindings, true));
		});
		*/
		$clientId=\CustomerRepository::getCurrentCustomerID();
		$shipdate=$request->input('shipdate');
		$source=$request->input('source');
		$source_address=$this->location($source['address_components']);
		
		
		$destination=$request->input('destination');
		$destination_address=$this->location($destination['address_components']);
		$weight=$request->input('weight');
		$pieces=$request->input('pieces');

		$contracts= ContractModel::with(['ratelane' => function ($query) use ($source_address,$destination_address,$weight,$pieces) {
			/************************************************************************************/			
			//condition for zip to zip			
			$query->where('cId','<>','');
			 $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true)
				;

                });
             //condition for zip to range   
               $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"3")
				//->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true)
				//->whereBetween("{$destination_address['postal_code']}", [DestinationZipcodeFrom, DestinationZipcodeTo])
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" )
				;

                });
                
             //condition for zip to state-city   
               $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true)
				;

                });
                
                //condition for zip to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"4")
				->where('DestinationTripleZipcode', $zip);

                });
                
                //condition for zip to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
               //condition for zip to state
				$query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
               //condition for zip to country 
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginalLocation.id',"6")
				->where(array('OriginalZipcode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationCountrycode',"{$destination_address['country']}");

                });
                
                /************************************************************************************/
                
                //condition for zip range to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for zip range to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for zip range to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                //condition for zip range to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )		
				->where('DestinationLocation.id',"4")
				->where('DestinationTripleZipcode', $zip);

                });
                
                //condition for zip range to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )		
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
                
                
                //condition for zip range to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for zip range to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"3")
                ->where('OriginalZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginalZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationCountrycode',"{$destination_address['country']}");

                });
               /************************************************************************************/ 
                
				//condition for city state to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for city state to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for city state to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                 //condition for city state to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"4")
				->where('DestinationTripleZipcode', $zip);

                });
                
                //condition for city state to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)	
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                //condition for city state to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for city state to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"1")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginalCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationCountrycode',"{$destination_address['country']}");

                });
                 /************************************************************************************/ 
                
                //condition for state to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for state to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for state to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                
                 //condition for state to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"4")
				->where('DestinationTripleZipcode', $zip);

                });
                
                //condition for state to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                //condition for state to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for state to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalLocation.id',"8")
				->where(array('OriginalState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationCountrycode',"{$destination_address['country']}");

                });
                
                 /************************************************************************************/ 
                
                //condition for all to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"6")
				->where(array('DestinationZipcode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for all to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for all to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                 //condition for all to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"4")
				->where('DestinationTripleZipcode', $zip);

                });
                
                //condition for all to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
                //condition for all to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for all to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginalCountrycode',"{$destination_address['country']}")
				->where('DestinationCountrycode',"{$destination_address['country']}");

                });
                
                if(!is_array($weight) && is_array($pieces) && count($weight)>0 && count($pieces)>0){
                
				   $query->where(function ($query)  use ($weight,$pieces) {
					 
					 for($i=0;$i<count($weight);$i++){
					 
						 if(!empty($weight[$i])){
							 $query->orWhere('FromWeightRange','>=', "{$weight[$i]}" )
							->where('ToWeightRange','<=', "{$weight[$i]}" );
							}
						/* if(!empty($pieces[$i])){
							 $query->orWhere('FromVolumeRange','>=', "{$pieces[$i]}" )
							->where('ToVolumeRange','<=', "{$pieces[$i]}" );
							}
						*/
					}

					});
					
				}
                
                
				$query->orderBy('OriginalZipcode', 'ASC');

			},
			
			
			
			
			
			
			'matrixratelane' => function ($query) use ($source_address,$destination_address) {
			//condition for zip to zip			
			$query->where('cId','<>','');
			 $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"6")
				->where(array('DestinationPostalCode'=> "{$destination_address['postal_code']}" ), 'exists', true)
				;

                });
                
                
                 //condition for zip to range   
               $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" )
				;

                });
                
             //condition for zip to state-city   
               $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true)
				;

                });
                
                //condition for zip to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"4")
				->where('DestinationPostalCode3', $zip);

                });
                
                //condition for zip to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
               //condition for zip to state
				$query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
               //condition for zip to country 
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 $query->where('OriginLocation.id',"6")
				->where(array('OriginPostalCode'=> "{$source_address['postal_code']}" ), 'exists', true)			
				->where('DestinationCountryCode',"{$destination_address['country']}");

                });
                
                /************************************************************************************/
                
                //condition for zip range to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"6")
				->where(array('DestinationPostalCode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for zip range to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for zip range to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                //condition for zip range to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )		
				->where('DestinationLocation.id',"4")
				->where('DestinationPostalCode3', $zip);

                });
                
                //condition for zip range to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )		
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
                
                
                //condition for zip range to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for zip range to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"3")
                ->where('OriginZipcodeFrom','>=', "{$source_address['postal_code']}" )
				->where('OriginZipcodeTo','<=', "{$source_address['postal_code']}" )
				->where('DestinationCountryCode',"{$destination_address['country']}");

                });
               /************************************************************************************/ 
                
				//condition for city state to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"6")
				->where(array('DestinationPostalCode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for city state to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for city state to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                 //condition for city state to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"4")
				->where('DestinationPostalCode3', $zip);

                });
                
                //condition for city state to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)	
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                //condition for city state to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for city state to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"1")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where(array('OriginCity'=> "{$source_address['city']}" ), 'exists', true)
				->where('DestinationCountryCode',"{$destination_address['country']}");

                });
                 /************************************************************************************/ 
                
                //condition for state to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"6")
				->where(array('DestinationPostalCode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for state to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for state to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                
                 //condition for state to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"4")
				->where('DestinationPostalCode3', $zip);

                });
                
                //condition for state to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                //condition for state to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for state to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginLocation.id',"8")
				->where(array('OriginState'=> "{$source_address['state']}" ), 'exists', true)
				->where('DestinationCountryCode',"{$destination_address['country']}");

                });
                
                 /************************************************************************************/ 
                
                //condition for all to zipcode
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"6")
				->where(array('DestinationPostalCode'=> "{$destination_address['postal_code']}" ), 'exists', true);

                });
                
                
                //condition for all to zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"3")
				->where('DestinationZipcodeFrom','>=', "{$destination_address['postal_code']}" )
				->where('DestinationZipcodeTo','<=', "{$destination_address['postal_code']}" );

                });
                
                
                
              
                //condition for all to city state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"1")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true)
				->where(array('DestinationCity'=> "{$destination_address['city']}" ), 'exists', true);

                });
                
                
                 //condition for all to 3zip
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"4")
				->where('DestinationPostalCode3', $zip);

                });
                
                //condition for all to 3zip range
                $query->orWhere(function ($query)  use ($source_address,$destination_address){
                 
                 $zip=substr($destination_address['postal_code'],0,3);
                  
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"5")
				->where('DestinationTripleZipcodeFrom','>=', "{$zip}" )
				->where('DestinationTripleZipcodeTo','<=', "{$zip}" );

                });
                
                
                
                //condition for all to state
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationLocation.id',"8")
				->where(array('DestinationState'=> "{$destination_address['state']}" ), 'exists', true);

                });
                
                 //condition for all to all
                $query->orWhere(function ($query)  use ($source_address,$destination_address) {
                 $query->where('OriginCountryCode',"{$destination_address['country']}")
				->where('DestinationCountryCode',"{$destination_address['country']}");

                });
                
                $query->orderBy('OriginPostalCode', 'ASC');
			
			}])
			
		->where('ProfileId', '=', $ProfileId)
				->where('clientID', '=', $clientId)
				->where('EffectiveFrom', '<=', $shipdate)
				->where('EffectiveTo', '>=', $shipdate)->get();
		
		

		
		return $contracts;
	}
	



/*
protected function calculateDistance($request,$contracts){
	
	
	foreach($contracts as $contract){
					$distanceapp=$contract->DistanceApp['id']; 
					
					switch($distanceapp)
						{
								//Rand McNally
								case 1:
								
								$this->options['process'][]=
										new Randmcnally($request,$contract);
								
								break;
								
								//PC Miler
								case 2:
								
								$this->options['process'][]=
										new Pcmiler($request,$contract);
								break;
								
								//Distance Mile
								case 3:
								
								
								$this->options['process'][]=
										new Distancemile($request,$contract);
								break;
								
								//Pro Mile
								case 4:
								
								$this->options['process'][]=
										new Promile($request,$contract);
								
								break;
								
								default:
								
								break;
								
								
							}
						
				 }
	
	return true;
	
}
*/


protected function getTerminal($request,$contracts){
	
	
	foreach($contracts as $contract){
					
								
						$this->options['process'][]=new Carrierterminal($request,$contract);
								
						
				 }
	
	return true;
	
}


protected function searchCarriersAPI($request,$contracts){
	$profileId=$request->input('profile');
	
	$Api=CarrierAPI::with('carrier')->where('contract_profilecode',$profileId)->get();
	foreach($Api as $contract){

		$this->options['process'][]=new APIThread($request,$contract);
	}
	
	
	return true;
	
}


protected function FuelAccessorialStore($searchId){
	//fetch tempsearch result and update with fule and accos
	/*
	$ProfileId=$request->input('profile');
	$carriers=array();
	$contracts = $this->searchCarriers($request,$ProfileId);
	*/ 
	$result= RateresultModel::where('searchId', '=', $searchId)->get();
	foreach($result as $item){
	
		if($item){
		$profileId=$item->ProfileId;
		$profile=ProfileModel::where('RateProfileCode',$profileId)->first();
		$item->Fuel =$profile['Fuel'];
		$item->Accessorial =$profile['Accessorial'];
		$item->save();	
		
		}
	}
	//$fuel=;
	//$accessorials=$profile['Accessorial'];
	
	
	
	return true;
	
}





	protected function SearchResult($request){
		
		$searchId=$request->input('searchId');
		
		
		$result=RateresultModel::where('searchId',$searchId)->get();
				
		return $result;
	}

	
	
	 private function location($address){
	  $sourceadress=array('city'=>'','area'=>'','state'=>'','country'=>'','postal_code'=>'');
	  
	  
	  foreach($address as $data){
			if($data['types'][0]=='locality'){
			$sourceadress['city']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_2'){
			$sourceadress['area']=$data['long_name'];
			}
			if($data['types'][0]=='administrative_area_level_1'){
			$sourceadress['state']=$data['short_name'];
			}
			if($data['types'][0]=='country'){
			$sourceadress['country']=$data['short_name'];
			if($sourceadress['country']=='US'){$sourceadress['country']='USA';}
			}
			if($data['types'][0]=='postal_code'){
			$sourceadress['postal_code']=$data['long_name'];
			}
			
		}
		
		return $sourceadress;
  } 
  
  
  
 /* 
  
	protected function getRate()
	{
		$data = array(
				array('class' => '70','description' => 'test','height' => '7','length' => '5','nmfc' => '','pallets' =>'5','pices' => '1','weight' => '1000','width' => '6'),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
	
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
				array('class' => '','description' => '','height' => '','length' => '','nmfc' => '','pallets' =>'','pices' => '','weight' => '','width' => ''),
		);
	
		//include_once 'application/models/rating/class/ratrequestproducts.php';
	
		for($i = 0;$i<count($data);$i++)
		{
			$requestproduct = new raterequestproducts();
			$requestproduct->setClass($data[$i]['class']);
			$requestproduct->setDescription($data[$i]['description']);
			$requestproduct->setHeight($data[$i]['height']);
			$requestproduct->setLength($data[$i]['length']);
			$requestproduct->setNMFC($data[$i]['nmfc']);
			$requestproduct->setPallets($data[$i]['pallets']);
			$requestproduct->setPices($data[$i]['pices']);
			$requestproduct->setWeight($data[$i]['weight']);
			$requestproduct->setWidth($data[$i]['width']);
			$d[] = $requestproduct;
		}
	
		//include_once 'application/models/rating/class/raterequest.php';
		$request = new raterequest();
		
		$request->setAccountName('');
		$request->setAccountNumber($_GET['accountnumber']);
		$request->setMeterNo('');
		$request->setCarrierAccessorials('');
		$request->setDestCountryCode('');
		$request->setFromCity('');
		$request->setFromState('');
		$request->setFromZip('');
		$request->setIsDebugMode('');
		$request->setIsTestMode('');
		$request->setOriginCountryCode('');
		$request->setPassword($_GET['password']);
		$request->setPaymentTerm('');
		$request->setProducts('');
		$request->setCarrierRateServiceProduct($d);
		
		$request->setScacCode($_GET['scaccode']);
		$request->setServiceLevel('');
		$request->setShipmentDate('');
		$request->setShipmentMode('');
		$request->setToCity('');
		$request->setToState('');
		$request->setToZip('');
		$request->setUseBrokerSrv('');
		$request->setUserName($_GET['username']);
		$request->setType('');
		
		$request->setClientAddress('');
		$request->setClientCity('');
		$request->setClientState('');
		$request->setClientZip('');
		$request->setClientCountry('');
		;
		
		//$this->load->model('rating/switchcasemodel');		
		$result = switchcasemodel::switchcase($request);

		$callback = $_GET['callback'];
		header('Content-type: application/javascript');
		$jsonp = json_encode($result, JSON_PRETTY_PRINT);
		echo $callback."(".$jsonp.")";			
		
		
		
		
		
	}
	
	
	
	protected function getRateData($apidata)
	{
		$data = array(
				array('class' => $apidata['Class1'],'description' => '','height' => $apidata['Height1'],'length' => $apidata['Length1'],'nmfc' => $apidata['NMFC1'],'pallets' =>$apidata['Pallet1'],'pices' => '','weight' => $apidata['Weight1'],'width' => $apidata['Width1']),
				array('class' => $apidata['Class2'],'description' => '','height' => $apidata['Height2'],'length' => $apidata['Length2'],'nmfc' => $apidata['NMFC2'],'pallets' =>$apidata['Pallet2'],'pices' => '','weight' => $apidata['Weight2'],'width' => $apidata['Width2']),
				array('class' => $apidata['Class3'],'description' => '','height' => $apidata['Height3'],'length' => $apidata['Length3'],'nmfc' => $apidata['NMFC3'],'pallets' =>$apidata['Pallet3'],'pices' => '','weight' => $apidata['Weight3'],'width' => $apidata['Width3']),
				array('class' => $apidata['Class4'],'description' => '','height' => $apidata['Height4'],'length' => $apidata['Length4'],'nmfc' => $apidata['NMFC4'],'pallets' =>$apidata['Pallet4'],'pices' => '','weight' => $apidata['Weight4'],'width' => $apidata['Width4']),
				array('class' => $apidata['Class5'],'description' => '','height' => $apidata['Height5'],'length' => $apidata['Length5'],'nmfc' => $apidata['NMFC5'],'pallets' =>$apidata['Pallet5'],'pices' => '','weight' => $apidata['Weight5'],'width' => $apidata['Width5']),
	

		);
	
		//include_once 'application/models/ratingdata/class/ratrequestproducts.php';
	
		for($i = 0;$i<count($data);$i++)
		{
			$requestproduct = new raterequestproducts();
			$requestproduct->setClass($data[$i]['class']);
			$requestproduct->setDescription($data[$i]['description']);
			$requestproduct->setHeight($data[$i]['height']);
			$requestproduct->setLength($data[$i]['length']);
			$requestproduct->setNMFC($data[$i]['nmfc']);
			$requestproduct->setPallets($data[$i]['pallets']);
			$requestproduct->setPices($data[$i]['pices']);
			$requestproduct->setWeight($data[$i]['weight']);
			$requestproduct->setWidth($data[$i]['width']);
			$d[] = $requestproduct;
		}
		
		 //include_once 'application/models/ratingdata/class/raterequest.php';
		$request = new raterequest();
		
		$request->setAccountName($apidata['accountname']);
		$request->setAccountNumber($apidata['Accountnumber']);
		$request->setMeterNo($apidata['meterno']);
		$request->setCarrierAccessorials($apidata['Accs1']);
		$request->setDestCountryCode($apidata['DeliveryCountry']);
		$request->setFromCity($apidata['PickupCity']);
		$request->setFromState($apidata['PickupState']);
		$request->setFromZip($apidata['PickupPostal']);
		$request->setIsDebugMode($apidata['isdebugmode']);
		$request->setIsTestMode($apidata['IsTestMode']);
		$request->setOriginCountryCode($apidata['PickupCountry']);
		$request->setPassword($apidata['Password']);
		$request->setPaymentTerm($apidata['PaymentTerm']);
		$request->setProducts($apidata['products']);
		$request->setCarrierRateServiceProduct($d);
		
		$request->setScacCode($apidata['ScacCode']);
		$request->setServiceLevel($apidata['ServiceLevel']);
		$request->setShipmentDate($apidata['ShipDate']);
		$request->setShipmentMode($apidata['Accounttype']);
		$request->setToCity($apidata['DeliveryCity']);
		$request->setToState($apidata['DeliveryState']);
		$request->setToZip($apidata['DeliveryPostal']);
		$request->setUseBrokerSrv($apidata['usebrokersrv']);
		$request->setUserName($apidata['UserID']);
		$request->setType($apidata['type']);
		
		$request->setClientAddress($apidata['Clientaddress']);
		$request->setClientCity($apidata['Clientcity']);
		$request->setClientState($apidata['Clientstate']);
		$request->setClientZip($apidata['ClientPostal']);
		$request->setClientCountry($apidata['clientcountry']);
		
		//$this->load->model('ratingdata/switchcasemodeldata');
		$result = switchcasemodeldata::switchcase($request);
		
		$callback = $apidata['callback'];
		header('Content-type: application/javascript');
		$jsonp = json_encode($result, JSON_PRETTY_PRINT);
		echo $callback."(".$jsonp.")";
		
		//$this->load->view('rateresponseview',$result);
		
		//$this->load->view('testingview',$result);
		}
	

	
	protected function gettrack()
	{
		
		//include_once 'application/models/tracking/class/trackingrequest.php';
		$request = new trackingrequest();
	
		$request->setUserName($_GET['username']);
		$request->setPassword($_GET['password']);
		$request->setAccountNumber($_GET['accountnumber']);
		$request->setCarrierCode($_GET['carriercode']);
		$request->setTrackingNumber($_GET['trackingnumber']);
	
		//$this->load->model('tracking/switchcasemodel');
		echo $result = switchcasemodel::switchcase($request);
	
	}
	
	*/
	
   }
