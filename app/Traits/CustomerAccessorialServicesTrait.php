<?php
namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Customer;

trait CustomerAccessorialServicesTrait {
    
    protected function listaccessorial(Request $request, array $fields=array()){
		$clientID = $request->get('clientID');
		//
		//if the sent client id is blank get the system set customer ID
		if(empty($clientID) || !is_numeric($clientID))
			$clientID = \CustomerRepository::getCurrentCustomerID();	
			
        $customer = new Customer;
		$accessorialData = $customer->find($clientID)->accessorial()->get();
		//dd($accessorialData);
		return response()->json(['success' => 'Items loaded successfully', 'count' => $accessorialData->count(), 'results' => $accessorialData]);
	}
	
    protected function listaccessorial_list(Request $request, array $fields=array()){
		$clientID = $request->get('clientID');
		//
		//if the sent client id is blank get the system set customer ID
		if(empty($clientID) || !is_numeric($clientID))
			$clientID = \CustomerRepository::getCurrentCustomerID();	
			
        $customer = new Customer;
		$accessorialData = $customer->find($clientID)->accessorial()->get(['accsname as label', 'accscode as id']);
		//echo "<pre>";
		//print_r($accessorialData); exit;
		return response()->json(['success' => 'Items loaded successfully', 'count' => $accessorialData->count(), 'results' => $accessorialData]);
	}	
        
}
