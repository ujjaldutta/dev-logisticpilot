<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Carrier;
use App\CarrierAccessorialTariff;
use App\Accessorial;
use App\CarrierFuelType;
use App\CarrierFuelTariff;


trait CarrierServices{
	/*
	* Method to save a quote for a carrier depending on the searched params
	*
	*/
	protected function getCarrierLiablityInsurance(Request $request){
		try{
			$scac = $request->get('scac');
			if(!empty($scac)){
				$insurances = Carrier::where('scac', $request->get('scac'))
					->with([
						'insurances' => function($query){
							
						},
					])->first(['id', 'carrier_name', 'scac', 'carrier_performance']);
					
				return response()->json(['success' => 'Insurance Data fetched Successfully', 'carrierInfo' => $insurances, 'currentIndex' => $request->get('currentIndex')]);
			}else{
				return response()->json(['error' => 'Missing required information']);
			}
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	/*
	* Method to save a quote for a carrier depending on the searched params
	*
	*/
	protected function uploadCustomAccsCsv(Request $request){
		try{
			if($request->get('carrierId')){
				$clientId = \Auth::user()->clientID;
				$carrierId = $request->get('carrierId');
				
				if($request->hasFile('file') && $request->file('file')->isValid()){
					$uploadedFile = $request->file('file');
					
					//checking extension or file type
					if($uploadedFile->getClientOriginalExtension() != self::getValidCSVFileExt)
						return response()->json(['error' => 'Invalid File Uploaded']);
					
					$output = $this->_processCSV(
						$uploadedFile, 
						[
							'carrierID', 'clientID', 'accs_code', 'crraccs_minrate', 'crraccs_rate',
							'crraccs_type',  'crraccs_addbydefault', 'crraccs_effectivefrom'
						],
						[
							$carrierId, $clientId
						]
					);
					
					//getting accs db id replacing the accs code placed inside the csv
					$accsIds = Accessorial::lists('id', 'accscode');
					
					//replacing the accs code with accs id
					$processedData = isset($output['rows']) ? $output['rows'] : [];
					array_walk($processedData, function(& $item, $key) use($accsIds){
						if(isset($item['accs_code']) && isset($accsIds[$item['accs_code']])){
							$item['accs_code'] = $accsIds[$item['accs_code']];
						}else
							$item['accs_code'] = null;
					});
					
					//dd(array_values($processedData));
					//now adding condition to control the data insertion
					$results = $this->_saveRecordsFromCsv($processedData, new CarrierAccessorialTariff());
					
					return response()->json(['success' => 'File uploaded and processed successfully', 'importResults' => $results]);
				}
			}else{
				return response()->json(['error' => 'Important information is missing.']);
			}
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	/*
	* Method to save a quote for a carrier depending on the searched params
	*
	*/
	protected function uploadCustomFuelCsv(Request $request){
		try{
			if($request->get('carrierId')){
				$clientId = \Auth::user()->clientID;
				$carrierId = $request->get('carrierId');
				
				if($request->hasFile('file') && $request->file('file')->isValid()){
					$uploadedFile = $request->file('file');
					
					//checking extension or file type
					if($uploadedFile->getClientOriginalExtension() != self::getValidCSVFileExt)
						return response()->json(['error' => 'Invalid File Uploaded']);
					
					$output = $this->_processCSV(
						$uploadedFile, 
						[
							'carrierID', 'clientID', 'fuel_code', 'rangefrom', 'rangeto',
							'LTLRate',  'TLRate', 'effectivefrom'
						],
						[
							$carrierId, $clientId
						]
					);
					
					//getting accs db id replacing the accs code placed inside the csv
					$fuelIds = CarrierFuelType::lists('id', 'fuelcode');
					
					//replacing the accs code with accs id
					$processedData = isset($output['rows']) ? $output['rows'] : [];
					array_walk($processedData, function(& $item, $key) use($fuelIds){
						if(isset($item['fuel_code']) && isset($fuelIds[$item['fuel_code']])){
							$item['fuel_code'] = $fuelIds[$item['fuel_code']];
						}else
							$item['fuel_code'] = null;
					});
					
					//dd(array_values($processedData));
					//now adding condition to control the data insertion
					$results = $this->_saveRecordsFromCsv($processedData, new CarrierFuelTariff());
					
					return response()->json(['success' => 'File uploaded and processed successfully', 'importResults' => $results]);
				}
			}else{
				return response()->json(['error' => 'Important information is missing.']);
			}
		}catch(\Exception $ex){
			return response()->json(['error' => $ex->getMessage()]);
		}
	}
	
	private function _processCSV(UploadedFile $file, array $fields = [], array $matchingData = []){
		//opening file
		$fp = @fopen($file->getPathName(), 'r');
		if($fp === FALSE)
			throw new \Exception('Could not open uploaded csv file');
		//global output container for errors
		$errorOutput = [];
		$rows = []; 
		//iterating through the array
		$itaCount = 0;
		while(($data = fgetcsv($fp)) !== FALSE){
			if($itaCount > 0){
				$dataArr = [];
				$data = array_merge($matchingData, $data);
				for($i = 0; $i < count($data); $i ++){
					//building array with data
					$dataArr[$fields[$i]] = $data[$i];
				}
				//print_r($dataArr);
				if(count($dataArr)){
					$rows[] = $dataArr;
				}else
					$errorOutput[] = "Row at {$i} has error and skipped";
			}else{ //matching the csv first row to check expected fields with the given one
				if(count(array_diff($fields, array_merge(['carrierID', 'clientID'], $data))))
					throw new \Exception('The Csv first row does not have matching column names with the specified format');
			}
			
			$itaCount ++;
		}
		return compact('errorOutput', 'rows');
	}
	
	private function _saveRecordsFromCsv(array $rows, Model $model){
		$results = [];
		foreach($rows as $row){
			$results[] = $model->callUploadCsvProc($row);
		}
		
		return $results;
	}
}