<?php
namespace App\Traits;

trait UtilityTrait {
	/* protected function to process google location details into the 
	* readable and required format for this application
	*/
	public static function _processGoogleLocation($location, $postalField = 'client_postal', 
		$cityField = 'client_city', $stateField = 'client_state', 
		$countryField = 'client_country'
	){
		$parsedLocation = [];
		if(isset($location['address_components']) && count($location['address_components'])){
			foreach($location['address_components'] as $address){
				if(isset($address['types'][0])){
					switch($address['types'][0]){
						case 'postal_code':
							$parsedLocation[$postalField] = isset($address['long_name']) ? $address['long_name'] : null;
							break;
						case 'locality':
							$parsedLocation[$cityField] = isset($address['long_name']) ? $address['long_name'] : null;
							break;
						case 'administrative_area_level_1':
							$parsedLocation[$stateField] = isset($address['short_name']) ? $address['short_name'] : null;
							break;
						case 'country':
							$parsedLocation[$countryField] = isset($address['short_name']) ? $address['short_name'] : null;
							break;
					}
				}
			}
		}
		return $parsedLocation;
	}
	
	  /*
    * Method to create csv file from a set of data given
    * @access public
    * @
    */
    public static function convertToCsv($data, $options){      
        // setting the csv header
        if (is_array($options) && isset($options['headers']) && is_array($options['headers'])) {
          $headers = $options['headers'];
        } else {

          $fileName = isset($options['fileName']) ? $options['fileName'] : 'report.csv';
          $headers = array(
              'Content-Type' => 'text/csv',
              'Content-Disposition' => (isset($options['fileName']) && !empty($options['fileName'])) ? 
				'attachment; filename="'. $options['fileName']. '"' : 'attachment; filename="report.csv"'
          );
        }        
        $output = '';        
        // setting the first row of the csv if provided in options array
        if (isset($options['firstRow']) && is_array($options['firstRow'])) {
          $output .= implode(',', $options['firstRow']);
          $output .= "\n"; // new line after the first line
        }        
        // setting the columns for the csv. if columns provided, then fetching the or else object keys
        if (isset($options['columns']) && is_array($options['columns'])) {
          $columns = $options['columns'];
        } else {
          $objectKeys = get_object_vars($data[0]);
          $columns = array_keys($objectKeys);
        }
       
        // populating the main output string
        foreach ($data as $row) {            
          foreach ($columns as $column) {
            $output .= str_replace(',', ';', $row[$column]); 
            $output .= ',';
          }
          $output .= "\n";
        }        
        // calling the Response class make function inside my class to send the response.
        // if our class is not a controller, this is required.
        return \Response::make($output, 200, $headers);
    }
}