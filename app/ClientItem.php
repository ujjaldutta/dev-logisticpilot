<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientItem extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_clientitem';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'clientId',            
                'item_code',
                'item_SKU',
                'item_name',
                'item_weight',
                'item_length',
                'item_width',
                'item_height',                
                'item_hazmat',                
                'item_nmfc', 
                'item_custnum',
                'item_distnum',
                'item_mannum',            
                'item_qty',
                'item_cube',
                'item_monetary',
                'item_stackability',
                'item_returnable',
                'item_hazmat',            
                'item_tempmin',
                'item_tempmax',
            	'item_dimuomtype',
            	'item_class', 
                'item_commodity',
                'item_packagetypeid',
                'item_weightType',
                'item_temperaturetype',
                            
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		'created_at', 'updated_at'
	];
	
	
}