<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

	//
	protected $table = 'm_state';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}
	
	public function country(){
		return $this->belongsTo('App\Country', 'country_code');
	}
}
