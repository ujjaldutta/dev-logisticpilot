<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeClassType extends Model {

	protected $table = 'v_freeClassType';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
}
