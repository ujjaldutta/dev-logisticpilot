<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierEquipement extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_carrierequipment';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function equipmentType(){
		return $this->belongsTo('App\EquipmentType', 'equipId');
	}
}