<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipB2b extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipb2b';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId', 'ship_b2bId', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function shipb2btypesname(){
		return $this->belongsTo('App\ShipB2BType', 'ship_b2bId');
	}
	
}
