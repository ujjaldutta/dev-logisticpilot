<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, \Venturecraft\Revisionable\RevisionableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'm_users';
	
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'usr_firstname', 
		'usr_lastname', 
		'email',
		'usr_adr1', 
		'usr_adr2', 
		'usr_city',
		'usr_state',
		'usr_postal',
		'usr_country',
		'usr_email',
		'usr_phone',
		'usr_type_id',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'created_at', 'updated_at'];
	
	public function registration(){
		return $this->hasOne('App\Registration', 'userID');
	}
	
	public function apps(){
		return $this->belongsToMany('App\AppModule', 'm_userappmap', 'userID', 'appID');
	}
	
	public function client(){
		return $this->belongsTo('App\Customer', 'clientID');
	}
	
	public function clients(){
		return $this->belongsToMany('App\Customer', 'm_userclientmap', 'userID', 'clientID');
	}
	
	public function userType(){
		return $this->belongsTo('App\UserType', 'usr_type_id');
	}
	
	/*public function shipnotes(){
		return $this->hasMany('App\ShipNote', 'ship_createdbyId');
	}*/

}
