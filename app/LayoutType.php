<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class LayoutType extends Model {
	
	//
	protected $table = 'v_layouttype';
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function layouts(){
		return $this->hasMany('App\Layout', 'layoutId');
	}
}