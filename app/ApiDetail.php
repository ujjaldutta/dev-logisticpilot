<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiDetail extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	Const API_SHIP_TYPE_SHIPPING = 'S';
	Const API_SHIP_TYPE_CONSIGNEE = 'C';
	
	//
	protected $table = 'rate_apidetail';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'carrierAPIID',
		'rate_shppostal',
		'rate_cnspostal',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];	
}