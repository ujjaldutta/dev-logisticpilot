<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierAccessorialTariff extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_carrieraccessorial';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'carrierID',
		'clientID',
		'accsID',
		'crraccs_rate',
		'crraccs_type',
		'crraccs_minrate',
		'crraccs_addbydefault',
		'crraccs_effectivefrom',
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	public function accsType(){
		return $this->belongsTo('App\Accessorial', 'accsID');
	}
	
	public function rate(){
		return $this->belongsTo('App\CalulationType', 'crraccs_type');
	}
	
	public function carrier(){
		return $this->belongsTo('App\Carrier', 'carrierID');
	}
	
	/*
	* static function to execute mysql stored proc to validate and insert data 
	* @param array values
	* @return array
	* @access public static
	*/
	public static function callUploadCsvProc(array $values){
		try{
			//dd($values);
			//getting db manager PDO object
			$pdo = \DB::getPDO();
			
			//setting up Mysql stored proc and its in out params
			$outVar = 'lId';
			$stmt = $pdo->prepare("call sp_importAccsCSV(?,?,?,?,?,?,?,?,@lId)");
			$stmt->bindParam(1, $values['carrierID']);
			$stmt->bindParam(2, $values['clientID']);
			$stmt->bindParam(3, $values['accs_code']);
			$stmt->bindParam(4, $values['crraccs_minrate']);
			$stmt->bindParam(5, $values['crraccs_rate']);
			$stmt->bindParam(6, $values['crraccs_type']);
			$stmt->bindParam(7, $values['crraccs_addbydefault']);
			$stmt->bindParam(8, $values['crraccs_effectivefrom']);
			$stmt->bindParam(9, $outVar);
			
			//executing the statement and throwing error if occurred
			if(!$stmt->execute())
				throw new \Exception("Error in executing sql statement");
				//throw new \Exception("Error code: ". implode(',', $stmt->errorCode()). " - Error: ". implode(',', $stmt->errorInfo());
			
			//relaesing current resource
			$stmt->closeCursor();
			
			//quering again using PDO statement to get the last insert id
			$queryStmt = $pdo->query("select @". $outVar ." as lastInsertID");
			if(!$queryStmt->execute())
				throw new \Exception("Error in executing sql statement to get last insert id");
			
			$result['success'] = $queryStmt->fetch(\PDO::FETCH_ASSOC);
			//var_dump($result);
			return $result;
		}catch(\Exception $e){
			return array('error' => $e->getMessage());
		}
	}
}