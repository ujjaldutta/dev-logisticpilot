<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientDefaultSettings extends Model {

	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'm_clientdefaultsettings';
	protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $hidden = [
		'clientID', 'created_at', 'updated_at',
	];
	
	protected $fillable = [ 
	  'pickup_locname',
	  'pickup_locadr1',
	  'pickup_locadr2',
	  'pickup_city',
	  'pickup_state',
	  'pickup_postal',
	  'pickup_country',
	  'pickup_phone',
	  'pickup_fax',
	  'pickup_hoursfrom',
	  'pickup_hoursto',
	  'pickup_notes',
	  'delivery_locname',
	  'delivery_locadr1',
	  'delivery_locadr2',
	  'delivery_city',
	  'delivery_state',
	  'delivery_postal',
	  'delivery_country',
	  'delivery_phone',
	  'delivery_fax',
	  'delivery_hoursfrom',
	  'delivery_hoursto',
	  'delivery_notes',
	  'billto_locname',
	  'billto_locadr1',
	  'billto_locadr2',
	  'billto_city',
	  'billto_state',
	  'billto_country',
	  'billto_postal',
	  'billto_phone',
	  'billto_fax',
	  'product_name',
	  'product_class',
	  'shipment_terms',
	  'bol_templateId',
	  'label_templateId',
	  'invoice_templateId',
	  'email_bolto',
	  'email_lableto',
	  'email_invoiceto',
	  'invoice_term',
	  'email_invoiceon',
	  'receive_edi204',
	  'receive_edi210',
	  'receive_edi214',
	  'receive_edi216',
	  'send_edi204',
	  'send_edi210',
	  'send_edi214',
	  'billto_noneditable',
	];
	
	protected $guarded = [
	
	];
}
