<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipDocument extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'trn_shipdocuments';
	protected $revisionEnabled = true;
	protected $fillable = ['id', 'shipId', 'doc_typeId', 'doc_location', 'doc_createdbyId', 'doc_createddate', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
	public function shipheader(){
		return $this->belongsTo('App\ShipHeader', 'shipId');
	}
	
	public function users(){
		return $this->belongsTo('App\User', 'doc_createdbyId');
	}	
}
