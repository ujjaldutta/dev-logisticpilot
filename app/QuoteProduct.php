<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteProduct extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'quote_product';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		'prd_srlno',
		'prd_itempieces',
		'prd_itemqty',
		'prd_itempkgtype',
		'prd_itemclass',
		'prd_itemweight',
		'prd_itemwidth',
		'prd_itemheight',
		'prd_itemlength',
		'prd_itemhazmat',
		'prd_stackable',
	];
	
	protected $guared = [
		'id',
	];
	
	protected $hidden = [
		
	];
}