<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessorial extends Model {
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	//
	protected $table = 'm_accessorial';
	protected $revisionEnabled = false;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.
	
	protected $fillable = [
		
	];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
    public function users()
    {
        return $this->belongsToMany('App\Customer');
    }	
}