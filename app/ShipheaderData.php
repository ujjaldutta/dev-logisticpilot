<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipheaderData extends Model {
    use \Venturecraft\Revisionable\RevisionableTrait;
	//
	protected $table = 'v_shipheaderdata';
	protected $revisionEnabled = true;
	//protected $fillable = ['id', 'shipId', 'ship_b2bId', ];
	
	protected $guared = [
		
	];
	
	protected $hidden = [
		
	];
	
	/*public function cities(){
		return $this->hasMany('App\City', 'state_code');
	}*/
	
}
