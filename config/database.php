<?php

return [

	/*
	|--------------------------------------------------------------------------
	| PDO Fetch Style
	|--------------------------------------------------------------------------
	|
	| By default, database results will be returned as instances of the PHP
	| stdClass object; however, you may desire to retrieve records in an
	| array format for simplicity. Here you can tweak the fetch style.
	|
	*/

	'fetch' => PDO::FETCH_CLASS,

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => 'mysql',

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => [

		'sqlite' => [
			'driver'   => 'sqlite',
			'database' => storage_path().'/database.sqlite',
			'prefix'   => '',
		],

		'mysql' => [
			'driver'    => 'mysql',
			'host'      => env('DB_HOST', 'localhost'),
			'database'  => env('DB_DATABASE', 'logisticspilot'),
			'username'  => env('DB_USERNAME', 'root'),
			'password'  => env('DB_PASSWORD', 'user123'),
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		/*'mysql' => [
			'driver'    => 'mysql',
			'host'      => env('DB_HOST', 'lpclouddb.csmu3qokxcbp.us-west-2.rds.amazonaws.com'),
			'database'  => env('DB_DATABASE', 'logisticspilot'),
			'username'  => env('DB_USERNAME', 'lpcloud1'),
			'password'  => env('DB_PASSWORD', 'lpcloud1'),
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'port'     => env('DB_PORT', 3306),
			'strict'    => false,
		],*/
		
            /*    
            'mongodb' => [
                                'driver'   => 'mongodb',
                                'host'     => env('DB_HOST', 'localhost'),
                                'port'     => env('DB_PORT', 27017),
                                'database' => env('DB_DATABASE', 'logisticspilot'),
                                'username' => env('DB_USERNAME', 'sadmin'),
                                'password' => env('DB_PASSWORD', 'admin@123'),
                                'options' => [
                                    'db' => 'admin' // sets the authentication database required by mongo 3
                                ]
                            ],
                 
             */
           
                'mongodb' => [
                                'driver'   => 'mongodb',
                                'host'     => env('MDB_HOST', 'ec2-52-34-182-62.us-west-2.compute.amazonaws.com'),
                                'port'     => env('MDB_PORT', 27017),
                                'database' => env('MDB_DATABASE', 'logisticspilot'),
                                'username' => env('MDB_USERNAME', 'sadmin'),
                                'password' => env('MDB_PASSWORD', 'admin@123'),
                                'options' => [
                                    'db' => 'admin' // sets the authentication database required by mongo 3
                                ]
                            ],
           
           /*
           'mongodb' => [
                                'driver'   => 'mongodb',
                                'host'     => env('MDB_HOST', 'lpclouddb.csmu3qokxcbp.us-west-2.rds.amazonaws.com'),
                                'port'     => env('MDB_PORT', 3306),
                                'database' => env('MDB_DATABASE', 'logisticspilot'),
                                'username' => env('MDB_USERNAME', 'lpcloud1'),
                                'password' => env('MDB_PASSWORD', 'lpcloud1'),
                                'options' => [
                                    'db' => 'admin' // sets the authentication database required by mongo 3
                                ]
                            ],
           */

		'pgsql' => [
			'driver'   => 'pgsql',
			'host'     => env('DB_HOST', 'lpclouddb.csmu3qokxcbp.us-west-2.rds.amazonaws.com:3306'),
			'database' => env('DB_DATABASE', 'logisticspilot'),
			'username' => env('DB_USERNAME', 'lpcloud1'),
			'password' => env('DB_PASSWORD', 'lpcloud1'),
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		],

		'sqlsrv' => [
			'driver'   => 'sqlsrv',
			'host'     => env('DB_HOST', 'localhost'),
			'database' => env('DB_DATABASE', 'forge'),
			'username' => env('DB_USERNAME', 'forge'),
			'password' => env('DB_PASSWORD', ''),
			'prefix'   => '',
		],

	],

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk haven't actually been run in the database.
	|
	*/

	'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => [

		'cluster' => false,

		'default' => [
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		],

	],

];
