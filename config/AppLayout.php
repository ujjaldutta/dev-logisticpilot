<?php
return [
	'customerApp' => 1,
	'userApp' => 5,
	'customerLocation' => 8,
        'customerProduct' => 9,
	'carrierAccessorialSetup' => 10,
	'carrierFuelSetup' => 11,
        'codeReference' => 13,
		'shipmentSetup' => 17,
		'dispatchSetup' => 2,
		'shipmentInvoiceSetup' => 18,
];