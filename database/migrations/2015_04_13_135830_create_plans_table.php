<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subcription_plans', function(BluePrint $table){
			$table->increments('id');
			$table->string('name', 100);
			$table->decimal('price', 7, 2);
			$table->text('description');
			$table->string('colorcode')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('subcription_plans');
	}

}
