DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `importAccsCSV`(IN `crId` INT(11), IN `clId` INT(11), IN `accId` INT(10), IN `minRate` DECIMAL(10,2), IN `maxRate` DECIMAL(10,2), IN `crrType` INT, IN `isDefault` TINYINT(1), IN `eFrom` DATE, OUT `lastId` INT)
BEGIN
        DECLARE rCount;
		SELECT COUNT(`id`) INTO rCount FROM `m_carrieraccessorial` WHERE `crraccs_effectivefrom` = eFrom;
		IF (rCount <= 0) THEN
			INSERT INTO `m_carrieraccessorial`(
                            `carrierID`,
                            `clientID`,
                            `accsID`,
                            `crraccs_rate`,
                            `crraccs_type`,
                            `crraccs_minrate`,
                            `crraccs_addbydefault`,
                            `crraccs_effectivefrom`,
                            `created_at`
                     ) VALUES(
                            crId,
                            clId,
                            accId,
                            maxRate,
                            crrType,
                            minRate,
                            isDefault,
                            eFrom,
                            NOW()
                     );
                 SELECT LAST_INSERT_ID() INTO lastId;
		END IF;
END$$
DELIMITER ;