DELIMITER $$
CREATE FUNCTION `logisticspilot`.`importAccsCSV`(
 	carrierID INT(11),
 	clientID INT(11),
 	accsID INT(10),
 	crraccs_rate DECIMAL(10,2),
 	crraccs_type INT(10),
 	crraccs_minrate DECIMAL(10,2),
 	crraccs_addbydefault TINYINT(1),
 	IN crraccs_effectivefrom DATE
 ) RETURNS INT
 BEGIN
 	DECLARE rcCount INT;
        SELECT COUNT(`id`) INTO rcCount FROM `m_carrieraccessorial` WHERE `crraccs_effectivefrom` = crraccs_effectivefrom;
            IF rcCount = 0 THEN
                    INSERT INTO `m_carrieraccessorial`(
                            `carrierID`,
                            `clientID`,
                            `accsIDINT`,
                            `crraccs_rate`,
                            `crraccs_type`,
                            `crraccs_minrate`,
                            `crraccs_addbydefault`,
                            `crraccs_effectivefrom`,
                            `created_at`
                     ) VALUES(
                            carrierID,
                            clientID,
                            accsID,
                            crraccs_rate,
                            crraccs_type,
                            crraccs_minrate,
                            crraccs_addbydefault,
                            crraccs_effectivefrom,
                            NOW()
                     );
                 RETURN LAST_INSERT_ID();
            ELSE
                 RETURN 0;
            END IF;
END$$
DELIMITER ;