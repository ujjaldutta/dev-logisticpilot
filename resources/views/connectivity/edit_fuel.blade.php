@extends('layouts.user_home')
@section('angular_controller')
	CarrierFuelEditController
@stop
@section('page_class')
	contract-page
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('carrier_equipment_fuel_add', $carrier_id) !!}
@stop
@section('styles')
	<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
     @include('_partials/carrier_equipment_sidebar')
@stop
@section('top_content')
	
@stop
@section('content')
<!-- content section-->
		<section class="content-wrapper" ng-init="newRecord=false; carrierId={{$carrier_id}}; recordId={{$id}}">
            <form name="carrierFuelForm" novalidate>
			<div class="add-new-shipment">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<div class="service_logo">
							<img src="{{ asset('uploads/carriers/'. $carrier->carrier_logo) }}" alt="Logo">
							{{{ $carrier->carrier_name or 'Unknown' }}}
						</div>
					</div>
				</div>
          </div>
			<div class="contract_table">
            	<h2>Add Fuel</h2>
                <div class="row">
                	<div class="col-lg-3">
						<div class="form-group" ng-class="{'has-error': carrierFuelForm.fuelType.$invalid}">
							<label>Fuel Type</label>
							<select class="form-control" ng-options="fuelType.id as fuelType.fuelType for fuelType in fuelTypes" ng-model="fuelData.fuelType" id="fuelType" name="fuelType" required>
								<option value="" disabled selected ng-hide="fuelData.fuelType">Please select</option>
							</select>
							<span class="help-block" ng-show="carrierFuelForm.fuelType.$invalid">
								<span ng-show="carrierFuelForm.fuelType.$error.required">Fuel type is required</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierFuelForm.rangefrom.$invalid}">
						  <label>From ($)</label>
						  <input type="number" class="form-control" placeholder="" ng-model="fuelData.rangefrom" name="rangefrom" required>
							<span class="help-block" ng-show="carrierFuelForm.rangefrom.$invalid">
								<span ng-show="carrierFuelForm.rangefrom.$error.required">From ($) is required</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierFuelForm.rangeto.$invalid}">
						  <label>To ($)</label>
						  <input type="number" class="form-control" placeholder="" ng-model="fuelData.rangeto" name="rangeto" required>
							<span class="help-block" ng-show="carrierFuelForm.rangeto.$invalid">
								<span ng-show="carrierFuelForm.rangeto.$error.required">To ($) is required</span>
							</span>
						</div>
                    </div>
                    <div class="col-lg-3">
                    	<div class="form-group" ng-class="{'has-error': carrierFuelForm.LTLRate.$invalid}">
						  <label>LTL Rate (%)</label>
						  <input type="number" class="form-control" placeholder="" ng-model="fuelData.LTLRate" name="LTLRate" required>
							<span class="help-block" ng-show="carrierFuelForm.LTLRate.$invalid">
								<span ng-show="carrierFuelForm.LTLRate.$error.required">LTL Rate (%) is required</span>
								<span ng-show="carrierFuelForm.LTLRate.$error.number">LTL Rate (%) is invalid</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierFuelForm.TLRate.$invalid}">
						  <label>TL Rate (%)</label>
						  <input type="number" class="form-control" placeholder="" ng-model="fuelData.TLRate" name="TLRate" required>
							<span class="help-block" ng-show="carrierFuelForm.TLRate.$invalid">
								<span ng-show="carrierFuelForm.TLRate.$error.required">TL Rate (%) is required</span>
								<span ng-show="carrierFuelForm.TLRate.$error.number">TL Rate (%) is invalid</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierFuelForm.effectivefrom.$invalid}">
							<label>Effective From</label>
							<div class="contractedAccsTariff-minume-charge">
								<datepickerkb date-format="yyyy-MM-dd">
									<input ng-model="fuelData.effectivefrom" type="text" required name="effectivefrom"/>
								</datepickerkb>
							</div>
							<span class="help-block" ng-show="carrierFuelForm.effectivefrom.$invalid">
								<span ng-show="carrierFuelForm.effectivefrom.$error.required">Effective From date is required</span>
							</span>
						</div>
                    </div>
                </div>
                <div class="btn-cont">
                	<button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
				<button  type="button" class="btn btn-primary" ng-disabled="carrierFuelForm.$invalid" ng-click="saveFuelData(carrierFuelForm)">Save / Update</button>
              	</div>
				<div class="alert" ng-show="error.status||success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
					<button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
					<strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
				</div>
        	</div>
			</form>
			
       </section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="{{ asset('js/CarrierFuelSetup.js') }}"> </script>
@endsection
