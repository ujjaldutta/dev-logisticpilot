@extends('layouts.user_home')
@section('angular_controller')
    CarrierFuelManagerController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
   Welcome to Carrier fuel Management!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('carrier_equipment_fuel_list', $carrier_id) !!}
@stop
@section('styles')
    
@stop
@section('sidebar')
  @include('_partials/carrier_equipment_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
  <section class="content-wrapper" ng-init="carrierId='{{$carrier_id}}'">
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12">
			<div class="add-new-shipment">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<div class="service_logo">
							<img src="{{ asset('uploads/carriers/'. $carrier->carrier_logo) }}" alt="Logo">
							{{{ $carrier->carrier_name or 'Unknown' }}}
						</div>
					</div>
				</div>
          </div>
            <div class="contract_table" style="min-height: 797px;">
                <div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
                <div class="clearfix"></div>
                <div class="row contract_buttons">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
                       <a href="{{url(\Config::get('app.frontPrefix'). '/setup-connectivity/download-fuel/'. $carrier_id )}}" class="btn btn-primary download_excel"><span></span>Download to Excel</a>
                       <a href="{{url(\Config::get('app.frontPrefix'). '/setup-connectivity/add-fuel/'. $carrier_id)}}" class="btn btn-primary download_excel">Add Fuel</a>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 pull-left">
                      <button type="button" class="btn btn-default" ng-click="deleteAllFuels()"><i class="fa fa-trash"></i></button>
                  </div>
                </div>
                <div class="table-responsive clearfix customer-table-transit">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">                    				
					<tr>
						<th align="center" valign="middle" scope="col" width="25px">
                            <div class="checkbox" style="width:15px;">
                                <label>
                                    <input type="checkbox" ng-click="checkAll()" ng-model="fuelData.selectAll">
                                </label>
                            </div>
                        </th>	
						@foreach($allFields as $field)
							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('{{$field->displayField}}')">{{ $field->displayName }} <i class="fa " ng-show="predicate === '{{$field->displayField}}'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>					
						@endforeach
						<th align="left" valign="middle" scope="col">&nbsp;</th>
					</tr>					
					<tr ng-repeat="fuel in fuelData.list">
						<td align="center" valign="middle" width="25px">
							<div class="checkbox" style="width:15px;">
								<label>
									<input type="checkbox" ng-model="fuel.selected" ng-change="checkSelection($index)">
								</label>
							</div>
						</td>
						@foreach($allFields as $field)
							<td align="left" valign="middle"><a href="{{ url(\Config::get('app.frontPrefix'). '/setup-connectivity/edit-fuel')}}/@{{fuel.id}}" ng-click="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
								<?php echo "{{ fuel.". $field->displayField ."}}"; ?></a></td>						
						@endforeach
						
						<td align="center" valign="middle">
							<a style="width:67px;margin-bottom:3px;" href="{{ url(\Config::get('app.frontPrefix'). '/setup-connectivity/edit-fuel')}}/@{{fuel.id}}" class="" title="Edit"><i class="fa fa-edit"></i></a>
                            
							<a class="" href="javascript:;" class="" ng-click="deleteFuel(fuel.id, true)" title="Delete"><i class="fa fa-trash"></i></a>
						</td>						
						
					</tr>
					<tr ng-hide="fuelData.list.length">
						<td colspan="{{ count($allFields) + 2}}" align="center"><strong>No Record Found!</strong></td>
					</tr>
              	  </table>
              </div>
				<div class="row">
					<div class="col-sm-4 col-md-4">
						<div class="row" style="margin-top:20px;">
							<div class="col-sm-7">
								<select ng-options="pageno.id as pageno.val for pageno in pageNos" ng-model="recordLimit" id="record_limit_selecter" name="record_limit_selecter" class="form-control"></select>
							</div>
						</div>
					</div>
					<!-- <div class="col-sm-6 col-md-4">
						<label>&nbsp;&nbsp;</label>
						<p>Showing record @{{(recordLimit * currentPage) > fuelData.count ? fuelData.count : recordLimit * currentPage}} of @{{fuelData.count}}</p>
					</div> -->
					<div class="col-sm-8 col-md-8">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-right">
									<span style="vertical-align: top; display: inline-block; margin: 26px 10px 0px 0px;">Showing record @{{(recordLimit * currentPage) > fuelData.count ? fuelData.count : recordLimit * currentPage}} of @{{fuelData.count}}</span>
									<pagination total-items="fuelData.count" items-per-page="recordLimit" ng-model="currentPage"  max-size="maxSize" ng-change="pageChanged()"></pagination>
								</div>
							</div>
						</div>
					</div>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- /content section-->
@endsection
@section('scripts')
<script src="{{ asset('js/CarrierFuelSetup.js') }}"> </script>
@endsection
