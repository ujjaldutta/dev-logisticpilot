@extends('layouts.user_home')
@section('angular_controller')
	CarrierAccessorialEditController
@stop
@section('page_class')
	contract-page
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('carrier_equipment_accessorial_add', $carrier_id) !!}
@stop
@section('styles')
	<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
     @include('_partials/carrier_equipment_sidebar')
@stop
@section('top_content')
	
@stop
@section('content')
<!-- content section-->
		<section class="content-wrapper" ng-init="newRecord=true; carrierId={{$carrier_id}};">
            <form name="carrierAccessorialForm" novalidate>
			<div class="add-new-shipment">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<div class="service_logo">
							<img src="{{ asset('uploads/carriers/'. $carrier->carrier_logo) }}" alt="Logo">
							{{{ $carrier->carrier_name or 'Unknown' }}}
						</div>
					</div>
				</div>
          </div>
			<div class="contract_table">
            	<h2>Add Accessorial</h2>
                <div class="row">
                	<div class="col-lg-3">
						<div class="form-group" ng-class="{'has-error': carrierAccessorialForm.accsID.$invalid}">
							<label>Accessorial</label>
							<select class="form-control" ng-options="accessorialType.id as accessorialType.accsname for accessorialType in accessorialTypes" ng-model="accessorialData.accsID" id="accsID" name="accsID" required>
								<option value="" disabled selected ng-hide="accessorialData.accsID">Please select</option>
							</select>
							<span class="help-block" ng-show="carrierAccessorialForm.accsID.$invalid">
								<span ng-show="carrierAccessorialForm.accsID.$error.required">Accessorial is required</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierAccessorialForm.crraccs_type.$invalid}">
							<label>Calculation Type</label>
							<select class="form-control" ng-options="calculationType.id as calculationType.ratetype for calculationType in calculationTypes" ng-model="accessorialData.crraccs_type" id="crraccs_type" name="crraccs_type" required>
								<option value="" disabled selected ng-hide="accessorialData.crraccs_type">Please select</option>
							</select>
							<span class="help-block" ng-show="carrierAccessorialForm.crraccs_type.$invalid">
								<span ng-show="carrierAccessorialForm.crraccs_type.$error.required">Calculation type is required</span>
							</span>
						</div>
                    </div>
                    <div class="col-lg-3">
                    	<div class="form-group" ng-class="{'has-error': carrierAccessorialForm.crraccs_minrate.$invalid}">
						  <label>Min Rate</label>
						  <input type="number" class="form-control" placeholder="" ng-model="accessorialData.crraccs_minrate" name="crraccs_minrate" required>
							<span class="help-block" ng-show="carrierAccessorialForm.crraccs_minrate.$invalid">
								<span ng-show="carrierAccessorialForm.crraccs_minrate.$error.required">Min Rate is required</span>
							</span>
						</div>
						<div class="form-group" ng-class="{'has-error': carrierAccessorialForm.crraccs_rate.$invalid}">
						  <label>Max Rate</label>
						  <input type="number" class="form-control" placeholder="" ng-model="accessorialData.crraccs_rate" name="crraccs_rate" required>
							<span class="help-block" ng-show="carrierAccessorialForm.crraccs_rate.$invalid">
								<span ng-show="carrierAccessorialForm.crraccs_rate.$error.required">Max Rate is required</span>
							</span>
						</div>
                    </div>
                    <div class="col-lg-3">
                    	<div class="form-group" ng-class="{'has-error': carrierAccessorialForm.crraccs_effectivefrom.$invalid}">
							<label>Effective From</label>
							<div class="contractedAccsTariff-minume-charge">
								<datepickerkb date-format="yyyy-MM-dd">
									<input ng-model="accessorialData.crraccs_effectivefrom" type="text" required name="crraccs_effectivefrom"/>
								</datepickerkb>
							</div>
							<span class="help-block" ng-show="carrierAccessorialForm.crraccs_effectivefrom.$invalid">
								<span ng-show="carrierAccessorialForm.crraccs_effectivefrom.$error.required">Effective From date is required</span>
							</span>
						</div>
                    </div>
                </div>
                <div class="btn-cont">
                	<button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
				<button  type="button" class="btn btn-primary" ng-disabled="carrierAccessorialForm.$invalid" ng-click="saveAccessorialData(carrierAccessorialForm)">Save / Update</button>
              	</div>
				<div class="alert" ng-show="error.status||success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
					<button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
					<strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
				</div>
        	</div>
			</form>
			
       </section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="{{ asset('js/CarrierAccessorialSetup.js') }}"> </script>
@endsection
