@extends('layouts.account_api_setup')
@section('angular_controller')
registrationStep3Controller
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('add_carrier') !!}
@stop
@section('styles')
	<!-- <link href="{{ asset('bower_components/angucomplete-alt/angucomplete-alt.css') }}" rel="stylesheet"> -->
	<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
	<!-- <style type="text/css">
		.highlight {color: #ff0000;}
		form.ng-invalid-autocomplete-required input.angucomplete-input-not-empty {border-color: red;}
	</style> -->
@stop
@section('sidebar')
	 <!-- sidebar-->
  <aside class="leftnavbar-wrapper">
    <div class="nav-col">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
            <div class="filter-head lanes_block no_bord">
                <h2>Your carrier list</h2>
				<form role="search" class="">
                <div class="input-group add-on">
                  <!-- <input type="text" id="srch-term" name="srch-term" placeholder="Search" class="form-control"> -->
				  <div angucomplete-alt id="client_search" placeholder="Search" pause="200" remote-url="/front/client-register?action=getSelectedCarriers&apiEnabled=@{{apiEnabledCarrierOnly==true ? 'true' : 'false'}}&q=" search-fields="name" title-field="name" minlength="2" remote-url-data-field="carriers" input-class="form-control" match-class="highlight" input-name="client_search" selected-object="selectedSearchCarrier" clear-selected="true"> </div>
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
                <carrier-list-setup></carrier-list-setup>                
				<div class="row contract_buttons carrier-connectivity">
                	<button type="button" class="btn btn-primary download_excel" ng-click="redirect()" style="width:auto;">Setup Carrier Connectivity</button> 
                </div>
            </div>
      	</div>
      </div>
    </div>
  </aside>
  <!-- /sidebar-->
@stop
@section('content')
	<form id="signup-form-step3" name="carrier_selection" novalidate>
 <input type="hidden" name="step" value="3" ng-init="formStep3.step=3" required>
 </form>
	<div class="reg_bot">
                  <h3>Select your carrier partners</h3>
                  <div class="row map_carrier_top">
                      <div class="col-lg-6">
                          <div class="map_carrier_box map_carrier_top_box_custom">
                              <div class="form-group">
                                  <label>Start Entering Carrier Name</label>
								  <div class="row file_upload">
									  <div class="col-sm-7">
										<div angucomplete-alt id="list-carriers" placeholder="Type Carrier Name e.g. UPS" pause="200" selected-object="currentSelectedProvider" remote-url="/list-carriers?selected=@{{ formStep3.carriers.join(',') }}&q=" search-fields="name" title-field="name" image-field="logo" minlength="1" input-class="form-control form-control-small" match-class="highlight" input-name="list-carriers" auto-match="true" clear-selected="true"></div>
									  </div>
									  <div class="col-sm-5">
										<button class="btn btn-primary" type="button" ng-click="addToExsitingList(carrier_selection)" ng-disabled="!currentSelectedProvider">Add to my Carrier List</button>
									  </div>
								  </div>
							  </div>
							  <div class="form-group" ng-show="currentSelectedProvider">
								You selected <img src="" ng-src="@{{ currentSelectedProvider.originalObject.logo }}" alt="@{{currentSelectedProvider.originalObject.name}}"> <strong>@{{currentSelectedProvider.originalObject.name}}</strong>. Click Add button to add to your list</span>
							  </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="file_upload text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                              <p>You didn't find carrier you are looking for? don't worry click below to add carrier or request support to add carrier</p>
                              <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-frame" ng-click="loadAddCarrierContent()">Add new Carrier</button>
                          </div>
                      </div>
                  </div>
                  <div class="row map_carrier_bottom">
						<selected-carrier-list></selected-carrier-list>
                  </div>
    </div>
@endsection
@section('modal_content')
<!-- Modal -->
<div class="modal fade" id="modal-frame" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	
</div>
<!-- Modal -->   
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/addCarrier.js') }}"> </script>
@endsection
