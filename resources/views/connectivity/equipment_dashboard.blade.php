@extends('layouts.user_home')
@section('angular_controller')
  CarrierEquipmentDashboardController  
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome to Carrier Equipment Management!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('carrier_equipment_dashboard') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  @include('_partials/carrier_equipment_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
  <section class="content-wrapper">
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="contract_table" style="min-height: 797px;">
                
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- /content section-->
@endsection
@section('scripts')
	<script src="{{ asset('js/CarrierEquipmentDashboard.js') }}"> </script>
@endsection
