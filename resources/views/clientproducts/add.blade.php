@extends('layouts.user_home')
@section('angular_controller')
CustomerProductEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('add_customer_product') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/clientproduct.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/customer_product_management_sidebar')
@stop
@section('top_content')
@stop
@section('content')
<!-- content section-->
<section class="content-wrapper" ng-init="newProduct=true; custID={{CustomerRepository::getCurrentCustomerID()}}">
    <form name="clientProductForm" novalidate>
        <div class="contract_table">
            <div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
            <div class="clearfix"></div>
            <h2>Product Setup</h2>
            <div class="row div-top-25">
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': clientProductForm.item_code.$invalid}">
                        <label>Item ID *</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_code" name="item_code" required>
                        <span class="help-block" ng-show="clientProductForm.item_code.$invalid">
                            <span ng-show="clientProductForm.item_code.$error.required">Product Code is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': clientProductForm.item_name.$invalid}">
                        <label>Item Name *</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_name" name="item_name" required>
                        <span class="help-block" ng-show="clientProductForm.item_name.$invalid">
                            <span ng-show="clientProductForm.item_name.$error.required">Item Name is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <label>Freight Class</label>
                    <div ng-dropdown-multiselect="" options="freeClassTypes" selected-model="clientProductData.item_class" extra-settings="freeclassSettings"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="col-lg-6 div-padd-0">
                        <div class="form-group">
                            <label>Weight</label>
                            <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_weight" name="item_weight">
                        </div>
                    </div>
                    <div class="col-lg-6 div-paddright-0">
                        <label>&nbsp;</label>
                        <div ng-dropdown-multiselect="" options="weightTypes" selected-model="clientProductData.item_weightType" extra-settings="weightSettings"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_qty" name="item_qty">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>SKU</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_SKU" name="item_SKU">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Cube</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_cube" name="item_cube">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Monetary Value</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_monetary" name="item_monetary">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Package Type</label>
                        <div ng-dropdown-multiselect="" options="packageTypes" selected-model="clientProductData.item_packagetypeid" extra-settings="packageSettings"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Commodity</label>
                        <div ng-dropdown-multiselect="" options="commodityTypes" selected-model="clientProductData.item_commodity" extra-settings="comSettings"></div>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>NMFC</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_nmfc" name="item_nmfc">
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Customer Part Num</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_custnum" name="item_custnum">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Manfuacturer Part Num</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_mannum" name="item_mannum">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Distributer Part Num</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_distnum" name="item_distnum">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-9">
                    <div class="col-lg-6  div-padd-0">
                        <div><label>Temperature Range</label></div>
                        <div class="col-lg-12 div-padd-0">
                            <div class="form-group">
                                <div class="col-lg-4 div-paddleft-0">
                                    <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_tempmin" name="item_tempmin">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_tempmax" name="item_tempmax">
                                </div>
                                <div class="col-lg-4">
                                    <div ng-dropdown-multiselect="" options="temperatureTypes" selected-model="clientProductData.item_temperaturetype" extra-settings="tempSettings"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 div-paddright-0">
                        <div><label>Dimensions</label></div>
                        <div class="form-group">
                            <div class="col-lg-12 div-padd-0 div-paddright-0">
                                <div class="col-lg-2 div-padd-0">
                                    <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_width" name="item_width">
                                </div>
                                <div class="col-lg-1 div-top-7 text-center">X</div>
                                <div class="col-lg-2 div-padd-0">
                                    <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_length" name="item_length">
                                </div>
                                <div class="col-lg-1 div-top-7 text-center">X</div>
                                <div class="col-lg-2 div-padd-0">
                                    <input type="text" class="form-control" placeholder="" ng-model="clientProductData.item_height" name="item_height">
                                </div>
                                <div class="col-lg-4 div-paddright-0">
                                    <div ng-dropdown-multiselect="" options="dimmensionsTypes" selected-model="clientProductData.item_dimuomtype" extra-settings="dimSettings"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row div-top-7">
                <div class="col-lg-3">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" ng-model="clientProductData.item_returnable"> Returnable
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" ng-model="clientProductData.item_stackability"> Stackability
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" ng-model="clientProductData.item_hazmat"> Hazmat
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>

            <div class="btn-cont div-top-50">
                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
                <button  type="button" class="btn btn-primary" ng-disabled="clientProductForm.$invalid" ng-click="saveProductData(clientProductForm)">Save / Update</button>
            </div>
            <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
                <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
                <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
            </div>
        </div>
    </form>

</section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CustomerProductManager.js')}}"></script>
@endsection