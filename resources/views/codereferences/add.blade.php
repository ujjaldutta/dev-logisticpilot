@extends('layouts.user_home')
@section('angular_controller')
    CodeReferenceEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('add_customer_product') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/clientproduct.css')}}" rel="stylesheet">
@stop
@section('sidebar')
    @include('_partials/code_reference_management_sidebar')
@stop
@section('top_content')
@stop
@section('content')
<!-- content section-->
<section class="content-wrapper" ng-init="newCode=true">
    <form name="codeReferenceForm" novalidate>
        <div class="contract_table">
            <h2>Code Reference Setup</h2>
            <div class="row div-top-25">
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': codeReferenceForm.codeId.$invalid}">
                        <label>Code ID *</label>
                        <input type="text" class="form-control" placeholder="" ng-model="codeReferenceData.codeId" name="codeId" required>
                        <span class="help-block" ng-show="codeReferenceForm.codeId.$invalid">
                            <span ng-show="codeReferenceForm.codeId.$error.required">Code Id is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': codeReferenceForm.codeValue.$invalid}">
                        <label>Code Name *</label>
                        <input type="text" class="form-control" placeholder="" ng-model="codeReferenceData.codeValue" name="codeValue" required>
                        <span class="help-block" ng-show="codeReferenceForm.codeValue.$invalid">
                            <span ng-show="codeReferenceForm.codeValue.$error.required">Item Name is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': codeReferenceForm.codeType.$invalid}">
                        <label>Code Type *</label>
                        <input type="text" class="form-control" placeholder="" ng-model="codeReferenceData.codeType" name="codeType" required>
                        <span class="help-block" ng-show="codeReferenceForm.codeType.$invalid">
                            <span ng-show="codeReferenceForm.codeType.$error.required">Code Type is required</span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="btn-cont div-top-50">
                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
                <button  type="button" class="btn btn-primary" ng-disabled="codeReferenceForm.$invalid" ng-click="saveProductData(codeReferenceForm)">Save / Update</button>
            </div>
            <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
                <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
                <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
            </div>
        </div>
    </form>

</section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CodeReferenceManager.js')}}"></script>
@endsection
