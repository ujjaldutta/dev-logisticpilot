 <div class="contract_table" style="min-height: 797px;" ng-show="rightpanel=='Fuel'" >
								
								<div id="horizontalTab1" class="tab-hld">
									<ul class="resp-tabs-list">
										<li ng-click="setcurrentfuelTab('fuel')">Carrier Fuel</li>
										<li ng-click="setcurrentfuelTab('scac')">SCAC Carrier Fuel</li>
										<li>Upload Fuel</li>
									</ul>
									<div class="resp-tabs-container">
										<div class="tab-content">
											<div class="row contract_buttons">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4">
												 <strong>{{ Lang::get('profile.profile_selected') }}	: @{{contractlist.profile.RateProfileDescription}}</strong> 	
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right ml10">
												<button type="button" class="btn btn-primary" ng-click="addNewFuel()">Add Fuel <i class="fa fa-plus-circle"></i></button>
													<button type="button" class="btn btn-primary download_excel"><span></span>Download to Excel</button>
												</div>
												
											</div>
											
											<div class="table-responsive clearfix">
														 <div class="col-sm-12 bordered-col">
														 <div class="row heading-row">
														 
														 
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Min Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Max Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Rate /Perc</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective From</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective To</div></div></div>
														   <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Fuel Type</div></div></div>
														    <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Fuel Calculation</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">&nbsp;</div></div></div>
														</div>
														
														<div class="row table-body-div" endless-scroll="assoc in fuel" assocdate>
															
														  <form method="post" name="sac_fuel_@{{$index}}" id="sac_fuel_@{{$index}}">
														<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="assoc._token='{{ csrf_token() }}'" required>
											
														<input type="hidden"   name="cid" ng-model="assoc.cid"  ng-init="assoc.cid"  value="@{{assoc.cid}}">
														
														
														
														  
														  <div class="col-sm-1">
																<div class="form-group">
																		<input type="text" class="form-control" id=""  name="min_rate" ng-model="assoc.min_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control" id=""  name="max_rate" ng-model="assoc.max_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control " id=""  name="rate_percentage" ng-model="assoc.rate_percentage" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																	<div class="form-group">
																		<input type="text" class="form-control fromdate" id=""  name="effective_from" ng-model="assoc.effective_from"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																		<div class="form-group">
																		<input type="text" class="form-control todate" id=""  name="effective_to" ng-model="assoc.effective_to"  autocomplete="off" required>
																	</div>
														  </div>
														  
														  
														  <div class="col-sm-2">
																<div class="form-group">
																		<select class="form-control" name="fuel_type" ng-model="assoc.fuel_type" ng-options="option.name for option in fueltype track by option.id" required></select>	
															</div>
														  </div>
														  
														  
														  <div class="col-sm-2">
																<div class="form-group">
																		<select class="form-control" name="fuel_calculation" ng-model="assoc.fuel_calculation" ng-options="option.name for option in calctype track by option.id" required></select>
																	</div>
														  </div>
														  
														  <div class="col-sm-1">
																						  
															  <button  type="submit" ng-click="saveFuel(assoc);" class="button-simple-link btn-icononly" ng-disabled="sac_fuel_@{{$index}}.$invalid"><i class="fa fa-floppy-o"></i></button>
														  <button class="button-simple-link btn-icononly"  type="button" ng-click="deleteFuel(assoc,$index)"><i class="fa fa-trash-o"></i></button>
														  </div>
														  </form>
														</div>
															
														<div class="row" ng-hide="fuel.length">
																 <div class="col-sm-12 bordered-col">
																	 <strong>No Record Found!</strong>
																	 </div>
															 </div>
														
														</div>
														
															<p ng-show="loading" class="text-center"> Loading...</p>
														</div>
													
											
											
											
											
											
											
											
											</div>
										<div class="tab-content">
											<div class="row">
																	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right ml10">
																	<button type="button" class="btn btn-primary pull-right ml10" ng-click="addNewSCACFuel()">Add Fuel <i class="fa fa-plus-circle"></i></button>
																		<button type="button" class="btn btn-primary download_excel pull-right"><span></span>Download to Excel</button>
																	</div>
																</div>
														<div class="table-responsive clearfix">
														 <div class="col-sm-12 bordered-col">
														 <div class="row heading-row">
														 
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Carrier</div></div></div>
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Min Rate</div></div></div>
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Max Rate</div></div></div>
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Rate /Perc</div></div></div>
														 <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective From</div></div></div>
														 <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective To</div></div></div>
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Fuel Type</div></div></div>
														    <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Fuel Calculation</div></div></div>
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">&nbsp;</div></div></div>
														</div>
														
														<div class="row table-body-div" endless-scroll="assoc in SCACfuel" assocdate>
															
														  <form method="post" name="sac_fuel_@{{$index}}" id="sac_fuel_@{{$index}}">
														<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="assoc._token='{{ csrf_token() }}'" required>
											
														<input type="hidden"   name="cid" ng-model="assoc.cid"  ng-init="assoc.cid"  value="@{{assoc.cid}}">
														
														
														<div class="col-sm-1">
																<div class="form-group">
																		<input type="hidden" class="form-control" id=""  name="scac" ng-model="assoc.scac" >
																		
																		<div angucomplete-alt
																				  id="fuelscac"
																				  placeholder="Search Carrier"
																				  pause="500"
																				  selected-object="onCarrierselect"
																				 focus-out="onCarrierselect()"
																				  remote-api-handler="carrierAPI"
																				  remote-url-request-formatter="remoteUrlRequestFn"
																				remote-url-data-field="items"
																				  title-field="name"
																				  description-field="description"
																				  minlength="2"
																				 ng-model="assoc.carrier"
																				 initial-value="@{{assoc.carrier}}"
																				  input-class="form-control"
																				  match-class="highlight" field-required="true">
																		</div>
																		
																	</div>
														  </div>
														  
														 
														  <div class="col-sm-1">
																<div class="form-group">
																		<input type="text" class="form-control" id=""  name="min_rate" ng-model="assoc.min_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control" id=""  name="max_rate" ng-model="assoc.max_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control " id=""  name="rate_percentage" ng-model="assoc.rate_percentage" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																	<div class="form-group">
																		<input type="text" class="form-control fromdate" id=""  name="effective_from" ng-model="assoc.effective_from"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																		<div class="form-group">
																		<input type="text" class="form-control todate" id=""  name="effective_to" ng-model="assoc.effective_to"  autocomplete="off" required>
																	</div>
														  </div>
														  
														  <div class="col-sm-1">
																<div class="form-group">
																	
																	
																	<select class="form-control" name="fuel_type" ng-model="assoc.fuel_type" ng-options="option.name for option in fueltype track by option.id" required></select>	
																		
															</div>
														  </div>
														  
														  
														  <div class="col-sm-2">
																<div class="form-group">
																		<select class="form-control" name="fuel_calculation" ng-model="assoc.fuel_calculation" ng-options="option.name for option in calctype track by option.id" required></select>
																	</div>
														  </div>
														  
														  
														  <div class="col-sm-1">
																						  
															  <button  type="submit" ng-click="saveSCACFuel(assoc);" class="button-simple-link btn-icononly" ng-disabled="sac_fuel_@{{$index}}.$invalid"><i class="fa fa-floppy-o"></i></button>
														  <button class="button-simple-link btn-icononly"  type="button" ng-click="deleteSCACFuel(assoc,$index)"><i class="fa fa-trash-o"></i></button>
														  </div>
														  </form>
														</div>
															
														<div class="row" ng-hide="SCACfuel.length">
																 <div class="col-sm-12 bordered-col">
																	 <strong>No Record Found!</strong>
																	 </div>
															 </div>
														
														</div>
														
															<p ng-show="loading" class="text-center"> Loading...</p>
														</div>
													
										</div>
										<div class="tab-content">
											<div class="row upload_rate">
												<div class="col-lg-5 col-md-4 col-sm-6">
													  <p><strong>This is an optional. DO not confuse, if you do not know what is it?</strong></p>
													<p>I would like to use my negotiated Fuel not carrier Fuel <a href="javascript:void(0);" class="on"></a></p>
													<button type="button" class="btn btn-primary"><span></span>Download Fuel Excel Template</button>
													<div class="form-group doc-uploadhld">
														<div class="inputuploadhld">
															<label>Upload Fuel</label>
															<input id="" type="file">
															<button  type="button" class="btn btn-default">Upload File</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							
							
				</div>
