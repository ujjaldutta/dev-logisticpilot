@extends('layouts.user_home')
@section('angular_controller')
    ProfileSearchController  
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop

@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
    
    <link href="http://ui-grid.info/release/ui-grid.css" rel="stylesheet">
	
	
	
   
@stop


@section('top_content')
    
@stop
@section('content')


<div class="row">
    	
        <div class="rate_test clearfix">
        	<h3>Shipment Quote</h3>
            <form method="post" name="searchform">
				
            <div class="row">
				<input type="hidden" id="searchtoken"   name="_token" value="{{ csrf_token() }}" ng-model="search._token" ng-init="search._token='{{ csrf_token() }}'" required>
				<div class="col-lg-12 col-md-3 col-sm-4 ">
					<div class="pull-right input-group">
						<select name="profile" class="form-control" ng-model="search.profile" required>
							<option value="">Please Select</option>
						<option ng-repeat="option in profilelist" value="@{{option.RateProfileCode}}">@{{option.RateProfileDescription}}</option>
						</select>
					</div>
					</div>
			</div>
            
            <div class="ship-location">
            <div class="row">
            	<div class="col-lg-2 col-md-3 col-sm-4">
                	<h5>Pick up Location</h5>
                    <div class="input-group">
					
                  <input type="text" kl_virtual_keyboard_secure_input="on" class="form-control" placeholder="Address" id="address1" ng-model="search.source"  g-places-autocomplete force-selection="true" options="autocompleteOption" autocomplete="off" ng-keypress="onlyNumbers($event)"  required>
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                  </span> </div>
                </div>
              <div class="col-lg-8 col-md-6 col-sm-4 ">
                	
                	<div class="custom-arrow">
                    	<div class="custom-arrow-in"></div>
                    </div>
              </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
						
                <h5>Delivery Location</h5>
                    <div class="input-group">
                 
                  <input type="text" class="form-control" placeholder="Address" id="address2" ng-model="search.destination" g-places-autocomplete force-selection="true" autocomplete="off"  options="autocompleteOption"   ng-keypress="onlyNumbers($event)" required>
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
                </div>
            </div>
            </div>
            </div>
            <div class="dashed-area">
            	<div class="row">
               	<div class="col-lg-1 col-md-2 col-sm-3">
                  <div class="checkbox">
                    <label><input type="checkbox">LIFT GATE</label>
                  </div>
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-3">
                  <div class="checkbox">
                    <label><input type="checkbox">INSIDE</label>
                  </div>
                    </div>
              <div class="col-lg-2 col-md-2 col-sm-3">
                  <div class="checkbox">
                    <label><input type="checkbox">SINGLE SHIPMENT</label>
                  </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3">
                  <div class="form-group">
                  <select class="form-control">
                    <option>Additional Accessorials</option>
                  </select>
                  </div>
              </div>
                    
            </div>
            
				<div class="row">
						<div class="col-lg-12 col-md-3 col-sm-3">
							  <div class="form-inline">
									<div class="form-group filter-date pull-right">
									<label for="ship-date">Ship Date</label>
									<input type="text" placeholder="Eg..(28/01/2016)" id="datepicker" class="form-control" ng-model="search.shipdate" required>
								  </div>
							  </div>
						</div>
				</div>
        	
        	</div>
        	<h5>Enter Commodity</h5>
            <div class="table_calc_block">
                <div class="table-responsive" >
                      <table width="100%" cellspacing="5" cellpadding="0" border="0" class="table noBord" id="carrierCost2">
                        <tbody><tr>
                          <th>Handling Unit</th>
                          <th>Unit</th>
                          <th>Pieces</th>
                          <th>Weight</th>
                          <th>NMFC</th>
                          <th>PCF</th>
                          <th>Class</th>
                          <th>Hazmat </th>
                          <th></th>
                        </tr>
                        <tr data-ng-repeat="choice in multisearch">
                        <td>
                          	<div class="form-group clearfix">
                          	<select style="width:100px;" class="form-control">
                              <option></option>
                            </select>
                            </div>
                          </td>
                          <td><div class="form-group clearfix">
                              <input type="text" placeholder="1" class="form-control">
                            </div></td>
                         
                          <td>	
                          	 <div class="form-group clearfix">
                              <input type="text" placeholder="1" class="form-control" name="pieces[]" ng-model="search.pieces[$index]">
                            </div>
                          </td>
                          <td><div class="form-group clearfix">
                              <input type="text" placeholder="1" class="form-control" name="weight[]" ng-model="search.weight[$index]">
                            </div></td>
                          
                          
                          <td><div class="form-group clearfix">
                              <input type="text" placeholder="1" class="form-control">
                            </div></td>
                          <td><div id="div_1" class="form-group clearfix">
                              <input type="text" readonly="" placeholder="11" class="pcf-drop form-control" id="in_1">
                              
                            </div></td>
                           <td><div class="form-group clearfix">
                          	<select style="width:100px;" class="form-control">
                              <option></option>
                            </select>
                            </div></td>
                          <td>
                          	<div class="form-group clearfix">
                            <input type="checkbox" value="" name="">
                            </div> 
                          </td>
                          <td></td>
                        </tr>
                        <tr><td colspan="10">
							<div class="btn-group-new m-top2 pull-right">
							<button class="btn btn-xs btn-danger"  ng-click="removeNewSearch()"><span class="glyphicon glyphicon-minus"></span></button>
							</div>
							</td></tr>
                      </tbody></table>
                </div>
                
                <div class="addBtnCont clearfix">
                        <button class="btn btn-primary btn-xs addRowBtn_carrier" type="button" ng-click="addNewSearch()"><span class="plus"></span>Add Another Item</button>
                    </div>
                    <div class="pull-right">
						
						<button type="submit" ng-disabled="searchform.$invalid" class="btn btn-primary download_excel" ng-click="SearchProfile()"><span></span>Get Quote</button>
						</div>
            </div>
        </form>
    </div>
    </div>
    
		  
	
	
	
	
	
	
	
	
	
	
	
	
	<aside class="leftnavbar-wrapper no-mar">
    <div class="nav-col no-mar">
      <div class="navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
        <div class="panel panel-default">
          <div class="filter-head shipment-status">
            <h2>Transportation Mode </h2>
            <div class="">
              <label class="radio-inline">
                <input type="radio" value="option1" id="inlineRadio1" name="inlineRadioOptions">
                LTL </label>
              <label class="radio-inline">
                <input type="radio" value="option2" id="inlineRadio2" name="inlineRadioOptions">
                TL </label>
            </div>
          </div>
          <div class="filter-head shipment-status">
            <h2>Equipment Type </h2>
            <select size="5" name="example-basic" multiple="multiple" title="Basic">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option>
            </select>
          </div>
          <div class="filter-head pricing nobord">
            <h2>Pricing
              <button class="btn-reset pull-right" type="button"><span>Reset</span></button>
              <p>By Filter</p>
            </h2>
            <div class="slide-range">
              <!--slider-->
              <div class="slider">
                <input id="price_range2">
              </div>
              <!--slider-->
            </div>
            <h2 class="transit">Transit days
              <button class="btn-reset pull-right" type="button"><span>Reset</span></button>
            </h2>
            <div class="slide-range">
              <!--slider-->
              <div class="slider">
                <input id="price_range3">
              </div>
              <!--slider-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </aside>
  
  
  
  
  
  
  
  
  
  <section class="content-wrapper">
      <div class="content-subcontainer">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="contract_table">
            	<ul class="carrier_list">
                    <li class="active"><a href="#">All Carriers</a></li>
                    <li>
                        <a href="#">
                            <img alt="" src="{{asset('images/fedex_small.jpg')}}">
                            FedEx
                            <span>$2580</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="{{asset('images/ups_small.jpg')}}">
                            UPS
                            <span>$2780</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="{{asset('images/eestes_small.png')}}">
                            Estate Express
                            <span>$2980</span>
                        </a>
                    </li>
                </ul>
                <div class="result_top">
                	<ul>
                    	<li>About 3 results</li>
                        <li class="email"><a href="#">Email All</a></li>
                        <li class="print"><a href="#">Print All</a></li>
                    </ul>
                </div>
           	  	<div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
           	  	<div class="quote_cont" endless-scroll="searchres in searchresult">
                	<div class="table-responsive">
                	  	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th width="20%" valign="middle" align="left">&nbsp;</th>
                                    <th valign="middle" align="left" data-hide="phone,tablet">Transit Days</th>
                                    <th valign="middle" align="left" data-hide="phone,tablet">Service</th>
                                    <th valign="middle" align="left" data-hide="phone,tablet">Carrier Liability</th>
                                    <th valign="middle" align="left" data-hide="phone,tablet">Carrier Performance</th>
                                    <th valign="middle" align="left" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td valign="middle" align="left">
                                    	<img alt="" src="{{asset('images/fedex-logo.png')}}"> @{{searchres.Carrier}}
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td valign="middle" align="left">2 days</td>
                                    <td valign="middle" align="left">Standard</td>
                                    <td valign="middle" align="left">Balance: 5,000</td>
                                    <td valign="middle" align="left"><img alt="" src="{{asset('images/star.png')}}"></td>
                                    <td valign="middle" align="left" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$@{{searchres.Rate}}</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                      <tbody><tr>
                                                        <td valign="top" align="left">Freight</td>
                                                        <td valign="top" align="right">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td valign="top" align="left">Fuel</td>
                                                        <td valign="top" align="right">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td valign="top" align="left">Discount</td>
                                                        <td valign="top" align="right">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td valign="top" align="left">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td valign="top" align="right">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td valign="top" align="left">Accessorial Description</td>
                                                        <td valign="top" align="right">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td valign="top" align="left">Total Shipment Cost :</td>
                                                        <td valign="top" align="right" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <br>
                                        <button type="submit" class="btn btn-primary">Book <i class="fa fa-angle-right"></i></button>
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                       <ul class="bot_buttons">
                        	<li><a href="#"><i class="fa fa-heart"></i>Save Quote</a></li>
                            <li><a href="#"><i class="fa fa-envelope-o"></i>Email this Quote</a></li>
                            <li><a href="#"><i class="fa fa-print"></i>Print this Quote</a></li>
                            <li><a href="#"><i class="fa fa-truck"></i>Create Shipment</a></li>
                            <li><a id="t-info" href="#" class="t-infolnk"><i class="fa fa-info-circle"></i>Terminal Info</a>
                            <div class="t-info">
                            	<h3>Terminal Info</h3>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table_cont">
								  <tbody><tr>
									<td valign="middle" align="left">Freight</td>
									<td valign="middle" align="right">$250.00</td>
								  </tr>
								  <tr>
									<td valign="middle" align="left">Fuel</td>
									<td valign="middle" align="right">$25</td>
								  </tr>
								  <tr>
									<td valign="middle" align="left">Freight</td>
									<td valign="middle" align="right">$250.00</td>
								  </tr>
								  <tr>
									<td valign="middle" align="left">Freight</td>
									<td valign="middle" align="right">$250.00</td>
								  </tr>
								  <tr>
									<td valign="middle" align="left">Freight</td>
									<td valign="middle" align="right">$250.00</td>
								  </tr>
								</tbody></table>

                            </div>
                            </li>
                            <li><a id="add-assu" class="add-assulnk" href="javascript:void(0)"><i class="fa fa-plus-circle"></i>Add Assuarance</a>
                            <div class="dropdown-menu add-assu" id="drpd1" >
                                <h5>Add Assuarance <i class="fa fa-times pull-right assu-close"></i></h5>
                                <div class="drpd1-inner"><label>sample text</label>
                                <input type="text" class="form-control" name="">
                                <label>sample text</label>
                                <input type="text" class="form-control" name="">
                                </div>
                              </div>
                            </li>
                        </ul>
                	</div>
              	</div>
                
               <div class="quote_cont">
				<div class="table-responsive"><strong>@{{resultmsg}}</strong></div>
				
				</div>
            
            
            
            </div>
          </div>
        </div>
      </div>
    </section>
  
  
  
  
  	  

@endsection
@section('scripts')


    <script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
        async defer></script>
<script src="{{ asset('js/ProfileSearch.js') }}"> </script>


@endsection

