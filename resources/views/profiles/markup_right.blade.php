
             <div class="contract_table" style="min-height: 797px;" ng-show="rightpanel=='Markup'">
						
						
				
				<div id="horizontalMarkupTab1" class="tab-hld">
									<ul class="resp-tabs-list">
										<li>New Markup Bucket</li>
										<li ng-click="loadMarkup('')">Assign Markup by profile</li>
										<li>Custom Markup for Profile</li>
									</ul>
									<div class="resp-tabs-container">
										<div class="tab-content">
											<h3 class="markup">New Up Charge/Markup </h3>
											<button type="button" class="btn btn-primary"  tabindex="-1" data-toggle="modal" data-target="#markupDetails" >New Markup</button>
											
											
											
											<div class="markup-cont" >
												<div class="row">
												<div  ng-repeat="markup in newmarkuplist" class="clearthird">
													
													
														<div class="col-md-6 col-sm-6">
													
															
															<div class="panel panel-primary">
																<div class="panel-heading">
																  <h3 class="panel-title">Mark up Name: @{{markup.UpChargeDescription}}</h3>
																  <ul>
																	<li><a href="javascript:void(0)" ng-click="deleteMarkup(markup._id)"><i class="fa fa-trash-o"></i></a></li>
																	<li><a href="javascript:void(0)"  ng-click="updateMarkup(markup)"><i class="fa fa-floppy-o"></i></a></li>
																  </ul>
																</div>
																<div class="panel-body">

																	
																		<div class="table-responsive">
																		<table class="table mark-table markup-tabtable" border="0" cellpadding="0" cellspacing="0" width="100%">
																		  <tbody><tr class="full-label">
																			<td align="left" valign="top" width="25%">&nbsp;</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Percentage</label>
																				  <input class="form-control" id="" placeholder="10%" type="text" ng-model="markup.TopLevelUpCharge.ChargePercent" name="ChargePercent" >
																			</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Min Charge</label>
																				  <input class="form-control" id="" placeholder="50" type="text"  ng-model="markup.TopLevelUpCharge.MinCharge" name="MinCharge" >
																				</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Charge Method</label>
																				  
																				  <select name="TopChargeType"  disabled>
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-model="markup.TopLevelUpCharge.TopChargeType" ng-selected="markup.TopLevelUpCharge.TopChargeType == option.id">@{{option.name}}</option>
																					</select>
																				</td>
																		  </tr>
																		  <tr>
																			<td colspan="4" align="left" valign="top">
																			<div class="carrier">Do you like to setup upcharge by carrier?
																			
																			<label class="radio-inline"><input type="radio" name="optradio@{{$index}}[]" ng-click="fetchCarrier('yes',$index)"  ng-model="markup.setCarrier" value="yes" ng-checked="markup.UpchargeByCarrier.length>0">Yes</label>
										
										
										<label class="radio-inline"><input type="radio" name="optradio@{{$index}}[]"  ng-click="fetchCarrier('no',$index)"  ng-model="markup.setCarrier" value="no" ng-checked="markup.UpchargeByCarrier.length==0">No</label></div>
										
																		
																		</td>
																		  </tr>
																		  <tr ng-show="markup.setCarrier=='yes'">
																		  <td align="left" valign="top">Select Carrier</td>
																			<td align="left" valign="top" colspan="2">
																				
																				
																	<div angucomplete-alt
																	  id="markupcarrier"
																	  placeholder="Search Carrier"
																	  pause="500"
																	  selected-object="markup.markupcarrier"
																	  focus-out="selectedMarkupCarrier()"
																	  remote-api-handler="searchCarrierAPI"
																	  remote-url-request-formatter="remoteUrlRequestFn"
																	  remote-url-data-field="items"
																	  title-field="name"
																	  description-field="description"
																	  minlength="2"
																	  name="markupcarrier"
																	  input-class="form-control"
																	 
																	  match-class="highlight" ng-model="markup.markupcarrier"  >
																	</div>
																				
																				</td>
																		  <td align="left" valign="top">
																		  <button  class="btn btn-primary" ng-click="addCarrier(markup.markupcarrier,$index);" type="submit" >Add</button>
																		  </td>
																		  </tr>
																		  </table>
																		  
																		  
																		  <table class="table mark-table markup-tabtable" border="0" cellpadding="0" cellspacing="0" width="100%">
																		   <tr ng-repeat="carriermarkup in markup.UpchargeByCarrier">
																			<td align="left" valign="top">@{{carriermarkup.CarrierName}}</td>
																			<td align="left" valign="top">
																				<input class="form-control" id="" placeholder="10%" type="text" ng-model="markup.UpchargeByCarrier[$index].ChargePercent" name="ChargePercent[]" ></td>
																			<td align="left" valign="top">
																			  <input class="form-control" id="input" placeholder="50" type="text" ng-model="markup.UpchargeByCarrier[$index].carrierMinCharge" name="carrierMinCharge[]" >
																		   </td>
																			<td align="left" valign="top">
																			  
																			  <select name="ChargeType[]" id="" ng-model="markup.UpchargeByCarrier[$index].ChargeType" >
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-selected="carriermarkup.ChargeType == option.id">@{{option.name}}</option>
																					</select>
																			  
																			</td>
																			<td align="left" valign="top">
																				<button ng-click="removeCarrierMarkup($parent.$index,$index)" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
																				</td>
																		  </tr>
																		 
																		 
																	  </tbody></table>
																	</div>
																	
																</div>
															  </div>
														
														</div>
												</div>
												</div>
											
											</div>
											
											
											
										</div>
										<div class="tab-content">
											
											<h3 class="markup">Up Charge/Markup </h3>
											<div class="profile-search">
												<form role="form" class="form-inline">
													<div class="form-group">
														<label>Start Entering Profile to search</label>
														<input class="form-control" id="" placeholder="Efdex" type="text" name="markupsearch" ng-model="markupsearch">
														<!--<button type="button" class="btn btn-primary"  tabindex="-1" data-toggle="modal" data-target="#markupDetails" >New Markup</button>-->
													</div>
												</form>
											</div>
											<div class="markup-cont" >
												<div class="row">
												<div id="markuplist" class="clearthird" ng-repeat="markup in markuplist" >
													
													
														<div class="col-md-6 col-sm-6">
														<form role="form" class="form-inline" >
															<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="markup._token='{{ csrf_token() }}'" required>
															<input type="hidden"   name="UpChargeCode" value="@{{markup.UpChargeCode}}"  ng-init="markup.UpChargeCode" required>
															<input type="hidden"   name="_id" value="@{{markup._id}}"  ng-init="markup._id" required>
															
															<div class="panel panel-primary">
																<div class="panel-heading">
																  <h3 class="panel-title">Mark up Name: @{{markup.UpChargeDescription}}</h3>
																  <ul>
																	<li><a href="javascript:void(0)" ng-click="deleteMarkup(markup._id)"><i class="fa fa-trash-o"></i></a></li>
																	
																	<li><a href="javascript:void(0)"  ng-click="updateMarkup(markup)"><i class="fa fa-floppy-o"></i></a></li>
																  </ul>
																</div>
																<div class="panel-body">

																<!--		<div class="form-group">
																		  <label>Select Profile to Up Charge:</label>
																		<select name="ProfileId" id="ProfileId" ng-model="markup.ProfileId">
																		  <option ng-repeat="option in profilelist" value="@{{option.RateProfileCode}}" ng-selected="markup.MappedProfile.ProfileId == option.RateProfileCode">@{{option.RateProfileDescription}}</option>
																		</select>
					
																		</div>
																		<hr>-->
																		<div class="table-responsive">
																		<table class="table mark-table markup-tabtable" border="0" cellpadding="0" cellspacing="0" width="100%">
																		  <tbody><tr class="full-label">
																			<td align="left" valign="top" width="25%">&nbsp;</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Percentage</label>
																				  <input class="form-control" id="" placeholder="10%" type="text" ng-model="markup.TopLevelUpCharge.ChargePercent" name="ChargePercent" disabled>
																			</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Min Charge</label>
																				  <input class="form-control" id="" placeholder="50" type="text"  ng-model="markup.TopLevelUpCharge.MinCharge" name="MinCharge" disabled>
																				</td>
																			<td align="left" valign="top" width="25%">
																				  <label>Charge Method</label>
																				  
																				  <select name="TopChargeType" id="UpChargeCode" ng-model="markup.TopLevelUpCharge.TopChargeType" disabled>
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-selected="markup.TopLevelUpCharge.TopChargeType == option.id">@{{option.name}}</option>
																					</select>
																				</td>
																		  </tr>
																		  <tr>
																			<td colspan="4" align="left" valign="top">
																			
																			<div class="carrier" ng-if="markup.UpchargeByCarrier.length==0">Do you like to setup upcharge by carrier?<label class="radio-inline"><input name="optradio" type="radio" value="Yes" ng-value="Yes" ng-model="markup.carrierUpcharge" ng-click="fetchCarrier('yes',$index)" disabled>Yes</label>
										<label class="radio-inline"><input name="optradio" type="radio" value="No" ng-value="No" ng-model="markup.carrierUpcharge" ng-click="fetchCarrier('no',$index)" disabled>No</label></div>
																		</td>
																		  </tr>
																		  <tr ng-repeat="carriermarkup in markup.UpchargeByCarrier">
																			<td align="left" valign="top">@{{carriermarkup.CarrierName}}</td>
																			<td align="left" valign="top">
																				<input class="form-control" id="" placeholder="10%" type="text" ng-model="markup.UpchargeByCarrier[$index].ChargePercent" name="ChargePercent[]" disabled></td>
																			<td align="left" valign="top">
																			  <input class="form-control" id="input" placeholder="50" type="text" ng-model="markup.UpchargeByCarrier[$index].carrierMinCharge" name="carrierMinCharge[]" disabled>
																		   </td>
																			<td align="left" valign="top">
																			  
																			  <select name="ChargeType[]" id="" ng-model="markup.UpchargeByCarrier[$index].ChargeType" disabled>
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-selected="carriermarkup.ChargeType == option.id">@{{option.name}}</option>
																					</select>
																			  
																			</td>
																		  </tr>
																		 
																	  </tbody></table>
																	</div>
																	
																	<hr>
																
																<div class="client-list" >
																<a id="client-list1" class="client-list" href="javascript:void(0)">
																			<i class="fa fa-users"></i> List of Clients Using This Upcharge</a> 
																
																<div id="cus-drop1" class="client-listdrop rel-position">
																		<div class="cl-header">
																			<h5>List of client using this markup</h5>
																		</div>
																<div class="cl-body">
																			<label>Select Profile to Up Charge:</label>
																			
																				<div class="form-group">
																					<select name="ProfileId" id="ProfileId" ng-model="markup.ProfileId">
																				  <option ng-repeat="option in profilelist" value="@{{option.RateProfileCode}}" >@{{option.RateProfileDescription}}</option>
																				</select>
																					<button type="button" class="btn btn-sm btn-primary" ng-click="addToMapping($index)">Add to Mapping</button>
																				</div>
																				<div class="for-scroll"><ul>
																					<li ng-repeat="profile in markup.MappedProfile"><label><input type="checkbox" checked="checked" ng-click="excludeProfile($parent.$index,$index,profile.ProfileId)">@{{profile.ProfileId}}</label></li>
																					
																				</ul></div>
																			
																		</div>
																</div>
																
																</div>
																
																</div>
															  </div>
														</form>
														</div>
												</div>
												</div>
											
											</div>
					
										</div>
										<div class="tab-content">
											<div class="row">
													<div class="col-md-6 col-sm-6">
													<strong>{{ Lang::get('profile.profile_selected') }}	: @{{contractlist.profile.RateProfileDescription}}</strong>
													</div>
											
											</div>
											
											
											<div class="markup-cont clearfix">
											<div class="col-sm-6 col-sm-offset-3">
												<div class="panel panel-primary">
													<div class="panel-heading">
													  <h3 class="panel-title">Profile Mark up </h3>
													  <ul>
														<li><a href="javascript:void(0)" ng-click="delProfileMarkup()"><i class="fa fa-trash-o"></i></a></li>
														
														<li><a href="javascript:void(0)" ng-click="updateProfileMarkup()"><i class="fa fa-floppy-o"></i></a></li>
													  </ul>
													</div>
											
													<div class="panel-body">
														
														<form role="form" class="form-inline">
															
															<div class="table-responsive">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table mark-table markup-tabtable">
															  <tr class="full-label">
																<td width="25%" align="left" valign="top">&nbsp;</td>
																<td width="25%" align="left" valign="top">
																	  <label>Percentage</label>
																	  <input type="text" class="form-control" id="" placeholder="10%" ng-model="mprofile.TopLevelUpCharge.profileChargePercent" name="profileChargePercent">
																</td>
																<td width="25%" align="left" valign="top">
																	  <label>Min Charge</label>
																	  <input type="text" class="form-control" id="" placeholder="50" ng-model="mprofile.TopLevelUpCharge.profilecarrierMinCharge" name="profilecarrierMinCharge">
																	</td>
																<td width="25%" align="left" valign="top">
																	  <label>Charge Method</label>
																	 <select name="profileTopChargeType" id="UpChargeCode" ng-model="mprofile.TopLevelUpCharge.profileTopChargeType">
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-selected="mprofile.TopLevelUpCharge.profileTopChargeType == option.id">@{{option.name}}</option>
																					</select>
																	</td>
															  </tr>
															
															  <tr ng-repeat="carriermarkup in mprofile.UpchargeByCarrier">
																<td align="left" valign="top">@{{carriermarkup.CarrierName}}</td>
																<td align="left" valign="top"><input type="text" class="form-control" id="" placeholder="10%" ng-model="mprofile.UpchargeByCarrier[$index].ChargePercent" name="profileChargePercent[]"></td>
																<td align="left" valign="top">
																  <input type="text" class="form-control" id="input" placeholder="50" ng-model="mprofile.UpchargeByCarrier[$index].carrierMinCharge" name="profilecarrierMinCharge[]">
															   </td>
																<td align="left" valign="top">
																  <select name="profileChargeType[]" id="" ng-model="mprofile.UpchargeByCarrier[$index].ChargeType">
																					  <option ng-repeat="option in chargemethodtype" value="@{{option.id}}" ng-selected="carriermarkup.ChargeType == option.id">@{{option.name}}</option>
																					</select>
																</td>
															  </tr>
															  
															 
															 
														  </table>
														</div>
														</form>
												
													
													</div>
												 </div> 
											
											</div>
											</div>
											
											
										</div>
				</div>
				</div>



</div>
