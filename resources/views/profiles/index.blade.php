@extends('layouts.user_home')
@section('angular_controller')
    ProfileManagerController  
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop

@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
    
    <link href="{{ asset('bower_components/angular-ui-grid/ui-grid.min.css') }}" rel="stylesheet">
	
	
	
   
@stop
@section('sidebar')
  @include('_partials/profile_management_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
 <section class="content-wrapper">
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12" >
      
        
        
          
				@include('profiles/contract_right')
				
            <!-- end of contact right panel-->
            
            
            
            @include('profiles/fuel_right')
            
           
             <!-- end of fuel right panel-->
            
            
            
            
            
            @include('profiles/accessorial_right')
            
            
            
           
             <!-- end of Accessorial right panel-->
            
            
            
            
            @include('profiles/markup_right')
            
            
            
            
             <!-- end of Markup right panel-->
            
            
            
            
        </div>
      </div>
    </div>
  </section>
 </div>
<!-- /content section-->


<!-- Modal -->
<div class="modal fade blue" id="moreDetails_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Upload Rate(s)</h2>
      </div>
      <div class="modal-body">
        <form>
        	<div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12">
                	<label>Upload Rate(s)</label>
                    <input type="file" class="form-control">
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12 buttons">
                	<button type="button" class="btn btn-default white_button" data-dismiss="modal">Close</button>
                	<button type="button" class="btn btn-primary blue_button">Save</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
 				
<!-- Add/Edit Contract Modal -->
<div class="modal fade blue" id="moreDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Add Contract</h2>
      </div>
      <div class="modal-body">
        <form id="contractform" name="contractform" >
			<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="contract._token='{{ csrf_token() }}'" required>
			<input type="hidden"   name="_id" ng-model="contract._id"  ng-init="contract._id" value="@{{contract._id}}" >
			<input type="hidden"   name="carriercode" ng-model="contract.carriercode"  ng-init="contract.carriercode" value="@{{contract.carriercode}}" >
        	<div class="row">
            	<div class="col-lg-2 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Carrier</label>
                    	<!--<input type="text" class="form-control" autocomplete="off"  placeholder=""  ng-model="contract.carrierId" name="carrierId" ng-change="getcarriers(contract.carrierId)"uib-typeahead="state.name for state in states"  typeahead-on-select="onSelect($item, contract, 'carrier')" required >-->
                    	
                    	<div angucomplete-alt
						  id="countryex5"
						  placeholder="Search Carrier"
						  pause="500"
						  selected-object="selectedCarrier"
						  focus-out="selectedCarrier()"
						  remote-api-handler="searchCarrierAPI"
						  remote-url-request-formatter="remoteUrlRequestFn"
						  remote-url-data-field="items"
						  title-field="name"
						  description-field="description"
						  minlength="2"
						  name="carrierId"
						  input-class="form-control"
						  initial-value="@{{contract.carrierId}}"
						  match-class="highlight" ng-model="contract.carrierId" required>
						</div>
						
                    	
                  	</div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <label>Rate Type</label>
                    <select class="form-control"   name="RateType"  
                    id="RateType"  ng-options="option.name for option 
                    in RateTypeOptions track by option.id" 
                    ng-model="contract.RateType" 
                    ng-disabled="contract.RateType.id.length>0 && 
                    lanes.length!=0" required>

                     
                    </select>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <label>Service Level</label>
                    <select class="form-control" ng-model="contract.ServiceLevel" name="ServiceLevel" required ng-options="option.name for option in ServiceLevelOptions track by option.id">
           
                    </select>
                </div>
                  <div class="col-lg-2 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Distance Application</label>
                    	<select class="form-control" ng-model="contract.DistanceApp" name="DistanceApp" required ng-options="option.name for option in DistanceAppOptions track by option.id">
							
						</select>
                    	
                  	</div>
                </div>
                 
            </div>
            
            <div class="row">
				<div class="col-lg-2">
                    <label>Equipment Type</label>
                    <select class="form-control" name="EquipmentType" ng-model="contract.EquipmentType" ng-options="option.name for option in EQuipTypeOptions track by option.id" required>
                        
                    </select>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                	<div class="form-group ">
                    	<label>Effective From</label>
                    	<input type="text" class="form-control"  placeholder="12/12/2014" id="datepicker5" ng-model="contract.EffectiveFrom" name="EffectiveFrom" required>
                  	</div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Effective To</label>
                    	<input type="text" class="form-control"  placeholder="12/12/2014" id="datepicker6" ng-model="contract.EffectiveTo" name="EffectiveTo" required>
                  	</div>
                </div>
			</div>
            
            
            
            
            <div class="row fak" ng-show="contract.RateType.id=='3'">
            				<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group filter-date">
                                    <label>Tariff Date</label>
                                    <input type="text" placeholder="12/12/2014" id="datepicker4" class="form-control hasDatepicker2"  ng-model="contract.TrafficDate" name="TrafficDate" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group t-name">
                                    <label>Tariff Name</label>
                                    <input type="text" placeholder="" id="datepicker4" class="form-control"  ng-model="contract.TariffName" name="TariffName" >
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row"  ng-show="contract.RateType.id=='3'">
                        <div class="col-lg-12">
                                <label>FAK</label>
                                <div class="row">
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                    <div class="form-group">
										<label for="">50</label>
										<input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK50" name="FAK50">
									</div>
                                 </div>
                                
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">55</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK55" name="FAK55">
                                </div>
                                </div>
                                
                                
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">60</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK60" name="FAK60">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">65</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK65" name="FAK65">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">70</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK70" name="FAK70">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">75</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK75" name="FAK75">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">80</label>
                                    <input type="text" class="form-control" id="" placeholder="" ng-model="contract.FAK80" name="FAK80">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">85</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK85" name="FAK85">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">95</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK95" name="FAK95">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">100</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK100" name="FAK100">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">110</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK110" name="FAK110">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">125</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK125" name="FAK125">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">150</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK150" name="FAK150">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">175</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK175" name="FAK175">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">200</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK200" name="FAK200">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">250</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK250" name="FAK250">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">300</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK300" name="FAK300">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">400</label> 
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK400" name="FAK400">
                                </div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-3">
                                <div class="form-group">
                                    <label for="">500</label>
                                    <input type="text" class="form-control" id="" placeholder=""  ng-model="contract.FAK500" name="FAK500">
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
            <div class="row">
				<div class="col-lg-4 col-md-3 col-sm-6">@{{cmessage}}</div>
            	<div class="col-lg-12 buttons">
                	<button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button  type="submit" ng-click="submitContract(contractform);" class="btn btn-primary" ng-disabled="contractform.$invalid">Save</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div> 
  






<!--Rate Modal -->
<div class="modal fade blue" id="ratemoreDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Add Lane</h2>
      </div>
      <div class="modal-body">
      	<form id="ratelaneform" name="ratelaneform">
			<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="ratelane._token='{{ csrf_token() }}'" required>
			
			<input type="hidden"   id="OriginalCountrycode" name="OriginalCountrycode" ng-model="ratelane.OriginalCountrycode"  ng-init="ratelane.OriginalCountrycode"  value="@{{ratelane.OriginalCountrycode}}">
			<input type="hidden"   id="DestinationCountrycode" name="DestinationCountrycode" ng-model="ratelane.DestinationCountrycode"  ng-init="ratelane.DestinationCountrycode"  value="@{{ratelane.DestinationCountrycode}}">
			
			
			<input type="hidden"   name="ProfileId" ng-model="ratelane.ProfileId"  ng-init="ratelane.ProfileId" value="@{{profile_id}}" >
			<input type="hidden"   name="carrierId" ng-model="ratelane.carrierId"  ng-init="ratelane.carrierId"  value="@{{contract.carriercode}}">
			<input type="hidden"   name="ratebaseId" ng-model="ratelane.ratebaseId"  ng-init="ratelane.ratebaseId"  value="@{{contract.RateType.id}}">
			<input type="hidden"   name="cId" ng-model="ratelane.cId"  ng-init="ratelane.cId"  value="@{{contract._id}}">
			<input type="hidden"   name="_id" ng-model="ratelane._id"  ng-init="ratelane._id" value="@{{ratelane._id}}" >
			
			
			
			
			
        <div class="prof-tbl">
   
        <h3 class="seperater">Rate Service Area</h3>
            <div class="row">
                <div class="col-lg-3">
                    <label>Original Country</label>
                 
                   
                    <!--<input type="text" class="form-control" autocomplete="off"  placeholder=""  ng-model="ratelane.OriginalCountry" name="OriginalCountry" ng-change="getCountry(ratelane.OriginalCountry)" uib-typeahead="country.name for country in countrylist"  typeahead-on-select="onSelect($item, ratelane, 'orgcountry')" typeahead-min-length="3" required>
                    -->
                    <div angucomplete-alt
						  id="countryex5"
						  placeholder="Search country"
						  pause="500"
						  selected-object="selectedCountryOrg"
						  focus-out="selectedCountryOrg()"
						  remote-api-handler="searchCountryAPI"
						  remote-url-request-formatter="remoteUrlRequestFn"
						  remote-url-data-field="items"
						  title-field="name"
						  description-field="description"
						  minlength="2"
						  name="OriginalCountry"
						  input-class="form-control"
						  initial-value="@{{ratelane.OriginalCountry}}"
						  match-class="highlight" ng-model="ratelane.OriginalCountry" required>
						</div>
						
                   <!--<div angucomplete-alt
						  id="ex5"
						  placeholder="Search carrier"
						  pause="500"
						  selected-object="country"
						  focus-out="focusOut()"
						  remote-api-handler="searchAPI"
						  remote-url-request-formatter="remoteUrlRequestFn"
						  remote-url-data-field="items"
						  title-field="name"
						  description-field="description"
						  minlength="2"
						  name="OriginalCountry"
						  input-class="form-control"
						  match-class="highlight" ng-model="ratelane.OriginalCountry" required>
						</div>-->
                    
                </div>
                <div class="col-lg-3">
                    <label>Destination Country</label>
                 
                   <!-- <input type="text" class="form-control" autocomplete="off"  placeholder=""  ng-model="ratelane.DestinationCountry" name="DestinationCountry" ng-change="getCountry(ratelane.DestinationCountry)" uib-typeahead="country.name for country in countrylist"  typeahead-on-select="onSelect($item, ratelane, 'destcountry')" typeahead-min-length="3" required>
                    -->
                    
                    <div angucomplete-alt
						  id="countryex5"
						  placeholder="Search country"
						  pause="500"
						  selected-object="selectedCountryDest"
						  focus-out="selectedCountryDest()"
						  remote-api-handler="searchCountryAPI"
						  remote-url-request-formatter="remoteUrlRequestFn"
						  remote-url-data-field="items"
						  title-field="name"
						  description-field="description"
						  minlength="2"
						  name="DestinationCountry"
						  input-class="form-control"
						  initial-value="@{{ratelane.DestinationCountry}}"
						  match-class="highlight" ng-model="ratelane.DestinationCountry" required>
						</div>
                    
                </div>
                    <div class="col-lg-3 radio-cont">
                        <label>Lane Direction</label><br/>
                    <label class="radio-inline">
                        <input type="radio" name="MoveType" ng-model="ratelane.MoveType" value="Direct" ng-required="!MoveType"/>Direct
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="MoveType" ng-model="ratelane.MoveType" value="Indirect" ng-required="!MoveType"/>Indirect
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="MoveType" ng-model="ratelane.MoveType" value="All" ng-required="!MoveType"/>All
                    </label>
                </div>
               
            </div>
            <div class="row form-group">
                <div class="col-lg-3">
                    <label>Original Location</label>
                    <select class="form-control" name="OriginalLocation" id="OriginalLocation" ng-model="ratelane.OriginalLocation" ng-options="option.name for option in LocTypeOptions track by option.id" ng-change="resetorgloc()" required>
                       
                    </select>
                </div>
                <div class="col-lg-3 selectContainer">
                    <label>Destination Location</label>
                    <select class="form-control" name="DestinationLocation" id="DestinationLocation"  ng-model="ratelane.DestinationLocation" ng-options="option.name for option in LocTypeOptions track by option.id" ng-change="resetdestloc()" required>
                        
                    </select>
                </div>
                <div class="col-lg-3">
                    <div class="radio-cont m-top2">
						<label class="radio-inline ">
								<input type="radio" name="RateBetween" ng-model="ratelane.RateBetween" value="Interstate" ng-required="!MoveType">Interstate
							</label>
							<label class="radio-inline">
								<input type="radio" name="RateBetween" ng-model="ratelane.RateBetween" value="Intrastate" ng-required="!MoveType">Intrastate
							</label>
							<label class="radio-inline">
								<input type="radio" name="RateBetween" ng-model="ratelane.RateBetween"  value="All" ng-required="!MoveType">All
							</label>
					</div>
                    

                </div>
            </div>
           
            <div class="row col-lg-12 customadd-logic" >
                
                
                
                
                 <!--source area-->
                 
                 
                <div class="col-lg-3 rate-auto" style="padding-left:0" >
					
					
					
					<!--
					<div class="row pull-left">
             
						<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='8' || ratelane.OriginalLocation.id=='1'">
							<div  ng-class="{row:ratelane.OriginalLocation.id=='1'}">
							
								<div ng-class="{'col-sm-6':ratelane.OriginalLocation.id=='1'}">
								<div id="orgstatecontainer" orgstate>
										<select id="orgstate" name="ratelane.OriginalState" multiple="multiple"  ng-model="ratelane.OriginalState"></select>
										</div>
								</div>
								
								<div ng-class="{'col-sm-6':ratelane.OriginalLocation.id=='1'}" ng-show="ratelane.OriginalLocation.id=='1'">
									<div id="orgcitycontainer" originalcity>
									<select id="orgcity" name="ratelane.OriginalCity" multiple="multiple"  ng-model="ratelane.OriginalCity" ></select>
									</div>
								</div>
							</div>
						</div>
					
					
						<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='6'">
						
								<div id="orgzipcontainer" orgzipcode>
									<select id="orgzip" name="ratelane.OriginalZipcode" multiple="multiple"  ng-model="ratelane.OriginalZipcode" ></select>
									</div>
					
						
						</div>
						
						
						<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='3'">
						<div   class="row">
						
							<div class="col-sm-6" >
							
							</div>
							
							<div class="col-sm-6" >
							
							</div>
						</div>	
					</div>
					
					
							
							
					</div>-->
					
                <div class="row pull-left" data-ng-repeat="choice in choices">
             
					<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='8' || ratelane.OriginalLocation.id=='1'">
						<div  ng-class="{row:ratelane.OriginalLocation.id=='1'}">
						
							<div ng-class="{'col-sm-6':ratelane.OriginalLocation.id=='1'}">
							
							
							<input type="text" class="form-control" autocomplete="off"  placeholder="State" name="OriginalState[]"  ng-model="ratelane.OriginalState[$index]" ng-change="getStates(ratelane.OriginalState[$index],ratelane.OriginalCountrycode)" uib-typeahead="state.name for state in statelist" typeahead-on-select="onSelect($item, ratelane, 'orgstate')" typeahead-min-length="2"  typeahead-wait-ms="2000">
							
					
							
							</div>
							
							<div ng-class="{'col-sm-6':ratelane.OriginalLocation.id=='1'}">
							<input type="text" class="form-control" id="" autocomplete="off" placeholder="City" name="OriginalCity[]" ng-model="ratelane.OriginalCity[$index]" ng-change="getCity(ratelane.OriginalCity[$index],ratelane.OriginalCountrycode,ratelane.OriginalState[$index])" uib-typeahead="city.name for city in citylist"  typeahead-on-select="onSelect($item, ratelane, 'orgcity')"  ng-show="ratelane.OriginalLocation.id=='1'" typeahead-min-length="3"  typeahead-wait-ms="2000">
							</div>
						</div>	
					</div>
					

				
				
				
				
					<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='6'">
						<input type="text" class="form-control" id="" autocomplete="off" placeholder="ZipCode" name="OriginalZipcode[]" ng-model="ratelane.OriginalZipcode[$index]" ng-change="getZipcode(ratelane.OriginalZipcode[$index],ratelane.OriginalCountrycode,ratelane.OrgStatecode,ratelane.OrgCity)" uib-typeahead="zip.name for zip in ziplist" typeahead-on-select="onSelect($item, ratelane.OriginalZipcode[$index], 'orgzipcode')" typeahead-min-length="3"  typeahead-wait-ms="1000">
						
						<!--<div angucomplete-alt
						  id="zipcodeex5"
						  placeholder="Search zipcode"
						  pause="500"
						  selected-object="selectedZipcode"
						   focus-out="orglostFocusZipcode($index)"
						  remote-url="/front/services/postal-list"
						  remote-url-request-formatter="remoteUrlOrgZipRequestFn"
						  remote-url-data-field="items"
						  title-field="name"
						  description-field="description"
						  minlength="2"
						  input-name="OriginalZipcode[]"
						  name="OriginalZipcode[]"
						  input-class="form-control"
						  initial-value="@{{ratelane.OriginalZipcode[$index]}}"
						  match-class="highlight" ng-model="ratelane.OriginalZipcode[$index]">
						</div>-->
						
					</div>
					
					
					
					
					
					
					<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='4'">
						<input type="text" class="form-control" id="" autocomplete="off" placeholder="ZipCode" name="OriginalTripleZipcode[]" ng-model="ratelane.OriginalTripleZipcode[$index]" ng-change="getZipcode(ratelane.OriginalTripleZipcode[$index],ratelane.OriginalCountrycode,ratelane.OrgStatecode,ratelane.OrgCity)" uib-typeahead="zip.name for zip in ziplist" typeahead-on-select="onSelect($item, ratelane.OriginalTripleZipcode[$index], 'orgtriplezipcode')" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
						
					</div>
				
				<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='5'">
						<div   class="row">
						
							<div class="col-sm-6" >
							<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode From" name="OriginalTripleZipcodeFrom[]" ng-model="ratelane.OriginalTripleZipcodeFrom[$index]" ng-change="getZipcode(ratelane.OriginalTripleZipcodeFrom[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
							</div>
							
							<div class="col-sm-6" >
							<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode To" name="OriginalTripleZipcodeTo[]" ng-model="ratelane.OriginalTripleZipcodeTo[$index]" ng-change="getZipcode(ratelane.OriginalTripleZipcodeTo[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
							</div>
						</div>	
					</div>
				
					
					
					<div class="form-group col-sm-10" ng-show="ratelane.OriginalLocation.id=='3'">
						<div   class="row">
						
							<div class="col-sm-6" >
							<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode From" name="OriginalZipcodeFrom[]" ng-model="ratelane.OriginalZipcodeFrom[$index]" ng-change="getZipcode(ratelane.OriginalZipcodeFrom[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="3"  typeahead-wait-ms="1000">
							</div>
							
							<div class="col-sm-6" >
							<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode To" name="OriginalZipcodeTo[]" ng-model="ratelane.OriginalZipcodeTo[$index]" ng-change="getZipcode(ratelane.OriginalZipcodeTo[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="3"  typeahead-wait-ms="1000">
							</div>
						</div>	
					</div>
				
					<div class="col-sm-2">
						
						
								<button class="btn btn-xs btn-danger" ng-show="$last" ng-click="removeChoice()"><span class="glyphicon glyphicon-minus"></span></button>
					</div>
				
				
                    
                   
              
                </div>
                
                <button type="button" class="addfields btn btn-xs btn-primary" ng-click="addNewChoice()"><span class="glyphicon glyphicon-plus"></span>Add</button>
                </div>
                
                
                
                
                
                
                <!--destination area-->
                
                
                
                <div  class="col-lg-3 ">
                <div  class="row pull-left" data-ng-repeat="choice in choices2">
                
                
								<div class="form-group col-sm-10" ng-show="ratelane.DestinationLocation.id=='8' || ratelane.DestinationLocation.id=='1'">
									<div  ng-class="{row:ratelane.DestinationLocation.id=='1'}">
									
										<div ng-class="{'col-sm-6':ratelane.DestinationLocation.id=='1'}">
										<input type="text" class="form-control" id="" autocomplete="off" placeholder="State" name="DestinationState[]" ng-model="ratelane.DestinationState[$index]" ng-change="getStates(ratelane.DestinationState[$index],ratelane.DestinationCountrycode)" uib-typeahead="state.name for state in statelist" typeahead-on-select="onSelect($item, ratelane.DestinationState[$index], 'deststate')" typeahead-min-length="2"  typeahead-wait-ms="2000">
										</div>
										
										<div ng-class="{'col-sm-6':ratelane.DestinationLocation.id=='1'}">
										<input type="text" class="form-control" id="" autocomplete="off" placeholder="City" name="DestinationCity[]" ng-model="ratelane.DestinationCity[$index]" ng-show="ratelane.DestinationLocation.id=='1'" ng-change="getCity(ratelane.DestinationCity[$index],ratelane.DestinationCountrycode,ratelane.DestinationState[$index])" uib-typeahead="city.name for city in citylist"  typeahead-on-select="onSelect($item, ratelane.DestinationCity[$index],'destcity')" typeahead-min-length="2"  typeahead-wait-ms="2000">
										</div>
									</div>	
								</div>
								
							
							
								<div class="form-group col-sm-10" ng-show="ratelane.DestinationLocation.id=='6'">
									<input type="text" class="form-control" id="" autocomplete="off" placeholder="ZipCode" name="DestinationZipcode[]" ng-model="ratelane.DestinationZipcode[$index]" ng-change="getZipcode(ratelane.DestinationZipcode[$index],ratelane.DestinationCountrycode,ratelane.DestStatecode,ratelane.DestCity)" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="3"  typeahead-wait-ms="1000">
								</div>
								
								
								<div class="form-group col-sm-10" ng-show="ratelane.DestinationLocation.id=='4'">
										<input type="text" class="form-control" id="" autocomplete="off" placeholder="ZipCode" name="DestinationTripleZipcode[]" ng-model="ratelane.DestinationTripleZipcode[$index]" ng-change="getZipcode(ratelane.DestinationTripleZipcode[$index],ratelane.DestinationCountrycode,ratelane.DestStatecode,ratelane.DestCity)" uib-typeahead="zip.name for zip in ziplist" typeahead-on-select="onSelect($item, ratelane.DestinationTripleZipcode[$index], 'desttriplezipcode')" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
										
									</div>
								
								<div class="form-group col-sm-10" ng-show="ratelane.DestinationLocation.id=='5'">
										<div   class="row">
										
											<div class="col-sm-6" >
											<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode From" name="DestinationTripleZipcodeFrom[]" ng-model="ratelane.DestinationTripleZipcodeFrom[$index]" ng-change="getZipcode(ratelane.DestinationTripleZipcodeFrom[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
											</div>
											
											<div class="col-sm-6" >
											<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode To" name="DestinationTripleZipcodeTo[]" ng-model="ratelane.DestinationTripleZipcodeTo[$index]" ng-change="getZipcode(ratelane.DestinationTripleZipcodeTo[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="2"  typeahead-wait-ms="1000" maxlength="3">
											</div>
										</div>	
									</div>
					
					
					
							
							
								<div class="form-group col-sm-10" ng-show="ratelane.DestinationLocation.id=='3'">
									<div   class="row">
									
										<div class="col-sm-6" >
										<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode From" name="DestinationZipcodeFrom[]" ng-model="ratelane.DestinationZipcodeFrom[$index]" ng-change="getZipcode(ratelane.DestinationZipcodeFrom[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="3"  typeahead-wait-ms="1000">
										</div>
										
										<div class="col-sm-6" >
										<input type="text" class="form-control col-sm-6" autocomplete="off" id="" placeholder="ZipCode To" name="DestinationZipcodeTo[]" ng-model="ratelane.DestinationZipcodeTo[$index]" ng-change="getZipcode(ratelane.DestinationZipcodeTo[$index])" uib-typeahead="zip.name for zip in ziplist" typeahead-min-length="3"  typeahead-wait-ms="1000">
										</div>
									</div>	
								</div>
								
								<div class="col-sm-2" >
											<button class="btn btn-xs btn-danger" ng-show="$last" ng-click="removeChoice2()"><span class="glyphicon glyphicon-minus"></span></button>
								</div>
					
					
						
			
               
               </div>
               <button type="button" class="addfields btn btn-xs btn-primary" ng-click="addNewChoice2()"><span class="glyphicon glyphicon-plus"></span>Add</button>
               </div>
               
                
               
                 <div style="height: 32px"><div class="orgloading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
                
            </div>


            <h3 class="seperater laneroute">Routes </h3>
            <div class="row range lanerange">
                <div class="col-lg-4">
                    <label class="lb-block">Distance Range</label>
                    <div class="form-group first">
                        <input type="number" min="0" class="form-control" id="" placeholder="From" name="DistanceFrom" ng-model="ratelane.DistanceFrom" >
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" class="form-control" id="" placeholder="To" name="DistanceTo" ng-model="ratelane.DistanceTo" >
                    </div>
                    <div class="form-group last">
                        <select class="form-control" ng-model="ratelane.DistanceType" ng-options="option.name for option in DistanceTypeOptions track by option.id" >
                           
                        </select>
                    </div>
                </div>
                <div class="col-lg-8 radio-cont m-top2">
                    <label class="checkbox-inline">
                        <input id="inlineRadio1" type="checkbox"  ng-true-value="'Ignore Carrier Connect'" ng-false-value="'None'" value="Ignore Carrier Connect" name="ICarrier" ng-model="ratelane.ICarrier" >Ignore Carrier Connect
                    </label>
                    <label class="checkbox-inline">
                       <input  id="inlineRadio2" type="checkbox"  ng-true-value="'Use Routing Guide'" ng-false-value="'None'" value="Use Routing Guide" name="RGuide" ng-model="ratelane.RGuide">Use Routing Guide
                    </label>
                </div>
            </div>
            
            <h3 class="seperater laneroute">Capacity Range</h3>
            <div class="row range lanerange">
                <div class="col-lg-4">
                    <label class="lb-block">Weight Range</label>
                    <div class="form-group first">
                        <input type="number" min="0" class="form-control" id="" placeholder="From" name="FromWeightRange" ng-model="ratelane.FromWeightRange" >
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" class="form-control" id="" placeholder="To" name="ToWeightRange" ng-model="ratelane.ToWeightRange" >
                    </div>
                    <div class="form-group last">
                        <select class="form-control"  name="CapacityWeightType" ng-model="ratelane.CapacityWeightType" ng-options="option.name for option in WeightTypeOptions track by option.id" >
                            
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label class="lb-block">Package Weight</label>
                    <div class="form-group first">
                        <input type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  class="form-control" id="" placeholder="From"  name="FromPackageWeight" ng-model="ratelane.FromPackageWeight" >
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  class="form-control" id="" placeholder="To"  name="ToPackageWeight" ng-model="ratelane.ToPackageWeight" >
                    </div>
                    <div class="form-group last">
                        <select class="form-control"  name="PackageWeightType" ng-model="ratelane.PackageWeightType" ng-options="option.name for option in WeightTypeOptions track by option.id" >
                            
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label class="lb-block">Volume Range</label>
                    <div class="form-group first">
                        <input type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" class="form-control" id="" placeholder="From" name="FromVolumeRange" ng-model="ratelane.FromVolumeRange" >
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" class="form-control" id="" placeholder="To"  name="ToVolumeRange" ng-model="ratelane.ToVolumeRange" >
                    </div>
                    <div class="form-group last">
                        <select class="form-control" name="VolumeWeightType" ng-model="ratelane.VolumeWeightType" ng-options="option.name for option in WeightTypeOptions track by option.id" >
                           
                        </select>
                    </div>
                </div>
            
            </div>
            
            <div class="tabbable boxed parentTabs">
                <ul class="nav nav-tabs responsive">
                    <li ng-class="{active: contract.RateType.id=='3'}" ng-show="contract.RateType.id=='3'"><a href="#set1">@{{contract.RateType.name}}</a></li>
                    <li ng-class="{active: contract.RateType.id=='2'}" ng-show="contract.RateType.id=='2'"><a href="#set2" >@{{contract.RateType.name}}</a></li>
                    <li ng-class="{active: contract.RateType.id=='4'}" ng-show="contract.RateType.id=='4'"><a href="#set3">@{{contract.RateType.name}}</a></li>
                    <!--<li ng-class="{active: contract.RateType.name=='Cost Per Unit'}"  ng-show="contract.RateType.name=='Cost Per Unit'"><a href="#set4">Cost Per Unit</a></li>-->
                    <li ng-class="{active: contract.RateType.id=='6'}" ng-show="contract.RateType.id=='6'"><a href="#set5">@{{contract.RateType.name}}</a></li>
                    <li ng-class="{active: contract.RateType.id=='8'}" ng-show="contract.RateType.id=='8'"><a href="#set6">@{{contract.RateType.name}}</a></li>
                </ul>
                <div class="tab-content responsive">
                    <div class="tab-pane fade in" ng-class="{active: contract.RateType.id=='3'}" id="set1">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Inbound Discount</label>
                                    <input class="form-control" type="number" min="0"   ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" id="" placeholder="0.00" name="Rate" ng-model="ratelane.Rateware.Rate">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Outbound Discount</label>
                                    <input class="form-control" type="number" min="0" id="" placeholder="0" name="OutboundDisc" ng-model="ratelane.Rateware.OutboundDisc">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Third Pary Discount</label>
                                    <input class="form-control" type="number" min="0" id="" placeholder="0" name="ThirdpartyDisc" ng-model="ratelane.Rateware.ThirdpartyDisc">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Absolute Minimum Charge</label>
                                    <input class="form-control" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" id="" placeholder="0.00" name="MinCharge" ng-model="ratelane.Rateware.MinCharge" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in" ng-class="{active: contract.RateType.id=='2'}" id="set2">
                        <div class="row" >
                            <div class="row">
								<div class="col-lg-2"><label>Rate Per</label></div>
								<div class="col-lg-2"><label>Outbound Discount</label></div>
								<div class="col-lg-2"><label>Minimum Charge</label></div>
								<div class="col-lg-2"><label>Stop</label></div>
								
                            </div>
                            
                           <div class="row" data-ng-repeat="choice in multidistance" >
                            <div class="col-lg-2">
                                <div class="form-group">
                                    
                                    <input type="number" class="form-control" id="" placeholder="0.00"  min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Rate" ng-model="ratelane.DistanceRate.Rate[$index]">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    
                                    <select class="form-control" name="OutDisc" ng-model="ratelane.DistanceRate.OutDisc[$index]" ng-options="option.name for option in DistanceTypeOptions track by option.id">
                                       
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    
                                    <input class="form-control" type="number" min="0" id="" placeholder="0.00" name="MinCharge" ng-model="ratelane.DistanceRate.MinCharge[$index]"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="" placeholder="" name="Stop" ng-model="ratelane.DistanceRate.Stop[$index]">
                                </div>
                            </div>
                            
                            
                            <div class="col-lg-2">
                                <div class="btn-group-new m-top2">
                                    <button class="btn btn-xs btn-danger" ng-show="$last" ng-click="removeDistance()"><span class="glyphicon glyphicon-minus"></span></button>
                                </div>
                            </div>
                            </div>
                            
                            
                            <div class="col-lg-12">
                                <div class="btn-group-new m-top2">
                                    <button type="button" class="btn btn-primary btn-m"  ng-click="addNewDistance()">Add +</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade in" ng-class="{active: contract.RateType.id=='4'}" id="set3">
                        <div class="row">
                            <div class="row">
								<div class="col-lg-1"><label>Unit Type</label></div>
								<div class="col-lg-1"><label>Maximum Weight</label></div>
								<div class="col-lg-1"><label>Maximum Length</label></div>
								<div class="col-lg-1"> <label>Maximum Height</label></div>
								<div class="col-lg-1"><label>Maximum Width</label></div>
								<div class="col-lg-1"><label>From Unit</label></div>
								<div class="col-lg-1"><label>To Unit</label></div>
								<div class="col-lg-1"><label>Cost</label></div>
								<div class="col-lg-1"><label>Minimum Charge</label></div>
								
                            </div>
                            
                            
                           <div class="row" data-ng-repeat="choice in multicostperunit" > 
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <select class="form-control" name="UnitType" ng-model="ratelane.CPU.UnitType[$index]" ng-options="option.name for option in UnitTypeOptions track by option.id">
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="MaxWeight" ng-model="ratelane.CPU.MaxWeight[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="MinWeight" ng-model="ratelane.CPU.MinWeight[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                   
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="MaxHeight" ng-model="ratelane.CPU.MaxHeight[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="MaxWidth" ng-model="ratelane.CPU.MaxWidth[$index]">
                                </div>
                            </div>
                       
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="FromUnit" ng-model="ratelane.CPU.FromUnit[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0" name="ToUnit" ng-model="ratelane.CPU.ToUnit[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    
                                    <input class="form-control" id="" placeholder="" type="number" min="0"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Rate" ng-model="ratelane.CPU.Rate[$index]">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
									 
                                    <input class="form-control" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" id="" placeholder="0.00" name="MinCharge" ng-model="ratelane.CPU.MinCharge[$index]">
                                </div>
                            </div>
                            
                            <div class="col-lg-1">
                                <div class="btn-group-new m-top2">
                                    <button class="btn btn-xs btn-danger" ng-show="$last" ng-click="removeCostperUnit()"><span class="glyphicon glyphicon-minus"></span></button>
                                </div>
                            </div>
                            
                            </div>
                            
                            <div class="col-lg-12">
                                <div class="btn-group-new m-top2">
                                    <button type="button" class="btn btn-primary btn-m"  ng-click="addNewCostperUnit()">Add More+</button>
                                </div>
                            </div>
                            <!--<div class="col-lg-1">
                                <div class="btn-group-new m-top2">
                                    <button type="button" class="btn btn-success btn-xs add-contract-btn">Add Lane +</button>
                                </div>
                            </div>-->
                        </div>
                    </div>
                   
                    <div class="tab-pane fade in"  ng-class="{active: contract.RateType.id=='6'}" id="set5">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Class</label>
                                    <input type="text" class="form-control" id="" placeholder="" name="Class" ng-model="ratelane.RateMatrix.Class">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Minimum</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" name="MinCharge" ng-model="ratelane.RateMatrix.MinCharge">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Weight From</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" name="WeightFrom" ng-model="ratelane.RateMatrix.WeightFrom">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Weight To</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" name="WeightTo" ng-model="ratelane.RateMatrix.WeightTo">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Rate</label>
                                    <input class="form-control" id="" placeholder="0.00"  type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" name="Rate" ng-model="ratelane.RateMatrix.Rate">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="tab-pane fade in" ng-class="{active: contract.RateType.id=='8'}" id="set6">
                        <div class="row">
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Min. Charge</label>
                                    <input  class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="MinCharge" ng-model="ratelane.CostPerHundredRate.MinCharge">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Weight < 500</label>
                                    <input  class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Rate" ng-model="ratelane.CostPerHundredRate.Rate">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Weight < 1000</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Weight1000" ng-model="ratelane.CostPerHundredRate.Weight1000">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Weight < 2000 </label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Weight2000" ng-model="ratelane.CostPerHundredRate.Weight2000">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Weight <5000</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Weight5000" ng-model="ratelane.CostPerHundredRate.Weight5000">
                                </div>
                            </div>
                               <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Weight < 10000 </label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Weight10000" ng-model="ratelane.CostPerHundredRate.Weight10000">
                                </div>
                            </div>
                               <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Weight < 12000 </label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="Weight12000" ng-model="ratelane.CostPerHundredRate.Weight12000">
                                </div>
                            </div>
                               <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Weight > 12000</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="WeightG12000" ng-model="ratelane.CostPerHundredRate.WeightG12000">
                                </div>
                            </div>
                               <div class="col-lg-1">
                                <div class="form-group">
                                    <label>TL Rate</label>
                                    <input class="form-control" id="" placeholder="0.00" type="number" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"  name="TLRate" ng-model="ratelane.CostPerHundredRate.TLRate">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        
         <div class="row">
				  
				  
            	<div class="col-lg-12 buttons pull-right">
                	<button type="reset" class="btn btn-default" ng-click="resetlane()" data-dismiss="modal">Close</button>
                	<button  type="submit" ng-click="submitRatelane(ratelaneform);" class="btn btn-primary" ng-disabled="ratelaneform.$invalid">Save</button>
                </div>
            </div>
        </div>
       
        </form>
	</div>
 </div>
</div>








 <!-- Markup Modal -->
<div class="modal fade blue" id="markupDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Add Markup</h2>
      </div>
      <div class="modal-body">
        <form id="createmarkup" name="createmarkup">
			<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="markupdata._token='{{ csrf_token() }}'" required>
			
        	<div class="row">
            	
                <div class="col-lg-4 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Markup Name</label>
                    	<input type="text" class="form-control" placeholder="" name="MarkupName" ng-model="markupdata.MarkupName" required>
                  	</div>
                </div>
               
               <div class="col-lg-4 col-md-3 col-sm-6">@{{markupmessage}}</div>
            </div>
            <div class="row">
            	<div class="col-lg-12 buttons">
                	<div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                	<button type="submit" ng-click="addMarkup(markupdata);" class="btn btn-primary" ng-disabled="createmarkup.$invalid">Save</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!--RateMatrix Modal -->
<div class="modal fade blue" id="ratematrixDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Add Rate</h2>
      </div>
      <div class="modal-body" >
		  
		  
		  <div class="loading hidden lodingStyle"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
		    <form id="ratematrixfrm" name="ratematrixfrm" ng-if="gridOptions.data.length==0">
		   <input type="hidden"   name="_token" value="@{{ csrf_token() }}"  ng-init="ratematrix._token='{{ csrf_token() }}'" required>
		   <div class="row">
			   <div class="col-lg-12 clearfix">
				   <div class="col-lg-6 clearfix">
						<div class="form-group">
							<label>Existing Matrix</label>
							<div class="clearfix"></div>
							<div class="col-lg-10 col-md-10 clearfix" >
							<select class="form-control pull-left" ng-change="hidenewrate()" ng-model="ratematrix.matrix" name="matrix" ng-options="option.name for option in MatrixOptions track by option.id">
								
							</select>
							
							<!--<div class="typeaheadcontainer"></div>		-->
							</div>
							<div class="col-lg-2 col-md-2 clearfix"><div class="alignmentActionBtn"><a href="javascript:void(0)" ng-click="deleteRatematrix(ratematrix.matrix);" ng-show="ratematrix.matrix.id"><i class="fa fa-trash-o"></i></a></div></div>
						</div>
					</div>
				   <div class="col-lg-6 buttons clearfix">
					   <button type="button" class="btn btn-primary" ng-click="matrixaddnew()" ng-show="!ratematrix.matrix.id">New Matrix</button>
					   
					   <button type="button" class="btn btn-primary" ng-click="loadGridData();" ng-show="ratematrix.matrix.id" >Load Matrix Grid</button>
					   
					</div>
				   
				</div>
				   
			</div>
			<div ng-show="rateaddnew==true">
			<div class="row">
				<div class="col-lg-12">
					<div class="col-lg-6">
						
						<div class="form-group">
							<label>Matrix Name </label>
							<input type="text" class="form-control" placeholder="" name="ratematrix.matrix_name" ng-model="ratematrix.matrix_name" required>
                    	
						</div>
						<div class="col-lg-8">
							
							<div class="form-group">
								<label>Origin Location </label>
								<select class="form-control" name="OriginalLocation" id="OriginalLocation" ng-model="ratematrix.OriginalLocation" ng-options="option.label for option in mLocTypeOptions track by option.id" required>  </select>
							
							</div>
							
							<div class="form-group">
								<label>Destination Location </label>
								<select class="form-control" name="DestinationLocation" id="DestinationLocation" ng-model="ratematrix.DestinationLocation" ng-options="option.label for option in mLocTypeOptions track by option.id" required>  </select>
							
							</div>
							
							
							
							<div class="form-group">
								<label>Column Heading </label>
								
							<select class="form-control" name="colhead" id="colhead" ng-model="ratematrix.colhead" ng-options="option.label for option in ColHeadingOptions track by option.id" >  </select>
							</div>
						</div>
						<div class="col-lg-4"><button type="button" class="btn btn-default pull-right" style="margin-top:20px;" ng-click="populateHead()" > >> </button> <button type="button" class="btn btn-default pull-right" style="margin-top:20px;" ng-click="deleteHead()" > << </button></div>
						
					</div>	
					<div class="col-lg-6">
						<div class="form-group">Populated Heading </div>
						<div class="form-group">
							<label></label>
						<select name="selectedcolhead" ng-model="ratematrix.selectedcolhead" ng-options="option.name for option in Matrixheads track by option.id" multiple style="width:200px;"></select>
						</div>
						
					</div>
				</div>
				
				
			</div>
			
			
			<div class="row">
            	<div class="col-lg-12 buttons">
                	<div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
			<button type="button" class="btn btn-default" ng-click="matrixclosenew()">Close</button>
                	<button type="submit" ng-click="addMatrix(ratematrix);" class="btn btn-primary" ng-disabled="ratematrixfrm.$invalid">Save</button>
                </div>
            </div>
		   </div>
		    </form>
		   
		   
		   
		   <form method="post" name="ratelanematrixfrm" id="ratelanematrixfrm">
			
			
			<input type="hidden"  id="_token" name="_token" value="{{csrf_token()}}"  ng-init="ratelanematrix._token='{{ csrf_token() }}'" required>
			
		    <div class="row clearfix" ng-show="gridOptions.data.length>0">
			   <div class="col-lg-12 col-md-12 clearfix">
				   <div class="row">
					   <div class="form-group clearfix">
						<div class="col-lg-3 radio-cont">
									
											<label>Lane Direction</label><br/>
										<label class="radio-inline">
											<input type="radio" name="MoveType" ng-model="ratelanematrix.MoveType" value="Direct" ng-required="!MoveType"/>Direct
										</label>
										<label class="radio-inline">
											<input type="radio" name="MoveType" ng-model="ratelanematrix.MoveType" value="Indirect" ng-required="!MoveType"/>Indirect
										</label>
										<label class="radio-inline">
											<input type="radio" name="MoveType" ng-model="ratelanematrix.MoveType" value="All" ng-required="!MoveType"/>All
										</label>
									
						</div>
						<div class="col-lg-4">
							<label>Interstate Options</label><br/>
										<label class="radio-inline">
											<input type="radio" name="Interstate" ng-model="ratelanematrix.InterstateOption" value="Intrastate" ng-required="!Interstate"/>Intrastate
										</label>
										<label class="radio-inline">
											<input type="radio" name="Interstate" ng-model="ratelanematrix.InterstateOption" value="Interstate" ng-required="!Interstate"/>Interstate
										</label>
										<label class="radio-inline">
											<input type="radio" name="Interstate" ng-model="ratelanematrix.InterstateOption" value="All" ng-required="!Interstate"/>All
										</label>
						</div>
						<div class="col-lg-5 radio-cont">
							<label>&nbsp;</label><br/>
										<label class="radio-inline">
											<input type="checkbox" name="IgnoreCarrierConnect" ng-true-value="'Ignore Carrier Connect'" ng-false-value="'None'"  ng-model="ratelanematrix.IgnoreCarrierConnect" value="1" />Ignore Carrier Connect
										</label>
										<label class="radio-inline">
											<input type="checkbox" name="UseRoutingGuide" ng-true-value="'Use Routing Guide'" ng-false-value="'None'"  ng-model="ratelanematrix.UseRoutingGuide" value="1" />Use Routing Guide
										</label>
										
						</div>
				   </div>
				   </div>
				  <div class="row"> 
				   <div ng-if="gridOptions.data.length>0">
					   <div class="clearfix"  ng-if="getLength(gridselection)>0"> 
							   <div class=" clearfix">
								   <div class="customRow row clearfix">
									   <div class=" form-group clearfix adMar">
											<div class="col-lg-1 col-md-1 padding5 first35">&nbsp;</div>
											<div class="col-lg-2 col-md-2 padding5" country>
											<input type="hidden" id="origincountrycode" ng-model="ratelanematrix.OriginCountryCode"  />
											<input type="hidden" id="destinationcountrycode" ng-model="ratelanematrix.DestinationCountryCode"  />
											
											 <input type="text" class="country sourcecountry" id="origincountry" ng-model="ratelanematrix.OriginCountry" placeholder="Select Origin Country" required/>
											
											 </div>
											 
											 <div class="col-lg-2 col-md-2 padding5" country>
											
											 <input type="text" class="country destcountry" id="destcountry" ng-model="ratelanematrix.DestinationCountry"  placeholder="Select Destination Country" required/>
											
											 </div>
											 
											<div class="col-lg-7 col-md-7 padding5">
											   
												   <button type="button" ng-click="AddRow();" class="btn btn-primary pull-right">Add Row</button>
												
											</div>
										</div>
									</div>
							   </div>
					   </div>
					   
					   <div class="customRow row clearfix">
						   <div class="col-lg-1 col-md-1 padding5 first35">&nbsp;</div>
							 
							 <div ng-repeat="(key, value) in gridselection">
								
								 <div  ng-if="($index==0 ) && key=='OriginPostalCode' ">
								 
								 <div class="col-lg-2 col-md-2 padding5" zipauto>
									
									 <input type="text" ng-disabled="!ratelanematrix.OriginCountryCode?true:false"   ng-model="gridselection[key]"  class="sourcepost" ng-blur="handleBlurEvent(gridselection,key,'.sourcepost')"  />
									
									 </div>
								
								</div>
								<div  ng-if="($index==1) && key=='DestinationPostalCode'">
								 
								 <div class="col-lg-2 col-md-2 padding5" zipauto>
									
									 <input type="text"  ng-disabled="!ratelanematrix.DestinationCountryCode?true:false" ng-model="gridselection[key]"  class="destpost"  ng-blur="handleBlurEvent(gridselection,key,'.destpost')" />
									
									 </div>
								
								</div>
								
								<div  ng-if="($index==0) && key=='OriginPostalRange'">
								 
								 <div class="col-lg-2 col-md-2 padding5" zipauto>
									<input type="text" ng-disabled="!ratelanematrix.OriginCountryCode?true:false"  ng-model="gridselection[key]"  class="sourcepost" ng-blur="handleBlurEvent(gridselection,key,'.sourcepost')"  />
									 
									
									 </div>
								
								</div>
								
								<div  ng-if="($index==1) && key=='DestinationPostalRange'">
								 
								 <div class="col-lg-2 col-md-2 padding5" zipauto>
									<input type="text"  ng-disabled="!ratelanematrix.DestinationCountryCode?true:false"  ng-model="gridselection[key]"  class="destpost" ng-blur="handleBlurEvent(gridselection,key,'.destpost')"  />
									
									
									 </div>
								
								</div>
								
								
								<div  ng-if="($index==0 ) && key=='OriginPostalCode3Range'">
								  <div class="col-lg-2 col-md-2 padding5" statecity>
									  <input type="text"  ng-disabled="!ratelanematrix.OriginCountryCode?true:false" ng-model="gridselection[key]" class="sourcepost" ng-blur="handleBlurEvent(gridselection,key,'.sourcepost')"/> 
									  </div>
								  </div>
								  <div  ng-if="($index==1) && key=='DestinationPostalCode3Range'">
								  <div class="col-lg-2 col-md-2 padding5" statecity>
									  <input type="text"  ng-disabled="!ratelanematrix.DestinationCountryCode?true:false"  ng-model="gridselection[key]" class="destpost" ng-blur="handleBlurEvent(gridselection,key,'.destpost')"/>
									   </div>
								  </div>
								  
								  
								  
								  
								
								 <div  ng-if="($index==0 ) && key=='OriginStateCity'">
								  <div class="col-lg-2 col-md-2 padding5" statecity>
									  <input type="text" ng-disabled="!ratelanematrix.OriginCountryCode?true:false" ng-model="gridselection[key]" class="sourcestatecity" ng-blur="handleBlurEvent(gridselection,key,'.sourcestatecity')"/> 
									  </div>
								  </div>
								  <div  ng-if="($index==1) && key=='DestinationStateCity'">
								  <div class="col-lg-2 col-md-2 padding5" statecity>
									  <input type="text"  ng-disabled="!ratelanematrix.DestinationCountryCode?true:false"  ng-model="gridselection[key]" class="deststatecity" ng-blur="handleBlurEvent(gridselection,key,'.deststatecity')"/> 
									  </div>
								  </div>
								  
								  
								  
								  
								  <div  ng-if="($index==0) && key=='OriginState'">
								  <div class="col-lg-2 col-md-2 padding5" state>
									  <input type="text" ng-disabled="!ratelanematrix.OriginCountryCode?true:false" ng-model="gridselection[key]" class="sourcestate" ng-blur="handleBlurEvent(gridselection,key,'.sourcestate')"/> 
								  </div>
								  </div>
								  
								  <div  ng-if="($index==1) && key=='DestinationState'">
								  <div class="col-lg-2 col-md-2 padding5" state>
									  <input type="text" ng-disabled="!ratelanematrix.DestinationCountryCode?true:false"   ng-model="gridselection[key]" class="deststate" ng-blur="handleBlurEvent(gridselection,key,'.deststate')"/> 
								  </div>
								  </div>
								  
								  
								  <div class="col-lg-1 col-md-1 padding5" ng-if="$index!=0 && $index!=1 && key!='_id'">
									  <input type="text"   ng-model="gridselection[key]" /> 
								 </div>
								  
								 
							</div>
							
							
							<!--<div class="col-lg-1 col-md-1 padding5"><button class="btn btn-primary" type="button" ng-click="saveGridItem(gridselection['_id'])">Save</button></div>-->
							
					   </div>
					   
						<div ui-grid="gridOptions" ui-grid-edit ui-grid-selection class="grid" ></div>
				   </div>
				   
				   <div class="row">
						<div class="col-lg-12 col-md-12 buttons">
							
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" ng-click="updateMatrixlane();" class="btn btn-primary" ng-disabled="ratelanematrixfrm.$invalid">Save</button>
						</div>
					</div>
				
            
            
				   
			 </div>
				</div>
		   	   
		</div>
		   
		   </form>
		   
		   
		   
		   
		   
		   
		 
		   
		   
		   
		   </div>
		  
		 </div>
    </div>
  </div>
</div>  
		  
		  
		  

@endsection
@section('scripts')


<script src="{{ asset('js/ProfileManager.js') }}"> </script>


@endsection

