 <div class="contract_table" style="min-height: 797px;" ng-show="rightpanel=='Carrier_Api'" >
	<link href="<?php echo asset('bower_components/ng-tags-input/ng-tags-input.min.css')?>" rel="stylesheet">
	<link href="<?php echo asset('bower_components/ng-tags-input/ng-tags-input.bootstrap.min.css')?>" rel="stylesheet">	
	<link href="<?php echo asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css')?>" rel="stylesheet">				
								
								
						<h2>Now, lets setup carrier contract</h2>
	<div class="row setup_top hidden" ng-repeat="carrier in apiSetupCollection.carriers" id="carrier_{{$index}}" data-scac="{{carrier.scac}}">
	  <div class="col-lg-4">
		  <div class="setup_top_left">
			  <div class="setup_left_top">
				  <img src="<?php echo asset('images/no_image.png')?>" alt="{{carrier.name}}" ng-src="{{carrier.logo}}">
				  <p><strong>{{carrier.name}}</strong>
					  {{carrier.address1}}<br>
					  {{carrier.address2}}
					  {{carrier.city}}, {{carrier.state}} {{carrier.postal}}<br>
					  Hours {{carrier.days ? carrier.days : ''}} {{carrier.startTime}} - {{carrier.endTime}} {{carrier.timezone ? carrier.timezone : ''}}</p>
			  </div>
			  <div class="setup_left_bottom">
					  <p><strong>{{carrier.contactName}}</strong>
					  <a href="mailto:{{carrier.contactEmail}}">{{carrier.contactEmail}}</a> | {{carrier.contactPhone}}</p>
				</div>
		  </div>
	  </div>
	  <div class="col-lg-8">
		  <div class="setup_top_right" ng-if="carrier.apiEnabled == 1">
				  <p class="api_message">This carrier have API enabled, you can easily get quote by just setting up userid and password</p>
				  <h4>API Setup Instruction</h4>
				  <p><strong>Do you have userid and password to carrier Site?</strong>
				  If you do not have userid and password to carrier site then contact carrier</p>
				  <h4>What is API?</h4>
				  <p>API is application programming interface which allow you to contact carrier via our application to carrier application programmatically.</p>
			  </div>
	  </div>
    </div>		
								
								
								
								
								
								
								<p class="contract_api">Take me to loading Contract, I don't want to use API <a href="#">Contract Loading</a></p>

						<div id="horizontalTab1" class="tab-hld" easytabs ng-show="apiSetupCollection.carriers.length">
							  <ul class="resp-tabs-list">
								<li>Carrier API Setup</li>
								<li>Carrier Rules Tariff / Accessorial </li>
								<li>Carrier Rules Tariff / Fuel </li>
							  </ul>
							  <div class="resp-tabs-container">
								  <div class="tab-content">
									  <div class="carrier-tab">
										<form name="carrier_account_setup" id="carrier_account_setup" novalidate>
										<input type="hidden" name="apisetup.step" value="4" ng-init="apisetup.step=4" required>
										<input type="hidden" ng-model="apisetup.carrierID" name="carrierID">
										<input type="hidden" ng-model="apisetup.scac" name="scac">
										<input type="hidden" ng-model="apisetup.id" name="id">
										 
										 <h3>I have User ID , Password and Account No</h3>
										  <div class="row">
											  <div class="col-lg-3">
												  <div class="form-group">
													  <label for="">User Name*</label>
													  <input type="text" class="form-control" id="rate_apiuserid" name="rate_apiuserid" ng-model="apisetup.rate_apiuserid" placeholder="" required>
												  </div>
												  <div class="form-group">
													  <label for="">Password*</label>
													  <input type="password" class="form-control" id="rate_apipwd" name="rate_apipwd" ng-model="apisetup.rate_apipwd" placeholder="" required>
												  </div>
												  <div class="form-group">
													  <label for="">Account No</label>
													  <input type="text" class="form-control" id="rate_apiaccount" name="rate_apiaccount" ng-model="apisetup.rate_apiaccount" placeholder="">
												  </div>
											  </div>
											  <div class="col-lg-9">
												<div class="row">
												  <div class="account_type">
													  <label class="top_label">What type of account it is?</label>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apishiptype" id="rate_apishiptype_shipper" ng-model="apisetup.rate_apishiptype" value="S" required>Shipper
															  </label>
														  </div>
														  <tags-input ng-model="apisetup.shpZip" display-property="shpZip"></tags-input>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apishiptype_shipper" id="rate_apishiptype_consignee" ng-model="apisetup.rate_apishiptype" value="C" required>Consignee 
															  </label>
														  </div>
														  <tags-input ng-model="apisetup.csnZip" display-property="csnZip"></tags-input>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apishiptype_shipper" id="rate_apishiptype_thirdparty" ng-model="apisetup.rate_apishiptype" value="T" required>Third Party 
															  </label>
														  </div>
													  </div>
												  </div>
												  </div>
												  <div class="row">
												  <div class="account_type">
													<label class="top_label">What type of Payment account?</label>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_prepaid" ng-model="apisetup.rate_apipaytype" value="P" required>Prepaid
															  </label>
														  </div>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_collect" ng-model="apisetup.rate_apipaytype" value="C" required>Collect 
															  </label>
														  </div>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdparty" ng-model="apisetup.rate_apipaytype" value="T" required>Third Party 
															  </label>
														  </div>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdpartycollect" ng-model="apisetup.rate_apipaytype" value="TC" required>Third Party Collect
															  </label>
														  </div>
													  </div>
													  <div class="account_type_inner">
														  <div class="checkbox">
															  <label class="radio-inline">
																  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdpartyprepaid" ng-model="apisetup.rate_apipaytype" value="TP" required>Third Party Prepaid
															  </label>
														  </div>
													  </div>
												  </div>
												  </div>
											  </div>
										  </div>
										  <div class="row">
											  <div class="col-lg-6">
												  <div class="api_gray">
													  <label class="top_label">What is the purpose of account?</label>
													  <label class="checkbox-inline">
														  <input type="checkbox" id="rate_enabletracking" name="rate_enabletracking" ng-model="apisetup.rate_enabletracking">Activate for Tracking
													  </label>
													  <label class="checkbox-inline">
														  <input type="checkbox" id="rate_enablerate" name="rate_enablerate" ng-model="apisetup.rate_enablerate"  >Activate for Rate 
													  </label>
													  <label class="checkbox-inline">
														  <input type="checkbox" id="rate_enablepickup" name="rate_enablepickup" ng-model="apisetup.rate_enablepickup">Activate for Pickup Request
													  </label>
												  </div>
											  </div>
											  <div class="col-lg-3">
												  <div class="api_gray">
													  <label class="top_label">Would you like to show Indirect Lane?</label>
													  <label class="radio-inline">
														  <input type="radio" name="rate_apidirectonly" id="inlineRadio1" ng-model="apisetup.rate_apidirectonly" value="1">Yes
													  </label>
													  <label class="radio-inline">
														  <input type="radio" name="rate_apidirectonly" id="inlineRadio2" ng-model="apisetup.rate_apidirectonly" value="0">No
													  </label>
												  </div>
											  </div>
										  </div>
										  <div class="row" ng-init="apiStatusImageUnknown='<?php echo asset('images/connection-status-icon-disable.png')?>';apiStatusImageError='<?php echo asset('images/connection-status-icon-red.png')?>';apiStatusImageSuccess='<?php echo asset('images/connection-status-icon.png')?>'">
											  <div class="col-lg-6">
												<div class="row">
												  <div class="connect_buttons">
													  <p>Alright, you are almost there. Lets test connectivity to carrier.</p>
													  <button  type="button" class="btn btn-primary" ng-disabled="carrier_account_setup.$invalid" ng-click="checkCarrierStatus()">Connect Carrier</button>
													  <img src="<?php echo asset('images/connection-status-icon-disable.png')?>" ng-src="{{ apiStatusImage }}" alt="carrier signal" ng-init="apiStatusImage=apiStatusImageUnknown">
													  <!-- <button  type="button" class="btn btn-default">Test Rate</button> -->
												  </div>
												</div>
												<div class="row">
													<div class="alert alert-danger" ng-show="error.status">
														<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
														<strong><span ng-bind="error.message"></span></strong>
													</div>
													<div class="alert alert-success" ng-show="success.status">
														<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
														<strong><span ng-bind="success.message"></span></strong>
													</div>
												</div>
											  </div>
										  </div>
										</form>
									  </div>
								  </div>
								  <div class="tab-content">
									  <div class="row upload_rate">
										<div class="col-lg-12 col-md-12 col-sm-12">
										  <div class="accs-tariffs" carrier-id="{{apisetup.carrierID}}" default-rates="accsRates.defaultRates" custom-rates="accsRates.customRates" accs-types="accsRates.accsTypes" calc-types="accsRates.calculationTypes" default-rate-accs="apisetup.acc_ruletariff"></div>
										</div>
									  </div>
								  </div>
								  <div class="tab-content">
									  <div class="row upload_rate">
										<div class="col-lg-12 col-md-12 col-sm-12">
										  <div class="fuel-tariffs" carrier-id="{{apisetup.carrierID}}" default-rate-count="{{fuelRates.totalDefaultCount}}" custom-rate-count="{{fuelRates.totalCustomRateCount}}" default-rates="fuelRates.defaultRates" custom-rates="fuelRates.customRates" default-rate-fuel="apisetup.fuel_ruletariff" fuel-types="fuelRates.fuelTypes"></div>
										</div>
									  </div>
								  </div>
							  </div>
						</div>

							
				</div>
