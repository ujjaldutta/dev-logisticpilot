<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.bootstrap.min.css')}}" rel="stylesheet">	
<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css')}}" rel="stylesheet">				
													
													
<div class="contract_table custom-contractapi" style="min-height: 797px;" ng-show="rightpanel=='profile'">
	<div class="tab-hld">
	<ul class="nav nav-tabs resp-tabs-list">
													  <li class="active"><a data-toggle="tab" href="#carrier">Carrier</a></li>
													  <li ><a data-toggle="tab" href="#carrierapi" ng-click="CarrierWebservice();" >Carrier API</a></li>
													  
													</ul>

													<div class="tab-content resp-tabs-container">
													  <div id="carrier" class="tab-pane fade in active">
																					
																					
																					
																					<div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
																					<div class="row contract_buttons">
																						<div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 right_buttons pull-right">
																							<div class="dropdown">
																								<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
																									Action
																									<span class="caret"></span>
																								</button>
																								<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
																									<li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#moreDetails_upload">Upload Rate(s)</a></li>
																									<li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#moreDetails" ng-click="resetcontract()">Add Contract</a></li>
																									<li role="presentation"><a href="#" >Mass Update</a></li>
																								</ul>
																							</div>
																					  </div>
																					  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 pull-left customer_searech no_gap">
																						  <strong>{{ Lang::get('profile.profile_selected') }}	: @{{contractlist.profile.RateProfileDescription}}</strong>
																						  
																					  </div>
																					</div>
																					<div class="table-responsive clearfix protable"  >
																					  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table big customStyle">
																						  <tr>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('carrierId')">Carrier<i class="fa " ng-show="predicate === 'carrierId'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col" ><a href="javascript:;" ng-click="sortColumn('RateType')">Rate Type<i class="fa " ng-show="predicate === 'RateType'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('ServiceLevel')">Service Level<i class="fa " ng-show="predicate === 'ServiceLevel'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('EquipmentType')">Equipment Type<i class="fa " ng-show="predicate === 'EquipmentType'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('DistanceApp')">Distance Application<i class="fa " ng-show="predicate === 'DistanceApp'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('EffectiveFrom')">Effective From<i class="fa " ng-show="predicate === 'EffectiveFrom'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('EffectiveTo')">Effective To<i class="fa " ng-show="predicate === 'EffectiveTo'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							
																							<th align="left" valign="middle" scope="col">View Contract</th>						
																							<th align="left" valign="middle" scope="col">Action</th>
																							</tr>
																						  
																						  <tr ng-class="{selected: contract._id==ratelane.cId}" ng-repeat="contract in contractlist.contracts" tooltipshow ng-click="editContract(contract._id);" style="cursor: pointer;">
																							<td align="left" valign="middle">@{{contract.carrierId}}</td>
																							<td align="left" valign="middle">@{{contract.RateType.name}}</td>
																							<td align="left" valign="middle">@{{contract.ServiceLevel}}</td>
																							<!--<td align="left" valign="middle" class="tooltip_cont" >
																								<a href="#">Show Rate</a>
																								
																								<div class="tooltip_div" ng-show="contract.RateType.id=='3'">
																									<h3>FAK</h3>
																									<p>
																										<span ng-show="contract.FAK50!=''">FAK50 : @{{contract.FAK50}}</span>
																										<span ng-show="contract.FAK55!=''">FAK55 : @{{contract.FAK55}}</span>
																										<span ng-show="contract.FAK60!=''">FAK60 : @{{contract.FAK60}}</span>
																									</p>
																									<p>
																										<span ng-show="contract.FAK65!=''">FAK65 : @{{contract.FAK65}}</span>
																										<span ng-show="contract.FAK70!=''">FAK70 : @{{contract.FAK70}}</span>
																										<span ng-show="contract.FAK75!=''">FAK75 : @{{contract.FAK75}}</span>
																									</p>
																									
																									<p>
																										<span ng-show="contract.FAK85!=''">FAK85 : @{{contract.FAK85}}</span>
																										<span ng-show="contract.FAK90!=''">FAK90 : @{{contract.FAK90}}</span>
																										<span ng-show="contract.FAK95!=''">FAK95 : @{{contract.FAK95}}</span>
																									</p>
																									
																									<p>
																										<span ng-show="contract.FAK100!=''">FAK100 : @{{contract.FAK100}}</span>
																										<span ng-show="contract.FAK110!=''">FAK110 : @{{contract.FAK110}}</span>
																										<span ng-show="contract.FAK125!=''">FK125 : @{{contract.FAK125}}</span>
																									</p>
																									<p>
																										<span ng-show="contract.FAK150!=''">FAK150 : @{{contract.FAK150}}</span>
																										<span ng-show="contract.FAK175!=''">FAK175 : @{{contract.FAK175}}</span>
																										<span ng-show="contract.FAK200!=''">FK200 : @{{contract.FAK200}}</span>
																									</p>
																									<p>
																										<span ng-show="contract.FAK250!=''">FAK250 : @{{contract.FAK250}}</span>
																										<span ng-show="contract.FAK300!=''">FAK300 : @{{contract.FAK300}}</span>
																										<span ng-show="contract.FAK400!=''">FK400 : @{{contract.FAK400}}</span>
																										<span ng-show="contract.FAK500!=''">FK500 : @{{contract.FAK500}}</span>
																									</p>
																									
																								</div>
																							</td>-->
																							<td align="left" valign="middle">@{{contract.EquipmentType}}</td>
																							<td align="left" valign="middle">@{{contract.DistanceApp.name}}</td>
																							<td align="left" valign="middle">@{{contract.EffectiveFrom}}</td>
																							<td align="left" valign="middle">@{{contract.EffectiveTo}}</td>
																							

																							<td align="left" valign="middle">
																							 <a ngf-select ngf-change="upload($files,contract._id)" accept=".xls,.pdf,.xlsx,.doc,.docx" >Upload</a>
																															
																							<div class="file_upload" ng-show="contract.file.length > 0">
																																		
																							   <span><a href="javascript:void(0)" ng-click="viewFile(contract._id);">View</a></span>
																								<span><a href="javascript:void(0)" ng-click="deleteFile(contract._id);">Delete</a></span>
																							  
																							</div>
																						   
																							
																							</td>

																							<td align="left" valign="middle">
																						
																								<ul class="edit-delete actionBtnBox">
																								<li><a  href="javascript:void(0)" ng-click="editContract(contract._id);" data-toggle="modal" data-target="#moreDetails"><i class="fa fa-pencil-square-o"></i></a></li>
																								<li><a href="javascript:void(0)" ng-click="deleteContract(contract._id);"><i class="fa fa-trash-o"></i></a></li>
																								<li><a href="javascript:void(0)" ng-click="copyContract(contract._id);"><i class="fa fa-copy"></i></a></li>
																								<li ng-if="contract.RateType.id=='6'"><a ngf-multiple="false" ngf-select ngf-change="UploadRate($files,contract._id)" accept=".xls,.csv" title="Upload Rate" ><i class="fa fa-upload"></i></a></li>
																							</ul>
																								</td>
																	  
																						</tr>
																						<tr ng-hide="contractlist.contracts.length">
																							<td colspan="11" align="center"><strong>No Record Found!</strong></td>
																						</tr>
																						  
																					</table>
																				  </div>
																				  
																				   <div class="row m-bot">
																						<div class="col-md-3 col-sm-4">
																							<div class="form-group">
																								<select ng-options="pageno.id as pageno.val for pageno in pageNos" ng-model="recordLimit" id="record_limit_selecter" name="record_limit_selecter" class="form-control"></select>
																							</div>
																						</div>
																						<div class="col-md-9 col-sm-8">
																						
																						  <uib-pagination class="pull-right" total-items="contractlist.count" items-per-page="recordLimit" ng-model="currentPage" ng-change="pageChanged()"></uib-pagination>
																						  <p class="record-data">
																							  Showing record @{{(recordLimit * currentPage) > contractlist.count ? contractlist.count : recordLimit * currentPage}} of @{{contractlist.count}}
																							 </p>
																						  
																						</div>
																					</div>
																					
																				  <div class="row contract_buttons">
																					 <!-- <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 no_gap mass_update_block">
																						  <form>
																							<div class="form-group">
																								<select class="form-control">
																								  <option>1</option>
																								  <option>2</option>
																								  <option>3</option>
																								  <option>4</option>
																								  <option>5</option>
																								</select>
																							</div>
																							<div class="form-group">
																								<input type="text" class="form-control" placeholder="">
																							</div>
																							<button type="button" class="btn btn-primary download_excel">Mass Update</button>
																						  </form>
																					  </div>-->
																					  <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 pull-right right_buttons pull-right" ng-show="contractlist.contracts.length>0">
																						  
																						  <button type="button" class="btn btn-primary download_excel" data-toggle="modal" data-target="#ratematrixDetails" ng-show="contract.RateType.id=='6' && gridOptions.data.length<=0">Add RateMatrix</button>
																						  <button type="button" class="btn btn-primary download_excel" data-toggle="modal" data-target="#ratematrixDetails" ng-show="contract.RateType.id=='6' && gridOptions.data.length>0">Edit RateMatrix</button>
																						  <button type="button" class="btn btn-primary download_excel" data-toggle="modal" data-target="#ratemoreDetails" ng-click="resetlane()" ng-show="contract.RateType.id!='6'">Add Lane</button>
																					  </div>
																					</div>
																				   
																					
																					
																					
																					
																					
																					
																					
																					<div class="table-responsive clearfix protable" ng-if="contract.RateType.id!='6'">
																					  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table big list-group">
																						  <tr>
																							<th align="center" valign="middle" scope="col">
																								<div class="checkbox">
																									<label>
																										<input type="checkbox">
																									</label>
																								</div>
																							</th>
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('OriginalCountry')">Origin<i class="fa " ng-show="predicate === 'OriginalCountry'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('DestinationCountry')">Destination<i class="fa " ng-show="predicate === 'DestinationCountry'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('FromWeightRange')">Weight Range<i class="fa " ng-show="predicate === 'FromWeightRange'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							
																							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('MoveType')">Lane Direction<i class="fa " ng-show="predicate === 'MoveType'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>
																							
																							<th align="left" valign="middle" scope="col">Rate</th>
																								
																							<th align="left" valign="middle" scope="col">Min. Charge</th>
																							
																							
																							<!--<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('DistanceFrom')">Distance Range<i class="fa " ng-show="predicate === 'DistanceFrom'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>-->
																							
																							<!--<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="lanesortColumn('EquipmentType')">Equipment Type<i class="fa " ng-show="predicate === 'EquipmentType'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>-->
																							
																							
																							<th align="left" valign="middle" scope="col">Action</th>

																							</tr>
																						  <tr endless-scroll="lanerate in lanes" tooltipshow>
																							<td align="center" valign="middle">
																								<div class="checkbox">
																									<label>
																										<input type="checkbox">
																									</label>
																								</div>
																							</td>
																							<td align="left" valign="middle" class="tooltip_cont"><a href="#">@{{lanerate.OriginalCountry}}</a>
																							<div class="tooltip_div">
																									<h3>Origin</h3>
																									<p  ng-if="lanerate.OriginalState">
																										<span class="row" ng-repeat="states in lanerate.OriginalState">@{{states}} -  @{{lanerate.OriginalCity[$index]}} </span> 
																									</p>
																									
																									<p  ng-if="lanerate.OriginalZipcode">
																										Zipcode: <span ng-repeat="zip in lanerate.OriginalZipcode">@{{zip}} </span>
																									</p>
																									
																									<p  ng-if="lanerate.OriginalZipcodeFrom">
																										Zipcode: <span ng-repeat="zip in lanerate.OriginalZipcodeFrom">@{{zip}} </span> -
																										<span ng-repeat="zip in lanerate.OriginalZipcodeTo">@{{zip}} </span>
																									</p>
																								
																								</div>
																							</td>
																							<td align="left" valign="middle" class="tooltip_cont"><a href="#">@{{lanerate.DestinationCountry}}</a>
																							<div class="tooltip_div">
																									<h3>Destination</h3>
																																			
																									
																									<p  ng-if="lanerate.DestinationState">
																										 <span class="row" ng-repeat="states in lanerate.DestinationState">@{{states}} - @{{lanerate.DestinationCity[$index]}} </span>
																									</p>
																									
																									<p  ng-if="lanerate.DestinationZipcode">
																										Zipcode: <span ng-repeat="zip in lanerate.DestinationZipcode">@{{zip}} </span>
																									</p>
																									<p  ng-if="lanerate.DestinationZipcodeFrom">
																										Zipcode: <span ng-repeat="zip in lanerate.DestinationZipcodeFrom">@{{zip}} </span> -
																										<span ng-repeat="zip in lanerate.DestinationZipcodeTo">@{{zip}} </span>
																									</p>
																									
																									
																								</div>
																							</td>
																							<td align="left" valign="middle">@{{lanerate.FromWeightRange}}-@{{lanerate.ToWeightRange}}</td>
																							
																							<td align="left" valign="middle">@{{lanerate.MoveType}}</td>
																							<td align="left" valign="middle">
																							@{{lanerate.Rateware.Rate}}
																							@{{lanerate.DistanceRate.Rate}}
																							@{{lanerate.CPU.Rate}}
																							@{{lanerate.RateMatrix.Rate}}
																							@{{lanerate.CostPerHundredRate.Rate}}
																							</td>
																							<td align="left" valign="middle">
																							@{{lanerate.Rateware.MinCharge}}
																							@{{lanerate.DistanceRate.MinCharge}}
																							@{{lanerate.CPU.MinCharge}}
																							@{{lanerate.RateMatrix.MinCharge}}
																							@{{lanerate.CostPerHundredRate.MinCharge}}
																							</td>
																							
																							
																							
																							<td align="left" valign="middle">
																						
																								<ul class="edit-delete">
																								
																								<li><a  href="javascript:void(0)" ng-click="editRatelane(lanerate._id);" data-toggle="modal" data-target="#ratemoreDetails"><i class="fa fa-pencil-square-o"></i></a></li>
																								<li><a href="javascript:void(0)" ng-click="deleteRatelane(lanerate._id);"><i class="fa fa-trash-o"></i></a></li>
																								
																							</ul></td>
																							
																						</tr>
																						<tr ng-hide="lanes.length">
																							<td colspan="11" align="center"><strong>No Record Found!</strong></td>
																						</tr>
																				 
																			  
																		 </table>
																						<p ng-show="loading" class="text-center">
																						  Loading...
																						</p>
																					</div>
																					<!--<div class="row">
																						<div class="col-md-3 col-sm-4">
																							<div class="form-group">
																								<select ng-options="pageno.id as pageno.val for pageno in pageNos" ng-model="lanerecordLimit" id="record_limit_selecter" name="record_limit_selecter" class="form-control"></select>
																							</div>
																						</div>
																						<div class="col-md-9 col-sm-8">
																						<uib-pagination class="pull-right" total-items="laneratelist.count" items-per-page="lanerecordLimit" ng-model="lanecurrentPage" ng-change="lanepageChanged()"></uib-pagination>
																						  <p class="record-data">
																							  Showing record @{{(lanerecordLimit * lanecurrentPage) > laneratelist.count ? laneratelist.count : lanerecordLimit * lanecurrentPage}} of @{{laneratelist.count}}
																							 </p>
																						  
																						</div>
																					</div>-->
																					
																					
																					
																					
																					
																					
																					
													  </div>
													  
													  <div id="carrierapi" class="tab-pane fade">
																		<div class="row">
																			<div class="col-sm-6">
																			<div class="filter-head custom-padding-no lanes_block no_bord">
																			<h2>Existing API Setup</h2>
																			
																			<!--<div class="checkbox">
																				<label><input type="checkbox" ng-model="apiEnabledCarrierOnly">Display API Carrier Only</label>
																			</div>-->
																			
																			<div class="table-responsive">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table lancecount_table table-striped  table-bordered">
																				  <tbody>
																					<tr ng-repeat="carrier in apiSetupCollection.carriers | filter:sortByApiEnabledCarrier">
																					  <td width="50%">
																					  <a href="#" ng-if="carrier.apiEnabled == 1" ng-click="visibleCarrierApiSetup(carrier.id, carrier.scac,'setup')">@{{carrier.name}}</a>
																						<a href="#" ng-click="visibleCarrierContract($index)" ng-if="carrier.apiEnabled == 0">@{{carrier.name}}</a>
																					  </td>
																					   <td width="10%">
																						<img src="{{ asset('images/connection-status-icon-red.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.apiEnabled == 1 && carrier.connectivity==0">
																						<img src="{{ asset('images/connection-status-icon.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.apiEnabled == 1 && carrier.connectivity==1">
																						<img src="{{ asset('images/connection-status-icon-disable.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.connectivity===undefined && carrier.apiEnabled == 1">
																						<img src="{{ asset('images/not_available.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if=" carrier.apiEnabled == 0">
																					  </td>
																					  <td width="40%" align="right">
																						<a href="" ng-if="carrier.apiEnabled == 1" ng-click="visibleCarrierApiSetup(carrier.id, carrier.scac,'setup')">Setup API</a>
																						<a href="" ng-click="visibleCarrierContract($index)" ng-if="carrier.apiEnabled == 0">Setup Contract</a>
																					  </td>
																					</tr>
																				  </tbody>
																				</table>
																			</div>
																			
																		</div>
																		
																		</div>
																		<div class="col-sm-6">
																			<div class="filter-head custom-padding-no lanes_block no_bord">
																					<h2>Your Carrier List</h2>
																						<form role="search" class="">
																						<div class="input-group add-on">
																						  <!-- <input type="text" id="srch-term" name="srch-term" placeholder="Search" class="form-control"> -->
																						  <div angucomplete-alt id="client_search" placeholder="Search" pause="200" remote-url="/front/client-register?action=getSelectedCarriers&apiEnabled=@{{apiEnabledCarrierOnly==true ? 'true' : 'false'}}&q=" search-fields="name" title-field="name" minlength="2" remote-url-data-field="carriers" 
																						  input-class="form-control" match-class="highlight" input-name="client_search" 
																						  selected-object="selectedSearchCarrier" input-changed="inputChanged" ng-model="clientsearch" > </div>
																						  <div class="input-group-btn">
																							<button type="submit" class="btn btn-default" ng-click="getCarrierSearch()"><i class="fa fa-search"></i></button>
																						  </div>
																						</div>
																					  </form>
																					<div class="table-responsive">
																						<div class="unasssign-carrier">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table lancecount_table table-striped  table-bordered">
																						  <tbody>
																							<tr ng-repeat="carrier in apiSetupCollection.unassignedcarriers | filter:sortByApiEnabledCarrier">
																							  <td width="50%">
																							  <a href="#" ng-if="carrier.apiEnabled == 1" ng-click="visibleCarrierApiSetup(carrier.id, carrier.scac,'withoutsetup')">@{{carrier.name}}</a>
																								<a href="#" ng-click="visibleCarrierContract($index)" ng-if="carrier.apiEnabled == 0">@{{carrier.name}}</a>
																							  </td>
																							   <td width="10%">
																								<img src="{{ asset('images/connection-status-icon-red.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.apiEnabled == 1 && carrier.connectivity==0">
																								<img src="{{ asset('images/connection-status-icon.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.apiEnabled == 1 && carrier.connectivity==1">
																								<img src="{{ asset('images/connection-status-icon-disable.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if="carrier.connectivity===undefined && carrier.apiEnabled == 1">
																								<img src="{{ asset('images/not_available.png')}}" alt="@{{carrier.name}}" style="max-width: 25px;" ng-if=" carrier.apiEnabled == 0">
																							  </td>
																							  <td width="40%" align="right">
																								<a href="" ng-if="carrier.apiEnabled == 1" ng-click="visibleCarrierApiSetup(carrier.id, carrier.scac,'withoutsetup')">Setup API</a>
																								<a href="" ng-click="visibleCarrierContract($index)" ng-if="carrier.apiEnabled == 0">Setup Contract</a>
																							  </td>
																							</tr>
																						  </tbody>
																						</table>
																						</div>
																					</div>
																		</div>
																		</div>
																</div>
																	
																	
																	
																	
																										
																												
																				<h2>Now, lets setup carrier contract</h2>
																					<div class="row setup_top hidden" ng-repeat="carrier in apiSetupCollection.carriers" id="carrier_@{{$index}}" data-scac="@{{carrier.scac}}">
																					  <div class="col-lg-4">
																						  <div class="setup_top_left">
																							  <div class="setup_left_top">
																								  <img src="{{ asset('images/no_image.png')}}" alt="@{{carrier.name}}" ng-src="@{{carrier.logo}}">
																								  <p><strong>@{{carrier.name}}</strong>
																									  @{{carrier.address1}}<br>
																									  @{{carrier.address2}}
																									  @{{carrier.city}}, @{{carrier.state}} @{{carrier.postal}}<br>
																									  Hours @{{carrier.days ? carrier.days : ''}} @{{carrier.startTime}} - @{{carrier.endTime}} @{{carrier.timezone ? carrier.timezone : ''}}</p>
																							  </div>
																							  <div class="setup_left_bottom">
																									  <p><strong>@{{carrier.contactName}}</strong>
																									  <a href="mailto:@{{carrier.contactEmail}}">@{{carrier.contactEmail}}</a> | @{{carrier.contactPhone}}</p>
																								</div>
																						  </div>
																					  </div>
																					  <div class="col-lg-8">
																						  <div class="setup_top_right" ng-if="carrier.apiEnabled == 1">
																								  <p class="api_message">This carrier have API enabled, you can easily get quote by just setting up userid and password</p>
																								  <h4>API Setup Instruction</h4>
																								  <p><strong>Do you have userid and password to carrier Site?</strong>
																								  If you do not have userid and password to carrier site then contact carrier</p>
																								  <h4>What is API?</h4>
																								  <p>API is application programming interface which allow you to contact carrier via our application to carrier application programmatically.</p>
																							  </div>
																					  </div>
																					</div>
																					
																					
																					
																					
																					
																					<div class="row setup_top hidden" ng-repeat="carrier in apiSetupCollection.unassignedcarriers" id="carrier2_@{{$index}}" data-scac="@{{carrier.scac}}">
																					  <div class="col-lg-4">
																						  <div class="setup_top_left">
																							  <div class="setup_left_top">
																								  <img src="{{ asset('images/no_image.png')}}" alt="@{{carrier.name}}" ng-src="@{{carrier.logo}}">
																								  <p><strong>@{{carrier.name}}</strong>
																									  @{{carrier.address1}}<br>
																									  @{{carrier.address2}}
																									  @{{carrier.city}}, @{{carrier.state}} @{{carrier.postal}}<br>
																									  Hours @{{carrier.days ? carrier.days : ''}} @{{carrier.startTime}} - @{{carrier.endTime}} @{{carrier.timezone ? carrier.timezone : ''}}</p>
																							  </div>
																							  <div class="setup_left_bottom">
																									  <p><strong>@{{carrier.contactName}}</strong>
																									  <a href="mailto:@{{carrier.contactEmail}}">@{{carrier.contactEmail}}</a> | @{{carrier.contactPhone}}</p>
																								</div>
																						  </div>
																					  </div>
																					  <div class="col-lg-8">
																						  <div class="setup_top_right" ng-if="carrier.apiEnabled == 1">
																								  <p class="api_message">This carrier have API enabled, you can easily get quote by just setting up userid and password</p>
																								  <h4>API Setup Instruction</h4>
																								  <p><strong>Do you have userid and password to carrier Site?</strong>
																								  If you do not have userid and password to carrier site then contact carrier</p>
																								  <h4>What is API?</h4>
																								  <p>API is application programming interface which allow you to contact carrier via our application to carrier application programmatically.</p>
																							  </div>
																					  </div>
																					</div>
																							
																												
																												
																												
																												
																												
																												
																												<p class="contract_api">Take me to loading Contract, I don't want to use API <a href="#">Contract Loading</a></p>


																
																						<ul class="nav nav-tabs resp-tabs-list">
																							  <li class="active"><a data-toggle="tab" href="#home">Carrier API Setup</a></li>
																							  <li><a data-toggle="tab" href="#menu1">Carrier Rules Tariff / Accessorial</a></li>
																							  <li><a data-toggle="tab" href="#menu2"> Rules Tariff / Fuel</a></li>
																							</ul>

																							<div class="tab-content">
																							  <div id="home" class="tab-pane fade in active">
																												<form name="carrier_account_setup" id="carrier_account_setup" novalidate>
																														<input type="hidden" name="apisetup.step" value="4" ng-init="apisetup.step=4" required>
																														<input type="hidden" ng-model="apisetup.carrierID" name="carrierID">
																														<input type="hidden" ng-model="apisetup.scac" name="scac">
																														<input type="hidden" ng-model="apisetup.id" name="id">
																														<input type="hidden" ng-model="apisetup.contract_profilecode" name="contract_profilecode" ng-init="apisetup.contract_profilecode=profile_id" value="@{{profile_id}}">
																														 
																														 <h3>I have User ID , Password and Account No</h3>
																														  <div class="row">
																															  <div class="col-lg-3">
																																  <div class="form-group">
																																	  <label for="">User Name*</label>
																																	  <input type="text" class="form-control" id="rate_apiuserid" name="rate_apiuserid" ng-model="apisetup.rate_apiuserid" placeholder="" required>
																																  </div>
																																  <div class="form-group">
																																	  <label for="">Password*</label>
																																	  <input type="password" class="form-control" id="rate_apipwd" name="rate_apipwd" ng-model="apisetup.rate_apipwd" placeholder="" required>
																																  </div>
																																  <div class="form-group">
																																	  <label for="">Account No</label>
																																	  <input type="text" class="form-control" id="rate_apiaccount" name="rate_apiaccount" ng-model="apisetup.rate_apiaccount" placeholder="">
																																  </div>
																															  </div>
																															  <div class="col-lg-9">
																																<div class="row">
																																  <div class="account_type">
																																	  <label class="top_label">What type of account it is?</label>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apishiptype" id="rate_apishiptype_shipper" ng-model="apisetup.rate_apishiptype" value="S" required>Shipper
																																			  </label>
																																		  </div>
																																		  <tags-input ng-model="apisetup.shpZip" display-property="shpZip"></tags-input>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apishiptype_shipper" id="rate_apishiptype_consignee" ng-model="apisetup.rate_apishiptype" value="C" required>Consignee 
																																			  </label>
																																		  </div>
																																		  <tags-input ng-model="apisetup.csnZip" display-property="csnZip"></tags-input>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apishiptype_shipper" id="rate_apishiptype_thirdparty" ng-model="apisetup.rate_apishiptype" value="T" required>Third Party 
																																			  </label>
																																		  </div>
																																	  </div>
																																  </div>
																																  </div>
																																  <div class="row">
																																  <div class="account_type">
																																	<label class="top_label">What type of Payment account?</label>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_prepaid" ng-model="apisetup.rate_apipaytype" value="P" required>Prepaid
																																			  </label>
																																		  </div>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_collect" ng-model="apisetup.rate_apipaytype" value="C" required>Collect 
																																			  </label>
																																		  </div>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdparty" ng-model="apisetup.rate_apipaytype" value="T" required>Third Party 
																																			  </label>
																																		  </div>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdpartycollect" ng-model="apisetup.rate_apipaytype" value="TC" required>Third Party Collect
																																			  </label>
																																		  </div>
																																	  </div>
																																	  <div class="account_type_inner">
																																		  <div class="checkbox">
																																			  <label class="radio-inline">
																																				  <input type="radio" name="rate_apipaytype" id="rate_apipaytype_thirdpartyprepaid" ng-model="apisetup.rate_apipaytype" value="TP" required>Third Party Prepaid
																																			  </label>
																																		  </div>
																																	  </div>
																																  </div>
																																  </div>
																															  </div>
																														  </div>
																														  <div class="row">
																															  <div class="col-lg-6">
																																  <div class="api_gray custom-api-grey">
																																	  <label class="top_label">What is the purpose of account?</label>
																																	  <label class="checkbox-inline">
																																		  <input type="checkbox" id="rate_enabletracking" name="rate_enabletracking" ng-model="apisetup.rate_enabletracking">Activate for Tracking
																																	  </label>
																																	  <label class="checkbox-inline">
																																		  <input type="checkbox" id="rate_enablerate" name="rate_enablerate" ng-model="apisetup.rate_enablerate"  >Activate for Rate 
																																	  </label>
																																	  <label class="checkbox-inline">
																																		  <input type="checkbox" id="rate_enablepickup" name="rate_enablepickup" ng-model="apisetup.rate_enablepickup">Activate for Pickup Request
																																	  </label>
																																  </div>
																															  </div>
																															  <div class="col-lg-3">
																																  <div class="api_gray custom-api-grey">
																																	  <label class="top_label">Would you like to show Indirect Lane?</label>
																																	  <label class="radio-inline">
																																		  <input type="radio" name="rate_apidirectonly" id="inlineRadio1" ng-model="apisetup.rate_apidirectonly" value="1">Yes
																																	  </label>
																																	  <label class="radio-inline">
																																		  <input type="radio" name="rate_apidirectonly" id="inlineRadio2" ng-model="apisetup.rate_apidirectonly" value="0">No
																																	  </label>
																																  </div>
																															  </div>
																														  </div>
																														  <div class="row" ng-init="apiStatusImageUnknown='{{ asset('images/connection-status-icon-disable.png')}}';apiStatusImageError='{{ asset('images/connection-status-icon-red.png')}}';apiStatusImageSuccess='{{ asset('images/connection-status-icon.png')}}'">
																															  <div class="col-lg-6">
																																<div class="row">
																																  <div class="connect_buttons pad-left">
																																	  <p>Alright, you are almost there. Lets test connectivity to carrier.</p>
																																	  <button  type="button" class="btn btn-primary" ng-disabled="carrier_account_setup.$invalid" ng-click="checkCarrierStatus()">Connect Carrier</button>
																																	  <img src="{{asset('images/connection-status-icon-disable.png')}}" ng-src="@{{ apiStatusImage }}" alt="carrier signal" ng-init="apiStatusImage=apiStatusImageUnknown">
																																	  <!-- <button  type="button" class="btn btn-default">Test Rate</button> -->
																																  </div>
																																</div>
																																<div class="row">
																																	<div class="alert alert-danger" ng-show="error.status">
																																		<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
																																		<strong><span ng-bind="error.message"></span></strong>
																																	</div>
																																	<div class="alert alert-success" ng-show="success.status">
																																		<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
																																		<strong><span ng-bind="success.message"></span></strong>
																																	</div>
																																</div>
																															  </div>
																														  </div>
																														</form>
																								
																								
																							  </div>
																							  <div id="menu1" class="tab-pane fade">
																								<div class="row upload_rate">
																														<div class="col-lg-12 col-md-12 col-sm-12">
																														  <div class="accs-tariffs" carrier-id="@{{apisetup.carrierID}}" default-rates="accsRates.defaultRates" custom-rates="accsRates.customRates" accs-types="accsRates.accsTypes" calc-types="accsRates.calculationTypes" default-rate-accs="apisetup.acc_ruletariff"></div>
																														</div>
																													  </div>
																							  </div>
																							  <div id="menu2" class="tab-pane fade">
																								<div class="row upload_rate">
																														<div class="col-lg-12 col-md-12 col-sm-12">
																														  <div class="fuel-tariffs" carrier-id="@{{apisetup.carrierID}}" default-rate-count="@{{fuelRates.totalDefaultCount}}" custom-rate-count="@{{fuelRates.totalCustomRateCount}}" default-rates="fuelRates.defaultRates" custom-rates="fuelRates.customRates" default-rate-fuel="apisetup.fuel_ruletariff" fuel-types="fuelRates.fuelTypes"></div>
																														</div>
																													  </div>
																							  </div>
																							</div>
																				
													  </div>
													  
													</div>
	
	</div>
	
	
	
					
					

</div>
				
	
	

