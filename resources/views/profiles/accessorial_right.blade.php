 <div class="contract_table" style="min-height: 797px;" ng-show="rightpanel=='Accessorial'" > 
<div class="row contract_buttons">
						<div class="col-md-6 col-sm-6">
							<strong>{{ Lang::get('profile.profile_selected') }}	: @{{contractlist.profile.RateProfileDescription}}</strong>
						</div>
						  
</div>

								<div id="assocTab1" class="tab-hld">
									<ul class="resp-tabs-list">
										<li ng-click="setcurrentTab('assoc')">Assocarial</li>
										<li ng-click="setcurrentTab('scac')">SCAC Assocarial</li>
									</ul>
									<div class="resp-tabs-container">
										<div class="tab-content">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
													  <button type="button" class="btn btn-primary download_excel pull-right ml10"><span></span>Download to Excel</button>
													  <button type="button" class="btn btn-primary pull-right" ng-click="addNewAssoc()" >Add Accessorial <i class="fa fa-plus-circle"></i></button>
													  
												 </div>
											 </div>
												
												<div class="table-responsive clearfix">
														 <div class="col-sm-12 bordered-col">
														 <div class="row heading-row">
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Accessorial</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Rate Calc</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Min Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Max Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Rate /Perc</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective From</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective To</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">&nbsp;</div></div></div>
														</div>
														
														<div class="row table-body-div" endless-scroll="assoc in assocarial"  assocdate>
															
														  <form method="post" name="assoc_@{{$index}}" id="assoc_@{{$index}}">
														<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="assoc._token='{{ csrf_token() }}'" required>
											
														<input type="hidden"   name="cid" ng-model="assoc.cid"  ng-init="assoc.cid"  value="@{{assoc.cid}}">
														<input type="hidden"   name="scac" ng-model="assoc.scac"  ng-init="assoc.cid"  value="@{{assoc.scac}}">
														<input type="hidden"   id="accessorial_code" name="accessorial_code" ng-model="assoc.accessorial_code"  ng-init="assoc.accessorial_code"  value="@{{assoc.accessorial_code}}">
											
														  <div class="col-sm-2">
																  <div class="form-group">
																		
																		<!--<input type="text" class="form-control" autocomplete="off"  placeholder=""  ng-model="assoc.accessorial" name="accessorial" ng-change="getassoc(assoc.accessorial)"uib-typeahead="atype.name for atype in assoctype"  typeahead-on-select="onSelect($item, assoc, 'assoc')" required >-->
																	
																		<div angucomplete-alt
																				  id="ex5"
																				  placeholder="Search Assoc"
																				  pause="500"
																				  selected-object="onSelectautocomplete"
																				 focus-out="onSelectautocomplete()"
																				  remote-api-handler="assocAPI"
																				  remote-url-request-formatter="remoteUrlRequestFn"
																				  remote-url-data-field="assoctype"
																				  title-field="name"
																				  description-field="description"
																				  minlength="2"
																				 ng-model="assoc.accessorial"
																				 initial-value="@{{assoc.accessorial}}"
																				  input-class="form-control"
																				  match-class="highlight" field-required="true">
																		</div>
																		
																	</div>
														  </div>
														  <div class="col-sm-2">
																<div class="form-group">
																		<select class="form-control" name="rate_calculation" ng-model="assoc.rate_calculation" ng-options="option.name for option in calctype track by option.id" required>
														
													</select>
																	</div>
														  </div>
														  <div class="col-sm-1">
																<div class="form-group">
																		<input type="text" class="form-control" id=""  name="min_rate" ng-model="assoc.min_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control" id=""  name="max_rate" ng-model="assoc.max_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control " id=""  name="rate_percentage" ng-model="assoc.rate_percentage" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																	<div class="form-group">
																		<input type="text" class="form-control fromdate" id=""  name="effective_from" ng-model="assoc.effective_from"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																		<div class="form-group">
																		<input type="text" class="form-control todate" id=""  name="effective_to" ng-model="assoc.effective_to"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																						  
															  <button  type="submit" ng-click="saveAssoc(assoc);" class="button-simple-link btn-icononly" ng-disabled="assoc_@{{$index}}.$invalid"><i class="fa fa-floppy-o"></i></button>
														  <button class="button-simple-link btn-icononly"  type="button" ng-click="deleteAssoc(assoc,$index)"><i class="fa fa-trash-o"></i></button>
														  </div>
														  </form>
														</div>
															
														<div class="row" ng-hide="assocarial.length">
																 <div class="col-sm-12 bordered-col">
																	 <strong>No Record Found!</strong>
																	 </div>
															 </div>
														
														</div>
														
														<p ng-show="loading" class="text-center"> Loading...</p>
														
													  </div>
													  
													<!--<div class="row">
														<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 no_gap mass_update_block">
															  <form>
																<div class="form-group">
																	<select class="form-control">
																	  <option>1</option>
																	  <option>2</option>
																	  <option>3</option>
																	  <option>4</option>
																	  <option>5</option>
																	</select>
																</div>
																<div class="form-group">
																	<input class="form-control" placeholder="" type="text">
																</div>
																<button type="button" class="btn btn-primary download_excel">Mass Update</button>
															  </form>
														  </div>			
													
													</div> -->
														
										</div>
										<!-- end of tab content -->
										
										<div class="tab-content">
																
												<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
													  <button type="button" class="btn btn-primary download_excel pull-right ml10"><span></span>Download to Excel</button>
													  <button type="button" class="btn btn-primary pull-right" ng-click="addNewSCACAssoc()" >Add Accessorial <i class="fa fa-plus-circle"></i></button>
												  </div>
											 </div>
												
												<div class="table-responsive clearfix">
														 <div class="col-sm-12 bordered-col">
														 <div class="row heading-row">
														 
														 <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Carrier</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Accessorial</div></div></div>
														  
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Rate Calc</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Min Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Max Rate</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">Rate /Perc</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective From</div></div></div>
														  <div class="col-sm-2"><div class="table-prop"><div class="table-cell">Effective To</div></div></div>
														  <div class="col-sm-1"><div class="table-prop"><div class="table-cell">&nbsp;</div></div></div>
														</div>
														
														<div class="row table-body-div" endless-scroll="assoc in SCACassocarial" assocdate>
															
														  <form method="post" name="sac_assoc_@{{$index}}" id="sac_assoc_@{{$index}}">
														<input type="hidden"   name="_token" value="{{ csrf_token() }}"  ng-init="assoc._token='{{ csrf_token() }}'" required>
											
														<input type="hidden"   name="cid" ng-model="assoc.cid"  ng-init="assoc.cid"  value="@{{assoc.cid}}">
														
														<input type="hidden"   id="accessorial_code" name="accessorial_code" ng-model="assoc.accessorial_code"  ng-init="assoc.accessorial_code"  value="@{{assoc.accessorial_code}}">
														<div class="col-sm-1">
																<div class="form-group">
																		<input type="hidden" class="form-control" id=""  name="scac" ng-model="assoc.scac">
																		
																		<div angucomplete-alt
																				  id="ffuelex5"
																				  placeholder="Search Carrier"
																				  pause="500"
																				  selected-object="onCarrierselect"
																				 focus-out="onCarrierselect()"
																				  remote-api-handler="carrierAPI"
																				  remote-url-request-formatter="remoteUrlRequestFn"
																				remote-url-data-field="items"
																				  title-field="name"
																				  description-field="description"
																				  minlength="2"
																				 ng-model="assoc.carrier"
																				 initial-value="@{{assoc.carrier}}"
																				  input-class="form-control"
																				  match-class="highlight" field-required="true">
																		</div>
																		
																	</div>
														  </div>
														  
														  <div class="col-sm-2">
															
																	
																	<div angucomplete-alt
																				  id="ex5"
																				  placeholder="Search Assoc"
																				  pause="500"
																				  selected-object="onSelectautocomplete"
																				 focus-out="onSelectautocomplete()"
																				  remote-api-handler="assocAPI"
																				  remote-url-request-formatter="remoteUrlRequestFn"
																				  remote-url-data-field="assoctype"
																				  title-field="name"
																				  description-field="description"
																				  minlength="2"
																				 ng-model="assoc.accessorial"
																				 initial-value="@{{assoc.accessorial}}"
																				  input-class="form-control"
																				  match-class="highlight" field-required="true">
																		</div>
																		
														  </div>
														  <div class="col-sm-1">
																<div class="form-group">
																		<select class="form-control" name="rate_calculation" ng-model="assoc.rate_calculation" ng-options="option.name for option in calctype track by option.id" required>
														
													</select>
																	</div>
														  </div>
														  <div class="col-sm-1">
																<div class="form-group">
																		<input type="text" class="form-control" id=""  name="min_rate" ng-model="assoc.min_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control" id=""  name="max_rate" ng-model="assoc.max_rate" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																	<div class="form-group">
																		<input type="text" class="form-control " id=""  name="rate_percentage" ng-model="assoc.rate_percentage" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																	<div class="form-group">
																		<input type="text" class="form-control fromdate" id=""  name="effective_from" ng-model="assoc.effective_from"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-2">
																		<div class="form-group">
																		<input type="text" class="form-control todate" id=""  name="effective_to" ng-model="assoc.effective_to"  autocomplete="off" required>
																	</div>
														  </div>
														  <div class="col-sm-1">
																						  
															  <button  type="submit" ng-click="saveSCACAssoc(assoc);" class="button-simple-link btn-icononly" ng-disabled="sac_assoc_@{{$index}}.$invalid"><i class="fa fa-floppy-o"></i></button>
														  <button class="button-simple-link btn-icononly"  type="button" ng-click="deleteSCACAssoc(assoc,$index)"><i class="fa fa-trash-o"></i></button>
														  </div>
														  </form>
														</div>
															
														<div class="row" ng-hide="SCACassocarial.length">
																 <div class="col-sm-12 bordered-col">
																	 <strong>No Record Found!</strong>
																	 </div>
															 </div>
														
														</div>
														
															<p ng-show="loading" class="text-center"> Loading...</p>
														</div>
														
													  </div>
													  
													<div class="row">
														<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 no_gap mass_update_block">
															  <form>
																<div class="form-group">
																	<select class="form-control">
																	  <option>1</option>
																	  <option>2</option>
																	  <option>3</option>
																	  <option>4</option>
																	  <option>5</option>
																	</select>
																</div>
																<div class="form-group">
																	<input class="form-control" placeholder="" type="text">
																</div>
																<button type="button" class="btn btn-primary download_excel">Mass Update</button>
															  </form>
														  </div>			
													
													</div> 
													
													
												
										</div>
										<!--end of tab content --->
										
									</div>
								</div>
							
							
							
							
