@extends('layouts.shipment_home')
@section('angular_controller')
    ShipmentEditController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('setup_customer') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop

@section('top_content')
    
@stop
@section('content')
<!-- content section-->
<?php $x = ''; 
     function getRows($key, $counter){ 
		 $product_formfields = array();
		 $product_formfields['po'] = '<td> <input type="text" class="form-control" placeholder="" name="prod_itemcode[]" ng-model="product.prod_itemcode"> </td>';
		 $product_formfields['unit_type'] = '<td class="type">   <select class="form-control" ng-options="item.id as item.label for item in packageTypes" ng-model="product.prod_itempkgtypeId" name="prod_itempkgtypeId[]" id="package_type_'.$counter.'_2" ></select> </td>';
		 $product_formfields['description'] = '<td>  <angucomplete-alt id="unit_desc_'.$counter.'_3" selected-object="itemSelected" input-changed="getProductInput" placeholder="" pause="200" remote-url="/front/customer-products/ajax-product-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="product.unit_desc[]" initial-value="{{product.prod_itemname}}"/></td>';
		 $product_formfields['unit'] = '<td>  <input type="text" class="form-control" placeholder="" name="prod_itemqty[]" ng-model="product.prod_itemqty">    </td> ';
		 $product_formfields['pieces'] = '<td> <input id="p_qty_'.$counter.'_5" name="prod_itempieces[]" ng-model="product.prod_itempieces" type="text" class="form-control" placeholder="" >   </td>';
		 $product_formfields['dimension'] = ' <td style="width:20%;">   <div class="diamention">   <input name="prod_itemlength[]" ng-model="product.prod_itemlength" id="p_length_'.$counter.'_6" type="text" class="form-control" placeholder="L" >   <input name="prod_itemwidth[]" ng-model="product.prod_itemwidth" id="p_width_'.$counter.'_7" type="text" class="form-control" placeholder="W" >   <input name="prod_itemheight[]" ng-model="product.prod_itemheight" id="p_height_'.$counter.'_8" type="text" class="form-control last" placeholder="H" >  </div>  </td>';
		 $product_formfields['class'] = '<td>  <select class="form-control" ng-options="item.id as item.label for item in classTypes" ng-model="product.prod_itemclass" name="prod_itemclass[]" id="prod_itemclass_'.$counter.'_9" ></select>  </td>';
		 $product_formfields['weight'] = '<td>  <input id="p_weight_'.$counter.'_10" name="prod_itemweight[]" ng-model="product.prod_itemweight" type="text" class="form-control" placeholder="">   </td>';

		 $product_formfields['hazmat'] = ' <td>  <input type="checkbox" name="prod_itemhazmat[]" ng-model="product.prod_itemhazmat" id="prod_itemhazmat_'.$counter.'_11"  i-check ng-checked="{{product.prod_itemhazmat > 0}}">  </td>  ';
		 $product_formfields['stop'] = ' <td> <input name="prod_stopId[]" ng-model="product.prod_stopId" id="prod_stopId_'.$counter.'_12" type="text" class="form-control" placeholder="1"> </td>';
		 
		 return $product_formfields[$key];
	 }
	 
?>
 <form id="shipmentProfileForm" name="client_shipment" enctype="multipart/form-data" novalidate ng-init="login_user_id='{{Auth::user()->id}}';login_user_name='{{ Auth::user()->usr_firstname.' '. Auth::user()->usr_lastname}}';shipID ='{{$ship_id}}'">
          <!--input type="hidden" name="_token" value="{{ csrf_token() }}"-->
          <input type="text" class='ng-hide' name="shp_shipnumber" id="shp_shipnumber" ng-model="shipment.shp_shipnumber" value="@{{generatenumber}}">
          <div class="load_entry" >
            	<div class="row load_entry_top">
                  <div class="col-lg-2 book_id">
                      <p><span>@{{shipment.shp_shipnumber}}</span>
                      {{ Lang::get('addshipment.bookingNumberId') }}</p>
                  </div>
                  <div class="col-lg-7 mid_row">
                      <div class="mid_row_inner">
                        <div class="form-group" ng-class="{'has-error': client_shipment.shp_equipmentId.$invalid}">
                          <label>{{ Lang::get('addshipment.equipmentType') }}</label>
                          <select class="form-control" ng-options="item.id as item.label for item in equipTypes" ng-model="shipment.shp_equipmentId" name="shp_equipmentId" id="equipment_id" required></select>
						  	  
						    <!--span class="help-block" ng-show="client_shipment.equipment_id.$invalid">
                            <span ng-show="client_shipment.equipment_id.$error.required">Equipment type is required</span>
                            </span-->
                        </div>
                        <div class="form-group" ng-class="{'has-error': client_shipment.shp_paymentermId.$invalid}">
                          <label>{{ Lang::get('addshipment.payTerms') }}</label>
                          <select class="form-control" ng-options="item.id as item.label for item in invoiceTerms" ng-model="shipment.shp_paymentermId" name="shp_paymentermId" id="invoiceterms_id" required></select>
                        </div>
                        <div class="form-group" ng-class="{'has-error': client_shipment.shp_modeId.$invalid}">
                          <label>{{ Lang::get('addshipment.mode') }}</label>
                          <select ng-change="chkOption()" class="form-control" ng-options="item.id as item.label for item in modeTypes" ng-model="shipment.shp_modeId" name="shp_modeId" id="equipment_mode" required></select>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.purchaseOrder') }}</label>
                          <input ng-model="shipment.shp_ponumber" name="shp_ponumber" type="text" class="form-control" id="shp_ponumber" placeholder="" value="123">
                        </div>
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search PO">
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-3 credit_limit violet_box">
                    <div class="credit_limit_inner">
                      <h3>{{ Lang::get('addshipment.creditLimit') }}</h3>
                      <div class="violet_body">
                          <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div>
                          <p>Your Usages:  <span>$12500 of $15000 used</span></p>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="row load_entry_mid">
                  <div class="col-lg-3">
                    <div class="mid_box">
                      <div class="mid_box_head clearfix">
                        <h3>{{ Lang::get('addshipment.pickupLocation_box') }}</h3>
                        <div class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/open_button.png') }}" alt class="open_button"></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="details_link">Details</a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="mid_box_body">
                        <div class="input-group">
						  <input type="text" class="ng-hide" id="shp_pickupname" name="shp_pickupname" ng-model="shipment.shp_pickupname">
                          <angucomplete-alt placeholder="Name Search" id="pickup_addr" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control form-control-small" match-class="highlight" input-name="shp_pickupnameauto" input-changed="getPickInput" initial-value="@{{shipment.shp_pickupname}}"/>
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.pick_addressLine1') }}</label>
                          <input name="shp_pickupadr1" id="pick_location_adr1" ng-model="shipment.shp_pickupadr1" type="text" class="form-control" value="123" placeholder="">
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.pick_addressLine2') }}</label>
                          <input name="shp_pickupadr2" id="pick_location_adr2" ng-model="shipment.shp_pickupadr2" type="text" class="form-control" placeholder="">
                        </div>
                        <!--div class="inner_row">
                            <div class="form-group" ng-class="{'has-error': client_shipment.pick_location_city.$invalid}">
                              <label>City</label>
                              <input name="pick_location_city" id="pick_location_city" ng-model="shipment.pick_location_city" type="text" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group" ng-class="{'has-error': client_shipment.pick_location_state.$invalid}">
                              <label>State</label>
                              <input name="pick_location_state" id="pick_location_state" ng-model="shipment.pick_location_state" type="text" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group last" ng-class="{'has-error': client_shipment.pick_location_postal.$invalid}">
                              <label>Postal</label>
                              <input name="pick_location_postal" id="pick_location_postal" ng-model="shipment.pick_location_postal" type="text" class="form-control" placeholder="" required>
                            </div>
                        </div-->
						<div class="form-group" ng-class="{'has-error': client_shipment.pick_location.$invalid}">
                          <label>{{ Lang::get('addshipment.pick_location') }}</label>
                          <input id="pick_location" type="text" class="form-control" name="pick_location" g-places-autocomplete ng-model="shipment.pick_location" force-selection="true" options="autocompleteOption" required>
                        </div>
                        <div class="inner_row1">
                            <div class="form-group filter-date" ng-class="{'has-error': client_shipment.shp_reqpickupdate.$invalid}">
							  <label>{{ Lang::get('addshipment.pick_pickupDate') }}</label>
                              <input readonly="readonly" name="shp_reqpickupdate" ng-model="shipment.shp_reqpickupdate" type="text" class="form-control second jqdatepicker" id="datepicker" placeholder="12/12/2014" value="" required>
                            </div>
                            <div class="form-group" ng-class="{'has-error': client_shipment.shp_pickuptimefrom.$invalid}">
                                <div class="bootstrap-timepicker">
								<label>{{ Lang::get('addshipment.pick_openTime') }}</label>
                                  <input type="text" id="timepicker1" name="shp_pickuptimefrom" ng-model="shipment.shp_pickuptimefrom" class="form-control time-box jqdatetimepicker" value="10:45 AM" required>
                                </div>
                            </div>
                            <div class="form-group last" ng-class="{'has-error': client_shipment.shp_pickuptimeto.$invalid}">
                                <div class="bootstrap-timepicker">
								<label>{{ Lang::get('addshipment.pick_closeTime') }}</label>
                                  <input type="text" id="timepicker2" name="shp_pickuptimeto" ng-model="shipment.shp_pickuptimeto" class="form-control time-box jqdatetimepicker" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.pick_contactPhone') }}</label>
                          <input name="shp_pickupphone" id="shp_pickupphone" ng-model="shipment.shp_pickupphone" type="text" class="form-control" value="" placeholder="">
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.pick_contactName') }}</label>
                          <input name="shp_pickupcontact" id="shp_pickupcontact" ng-model="shipment.shp_pickupcontact" type="text" class="form-control" value="" placeholder="">
                        </div>						
						
						
                        <div class="form-group">
                          <label id="location-colapse">{{ Lang::get('addshipment.pick_note') }} <i id="arrow" class="fa fa-chevron-circle-down"></i></label>
                          <textarea class="form-control lc-tarea" ng-model="shipment.shp_pickupnotes" name="shp_pickupnotes" rows="3"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="mid_box">
                      <div class="mid_box_head clearfix">
                        <h3>{{ Lang::get('addshipment.deliveryLocation_box') }}</h3>
                        <div class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/open_button.png') }}" alt class="open_button"></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="details_link">Details</a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="mid_box_body">
                        <div class="input-group">
						<input type="text" class="ng-hide" id="shp_deliveryname" name="shp_deliveryname" ng-model="shipment.shp_deliveryname">
                         <angucomplete-alt placeholder="Name Search" id="del_addr" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shp_deliverynameauto" input-changed="getDelInput" initial-value="@{{shipment.shp_deliveryname}}"/>
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.del_addressLine1') }}</label>
                          <input name="shp_deliveryadr1" id="del_location_adr1" ng-model="shipment.shp_deliveryadr1" type="text" class="form-control" placeholder="" >
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.del_addressLine2') }}</label>
                          <input name="shp_deliveryadr2" id="del_location_adr1" ng-model="shipment.shp_deliveryadr2" type="text" class="form-control" placeholder="">
                        </div>
                        <!--div class="inner_row">
                            <div class="form-group" ng-class="{'has-error': client_shipment.del_location_city.$invalid}">
                              <label>City</label>
                              <input name="del_location_city" id="del_location_city" ng-model="shipment.del_location_city" type="text" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group" ng-class="{'has-error': client_shipment.del_location_state.$invalid}">
                              <label>State</label>
                              <input name="del_location_state" id="del_location_state" ng-model="shipment.del_location_state" type="text" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group last" ng-class="{'has-error': client_shipment.del_location_postal.$invalid}">
                              <label>Postal</label>
                              <input name="del_location_postal" id="del_location_postal" ng-model="shipment.del_location_postal" type="text" class="form-control" placeholder="" required>
                            </div>
                        </div-->
						<div class="form-group" ng-class="{'has-error': client_shipment.del_location.$invalid}">
                          <label>{{ Lang::get('addshipment.del_location') }}</label>
                          <input type="text" class="form-control" id="del_location" name="del_location" g-places-autocomplete ng-model="shipment.del_location" force-selection="true" options="autocompleteOption" required>
                        </div>
                        <div class="inner_row1">
                            <div class="form-group filter-date" ng-class="{'has-error': client_shipment.shp_expdeliverydate.$invalid}">
							<label>{{ Lang::get('addshipment.del_delDate') }}</label>
                              <input readonly="readonly" type="text" class="form-control second jqdatepicker" id="datepicker2" name="shp_expdeliverydate" ng-model="shipment.shp_expdeliverydate" placeholder="12/12/2014" required>
                            </div>
                            <div class="form-group" ng-class="{'has-error': client_shipment.shp_deliverytimefrom.$invalid}">
                                <div class="bootstrap-timepicker">
								<label>{{ Lang::get('addshipment.del_openTime') }}</label>
                                  <input type="text" id="timepicker3" name="shp_deliverytimefrom" ng-model="shipment.shp_deliverytimefrom" class="form-control time-box jqdatetimepicker" required>
                                </div>
                            </div>
                            <div class="form-group last" ng-class="{'has-error': client_shipment.shp_deliverytimeto.$invalid}">
                                <div class="bootstrap-timepicker">
								<label>{{ Lang::get('addshipment.del_closeTime') }}</label>
                                  <input type="text" id="timepicker4" name="shp_deliverytimeto" ng-model="shipment.shp_deliverytimeto" class="form-control time-box jqdatetimepicker" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.del_contactPhone') }}</label>
                          <input name="shp_deliveryphone" id="shp_deliveryphone" ng-model="shipment.shp_deliveryphone" type="text" class="form-control" value="" placeholder="">
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.del_contactName') }}</label>
                          <input name="shp_deliverycontact" id="shp_deliverycontact" ng-model="shipment.shp_deliverycontact" type="text" class="form-control" value="" placeholder="">
                        </div>							
						
                        <div class="form-group">
                          <label id="location-colapse1">{{ Lang::get('addshipment.del_note') }} <i id="arrow1" class="fa fa-chevron-circle-down"></i></label>
                          <textarea id="shp_deliverynotes" ng-model="shipment.shp_deliverynotes" name="shp_deliverynotes" class="form-control lc-tarea1" rows="3"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="mid_box">
                      <div class="mid_box_head clearfix">
                        <h3>{{ Lang::get('addshipment.billToAddress_box') }}</h3>
                        <div class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/open_button.png') }}" alt class="open_button"></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="details_link">Details</a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="mid_box_body">
                        <div class="form-group">
                          <!--label>Name</label-->
						  <input type="text" class="ng-hide" id="shp_carrierbilltoname" name="shp_carrierbilltoname" ng-model="shipment.shp_carrierbilltoname">
                          <angucomplete-alt placeholder="Name Search" id="billto_addr" pause="200" selected-object="billtolocationSelected" remote-url="/front/customer-locations/ajax-client-billing-info?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="billto_name" title-field="billto_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shp_carrierbilltonameauto" input-changed="getBilltoInput" initial-value="@{{shipment.shp_carrierbilltoname}}" disable-input="clientDefaultSettings.default_settings.billto_noneditable"/>
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.bill_addressLine1') }}</label>
                          <input name="shp_carrierbilltoadr1" ng-model="shipment.shp_carrierbilltoadr1" type="text" class="form-control" id="billto_adr1" placeholder="" ng-disabled="clientDefaultSettings.default_settings.billto_noneditable">
                        </div>
                        <div class="form-group">
                          <label>{{ Lang::get('addshipment.bill_addressLine2') }}</label>
                          <input name="shp_carrierbilltoadr2" ng-model="shipment.shp_carrierbilltoadr2" type="text" class="form-control" id="billto_adr2" placeholder="" ng-disabled="clientDefaultSettings.default_settings.billto_noneditable">
                        </div>
						<div class="form-group" ng-class="{'has-error': client_shipment.billto_location.$invalid}">
                          <label>{{ Lang::get('addshipment.bill_location') }}</label>
                          <input type="text" class="form-control" id="billto_location" name="billto_location" g-places-autocomplete ng-model="shipment.billto_location" force-selection="true" options="autocompleteOption" ng-disabled="clientDefaultSettings.default_settings.billto_noneditable">
                        </div>
                        <!--div class="form-group" ng-class="{'has-error': client_shipment.billto_city.$invalid}">
                          <label>City</label>
                          <input name="billto_city" ng-model="shipment.billto_city" type="text" class="form-control" id="billto_city" placeholder="" required>
                        </div>
                        <div class="form-group" ng-class="{'has-error': client_shipment.billto_state.$invalid}">
                          <label>State</label>
                          <input name="billto_state" ng-model="shipment.billto_state" type="text" class="form-control" id="billto_state" placeholder="" required>
                        </div>
                        <div class="form-group" ng-class="{'has-error': client_shipment.billto_postal.$invalid}">
                          <label>Postal</label>
                          <input name="billto_postal" ng-model="shipment.billto_postal" type="text" class="form-control" id="billto_postal" placeholder="" required>
                        </div-->
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="violet_box">
                      <h3 class="icon">{{ Lang::get('addshipment.similarShipments') }}</h3>
                      <div class="violet_body">
                        <ul>
                          <li><a href="#">123456</a></li>
                          <li><a href="#">245689</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="violet_box">
                      <h3 class="icon">{{ Lang::get('addshipment.suggestedCarrier') }}</h3>
                      <div class="violet_body">
                        <ul>
                          <li><a href="#">Roadway</a></li>
                          <li><a href="#">FedEx</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="violet_box">
                      <h3>{{ Lang::get('addshipment.tlLoadBoard') }}</h3>
                      <div class="violet_body">
                        <!--div class="checkbox">
                          <label>
                            <input type="checkbox">Hot Shipment
                          </label>
                        </div-->
                        <div class="checkbox">
						  <div ng-repeat="suggested_item in suggestedHeadings" style="margin:0 5px; 0 0">
                          <label>
                            <input type="checkbox" name="suggested_list[]" ng-model="suggested_item.suggested_list" value="@{{suggested_item.id}}" ng-checked="suggestedHeadingslist.indexOf(suggested_item.id) > -1">@{{suggested_item.label}}
                          </label>
                          </div>						  
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="add_stop" ng-show="isTruckLoad">
                <h3>{{ Lang::get('addshipment.stopHeading') }} <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="collapsed">+-</a></h3>
                <div class="collapse" id="collapseExample">
                  <div class="table-responsive">
                      <table id="customerCost1" width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                        <thead>
                          <tr>
                            <th width="10%">{{ Lang::get('addshipment.searchName') }}</th>
                            <th>{{ Lang::get('addshipment.stop_addressLine1') }}</th>
                            <th>{{ Lang::get('addshipment.stop_addressLine2') }}</th>
                            <th>{{ Lang::get('addshipment.stop_city') }}</th>
                            <th>{{ Lang::get('addshipment.stop_state') }}</th>
                            <th>{{ Lang::get('addshipment.stop_postal') }}</th>
                            <th>{{ Lang::get('addshipment.stop_type') }}</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr id="stop_tr_1_1" ng-repeat="stop in stops">
                            <td class="first">
                              <div class="input-group">
							  <input type="text" class="ng-hide" name="stop_name[]" ng-model="stop.stop_name" id="stop_name_@{{$index+1}}1">
                                <!--input type="text" name="add_stop_input11" class="form-control" placeholder="Name Search"-->
								<angucomplete-alt placeholder="Name Search" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="stop_name_auto[]" id="add_stop_name_@{{$index+1}}_1" input-changed="getInput" initial-value="@{{stop.stop_name}}" />
								
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                              </div>
                            </td>
                            <td>
							
                                <input type="text" name="stop_adr1[]" ng-model="stop.stop_adr1" id="add_stop_addr1_@{{$index+1}}2" class="form-control" placeholder="Address line 1" >
                            </td>
                            <td>
                                <input type="text" name="stop_adr2[]" ng-model="stop.stop_adr2" id="add_stop_addr2_@{{$index+1}}3" class="form-control" placeholder="Address line 2" >
                            </td>
                            <td>
                                <input type="text" name="stop_city[]" ng-model="stop.stop_city" id="add_stop_city_@{{$index+1}}4" class="form-control" placeholder="City" >
                            </td>
                            <td>
                                <input type="text" ng-model="stop.stop_state" name="stop_state[]" id="add_stop_state_@{{$index+1}}5" class="form-control" placeholder="State" >
                            </td>
                            <td>
                                <input type="text" ng-model="stop.stop_postal" name="stop_postal[]" id="add_stop_postcode_@{{$index+1}}6" class="form-control" placeholder="Postal" >
                            </td>
                            <td>
                                <select type="text" ng-model="stop.stop_type" name="stop_type[]" id="add_stop_pd_@{{$index+1}}7" class="form-control" ><option value="P">Pickup - P</option><option value="D">DropOff - D</option></select>
                            </td>
                            <td><button type="button" class="btn btn-default delete" ng-click="removeStop($index)"><span class="minus"></span>Delete</button></td>
                          </tr>
                          <!--tr id="stop_tr_2_1">
                            <td class="first">
                              <div class="input-group">
							  <input type="text" class="ng-hide" name="stop_name[]" ng-model="shipment.stop_name[2]" id="stop_name_21">
                                
								<angucomplete-alt placeholder="Name Search" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="stop_name_auto[]" id="add_stop_name_2_1" input-changed="getInput"/>
								
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                              </div>
                            </td>
                            <td>
                                <input type="text" ng-model="shipment.stop_adr1[2]" name="stop_adr1[]" id="add_stop_addr1_22" class="form-control" placeholder="Address line 1" >
                            </td>
                            <td>
                                <input type="text" ng-model="shipment.stop_adr2[2]" name="stop_adr2[]" id="add_stop_addr2_23" class="form-control" placeholder="Address line 2" >
                            </td>
                            <td>
                                <input type="text" ng-model="shipment.stop_city[2]" name="stop_city[]" id="add_stop_city_24" class="form-control" placeholder="City" >
                            </td>
                            <td>
                                <input type="text" ng-model="shipment.stop_state[2]" name="stop_state[]" id="add_stop_state_25" class="form-control" placeholder="State" >
                            </td>
                            <td>
                                <input type="text" ng-model="shipment.stop_postal[2]" name="stop_postal[]" id="add_stop_postcode_26" class="form-control" placeholder="Postal" >
                            </td>
                            <td>
                                <select type="text" ng-model="shipment.add_stop_pd[2]" name="add_stop_pd[]" id="add_stop_pd_27" class="form-control" ><option value="P">Pickup - P</option><option value="D">DropOff - D</option></select>
                            </td>
                            <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                          </tr-->
						  <tr id="add_row">
						  </tr>
                        </tbody>
                      </table>
                      <div class="col-sm-12 addBtnCont clearfix">
                          <button type="button" class="btn btn-default addRowBtn_customer" ng-click="addStopRow()"><span class="plus"></span>Add more Cost</button>
                      </div>
                  </div>
                </div>
              </div>
			  
              <div class="shipping_product">
                  <h3>{{ Lang::get('addshipment.productHeading') }}</h3>
                  <div class="row shipping_product_inner">
                    <div class="col-lg-10">
                        <div class="table-responsive">
                            <table id="customerCost2" width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                              <thead>
                                <tr>
								@if(sizeof($allFields))
								@foreach($allFields as $field)
								 <th>{{$field->displayName}}</th>
								@endforeach								
								@else
                                  <th>PO#</th>
                                  <th>Unit Type</th>
                                  <th>Description</th>
                                  <th>Unit</th>
                                  <th>Pieces</th>
                                  <th>Dimension</th>
                                  <th>Class</th>
                                  <th>Weight</th>
                                  <th>Hazmat</th>
                                  <th>Stop#</th>
                                  <th></th>
								 @endif
                                </tr>
                              </thead>
                              <tbody>
                                <tr id="product_tr_1_1" ng-repeat="product in shpproducts"> 
								@if(sizeof($allFields))
								@foreach($allFields as $field)
								  <?php echo getRows($field->displayField, '{{$index+1}}'); ?>
								  <?php $x .= $field->displayField.','; ?>
								@endforeach							
								@endif
							   <td><input type="text" class="ng-hide" ng-model="product.prod_itemname" name="prod_itemname[]" id="prod_itemname_@{{$index+1}}_3"><button type="button" class="btn btn-default delete" ng-click="removeProduct($index)"><span class="minus"></span>Delete</button></td>
                                  <!--td>
                                    <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td class="type">
                                      
                                  <select class="form-control" ng-options="item.id as item.label for item in packageTypes" ng-model="shipment.package_types[12]" name="package_type[]" ></select>								
                                  </td>
                                  <td>
                                      <angucomplete-alt id="unit_desc_1_3" placeholder="" pause="200" selected-object="itemSelected" remote-url="/front/customer-products/ajax-product-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_desc[]" />	
                                  </td>
                                  <td>
                                      <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td>
                                      <input id="p_qty_1_5" type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td style="width:20%;">
                                      <div class="diamention">
                                          <input id="p_length_1_6" type="text" class="form-control" placeholder="L" >
                                          <input id="p_width_1_7" type="text" class="form-control" placeholder="W" >
                                          <input id="p_height_1_8" type="text" class="form-control last" placeholder="H" >
                                      </div>
                                  </td>
                                  <td>

									  
									  <select class="form-control" ng-options="item.id as item.label for item in classTypes" ng-model="shipment.class_type[19]" name="class_type[]"  ></select>
									  
                                  </td>
                                  <td>
                                      <input id="p_weight_1_10" type="text" class="form-control" placeholder="">
                                  </td>
                                  <td>
                                      <input type="checkbox">
                                  </td>   
                                  <td>
                                      <input type="text" class="form-control" placeholder="1">
                                  </td>
                                  <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                </tr-->
                                <!--tr>
                                  <td>
                                    <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td class="type">
                                      <angucomplete-alt placeholder="" pause="200" remote-url="/front/customer-products/ajax-package-type-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_type[]" />	
									  <select class="form-control" ng-options="item.id as item.label for item in packageTypes" ng-model="shipment.package_types[22]" name="package_type[]" ></select>	
                                  </td>
                                  <td>
                                     <angucomplete-alt id="unit_desc_2_3" placeholder="" pause="200" selected-object="itemSelected" remote-url="/front/customer-products/ajax-product-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_desc[]" />	
                                  </td>
                                  <td>
                                      <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td>
                                      <input id="p_qty_2_5" type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td style="width:20%;">
                                      <div class="diamention">
                                          <input id="p_length_2_6" type="text" class="form-control" placeholder="L" >
                                          <input id="p_width_2_7" type="text" class="form-control" placeholder="W" >
                                          <input id="p_height_2_8" type="text" class="form-control last" placeholder="H" >
                                      </div>
                                  </td>
                                  <td>
                                      <select class="form-control" ng-options="item.id as item.label for item in classTypes" ng-model="shipment.class_type[29]" name="class_type[]"  ></select>
                                  </td>
                                  <td>
                                      <input id="p_weight_2_10" type="text" class="form-control" placeholder="">
                                  </td>
                                  <td>
                                      <input type="checkbox">
                                  </td>   
                                  <td>
                                      <input type="text" class="form-control" placeholder="1">
                                  </td>
                                  <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                </tr>
                                <tr>
                                  <td>
                                    <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td class="type">
                                      <select class="form-control" ng-options="item.id as item.label for item in packageTypes" ng-model="shipment.package_type[32]" name="package_type[]" ></select>		
                                  </td>
                                  <td>
                                      <angucomplete-alt id="unit_desc_3_3" placeholder="" pause="200" selected-object="itemSelected" remote-url="/front/customer-products/ajax-product-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_desc[]" />	
                                  </td>
                                  <td>
                                      <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td>
                                      <input id="p_qty_3_5" type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td style="width:20%;">
                                      <div class="diamention">
                                          <input id="p_length_3_6" type="text" class="form-control" placeholder="L" >
                                          <input id="p_width_3_7" type="text" class="form-control" placeholder="W" >
                                          <input id="p_height_3_8" type="text" class="form-control last" placeholder="H" >
                                      </div>
                                  </td>
                                  <td>
                                      <select class="form-control" ng-options="item.id as item.label for item in classTypes" ng-model="shipment.class_type[39]" name="class_type[]"  ></select>
                                  </td>
                                  <td>
                                      <input id="p_weight_3_10" type="text" class="form-control" placeholder="">
                                  </td>
                                  <td>
                                      <input type="checkbox">
                                  </td>   
                                  <td>
                                      <input type="text" class="form-control" placeholder="1">
                                  </td>
                                  <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                </tr>
                                <tr>
                                  <td>
                                    <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td class="type">
                                      <select class="form-control" ng-options="item.id as item.label for item in packageTypes" ng-model="shipment.package_type[42]" name="package_type[]" ></select>		
                                  </td>
                                  <td>
                                      <angucomplete-alt id="unit_desc_4_3" placeholder="" pause="200" selected-object="itemSelected" remote-url="/front/customer-products/ajax-product-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_desc[]" />	
                                  </td>
                                  <td>
                                      <input type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td>
                                      <input id="p_qty_4_5" type="text" class="form-control" placeholder="" >
                                  </td>
                                  <td style="width:20%;">
                                      <div class="diamention">
                                          <input id="p_length_4_6" type="text" class="form-control" placeholder="L" >
                                          <input id="p_width_4_7" type="text" class="form-control" placeholder="W" >
                                          <input id="p_height_4_8" type="text" class="form-control last" placeholder="H" >
                                      </div>
                                  </td>
                                  <td>
                                     <select class="form-control" ng-options="item.id as item.label for item in classTypes" ng-model="shipment.class_type[49]" name="class_type[]"  ></select>
                                  </td>
                                  <td>
                                      <input id="p_weight_4_10" type="text" class="form-control" placeholder="">
                                  </td>
                                  <td>
                                      <input type="checkbox">
                                  </td>   
                                  <td>
                                      <input type="text" class="form-control" placeholder="1">
                                  </td>
                                  <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                </tr-->
								<tr id="product_add_more">
								</tr>
                              </tbody>
                            </table>
                            <div class="col-sm-6 addBtnCont clearfix">
							    <input type="text" class="ng-hide" name="product_fields" id="product_fields" value="<?php echo $x; ?>">
                                <button type="button" class="btn btn-primary addRowBtn_customer" ng-click="addProduct('<?php echo $x; ?>')"><span class="plus"></span>Add More Product</button>
								<button type="button" tabindex="-1" data-toggle="modal" data-target="#moreDetails1" class="btn btn-primary">Get Rate</button>
                            </div>
							
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="accessorial">
                          <h3>{{ Lang::get('addshipment.accessorialHeading') }}</h3>
                          <div class="accessorial_body" style="overflow:visible!important;">
							 <!--select title="Basic example" multiple="multiple" name="example-basic" size="5">
							  <option value="option1">Lift Gate</option>
							  <option value="option2">Inside Delivery</option>
							  <option value="option3">Single Shipment</option>
							  <option value="option4">Cross Border</option>
							  <option value="option5">Single Shipment</option>
							 </select-->
							 
							<!--select size="5" name="accessorial_opts" multiple="multiple" title="Basic example" >
								<option label="LIFT GATE PICKUP" value="1">LIFT GATE PICKUP</option>
								<option label="LIFG GATE DELIVERY" value="2">LIFG GATE DELIVERY</option>
								<option label="RESIDENITIAL PICKUP" value="3">RESIDENITIAL PICKUP</option>
								<option label="RESIDENTIAL DELVIERY" value="4">RESIDENTIAL DELVIERY</option>
							</select-->							 

							 <div ng-dropdown-multiselect="" options="accessorials" selected-model="accessorialsModel" checkboxes="true"></div>
			                <!--select title="Basic example" ng-options="item.id as item.accsname for item in accessorials" multiple="multiple" name="example-basic" size="5" ng-model="shipment.accessorial" name="accessorial">
							</select-->							
                            <!--div class="checkbox">
                              <label>
                                <input type="checkbox"> Inside Delivery
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Single Shipment
                              </label>
                            </div>
							                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Cross Border
                              </label>
                            </div>
							                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Single Shipment
                              </label>
                            </div>                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Single Shipment
                              </label>
                            </div>                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Single Shipment
                              </label>
                            </div-->
			
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
					
					
			    <div class="add_stop">
                <h3>{{ Lang::get('addshipment.shipmentDetailsHeading') }} <a data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample" class="collapsed">+-</a></h3>
                <div class="collapse" id="collapseExample1">
                  <div class="table-responsive">
				  <div class="row commission">
                            	<div class="col-lg-3">
                                <h3><strong>{{ Lang::get('addshipment.row_heading1') }}</strong> </h3>
                                    <div class="gray_body">
                                	<div class="form-group">
                                        <label>Sales order number</label>
                                        <input type="text" name="shp_sonumber" ng-model="shipment.shp_sonumber" class="form-control" id="shp_sonumber" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Pro number</label>
                                        <input type="text" name="shp_pronumber" ng-model="shipment.shp_pronumber" class="form-control" id="shp_pronumber" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Carrier Quote number</label>
                                        <input type="text" name="shp_quotenumber" ng-model="shipment.shp_quotenumber" class="form-control" id="shp_quotenumber" placeholder="">
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                <h3><strong>{{ Lang::get('addshipment.row_heading2') }}</strong> </h3>
                                    <div class="gray_body">
                                	<div class="form-group">
                                        <label>Billable weight</label>
                                        <input type="text" name="shp_shipmentvalue" ng-model="shipment.shp_shipmentvalue" class="form-control" id="shp_shipmentvalue" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Shipment value</label>
                                        <input type="text" name="shp_valueperpound" ng-model="shipment.shp_valueperpound" class="form-control" id="shp_valueperpound" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Cost per pound</label>
                                        <input type="text" name="shp_billweight" ng-model="shipment.shp_billweight" class="form-control" id="shp_billweight" placeholder="">
                                    </div>
                                    </div>
                                </div>
								  <div class="col-lg-3">
                                  <h3><strong>{{ Lang::get('addshipment.row_heading3') }}</strong> </h3>
                                    <div class="gray_body">
                                	<div class="form-group">
                                        <label>Billable Weight</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Declared Weight</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Distance</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    </div>
                                </div>
								  <div class="col-lg-3">
                                  <h3><strong>{{ Lang::get('addshipment.row_heading4') }}</strong> </h3>
                                    <div class="gray_body">
                                	<div class="form-group">
                                        <label>Billable Weight</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Declared Weight</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Distance</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    </div>
                                </div>
								
								  <div class="col-lg-3">
								      <h3><strong>{{ Lang::get('addshipment.row_heading5') }}</strong> </h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>MC</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>DOT</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                          	<label>Safety Rating</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
								</div>
								  <div class="col-lg-3">
								      <h3><strong>{{ Lang::get('addshipment.row_heading6') }}</strong> </h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>MC</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>DOT</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                          	<label>Safety Rating</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
								</div>
                                <div class="col-lg-3">
								      <h3><strong>{{ Lang::get('addshipment.row_heading7') }}</strong> </h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>MC</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>DOT</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                          	<label>Safety Rating</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
								</div>
                                <div class="col-lg-3">
								      <h3><strong>{{ Lang::get('addshipment.row_heading8') }}</strong> </h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>MC</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>DOT</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                          	<label>Safety Rating</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
								</div>
                          	</div>
                    
                </div>
              </div> 
			  </div>	  
			  
              <div id="horizontalTab1" class="tab-hld">
                  <ul class="resp-tabs-list">
                    <li>{{ Lang::get('addshipment.shipment_notesHeading') }}</li>
                    <!--<li>Commission</li>-->
                  </ul>
                  <div class="resp-tabs-container">
				  <!--
                      <div class="tab-content">
                          <div class="row">
                            <div class="col-lg-9">
                                <div class="table-responsive">
                                    <table id="customerCost3" width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                      <thead>
                                        <tr>
                                          <th>Carrier</th>
                                          <th>Accessorial</th>
                                          <th>Cost</th>
                                          <th>Sell Cost</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td> 
                                          <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                        </tr>
                                        <tr>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td> 
                                          <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                        </tr>
                                        <tr>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td>
                                          <td>
                                              <input type="text" class="form-control" placeholder="" >
                                          </td> 
                                          <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <div class="col-sm-12 addBtnCont clearfix">
                                        <button type="button" class="btn btn-primary addRowBtn_customer"><span class="plus"></span>Add more Cost</button>
                                    </div>
                                </div>
                            </div>
							
							<!--
                            <div class="col-lg-3">
                                <div class="gray-box">
                                  	<div class="gray_body">
                                    	<div class="form-group">
                                          	<label>Billable Weight</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>Declared Value</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
                                    <h3><strong>Carrier</strong> Safety</h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>MC</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>DOT</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                          	<label>Safety Rating</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
                                    <h3><strong>Carrier</strong> Insurance</h3>
                                    <div class="gray_body">
                                    	<div class="form-group">
                                          	<label>Cargo</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group last">
                                          	<label>Liability</label>
                                          	<input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                  	</div>
                                </div>
                            </div>
							
                          </div>
                      	</div>
							-->
                      	<div class="tab-content">
                      		<div class="row">
                            	<div class="col-lg-4">
                                	<label><h3>{{ Lang::get('addshipment.customerNote') }}</h3></label>
                                	<div class="check_calls">
                                      	<div class="check_calls_inner">
										   
										    <p ng-show="isShipmentDefaultCustomerNotes" style="padding-top:25%; text-align:center;">Add customer notes here ...</p>
                                        	<p ng-repeat="customernote in shipmentCustomerNotes.slice().reverse() track by $index">
											<span><strong>@{{customernote.usr_firstname}} @{{customernote.usr_lastname}}</strong>
											@{{customernote.ship_createddate}} <i class="fa fa-trash-o pull-right" ng-click="removeCustomerNote(customernote)"></i></span>
											<span ng-bind-html="customernote.ship_notes"></span>
											<?php /*$date_val = '{{customernote.ship_createddate}}';
											echo "dddd".$date_val;
											echo $time = strtotime($date_val);
                                             $newformat = date('Y-m-d H:i:s',$time);											
											 $datetime1 = new DateTime($newformat);
												$datetime2 = new DateTime(date('Y-m-d H:i:s'));
												$interval = $datetime1->diff($datetime2);
												//echo $interval->format('%R%a days');*/
											?>
											</p>

                                        	<!--p><span><strong>Sanjay Dey</strong> 7 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 6 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 5 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p-->
                                      	</div>
                                      	<div class="check_calls_textarea">
                                        	<textarea rows="3" class="form-control" ng-model="shipment.customer_note" name="customer_note" id="customer_note" placeholder="Add Note"></textarea>
                                        	<button class="btn btn-default customer_notes" type="button" ng-click="addCustomerNotes();">Submit</button>
                                      	</div>
                                    </div>
                                </div>
                                	<div class="col-lg-4">
                                	<label><h3>{{ Lang::get('addshipment.carrierNote') }}</h3></label>
                                	<div class="check_calls">
                                      	<div class="check_calls_inner">
											<p ng-show="isShipmentDefaultCarrierNotes" style="padding-top:25%; text-align:center;">Add carrier notes here ...</p>
                                        	<p ng-repeat="carriernote in shipmentCarrierNotes.slice().reverse() track by $index">
											<span><strong>@{{carriernote.usr_firstname}} @{{carriernote.usr_lastname}}</strong>
											@{{carriernote.ship_createddate}} <i class="fa fa-trash-o pull-right" ng-click="removeCarrierNote(carriernote)"></i></span>
											<span ng-bind-html="carriernote.ship_notes"></span>
											
											</p>
                                        	<!--p><span><strong>Sanjay Dey</strong> 8 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 7 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 6 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 5 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p-->
                                      	</div>
                                      	<div class="check_calls_textarea">
                                        	<textarea rows="3" class="form-control" ng-model="shipment.carrier_note" name="carrier_note" id="carrier_note" placeholder="Add Note"></textarea>
                                        	<button class="btn btn-default" type="button" ng-click="addCarrierNotes();">Submit</button>
                                      	</div>
                                    </div>
                                </div>    
								<div class="col-lg-4">
                                	<label><h3>{{ Lang::get('addshipment.internalNote') }}</h3></label>
                                	<div class="check_calls">
                                      	<div class="check_calls_inner">
											<p ng-show="isShipmentDefaultInternalNotes" style="padding-top:25%; text-align:center;">Add internal notes here ...</p>
                                        	<p ng-repeat="internalnote in shipmentInternalNotes.slice().reverse() track by $index">
											<span><strong>@{{internalnote.usr_firstname}} @{{internalnote.usr_lastname}}</strong>
											@{{internalnote.ship_createddate}} <i class="fa fa-trash-o pull-right" ng-click="removeInternalNote(internalnote)"></i></span>
											<span ng-bind-html="internalnote.ship_notes"></span>
											
											</p>										
                                        	<!--p><span><strong>Sanjay Dey</strong> 8 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 7 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 6 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                        	<p><span><strong>Sanjay Dey</strong> 5 days ago <i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p-->
                                      	</div>
                                      	<div class="check_calls_textarea">
                                        	<textarea rows="3" class="form-control" ng-model="shipment.internal_note" name="internal_note" id="internal_note" placeholder="Add Note"></textarea>
                                        	<button class="btn btn-default" type="button" ng-click="addInternalNotes();">Submit</button>
                                      	</div>
                                    </div>
                                </div>
                                </div>
                      	</div>
                      	<!--<div class="tab-content">
                        	<div class="row commission">
                            	<div class="col-lg-3">
                                	<div class="form-group">
                                        <label>Sales Rep Name</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Commission %</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                        <span class="com_amount">$ 16.56</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Commission Type</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                	<div class="form-group">
                                        <label>Office</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Commission %</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                        <span class="com_amount">$ 14.56</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Commission Type</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                          	</div>
                      	</div>-->
                      	<!--<div class="tab-content">
                        	<div class="row commission">
                            	<div class="col-lg-3">
                                	<div class="form-group">
                                        <label>Reference 1</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Reference 2</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Reference 3</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                	<div class="form-group">
                                        <label>Reference 4</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Reference 5</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label>Reference 6</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                          	</div>
							
                      	</div>-->
                  	</div>
                  	<div class="btn-cont">
					    <input type="text" class="ng-hide" name="total_counter_of_addstop" ng-model="shipment.total_counter_of_addstop" id="total_counter_of_addstop" value="0">
						<input type="text" class="ng-hide" name="total_counter_of_shipproduct" ng-model="shipment.total_counter_of_shipproduct" id="total_counter_of_shipproduct" value="1">
						
						<!--input type="text" class="ng-show" name="total_counter_of_customernotes" ng-model="shipment.total_counter_of_customernotes" id="total_counter_of_customernotes" value="0">
						<input type="text" class="ng-show" name="total_counter_of_carriernotes" ng-model="shipment.total_counter_of_carriernotes" id="total_counter_of_carriernotes" value="0">
						<input type="text" class="ng-show" name="total_counter_of_internalnotes" ng-model="shipment.total_counter_of_internalnotes" id="total_counter_of_internalnotes" value="0"-->

                    	<button ng-click="cancelEditing();" type="button" class="btn btn-default" style="color:#000;">Cancel</button>
						<div class="loading hidden"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                    	<button  type="button" class="btn btn-primary saveship" ng-disabled="client_shipment.$invalid" ng-click="saveShipmentData(client_shipment)">Save / Update</button>
						
                  	</div>
					<div class="alert alert-success" ng-show="success.status" style="text-align:center;">
					  <strong ng-bind="success.message"></strong>
					</div>
              </div>			  
			  
			  
			  
        	</div>
			</form>
<!-- /content section-->
@section('modal')

<!-- /content section-->
<!-- /modal-->
<div class="modal fade blue bs-example-modal-lg" id="moreDetails1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2>Cheapest Carrier List</h2>
      </div>
      <div class="modal-body">
        <div class="contract_table">
           	  	<div class="quote_cont" id="fedex-con">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                    	<img alt="" src="{{ asset('images/fedex-logo.png') }}"> Fed Ex
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <input name="fedex" type="checkbox" value="" id="fedex">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                       
                	</div>
              	</div>
                <div class="quote_cont">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                        <img alt="" src="{{ asset('images/ups-logo.png') }}"> UPS
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                       <input name="" type="checkbox" value="">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                        
                	</div>
              	</div>
                <div class="quote_cont">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                        <img alt="" src="{{ asset('images/estes-logo.png') }}"> Estate Express
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <input name="" type="checkbox" value="">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                       
                	</div>
              	</div>
                <div class="quote_cont">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                    	<img alt="" src="{{ asset('images/fedex-logo.png') }}"> Fed Ex
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <input name="" type="checkbox" value="">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                       
                	</div>
              	</div>
                <div class="quote_cont">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                        <img alt="" src="{{ asset('images/ups-logo.png') }}"> UPS
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <input name="" type="checkbox" value="">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                        
                	</div>
              	</div>
                <div class="quote_cont">
                	<div class="table-responsive">
                	  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="footable table th-mobwidth">
                        	<thead>
                                <tr>
                                    <th align="left" width="20%" valign="middle">&nbsp;</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Transit Days</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Service</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Liability</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Performance</th>
                                    <th align="left" valign="middle" data-hide="phone,tablet">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
                                        <img alt="" src="{{ asset('images/estes-logo.png') }}"> Estate Express
                                        <br><span class="direct">Direct</span>
                                    </td>
                                    <td align="left" valign="middle">2 days</td>
                                    <td align="left" valign="middle">Standard</td>
                                    <td align="left" valign="middle">New: 5,000 / Used : 2,000</td>
                                    <td align="left" valign="middle"><img alt="" src="{{ asset('images/star.png') }}"></td>
                                    <td align="left" valign="middle" class="rate">
                                    	<div class="hover_div">
                                        	<span class="text">$2580</span>
											<div class="ship_cost">
                           	  				<h3>Shipment Cost</h3>
                                            	<div class="table_cont">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                      <tbody><tr>
                                                        <td align="left" valign="top">Freight</td>
                                                        <td align="right" valign="top">$250.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Fuel</td>
                                                        <td align="right" valign="top">$25.00</td>
                                                  </tr>
                                                      <tr class="red">
                                                        <td align="left" valign="top">Discount</td>
                                                        <td align="right" valign="top">- $10.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                            Net Freight :
                                                            <span>(Freight &ndash; Discount + Fuel)</span>
                                                        </td>
                                                        <td align="right" valign="top">$265.00</td>
                                                  </tr>
                                                      <tr>
                                                        <td align="left" valign="top">Accessorial Description</td>
                                                        <td align="right" valign="top">$30.00</td>
                                                  </tr>
                                                      <tr class="total">
                                                        <td align="left" valign="top">Total Shipment Cost :</td>
                                                        <td align="right" valign="top" class="total_cost">$280.00</td>
                                                  </tr>
                                                </tbody></table>
                                            </div>
                                            </div>
                                      	</div>
                                        <input name="" type="checkbox" value="">
                                        <!--<button type="submit" class="btn btn-xs btn-success">Select</button>-->
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                       
                	</div>
              	</div>
            </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<!--div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div-->
@stop
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/ShipmentManager.js') }}"> </script>
<script>
$("document").ready(function(e) {
    $("#location-colapse").click( function(){
		$(".lc-tarea").slideToggle("slow");
		$("#arrow").toggleClass("fa-chevron-circle-down fa-chevron-circle-up");
		});
		 $("#location-colapse1").click( function(){
		$(".lc-tarea1").slideToggle("slow");
		$("#arrow1").toggleClass("fa-chevron-circle-down fa-chevron-circle-up");
		});
		$("#fedex-con").click(function(){
			$("#fedex").attr('checked', true);
			});
});
     //  function multiselect_change(){		

		
	//	}
</script>
@endsection