					<div class="row load_entry_mid">		  
                 <div class="d_box" style="display:none;" id="d_box@{{$index}}">
                      <div class="mid_box_head clearfix">
                        <h3>Delivery Location</h3>
                        <div class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/open_button.png') }}" alt class="open_button"></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="details_link">Details</a></li>
                            </ul>
                        </div>
                      </div>
                      <div class="mid_box_body">
                        <div class="input-group">
						<input type="text" class="ng-hide" id="shp_deliveryname" name="shp_deliveryname" ng-model="shipment.shp_deliveryname">
                         <angucomplete-alt placeholder="Name Search" id="del_addr" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shp_deliverynameauto" input-changed="getDelInput" initial-value="@{{shipment.shp_deliveryname}}"/>
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                        <div class="form-group">
                          <label>Address line 1</label>
                          <input name="shp_deliveryadr1" id="del_location_adr1" ng-model="shipment.shp_deliveryadr1" type="text" class="form-control" placeholder="" >
                        </div>
                        <div class="form-group">
                          <label>Address line 2</label>
                          <input name="shp_deliveryadr2" id="del_location_adr1" ng-model="shipment.shp_deliveryadr2" type="text" class="form-control" placeholder="">
                        </div>
                        
						<div class="form-group" ng-class="{'has-error': client_shipment.del_location.$invalid}">
                          <label>Location</label>
                          <input type="text" class="form-control" id="del_location" name="del_location" g-places-autocomplete ng-model="shipment.del_location" force-selection="true" options="autocompleteOption" required>
                        </div>
                        
						<div>
						 <a class="btn btn_success cancel" ng-click="closeBox_s('destination', $index);">Cancel</a>&nbsp;
						 <button type="" class="btn btn_success">Save</button>
						</div>

                      </div>
                    </div>							  
						</div>	  
							  