@extends('layouts.shipment_home')
@section('angular_controller')
    ShipmentManagerController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('setup_customer') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  @include('_partials/customer_management_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
<!--form action="javascript:void(0);" name="my_form" ng-submit="savePartialShipmentData(my_form)">
<input type="text" name="uname" ng-model="user.uname">
<button type="submit" ></button>
</form-->
		
         <div class="contract_table" ng-init="login_user_id='{{Auth::user()->id}}';userID='{{Auth::user()->id}}';clientID='{{Auth::user()->clientID}}';login_user_name='{{ Auth::user()->usr_firstname.' '. Auth::user()->usr_lastname}}';shipID ='';isfilter='';">
         <div class="add-new-shipment">
            <div class="row clearfix">
              <div class="col-xs-12 col-sm-7 col-md-7">
                <div class="action-taken">
                        <ul class="clearfix">
                          <li class="dropdown">
                            <button id="dropdropdownMenu2" class="btn btn-email dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Email</button>
                            <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu2">
                              <li><a href="javascript:void(0);" ng-click="emailAction('1')">Email Bill Of Lading</a></li>
                              <li><a href="javascript:void(0);" ng-click="emailAction('1')">Email Shipping Label</a></li>
                              <li><a href="javascript:void(0);" ng-click="emailAction('1')">Email Customer Quote</a></li>
                              <li><a href="javascript:void(0);" ng-click="emailAction('1')">Email Pickup Confirmation</a></li>
                        	</ul></li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu3" class="btn btn-print dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Print</button>
                          <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu3">
                        <li><a href="javascript:void(0);" ng-click="printAction('1')">Print Bill Of Lading</a></li>
                        <li><a href="javascript:void(0);" ng-click="printAction('2')">Print Shipping Label</a></li>
                        <li><a href="javascript:void(0);" ng-click="printAction('3')">Print Customer Quote</a></li>
                        <li><a href="javascript:void(0);" ng-click="printAction('4')">Print Pickup Confirmation</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu4" class="btn btn-action dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Action</button>
                          <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu4">
                        <li><a href="#" ng-click="resolveAction('delete');">Delete Shipment</a></li>
						<li><a href="javascript:void(0);" >Consolidate Order</a></li>
						<li><a href="javascript:void(0);" >Rate Shipment</a></li>
						<li><a href="javascript:void(0);" >Post Loads to Load board</a></li>
                        <!--li><a href="#">Pickup Request</a></li-->
                        <li class="menu-toggle"><a href="#">Change Status</a>
						 <ul class="action_submenu dropdown-menu">
                           <li ng-repeat="shipStatusitem in shipStatus"> <a href="javascript:void(0)" ng-click="resolveAction('update', shipStatusitem.id)"> @{{shipStatusitem.label}} </a> </li>						 
						 </ul>
						</li>
                        <!--li><a href="#">Separated link</a></li-->
                        </ul>
                        </li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu5" class="btn btn-action dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" ><i class="fa"></i>Load View</button>
						 <ul class="action_submenu dropdown-menu">
                           <li> <a href="javascript:void(0)" ng-click="summaryListChange();">
                              Summary View
						   </a> </li>
                           <li> <a href="javascript:void(0)" ng-click="viewAll()">
                             Detail View
						   </a> </li>							   
						 </ul>
                        </li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu6" class="btn btn-action dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" ><i class="fa"></i>Reports</button>
						 <ul class="action_submenu dropdown-menu">
                           <li> <a href="javascript:void(0)" >
                              Download Shipments
						   </a> </li>
                           <li> <a href="javascript:void(0)" >
                             Download Summary
						   </a> </li>							   
						 </ul>
                        </li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu7" class="btn btn-action dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" ><i class="fa"></i>Invoice</button>
						 <ul class="action_submenu dropdown-menu">
                           <li> <a href="/front/shipmentinvoice/invoice" >
                              Customer Billing
						   </a> </li>
                           <li> <a href="javascript:void(0)" >
                             Pay Carrier
						   </a> </li>							   
						 </ul>
                        </li>						
                        </ul>
                      </div>
              </div>
              <div class="col-xs-12 col-sm-5 col-md-5 text-right">
                <div class="btn-group-new">
                  <a href="{{ asset('front/shipment') }}" type="button" class="btn btn-new"><span>New Shipment</span></a>
                  <a href="" type="button" class="btn btn-new"><span>Quote Shipment</span> </a>
                </div>
              </div>
            </div>
          </div> 
         	<div class="grid-view invoice-right co-table co1">
			<div infinite-scroll='loadMore()' infinite-scroll-disabled='isBusy' infinite-scroll-distance='0'>
            <div class="table-responsive">
            <div class="loading hidden" style="margin-left:40%; position:absolute;"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
				<table class="footable table th-mobwidth summarylist" style="display:none;">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th data-hide="phone">Customer Name </th>
                    <th data-hide="phone">Carrier Name </th>
                    <th data-hide="phone">Revenue</th>
                    <th data-hide="phone,tablet">Expense</th>
                    <th data-hide="phone,tablet">Margin/Profit </th>
                    <th data-hide="phone,tablet"> Margin Percentage </th>
                    <th data-hide="phone,tablet">Shipment Count </th>
                  </tr>
                </thead>
                <tbody ng-repeat="shipdata in summaryListData track by $index">
                  <tr>
                    <td><input type="checkbox"></td>
                    <td><a href="javascript:void(0);" ng-click="fetchDataWithClient(shipdata.client_id, shipdata.carrier_id);">@{{shipdata.client_compname}}</a></td>
                    <td>@{{shipdata.carrier_name}}</td>
                    <td>@{{shipdata.payable}}</td>
                    <td>@{{shipdata.receiveablecost}}</td>
                    <td>@{{shipdata.margin}}</td>
                    <td>@{{shipdata.margin_percent}}%</td>
                    <td>@{{shipdata.ship_count}}</td>
                  </tr>
				</table>
			
              <table class="footable table th-mobwidth shiplist">
                <thead>
                  <tr>
                    <th><input type="checkbox" ng-model="shipmentData.selectAll" name="selectAll" ng-click="checkAll()"></th>
					@foreach ($allFields as $items)
                      <th data-hide="">{{$items->displayName}} </th>
					@endforeach
                    <!--th data-hide="phone">Customer Name </th>
                    <th data-hide="phone">Carrier Name </th>
                    <th data-hide="phone">Pro Number </th>
                    <th data-hide="phone,tablet">Pickup Location </th>
                    <th data-hide="phone,tablet">Delivery Location </th>
                    <th data-hide="phone,tablet"> Pickup Date </th>
                    <th data-hide="phone,tablet">Shipment Status </th-->
                    <th>&nbsp; </th>
                  </tr>
                </thead>
	
                <tbody ng-repeat="shipmentData in shipmentData.list" id="conx">
                  <tr >
                    <td><input type="checkbox" ng-click="checkSelection($index)" ng-model="shipmentData.selected"></td>
					@foreach ($allFields as $field)
					  @if($field->displayField == 'shp_shipnumber')
						<td>
							<a href="/front/shipments/edit/@{{shipmentData.id}}">
							 <?php echo "{{ shipmentData.". $field->displayField ."}}"; ?>
							</a>
						</td>
						@elseif($field->displayField == 'shp_loadstatusid')
						<td class="status"><span class="sta"><?php echo "{{ shipmentData.statusname }}"; ?></span></td>
						@elseif($field->displayField == 'shp_pickupstate')
						<td class="pick-info">@{{shipmentData.shp_pickupcity}} , @{{shipmentData.shp_pickupstate}} , @{{shipmentData.shp_pickuppostal}} , @{{shipmentData.shp_pickupcountry}}</td>
						@elseif($field->displayField == 'shp_deliverystate')
						<td class="pick-info">@{{shipmentData.shp_deliverycity}} , @{{shipmentData.shp_deliverystate}} , @{{shipmentData.shp_deliverypostal}} , @{{shipmentData.shp_deliverycountry}}</td>
						@else
						 <td><?php echo "{{ shipmentData.". $field->displayField ."}}"; ?></td>
						@endif 
					@endforeach
                    <!--td><a href="#">@{{shipmentData.client_compname}}</a></td>
                    <td><a href="#">@{{shipmentData.carrier_name}}</a></td>
                    <td>@{{shipmentData.shp_pronumber}}</td>
                    <td class="pick-info">@{{shipmentData.shp_pickupcity}} , @{{shipmentData.shp_pickupstate}} , @{{shipmentData.shp_pickuppostal}} , @{{shipmentData.shp_pickupcountry}}</td>
                    <td class="pick-info">@{{shipmentData.shp_deliverycity}} , @{{shipmentData.shp_deliverystate}} , @{{shipmentData.shp_deliverypostal}} , @{{shipmentData.shp_deliverycountry}}</td>
                    <td>@{{shipmentData.shp_reqpickupdate}}</td>
                    <td class="status"><span class="sta">@{{shipmentData.statusname}}</span></td-->
                      <td>
					  <input type="text" name="ship_id" id="ship_id@{{$index}}" class="ng-hide" value="@{{shipmentData.id}}">
					  <button id="exp@{{$index}}" class="btn btn-toggle btn-xs showArchieved" ng-click="expandInfo($index)"><span class="glyphicon glyphicon-plus" aria-hidden="true" id="ico_@{{$index}}"></span></button></td>
                  </tr>
                  <tr >
                    <td colspan="<?php echo sizeof($allFields)+2; ?>" class="btn-all">
                      <div class="action-open" style="display:none;" id="act@{{$index}}">
                        <div class="row clearfix">
                          <!--div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						  
                            <source-location picklocation="@{{shipmentData.shp_pickupcity}} , @{{shipmentData.shp_pickupstate}} , @{{shipmentData.shp_pickuppostal}} , @{{shipmentData.shp_pickupcountry}}" autofillpick="@{{shipmentData.shp_pickupname}}" id="@{{$index}}" autofilldel="@{{shipmentData.shp_deliveryname}}" dellocation="@{{shipmentData.shp_deliverycity}} , @{{shipmentData.shp_deliverystate}} , @{{shipmentData.shp_deliverypostal}} , @{{shipmentData.shp_deliverycountry}}" elmshipid="@{{shipmentData.id}}" phone="@{{shipmentData.client_phone}}" shppickphone="@{{shipmentData.shp_pickupphone}}" shppickcontact="@{{shipmentData.shp_pickupcontact}}" shpdelphone="@{{shipmentData.shp_deliveryphone}}" shpdelcontact="@{{shipmentData.shp_deliverycontact}}"></source-location>
						  
                            
                          </div-->
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="s_d_loc@{{shipmentData.id}}">
                            <div class="loading hidden"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                          </div>
						  
                          <!--div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <img src="{{ asset('images/map1.jpg') }}" alt="" class="img-responsive" > </div-->
						  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="direction_@{{shipmentData.id}}"> 
                            <div class="loading hidden"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
						  </div>
                        </div>
	   
                        <div class="row clearfix">
						  <div id="loading_shipment@{{shipmentData.id}}" class="loading_shipment hidden tabinfo" style="text-align:center;"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                          <div class="col-xs-12 col-sm-12 col-lg-12" id="direct_in_@{{shipmentData.id}}">
						     	
                          </div>
						 
                        </div>
                       
                      </div>
                      </td>
                  </tr>
 
                </tbody>
				
				<tfoot>
				  <td colspan="<?php echo sizeof($allFields)+2; ?>" style="text-align:center; padding: 15px !important;">
				    <div class="loading_more hidden"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
					<button id="load_more" class="btn" style="border:2px solid #e4e4e4;" href="#" ng-disabled="isBusy"
					  ng-click="loadMore()">Load More Shipment Data</button>
					  
				  </td>
				</tfoot>				
				
              </table>
            </div>
			</div>
          </div>
          <!--div class="text-right">
            <ul class="pagination">
              <li class="disabled"><a aria-label="Previous" href="#"><span aria-hidden="true">«</span></a></li>
              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a aria-label="Next" href="#"><span aria-hidden="true">»</span></a></li>
            </ul>
          </div-->
         </div> 

<!-- /content section-->
<div id="script"></div>
<script type="text/ng-template" id="printmodal.html">
        <div class="modal-header">
            <h3 class="modal-title">@{{items.modal_title}}</h3>
        </div>
        <div class="modal-body">
            
           <table width="100%" cellspacing="0" cellpadding="0">
		    <tr ng-repeat="itemdata in items.seletedShipList" style="padding-bottom:10px; border-bottom:1px solid #e4e4e4;">
			  <td align="left">
			   <strong>Load ID : @{{itemdata.shp_shipnumber}}</strong>
			  </td>
			  <td align="right">
			   <a class="btn btn-warning" href="/front/shipments/ajax-print-shipment/@{{itemdata.id}}/@{{items.modal_title}}" target="_blank">Generate Report & Print</a>
			  </td>			  
			</tr>
		   </table>
			
        </div>
        <div class="modal-footer">
            <!--button class="btn btn-primary" type="button" ng-click="ok()">OK</button-->
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
</script>

<script type="text/ng-template" id="printmodaldelete.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm </h3>
        </div>
        <div class="modal-body">
            
			You want to delete these data ?
			
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="deletedata_remove()">Yes</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">No</button>
        </div>
</script>

<script type="text/ng-template" id="printmodalupdate.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm </h3>
        </div>
        <div class="modal-body">
            
			You want to update these data ?
			
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="updatestatus()">Yes</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">No</button>
        </div>
</script>

<script type="text/ng-template" id="reloadlist.html">
        <div class="modal-body" style="text-align:center;">
            
			Please wait the list is reloading ....
			
        </div>
</script>

@endsection
@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/ShipmentManager.js') }}"> </script>
@endsection