@extends('layouts.shipment_invoice')
@section('angular_controller')
    ShipmentInvoiceManagerController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop

@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  @include('_partials/customer_management_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
<!--form action="javascript:void(0);" name="my_form" ng-submit="savePartialShipmentData(my_form)">
<input type="text" name="uname" ng-model="user.uname">
<button type="submit" ></button>
</form-->
		
            <div class="contract_table">
          <div class="add-new-shipment">
            <div class="row clearfix">
              <div class="col-xs-12 col-sm-7 col-md-7">
              <div class="action-taken">
                        <ul class="clearfix">
                          <li class="dropdown">
                            <button aria-expanded="false" data-toggle="dropdown" type="button" class="btn btn-email dropdown-toggle" id="dropdropdownMenu2"><i class="fa"></i>Email</button>
                            <ul aria-labelledby="dropdropdownMenu2" class="dropdown-menu" role="menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li><a href="#">Separated link</a></li>
                        	</ul></li>
                        <li class="dropdown">
                          <button aria-expanded="false" data-toggle="dropdown" type="button" class="btn btn-print dropdown-toggle" id="dropdropdownMenu3"><i class="fa"></i>Print</button>
                          <ul aria-labelledby="dropdropdownMenu3" class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                          <button aria-expanded="false" data-toggle="dropdown" type="button" class="btn btn-action dropdown-toggle" id="dropdropdownMenu4"><i class="fa"></i>Action</button>
                          <ul aria-labelledby="dropdropdownMenu4" class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                        </ul>
                        </li>
                        </ul>
                      </div>
              </div>
              <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search shipment">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                  </span> </div>
              </div>
            </div>
          </div>
          <div class="grid-view invoice-right co-table">
            <div class="table-responsive">
              <table class="footable table th-mobwidth">
                <thead>
                  <tr>
                    <th align="left" valign="middle"><input type="checkbox"></th>
                    <th align="left" valign="middle" data-hide="">Invoice No</th>
                    <th align="left" valign="middle" data-hide="phone">Customer Name</th>
                    <th align="left" valign="middle" data-hide="phone">Carrier Name </th>
                    <th align="left" valign="middle" data-hide="phone">Pro Number</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Carrier Cost</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Customer Cost</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Margin</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Ship Date</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Shipment Status</th>
                    <th align="left" valign="middle" data-hide="phone,tablet">Sales Rep/User</th>
                    <th align="left" valign="middle">&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td align="left" valign="middle"><input type="checkbox"></td>
                    <td align="left" valign="middle">ADK001</td>
                    <td align="left" valign="middle">Cracker &amp; Barrel</td>
                    <td align="left" valign="middle"><a href="#">FEDEX</a></td>
                    <td align="left" valign="middle"><a href="#">123456</a></td>
                    <td align="left" valign="middle">$785.25</td>
                    <td align="left" valign="middle">$975.45</td>
                    <td align="left" valign="middle">$200.20</td>
                    <td align="left" valign="middle">21/01/2015</td>
                    <td align="left" valign="middle" class="status"><span>Dispatched</span></td>
                    <td align="left" valign="middle">Sanjay Dey</td>
                    <td align="left" valign="middle"><button id="swapArchived" class="btn btn-toggle btn-xs showArchieved"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></td>
                  </tr>
                  <tr>
                    <td colspan="12">
                      <div class="action-open" style="display:none;" id="act1">
                       <div class="first-row">
                        <div class="row">
                              <div class="col-lg-3">
                                  <div class="payment-status">
                                      <h2>Received Payment <span><span class="paid">Paid</span></span></h2>
                                      <p><a href="#">1 payment</a> Received on 01/02/2015</p>
                                  </div>
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Pickup Location</h2>
                                      <div class="content">
                                          <p><strong>Sanjay Dey</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Delivery Location</h2>
                                      <div class="content">
                                          <p><strong>Ilango</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>  
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Stop Off</h2>
                                      <div class="content">
                                          <p><strong>Sanjay Dey</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>
                              </div>
                         </div>
                         
                         </div>
                          <div class="panel panel-primary transportors">
                          	<div class="panel-heading">
                            <h4>Transportors</h4>
                            </div>
                            <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="transport_box">
                                      <h3><img src="images/act-logo.png" alt="" > AAA Cooper</h3>
                                      <div class="table-responsive">
                                          <table id="customerCost1" width="100%" border="0" cellspacing="0" cellpadding="0" class="table transp-table">
                                            <tr>
                                              <th>Charge Type</th>
                                              <th>Quoted Cost</th>
                                              <th>Biled Cost</th>
                                              <th>Approved Cost</th>
                                              <th></th>
                                            </tr>
                                            <tr>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><i class="fa fa-trash-o deleteRow"></i></td>
                                            </tr>
                                            <tr>
                                              <td><input name="" type="text" class="form-control"> </td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><i class="fa fa-trash-o deleteRow"></i></td>
                                            </tr>
                                          </table>
                                      </div>
                                      <div class="addBtnCont">
                                      <button type="button" class="btn btn-primary btn-sm addRowBtn_customer"><span class="plus"></span>Add more</button>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="transport_box">
                                      <h3><img src="images/fedex-logo2.png" alt="" > FedEx</h3>
                                      <div class="table-responsive">
                                          <table id="carrierCost1" width="100%" border="0" cellspacing="0" cellpadding="0" class="table transp-table">
                                            <tr>
                                              <th>Charge Type</th>
                                              <th>Quoted Cost</th>
                                              <th>Biled Cost</th>
                                              <th>Approved Cost</th>
                                              <th></th>
                                            </tr>
                                            <tr>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><i class="fa fa-trash-o deleteRow"></i></td>
                                            </tr>
                                            <tr>
                                              <td><input name="" type="text" class="form-control"> </td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><input name="" type="text" class="form-control"></td>
                                              <td><i class="fa fa-trash-o deleteRow"></i></td>
                                            </tr>
                                          </table>
                                          <div class="addBtnCont">
                                              <button type="button" class="btn btn-primary btn-sm addRowBtn_customer"><span class="plus"></span>Add more</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            </div>
                          </div>
                          <div class="row ">
                            <div class="col-lg-3">
                              <div class="form-group ">
                                <label class="control-label">&nbsp;</label>
                                <label class=" control-label" for="pickupno">Customer Notes- Check Calls</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="control-label" for="pickupno">Internal Notes</label>
                                <label class=" control-label">Dates | Notes | User</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group clearfix">
                                <label class=" control-label">&nbsp;</label>
                                <label class="control-label" for="pickupno">Carrier Notes</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">EDI</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group email-status edi">
                                    <li class="clearfix"><span><a href="#" data-toggle="modal" data-target="#myModal">View</a> - 204 (Pickup Requested)</span> <span class="status-abt yes">Y</span> </li>
                                    <li class="clearfix"><span>Generate New 204</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"><span><a href="#" data-toggle="modal" data-target="#myModal">View</a> - 201 (Invoice)</span> <span class="status-abt no">N</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row boardStatus-cont">
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Load Board Statistics</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group boardstatus-hld">
                                    <li class="clearfix"> <strong class="satus-label">Invoice Status:</strong> <span class="status-abt sent">Sent</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Payment Status:</strong> <span class="status-abt due">Due</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Email Statistics</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group email-status">
                                    <li class="clearfix"> <strong class="satus-label">Bill of Landing:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Carrier Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Customer Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Customer Invoice:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">User Audit Trail</h3>
                                </div>
                                <div class="panel-body">
                                  <div class="check_calls small">
                                    <div class="check_calls_inner no-bg">
                                      <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Scan Document &amp; View Document</h3>
                                </div>
                                <div class="panel-body">
                                  <div class="form-group doc-uploadhld">
                                    <div class="inputuploadhld">
                                      <input id="" type="file">
                                      <button  type="button" class="btn btn-default">Upload File</button>
                                    </div>
                                  </div>
                                  <ul class="list-group file-type-hld">
                                    <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Bill of Landing</span>
                                    <a href="#"><img src="images/delete-icon.png" alt="" /></a></li>
                                    <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Proof of Delivery</span>
                                    <a href="#"><img src="images/delete-icon.png" alt="" /></a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="btn-cont">
                              <button  type="button" class="btn btn-sm btn-default">Cancel</button>
                              <button  type="button" class="btn btn-sm btn-primary">Save / Update</button>
                          </div>
                      </div>
                      </td>
                  </tr>
                  <tr>
                    <td align="left" valign="middle"><input type="checkbox"></td>
                    <td align="left" valign="middle">ADK001</td>
                    <td align="left" valign="middle">Cracker &amp; Barrel</td>
                    <td align="left" valign="middle"><a href="#">FEDEX</a></td>
                    <td align="left" valign="middle"><a href="#">123456</a></td>
                    <td align="left" valign="middle">$785.25</td>
                    <td align="left" valign="middle">$975.45</td>
                    <td align="left" valign="middle">$200.20</td>
                    <td align="left" valign="middle">21/01/2015</td>
                    <td align="left" valign="middle" class="status"><span>Dispatched</span></td>
                    <td align="left" valign="middle">Sanjay Dey</td>
                    <td align="left" valign="middle"><button id="swapArchived2" class="btn btn-toggle btn-xs showArchieved"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></td>
                  </tr>
                  <tr>
                    <td colspan="12">
                      <div class="action-open" style="display:none;" id="act2">
                       <div class="first-row">
                        <div class="row">
                              <div class="col-lg-3">
                                  <div class="payment-status">
                                      <h2>Received Payment <span><span class="paid">Paid</span></span></h2>
                                      <p><a href="#">1 payment</a> Received on 01/02/2015</p>
                                  </div>
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Pickup Location</h2>
                                      <div class="content">
                                          <p><strong>Sanjay Dey</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Delivery Location</h2>
                                      <div class="content">
                                          <p><strong>Ilango</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>  
                              </div>
                              <div class="col-lg-3">
                                  <div class="box-round">
                                      <h2>Stop Off</h2>
                                      <div class="content">
                                          <p><strong>Sanjay Dey</strong>
                                          P 73 Niva Park Extension<br>
                                          Bramhapur<br>
                                          Kolkata, West Bengal<br>
                                          700096<br>
                                          India</p>
                                      </div>
                                  </div>
                              </div>
                         </div>
                         </div>
                          <div class="panel panel-primary transportors">
                          	<div class="panel-heading">
                            <h4>Transportors</h4>
                            </div>
                            <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="transport_box">
                                      <h3><img src="images/act-logo.png" alt="" > AAA Cooper</h3>
                                      <div class="table-responsive">
                                          <table id="customerCost1" width="100%" border="0" cellspacing="0" cellpadding="0" class="table transp-table">
                                            <tr>
                                              <th>Charge Type</th>
                                              <th>Quoted Cost</th>
                                              <th>Biled Cost</th>
                                              <th>Approved Cost</th>
                                              <th></th>
                                            </tr>
                                            <tr>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                            </tr>
                                            <tr>
                                              <td><input type="text" class="form-control" > </td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                            </tr>
                                          </table>
                                      </div>
                                      <div class="addBtnCont">
                                      <button type="button" class="btn btn-primary addRowBtn_customer"><span class="plus"></span>Add more</button>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="transport_box">
                                      <h3><img src="images/fedex-logo2.png" alt="" > FedEx</h3>
                                      <div class="table-responsive">
                                          <table id="carrierCost1" width="100%" border="0" cellspacing="0" cellpadding="0" class="table transp-table">
                                            <tr>
                                              <th>Charge Type</th>
                                              <th>Quoted Cost</th>
                                              <th>Biled Cost</th>
                                              <th>Approved Cost</th>
                                              <th></th>
                                            </tr>
                                            <tr>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                               <td><input type="text" class="form-control" ></td>
                                              <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                            </tr>
                                            <tr>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><input type="text" class="form-control" ></td>
                                              <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                            </tr>
                                          </table>
                                          <div class="addBtnCont">
                                              <button type="button" class="btn btn-primary addRowBtn_customer"><span class="plus"></span>Add more</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            </div>
                          </div>
                          <div class="row ">
                            <div class="col-lg-3">
                              <div class="form-group ">
                                <label class="control-label">&nbsp;</label>
                                <label class=" control-label" for="pickupno">Customer Notes- Check Calls</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="control-label" for="pickupno">Internal Notes</label>
                                <label class=" control-label">Dates | Notes | User</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group clearfix">
                                <label class=" control-label">&nbsp;</label>
                                <label class="control-label" for="pickupno">Carrier Notes</label>
                                <div class="check_calls">
                                  <div class="check_calls_inner">
                                    <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                  </div>
                                  <div class="check_calls_textarea">
                                    <textarea rows="3" class="form-control">Add Note</textarea>
                                    <button class="btn btn-default" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">EDI</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group email-status edi">
                                    <li class="clearfix"><span><a href="#" data-toggle="modal" data-target="#myModal">View</a> - 204 (Pickup Requested)</span> <span class="status-abt yes">Y</span> </li>
                                    <li class="clearfix"><span>Generate New 204</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"><span><a href="#" data-toggle="modal" data-target="#myModal">View</a> - 201 (Invoice)</span> <span class="status-abt no">N</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row boardStatus-cont">
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Load Board Statistics</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group boardstatus-hld">
                                    <li class="clearfix"> <strong class="satus-label">Invoice Status:</strong> <span class="status-abt sent">Sent</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Payment Status:</strong> <span class="status-abt due">Due</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Email Statistics</h3>
                                </div>
                                <div class="panel-body">
                                  <ul class="list-group email-status">
                                    <li class="clearfix"> <strong class="satus-label">Bill of Landing:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Carrier Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Customer Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                    <li class="clearfix"> <strong class="satus-label">Customer Invoice:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">User Audit Trail</h3>
                                </div>
                                <div class="panel-body">
                                  <div class="check_calls small">
                                    <div class="check_calls_inner no-bg">
                                      <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                      <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Scan Document &amp; View Document</h3>
                                </div>
                                <div class="panel-body">
                                  <div class="form-group doc-uploadhld">
                                    <div class="inputuploadhld">
                                      <input id="" type="file">
                                      <button  type="button" class="btn btn-default">Upload File</button>
                                    </div>
                                  </div>
                                  <ul class="list-group file-type-hld">
                                    <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Bill of Landing</span>
                                    <a href="#"><img src="images/delete-icon.png" alt="" /></a></li>
                                    <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Proof of Delivery</span>
                                    <a href="#"><img src="images/delete-icon.png" alt="" /></a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="btn-cont">
                              <button  type="button" class="btn btn-sm btn-default">Cancel</button>
                              <button  type="button" class="btn btn-sm btn-primary">Save / Update</button>
                          </div>
                      </div>
                      </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
                	<div class="col-md-2 col-sm-4">
                    	<div class="form-group">
                            <select class="form-control">
                              <option>3 per page</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-8">
                    <ul class="pagination pull-right">
                        <li>
                          <a href="#" aria-label="Previous">
                            <span aria-hidden="true">Previous</span>
                          </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                          <a href="#" aria-label="Next">
                            <span aria-hidden="true">Next</span>
                          </a>
                        </li>
                      </ul>
                      <p class="record-data">Showing Record 3 of 19</p>
                      
                    </div>
                </div>
          </div>


@endsection
@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/ShipmentInvoiceManager.js') }}"> </script>
@endsection