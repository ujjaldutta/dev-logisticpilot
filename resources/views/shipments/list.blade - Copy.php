@extends('layouts.shipment_home')
@section('angular_controller')
    ShipmentManagerController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('setup_customer') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  @include('_partials/customer_management_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
<!--form action="javascript:void(0);" name="my_form" ng-submit="savePartialShipmentData(my_form)">
<input type="text" name="uname" ng-model="user.uname">
<button type="submit" ></button>
</form-->
		  <form action="javascript:void(0);" name="form_input" >
         <div class="contract_table" ng-init="login_user_id='{{Auth::user()->id}}';login_user_name='{{ Auth::user()->usr_firstname.' '. Auth::user()->usr_lastname}}';shipID =''">
         <div class="add-new-shipment">
            <div class="row clearfix">
              <div class="col-xs-12 col-sm-7 col-md-7">
                <div class="action-taken">
                        <ul class="clearfix">
                          <li class="dropdown">
                            <button id="dropdropdownMenu2" class="btn btn-email dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Email</button>
                            <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu2">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li><a href="#">Separated link</a></li>
                        	</ul></li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu3" class="btn btn-print dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Print</button>
                          <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu3">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                          <button id="dropdropdownMenu4" class="btn btn-action dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa"></i>Action</button>
                          <ul role="menu" class="dropdown-menu" aria-labelledby="dropdropdownMenu4">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                        </ul>
                        </li>
                        </ul>
                      </div>
              </div>
              <div class="col-xs-12 col-sm-5 col-md-5 text-right">
                <div class="btn-group-new">
                  <a href="{{ asset('front/shipment') }}" type="button" class="btn btn-new"><span>New Shipment</span></a>
                  <a href="" type="button" class="btn btn-new"><span>Quote Shipment</span> </a>
                </div>
              </div>
            </div>
          </div> 
         	<div class="grid-view invoice-right co-table co1">
            <div class="table-responsive">

              <table class="footable table th-mobwidth">
                <thead>
                  <tr>
                    <th><input type="checkbox" ng-model="shipmentData.selectAll" name="selectAll" ng-click="checkAll()"></th>
                    <th data-hide="">Load no </th>
                    <th data-hide="phone">Customer Name </th>
                    <th data-hide="phone">Carrier Name </th>
                    <th data-hide="phone">Pro Name </th>
                    <th data-hide="phone,tablet">Pickup Location </th>
                    <th data-hide="phone,tablet">Delivery Location </th>
                    <th data-hide="phone,tablet"> Pickup Date </th>
                    <th data-hide="phone,tablet">Shipment Status </th>
                     <th>&nbsp; </th>
                  </tr>
                </thead>
                <tbody ng-repeat="shipmentData in shipmentData.list">
                  <tr >
                    <td><input type="checkbox" ng-click="checkSelection($index)" ng-model="shipmentData.selected"></td>
                    <td><a href="/front/shipments/edit/@{{shipmentData.id}}" target="_blank">@{{shipmentData.shp_shipnumber}}</a></td>
                    <td><a href="#">@{{shipmentData.customers.client_firstname}} @{{shipmentData.customers.client_lastname}}</a></td>
                    <td><a href="#">1234567890</a></td>
                    <td>@{{shipmentData.shp_pronumber}}</td>
                    <td class="pick-info">@{{shipmentData.shp_pickupcity}} , @{{shipmentData.shp_pickupstate}} , @{{shipmentData.shp_pickuppostal}} , @{{shipmentData.shp_pickupcountry}}</td>
                    <td class="pick-info">@{{shipmentData.shp_deliverycity}} , @{{shipmentData.shp_deliverystate}} , @{{shipmentData.shp_deliverypostal}} , @{{shipmentData.shp_deliverycountry}}</td>
                    <td>@{{shipmentData.shp_reqpickupdate}}</td>
                    <td class="status"><span class="sta">Pickup Requested</span></td>
                      <td>
					  <input type="text" name="ship_id" id="ship_id@{{$index}}" class="ng-hide" value="@{{shipmentData.id}}">
					  <button id="exp@{{$index}}" class="btn btn-toggle btn-xs showArchieved" ng-click="expandInfo($index)"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></td>
                  </tr>
                  <tr >
                    <td colspan="10" class="btn-all">
                      <div class="action-open" style="display:none;" id="act@{{$index}}">
                        <div class="row clearfix">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="address">
                              <h3>@{{shipmentData.shp_pickupname}} <span>Origin Location</span></h3>
                              <p><strong>Address:</strong> @{{shipmentData.shp_pickupcity}} , @{{shipmentData.shp_pickupstate}} , @{{shipmentData.shp_pickuppostal}} , @{{shipmentData.shp_pickupcountry}}<br>
                                <strong>Phone: </strong><a href="tel:+91 9007320481">@{{shipmentData.customers.client_phone}}</a><br>
                                <strong>Fax:</strong> +91 33 24100422</p>
                              <img src="{{ asset('images/drop-arrow.png') }}" alt=""> </div>
                            <div class="address destination">
                              <h3>@{{shipmentData.shp_deliveryname}} <span>Destination Location</span></h3>
                              <p><strong>Address:</strong> @{{shipmentData.shp_deliverycity}} , @{{shipmentData.shp_deliverystate}} , @{{shipmentData.shp_deliverypostal}} , @{{shipmentData.shp_deliverycountry}}<br>
                                <strong>Phone: </strong><a href="tel:+91 9007320481">@{{shipmentData.customers.client_phone}}</a><br>
                                <strong>Fax:</strong> +91 33 24100422</p>
                            </div>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <img src="{{ asset('images/map1.jpg') }}" alt="" class="img-responsive" > </div>
                        </div>

                       <div class="loading hidden pull-right tabinfo"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>					   
                        <div class="row clearfix">
                          <div class="col-xs-12 col-sm-12 col-lg-12">
	
						  <ng-form name="partial_data" >
                            <div id="horizontalTab@{{$index}}" class="tab-hld">
                              <ul class="resp-tabs-list">
                                <li>Shipment</li>
                                <li>Carrier Communication</li>
                                <li>Cost and Commission</li>
                                <li>Notes</li>
                              </ul>
                              <div class="resp-tabs-container">
                                <div class="tab-content">
                                 <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix">
                                        <label for="pickupno" class="control-label">Purchase Order Number</label>
                                        <input name="shp_sonumber[]" ng-model="shipmentData.shp_sonumber" type="text" class="form-control" kl_virtual_keyboard_secure_input="on" >
                                      </div>
                                      <div class="form-group clearfix">
                                        <label for="pickupno" class="control-label">Pro Number</label>
                                        <input type="text" name="shp_pronumber" ng-model="shipmentData.shp_pronumber" class="form-control" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Expected Pickup Date</label>
                                        <input type="text" id="datepicker7" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Expected Delivery Date</label>
                                        <input type="text" id="datepicker8" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Pickup Date</label>
                                        <input type="text" name="shp_reqpickupdate[]" ng-model="shipmentData.shp_reqpickupdate" id="datepicker_ship1@{{shipmentData.id}}" class="form-control jqdatepicker">
                                      </div>
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Delivered Date</label>
                                        <input type="text" name="shp_expdeliverydate[]" ng-model="shipmentData.shp_expdeliverydate" id="datepicker_ship2@{{shipmentData.id}}" class="form-control jqdatepicker" >
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="row clearfix">
                                        <div class="col-md-6 col-xs-6">
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Open Time</label>
                                              <div class="bootstrap-timepicker">
                                                <!--timepicker ng-model="shipment.shp_pickuptimefrom" show-meridian="true" value="Tue, 23 Feb 2016 13:28
:20 +0000"></timepicker-->
                                                 <input type="text" name="shp_pickuptimefrom" ng-model="shipmentData.shp_pickuptimefrom" class="form-control time-box timepicker-box jqdatetimepicker" id="timepicker@{{$index}}2" kl_virtual_keyboard_secure_input="on">
                                              </div>
                                          </div>
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Open Time</label>
                                              <div class="bootstrap-timepicker">
											  <!--timepicker ng-model="shipment.shp_deliverytimefrom" show-meridian="true"></timepicker-->
                                                <input type="text" name="shp_pickuptimeto" ng-model="shipmentData.shp_pickuptimeto" class="form-control time-box timepicker timepicker-box jqdatetimepicker" id="timepicker@{{$index}}2" kl_virtual_keyboard_secure_input="on">
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Close Time</label>
                                              <div class="bootstrap-timepicker">
                                                  <input type="text" name="shp_deliverytimefrom[]" ng-model="shipmentData.shp_deliverytimefrom" class="form-control time-box timepicker timepicker-box jqdatetimepicker" id="timepicker@{{$index}}3" kl_virtual_keyboard_secure_input="on">
												  <!--timepicker ng-model="shipment.shp_pickuptimeto" show-meridian="true"></timepicker-->
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Close Time</label>
                                              <div class="bootstrap-timepicker">
                                                  <input type="text" name="shp_deliverytimeto" ng-model="shipmentData.shp_deliverytimeto" class="form-control time-box timepicker timepicker-box jqdatetimepicker" id="timepicker@{{$index}}4" kl_virtual_keyboard_secure_input="on">
												  <!--timepicker ng-model="shipment.shp_deliverytimeto" show-meridian="true"></timepicker-->
                                              </div>
                                          </div>
                                        </div>										
                                      </div>
                                    </div>
                                    
                                  </div>
                                  <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix">
                                        <label for="pickupno" class="control-label">Carrier Quote number</label>
                                        <input type="text" name="shp_quotenumber" ng-model="shipmentData.shp_quotenumber" class="form-control" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                      <div class="form-group clearfix">
                                        <label for="pickupno" class="control-label">Pro Number</label>
                                        <input type="text" class="form-control" kl_virtual_keyboard_secure_input="on">
                                      </div>
									  
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix ">
                                        <label for="pickupno" class="control-label">Billable weight</label>
                                        <input type="text" name="shp_shipmentvalue" ng-model="shipmentData.shp_shipmentvalue" class="form-control " kl_virtual_keyboard_secure_input="on">
                                      </div>
                                      <div class="form-group clearfix ">
                                        <label for="pickupno" class="control-label">Shipment value</label>
                                        <input type="text" name="shp_valueperpound" ng-model="shipmentData.shp_valueperpound" class="form-control " kl_virtual_keyboard_secure_input="on">
                                      </div>									


                                    </div>	
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix">
                                        <label for="pickupno" class="control-label">Cost per pound</label>
                                        <input type="text" name="shp_billweight" ng-model="shipmentData.shp_billweight" class="form-control " kl_virtual_keyboard_secure_input="on">
                                      </div>

                                    </div>
									
                                    <!--div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Expected Pickup Date</label>
                                        <input type="text" id="datepicker7" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Expected Delivery Date</label>
                                        <input type="text" id="datepicker8" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Pickup Date</label>
                                        <input type="text" id="datepicker9" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                      <div class="form-group clearfix date-pick">
                                        <label for="pickupno" class="control-label">Delivered Date</label>
                                        <input type="text" id="datepicker10" class="form-control hasDatepicker" kl_virtual_keyboard_secure_input="on">
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="row clearfix">
                                        <div class="col-md-6 col-xs-6">
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Open Time</label>
                                              <div class="bootstrap-timepicker">
                                                <input type="text" class="form-control time-box" id="timepicker1" kl_virtual_keyboard_secure_input="on">
                                              </div>
                                          </div>
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Delivery Time</label>
                                              <div class="bootstrap-timepicker">
                                                <input type="text" class="form-control time-box" id="timepicker2" kl_virtual_keyboard_secure_input="on">
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                          <div class="form-group clearfix">
                                            <label for="pickupno" class="control-label">Close Time</label>
                                              <div class="bootstrap-timepicker">
                                                  <input type="text" class="form-control time-box" id="timepicker3" kl_virtual_keyboard_secure_input="on">
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div-->
                                    
                                  </div>
								  
                                  <div class="row clearfix boardStatus-cont">
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <h3 class="panel-title">Load Board Statistics</h3>
                                        </div>
                                        <div class="panel-body">
                                          <ul class="list-group boardstatus-hld">
                                            <li class="clearfix"> <strong class="satus-label">Invoice Status:</strong> <span class="status-abt sent">Sent</span> </li>
                                            <li class="clearfix"> <strong class="satus-label">Payment Status:</strong> <span class="status-abt due">Due</span> </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <h3 class="panel-title">Email Statistics</h3>
                                        </div>
                                        <div class="panel-body">
                                          <ul class="list-group email-status">
                                            <li class="clearfix"> <strong class="satus-label">Bill of Landing:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                            <li class="clearfix"> <strong class="satus-label">Carrier Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                            <li class="clearfix"> <strong class="satus-label">Customer Quote:</strong> <span>Email Status</span> <span class="status-abt no">N</span> </li>
                                            <li class="clearfix"> <strong class="satus-label">Customer Invoice:</strong> <span>Email Status</span> <span class="status-abt yes">Y</span> </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <h3 class="panel-title">User Audit Trail</h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="check_calls small">
                                            <div class="check_calls_inner no-bg">
                                              <p><span><strong>Sanjay Dey</strong> 8 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                              <p><span><strong>Sanjay Dey</strong> 7 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                              <p><span><strong>Sanjay Dey</strong> 6 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                              <p><span><strong>Sanjay Dey</strong> 5 days ago<i class="fa fa-trash-o pull-right"></i></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus varius malesuada. Curabitur sapien magna, ornare et justo sollicitudin, mollis scelerisque arcu.</p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-lg-3">
                                      <div class="panel panel-primary">
                                        <div class="panel-heading">
                                          <h3 class="panel-title">Scan Document &amp; View Document</h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group doc-uploadhld">
                                            <div class="inputuploadhld">
                                              <input id="" type="file">
                                              <button  type="button" class="btn btn-default">Upload File</button>
                                            </div>
                                          </div>
                                          <ul class="list-group file-type-hld">
                                            <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Bill of Landing</span>
                                            <a href="#"><img src="{{ asset('images/delete-icon.png') }}" alt="" /></a></li>
                                            <li class=""> <span class="file-type-icon"><i class="fa fa-file-text-o"></i></span> <span>Proof of Delivery</span>
                                            <a href="#"><img src="{{ asset('images/delete-icon.png') }}" alt="" /></a></li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="tab-content">
                                  <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                      <div class="panel panel-primary career-responded">
                                        <div class="panel-heading">
                                          <h3 class="panel-title">Carrier Responded for SPOT Quote</h3>
                                        </div>
                                        <div class="panel-body">
                                          <h4 class="sky-blue">JOHN TRUCK</h4>
                                          <ul class="list-group email-status">
                                            <li class="blue"><strong class="email-count">12</strong> <span>Total Carriers</span></li>
                                            <li class="green"><strong class="email-count">7</strong> <span>Responded</span></li>
                                            <li class="red"><strong class="email-count">3</strong> <span>Not Responded</span></li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-lg-8">
                                      <div class="table-responsive">
                                        <div class="panel panel-default career-table">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover table-bordered">
                                            <thead>
                                              <tr>
                                                <th>Carrier Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Cost</th>
                                                <th></th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status accept">Accept</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status denied">Denied</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status pending">Pending</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status accept">Accept</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status denied">Denied</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                              <tr>
                                                <td>John Truck</td>
                                                <td><a href="#">deysanjay74@gmail.com</a></td>
                                                <td><span class="status pending">Pending</span></td>
                                                <td>$ 3200</td>
                                                <td><div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                                    </ul>
                                                  </div></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="tab-content">
								 
                                  <div class="row clearfix">
								  <parent-dir>
                                    <div class="col-xs-12 col-sm-6 col-lg-6">
                                      <div class="panel panel-default customer-cost">
                                        <div class="panel-heading">
                                          <div class="row clearfix">
                                            <div class="col-xs-12 col-sm-6 col-lg-7">
                                              <div class="cost-seeting">
                                                <p>Customer Cost $ 120.25</p>
                                              </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-lg-5">
                                              <div class="cost-seeting"> <a href="#">Rate Customer Cost</a> </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="ash-con" id="customer_cost_@{{shipmentData.id}}">
										  <div class="loading"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
										 <!--customercost></customercost-->
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-lg-6">
                                      <div class="panel panel-default career-cost">
                                        <div class="panel-heading">
                                          <div class="row clearfix">
                                            <div class="col-xs-12 col-sm-6 col-lg-7">
                                              <div class="cost-seeting">
                                                <p>Carrier Cost : $350</p>
                                              </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-lg-5">
                                              <div class="cost-seeting"> <a href="#">Rate Carrier Cost</a> </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="ash-con" id="carrier_cost_@{{shipmentData.id}}">
										 <div class="loading"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
 										 <!--carriercost></carriercost-->
                                        </div>
                                        </div>
                                    </div>
									</parent-dir>
                                  </div>
                                  
                                </div>
                                <div class="tab-content">
                                  <div class="row clearfix">
								  <parent-dir>
                                    <div class="col-xs-12 col-sm-4 col-lg-4" id="customer_note_@{{shipmentData.id}}">
									 <div class="loading"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                                     <!--customernotes></customernotes-->
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4" id="carrier_note_@{{shipmentData.id}}">
									 <div class="loading"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                                     <!--carriernotes></carriernotes--> 
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4" id="terminal_note_@{{shipmentData.id}}">
									 <div class="loading"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
                                     <!--terminalnotes></terminalnotes--> 
                                    </div>
								  </parent-dir>
                                  </div>
                                </div>
                              </div>
                              <div class="btn-cont">
                                <button  type="button" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary" ng-click="savePartialShipmentData(form_input)">Save / Update</button>
                              </div>
                              <!-- Debanjan End for tab statistics ------------------>
							  
                            </div>

							</ng-form>
                          </div>
						 
                        </div>
                       
                      </div>
                      </td>
                  </tr>
 
                </tbody>
				
				<tfoot>
				  <td colspan="12" style="text-align:center; padding: 15px !important;">
				    <div class="loading hidden"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
					<button id="load_more" class="btn" style="border:2px solid #e4e4e4;" href="#" ng-class="nextPageDisabledClass()"
					  ng-click="loadMore()">Load More Shipment Data</button>
				  </td>
				</tfoot>				
				
              </table>
            </div>
          </div>
          <!--div class="text-right">
            <ul class="pagination">
              <li class="disabled"><a aria-label="Previous" href="#"><span aria-hidden="true">«</span></a></li>
              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a aria-label="Next" href="#"><span aria-hidden="true">»</span></a></li>
            </ul>
          </div-->
         </div> 

 </form>
<!-- /content section-->
<div id="script"></div>
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/ShipmentManager.js') }}"> </script>
@endsection