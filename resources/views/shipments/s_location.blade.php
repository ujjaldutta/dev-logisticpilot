<div class="address source" >

<div class="row load_entry_mid" id="source@{{id}}" style="display:none; padding:15px;">
  <form action="javascript:void(0);" name="form_input" >
                    <div class="s_box" >
                      <div class="mid_box_head clearfix">
                        <h3>{{ Lang::get('listshipment.originbox_heading') }}</h3>

                      </div>
                      <div class="mid_box_body">
                        <div class="input-group">
						 <div style="text-align:left;">Name</div>
						  <input type="text" class="ng-hide" id="shp_pickupname@{{id}}" name="shp_pickupname" ng-model="location.shp_pickupname">
                          <angucomplete-alt placeholder="Name Search" id="pickup_addr" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control pickupautobox" match-class="highlight" input-name="shp_pickupnameauto" input-changed="getPickInput" initial-value="@{{autofillpick}}" input-width="400"/>
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
						<div style="text-align:left;">Address</div>
                        <div class="form-group">
                          <!--label>Address line 1</label-->
                          <input name="shp_pickupadr1" id="pick_location_adr1" ng-model="location.shp_pickupadr1" type="text" class="form-control" value="123" placeholder="Address line 1">
                        </div>
                        <div class="form-group">
                          <!--label>Address line 2</label-->
                          <input name="shp_pickupadr2" id="pick_location_adr2" ng-model="location.shp_pickupadr2" type="text" class="form-control" placeholder="Address line 2">
                        </div>
						<div class="form-group" ng-class="{'has-error': form_input.pick_location.$invalid}">
                          <!--label>Location</label-->
						  <div style="text-align:left;">Location</div>
                          <input id="pick_location@{{id}}" type="text" class="form-control" name="pick_location" g-places-autocomplete ng-model="location.pick_location" force-selection="true" options="autocompleteOption" placeholder="Location" required>
                        </div>
                        <div class="form-group">
                          <!--label>Pickup Phone</label-->
						  <div style="text-align:left;">Contact Number</div>
                          <input name="shp_pickupphone" id="shp_pickupphone@{{id}}" ng-model="location.shp_pickupphone" type="text" class="form-control" value="" placeholder="Pickup Phone" style="width:165px;">
                        </div>
                        <div class="form-group">
                          <!--label>Pickup Contact</label-->
						  <div style="text-align:left;">Contact Name</div>
                          <input name="shp_pickupcontact" id="shp_pickupcontact@{{id}}" ng-model="location.shp_pickupcontact" type="text" class="form-control" value="" placeholder="Pickup Contact" style="width:165px;">
                        </div>                        
						
						<div>
						<i>*Location is a Mandetary field</i><br />
						 <button class="btn btn_success btn-default" ng-click="closeBox_s('source', id);">Cancel</button>&nbsp;
						 <button class="btn btn_success btn-primary s_save" ng-disabled="form_input.$invalid" ng-click="saveSource(form_input);">Save</button>
						<div class="loading hidden s_loading" style="text-align:center;"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
						</div>
                      </div>
                    </div>
					</form>
                  		</div>
   <div id="s_box@{{id}}">
	<div class="edit_box"><a href="javascript:void(0);" ng-click="openBox_s('source', id);"><i class="fa fa-pencil-square-o"></i></a></div>
	<h3>@{{location.shp_pickupname}} <span>{{ Lang::get('listshipment.originbox_heading') }}</span></h3>
	<p><strong>Address:</strong> @{{picklocation}} ,<br>
	<strong>Phone: </strong><a href="tel:@{{shppickphone}}">@{{shppickphone}}</a><br>
	<strong>Contact Name:</strong> @{{shppickcontact}}</p>
	<img src="{{ asset('images/drop-arrow.png') }}" alt=""> 
	</div>
</div>



<div class="address destination">
<div class="row load_entry_mid" id="destination@{{id}}" style="display:none; padding:15px;">	
<form action="javascript:void(0);" name="form_input_d" >	  
                 <div class="d_box" >
                      <div class="mid_box_head clearfix">
                        <h3>{{ Lang::get('listshipment.destinationbox_heading') }}</h3>

                      </div>
                      <div class="mid_box_body">
                        <div class="input-group">
						<div style="text-align:left;">Name</div>
						<input type="text" class="ng-hide" id="shp_deliveryname@{{id}}" name="shp_deliveryname" ng-model="location.shp_deliveryname">
                         <angucomplete-alt placeholder="Name Search" id="del_addr" pause="200" selected-object="locationSelected" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control pickupautobox" match-class="highlight" input-name="shp_deliverynameauto" input-changed="getDelInput" initial-value="@{{autofilldel}}" style="width:240px;"/>
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
						<div style="text-align:left;">Address</div>
                        <div class="form-group">
                          <!--label>Address line 1</label-->
                          <input name="shp_deliveryadr1" id="del_location_adr1" ng-model="location.shp_deliveryadr1" type="text" class="form-control" placeholder="Address line 1" >
                        </div>
                        <div class="form-group">
                          <!--label>Address line 2</label-->
                          <input name="shp_deliveryadr2" id="del_location_adr1" ng-model="location.shp_deliveryadr2" type="text" class="form-control" placeholder="Address line 2">
                        </div>
                        
						<div class="form-group" ng-class="{'has-error': form_input_d.del_location.$invalid}">
                          <!--label>Location</label-->
						  <div style="text-align:left;">Location</div>
                          <input type="text" class="form-control" id="del_location@{{id}}" name="del_location" g-places-autocomplete ng-model="location.del_location" force-selection="true" options="autocompleteOption" placeholder="Location" required>
                        </div>
                        
                        <div class="form-group">
                          <!--label>Delivery Phone</label-->
						  <div style="text-align:left;">Contact Number</div>
                          <input name="shp_deliveryphone" id="shp_deliveryphone@{{id}}" ng-model="location.shp_deliveryphone" type="text" class="form-control" value="" placeholder="Delivery Phone" style="width:165px;">
                        </div>
                        <div class="form-group">
                          <!--label>Delivery Contact</label-->
						  <div style="text-align:left;">Contact Name</div>
                          <input name="shp_deliverycontact" id="shp_deliverycontact@{{id}}" ng-model="location.shp_deliverycontact" type="text" class="form-control" value="" placeholder="Delivery Contact" style="width:165px;">
                        </div>							
						
						
						<div>
						 <i>*Location is a Mandetary field</i><br />
						 <button class="btn btn_success btn-default" ng-click="closeBox_d('destination', id);">Cancel</button>&nbsp;
						 <button type="" class="btn btn_success btn-primary d_save" ng-disabled="form_input_d.$invalid" ng-click="saveDestination(form_input_d)">Save</button>
						 <div class="loading hidden d_loading" style="text-align:center;"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
						</div>

                      </div>
					  </form>
                    </div>							  
						</div>
<div id="d_box@{{id}}">						
	<div class="edit_box"><a href="javascript:void(0);" ng-click="openBox_d('destination', id);"><i class="fa fa-pencil-square-o"></i></a></div>
	<h3>@{{location.shp_deliveryname}} <span>{{ Lang::get('listshipment.destinationbox_heading') }}</span></h3>
	<p><strong>Address:</strong> @{{dellocation}} , <br>
	<strong>Phone: </strong><a href="tel:@{{shpdelphone}}">@{{shpdelphone}}</a><br>
	<strong>Contact Name:</strong> @{{shpdelcontact}}</p>
	</div>
</div>
