@extends('layouts.user_home')
@section('angular_controller')
CustomerLocationEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('add_customer_location') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/customer_location_management_sidebar')
@stop
@section('top_content') 

@stop
@section('content')
<!-- content section-->
<section class="content-wrapper" ng-init="locationId ='{{$id}}'; custID={{CustomerRepository::getCurrentCustomerID()}}">
    <form name="clientLocationForm" novalidate>
        <div class="contract_table">
            <h2>Location Setup</h2>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_name.$invalid}">
                        <label>Location name</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientLocationData.location_name" name="location_name" required>
                        <span class="help-block" ng-show="clientLocationForm.location_name.$invalid">
                            <span ng-show="clientLocationForm.location_name.$error.required">Location Name is required</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_adr1.$invalid}">
                        <label>Location Address Line 1</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientLocationData.location_adr1" name="location_adr1" required>
                        <span class="help-block" ng-show="clientLocationForm.location_adr1.$invalid">
                            <span ng-show="clientLocationForm.location_adr1.$error.required">Location Address Line 1 is required</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_adr2.$invalid}">
                        <label>Location Address Line 2</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientLocationData.location_adr2" name="location_adr2">
                        <span class="help-block" ng-show="clientLocationForm.location_adr2.$invalid">
                            <span ng-show="clientLocationForm.location_adr2.$error.required">Location Address Line 2 is required</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_location.$invalid}">
                        <label>Location: </label>
                        <input class="form-control" name="location_location" g-places-autocomplete ng-model="clientLocationData.location_location" force-selection="true" options="autocompleteOption" required />
                        <span class="help-block" ng-show="clientLocationForm.location_location.$invalid">
                            <span ng-show="clientLocationForm.location_location.$error.required">Location is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_contact.$invalid}">
                        <label>Contact Name</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientLocationData.location_contact" name="location_contact" required>
                        <span class="help-block" ng-show="clientLocationForm.location_contact.$invalid">
                            <span ng-show="clientLocationForm.location_contact.$error.required">Contact Name is required</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_email.$invalid}">
                        <label>Contact Email</label>
                        <input type="email" class="form-control" placeholder="" ng-model="clientLocationData.location_email" name="location_email" required>
                        <span class="help-block" ng-show="clientLocationForm.location_email.$invalid">
                            <span ng-show="clientLocationForm.location_email.$error.required">Contact Email is required</span>
                            <span ng-show="clientLocationForm.location_email.$error.email">Contact Email is invalid</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_fax.$invalid}">
                        <label>Fax</label>
                        <input type="text" class="form-control" placeholder="" ng-model="clientLocationData.location_fax" name="location_fax">
                        <span class="help-block" ng-show="clientLocationForm.location_fax.$invalid">
                            <span ng-show="clientLocationForm.location_fax.$error.required">Contact fax is required</span>
                        </span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_typeId.$invalid}">
                        <label>Location Type</label>
                        <select class="form-control" ng-options="locationType.id as locationType.location_type for locationType in locationTypes" ng-model="clientLocationData.location_typeId" id="location_typeId" name="location_typeId" required></select>
                        <span class="help-block" ng-show="clientLocationForm.location_typeId.$invalid">
                            <span ng-show="clientLocationForm.location_typeId.$error.required">Location Type is required</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row inner_row">
                        <div class="col-lg-6">
                            <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refcode1.$invalid}">
                                <label>Reference Code 1</label>
                                <input class="form-control" name="location_refcode1" ng-model="clientLocationData.location_refcode1" ng-required="clientLocationData.location_refvalue1"/>
                                <span class="help-block" ng-show="clientLocationForm.location_refcode1.$invalid">
                                    <span ng-show="clientLocationForm.location_refcode1.$error.required">Reference Code 1 is required</span>
                                </span>
                            </div>
                            <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refcode2.$invalid}">
                                <label>Reference Code 2</label>
                                <input class="form-control" name="location_refcode2" ng-model="clientLocationData.location_refcode2" ng-required="clientLocationData.location_refvalue2"/>
                                <span class="help-block" ng-show="clientLocationForm.location_refcode2.$invalid">
                                    <span ng-show="clientLocationForm.location_refcode2.$error.required">Reference Code 2 is required</span>
                                </span>
                            </div>
                            <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refcode3.$invalid}">
                                <label>Reference Code 3</label>
                                <input class="form-control" name="location_refcode3" ng-model="clientLocationData.location_refcode3" ng-required="clientLocationData.location_refvalue3"/>
                                <span class="help-block" ng-show="clientLocationForm.location_refcode3.$invalid">
                                    <span ng-show="clientLocationForm.location_refcode3.$error.required">Reference Code 3 is required</span>
                                </span>
                            </div>
                            <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refcode4.$invalid}">
                                <label>Reference Code 4</label>
                                <input class="form-control" name="location_refcode4" ng-model="clientLocationData.location_refcode4" ng-required="clientLocationData.location_refvalue4"/>
                                <span class="help-block" ng-show="clientLocationForm.location_refcode4.$invalid">
                                    <span ng-show="clientLocationForm.location_refcode4.$error.required">Reference Code 4 is required</span>
                                </span>
                            </div>
                            <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refcode5.$invalid}">
                                <label>Reference Code 5</label>
                                <input class="form-control" name="location_refcode5" ng-model="clientLocationData.location_refcode5" ng-required="clientLocationData.location_refvalue5"/>
                                <span class="help-block" ng-show="clientLocationForm.location_refcode5.$invalid">
                                    <span ng-show="clientLocationForm.location_refcode5.$error.required">Reference Code 5 is required</span>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group ref_cont">
                                <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refvalue1.$invalid}">
                                    <label>Reference Value 1</label>
                                    <input class="form-control" name="location_refvalue1" ng-model="clientLocationData.location_refvalue1" ng-required="clientLocationData.location_refcode1"/>
                                    <span class="help-block" ng-show="clientLocationForm.location_refvalue1.$invalid">
                                        <span ng-show="clientLocationForm.location_refvalue1.$error.required">Reference Code value 1 is required</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refvalue2.$invalid}">
                                    <label>Reference Value 2</label>
                                    <input class="form-control" name="location_refvalue2" ng-model="clientLocationData.location_refvalue2" ng-required="clientLocationData.location_refcode2"/>
                                    <span class="help-block" ng-show="clientLocationForm.location_refvalue2.$invalid">
                                        <span ng-show="clientLocationForm.location_refvalue2.$error.required">Reference Code value 2 is required</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refvalue3.$invalid}">
                                    <label>Reference Value 3</label>
                                    <input class="form-control" name="location_refvalue3" ng-model="clientLocationData.location_refvalue3" ng-required="clientLocationData.location_refcode3"/>
                                    <span class="help-block" ng-show="clientLocationForm.location_refvalue3.$invalid">
                                        <span ng-show="clientLocationForm.location_refvalue3.$error.required">Reference Code value 3 is required</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refvalue4.$invalid}">
                                    <label>Reference Value 4</label>
                                    <input class="form-control" name="location_refvalue4" ng-model="clientLocationData.location_refvalue4" ng-required="clientLocationData.location_refcode4"/>
                                    <span class="help-block" ng-show="clientLocationForm.location_refvalue4.$invalid">
                                        <span ng-show="clientLocationForm.location_refvalue4.$error.required">Reference Code value 4 is required</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': clientLocationForm.location_refvalue5.$invalid}">
                                    <label>Reference Value 5</label>
                                    <input class="form-control" name="location_refvalue5" ng-model="clientLocationData.location_refvalue5" ng-required="clientLocationData.location_refcode5"/>
                                    <span class="help-block" ng-show="clientLocationForm.location_refvalue5.$invalid">
                                        <span ng-show="clientLocationForm.location_refvalue5.$error.required">Reference Code value 5 is required</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_timezone.$invalid}">
                        <label>Time Zone</label>
                        <select class="form-control" ng-options="timeZone.id as timeZone.name for timeZone in timeZones" ng-model="clientLocationData.location_timezone" id="location_timezone" name="location_timezone" required></select>
                        <span class="help-block" ng-show="clientLocationForm.location_timezone.$invalid">
                            <span ng-show="clientLocationForm.location_timezone.$error.required">Time zone is required</span>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Opening Time</label>
                        <timepicker ng-model="clientLocationData.location_hoursfrom" show-meridian="true"></timepicker>
                    </div>
                    <div class="form-group">
                        <label>Closing Time</label>
                        <timepicker ng-model="clientLocationData.location_hoursto" show-meridian="true"></timepicker>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label><strong>Location Requirement</strong></label>
                    <div class="form-group">
                        <label for="location_liftgate"><input type="checkbox" ng-model="clientLocationData.location_liftgate" id="location_liftgate"> Liftgate Required</label>

                    </div>
                    <div class="form-group">
                        <label for="location_apptrequired"><input type="checkbox" ng-model="clientLocationData.location_apptrequired" id="location_apptrequired"> Appointment Required</label>

                    </div>
                    <div class="form-group">
                        <label for="location_other"><input type="checkbox" ng-model="clientLocationData.location_other" id="location_other"> Other</label>

                    </div>
                    <div class="form-group" ng-class="{'has-error': clientLocationForm.location_notes.$invalid}">
                        <label>Remark</label>
                        <textarea class="form-control" rows="3" name="location_notes" ng-model="clientLocationData.location_notes" ng-required="clientLocationData.location_other"></textarea>
                        <span class="help-block" ng-show="clientLocationForm.location_notes.$invalid">
                            <span ng-show="clientLocationForm.location_notes.$error.required">Remark is required</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="btn-cont">
                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
                <button  type="button" class="btn btn-primary" ng-disabled="clientLocationForm.$invalid" ng-click="saveLocationData(clientLocationForm)">Save / Update</button>
            </div>
            <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
                <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
                <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
            </div>
        </div>
    </form>
</section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CustomerLocationManager.js')}}"></script>
@endsection
