<div class="modal-dialog">
        <div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Registration Information for {{ $registration->reg_usrfirstname }} {{ $registration->reg_usrlastname }} </h3>
  </div>
  <div class="modal-body">
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<tr>
				<td>Email</td>
				<td><strong>{{ $registration->reg_usremail }}</strong></td>
			</tr>
			<tr>
				<td>Company Name</td>
				<td><strong>{{ $registration->reg_company }}</strong></td>
			</tr>
			<tr>
				<td>Contact Number</td>
				<td><strong>{{ $registration->reg_cno }}</strong></td>
			</tr>
			<tr>
				<td>Plan</td>
				<td><strong>{{ $registration->plan->name }}</strong></td>
			</tr>
			<tr>
				<td>Registered from IP</td>
				<td><strong>{{ $registration->reg_userip }}</strong></td>
			</tr>
			<tr>
				<td>Registered from location</td>
				<td><strong>{{ $registration->reg_usrlocation }}</strong></td>
			</tr>
			<tr>
				<td>Registered from User Agent</td>
				<td><strong>{{ $registration->reg_usrbrowser }}</strong></td>
			</tr>
			<tr>
				<td>Email verified</td>
				<td><strong>{{ $registration->reg_emailverified ? 'Yes' : 'No' }}</strong></td>
			</tr>
			<tr>
				<td>Status</td>
				<td><strong>{{ !empty($registration->userID) ? 'Active' : 'Inactive' }}</strong></td>
			</tr>
			<tr>
				<td>Registered On</td>
				<td><strong>{{ $registration->created_at->toRfc850String() }}</strong></td>
			</tr>
		</table>
	</div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn">Close</button>
  </div>
</div>
</div>