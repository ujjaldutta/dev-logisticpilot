<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		@section('head')
			<title>@yield('page_title')</title>
			<!-- start: META -->
			<meta charset="utf-8" />
			<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
			<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
			<meta name="apple-mobile-web-app-capable" content="yes">
			<meta name="apple-mobile-web-app-status-bar-style" content="black">
			<meta content="@yield('meta_description')" name="description" />
			<meta content="@yield('meta_keywords')" name="keywords" />
			<!-- end: META -->
			<!-- start: MAIN CSS -->
			<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
			<link rel="stylesheet" href="{{ asset('bower_components/fontawesome/css/font-awesome.min.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/fonts/style.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/css/main.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/css/main-responsive.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/css/theme_light.css') }}" id="skin_color">
			<!-- end: MAIN CSS -->
		@show
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login example2">
		<div class="main-login col-sm-4 col-sm-offset-4">
			@section('logo')
				<div class="logo">
					<img src="{{ asset('images/logo.png') }}" height="32" alt="Logo" />
				</div>
			@show
			
			@yield('content')
			
			@section('footer')
			<!-- start: COPYRIGHT -->
				<div class="copyright">
					{{ date('Y') }} &copy; Logistics Pilot.
				</div>
			@show
			<!-- end: COPYRIGHT -->
		</div>
		
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
		<!-- <script src="{{ asset('admin/js/main.js') }}"></script> -->
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="{{ asset('admin/plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('admin/js/login.js') }}"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				//Main.init();
				Login.init();
			});
			//code to fade out the notification from all places after specified time (3000)
			/* 
			setTimeout(function(){
				if($('.alert').length)
					$('.alert').fadeOut('slow');
			}, 3000);
			*/
		</script>
			
		@yield('scripts')
	</body>
	<!-- end: BODY -->
</html>