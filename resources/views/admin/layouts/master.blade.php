<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>
			@yield('page_title')
		</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="@yield('meta_description')" name="keywords" />
		<meta content="@yield('meta_keywords')" name="description" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="{{ asset('bower_components/fontawesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/fonts/style.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/css/main.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/css/main-responsive.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/css/theme_light.css') }}" id="skin_color">
		<link rel="stylesheet" href="{{ asset('admin/plugins/iCheck/skins/all.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/css/theme_light.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('admin/css/alertify.core.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('admin/css/alertify.bootstrap.css') }}" type="text/css" id="skin_color">
		<link rel="stylesheet" href="{{ asset('admin/plugins/perfect-scrollbar/src/perfect-scrollbar.css') }}">
		<!-- end: MAIN CSS -->
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
		<style type="text/css">
			.datepicker{z-index:1151 !important;}
		</style>
		@yield('styles')
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<a class="navbar-brand" href="{{ URL::to('/admin/users/dashboard') }}">
						<img src="{{ asset('images/logo.png') }}" alt="{{-- SiteSettings::getName() --}}" height="32"/>
					</a>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
						@include('admin._partials.top_menu')
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">
				<!-- start: SIDEBAR -->
					@include('admin._partials.sidebar')
				<!-- end: SIDEBAR -->
			</div>
			<!-- start: PAGE -->
			<div class="main-content">
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
								@yield('breadcrumbs')
							<!-- end: PAGE TITLE & BREADCRUMB -->
							<div class="page-header">
								@yield('page_desc')
							</div>
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->
						@yield('content')
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<div class="footer clearfix">
			<div class="footer-inner">
				{{ date('Y') }} &copy; Logistics Pilot.
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('admin/plugins/iCheck/jquery.icheck.min.js') }}"></script>
		<script src="{{ asset('admin/plugins/perfect-scrollbar/src/jquery.mousewheel.js') }}"></script>
		<script src="{{ asset('admin/plugins/perfect-scrollbar/src/perfect-scrollbar.js') }}"></script>
		<script src="{{ asset('admin/js/alertify.js') }}"></script>
		<script src="{{ asset('admin/js/main.js') }}"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="{{ asset('admin/js/Config/Config.js') }}" type="text/javascript"></script>
		<script src="{{ asset('admin/js/Admin/Admin.js') }}" type="text/javascript"></script>
		@yield('scripts')
	</body>
	<!-- end: BODY -->
</html>