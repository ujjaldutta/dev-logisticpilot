<ul class="nav navbar-right">
	<!-- start: USER DROPDOWN -->
	<li class="dropdown current-user">
		<a data-toggle="dropdown" class="dropdown-toggle" href="#">
			<img src="{{ asset('admin/images/avatar-1-small.jpg') }}" class="circle-img" alt="">
			<span class="username">{{ Auth::user()->fname. ' '. Auth::user()->lname }}</span>
			<i class="clip-chevron-down"></i>
		</a>
		<ul class="dropdown-menu">
			<li>
				<a href="{{ url('/admin/users/change-password') }}">
					<i class="clip-exit"></i>
					&nbsp;Change Password
				</a>
				<a href="{{ url('/admin/users/logout') }}">
					<i class="clip-exit"></i>
					&nbsp;Sign Out
				</a>
			</li>
		</ul>
	</li>
	<!-- end: USER DROPDOWN -->
</ul>
