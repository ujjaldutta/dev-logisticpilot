@extends('admin.layouts.login')
@section('page_title')
	Administration Login
@stop
@section('content')
<!-- start: LOGIN BOX -->
			<div class="box-login">
				<h3>Sign in to your account</h3>
				<p>
					Please enter your name and password to log in.
				</p>
				@if(Session::has('error'))
					<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<p> {{ Session::get('error') }}	</p>	
					</div>
				@endif
				<form action="/admin/users/login" class="form-login" method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
					</div>
					@if ($errors->has())
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							@foreach ($errors->all() as $error)
								<p> {{ $error }}	</p>	
							@endforeach
						</div>
					@endif
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<input name="email" type="email" value="{{ old('email') }}" class="form-control required email" placeholder="Email" />
								<i class="fa fa-user"></i> </span>
						</div>
						<div class="form-group form-actions">
							<span class="input-icon">
								<input name="password" type="password" value="{{ old('password') }}" class="form-control required" placeholder="Password" />
								<i class="fa fa-lock"></i>
								<!-- <a class="forgot" href="javascript:;">
									I forgot my password
								</a> --> </span>
						</div>
						<div class="form-actions">
							<label for="remember" class="checkbox-inline">
							</label>
							<input type="submit" class="btn btn-bricky pull-right" value="Sign In" />
						</div>
					</fieldset>
				</form>
			</div>
			<!-- end: LOGIN BOX -->
			<!-- start: FORGOT BOX -->
			<div class="box-forgot">
				@if(Session::has('pwResetError'))
					<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<p> {{ Session::get('pwResetError') }}	</p>	
					</div>
				@endif
				@if(Session::has('pwReseStatus'))
					<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<p> {{ Session::get('pwReseStatus') }}	</p>	
					</div>					
				@endif
				<h3>Forget Password?</h3>
				<p>
					Enter your e-mail address below to reset your password.
				</p>
				<form action="/auth/remind" id="form-forgot" role="form" method="POST">
					<input type="hidden" name="_token" value={{ csrf_token() }} />
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
					</div>
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<input name="email" type="email" value="{{ old('email') }}" class="form-control required email" placeholder="Email" />
								<i class="fa fa-envelope"></i> </span>
						</div>
						<div class="form-actions">
							<button class="btn btn-light-grey go-back" type="button">
								<i class="fa fa-circle-arrow-left"></i> Back
							</button>
							<input type="submit" class="btn btn-bricky pull-right" value="Reset Password" />
						</div>
					</fieldset>
				</form>
			</div>
			<!-- end: FORGOT BOX -->
@stop

@section('scripts')
	@if(Session::has('pwResetError') || Session::has('pwReseStatus'))
		<script>
			$(document).on('ready', function(){
				$('.forgot').click();
			});
		</script>
	@endif
@stop