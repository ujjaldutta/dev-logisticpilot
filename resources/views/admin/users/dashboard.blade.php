@extends('admin.layouts.user_home')

@section('page_title')
	Administration Dashboard
@stop
@section('page_desc')
	<h1>Welcome <small> </small></h1>
@stop
@section('styles')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
	<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.css') }}" />
	<link href="{{ asset('admin/plugins/bootstrap-date-time-picker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
	<style type="text/css">
		span.links{cursor:pointer;}
		span.links:hover{text-decoration:underline;}
	</style>
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('dashboard') !!}
@stop
@section('content')
@yield('message')
	@if(Session::has('success'))
		<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p> {{ Session::get('success') }}	</p>	
		</div>
	@endif
	@if(Session::has('error'))
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p> {{ Session::get('error') }}	</p>	
		</div>
	@endif
	@if($errors->has())
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			</button>
		@foreach ($errors->all() as $error)
				<p> {{ $error }}</p>
		@endforeach
		</div>
	@endif
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="contract_table" style="min-height: 797px;">
                
                <div class="clearfix"></div>                
                <div class="table-responsive clearfix customer-table-transit">
                  <table class="table table-striped table-bordered table-hover" id="user-management">	
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Plan</th>
								<th>Status</th>
								<th class="hidden-xs">Registered On</th>
								<th align="center">Actions</th>
							</tr>
						</thead>
				  </table>                   
              </div>               
            </div>
        </div>
		
		<!---- 2nd Modal for confirmed shift/event -->
		<div id="user-details-modal" class="modal fade" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body text-center">
						<img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/>
					</div>
				</div>
			</div>
		</div>
		<!---- Modal Ends -->
      </div>
    </div>
  

@stop

@section('scripts')
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ asset('admin/plugins/DataTables/media/js/DT_bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugins/select2/select2.min.js') }}"></script>
	<script src="{{ asset('admin/js/Admin/AdminDashboardNew.js') }}"></script>	
	<script>		
		//pending shift SPA
		app.AdminDashboardNew.MemberView.init();
	</script>
	<script src="{{ asset('bower_components/angular/angular.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/lib/angular_modules/angucomplete-alt.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/lib/angular_modules/controllers/CommonAppController.js') }}" type="text/javascript"></script>
	<script>
		var ng = angular.module('admin_module', ['angucomplete-alt'])
			.controller('CommonAppController', CommonAppController);
	</script>
@stop