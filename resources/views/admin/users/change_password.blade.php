@extends('admin.layouts.master')
@section('page_title')
	Change Password
@stop
@section('page_desc')
	<h1>Change Password <small>Updates your password</small></h1>
@stop
@section('content')
	@yield('message')
		@if(Session::has('success'))
			<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<p> {{ Session::get('success') }}	</p>	
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<p> {{ Session::get('error') }}	</p>	
			</div>
		@endif
		@if($errors->has())
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
			@foreach ($errors->all() as $error)
					<p> {{ $error }}</p>
			@endforeach
			</div>
		@endif
	<form name="password_change" action="{{ url(\Config::get('app.adminPrefix'). '/users/change-password') }}" id="password_change" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}" required/> 
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<div class="form-group">
					<label for="old_password">Old Password</label>
					<input type="password" name="old_password" id="old_password" class="form-control">
				</div>
				<div class="form-group">
					<label for="password">New Password</label>
					<input type="password" name="password" id="password" class="form-control">
				</div>
				<div class="form-group">
					<label for="password_confirmation">Confirm New Password</label>
					<input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Update">
				</div>
			</div>
		</div>
	</form>
@endsection

@section('scripts')
	<script src="{{ asset('admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
	<script src="{{ asset('admin/js/Config/Config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('admin/js/Admin/Admin.js') }}" type="text/javascript"></script>
	<script src="{{ asset('admin/js/Admin/AdminUsers.js') }}"></script>	
	<script>
		//highlight current section root item on the left menu
		app.Admin.highLightleftMenu('leftmenu-root-dashboard');
		
		//pending shift SPA
		app.AdminUsers.PasswordChange.init();
	</script>
@stop