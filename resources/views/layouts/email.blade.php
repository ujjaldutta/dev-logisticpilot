<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title>@yield('title')</title>
</head>
<body>
<table width="100%" border="0" cellpadding="5" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000;">
	<tr>
    <td align="left" valign="middle">
		<img src="{{ asset('images/logo.png') }}" alt="Logistics Pilot" />
	</td>
  </tr>
	@yield('content')
  <tr>
    <td align="center" style="font-family:Arial, Helvetica, sans-serif;">
		<strong>This is an automated message for information purposes only.</strong>
	</td>
  </tr>
</table>
</body>
</html>