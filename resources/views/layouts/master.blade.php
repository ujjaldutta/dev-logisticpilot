<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
    <title>@yield('page_title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <meta name="author" content="DC">
    <!-- Bootstrap Styles -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- font awesome -->
	<link href="{{ asset('bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
	
	 <!-- CSS Animations -->
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
	
    <!-- Styles -->
    <link href="{{ asset('style.css') }}" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>
    <!-- Support for HTML5 -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Enable media queries on older bgeneral_rowsers -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/respond.min.js')}}></script>  <![endif]-->
</head>
<body>
    <div class="animationload">
        <div class="loader">Loading...</div>
    </div>
    
    <header id="header-style-1">
		<div class="container">
        	<div id="topbar" class="clearfix">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="topmenu">
                        <span class="topbar-email"><i class="fa fa-envelope"></i> <a href="mailto:info@logisticspilot.com">info@logisticspilot.com</a></span>
                        <span class="topbar-phone"><i class="fa fa-phone"></i> 1-900-324-5467</span>
                        <span class="topbar-login"><i class="fa fa-user"></i> <a href="{{ url('/plans/pricing') }}">Register</a></span>
                    </div><!-- end top menu -->
                </div><!-- end columns -->
            </div>
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{ url('/') }}" class="navbar-brand">Logistics Pilot</a>
        		</div><!-- end navbar-header -->
                
				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
                    	<li><a href="index.html">Home</a></li>
                    	<li><a href="features.html">Features</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="partners.html">Partners</a></li>
                        <li><a href="about.html">About</a></li>
                        <!--<li><a href="blog.html">Blog</a></li>-->
                        <li><a href="contact.html">Contact</a></li>
					</ul><!-- end navbar-nav -->
				</div><!-- #navbar-collapse-1 -->
           	</nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->
	</header><!-- end header-style-1 -->
    
    <section id="one-parallax" class="parallax post-wrapper-top clearfix" style="background-image: url('/images/cloud-BG.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="container">
			<div class="col-lg-12">
				<h2>@yield('page_heading')</h2>
                <!--<ul class="breadcrumb pull-right">
                    <li><a href="index.html">Home</a></li>
                    <li>Pricing</li>
                </ul>-->
			</div>
		</div>
	</section>
		@yield('content')
    
	<footer id="footer-style-1">
    	<div class="container">
        	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            	<div class="widget">
                	<div class="title">
                        <h3>About Logistics Pilot</h3>
                    </div><!-- end title -->
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend varius purus, sed bibendum purus scelerisque nec. Maecenas faucibus urna vitae ultrices feugiat. Nullam lacinia suscipit diam, nec scelerisque ipsum. Vivamus pharetra ipsum eros. Donec lobortis, elit eget faucibus sodales, orci mi egestas magna, quis pharetra turpis dolor vel felis. Morbi mattis, dolor fermentum fermentum pulvinar, metus augue rutrum metus, ac molestie eros enim a mauris. Vestibulum et laoreet dolor. In eget tortor at augue eleifend ornare ac sit amet arcu. Vivamus mattis in lectus nec varius. Mauris quis turpis nulla. Nam interdum lectus a bibendum vehicula.</p>
                 	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend varius purus, sed bibendum purus scelerisque nec. Maecenas faucibus urna vitae ultrices feugiat. Nullam lacinia suscipit diam, nec scelerisque ipsum.</p>
                </div><!-- end widget -->
            </div><!-- end columns -->
        	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            	<div class="widget">
                	<div class="title">
                        <h3>NewsLetter</h3>
                    </div><!-- end title -->
					<div class="newsletter_widget">
                    	<p>Subscribe to our newsletter to receive news, updates, free stuff and new releases by email. We don't do spam..</p>
                        <form action="#" class="newsletter_form">
                            <input type="text" class="form-control" placeholder="Enter your email address"> 
                            <a href="#" class="btn btn-primary pull-right">Subscribe</a>    
                        </form><!-- end newsletter form -->
					</div>
                </div><!-- end widget -->
            </div><!-- end columns --> 
    	</div><!-- end container -->
    </footer><!-- end #footer-style-1 -->    

<div id="copyrights">
    	<div class="container">
			<div class="col-lg-5 col-md-6 col-sm-12">
            	<div class="copyright-text">
                    <p>Copyright © 2014 - Logistics Pilot</p>
                </div><!-- end copyright-text -->
			</div><!-- end widget -->
			<div class="col-lg-7 col-md-6 col-sm-12 clearfix">
				<div class="footer-menu">
                    <ul class="menu">
                        <li><a href="index.html">Home</a></li>
                    	<li><a href="features.html">Features</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="partners.html">Partners</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
			</div><!-- end large-7 --> 
        </div><!-- end container -->
    </div><!-- end copyrights -->
    
	<div class="dmtop">Scroll to Top</div>
   
  <!-- Main Scripts-->
  <script src="{{ asset('bower_components/requirejs/require.js') }}"></script>
  <script src="{{ asset('js/config.js') }}"></script>
  <script src="{{ asset('js/common_actions.js') }}"></script>
	@yield('scripts')
  </body>
</html>