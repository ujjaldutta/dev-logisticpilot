<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="@yield('meta_description')">
<meta name="keywords" content="@yield('meta_keywords')">
<meta name="author" content="DC">
<title>@yield('page_title')</title>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
<style>
.hidden{
 display:none;
}
.animationload {
    background-color: #fff;
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 999999
}
.loader{background-image:url(/images/loading.gif);background-position:center center;background-repeat:no-repeat;font-size:0;height:200px;left:50%;margin:-100px 0 0 -100px;position:absolute;top:50%;width:200px}.animationload{background-color:#fff;bottom:0;left:0;position:fixed;right:0;top:0;z-index:999999}
</style>
</head>
<body>
<div class="animationload">
        <div class="loader">Loading...</div>
</div>

<div class="body-container @yield('page_class', 'qoute_page')" id="page-wrapper" ng-controller="@yield('angular_controller')">
  <!-- content section-->
  <section class="content-wrapper">
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12">
		  <!--div class="row breadcrumb-cont">
           @yield('breadcrumbs')
		  </div-->
		@yield('content')
        </div>
      </div>
    </div>
  </section>	
  <!-- /content section-->  

</div>

<script src="{{ asset('bower_components/requirejs/require.js') }}"></script>
<script src="{{ asset('js/config_jquery_easytabs.js') }}"></script>
<script src="{{ asset('js/common_actions.js') }}"></script>
@yield('scripts')
</body>
</html>