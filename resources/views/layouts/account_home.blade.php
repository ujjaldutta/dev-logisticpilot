<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="@yield('meta_description')">
<meta name="keywords" content="@yield('meta_keywords')">
<meta name="author" content="DC">
<title>@yield('page_title')</title>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700,600' rel='stylesheet' type='text/css'>
<!-- Bootstrap Styles -->
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="{{ asset('css/easy-responsive-tabs.css') }}" />
<link href="{{ asset('css/screen.css') }}" rel="stylesheet" type="text/css" />
<?php $colorSchemeCss = CustomerRepository::getCustomerTheme(); ?>
@if($colorSchemeCss != '')
	<link href="{{ asset($colorSchemeCss) }}" rel="stylesheet" type="text/css" />
@endif
<!-- font awesome -->
<!-- <link href="{{ asset('bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@yield('styles')
</head>
<body>
<div class="animationload">
        <div class="loader">Loading...</div>
</div>
<div class="top-bar">
  <div class="wrapper">
    <div class="row">
       @include('_partials.brandlogo')
      <div class="col-xs-12 col-sm-9 col-md-6 pull-right">
        <div class="login pull-right">
			@include('_partials.account.top_right_menu')
        </div>
        <div class="user-menu pull-right">
          @include('_partials.account.user_menu')
        </div>
        <div class="support-team pull-right">
          @include('_partials.account.support_menu')
        </div>
		 <!-- Theme List: Need to add -->
		 {{-- @include('_partials.color_changer') --}}
        <!-- Theme List: Need to add -->
      </div>
    </div>
  </div>
</div>
<div class="row clearfix">
  <div class="col-xs-12 col-lg-12">
    <nav role="navigation" class="navbar navbar-default navbar-fixed-top main-menu">
      <div class="container-fluid"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="toggle-menu filter_menu" href="#"><span class="visible-lg filter_menu">Filter menu<i class="fa fa-angle-down"></i></span></a> </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
          @include('_partials.top_navigation')
        </div>
        <!-- /.container-fluid --> 
      </div>
      <!-- /.navbar-collapse --> 
    </nav>
  </div>
</div>
<div class="body-container marg @yield('page_class', 'qoute_page')" id="page-wrapper" ng-controller="@yield('angular_controller')">
	@include('_partials.top_drawer')
  <div  class="col-sm-12 search-page">
    <div class="row breadcrumb-cont">
      <div class="col-lg-12 col-md-12 col-xs-12">
         @yield('breadcrumbs')
      </div>
    </div>
	<form class="form-inline" name="rateSearch">
    <div class="row">
    	@yield('top_content')
    </div>
	<!-- sidebar-->
	@include('_partials.sidebar')
	<!-- /sidebar-->
    <!-- content section-->
		@yield('content')
	</form>
  </div>
  <!-- /content section--> 
  <!-- Page footer-->
  <footer class="footer-wrapper"> </footer>
  <!-- /Page footer--> 
</div>
<script src="{{ asset('bower_components/requirejs/require.js') }}"></script>
<script src="{{ asset('js/config.js') }}"></script>
<script src="{{ asset('js/common_actions.js') }}"></script>
@yield('scripts')
</body>
</html>