<!-- sidebar-->
<aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search">
    <div class="nav-col no-mar sidebar">
        <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
            <div class="panel panel-default">
                <div class="filter-head">
                    <h2>Carrier</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Name" ng-model="filter.carrName" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}" >
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('carrName')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head shipment-status">
                    <h2>Location</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Location" ng-model="filter.carrLoc" g-places-autocomplete force-selection="true" options="autocompleteOption">
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('carrLoc')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                 <div class="filter-head">
                    <h2>Insurance</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <div ng-dropdown-multiselect="" options="filter.insuranceTypes" selected-model="filter.insId" extra-settings="insSettings" events="searchInsuranceEvent"></div>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>Mode</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <div ng-dropdown-multiselect="" options="filter.modeTypes" selected-model="filter.modId" extra-settings="modSettings" events="searchModeEvent"></div>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                 <div class="filter-head">
                    <h2>Equipment Type</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <div ng-dropdown-multiselect="" options="filter.equipmentTypes" selected-model="filter.equipId" extra-settings="equipmentSettings" events="searchEquipmentEvent"></div>
                        </div>
                        <div class="col-xs-3">                           
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>Temperature Control</h2>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrEquipTC" value="allTC" checked> All
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrEquipTC" value="enabledTC"> Enabled
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrEquipTC" value="disabledTC"> Not Available
                        </label>
                    </div>
                </div>               
                
                <div class="filter-head contract_block nobord">
                    <h2>API</h2>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrApi" value="all" checked> All
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrApi" value="enabled"> Enabled
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="filter.carrApi" value="disabled"> Carrier does not have API
                        </label>
                    </div>
                </div>

            </div>
        </div>
    </div>
</aside>
<!-- /sidebar-->