  <!-- sidebar-->
  <aside class="leftnavbar-wrapper" >
    <div class="nav-col">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
            <div class="filter-head lanes_block">
                <h2>Contract Profile(s) <button class="btn btn-primary btn-blue pull-right" tabindex="-1" data-toggle="modal" data-target="#moreDetails1" type="button" ng-click="addProfile()"><span>Add Profile</span></button></h2>
                
                <div class="profile-list autoscroll">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table lancecount_table">
                <tbody>
                  <tr  ng-repeat="cprofile in profilelist">
                    <td width="60%" class="tooltip_cont">
						<a href="javascript:void(0)" ng-click="loadContract({profileid:cprofile.RateProfileCode, limit: recordLimit,page: 1});">@{{cprofile.RateProfileDescription}}</a>
                    <div class="tooltip_div">
                            	<h3>Profile Code</h3>
                            	<p>@{{cprofile.RateProfileCode}}</p>
								
                            </div>
                    </td>
                    <td width="25%" ><a href="javascript:void(0)" ng-click="loadCarriersection(cprofile.RateProfileCode,cprofile._id);">@{{cprofile.contractors.length}}</a>
						
                    </td>
                    <td>
                        <div class="dropdown">
                          <!--<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                            Details
                            <span class="caret"></span>
                          </button>-->
                          <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" ng-click="showdropdown(cprofile)" data-toggle="dropdown" aria-expanded="true">
                            Details
                            <span class="caret"></span>
                          </button>
                          
                         
                        </div>
                    </td>
                  </tr>
                 
                 
                   </tbody>
              </table>
              </div>
             <ul class="dropdown-menu mainprofile" role="menu" aria-labelledby="dropdownMenu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="contract" ng-click="loadCarriersection(selectedprofile.RateProfileCode,selectedprofile._id);">Profile Contracts</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="contract" ng-click="loadCarriersection(selectedprofile.RateProfileCode,selectedprofile._id);" tabindex="-1" data-toggle="modal" data-target="#moreDetails1" >Edit Profile</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="fuel" ng-click="loadCarriersection(selectedprofile.RateProfileCode,selectedprofile._id);loadFuelsection(selectedprofile.RateProfileCode,selectedprofile._id);">Profile Fuel</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1"  href="javascript:void(0)" class="fuel"  ng-click="loadCarriersection(selectedprofile.RateProfileCode,selectedprofile._id);loadAccessorialsection(selectedprofile.RateProfileCode,selectedprofile._id);">Profile Accessorial</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1"  href="javascript:void(0)" class="services" ng-click="loadCarriersection(selectedprofile.RateProfileCode,selectedprofile._id);loadMarkupsection(selectedprofile.RateProfileCode,selectedprofile._id);">Profile Markup</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="services" ng-click="copyProfile(selectedprofile);">Copy Profile</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="services" ng-click="deleteProfile(selectedprofile.RateProfileCode);">Delete Profile</a></li>
							<!--<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="services" ng-click="loadCarriersection(cprofile.RateProfileCode,cprofile._id);loadCarrier_Api();">Carrier API</a></li>-->
                          </ul>
            
            </div>
            <div class="filter-head" style="padding-top:25px;" ng-hide="rightpanel=='Carrier_Api'">
            	<form class="" role="search">
                    <div class="input-group add-on">
                      <input type="text" class="form-control" placeholder="search" name="srch-term" id="srch-term">
                      <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
            </div>
            <div class="filter-head contract_block" ng-hide="rightpanel=='Carrier_Api'">
                <h2>Filter contract</h2>
                <div class="checkbox">
                    <h2>Profile</h2>
                    </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Profile" ng-model="filter.profileName" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                </div>
                
                <div class="" ng-show="rightpanel=='profile'">
                <div class="checkbox">
                    <h2>Carrier</h2>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Carrier" ng-model="filter.carrier" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                </div>
                </div>
                
                <div ng-show="rightpanel=='profile'">
                <div class="checkbox">
                    <h2>Effective Date</h2>
                </div>
                <div class="row filter-date" >
					
					
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker" placeholder="12/12/2014" ng-model="filter.EffectiveFrom" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker3" placeholder="12/12/2014" ng-model="filter.EffectiveTo" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                </div>
               </div>
                <div ng-show="rightpanel=='Accessorial'" >
				<div class="checkbox">
                    <h2>Effective Date</h2>
                </div>
                <div class="row filter-date">
					
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepickerassoc" placeholder="12/12/2014" ng-model="assocfilter.EffectiveFrom" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepickerassoc1" placeholder="12/12/2014" ng-model="assocfilter.EffectiveTo" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                   
                    
                </div>
                </div>
                
                
                <div ng-show="rightpanel=='Fuel'" >
				<div class="checkbox">
                    <h2>Effective Date</h2>
                </div>
                <div class="row filter-date">
					
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepickerfuel" placeholder="12/12/2014" ng-model="fuelfilter.EffectiveFrom" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepickerfuel1" placeholder="12/12/2014" ng-model="fuelfilter.EffectiveTo" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                    </div>
                   
                    
                </div>
                </div>
                
                <div class="" ng-show="rightpanel=='profile'">
                <div class="checkbox">
                    <h2>State</h2>
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" placeholder="Search States" ng-model="lanefilters.state" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}" ng-change="getStates(lanefilters.state,'USA')" uib-typeahead="state.name for state in statelist"  typeahead-on-select="onStateSelect($item)">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" placeholder="Search City" ng-model="lanefilters.city" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}" ng-change="getCity(lanefilters.city,'USA',Statecode)" uib-typeahead="city.name for city in citylist" typeahead-on-select="onCitySelect($item)">
                    </div>
                </div>
                </div>
                
                <div class="row " ng-show="rightpanel=='Accessorial'" >
                <div class="checkbox">
                    <h2>Accessorial</h2>
                </div>
                <div class="form-group" >
                    <input type="text" class="form-control" placeholder="Search Accessorial" id="assocsearch" name="assocsearch" autocomplete="off"    ng-model="assocfilter.accessorial"  ng-change="getassoc(assocfilter.accessorial)" uib-typeahead="atype.name for atype in assoctype"  typeahead-on-select="onSelectAssoc($item, assocfilter, 'assocfilter')">
                    
                    
                </div>
                </div>
                
                
                
                
            </div>
            <div class="filter-date expiring_contracts" ng-show="rightpanel=='profile'">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Show Expiring Contracts
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li>
                        <div class="checkbox all">
                            <label>
                                <input type="checkbox"  ng-click="showExpireContracts('all',filter.selectAll);checkAll();" ng-model="filter.selectAll" value="all">Show All
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" ng-click="showExpireContracts('1',filter.month1)" ng-model="filter.month1" name="month1" value="1">1 Month
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" ng-click="showExpireContracts('2',filter.month2)" ng-model="filter.month2" name="month2" value="2">2 Months
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" ng-click="showExpireContracts('3',filter.month3)" ng-model="filter.month3" name="month3" value="3">3 Months
                            </label>
                        </div>
                    </li>
                  </ul>
                </div>
            </div>
      	
      	
      	
      	
      	
      	
      	</div>
      </div>
    </div>
  </aside>
  <!-- /sidebar-->
  
  
  <!-- Profile Modal -->
<div class="modal fade blue" id="moreDetails1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="reloadProfile()"><span aria-hidden="true">&times;</span></button>
        <h2>Add Profile</h2>
      </div>
      <div class="modal-body">
        <form id="createprofile" name="createprofile">
			<input type="hidden"   name="_id" ng-model="profile._id"  ng-init="profile._id" value="@{{profile._id}}" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}" ng-model="profile._token">
        	<div class="row">
            	<div class="col-lg-3 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Profile Code</label>
                    	<input type="text" class="form-control" placeholder="" name="RateProfileCode" ng-model="profile.RateProfileCode" required>
                  	</div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Profile Description</label>
                    	<input type="text" class="form-control" placeholder="" name="RateProfileDescription" ng-model="profile.RateProfileDescription" required>
                  	</div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Active</label>
                    	<select class="form-control" name="Active" ng-model="profile.Active" required>
                          <option value="true">yes</option>
                          <option value="false">no</option>
                        </select>
                  	</div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-6">
                	<div class="form-group">
                    	<label>Customer Reference Code</label>
                    	<input type="text" class="form-control" placeholder="" name="CustomerReferenceCode" ng-model="profile.CustomerReferenceCode" required>
                  	</div>
                </div>
               
            </div>
            <div class="row"><div class="col-lg-12 col-md-3 col-sm-6">@{{message}}</div></div>
            <div class="row">
            	<div class="col-lg-12 buttons">
                	<div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
			<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reloadProfile()">Close</button>
                	<button type="submit" ng-click="submitProfile(createprofile);" class="btn btn-primary" ng-disabled="createprofile.$invalid">Save</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>







