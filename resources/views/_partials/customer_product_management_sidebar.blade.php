<!-- sidebar-->
<aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search">
    <div class="nav-col no-mar sidebar">
        <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
            <div class="panel panel-default">
                <div class="filter-head">
                    <h2>Item Name</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Name" ng-model="filter.itemName" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('itemName')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>NMFC</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Name" ng-model="filter.itemNMFC" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('itemNMFC')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>Freight Class</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <div ng-dropdown-multiselect="" options="filter.classTypes" selected-model="filter.classId" extra-settings="classSettings" events="searchClassEvent"></div>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</aside>
<!-- /sidebar-->
