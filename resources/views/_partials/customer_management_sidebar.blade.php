<!-- sidebar-->
  <aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search">
    <div class="nav-col no-mar sidebar">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
			<div class="filter-head customer-info">
                <h2>Customer</h2>
                <div class="form-group">
					<div class="col-xs-9">
						<input type="text" class="form-control" placeholder="Name" ng-model="filter.custName" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
					</div>
					<div class="col-xs-3">
						<button ng-click="resetFilter('userName')" class="btn">Reset</button>
					</div>
					<div class="clearfix"></div>
                </div>
           	</div>
          <div class="filter-head shipment-status">
               <h2>Location</h2>
			   <div class="form-group">
					<div class="col-xs-9">
						<input type="text" class="form-control" placeholder="Location" ng-model="filter.custLoc" g-places-autocomplete force-selection="true" options="autocompleteOption">
					</div>
					<div class="col-xs-3">
						<button ng-click="resetFilter('userLoc')" class="btn">Reset</button>
					</div>
					<div class="clearfix"></div>
                </div>
          </div>
          <div class="filter-head pricing nobord">
            <h2>Credit Limit
            </h2>
            <div class="slide-range">
              <!--slider-->
                <rzslider
                    rz-slider-floor="priceFilter.floor"
                    rz-slider-ceil="priceFilter.ceil"
                    rz-slider-model="priceFilter.min"
                    rz-slider-high="priceFilter.max"
                    rz-slider-on-change="onPriceFilter()"
                >
                </rzslider>
              <!--slider-->
            </div>
            <h2 class="transit">Overdue
            </h2>
            <div class="slide-range">
              <!--slider-->
              <div class="slider">
                <rzslider
                    rz-slider-floor="transitFilter.floor"
                    rz-slider-ceil="transitFilter.ceil"
                    rz-slider-model="transitFilter.min"
                    rz-slider-high="transitFilter.max"
                    rz-slider-on-change="onTransitFilter()"
                >
                </rzslider>
              </div>
              <!--slider-->
            </div>
          </div>
		  <div class="filter-head contract_block nobord">
                <h2>Status</h2>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="filter.custActive">Active
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="filter.custInActive">Inactive
                    </label>
                </div>
         	</div>
        </div>
      </div>
    </div>
  </aside>
  <!-- /sidebar-->