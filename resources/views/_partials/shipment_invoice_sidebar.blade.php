  <aside class="leftnavbar-wrapper">
    <div class="nav-col">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
          <div class="filter-head shipment-status-list">
            <h2>shipment status
              <button type="button" class="btn btn-update pull-right"><span>Update</span></button>
              <p>Up to date</p>
            </h2>
            <ul class="list-group shipment-status">
              <li class="list-group-item"> <a href="javascript:void(0)" class="booked"> <span class="badge">250</span><img src="images/booked-icon.png" alt=""> Booked </a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="dispatched"><span class="badge">350</span><img src="images/dispatched-icon.png" alt=""> Dispatched </a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="transit"><span class="badge">150</span><img src="images/in-transit-icon.png" alt=""> In Transit</a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="out-delivery"><span class="badge">850</span><img src="images/out-for-delivery-icon.png" alt=""> Out for dlivery</a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="delivered"> <span class="badge">350</span><img src="images/delivered-icon.png" alt=""> Delivered </a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="quote"> <span class="badge">25</span><img src="images/quote-requested-icon.png" alt=""> Quote Requested </a> </li>
              <li class="list-group-item"> <a href="javascript:void(0)" class="waiting"> <span class="badge">450</span><img src="images/waiting-icon.png" alt=""> Waiting for Carrier </a> </li>
            </ul>
          </div>
          <div class="filter-head contract_block nobord nobotPad">
                <h2>Filter contract</h2>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Customer
                    </label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Customer">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Carrier
                    </label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Carrier">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Ship Date
                    </label>
                </div>
                <div class="row filter-date">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker" placeholder="12/12/2014">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker2" placeholder="12/12/2014">
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Delivery Date
                    </label>
                </div>
                <div class="row filter-date">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker3" placeholder="12/12/2014">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker4" placeholder="12/12/2014">
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Sales Rep
                    </label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Sales Rep">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Carrier Payment Approved
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Check Date
                    </label>
                </div>
                <div class="row filter-date">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker5" placeholder="12/12/2014">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="datepicker6" placeholder="12/12/2014">
                    </div>
                </div>
                <div class="form-group">
                    <label class="topPad10">Pro Number</label>
                    <input type="text" class="form-control" placeholder="Check No">
                </div>
                <div class="form-group">
                    <label>Load No / Invoice No</label>
                    <input type="text" class="form-control" placeholder="Check No">
                </div>
                <div class="form-group">
                    <label>Invoice Received (210)</label>
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">No
                    </label>
                </div>
            </div>
            <div class="filter-head shipment-status nobord">
                <h2>Transportation Mode</h2>
                <div class="">
                  <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                      LTL </label>
                  <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                      TL </label>
                </div>
            </div>
        </div>
      </div>
    </div>
  </aside>