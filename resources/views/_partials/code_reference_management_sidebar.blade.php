<!-- sidebar-->
<aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search">
    <div class="nav-col no-mar sidebar">
        <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
            <div class="panel panel-default">
                <div class="filter-head">
                    <h2>Code Type</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <select ng-model="filter.codeType" ng-options="type.codeType as type.codeType for type in filter.types" class="form-control"></select>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>Item Code</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Name" ng-model="filter.itemCode" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('itemCode')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="filter-head">
                    <h2>Item Description</h2>
                    <div class="form-group">
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Name" ng-model="filter.itemDesc" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
                        </div>
                        <div class="col-xs-3">
                            <button ng-click="resetFilter('itemDesc')" class="btn">Reset</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>               
                
            </div>
        </div>
    </div>
</aside>
<!-- /sidebar-->
