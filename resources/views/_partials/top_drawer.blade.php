<!-- content section-->
<?php
	$currentCustomer = CustomerRepository::getCurrentCustomer();
	//var_dump($currentCustomer);
	$fname = isset($currentCustomer['client_firstname']) ? $currentCustomer['client_firstname'] : '';
	$lname = isset($currentCustomer['client_lastname']) ? $currentCustomer['client_lastname'] : '';
	$cname = isset($currentCustomer['client_compname']) ? $currentCustomer['client_compname'] : '';
	$address = isset($currentCustomer['client_addr1']) ? $currentCustomer['client_addr1'] : '';
	$city = isset($currentCustomer['client_city']) ? $currentCustomer['client_city'] : '';
	$state = isset($currentCustomer['client_state']) ? $currentCustomer['client_state'] : '';
	$postal = isset($currentCustomer['client_postal']) ? $currentCustomer['client_postal'] : '';
	$country = isset($currentCustomer['client_country']) ? $currentCustomer['client_country'] : '';
	$email = isset($currentCustomer['client_email']) ? $currentCustomer['client_email'] : '';
	$phone = isset($currentCustomer['client_phone']) ? $currentCustomer['client_phone'] : '';
	$isNotOriginalUser = (isset($currentCustomer['custID']) && $currentCustomer['custID'] != Auth::user()->clientID) ? 
		true : false;
?>
<section class="content-wrapper full-width">
<div class="content-subcontainer">
      		<div class="row clearfix">
        		<div class="col-lg-12 col-md-12 col-xs-12">
          			<div class="row dashboard_top">
                        <div class="col-lg-2 col-md-6 col-xs-12">
                        	<p><span>{{{ $cname }}}</span>
                            {{{ $address }}}, {{$city}}, {{ $state }} - {{ $postal }}<br>
                            Contact Name: {{{ $fname }}}<br>
                            Phone: {{{ $phone }}}<br>
                            Email: {{{ $email }}}</p>  
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                       	  <ul class="balance_list">
                            	<li class="credit">Credit Line: $ 25,292</li>
                                <li class="used">Used: $ 15,200</li>
                                <li class="balance">Balance: $ 10,092</li>
                            </ul>
                          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr class="head">
                                <td>0 Days</td>
                                <td>30 Days</td>
                                <td>60 Days</td>
                                <td>90 Days</td>
                                <td>120 Days</td>
                              </tr>
                              <tr>
                                <td>$ 1100.00</td>
                                <td>$ 1582.22</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                          	</table>
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-12">
                          	<p><span>Last Activity</span>
                            Load Created: ABC001<br>
                            Ceated Date: 01/10/2015<br>
                            Created By: John Doe</p> 
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-12 center_text">
                          	<p><span>Status</span>
                            Quoted: 15 &nbsp;&nbsp;&nbsp; Booked: 152</p>
                            <p><span>Sales Person</span>
                            John Doe &nbsp;&nbsp;&nbsp; Contact: 292-292-2822</p> 
                        </div>
                	</div>
                    <div class="row career_search">
                    	<div class="col-lg-3 col-md-6 col-xs-12">
                    		<form class="" role="search">
								<div class="input-group add-on" ng-controller="CommonAppController">
									<div angucomplete-alt id="client_search" placeholder="{{ $cname }}" pause="200" remote-url="/front/list-clients?q=" search-fields="name" title-field="name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="client_search" selected-object="customerSelected"> </div>
									<div class="input-group-btn">
										@if($isNotOriginalUser)
											<button class="btn btn-default" ng-click="resetLoggedInClient()"><i class="fa fa-times"></i></button>
										@else
											<button class="btn btn-default"><i class="fa fa-search"></i></button>
										@endif
									</div>
								</div>
                                <!-- <div class="input-group add-on">
                                  <input type="text" class="form-control" placeholder="Search Career" name="srch-term" id="srch-term">
                                  <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                  </div>
                                </div> -->
                              </form>
                       	</div>
                        <div class="col-lg-3 col-md-6 col-xs-12">
                        	<p>{{{ $address }}}, {{$city}}, {{ $state }} - {{ $postal }}</p>
                        </div>
                    </div>
        		</div>
      		</div>
    	</div>
	</section>