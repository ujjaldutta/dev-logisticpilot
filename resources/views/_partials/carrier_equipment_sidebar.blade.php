<!-- sidebar-->
  <aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search" ng-controller="CarrierEquipmentSidebarController">
    <div class="nav-col">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
            <div class="filter-head lanes_block">
                <h2>Lanes count by Carrier</h2>
                <div class="input-group add-on">
                  <input type="text" placeholder="Search" class="form-control" ng-model="sortByName">
				 <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </div> 
                </div> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table lancecount_table">
                <tbody>
                  <tr ng-repeat="carrier in carrierList|filter:sortByName|limitTo:carrierRecordLimit">
                    <td width="60%"><a href="#">@{{carrier.name}}</a></td>
                    <td width="25%"><a href="#">0</a></td>
                    <td>
                        <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                            Deatail
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="contract">Carrier Contracts</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" ng-click="gotoFuelSetup(carrier.id)"class="fuel">Carrier Fuel</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" ng-click="gotoAccessorialSetup(carrier.id)" class="fuel">Carrier Accessorial</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="services">Service Area</a></li>
                          </ul>
                        </div>
                    </td>
                  </tr>
				  <tr ng-show="carrierList.length == 0">
					<td colspan="3"><strong>No carrier found</strong></td>
				  </tr>
				  <tr ng-show="carrierList.length == 0">
					<td colspan="3"><div style="height: 32px"><div class="loading-carrier hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div></td>
				  </tr>
                </tbody>
              </table>
            </div>
            <div class="filter-head customer-info">
                <h2>Filter contract</h2>
                <!-- <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Customer">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Carrier">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Effective Date
                    </label>
                </div> -->
                <div class="row filter-date">
					<div class="col-md-10 col-sm-10">
						<label>Effective From:</label>
						<p class="input-group">
						  <input type="text" class="form-control" uib-datepicker-popup ng-model="filter.effectiveFrom" is-open="dtPickerstatus.fromOpened" ng-required="true" close-text="Close" placeholder="Effective From" datepicker-options="dateOptions" style="background:none" />
						  <span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openSortableFrom($event)"><i class="glyphicon glyphicon-calendar"></i></button>
						  </span>
						</p>
					</div>
				</div>
				<div class="row filter-date">
					<div class="col-md-10 col-sm-10">
						<label>Effective To:</label>
						<p class="input-group">
						  <input type="text" class="form-control" uib-datepicker-popup ng-model="filter.effectiveTo" is-open="dtPickerstatus.toOpened" ng-required="true" close-text="Close" placeholder="Effective To" datepicker-options="dateOptions" style="background:none" />
						  <span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openSortableTo($event)"><i class="glyphicon glyphicon-calendar"></i></button>
						  </span>
						</p>
					</div>
                </div>
				
                <!-- <div class="row">
                    <div class="col-sm-6">
                        <select class="form-control">
                            <option>IL</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control">
                            <option>CA</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div> -->
                <!-- <div class="checkbox">
                    <label>
                        <input type="checkbox">Rating Profile
                    </label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search Sales Rep">
                </div> -->
            </div>
            <div class="filter-date expiring_contracts">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Show Expiring Contracts
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li>
                        <div class="checkbox all">
                            <label>
                                <input type="checkbox">Show All
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">1 Month
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">2 Months
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">3 Months
                            </label>
                        </div>
                    </li>
                  </ul>
                </div>
            </div>
      	</div>
      </div>
    </div>
  </aside>
 <!-- /sidebar-->