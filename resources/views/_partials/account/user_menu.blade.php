<div class="btn-group">
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	<h4 class="hidden-xs">{{ Lang::get('navigation.welcome') }} <strong>{{{ Auth::user()->usr_firstname }}} {{{ Auth::user()->usr_lastname }}}</strong> </h4>
	<figure><img src="{{ asset('images/avtar.png') }}" alt=""> </figure>
	<span class="caret"></span></button>
	<ul class="dropdown-menu" role="menu">
	  <li><a href="#"><i class="fa fa-music"></i>activity log</a></li>
	  <li><a href="#"><i class="fa fa-laptop"></i>news feed</a></li>
	  <li><a href="#"><i class="fa fa-cogs"></i>settings</a></li>
	  <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i>logout</a></li>
	</ul>
</div>