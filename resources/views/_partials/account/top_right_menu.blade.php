<div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-power-off"></i><span>{{ Lang::get('navigation.logout') }}</span> <span class="caret"></span> </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">			  
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('/auth/logout') }}">{{ Lang::get('navigation.logout') }}</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url(\config::get('app.frontPrefix'). '/users/profile')}}/">{{ Lang::get('navigation.updateProfile') }}</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('/auth/change-password') }}">{{ Lang::get('navigation.changePassword') }}</a></li>
            </ul>
          </div>