  <aside class="leftnavbar-wrapper" style="margin-top: 0px;">
    <div class="nav-col">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
			<div class="filter-head shipment-status-list">
                <h2>{{ Lang::get('shipmentsidebar.sidebarHeading1') }}</h2>
				<button type="button" class="btn btn-update pull-right" ng-click="resetFilter('shipText')"><span>Reset</span></button>
                <div class="form-group">
					<div class="col-xs-9">
						<input type="text" class="form-control" placeholder="Name" ng-model="filter.shipText" ng-model-options="{debounce:{ 'default': 500, 'blur': 0 }}">
					</div>
					<!--div class="col-xs-3">
						<button ng-click="resetFilter('shipText')" class="btn">Reset</button>
					</div-->
					<div class="clearfix"></div>
                </div>
           	</div>		
          <div class="filter-head shipment-status-list">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading2') }}
              <!--button type="button" class="btn btn-update pull-right"><span>Update</span></button-->
              <p>Up to date</p>
            </h2>
			<!--input class="ng-hide" type="checkbox" name="post_status_booked" ng-model="filters.shp_postatusId_booked" value="1" id="chk_1">
			<input class="ng-hide" type="checkbox" name="post_status_dispatched" ng-model="filters.shp_postatusId_dispatched" value="2" id="chk_2">
			<input class="ng-hide" type="checkbox" name="post_status_transit" ng-model="filters.shp_postatusId_transit" value="3" id="chk_3">
			<input class="ng-hide" type="checkbox" name="post_status_outfordel" ng-model="filters.shp_postatusId_outfordel" value="4" id="chk_4">
			<input class="ng-hide" type="checkbox" name="post_status_delivered" ng-model="filters.shp_postatusId_delivered" value="5" id="chk_5">
			<input class="ng-hide" type="checkbox" name="post_status_requested" ng-model="filters.shp_postatusId_requested" value="6" id="chk_6">
			<input class="ng-hide" type="checkbox" name="post_status_carrier" ng-model="filters.shp_postatusId_carrier" value="7" id="chk_7"-->
            <ul class="list-group shipment-status">
              <li ng-repeat="shipStatusitem in shipStatus track by $index" id="li_@{{shipStatusitem.id}}" class="list-group-item" ng-click="checkStatus(shipStatusitem.id)"> <a href="javascript:void(0)" class="@{{statuscolors[$index]}}"> <span class="badge">@{{shipStatusitem.items}}</span><img src="{{ asset('images/booked-icon.png') }}" alt=""> @{{shipStatusitem.label}} </a><input class="ng-hide" type="checkbox" name="post_status_booked" ng-model="shipStatusitem.shp_postatusId" value="@{{shipStatusitem.id}}" id="chk_@{{shipStatusitem.id}}"> </li>
              <!--li id="li_2" class="list-group-item" ng-click="checkStatus('2')"> <a href="javascript:void(0)" class="dispatched"><span class="badge">350</span><img src="{{ asset('images/dispatched-icon.png') }}" alt=""> Dispatched </a> </li>
              <li id="li_3" class="list-group-item" ng-click="checkStatus('3')"> <a href="javascript:void(0)" class="transit"><span class="badge">150</span><img src="{{ asset('images/in-transit-icon.png') }}" alt=""> In Transit</a> </li>
              <li id="li_4" class="list-group-item" ng-click="checkStatus('4')"> <a href="javascript:void(0)" class="out-delivery"><span class="badge">850</span><img src="{{ asset('images/out-for-delivery-icon.png') }}" alt=""> Out for dlivery</a> </li>
              <li id="li_5" class="list-group-item" ng-click="checkStatus('5')"> <a href="javascript:void(0)" class="delivered"> <span class="badge">350</span><img src="{{ asset('images/delivered-icon.png') }}" alt=""> Delivered </a> </li>
              <li id="li_6" class="list-group-item" ng-click="checkStatus('6')"> <a href="javascript:void(0)" class="quote"> <span class="badge">25</span><img src="{{ asset('images/quote-requested-icon.png') }}" alt=""> Quote Requested </a> </li>
              <li id="li_7" class="list-group-item" ng-click="checkStatus('7')"> <a href="javascript:void(0)" class="waiting"> <span class="badge">450</span><img src="{{ asset('images/waiting-icon.png') }}" alt=""> Waiting for Carrier </a> </li-->
            </ul>
          </div>
          <div class="filter-head customer-info">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading3') }} </h2>
			
			 <select class="form-control" ng-options="item.id*1 as item.label for item in customerList" ng-model="filter.shp_clientlist" name="shp_clientlist" id="shp_clientlist" multiple="multiple">
				<option>Select</option>
			  </select>	
              <!--div ng-dropdown-multiselect="" options="customerList" selected-model="filter.shp_clientlist" extra-settings="insSettings" events="searchCustomerEvent"></div-->

            <!--select title="Basic example" multiple="multiple" name="example-basic" size="5">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option>
            </select-->
          </div>
          <div class="filter-head shipment-status">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading4') }} </h2>
            <!--select title="Basic" multiple="multiple" name="example-basic" size="5">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option>
            </select-->
			 <select class="form-control" ng-options="item.id*1 as item.label for item in carrierList" ng-model="filter.shp_carriertype" name="shp_carriertype" id="carriertype" multiple="multiple">
				<option>Select</option>
			  </select>	
              <!--div ng-dropdown-multiselect="" options="carrierList" selected-model="filter.shp_carriertype" extra-settings="insSettings" events="searchCarrierEvent"></div-->			  
          </div>
          <div class="filter-head shipment-status">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading5') }} </h2>
            <div class="">
              <!--label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                LTL </label>
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                TL </label-->
			  <select class="form-control" ng-options="item.id*1 as item.label for item in modeTypes" ng-model="filter.shp_loadstatusId" name="shp_loadstatusId" id="shp_loadstatusId" multiple="multiple">
				<option>Select</option>

			  </select>	
              <!--div ng-dropdown-multiselect="" options="modeTypes" selected-model="filter.shp_loadstatusId" extra-settings="insSettings" events="searchLoadEvent"></div-->			  
            </div>
          </div>
          <div class="filter-head shipment-status">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading6') }} </h2>
            <!--select title="Basic" multiple="multiple" name="example-basic" size="5">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option>
            </select-->
			<select class="form-control" ng-options="item.id*1 as item.label for item in equipTypes" ng-model="filter.shp_equipmentId" name="shp_equipmentId" id="equipment_id" multiple="multiple"></select>
			<!--div ng-dropdown-multiselect="" options="equipTypes" selected-model="filter.shp_equipmentId" extra-settings="insSettings" events="searchEquipmentEvent"></div-->	
          </div>
          <div class="filter-head shipment-status">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading7_fromstate') }}</h2>
			
            <select title="Basic" name="shp_from_state" ng-model="filter.shp_from_state" ng-options="item.state_code as item.state_code+' '+item.state_name for item in stateList" size="5">
              <!--option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option-->
            </select>
             <span style="padding-left:20px; padding-right:20px;">to</span>
            <select title="Basic" name="shp_to_state" ng-model="filter.shp_to_state" ng-options="item.state_code as item.state_code+' '+item.state_name for item in stateList" size="5">			
              <!--option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
              <option value="option4">Option 4</option>
              <option value="option5">Option 5</option>
              <option value="option6">Option 6</option>
              <option value="option7">Option 7</option>
              <option value="option8">Option 8</option>
              <option value="option9">Option 9</option>
              <option value="option10">Option 10</option>
              <option value="option11">Option 11</option>
              <option value="option12">Option 12</option-->
            </select>			
          </div>
		  
          <div class="filter-head pricing">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading9') }}
              <button type="button" class="btn-reset pull-right"><span>Reset</span></button>
              <p>By Filter</p>
            </h2>
            <div class="slide-range">
              <!--slider-->
              <div class="slider">
                <input id="price_range2" />
              </div>
              <!--slider-->
            </div>
          </div>
          <div class="filter-head resp-block">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Sales Rep </label>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="Search Sales Rep">
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Show only Quotes </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Show Invoiced </label>
            </div>
          </div>
          <div class="filter-head edi-block">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading10') }}</h2>
            <h3>Pickup Requested (204)</h3>
            <div class="radio-cont">
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                  Yes </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  No </label>
              </div>
            </div>
            <h3>Invoice Received (210)</h3>
            <div class="radio-cont">
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                  Yes </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  No </label>
              </div>
            </div>
            <h3>Pro Number Updated</h3>
            <div class="radio-cont">
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                  Yes </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  No </label>
              </div>
            </div>
          </div>
          <div class="filter-head date-select clearfix">
            <h2>{{ Lang::get('shipmentsidebar.sidebarHeading11') }}
              <button type="button" class="btn-reset pull-right"><span>Reset</span></button>
              <p>By Filter</p>
            </h2>
            <div class="filter-date">
              <h3>Ship Date:</h3>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">To </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp1" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">From </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp2" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-date">
              <h3>Delivery Date :</h3>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">To </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp3" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">From </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp4" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-date">
              <h3>Pickup Date :</h3>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">To </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp5" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
              <div class="form-group clearfix clearfix">
                <div class="row">
                  <label class="col-sm-4 control-label" for="shipdate">From </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control jqdatepicker" id="dp6" placeholder="Eg..(12/12/2014)">
                  </div>
                </div>
              </div>
              <button type="button" class="btn btn-update pull-right"><span>Submit</span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <!-- /sidebar-->