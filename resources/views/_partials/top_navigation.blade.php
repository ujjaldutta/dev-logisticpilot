<?php
 $menuContent = \SiteAccess::getAppGroupswithApps(\Auth::user());
 //dd($menuContent);
?>

<ul id="magicArrow" class="nav navbar-nav">
			<li><a href="#" class="open_dashboard">Open</a></li>
			<!-- <li class="search-text menu-search"><span style="margin:6px 6px 0 0;display:inline-block; color:#ffffff;font-weight:700;">Search Customer:</span></li>
            <li style="" class="menu-search ">
              <form role="search" class="">
                <div class="input-group add-on" ng-controller="CommonAppController">
                  <!-- <input type="text" id="srch-term" name="srch-term" placeholder="Search" class="form-control"> -->
				  <?php
					$currentCustomer = CustomerRepository::getCurrentCustomer();
					$fname = isset($currentCustomer['client_firstname']) ? $currentCustomer['client_firstname'] : '';
					$lname = isset($currentCustomer['client_lastname']) ? $currentCustomer['client_lastname'] : '';
					$cname = isset($currentCustomer['client_compname']) ? $currentCustomer['client_compname'] : '';
					$isNotOriginalUser = (isset($currentCustomer['custID']) && $currentCustomer['custID'] != Auth::user()->clientID) ? 
						true : false;
				  ?>
				 <!-- <div angucomplete-alt id="client_search" placeholder="{{-- $cname --}}" pause="200" remote-url="/front/list-clients?q=" search-fields="name" title-field="name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="client_search" selected-object="customerSelected"> </div>
				  <div class="input-group-btn">
					@if($isNotOriginalUser)
						<button class="btn btn-default" ng-click="resetLoggedInClient()"><i class="fa fa-times"></i></button>
					@else
						<button class="btn btn-default"><i class="fa fa-search"></i></button>
					@endif
				  </div>
                </div>
              </form>
            </li> -->
			@foreach($menuContent as $content)
				<li class="dropdown current_page_item"> <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-set {{ $content['icon'] }}"></i><span class="hidden-sm">{{ $content['name'] or 'Unknown' }}</span><i class="fa fa-angle-down"></i></a>
				  <ul role="menu" class="dropdown-menu">
					@foreach($content['apps'] as $app)
						<li><a href="{{ url($app['action']) }}"><i class="icon-set {{ $app['icon'] }}"></i>{{ $app['name'] }}</a></li>
					@endforeach
				  </ul>
				</li>
			@endforeach
            <li id="magic-line" style="width: 139px;"></li>
</ul>