<?php
	$image = \CustomerRepository::getCustomerLogo(); 
	$image = !empty($image) ? asset(env('lpS3bucketUrl'). App\Http\Controllers\Front\ClientRegisterController::BRAND_IMAGE_DIRECTORY. '/'. $image) : 
		asset('images/logo.png');
	$tagline = \CustomerRepository::getCustomerTagline();	
?>
<div class="col-xs-12 col-sm-3 col-md-2">	
	<div class="logo"><a href="{{ url('/') }}"><img height="50" width="auto" src="{{ $image }}" alt="{{{ $tagline }}}"></a>
		<h2 class="tagline visible-sm">{{{ $tagline }}}</h2>
	</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 hidden-sm hidden-xs">
	<h2 class="tagline">{{{ $tagline }}}</h2>
</div>