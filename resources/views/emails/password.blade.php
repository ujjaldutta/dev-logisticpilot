@extends('layouts.email')
@section('title')
	Account Approved
@stop

@section('content')
	<tr>
    <td>Hello, </td>
  </tr>
  <tr>
    <td>Your have initiated a password reset request. Please find below the link to reset your password:</td>
  </tr>
  <tr>
	<td align="left">
		<table width="40%">
			<tr>
				<td>Click here to reset your password: {{ url('password/reset/'.$token) }}</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>Sincerely, </td>
  </tr>
  <tr>
    <td>Logistics Pilot System</td>
  </tr>
@stop