@extends('layouts.email')
@section('title')
	Account Approved
@stop

@section('content')
	<tr>
    <td>Hello, </td>
  </tr>
  <tr>
    <td>Your account has been set with a new password. Your password is: <strong>{{ $password }}</strong></td>
  </tr> 
  <tr>
    <td>Sincerely, </td>
  </tr>
  <tr>
    <td>Logistics Pilot System</td>
  </tr>
@stop