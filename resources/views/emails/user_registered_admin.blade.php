@extends('layouts.email')
@section('title')
	New User registered
@stop

@section('content')
	<tr>
    <td>Hello, </td>
  </tr>
  <tr>
    <td>The following user has been registered, please check the details below:</td>
  </tr>
  <tr>
	<td align="left">
		<table width="40%">
			<tr>
				<td>Name: <strong>{{{ $reg_usrfirstname }}} {{{ $reg_usrlastname}}} </strong></td>
			</tr>
			<tr>
				<td>Email: <strong>{{{ $reg_usremail }}}</strong></td>
			</tr>
			<tr>
				<td>Company Name: <strong>{{{ $reg_company }}}</strong></td>
			</tr>
			<tr>
				<td>Contact Number <strong>{{ $reg_cno }}</strong></td>
			</tr>
			<tr>
				<td>Plan: <strong>{{ $plan_name }}</strong></td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>Sincerely,</td>
  </tr>
  <tr>
    <td>Logistics Pilot System</td>
  </tr>
@stop