@extends('layouts.email')
@section('title')
	Account Approved
@stop

@section('content')
	<tr>
    <td>Hello {{ $fname }}, </td>
  </tr>
  <tr>
    <td>Your account has been approved successfully! Please find below the password and link to activate your account:</td>
  </tr>
  <tr>
	<td align="left">
		<table width="40%">
			<tr>
				<td>Login ID: <strong>{{ $loginid }} </strong></td>
			</tr>
			<tr>
				<td>Password: <strong>{{ $password }}</strong></td>
			</tr>
			<tr>
				<td>Link to activate: <strong>
				<a href="{{ url('/auth/activate/'.$token ) }}">{{ url('/auth/activate/'. $token) }}</a></strong></td>
			</tr>
			<tr>
				<td>Your Plan: <strong>{{ $plan }}</strong></td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>Sincerely, </td>
  </tr>
  <tr>
    <td>Logistics Pilot System</td>
  </tr>
@stop