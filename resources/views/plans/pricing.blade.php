@extends('layouts.master')

@section('page_title')
	Logistics Pilot | Pricing
@stop

@section('page_heading')
	Pricing Overview
@stop

@section('content')
	<section class="white-wrapper">
    	<div class="container">
        	<div class="general-title">
            	<h2>Pricing Table</h2>
                <hr>
                <p class="lead">We provide best solution for your business</p>
            </div><!-- end general title -->
            <div class="doc">
                <div class="pricing_boxes">
					@forelse($plans as $key => $plan)
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 {{ !$key ? 'first' : '' }}">
							<div class="pricing_detail priceamount-01" style="background: none repeat scroll 0 0 {{ $plan->colorcode }}">
								<span class="priceamount"><span>{{ $plan->price == 0.00 ? 'FREE' : sprintf("%d", $plan->price) }}</span><br>Monthly</span>
								<header>
									<h3>{{ strtoupper($plan->name) }}</h3>
								</header><!-- end header -->
								<div class="pricing_info">
									<ul>
										{!! $plan->description !!}
									</ul>
									<footer>
										<a href="{{ URL::to('/auth/register/'. $plan->id) }}" class="btn btn-primary btn-lg">Purchase</a>
									</footer>
								</div><!-- end pricing-info -->
							</div><!-- end pricing_detail -->
						</div><!-- end col-lg-3 -->
					@empty
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 {{ !$key ? 'first' : '' }}">
							<div class="pricing_detail priceamount-01">
								<h3> No Plan Found </h3>
							</div><!-- end pricing_detail -->
						</div><!-- end col-lg-3 -->
					@endforelse
                </div><!-- end pricing_boxes -->
			</div><!-- end doc -->
		</div><!-- end container -->
    </section><!-- end white-wrapper -->
@stop