@extends('layouts.account')
@section('page_title')
	Change Password
@stop
@section('content')
	<h1>Change Your Password</h1>
	@yield('message')
		@if(Session::has('success'))
			<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<p> {{ Session::get('success') }}	</p>	
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<p> {{ Session::get('error') }}	</p>	
			</div>
		@endif
		@if($errors->has())
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
			@foreach ($errors->all() as $error)
					<p> {{ $error }}</p>
			@endforeach
			</div>
		@endif
	<div ng-controller="passwordChangeController">
	<form name="password_change" action="{{ url('/auth/change-password') }}" id="password_change" method="POST" novalidate>
		<input type="hidden" name="_token" value="{{ csrf_token() }}" required/> 
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<div class="form-group" ng-class="{'has-error': password_change.old_password.$dirty && password_change.old_password.$invalid}">
					<label for="old_password">Old Password</label>
					<input type="password" name="old_password" id="old_password" class="form-control" required ng-model="password.old_password">
					<span class="help-block" ng-show="password_change.old_password.$dirty && password_change.old_password.$invalid">
						<span ng-show="password_change.old_password.$error.required">Old Password is required.</span>
					</span>
				</div>
				<div class="form-group" ng-class="{'has-error': password_change.password.$dirty && password_change.password.$invalid}">
					<label for="password">New Password</label>
					<input type="password" name="password" id="password" class="form-control" required ng-model="password.password">
					<span class="help-block" ng-show="password_change.password.$dirty && password_change.password.$invalid">
						<span ng-show="password_change.password.$error.required">New Password is required.</span>
					</span>
				</div>
				<div class="form-group" ng-class="{'has-error': password_change.password_confirmation.$dirty && password_change.password_confirmation.$invalid}">
					<label for="password_confirmation">Confirm New Password</label>
					<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" ng-model="password.password_confirmation" required data-password-verify="password.password">
					
					<span class="help-block" ng-show="password_change.password_confirmation.$dirty && password_change.password_confirmation.$invalid">
						<span ng-show="password_change.password_confirmation.$error.required">New Password confirmation is required.</span>
						<span ng-show="password_change.password_confirmation.$error.passwordVerify">New Password Doesn't Match.</span>
					</span>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" ng-disabled="password_change.$invalid" ng-click="requestChange(password_change)" value="@{{ btnText }}">
				</div>
			</div>
		</div>
	</form>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/password_change.js') }}"></script>
@stop