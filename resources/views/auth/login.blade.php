@extends('layouts.master')
@section('page_title')
	Logistics Pilot | Login
@stop
@section('page_heading')
	Login
@stop
@section('content')
	<section class="blog-wrapper">
    	<div class="container">
        	<div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-controller="loginController">
                <div class="row">
                   	<div class="col-md-4">
 						<div class="widget">
                        	<div class="title">
                            	<h3>Login to Your Account</h3>
                            </div><!-- end title -->
							@yield('message')
								@if(Session::has('success'))
									<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<p> {{ Session::get('success') }}	</p>	
									</div>
								@endif
								@if(Session::has('error'))
									<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<p> {{ Session::get('error') }}	</p>	
									</div>
								@endif
								@if($errors->has())
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert">
											<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
										</button>
									@foreach ($errors->all() as $error)
											<p> {{ $error }}</p>
									@endforeach
									</div>
								@endif
							<form id="loginform" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}" name="loginform" novalidate>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group" ng-class="{'has-error' : loginform.email.$dirty && loginform.email.$invalid}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="email" class="form-control" name="email" placeholder="Email" required ng-model="email">
									</div>
									<span class="help-block" ng-show="loginform.email.$dirty && loginform.email.$invalid">
											<span ng-show="loginform.email.$error.required">Email is required.</span>
											<span ng-show="loginform.email.$error.email">Invalid email address.</span>
									</span>
								</div>
								<div class="form-group" ng-class="{'has-error' : loginform.password.$dirty && loginform.password.$invalid}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" name="password" placeholder="Password" ng-model="password" required>
									</div>
									<span class="help-block" ng-show="loginform.password.$dirty && loginform.password.$invalid">
											<span ng-show="loginform.password.$error.required">Password is required.</span>
									</span>
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label> 
											<input type="checkbox" name="remember"> Remember Me
										</label>
									</div>
								</div>
								<div class="form-group">
								<button class="btn btn-primary" ng-disabled="loginform.email.$invalid || loginform.password.$invalid" ng-click="submitLogin(loginform)">@{{buttonText}}</button>
								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
								</div>
							</form>
                        </div><!-- end widget -->
					</div><!-- end col-md-4 -->
                    <div class="col-md-4"><img src="{{ asset('images/login-bg.png') }}" class="img-responsive"></div>
            		</div><!-- end col-md-4 -->
            	</div><!-- end row --> 
            </div><!-- end content -->
    	</div><!-- end container -->
    </section><!-- end white-wrapper -->
@stop
@section('scripts')
	<script src="{{ asset('js/login.js') }}"></script>
@stop