@extends('layouts.master')

@section('page_title')
	Logistics Pilot | Register
@stop

@section('page_heading')
	Login & Register
@stop

@section('content')
<section class="blog-wrapper">
    	<div class="container" ng-controller="registrationController">
        	<div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                	<div class="col-md-1 col-md-1 col-sm-12 col-xs-12">&nbsp;</div>
                    <div class="col-md-6 col-md-8 col-sm-12 col-xs-12"><img src="{{ asset('images/register.png') }}" class="img-responsive"></div>
                   	<div class="col-md-4 col-md-4 col-sm-12 col-xs-12">
 						<div class="widget">
                        	<div class="title">
                            	<h3>Create An Account</h3>
                            </div><!-- end title -->
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<strong>Whoops!</strong> There were some problems with your input.<br><br>
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
                                
								<form id="registerform" method="post" name="registerform" action="{{ url('/auth/register') }}" novalidate>
									<input type="hidden" name="_token" value="{{ csrf_token() }}" ng-init="user._token='{{ csrf_token() }}'">
									<input type="hidden" name="subscription_plan_id" value="{{ $subscription_plan_id }}" ng-init="user.reg_subsciption_plan_id='{{ $subscription_plan_id }}'" required>
                                    
									<div class="form-group" ng-class="{'has-error': registerform.fname.$dirty && registerform.fname.$invalid}">
                                        <input type="text" name="fname" class="form-control" placeholder="First name" ng-model="user.reg_usrfirstname" required>
										<span class="help-block" ng-show="registerform.fname.$dirty && registerform.fname.$invalid">
											<span ng-show="registerform.fname.$error.required">First Name is required.</span>
										</span>
                                    </div>
                                    <div class="form-group" ng-class="{'has-error': registerform.lname.$dirty && registerform.lname.$invalid}">
                                        <input type="text" name="lname" class="form-control" placeholder="Last name" ng-model="user.reg_usrlastname" required>
										<span class="help-block" ng-show="registerform.lname.$dirty && registerform.lname.$invalid">
											<span ng-show="registerform.lname.$error.required">Last Name is required.</span>
										</span>
                                    </div>
                                    <div class="form-group" ng-class="{'has-error': registerform.email.$dirty && registerform.email.$invalid}">
                                        <input type="email" name="email" class="form-control" placeholder="Email" ng-model="user.reg_usremail" required>
										<span class="help-block" ng-show="registerform.email.$dirty && registerform.email.$invalid">
											<span ng-show="registerform.email.$error.required">Email is required.</span>
											<span ng-show="registerform.email.$error.email">Valid Email is required.</span>
										</span>
                                    </div>
                                    <div class="form-group" ng-class="{'has-error': registerform.company_name.$dirty && registerform.company_name.$invalid}">
                                        <input type="text" name="company_name" class="form-control" placeholder="Company Name" ng-model="user.reg_company" required>
										<span class="help-block" ng-show="registerform.company_name.$dirty && registerform.company_name.$invalid">
											<span ng-show="registerform.company_name.$error.required">Company Name is required.</span>
										</span>
                                    </div>
                                    <div class="form-group" ng-class="{'has-error': registerform.cno.$dirty && registerform.cno.$invalid}">
                                        <input type="text" name="cno" class="form-control" placeholder="Contact Phone" ng-model="user.reg_cno" required ng-pattern="/^([0-9]{5,12})$/">
										<span class="help-block" ng-show="registerform.cno.$dirty && registerform.cno.$invalid">
											<span ng-show="registerform.cno.$error.required">Contact number is required.</span>
											<span ng-show="registerform.cno.$error.pattern">Please write a valid phone</span>
										</span>
                                    </div>
									<div class="form-group" ng-class="{'has-error': registerform.captcha.$dirty && registerform.captcha.$invalid}">
										<p><img ng-src="@{{user.captchasrc}}" ng-init="user.captchasrc='{{ captcha_src() }}'" alt="captcha" /> <span> <button class="btn" type="button" ng-click="refreshCaptcha()"><i class="fa fa-refresh"></i>
</button></span></p>
										<input type="text" name="captcha" class="form-control" placeholder="Captcha" ng-model="user.captcha" required>
										<span class="help-block" ng-show="registerform.captcha.$dirty && registerform.captcha.$invalid">
											<span ng-show="registerform.captcha.$error.required">Please enter the captcha code.</span>
										</span>
                                    </div>
                                    <div class="form-group">
                                        <input type="button" class="btn btn-primary" value="Register an account" ng-disabled="registerform.$invalid" ng-click="handleRegistration(registerform)">
										<img src="{{ asset('images/loading.gif') }}" alt="Loading.." height="32" width="32" ng-show="processing.status" /> 
                                    </div>
                                </form>
								<div class="row">
									
								</div>
								<div class="alert alert-success" ng-show="success.status">
									<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
									<strong><span ng-bind="success.message"></span></strong>
								</div>
								<div class="alert alert-danger" ng-show="error.status">
									<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
									<strong><span ng-bind="error.message"></span></strong>
								</div>
                        </div><!-- end widget -->
					</div><!-- end col-md-4 -->
                    <div class="col-md-1 col-md-1 col-sm-12 col-xs-12">&nbsp;</div>
            	</div><!-- end row --> 
            </div><!-- end content -->
    	</div><!-- end container -->
    </section><!-- end white-wrapper -->
@endsection
@section('scripts')
	<script src="{{ asset('js/register.js') }}"></script>
@stop