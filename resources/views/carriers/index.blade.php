@extends('layouts.user_home')
@section('angular_controller')
CarrierManagerController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('setup_carrier') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/carrier_management_sidebar')
@stop
@section('top_content')

@stop

@section('content')
<!-- content section-->
<section class="content-wrapper">
    <div class="content-subcontainer">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-xs-12">               
                <div class="contract_table" style="min-height: 797px;">
                     <h2>Carrier Onboard</h2>
                    <div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
                    <div class="clearfix"></div>
                    <div class="row contract_buttons">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
                            <a href="{{url(\Config::get('app.frontPrefix').'/carriers/download')}}" class="btn btn-primary download_excel"><span></span>Download to Excel</a>
                            <a href="{{url(\Config::get('app.frontPrefix').'/carriers/add')}}" class="btn btn-primary download_excel">Add Carrier</a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 pull-left">
                            <button type="button" class="btn btn-default" ng-click="deleteAllCarrier()"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>

                    <div class="table-responsive clearfix customer-table-transit">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                            <tr>
                                <th align="center" valign="middle" scope="col" width="25px">
                            <div class="checkbox" style="width:15px;">
                                <label>
                                    <input type="checkbox" ng-click="checkAll()" ng-model="carrierData.selectAll">
                                </label>
                            </div>
                            </th>	
                            @foreach($allFields as $field)
                            <th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('{{$field -> displayField}}')">{{ $field -> displayName}} <i class="fa " ng-show="predicate === '{{$field -> displayField}}'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>					
                            @endforeach
                            <th align="left" valign="middle" scope="col">&nbsp;</th>
                            </tr>					
                            <tr ng-repeat="carrier in carrierData.list|filter:filterCarrierListing|orderBy:predicate:reverse">
                                <td align="center" valign="middle" width="25px">
                                    <div class="checkbox" style="width:15px;">
                                        <label>
                                            <input type="checkbox" ng-model="carrier.selected" ng-change="checkSelection($index)">
                                        </label>
                                    </div>
                                </td>

                                @foreach($allFields as $field)
                                <td align="left" valign="middle">
                                    <a href="{{ url(\config::get('app.frontPrefix').'/carriers/edit')}}/@{{carrier.id}}" ng-click="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
                                        <?php
                                            echo "{{ carrier." . $field->displayField . "|formatByType:'" . $field->fieldType . "'}}";
                                        ?>
                                    </a></td>						
                                @endforeach
                                <td align="center" valign="middle">
                                    <a style="width:67px;margin-bottom:3px;" href="{{ url(\config::get('app.frontPrefix').'/carriers/edit')}}/@{{carrier.id}}" class="" title="Edit"><i class="fa fa-edit"></i></a>
                                   
                                    <a class="" href="javascript:;" class="" ng-click="deleteCarrier(carrier.id, true)"><i class="fa fa-trash"></i></a>
                                </td>						

                            </tr>
                            <tr ng-hide="carrierData.list.length">
                                <td colspan="12" align="center"><strong>No Record Found!</strong></td>
                            </tr>
                        </table>
                        <p>Showing record @{{(recordLimit * currentPage) > carrierData.count ? carrierData.count : recordLimit * currentPage}} of @{{carrierData.count}}</p>
                    </div>

                    <div class="text-right">
                        <pagination total-items="carrierData.count" items-per-page="recordLimit" ng-model="currentPage" ng-change="pageChanged()"></pagination>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CarrierManager.js') }}"> </script>
@endsection
