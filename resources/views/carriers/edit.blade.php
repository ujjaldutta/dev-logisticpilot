@extends('layouts.user_home')
@section('angular_controller')
CarrierEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('edit_carrier') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/carrier.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/carrier_management_sidebar')
@stop
@section('top_content')	
@stop

@section('content')
<!-- content section-->
<section class="content-wrapper" >
    <div class="contract_table">
        <div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
        <div class="clearfix"></div>
        <div id="horizontalTab1" class="tab-hld easytabs">
            <ul class="resp-tabs-list">
                <li>Carrier</li>
                <li>Carrier Insurance</li>
                <li>Carrier Mode</li>
                <li>Carrier Equipment</li>
                <li>Carrier Remit</li>
            </ul>
            <div class="resp-tabs-container">
                <div class="tab-content">
                    <div class="row">
                        <form id="carrierProfileForm" name="carrier" enctype="multipart/form-data" novalidate ng-init="carrID ='{{$carrier_id}}'">
                            <div class="col-lg-3">
                                <div class="form-group" ng-class="{'has-error': carrier.scac.$invalid}">
                                    <label>{{ Lang::get('carrier.addCarrierCode') }} *</label>
                                    <input type="text" name="scac" class="form-control" placeholder="" ng-model="carrierData.scac" required>
                                    <span class="help-block" ng-show="carrier.scac.$invalid">
                                        <span ng-show="carrier.scac.$error.required">{{ Lang::get('carrier.addCarrierValidationScacRequired') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': carrier.carrier_name.$invalid}">
                                    <label>{{ Lang::get('carrier.addCarrierName') }} *</label>
                                    <input type="text" name="carrier_name" class="form-control" placeholder="" ng-model="carrierData.carrier_name" required>
                                    <span class="help-block" ng-show="carrier.carrier_name.$invalid">
                                        <span ng-show="carrier.carrier_name.$error.required">{{ Lang::get('carrier.addCarrierValidationNameRequired') }}</span>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('carrier.addCarrierAddr1') }}</label>
                                    <input type="text" name="carrier_adr1" class="form-control" placeholder="" ng-model="carrierData.carrier_adr1"> 

                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('carrier.addCarrierAddr2') }}</label>
                                    <input type="text" name="carrier_adr2" class="form-control" placeholder=""  ng-model="carrierData.carrier_adr2">

                                </div>
                                <div class="form-group" ng-class="{'has-error': carrier.location.$invalid}">
                                    <label>{{ Lang::get('carrier.addCarrierLocation') }} *</label>
                                    <input type="text" name="location" class="form-control" placeholder="" g-places-autocomplete ng-model="carrierData.location" force-selection="true" options="autocompleteOption" required>

                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('carrier.addCarrierReferenceCode') }}</label>
                                    <input type="text" name="carrier_xrefcode" class="form-control" placeholder="" ng-model="carrierData.carrier_xrefcode">
                                </div>                                
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>{{ Lang::get('carrier.addCarrierMC') }}</label>
                                    <input type="text" name="carrier_mc" class="form-control" placeholder="" ng-model="carrierData.carrier_mc">

                                </div>
                                <div class="form-group" ng-class="{'has-error': carrier.carrier_email.$invalid}">
                                    <label>{{ Lang::get('carrier.addCarrierEmail') }}</label>
                                    <input type="email" name="carrier_email" class="form-control" placeholder="" ng-model="carrierData.carrier_email">
                                    <span class="help-block" ng-show="carrier.carrier_email.$invalid">
                                        <span ng-show="carrier.carrier_email.$error.email">{{ Lang::get('carrier.addCarrierValidationEmailInvalid') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': carrier.carrier_phone.$invalid}">
                                    <label>{{ Lang::get('carrier.addCarrierPhone') }}</label>
                                    <input type="text" name="carrier_phone" class="form-control" placeholder="" ng-model="carrierData.carrier_phone" ng-pattern="/^([0-9]{5,12})$/"> 
                                    <span class="help-block" ng-show="carrier.carrier_phone.$invalid">
                                        <span ng-show="carrier.carrier_phone.$error.pattern">{{ Lang::get('carrier.addCarrierValidationPhoneInvalid') }}</span>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>FAX</label>
                                    <input type="text" name="carrier_fax" class="form-control" placeholder=""  ng-model="carrierData.carrier_fax" ng-pattern="/^([0-9]{5,12})$/">

                                </div>
                                <div class="form-group" ng-hide="!carrID">
                                    <label>{{ Lang::get('carrier.addCarrierUploadLogo') }}</label>
                                    <div class="file_upload">
                                        <button class="btn btn-primary font-color-white" type="button" ngf-select ngf-change="upload($files)" accept="image/*" >{{ Lang::get('carrier.addCarrierUploadLogoButton') }}</button>
                                        <img ngf-src="files[0]" ng-show="files[0].type.indexOf('image') > - 1" class="thumb">
                                        <p>@{{carrierData.log}}</p>
                                        <p>( {{ Lang::get('carrier.addCarrierLogoMessage') }})</p>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="col-lg-6">
                                <div class="col-lg-6 div-padd-0 form-group div-top-20">
                                    <div class="col-lg-12 div-padd-0">
                                        <div class="col-lg-6">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_rateapi"> {{ Lang::get('carrier.addCarrierRateapi') }}
                                            </label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_bookingapi"> {{ Lang::get('carrier.addCarrierBookingapi') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 div-padd-0">
                                        <div class="col-lg-6">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_trackingapi">  {{ Lang::get('carrier.addCarrierTrackingapi') }}
                                            </label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_imageapi"> {{ Lang::get('carrier.addCarrierImageapi') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>                             
                                <div class="col-lg-12" style="margin-top:-11px;">                                    
                                    <label>{{ Lang::get('carrier.carrierType') }} </label>
                                    <div ng-dropdown-multiselect="" options="carrierTypes" selected-model="carrierData.carrier_type" extra-settings="carrSettings"></div>
                                </div>                                
                                
                                <div class="col-lg-12 div-padd-0 div-top-20">
                                        <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_smartwaypay"> {{ Lang::get('carrier.addCarrierSmartwayPay') }}
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_mexicoauth"> {{ Lang::get('carrier.addCarrierMexicoAuth') }}
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_canauth"> {{ Lang::get('carrier.addCarrierCanadaAuth') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 div-padd-0">
                                        <div class="col-lg-4">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_acceptbid">  {{ Lang::get('carrier.addCarrierAcceptBid') }}
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                <input type="checkbox" ng-model="carrierData.carrier_safetyrating"> {{ Lang::get('carrier.addCarrierSafityRating') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 div-padd-0 div-top-20">
                                    <div class="form-group col-lg-4">
                                        <label>
                                            <input type="checkbox" ng-model="carrierData.carrier_hazmat"> {{ Lang::get('carrier.addCarrierHazmat') }}
                                        </label>
                                    </div>                                        
                                </div>
                                
                                <div class="col-lg-12 div-padd-0">
                                    <div class="form-group col-lg-4">
                                        <label>{{ Lang::get('carrier.addCarrierHazmatContact') }}</label>
                                        <input type="text" name="carrier_hazmatcontact" class="form-control" placeholder="" ng-model="carrierData.carrier_hazmatcontact">
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>{{ Lang::get('carrier.addCarrierHazmatPhone') }}</label>
                                        <input type="text" name="carrier_hazmatphone" class="form-control" placeholder="" ng-model="carrierData.carrier_hazmatphone" ng-pattern="/^([0-9]{5,12})$/">
                                    </div>
                                </div>

                                <div class="col-lg-12 div-padd-0">
                                    <div class="form-group col-lg-4">
                                        <label>{{ Lang::get('carrier.currencyType') }} </label>
                                        <div ng-dropdown-multiselect="" options="currencyTypes" selected-model="carrierData.carrier_currencycode" extra-settings="currencySettings"></div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="clearfix"></div>
                            <div class="btn-cont" style="padding-top:20px;">
                                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">{{ Lang::get('carrier.cancelButton') }}</button>
                                <button  type="button" class="btn btn-primary font-color-white" ng-disabled="carrier.$invalid" ng-click="saveCarrierData(carrier)">{{ Lang::get('carrier.saveOrUpdateButton') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="row">
                        <form id="carrierInsuranceForm" name="carrierInsurance" enctype="multipart/form-data" novalidate >
                            <div ng-repeat="insuranceItem in carriers.insurances">
                                <div class="row">
                                    <div class="col-lg-11 div-padd-left">
                                        <div class="col-lg-3">
                                            <div class="form-group" ng-class="{'has-error': carrierInsurance.insuranceTypeId_@{{$index}}.$invalid}">
                                                <label>{{ Lang::get('carrier.insurance') }} *</label>
                                                <select ng-model="insuranceItem.insuranceTypeId" ng-options="item.id as item.codeValue for item in insuranceTypes" name="insuranceTypeId_@{{$index}}" class="form-control" required></select>
                                                <span class="help-block" ng-show="carrierInsurance.insuranceTypeId_@{{$index}}.$invalid">
                                                    <span ng-show="carrierInsurance.insuranceTypeId_@{{$index}}.$error.required">Insurance Type is required</span>
                                                </span>
                                            </div>                              
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insurancePolicyName') }}</label>
                                                <input type="text" name="insurance_policyname_@{{$index}}" ng-model="insuranceItem.insurance_policyname" class="form-control" placeholder="" >                                                
                                            </div>                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insurancePolicyAmount') }}</label>
                                                <input type="text" name="insurance_amount_@{{$index}}" ng-model="insuranceItem.insurance_amount" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group clearfix date-pick">
                                                <label>{{ Lang::get('carrier.insuranceExpiryDate') }}</label>
                                                <datepickerkb date-format="yyyy-MM-dd" ng-init="item.insurance_effto ='{{ date('Y-m-d')}}'">
                                                    <input ng-model="insuranceItem.insurance_effto" class="form-control" type="text" placeholder="Eg..(2015/12/31)" name="insurance_effto_@{{$index}}" />
                                                </datepickerkb>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-11 div-padd-left">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insuranceCompanyName') }}</label>
                                                <input type="text" name="insurance_companyname_@{{$index}}" ng-model="insuranceItem.insurance_companyname" class="form-control" placeholder="" >
                                            </div>                                    
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insuranceAddress') }} </label>
                                                <input type="text" name="insurance_address1_@{{$index}}" ng-model="insuranceItem.insurance_address1" class="form-control" placeholder="" >

                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insuranceCityStateZip') }}</label>
                                                <input type="text" name="insurance_location_@{{$index}}" ng-model="insuranceItem.insurance_location" g-places-autocomplete class="form-control" placeholder="" force-selection="true" options="autocompleteOption" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.insuranceContact') }} </label>
                                                <input type="text" name="insurance_agentname_@{{$index}}" ng-model="insuranceItem.insurance_agentname" class="form-control" placeholder="" >

                                            </div>
                                        </div>
                                        <div class="btn-cont">
                                            <button type="button" class="btn btn-default font14" ng-click="removeItem($index)" ng-disabled="$index == 0"><i class="fa fa-trash-o"></i> {{ Lang::get('carrier.removeInsurance') }}</button>
                                        </div>
                                    </div>
                                </div>   
                            </div>

                            <div class="row div-padd-left">
                                <div class="col-lg-11">
                                    <a href="#" ng-click="addItem()"><span class="icon-size"><i class="fa fa-plus-circle"></i></span> {{ Lang::get('carrier.addAnotherInsurance') }}</a>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="btn-cont" style="padding-top:20px;">
                                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">{{ Lang::get('carrier.cancelButton') }}</button>
                                <button  type="button" class="btn btn-primary font-color-white" ng-disabled="carrierInsurance.$invalid" ng-click="saveInsurances(carrierInsurance)">{{ Lang::get('carrier.saveOrUpdateButton') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content">                    
                    <div class="row">
                        <form id="carrierModeForm" name="carrierMode" enctype="multipart/form-data" novalidate >
                            <div ng-repeat="modeItem in carriers.modes">
                                <div class="row">
                                    <div class="col-lg-11 div-padd-left">
                                        <div class="col-lg-3">
                                            <div class="form-group" ng-class="{'has-error': carrierMode.modeId_@{{$index}}.$invalid}">
                                                <label>{{ Lang::get('carrier.mode') }} *</label>
                                                <select ng-model="modeItem.modeId" ng-options="item.id as item.codeValue for item in modeTypes" name="modeId_@{{$index}}" class="form-control" required></select>
                                                <span class="help-block" ng-show="carrierMode.modeId_@{{$index}}.$invalid">
                                                    <span ng-show="carrierMode.modeId_@{{$index}}.$error.required">Mode Type is required</span>
                                                </span>
                                            </div>                              
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.modeMinWeight') }}</label>
                                                <input type="text" name="minweight_@{{$index}}" ng-model="modeItem.minweight" class="form-control" placeholder="" >                                                
                                            </div>                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.modeMaxWeight') }}</label>
                                                <input type="text" name="maxweight_@{{$index}}" ng-model="modeItem.maxweight" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>{{ Lang::get('carrier.modeComments') }}</label>
                                                <input type="text" name="comments_@{{$index}}" ng-model="modeItem.comments" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="btn-cont">
                                            <button type="button" class="btn btn-default font14" ng-click="removeModeItem($index)" ng-disabled="$index == 0"><i class="fa fa-trash-o"></i> {{ Lang::get('carrier.removeMode') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row div-padd-left">
                                <div class="col-lg-11">
                                    <a href="#" ng-click="addModeItem()"><span class="icon-size"><i class="fa fa-plus-circle"></i></span> {{ Lang::get('carrier.addAnotherMode') }}</a>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="btn-cont" style="padding-top:20px;">
                                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">{{ Lang::get('carrier.cancelButton') }}</button>
                                <button  type="button" class="btn btn-primary font-color-white" ng-disabled="carrierMode.$invalid" ng-click="saveModes(carrierMode)">{{ Lang::get('carrier.saveOrUpdateButton') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">               
                            <div class="" style="min-height: 797px;">
                                <div class="clearfix"></div>
                                <div class="row contract_buttons">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
                                        <a href="{{url(\Config::get('app.frontPrefix').'/carriers/equipment-download')}}/@{{carrID}}" class="btn btn-primary download_excel font-color-white"><span></span>Download to Excel</a>
                                        <a href="{{url(\Config::get('app.frontPrefix').'/carriers/edit/equipments/add')}}/@{{carrID}}" class="btn btn-primary download_excel font-color-white" ng-disabled="isExistCarrID()">Add Equipment</a>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 pull-left">
                                        <button type="button" class="btn btn-default delete_button" ng-click="deleteAllEquipment()" ><span></span>Delete</button>
                                    </div>
                                </div>

                                <div class="table-responsive clearfix customer-table-transit">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                        <tr>
                                            <th align="center" valign="middle" scope="col" width="25px">
                                        <div class="checkbox" style="width:15px;">
                                            <label>
                                                <input type="checkbox" ng-click="checkAll()" ng-model="equipmentData.selectAll">
                                            </label>
                                        </div>
                                        </th>	
                                        @foreach($allFields as $field)
                                        <th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('{{$field->displayField}}')">{{ $field->displayName}} <i class="fa " ng-show="predicate === '{{$field->displayField}}'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>					
                                        @endforeach
                                        <th align="left" valign="middle" scope="col">&nbsp;</th>
                                        </tr>					
                                        <tr ng-repeat="equipment in equipmentData.list|filter:filterEquipmentListing|orderBy:predicate:reverse">
                                            <td align="center" valign="middle" width="25px">
                                                <div class="checkbox" style="width:15px;">
                                                    <label>
                                                        <input type="checkbox" ng-model="equipment.selected" ng-change="checkSelection($index)">
                                                    </label>
                                                </div>
                                            </td>

                                            @foreach($allFields as $field)
                                            <td align="left" valign="middle">
                                                <a href="#" ng-click="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
                                                    <?php
                                                    echo "{{ equipment." . $field->displayField . "|formatByType:'" . $field->fieldType . "'}}";
                                                    ?>
                                                </a></td>						
                                            @endforeach
                                            <td align="center" valign="middle">
                                                <a style="width:67px;margin-bottom:3px;" href="{{url(\Config::get('app.frontPrefix').'/carriers/edit/equipments/edit')}}/@{{carrID}}/@{{equipment.id}}" class="btn btn-default edit_button"><span></span>Edit</a>
                                                <br/>
                                                <button type="button" class="btn btn-default delete_button" ng-click="deleteEquipment(equipment.id, true)"><span></span>Delete</button>
                                            </td>						

                                        </tr>
                                        <tr ng-hide="equipmentData.list.length">
                                            <td colspan="7" align="center"><strong>No Record Found!</strong></td>
                                        </tr>
                                    </table>
                                    <p>Showing record @{{(recordLimit * currentPage) > equipmentData.count ? equipmentData.count : recordLimit * currentPage}} of @{{equipmentData.count}}</p>
                                </div>

                                <div class="text-right">
                                    <pagination total-items="equipmentData.count" items-per-page="recordLimit" ng-model="currentPage" ng-change="pageChanged()"></pagination>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="tab-content">
                    <div class="row">
                       <div class="col-lg-12 col-md-12 col-xs-12">               
                            <div class="" style="min-height: 797px;">
                                <div class="clearfix"></div>
                                <div class="row contract_buttons">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
                                        <a href="{{url(\Config::get('app.frontPrefix').'/carriers/remit-download')}}/@{{carrID}}" class="btn btn-primary download_excel font-color-white" ng-disabled="isExistCarrID()"><span></span>Download to Excel</a>
                                        <a href="{{url(\Config::get('app.frontPrefix').'/carriers/edit/remits/add')}}/@{{carrID}}" class="btn btn-primary download_excel font-color-white" ng-disabled="isExistCarrID()">Add Remit</a>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 pull-left">
                                        <button type="button" class="btn btn-default delete_button" ng-click="deleteAllRemit()" ><span></span>Delete</button>
                                    </div>
                                </div>

                                <div class="table-responsive clearfix customer-table-transit">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                        <tr>
                                            <th align="center" valign="middle" scope="col" width="25px">
                                        <div class="checkbox" style="width:15px;">
                                            <label>
                                                <input type="checkbox" ng-click="checkAll()" ng-model="remitData.selectAll">
                                            </label>
                                        </div>
                                        </th>	
                                        @foreach($remitFields as $field)
                                        <th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('{{$field->displayField}}')">{{ $field->displayName}} <i class="fa " ng-show="predicateRemit === '{{$field->displayField}}'" ng-class="{'fa-sort-asc' : !reverseRemit, 'fa-sort-desc': reverseRemit}"></i></a></th>					
                                        @endforeach
                                        <th align="left" valign="middle" scope="col">&nbsp;</th>
                                        </tr>					
                                        <tr ng-repeat="remit in remitData.list|filter:filterCustomerListing|orderBy:predicateRemit:reverseRemit">
                                            <td align="center" valign="middle" width="25px">
                                                <div class="checkbox" style="width:15px;">
                                                    <label>
                                                        <input type="checkbox" ng-model="remit.selected" ng-change="checkSelection($index)">
                                                    </label>
                                                </div>
                                            </td>

                                            @foreach($remitFields as $field)
                                            <td align="left" valign="middle">
                                                <a href="#" ng-click="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
                                                    <?php
                                                    echo "{{ remit." . $field->displayField . "|formatByType:'" . $field->fieldType . "'}}";
                                                    ?>
                                                </a></td>						
                                            @endforeach
                                            <td align="center" valign="middle">
                                                <a style="width:67px;margin-bottom:3px;" href="{{url(\Config::get('app.frontPrefix').'/carriers/edit/remits/edit')}}/@{{carrID}}/@{{remit.id}}" class="btn btn-default edit_button"><span></span>Edit</a>
                                                <br/>
                                                <button type="button" class="btn btn-default delete_button" ng-click="deleteRemit(remit.id, true)"><span></span>Delete</button>
                                            </td>						

                                        </tr>
                                        <tr ng-hide="remitData.list.length">
                                            <td colspan="7" align="center"><strong>No Record Found!</strong></td>
                                        </tr>
                                    </table>
                                    <p>Showing record @{{(recordLimitRemit * currentPageRemit) > remitData.count ? remitData.count : recordLimitRemit * currentPageRemit}} of @{{remitData.count}}</p>
                                </div>

                                <div class="text-right">
                                    <pagination total-items="remitData.count" items-per-page="recordLimit" ng-model="currentPageRemit" ng-change="pageChanged()"></pagination>
                                </div>

                            </div>
                        </div>                        
                    </div>
                </div>
                
                
                
            </div>
        </div>
        <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
            <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
            <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
        </div>

    </div>
</section>
<!-- /content section-->
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CarrierManager.js')}}"></script>
@endsection