@extends('layouts.user_home')
@section('angular_controller')
    CarrierRemitEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
    {!! Breadcrumbs::render('new_carrierremit') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/carrier.css')}}" rel="stylesheet">
@stop
@section('sidebar')
    @include('_partials/carrier_management_sidebar')
@stop
@section('top_content')	
@stop

@section('content')
<!-- content section-->
<section class="content-wrapper">
    <div class="contract_table">
        <div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-xs-12">               
                <div>
                    <h2>Add Carrier Remit </h2>
                    <form name="carrierRemit" enctype="multipart/form-data" novalidate ng-init="remitID = ''; carrierId={{ $carrierId}}">
                        <div class="row  div-top-25">
                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <input type="hidden" name="carrierId" ng-model="remitData.carrierId" value="{{ $carrierId}}" >
                                    <div class="form-group" ng-class="{'has-error': carrierRemit.remit_name.$invalid}">
                                        <label>{{ Lang::get('carrier.remitName') }}</label>
                                        <input type="text" name="remit_name" ng-model="remitData.remit_name" class="form-control" placeholder=""  required> 
                                        <span class="help-block" ng-show="carrierRemit.remit_name.$invalid" >
                                            <span ng-show="carrierRemit.remit_name.$error.required">{{ Lang::get('carrier.remitValidationNameInvalid') }}</span>
                                        </span>
                                    </div>                                                         
                                </div>
                                <div class="col-lg-3">                                    
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.remitCurrency') }}</label>
                                        <div ng-dropdown-multiselect="" options="currencyTypes" selected-model="remitData.remit_currency" extra-settings="currencySettings"></div>                                    
                                    </div>                                                  
                                </div>
                                <div class="col-lg-3"></div>                                        
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.remitAddress1') }}</label>
                                        <input type="text" name="remit_address1" ng-model="remitData.remit_address1"  class="form-control" placeholder="" >
                                    </div>                                
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.remitAddress2') }}</label>
                                        <input type="text" name="remit_address2" ng-model="remitData.remit_address2"  class="form-control" placeholder="" >
                                    </div>                                             
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"  ng-class="{'has-error': carrierRemit.location.$invalid}">
                                        <label>{{ Lang::get('carrier.remitLocation') }}</label>
                                        <input type="text" name="location" class="form-control" placeholder="" g-places-autocomplete ng-model="remitData.location" options="autocompleteOption" required>
                                        <span class="help-block" ng-show="carrierRemit.location.$invalid" >
                                            <span ng-show="carrierRemit.location.$error.required">{{ Lang::get('carrier.remitValidationLocationInvalid') }}</span>
                                        </span>
                                    </div>
                                </div>                                
                            </div>

                                               
                        </div>

                        <div class="clearfix"></div>
                        <div class="btn-cont" style="padding-top:20px;">
                            <button  type="button" class="btn btn-default" ng-click="cancelRemitEditing(carrierId)" style="color:black">{{ Lang::get('carrier.remitCancelButton') }}</button>
                            <button  type="button" class="btn btn-primary font-color-white" ng-disabled="carrierRemit.$invalid" ng-click="saveRemitData(carrierRemit)">{{ Lang::get('carrier.saveOrUpdateButton') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
            <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
            <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
        </div>

    </div>
</section>
<!-- /content section-->
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CarrierManager.js')}}"></script>
@endsection