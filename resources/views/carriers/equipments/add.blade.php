@extends('layouts.user_home')
@section('angular_controller')
CarrierEquipmentEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('new_carrierequipment') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/carrier.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/carrier_management_sidebar')
@stop
@section('top_content')	
@stop

@section('content')
<!-- content section-->
<section class="content-wrapper">
    <div class="contract_table">
        <div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-xs-12">               
                <div>
                    <h2>Add Carrier Equipment </h2>
                    <form name="carrierEquipment" enctype="multipart/form-data" novalidate ng-init="equipID = ''; carrierId={{ $carrierId}}">

                        <div class="row  div-top-25">
                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">

                                    <input type="hidden" name="carrierId" ng-model="equipmentData.carrierId" value="{{ $carrierId}}" > 
                                    <div class="form-group" ng-class="{'has-error': carrierEquipment.equipId.$invalid}">
                                        <label>{{ Lang::get('carrier.equipment') }}</label>                                        
                                        <div ng-dropdown-multiselect="" options="equipTypes" selected-model="equipmentData.equipId" extra-settings="equipSettings"></div>                                                                           
                                        <span class="help-block" ng-show="carrierEquipment.equipId.$invalid" >
                                            <span ng-show="carrierEquipment.equipId.$error.required">{{ Lang::get('carrier.equipmentValidationTypeInvalid') }}</span>
                                        </span>
                                    </div>                              
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentDesc') }}</label>
                                        <input type="text" name="equip_description" ng-model="equipmentData.equip_description" class="form-control" placeholder="" >                                                
                                    </div>                                            
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentISOCode') }}</label>
                                        <input type="text" name="equip_no" ng-model="equipmentData.equip_no" class="form-control" placeholder="">
                                    </div>
                                </div>                                        
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentMaxGrossWeight') }}</label>
                                        <input type="text" name="equip_weight" ng-model="equipmentData.equip_weight" class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentTareWeight') }}</label>
                                        <input type="text" name="equip_tareweight" ng-model="equipmentData.equip_tareweight" class="form-control" placeholder="" >                                                
                                    </div>                                            
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentInteriorVolume') }}</label>
                                        <input type="text" name="equip_interiorvolume" ng-model="equipmentData.equip_interiorvolume" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-3 div-top-20">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" ng-model="equipmentData.equip_refridgerate"> {{ Lang::get('carrier.equipmentTemparature') }}
                                        </label>
                                    </div>
                                </div>  
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group clearfix date-pick">
                                        <label>{{ Lang::get('carrier.equipmentPurchaseDate') }}</label>                                        
                                        <datepickerkb date-format="yyyy-MM-dd" ng-init="equip_purchasedate ='{{ date('Y-m-d')}}'">
                                            <input ng-model="equipmentData.equip_purchasedate" class="form-control" type="text" placeholder="Eg..(2015/12/31)" name="equip_purchasedate" />
                                        </datepickerkb>
                                    </div>                                     
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentYearBuilt') }}</label>
                                        <input type="text" name="equip_year" ng-model="equipmentData.equip_year" class="form-control" placeholder="" >                                                
                                    </div>                                            
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentMake') }}</label>
                                        <input type="text" name="equip_make" ng-model="equipmentData.equip_make" class="form-control" placeholder="">
                                    </div>
                                </div>                                        
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentRegistrationNumber') }}</label>
                                        <input type="text" name="equip_registrationnumber" ng-model="equipmentData.equip_registrationnumber" class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-3">                                    
                                    <div class="form-group clearfix date-pick">
                                        <label>{{ Lang::get('carrier.equipmentRegistrationDate') }}</label>                                        
                                        <datepickerkb date-format="yyyy-MM-dd" ng-init="equip_registrationdate ='{{ date('Y-m-d')}}'">
                                            <input ng-model="equipmentData.equip_registrationdate" class="form-control" type="text" placeholder="Eg..(2015/12/31)" name="equip_registrationdate" />
                                        </datepickerkb>
                                    </div>                                    
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentRegisteredAmount') }}</label>
                                        <input type="text" name="equip_registeredamount" ng-model="equipmentData.equip_registeredamount" class="form-control" placeholder="">
                                    </div>
                                </div>                                        
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentLicenseNumber') }}</label>
                                        <input type="text" name="equip_licenseplateno" ng-model="equipmentData.equip_licenseplateno" class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentLicenseState') }}</label>
                                        <input type="text" name="equip_licensestate" ng-model="equipmentData.equip_licensestate" class="form-control" placeholder="" >                                                
                                    </div>                                            
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group clearfix date-pick">
                                        <label>{{ Lang::get('carrier.equipmentLicenseExpiryDate') }}</label>                                        
                                        <datepickerkb date-format="yyyy-MM-dd" ng-init="equip_licenseexpirydate ='{{ date('Y-m-d')}}'">
                                            <input ng-model="equipmentData.equip_licenseexpirydate" class="form-control" type="text" placeholder="Eg..(2015/12/31)" name="equip_licenseexpirydate" />
                                        </datepickerkb>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ Lang::get('carrier.equipmentTag1') }}</label>
                                            <input type="text" name="equip_tag1" ng-model="equipmentData.equip_tag1" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ Lang::get('carrier.equipmentTag2') }}</label>
                                            <input type="text" name="equip_tag2" ng-model="equipmentData.equip_tag2" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ Lang::get('carrier.equipmentTag3') }}</label>
                                            <input type="text" name="equip_tag3" ng-model="equipmentData.equip_tag3" class="form-control" placeholder="">
                                        </div>
                                    </div>                                            
                                </div>  
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentChasisNumber') }}</label>
                                        <input type="text" name="equip_chasisnumber" ng-model="equipmentData.equip_chasisnumber" class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentVinNumber') }}</label>
                                        <input type="text" name="equip_vin" ng-model="equipmentData.equip_vin" class="form-control" placeholder="" >                                                
                                    </div>                                            
                                </div>                                                                              
                            </div>

                            <div class="col-lg-11 div-padd-left div-top-25">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentOwnerName') }}</label>
                                        <input type="text" name="equip_ownername" ng-model="equipmentData.equip_ownername" class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentOwnerType') }}</label>
                                        <div ng-dropdown-multiselect="" options="ownerTypes" selected-model="equipmentData.ownerId" extra-settings="ownerSettings"></div>                                                                           
                                    </div>                                            
                                </div>  
                                <div class="col-lg-3">
                                    <div class="form-group" ng-class="{'has-error': carrierEquipment.equip_ownerphone.$invalid}">
                                        <label>{{ Lang::get('carrier.equipmentContactPhone') }}</label>
                                        <input type="text" name="equip_ownerphone" ng-model="equipmentData.equip_ownerphone" ng-pattern="/^([0-9]{5,12})$/" class="form-control" placeholder="" >                                                
                                        <span class="help-block" ng-show="carrierEquipment.equip_ownerphone.$invalid">
                                            <span ng-show="carrierEquipment.equip_ownerphone.$error.pattern">{{ Lang::get('carrier.addCarrierValidationPhoneInvalid') }}</span>
                                        </span>
                                    </div>                                            
                                </div>  
                            </div>

                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentAddress') }}</label>
                                        <input type="text" name="equip_owneradr1" ng-model="equipmentData.equip_owneradr1"  class="form-control" placeholder="" >
                                    </div>                              
                                </div>
                                <div class="col-lg-3"></div>  
                                <div class="col-lg-3">
                                    <div class="form-group" ng-class="{'has-error': carrierEquipment.equip_owneremail.$invalid}">
                                        <label>{{ Lang::get('carrier.equipmentEmailAddress') }}</label>
                                        <input type="email" name="equip_owneremail" ng-model="equipmentData.equip_owneremail" class="form-control" placeholder="" >                                                
                                        <span class="help-block" ng-show="carrierEquipment.equip_owneremail.$invalid">
                                            <span ng-show="carrierEquipment.equip_owneremail.$error.email">{{ Lang::get('carrier.addCarrierValidationEmailInvalid') }}</span>
                                        </span>
                                    </div>                                            
                                </div>  
                            </div>
                            <div class="col-lg-11 div-padd-left">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{ Lang::get('carrier.equipmentLocation') }}</label>
                                        <input type="text" name="location" class="form-control" placeholder="" g-places-autocomplete ng-model="equipmentData.location" options="autocompleteOption">
                                    </div>
                                </div>
                            </div>                            
                        </div>

                        <div class="clearfix"></div>
                        <div class="btn-cont" style="padding-top:20px;">
                            <button  type="button" class="btn btn-default" ng-click="cancelEditing(carrierId)" style="color:black">{{ Lang::get('carrier.equipCancelButton') }}</button>
                            <button  type="button" class="btn btn-primary font-color-white" ng-disabled="carrierEquipment.$invalid" ng-click="saveEquipmentData(carrierEquipment)">{{ Lang::get('carrier.saveOrUpdateButton') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
            <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
            <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
        </div>

    </div>
</section>
<!-- /content section-->
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CarrierManager.js')}}"></script>
@endsection