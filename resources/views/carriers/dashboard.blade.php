@extends('layouts.account_home')
@section('page_title')
	Welcome!
@stop
@section('angular_controller')
    CarrierDashabordSetupController
@stop
@section('styles')
	<link rel="stylesheet" href="{{ asset('css/additional_cssrules.css') }}" />
@stop
@section('sidebar')
	 
@stop
@section('content')
	<div class="row clearfix">
        	<div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-truck fa-2x"></i>
                            <h3>Carriers</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('crr')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/setup-connectivity') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-customer-carriers-section" style="position: absolute; width: 100%; height: 100%; background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                                <li ng-repeat="carrier in customerCarrierData.list"><a href="javascript:;" ng-click="changeCarrier(carrier.id, $event)">@{{ carrier.scac + '-' }} @{{ carrier.name }}</a></li>
								<li ng-show="customerCarrierData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="customerCarrierData.count >= 0 ">Total: <span class="summary-count">@{{ customerCarrierData.count }} Record(s)</span></p>
						<p ng-show="customerCarrierData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="green">&nbsp;</p>
                    </div>
                </div>
            </div>
			<div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-usd fa-2x"></i>
                            <h3>Accessorials</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('accs')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/setup-connectivity/list-carrier-accessorials') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-accessorial-section" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="rates in accsRates.customRates">@{{rates.accs_type.accsname}}</li>
                               <li ng-show="accsRates.customRates.length == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="accsRates.customRates.length >= 0 ">Total: <span class="summary-count">@{{ accsRates.customRates.length }} Record(s)</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="yellow">Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-battery-quarter fa-2x"></i>
                            <h3>Fuel</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('fuel')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/setup-connectivity/list-carrier-fuel') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-fuel-section" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="rates in fuelRates.customRates">@{{rates.fuel_type.fuelType ? rates.fuel_type.fuelType : 'Unknown'}}</li>
                               <li ng-show="fuelRates.customRates.length == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="fuelRates.totalCustomCount >= 0 ">Total: <span class="summary-count">@{{ fuelRates.customRates.length }} Record(s)</span></p>
						<p ng-show="fuelRates.totalCustomCount < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="green">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-wrench fa-2x"></i>
                            <h3>Equipments</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('eqp')" class="details_link">Reload</a></li>
                                   <li><a href="{!! url(Config::get('app.frontPrefix'). '/carriers/edit') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-equipment-section" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="equipment in equipmentData.list">@{{ equipment.equipment_type.name ? equipment.equipment_type.name + ' - ' : '' }} @{{equipment.equip_no ? equipment.equip_no : 'Unknown'}}</li>
                               <li ng-show="equipmentData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="equipmentData.count >= 0 ">Total: <span class="summary-count">@{{ equipmentData.list.length }} Record(s)</span></p>
						<p ng-show="equipmentData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                           <i class="fa fa-modx fa-2x"></i>
                            <h3>Mode</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('mode')" class="details_link">Reload</a></li>
                                   <li><a href="{!! url(Config::get('app.frontPrefix'). '/carriers/edit') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-modes-section" id="loading-modes-section" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="mode in carriers.modes">@{{ mode.mode_type.name ? mode.mode_type.name : '' }} {{-- mode.comments ? mode.comments : 'Unknown' --}}</li>
                               <li ng-show="carriers.modes.length == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="carriers.modes.length >= 0 ">Total: <span class="summary-count">@{{ carriers.modes.length }} Record(s)</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-money fa-2x"></i>
                            <h3>Remittance</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('rmt')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/carriers/edit') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);" id="loading-remits-section"> 
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
								<li ng-repeat="remit in remits.list">@{{ remit.remit_state ?  remit.remit_state : 'Unknown' }} @{{ remit.remit_city ?  remit.remit_city : 'Unknown' }} @{{ remit.remit_postal ?  remit.remit_postal : 'Unknown' }}</li>
								<li ng-show="remits.count == 0"><strong>No Data Available</strong></li>  
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="remits.count >= 0 ">Total: <span class="summary-count">@{{ remits.list.length }} Record(s)</span></p>
                    	<p ng-show="remits.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-ambulance fa-2x"></i>
                            <h3>Insurance</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('ins')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/carriers/edit') !!}/@{{selectedCarrierId}}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);" id="loading-insurance-section"> 
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="insurance in insurances.list">@{{ insurance.insurance_type.insurance_name ? insurance.insurance_type.insurance_name : '' }} {{-- insurance.insurance_policyname ?  insurance.insurance_policyname : 'Unknown' --}}</li>
                               <li ng-show="insurances.list.length == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="insurances.list.length >= 0">Total:  <span class="summary-count">@{{ insurances.list.length }} Record(s)</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-cubes fa-2x"></i>
                            <h3>Rates/Contract</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="details_link">Details</a></li>
                                </ul>
                            </div>
                     	</div>
                        <div class="entries" style="position:relative">
                            <ul>
                               <li>No Data Available</li> 
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p>Total: 0 Record(s)</p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
<script src="{{ asset('js/CarrierDashabordSetup.js') }}"> </script>
@endsection