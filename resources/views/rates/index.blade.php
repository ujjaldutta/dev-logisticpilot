@extends('layouts.account_home')
@section('angular_controller')
rateCalculationController
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('shop_rates') !!}
@stop
@section('styles')
	<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angularjs-datepicker/dist/angular-datepicker.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  <!-- sidebar-->
  <aside class="leftnavbar-wrapper no-mar leftnavbar-wrapper-rates sidebar-for-rate-search" ng-hide="!searchStarted">
    <div class="nav-col no-mar sidebar">
      <div id="sidebar-nav" class="navbar-collapse navbar-ex1-collapse">
        <div class="panel panel-default">
          <div class="filter-head shipment-status">
            <h2>Transportation Mode </h2>
            <div class="">
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                LTL </label>
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                TL </label>
            </div>
          </div>
          <div class="filter-head shipment-status">
            <h2>Equipment Type </h2>
            <div ng-dropdown-multiselect="" options="accessorials" selected-model="rates.addlAccessorial"></div>
          </div>
          <div class="filter-head pricing nobord">
            <h2>Pricing
              <button type="button" class="btn-reset pull-right" ng-click="resetFilter('price')"><span>Reset</span></button>
              <p>By Filter</p>
            </h2>
            <div class="slide-range">
              <!--slider-->
				<rzslider
					rz-slider-floor="priceFilter.floor"
					rz-slider-ceil="priceFilter.ceil"
					rz-slider-model="priceFilter.min"
					rz-slider-high="priceFilter.max"
					rz-slider-on-change="onPriceFilter()"
				>
				</rzslider>
              <!--slider-->
            </div>
            <h2 class="transit">Transit days
              <button type="button" class="btn-reset pull-right" ng-click="resetFilter('transit')"><span>Reset</span></button>
            </h2>
            <div class="slide-range">
              <!--slider-->
              <div class="slider">
                <rzslider
					rz-slider-floor="transitFilter.floor"
					rz-slider-ceil="transitFilter.ceil"
					rz-slider-model="transitFilter.min"
					rz-slider-high="transitFilter.max"
					rz-slider-on-change="onTransitFilter()"
				>
				</rzslider>
              </div>
              <!--slider-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <!-- /sidebar-->
@stop
@section('top_content')
	<div class="rate_test clearfix" ng-init="rates.clientInfo.Clientaddress='{{ Auth::user()->client->client_adr1 }}'; rates.clientInfo.Clientcity='{{ Auth::user()->client->client_city }}'; rates.clientInfo.Clientstate='{{ Auth::user()->client->client_state }}'; rates.clientInfo.ClientPostal='{{ Auth::user()->client->client_postal }}'; rates.clientInfo.clientcountry='{{ Auth::user()->client->client_country }}';" ng-cloak>
          
          
          <div>
          <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
            <h3>Lets test the rates.</h3>
            <div class="block_rate_form">
              <form class="form-inline">
                <div class="row">
                <div class="col-sm-4">
                <div class="form_block rates-top-section">
                  <div class="form-group" ng-class="{'has-error': rateSearch.shipment_pickup_location.$invalid}" style="width:100%">
                      <label for="selectBox">Pickup Country and City/Zip</label>
					  <input class="form-control pickup" name="shipment_pickup_location" g-places-autocomplete ng-model="rates.pickup" force-selection="true" required/ >
						<span class="help-block" ng-show="rateSearch.shipment_pickup_location.$invalid">
							<span ng-show="rateSearch.shipment_pickup_location.$error.required">Pickup location is required</span>
						</span>
                      <!-- <a href="#" class="remove">Remove</a> -->
                  </div>
                  <div class="clearfix"></div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" ng-model="rates.pickup_commercial">
                      Commercial </label>
                  </div>
                </div>
                </div>
                
                <div class="col-sm-4">
                <div class="form_block rates-top-section">
                  <div class="form-group" ng-class="{'has-error': rateSearch.shipment_delivery_location.$invalid}" style="width:100%">
                    <label for="selectBox">Delivery Country and City/Zip</label>
					<input class="form-control delivery" name="shipment_delivery_location" g-places-autocomplete ng-model="rates.delivery" force-selection="true" required/ >
                    <!-- <a href="#" class="remove">Remove</a> -->
						<span class="help-block" ng-show="rateSearch.shipment_delivery_location.$invalid">
							<span ng-show="rateSearch.shipment_delivery_location.$error.required">Delivery location is required</span>
						</span>
                  </div>
                  <div class="clearfix"></div>
                  <div class="checkbox">
                    <label>
                     <input type="checkbox" ng-model="rates.delivery_commercial">
                      Commercial </label>
                  </div>
                </div>
                </div>
                <div class="col-sm-2">
                    <form class="form-inline">
                     <div class="form-group clearfix date-pick" ng-class="{'has-error': rateSearch.shipDate.$invalid}" style="width:100%;">
                       <label for="pickupno" style="display:block;">Ship Date</label>
                        <datepickerkb date-format="yyyy-MM-dd" ng-init="rates.shipDate='{{ date('Y-m-d') }}'" class="rate-date">
                            <input ng-model="rates.shipDate" class="form-control rate-date" type="text" required placeholder="Eg..(2015/12/31)" name="shipDate" style="margin-left:0;background-image:none; width:100%;" />
                        </datepickerkb>
                     </div>
                     </form>
            	</div>
                
                <div class="col-sm-2">
                <div class="form_block"><!--form-group-->
					<label>Additional Accessorial</label>
					<div ng-dropdown-multiselect="" options="accessorials" selected-model="rates.addlAccessorial"></div>
				</div>
                </div>
                
                </div>
              </form>
            </div>
			<!-- Multiselect accessorial -->
			
          </div>
          
          
          <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
           <div class="table_calc_block">
                <div class="row">
                	<div class="ratel-content-wrapper" ng-repeat="item in rates.products">
                    <!--Item Open-->
                    <div class="col-sm-4">
                    	<label class="ratel-label-heading">Item</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.itemType_@{{$index}}.$invalid}">
							<select ng-options="item.id as item.name for item in itemTypes" ng-model="item.itemType" required class="form-control rates-long-select" name="itemType_@{{$index}}">
								<option value="" disabled selected ng-hide="item.itemType">Please select</option>
							</select>
							<span class="help-block" ng-show="rateSearch.itemType_@{{$index}}.$invalid">
							  <span ng-show="rateSearch.itemType_@{{$index}}.$error.required">Item is required</span>
							</span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Unit</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.qty_@{{$index}}.$invalid}">
							  <input name="qty_@{{$index}}" type="number" ng-model="item.qty" class="form-control" placeholder="1" required ng-pattern="/^[1-9]{1,}$/">
							  <span class="help-block" ng-show="rateSearch.qty_@{{$index}}.$invalid">
								<span ng-show="rateSearch.qty_@{{$index}}.$error.required">Quantity is required</span>
								<span ng-show="rateSearch.qty_@{{$index}}.$error.pattern">Quantity is invalid</span>
							  </span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Class</label>
                        <div class="form-group" ng-class="{'has-error': rateSearch.classType_@{{$index}}.$invalid}">
							 <select ng-options="item.id as item.label for item in productClassTypes" ng-model="item.classType" required class="form-control" name="classType_@{{$index}}">
								<option value="" disabled selected ng-hide="item.classType">Please select</option>
							 </select>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading"></label>
                        <div class="form-group">
							 <select ng-options="item.id as item.label for item in productClasses" ng-model="item.class" class="form-control" ng-disabled="item.classType == 2">
								<option value="" disabled selected ng-hide="item.class">Please select</option>
							 </select>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Nmfc</label>
                        <div class="form-group">
							 <input type="text" class="form-control" />
						</div>
                    </div>
                    <!--Item Close-->
                    <div class="clearfix"></div>
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Length</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.len_@{{$index}}.$invalid}">
							  <input type="number" class="form-control" ng-model="item.len" placeholder="1" required ng-disabled="item.classType == 1" name="len_@{{$index}}" required ng-pattern="/^[1-9](?:(\.[0-9]{2})?)/">
							   <span class="help-block" ng-show="rateSearch.len_@{{$index}}.$invalid">
								<span ng-show="rateSearch.len_@{{$index}}.$error.required">Length is required</span>
								<span ng-show="rateSearch.len_@{{$index}}.$error.pattern">Length is invalid</span>
							  </span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Width</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.width_@{{$index}}.$invalid}">
                          <input type="number" class="form-control" ng-model="item.width" placeholder="1" required ng-disabled="item.classType == 1" name="width_@{{$index}}" ng-pattern="/^[1-9](?:(\.[0-9]{2})?)/">
                          <span class="help-block" ng-show="rateSearch.width_@{{$index}}.$invalid">
                            <span ng-show="rateSearch.width_@{{$index}}.$error.required">Width is required</span>
                            <span ng-show="rateSearch.width_@{{$index}}.$error.pattern">Width is invalid</span>
                          </span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Height</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.height_@{{$index}}.$invalid}">
                            <input type="number" class="form-control" ng-model="item.height" placeholder="1" required ng-disabled="item.classType == 1" name="height_@{{$index}}" ng-pattern="/^[1-9](?:(\.[0-9]{2})?)/">
                            <span class="help-block" ng-show="rateSearch.height_@{{$index}}.$invalid">
                              <span ng-show="rateSearch.height_@{{$index}}.$error.required">Height is required</span>
                              <span ng-show="rateSearch.height_@{{$index}}.$error.pattern">Height is invalid</span>
                            </span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                    	<label class="ratel-label-heading">Weight</label>
                        <div class="form-group clearfix" ng-class="{'has-error': rateSearch.weight_@{{$index}}.$invalid}">
                            <input type="number" class="form-control" ng-model="item.weight" placeholder="" required name="weight_@{{$index}}" ng-pattern="/^[1-9](?:(\.[0-9]{2})?)/">
                            <span class="help-block" ng-show="rateSearch.weight_@{{$index}}.$invalid">
                                <span ng-show="rateSearch.weight_@{{$index}}.$error.required">Weight is required</span>
                                <span ng-show="rateSearch.weight_@{{$index}}.$error.pattern">Weight is invalid</span>
                            </span>
						</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                 		<label class="density-head ratel-label-heading"><span>Density</span> <a href="#" class="help" data-toggle="tooltip" data-placement="bottom" title="Formula of Density will come">Help</a></label>
                        <div class="density">@{{ calculateDensity(item.len, item.width, item.height, item.weight) }} cc/lb</div>
                    </div>
                    <!--Item Close-->
                    <!--Item Open-->
                    <div class="col-sm-2">
                 		<div class="rates-last-div clearfix">
                        	<div class="rates-last-div-left">
                                <div style="display:block;margin-right:4px;">
                                    <label><input ng-model="item.stackable" type="checkbox" value="" name="Stackable" style="display:inline-block; margin-right: 6px;max-width: 15px;min-width: 10px;" />Stackable</label>
                                </div>
                                <div style="display:block;margin-right:4px;">
                                    <label><input ng-model="item.hazmat" type="checkbox" value="" name="Hazardous" style="display:inline-block; margin-right: 6px;max-width: 15px;min-width: 10px;" />Hazardous</label>
                                </div>
                         	</div>
                            <div class="rates-delete-row-wrapper">
                                <button type="button" class="btn btn-default deleteRow" ng-click="removeItem($index)" ng-disabled="$index == 0"><span class="minus"></span>Delete</button>
                            </div>
                    	</div>
                    </div>
                    <!--Item Close-->
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                	<div class="col-sm-12 addBtnCont clearfix">
                        <button type="button" class="btn btn-default addRowBtn_carrier" ng-click="addItem()"><span class="plus"></span>Add product</button>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-7 col-sm-12">
                              <div class="total">
                                  <div class="total_inner">
                                      Total Weight : <strong>@{{ calculateTotalWeight() }} Ib</strong>
                                  </div>
                              </div>
                              <button type="button" class="btn btn-primary download_excel" ng-disabled="rateSearch.$invalid" ng-click="searchForFilter()"> <span>&nbsp;</span>@{{searchBtnText}}</button>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
        </div>
    </div>
@stop
@section('content')
	<!-- content section-->
    <section class="content-wrapper content-wrapper-rates" ng-hide="!searchStarted">
      <div class="content-subcontainer">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-xs-12">
             <div class="contract_table">
             <div class="rates-search-result">
				<div class="row" ng-show="showSearchProgress">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<progressbar class="progress-striped active" value="searchProgress" type="success">@{{searchProgress.toFixed(0)}}%</progressbar>
					</div>
				</div>
                <div class="result_top" ng-show="(rates.searchResults | filter:sortByPriceAndTransit).length">
                	<ul>
                    	<li>About @{{(rates.searchResults | filter:sortByPriceAndTransit).length}} results</li>
                        <li class="email"><a href="#">Email All</a></li>
                        <li class="print"><a href="#">Print All</a></li>
                    </ul>
                </div>
           	  	<div class="quote_cont" ng-repeat="item in rates.searchResults | filter: sortByPriceAndTransit">
                	<div class="table-responsive">
                	  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        	<thead>
                                <tr>
                                    <th align="left" valign="middle" width="20%">&nbsp;</th>
                                    <th align="left" valign="middle">Transit Days</th>
                                    <th align="left" valign="middle">Service</th>
                                    <th align="left" valign="middle">Carrier Liability</th>
                                    <th align="left" valign="middle">Carrier Performance</th>
                                    <th align="left" valign="middle">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle">
										<img src="{{asset('images/no_image.png')}}" ng-src="@{{getCarrierInfoBySCAC('logo', item.SCAC)}}" alt="carrier logo"> @{{getCarrierInfoBySCAC('name', item.SCAC)}}
                                        <br><span class="direct">@{{ item.TransitType }}</span>
                                    </td>
                                    <td align="left" valign="middle">@{{ item.TransitTime }}</td>
                                    <td align="left" valign="middle">@{{ item.TransitType }}</td>
                                    <td align="left" valign="middle" ng-init="item.insuranceLiabilityAmt=item.insuranceLiabilityAmt === undefined ? 'Loading...' : item.insuranceLiabilityAmt">@{{ item.insuranceLiabilityAmt }}</td>
                                    <td align="left" valign="middle">
										<span ng-show="item.rating === undefined">Loading...</span>
										<rating readonly="true" ng-model="item.rating" ng-show="item.rating !== undefined"></rating>
									</td>
                                    <td align="left" valign="middle" class="rate" ng-init="item.popTemplate='dynamicpopover.html'">
                                    	$@{{item.TotalShipmentCharge}}<br>
                                        <button class="btn btn-success" type="submit" popover-template="item.popTemplate" popover-placement="top" popover-trigger="mouseenter">Book</button>
                                    </td>
                                </tr>
                            </tbody>
              	    	</table>
                        <ul class="bot_buttons">
                        	<li class="save_quote" ng-init="item.saved=0; item.saveText='Save Quote';"><a href="javascript:;" ng-click="saveQuote($index)">@{{ item.saveText }}</a></li>
                            <li class="email"><a href="#">Email this Quote</a></li>
                            <li class="print"><a href="#">Print this Quote</a></li>
                            <li class="shipment"><a href="#">Create Shipment</a></li>
                            <li class="info"><a href="#">Terminal Info</a></li>
                            <li class="add"><a href="#">Add Assuarance</a></li>
                        </ul>
                	</div>
              	</div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script type="text/ng-template" id="dynamicpopover.html">
		<div class="row">
			<div class="col-sm-12">
				Freight - <strong>@{{item.GrossCharges}}</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				Fuel - <strong>@{{item.FuelSurCharge}}</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				Discount - <strong style="color:red;">@{{item.Discount}}</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				Total Accessorial Charges - <strong>@{{item.TotalAccessorialCharges}}</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				Net Freight : - <strong>@{{eval((item.GrossCharges-item.Discount)-item.FuelSurCharge)}}</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				Total Shipment Cost : <strong>@{{item.TotalShipmentCharge}}</strong>
			</div>
		</div>
  </script>
  <!-- /content section--> 
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/rateCalculation.js') }}"> </script>
@endsection
