@extends('layouts.user_home')
@section('angular_controller')
	UserRegistrationController
@stop
@section('page_class')
	contract-page
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('update_profile') !!}
@stop
@section('styles')
	<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/ng-tags-input/ng-tags-input.bootstrap.min.css') }}" rel="stylesheet">
@stop
@section('top_content')
	
@stop
@section('content')
<!-- content section-->
		<section class="content-wrapper" ng-init="userID='{{$user_id}}';clientID={{CustomerRepository::getCurrentCustomerID()}}" style="margin-left:0">
            <div class="contract_table">
				<div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
				<div class="clearfix"></div>
                <div id="horizontalTab1" class="tab-hld easytabs">
                    <ul class="resp-tabs-list">
                      <li>User Setup</li>
                      <li>User Default</li>
                    </ul>
                    <div class="resp-tabs-container">
                        <div class="tab-content">
                            <div class="row">
							<form id="user_profile" name="user_profile" enctype="multipart/form-data" novalidate ng-init="userId='{{$user_id}}'">
		  <div class="col-lg-3">			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_firstname.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userFirstName') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="userData.usr_firstname" name="usr_firstname" required>
					<span class="help-block" ng-show="user_profile.usr_firstname.$invalid">
						<span ng-show="user_profile.usr_firstname.$error.required">First Name is required</span>
					</span>
			  </div>
			   <div class="form-group" ng-class="{'has-error': user_profile.usr_lastname.$invalid}">
					<label>{{ Lang::get('registrationsteps.userLastName') }}</label>
					<input type="text" class="form-control" placeholder="" ng-model="userData.usr_lastname" name="usr_lastname" required> 
					<span class="help-block" ng-show="user_profile.usr_lastname.$invalid">
						<span ng-show="user_profile.usr_lastname.$error.required">Last Name is required</span>
					</span>
			  </div>
			  <div class="form-group">
				  <label>Address 1</label>
				  <input type="text" class="form-control" name="usr_adr2"  ng-model="userData.usr_adr2" />
			  </div>
				<div class="form-group">
				  <label>Address 2</label>
				  <input type="text" class="form-control" name="usr_adr1"  ng-model="userData.usr_adr1" />
			  </div>			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_phone.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userPhone') }}</label>
				  <input type="text" name="usr_phone" class="form-control" placeholder="" ng-model="userData.usr_phone" ng-pattern="/^([0-9]{5,12})$/">
				  <span class="help-block" ng-show="user_profile.usr_phone.$invalid">
					<span ng-show="user_profile.usr_phone.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
				  </span>
			  </div>
			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_location.$invalid}">
			   <label>{{ Lang::get('registrationsteps.userLocation') }}*</label>
				<input class="form-control" name="usr_location" g-places-autocomplete ng-model="userData.usr_location" force-selection="true" options="autocompleteOption" required />
				<span class="help-block" ng-show="user_profile.usr_location.$invalid">
					<span ng-show="user_profile.usr_location.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
				  </span>
			  </div>
		  </div>
		   <div class="col-lg-3">
			<div class="form-group" ng-class="{'has-error': user_profile.email.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userEmail') }}</label>
				  <input type="email" class="form-control" name="email"  ng-model="userData.email"  required />
				  <span class="help-block" ng-show="user_profile.email.$invalid">
					<span ng-show="user_profile.email.$error.error">Email address is required</span>
					<span ng-show="user_profile.email.$error.email">{{ Lang::get('registrationsteps.step1ValidationEmailInvalid') }}</span>
				  </span>
			  </div>
		   </div>
		  <div class="clearfix"></div>
			<div class="btn-cont" style="padding-top:20px;">
				<button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
				<button  type="button" class="btn btn-primary" ng-disabled="user_profile.$invalid" ng-click="saveRegistredData(user_profile)">Save / Update</button>
            </div>
		  </form>
	  </div>
			</div>
	
			<div class="tab-content">
                <div class="row">
					<div class="col-sm-6">
						<h4> Editing user default for: @{{userData.usr_firstname}} @{{userData.usr_lastname}} </h4>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="box-round">
							<h2>Layout Selection</h2>
							<div class="content">
								<div class="form-group">
									<label>Select Layout Type</label>
									<select class="form-control" ng-options="layoutType.id as layoutType.layoutName for layoutType in layoutTypes" ng-model="userLayoutSettings.layoutId" id="layoutId" name="layoutId" ng-change="saveCurrentLayoutSettings()" >
										<option value="" disabled selected ng-hide="userLayoutSettings.layoutId">Select your option</option>
									</select>
								</div>
								<div class="row" ng-show="userLayoutSettings.layoutId">
									<tags-input ng-model="userLayoutSettings.layoutDetails" display-property="displayName" key-property="displayField" placeholder="Enter Columns names" add-from-autocomplete-only="true">
										<auto-complete source="loadTags($query)" display-property="displayName" load-on-down-arrow="true" min-length="0"></auto-complete>
									</tags-input>
									<p> Press Down Arrow from keyboard or type few chars to list available fields</p>
									<div class="pull-right file_upload">
										<button ng-click="saveCurrentLayoutSettings(userLayoutSettings.layoutId)" class="btn btn-default" ng-disabled="!userLayoutSettings.layoutDetails || !userLayoutSettings.layoutDetails.length">Save</button>
									</div>
								</div>
								<div class="row table-responsive file_upload">
								<table class="table-bordered">
									<tr>
										<th align="center">Application setup</th>
										<th align="center">Action</th>
									</tr>
									<tr ng-repeat="mapping in layoutMappings">
										<td ng-if="mapping.layouts.length">
											<h4><strong>@{{mapping.layoutName}}</strong></h4>
											<p ng-repeat="fields in mapping.layouts">@{{fields.displayName}}</p>
										</td>
										<td align="center" ng-if="mapping.layouts.length">
											<button class="btn btn-primary" ng-click="removeLayoutMapping(mapping.id, userID)">Remove</button>
										</td>
										
									</tr>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
                    </div>
                </div>
				<div class="alert" ng-show="error.status||success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
					<button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">�</span></button>
					<strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
				</div>
            </div>
       </section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/UserManagerInit.js') }}"> </script>
@endsection
