@extends('layouts.user_home')
@section('angular_controller')
	UserRegistrationController
@stop
@section('page_class')
	contract-page
@stop
@section('page_title')
	Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('add_user') !!}
@stop
@section('styles')
	<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
	@include('_partials/user_management_sidebar')
@stop
@section('top_content')
	
@stop
@section('content')
<!-- content section-->
		<section class="content-wrapper" ng-init="newUser=true; clientID={{CustomerRepository::getCurrentCustomerID()}}">
            <div class="contract_table">
				<div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
				<div class="clearfix"></div>
                <div id="horizontalTab1" class="tab-hld easytabs">
                    <ul class="resp-tabs-list">
                      <li>User Setup</li>
                      <li ng-show="userID">User Default</li>
                    </ul>
                    <div class="resp-tabs-container">
                        <div class="tab-content">
                            <div class="row">
							<form id="user_profile" name="user_profile" enctype="multipart/form-data" novalidate ng-init="userID=''">
		  <div class="col-lg-3">			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_firstname.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userFirstName') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="userData.usr_firstname" name="usr_firstname" required>
					<span class="help-block" ng-show="user_profile.usr_firstname.$invalid">
						<span ng-show="user_profile.usr_firstname.$error.required">First Name is required</span>
					</span>
			  </div>
			   <div class="form-group" ng-class="{'has-error': user_profile.usr_lastname.$invalid}">
					<label>{{ Lang::get('registrationsteps.userLastName') }}</label>
					<input type="text" class="form-control" placeholder="" ng-model="userData.usr_lastname" name="usr_lastname" required> 
					<span class="help-block" ng-show="user_profile.usr_lastname.$invalid">
						<span ng-show="user_profile.usr_lastname.$error.required">Last Name is required</span>
					</span>
			  </div>
			  <div class="form-group">
				  <label>Address 1</label>
				  <input type="text" class="form-control" name="usr_adr1"  ng-model="userData.usr_adr1" />
			  </div>
				<div class="form-group">
				  <label>Address 2</label>
				  <input type="text" class="form-control" name="usr_adr2"  ng-model="userData.usr_adr2" />
			  </div>			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_phone.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userPhone') }}</label>
				  <input type="text" name="usr_phone" class="form-control" placeholder="" ng-model="userData.usr_phone" ng-pattern="/^([0-9]{5,12})$/">
				  <span class="help-block" ng-show="user_profile.usr_phone.$invalid">
					<span ng-show="user_profile.usr_phone.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
				  </span>
			  </div>
			  
			  <div class="form-group" ng-class="{'has-error': user_profile.usr_location.$invalid}">
			   <label>{{ Lang::get('registrationsteps.userLocation') }}*</label>
				<input class="form-control" name="usr_location" g-places-autocomplete ng-model="userData.usr_location" force-selection="true" options="autocompleteOption" required />
				<span class="help-block" ng-show="user_profile.usr_location.$invalid">
					<span ng-show="user_profile.usr_location.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
				  </span>
			  </div>
		  </div>
		   <div class="col-lg-3">
			<div class="form-group" ng-class="{'has-error': user_profile.email.$invalid}">
				  <label>{{ Lang::get('registrationsteps.userEmail') }}</label>
				  <input type="email" class="form-control" name="email"  ng-model="userData.email" required />
				  <span class="help-block" ng-show="user_profile.email.$invalid">
					<span ng-show="user_profile.email.$error.error">Email address is required</span>
					<span ng-show="user_profile.email.$error.email">{{ Lang::get('registrationsteps.step1ValidationEmailInvalid') }}</span>
				  </span>
			  </div>
			<div class="row">
				<div class="col-xs-9">
					<div class="form-group">
					  <label>{{ Lang::get('registrationsteps.userPassword') }}</label>
					  <input type="text" ng-model="userData.password" ng-init="userData.password='{{ $randomPassword }}'" class="form-control" placeholder=""  required>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="form-group file_upload">
					<label>&nbsp;&nbsp;</label>
						<button  type="button" class="btn btn-primary" ng-click="sendEmail()" ng-disabled="userID ==''" >@{{buttonMailText}}</button>						
					</div>
				</div>
			  </div>
			<div class="form-group" ng-class="{'has-error': user_profile.usr_type_id.$invalid}">
				<label>User Type</label>
				<select class="form-control" ng-options="userType.id as userType.usertype for userType in userTypes" ng-model="userData.usr_type_id" id="usr_type_id" name="usr_type_id" required>
					<option value="" disabled selected ng-hide="userData.usr_type_id">Please select</option>
				</select>
				<span class="help-block" ng-show="user_profile.usr_type_id.$invalid">
					<span ng-show="user_profile.usr_type_id.$error.required">User Type is required</span>
				</span>
			</div>
		   </div>
		  <div class="clearfix"></div>
			<div class="btn-cont" style="padding-top:20px;">
				<button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
				<button  type="button" class="btn btn-primary" ng-disabled="user_profile.$invalid" ng-click="saveRegistredData(user_profile)">Save / Update</button>
            </div>
		  </form>
	  </div>
			</div>
            <div class="tab-content" ng-show="userID">
                <div class="row">
					<div class="col-sm-12">
						<h4> Editing user default for: @{{userData.usr_firstname}} @{{userData.usr_lastname}} </h4>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
							<div class="box-round">
                                <h2>User Client Mapping</h2>
                                <div class="content">
									<div class="form-group">
                                  <label>Start Entering Client Name</label>
								  <div class="row">
									 <div class="col-sm-5">
									  <div angucomplete-alt placeholder="Type Company Name" pause="200" selected-object="selectedClient" remote-url="@{{UserAPIEndpoint.base + UserAPIEndpoint.clientList + 'selected=' + selectedClients.join(',') + '&q=' }}" title-field="name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="client_search" auto-match="true" clear-selected="true"> </div>
									  </div>
									  <div class="col-sm-4 file_upload">
										<button class="btn btn-primary" type="button" ng-click="addToExsitingClientList(selectedClient)" ng-disabled="!selectedClient">Add Customer</button>
									  </div>
									   <div class="col-sm-3 file_upload">
										<button class="btn btn-primary" type="button" ng-click="addToExsitingClientList(selectedClient, true)">Add All</button>
									  </div>
								  </div>
							  </div>
							  <div class="form-group" ng-show="selectedClient">
								You selected <strong>@{{selectedClient.originalObject.companyName}}</strong>. Click Add button to add to your list</span>
							  </div>
							  <div class="row table-responsive">
								<table class="table-bordered">
									<tr>
										<th>Client Name</th>
										<th>Action</th>
									</tr>
									<tr ng-repeat="mapping in clientMappings">
										<td>
											<span>@{{mapping.client_compname}}</span><br />
											<span>@{{mapping.client_adr1}}</span><br />
											<span>@{{mapping.client_city}}, @{{mapping.client_state}} - @{{mapping.client_postal}}</span><br />
											<span>Phone: @{{mapping.client_phone}}</span><br />
											<span>Email: @{{mapping.client_email}}</span><br />
											<span>Contact: @{{mapping.client_firstname}} @{{mapping.client_lastname}}</span><br />
										</td>
										<td align="center">
											<button class="btn btn-default btn-success" ng-click="removeMapping(mapping.id)">Remove</button>
										</td>
									</tr>
									<tr ng-show="!clientMappings.length">
										<td colspan="2" class="center"><strong>No Record Found</strong></td>
									</tr>
								</table>
							  </div>
								</div>
							</div>
					</div>
					<div class="col-sm-6">
						<div class="box-round">
							<h2>Layout Selection</h2>
							<div class="content">
								<div class="form-group">
									<label>Select Layout Type</label>
									<select class="form-control" ng-options="layoutType.id as layoutType.layoutName for layoutType in layoutTypes" ng-model="userLayoutSettings.layoutId" id="layoutId" name="layoutId" ng-change="saveCurrentLayoutSettings()" >
										<option value="" disabled selected ng-hide="userLayoutSettings.layoutId">Select your option</option>
									</select>
								</div>
								<div class="row" ng-show="userLayoutSettings.layoutId">
									<tags-input ng-model="userLayoutSettings.layoutDetails" display-property="displayName" key-property="displayField" placeholder="Enter Columns names" add-from-autocomplete-only="true">
										<auto-complete source="loadTags($query)" display-property="displayName" load-on-down-arrow="true" min-length="0"></auto-complete>
									</tags-input>
									<p> Press Down Arrow from keyboard or type few chars to list available fields</p>
									<div class="pull-right file_upload">
										<button ng-click="saveCurrentLayoutSettings()" class="btn btn-default" ng-disabled="!userLayoutSettings.layoutDetails || !userLayoutSettings.layoutDetails.length">Save</button>
									</div>
								</div>
								<div class="row table-responsive file_upload">
								<table class="table-bordered">
									<tr>
										<th align="center">Application setup</th>
										<th align="center">Action</th>
									</tr>
									<tr ng-repeat="mapping in layoutMappings">
										<td ng-if="mapping.layouts.length">
											<h4><strong>@{{mapping.layoutName}}</strong></h4>
											<p ng-repeat="fields in mapping.layouts">@{{fields.displayName}}</p>
										</td>
										<td align="center" ng-if="mapping.layouts.length">
											<button class="btn btn-primary" ng-click="removeLayoutMapping(mapping.id, userID)">Remove</button>
										</td>
										
									</tr>
								</table>
							</div>
							</div>
						</div>
					</div>
			</div>
			<div class="row">
					<div class="col-sm-6">
						<div class="box-round">
							<h2>Add Screen Filtering</h2>
							<div class="content">
							<form name="screen_filter" novalidate>
								<div class="form-group">
									<label>Screen Type</label>
									<select class="form-control" ng-options="layoutType.id as layoutType.layoutName for layoutType in layoutTypes" ng-model="screenFilter.layoutId" id="screen_layoutId" name="screen_layoutId" >
										<option value="" disabled selected ng-hide="screenFilter.layoutId" required>Select your option</option>
									</select>
								</div>
								<div class="form-group" ng-show="screenFilter.layoutId">
									<label>Screen Type</label>
									<select class="form-control" ng-options="attr.id as attr.displayName for attr in screenAttributes" ng-model="screenFilter.attributeId" id="screen_attribute_id" name="screen_attribute_id" required>
										<option value="" disabled selected ng-hide="screenFilter.attributeId">Select your option</option>
									</select>
								</div>
								<div class="form-group" ng-show="screenFilter.attributeId">
									<label>Operator</label>
									<select class="form-control" ng-options="opt.id as opt.operatorDesscription for opt in screenConstraintOperators" ng-model="screenFilter.screenConstraintOperatorId" id="screen_constriant_operator_id" name="screen_constriant_operator_id" required>
										<option value="" disabled selected ng-hide="screenFilter.screenConstraintOperatorId">Select your option</option>
									</select>
								</div>
								<div class="form-group" ng-show="screenFilter.screenConstraintOperatorId">
									<label>Value</label>
									<input type="text" class="form-control" ng-model="screenFilter.filterValue" required />
								</div>
								<div class="form-group" ng-show="screenFilter.screenConstraintOperatorId">
									<!-- <label>Value</label> -->
									<button type="button" class="btn btn-primary" ng-disabled="screen_filter.$invalid" ng-click="saveScreenFilter()">Save</button>
								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="box-round">
							<h2>Current Screen Filters</h2>
							<div class="content">
								<div class="row table-responsive file_upload">
								<table class="table-bordered">
									<tr>
										<th align="center">Application setup</th>
										<th align="center">Action</th>
									</tr>
									<tr ng-repeat="(key, filter) in screenFilters">
										<td>
											<h4><strong>@{{filter[0].layout_type.layoutName ? filter[0].layout_type.layoutName : 'Unknown' }}</strong></h4>
											<div class="row" ng-repeat="item in filter">
												<div class="col-xs-8">@{{item.attribute.displayName}} @{{item.operator.sqloperator}} @{{item.filterValue}}</div>
												<div class="col-xs-4"><button class="btn btn-primary" ng-click="removeScreenFiltering(item.layoutId, item.id)"><i class="fa fa-minus"></i></button></div>
											</div>
										</td>
										<td align="center">
											<button class="btn btn-primary" ng-click="removeScreenFiltering(key)">Remove</button>
										</td>
									</tr>
									<tr ng-show="screenFilters.length == 0"><td colspan="2" align="center"><strong>No Record Found</strong></td></tr>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
                    </div>
                </div>
				<div class="alert" ng-show="error.status||success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
					<button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">�</span></button>
					<strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
				</div>
            </div>
       </section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/UserManagerInit.js') }}"> </script>
@endsection
