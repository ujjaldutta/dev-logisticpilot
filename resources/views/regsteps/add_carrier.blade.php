 <div class="modal-dialog" role="document">
	 <style>
 
 .angucomplete-dropdown {
    background-color: #ffffff;
    border-color: #ececec;
    border-radius: 2px;
    border-style: solid;
    border-width: 1px;
    cursor: pointer;
    margin-top: -6px;
    padding: 6px;
    position: relative;
    width: 250px;
    z-index: 9999;
}
</style>
	<?php $uniqid=uniqid(); ?>
	<form name="addCarrierForm_{{$uniqid}}" enctype="multipart/form-data" novalidate>
    <div class="modal-content modal-dialog add-new-carrier-popup-content-wrapper">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="resetAddNewCarrier()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Carrier</h4>
      </div>
      <div class="modal-body">
			<div role="alert" class="alert alert-danger" ng-show="addCarrierMessage.error_status">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Error!</strong> @{{ addCarrierMessage.error_message}}.
			</div>
			<div role="alert" class="alert alert-success" ng-show="addCarrierMessage.success_status">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Success!</strong> @{{ addCarrierMessage.success_message}}.
			</div>
        <div class="add-new-carrier-popup-content">
            <h3>Carrier</h3>
		    <div class="row">
			  <div class="clearfix">	  
				  <div class="col-md-6">
					  <div class="form-group" ng-class="{'has-error': addCarrierForm_{{$uniqid}}.scac.$invalid}">
						  <label for="">SCAC / Carrier Code</label>
						  <input type="text" class="form-control" id="scac" name="scac" ng-model="addCarrier.scac" placeholder="" required>
						  <span class="help-block" ng-show="addCarrierForm_{{$uniqid}}.scac.$invalid">
							<span ng-show="addCarrierForm_{{$uniqid}}.scac.$error.required">SCAC / Carrier Code is required.</span>
						  </span>
					  </div>
					  
				  </div>
				  <div class="col-md-6">
					  <div class="form-group">
						  <label for="">MC#</label>
						  <input type="text" class="form-control" ng-model="addCarrier.carrier_mc" placeholder="" name="addcarrierMc" id="addcarrier-mc">
					  </div>
				 </div>
			 </div>
			 <div class="col-md-6">
				  <div class="form-group" ng-class="{'has-error': addCarrierForm_{{$uniqid}}.addcarrierName.$invalid}">
					  <label for="">Carrier Name</label>
					  <input type="text" class="form-control" name="addcarrierName" id="addcarrier-name" placeholder="" ng-model="addCarrier.carrier_name" required>
					  <span class="help-block" ng-show="addCarrierForm_{{$uniqid}}.addcarrierName.$invalid">
						<span ng-show="addCarrierForm_{{$uniqid}}.addcarrierName.$error.required">Name is required.</span>
					  </span>
				  </div>
				  <div class="form-group">
					  <label for="">Address Line 1</label>
					  <input type="text" class="form-control" name="addcarrierAdd1" id="addcarrier-add1" placeholder="" ng-model="addCarrier.carrier_adr1">
				  </div>
				  <div class="form-group">
					  <label for="">Address Line 2</label>
					  <input type="text" class="form-control" ng-model="addCarrier.carrier_adr2" placeholder="" name="addcarrierAdd2" id="addcarrier-add2">
				  </div>
				  <div class="form-group" ng-class="{'has-error': addCarrierForm_{{$uniqid}}.carrier_location.$invalid}">
					   <label>{{ Lang::get('registrationsteps.step1CompanyLocation') }}*</label>
						<input class="form-control" name="carrier_location" g-places-autocomplete ng-model="addCarrier.location" force-selection="true" options="autocompleteOptionsAddCarrier" required/>
				</div>
			</div>
			  <div class="col-md-6">
				  
				  <div class="form-group" ng-class="{'has-error': addCarrierForm_{{$uniqid}}.addcarrierEmail.$invalid}">
					  <label for="">Contact Email</label>
					  <input type="email" class="form-control" ng-model="addCarrier.carrier_email" placeholder="" name="addcarrierEmail" id="addcarrier-email" required>
					  <span class="help-block" ng-show="addCarrierForm_{{$uniqid}}.addcarrierEmail.$invalid">
						<span ng-show="addCarrierForm_{{$uniqid}}.addcarrierEmail.$error.required">Email is required.</span>
						<span ng-show="addCarrierForm_{{$uniqid}}.addcarrierEmail.$error.email">Email is invalid.</span>
					  </span>
				  </div>
				  <div class="form-group" ng-class="{'has-error': addCarrierForm_{{$uniqid}}.addcarrierPhone.$invalid}">
					  <label for="">Contact Phone</label>
					  <input type="text" class="form-control" ng-model="addCarrier.carrier_phone" placeholder="" name="addcarrierPhone" id="addcarrier-phone" ng-pattern="/^([0-9]{5,12})$/" required>
					  <span class="help-block" ng-show="addCarrierForm_{{$uniqid}}.addcarrierPhone.$invalid">
						<span ng-show="addCarrierForm_{{$uniqid}}.addcarrierPhone.$error.required">Phone is required.</span>
						<span ng-show="addCarrierForm_{{$uniqid}}.addcarrierPhone.$error.pattern">Phone is invalid.</span>
					  </span>
				  </div>
				  <div class="form-group carrier-connectivity" ng-show="addCarrier.id">
					<button class="btn btn-default" type="button" ngf-select ngf-change="upload($files)" accept="image/*" >{{ Lang::get('registrationsteps.step1UploadLogoButton') }}</button>
					 <img ngf-src="files[0]" ng-show="files[0].type.indexOf('image') > -1" class="thumb">
					<p>@{{addCarrier.log}}</p>
				  </div>
			  </div>
		  </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-cont">
                      <button  type="button" class="btn btn-default" data-dismiss="modal" ng-click="resetAddNewCarrier()">Close</button>
                      <button  type="button" class="btn btn-primary" ng-disabled="addCarrierForm_{{$uniqid}}.$invalid" ng-click="submitNewCarrier(addCarrierForm_{{$uniqid}})">@{{addCarrierMessage.saveButtonText}}</button>
        </div>
      </div>
    </div>
	</form>
  </div>