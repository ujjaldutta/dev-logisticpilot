@extends('layouts.account')
@section('page_title')
	Registration
@stop
@section('styles')
	<link href="{{ asset('bower_components/angucomplete-alt/angucomplete-alt.css') }}" rel="stylesheet">
	<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
	<style type="text/css">
		.highlight {color: #ff0000;}
		form.ng-invalid-autocomplete-required input.angucomplete-input-not-empty {border-color: red;}
	</style>
@stop
@section('content')
<div class="main-container" ui-view ng-init="saveButtonNormalTextLocale = '{{ Lang::get('registrationsteps.NextMessage') }}'; saveButtonLoadingTextLocale='{{ Lang::get('registrationsteps.NextLoadingMessage') }}'">

</div>
@stop
@section('modal_content')
<!-- Modal -->
<div class="modal fade" id="modal-frame" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	
</div>
<!-- Modal -->   
@stop
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/registration_steps.js') }}"> </script>
@stop