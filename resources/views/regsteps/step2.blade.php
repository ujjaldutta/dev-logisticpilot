 <form id="signup-form-step2" name="client_billing" novalidate>
<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}" ng-init="formStep2._token='{{ csrf_token() }}'"> -->
<input type="hidden" name="step" value="2" ng-init="formStep2.step=2" required>
<input type="hidden" name="clientID" value="{{ Auth::user()->clientID }}" ng-init="formStep2.clientID='{{ Auth::user()->clientID }}'; formStep2.billto_country='{{ $billing_info->billto_country or '' }}'" required>
 <h3>Bill to - Where carrier will be sending frieght bills</h3>
                  <div class="row">
                      <div class="col-lg-3">
                          <div class="form-group" ng-class="{'has-error': client_billing.billto_name.$invalid}">
                              <label>Bill to Name*</label>
                              <input type="text" name="billto_name" class="form-control" placeholder="" ng-model="formStep2.billto_name" required ng-init="formStep2.billto_name='{{ $billing_info->billto_name or '' }}'">
								<span class="help-block" ng-show="client_billing.billto_name.$invalid">
									<span ng-show="client_billing.billto_name.$error.required">Bill to Name is required.</span>
								</span>
                          </div>
                          <div class="form-group">
                              <label>Bill to Address Line 1</label>
                              <input type="text" name="billto_adr1" class="form-control" placeholder="" ng-model="formStep2.billto_adr1" ng-init="formStep2.billto_adr1='{{ $billing_info->billto_adr1 or ''}}'">
                          </div>
                          <div class="form-group">
                              <label>Bill to Address Line 2</label>
                               <input type="text" name="billto_adr2" class="form-control" placeholder="" ng-model="formStep2.billto_adr2" ng-init="formStep2.billto_adr2='{{ $billing_info->billto_adr2 or ''}}'">
                          </div>
				 <div class="form-group" ng-class="{'has-error': client_billing.billto_location.$invalid}">
				   <label>{{ Lang::get('registrationsteps.step1CompanyLocation') }}*</label>
					<input class="form-control" name="billto_location" g-places-autocomplete ng-model="formStep2.location" force-selection="true" options="autocompleteOptionsBilling" required/>
				 </div>
            </div>
			<div class="col-lg-3">
           <div class="form-group">
				<label>Contact Name</label>
				<input type="text" class="form-control" placeholder="" name="billto_contact" ng-model="formStep2.billto_contact" ng-init="formStep2.billto_contact='{{ $billing_info->billto_contact or ''}}'">
			</div>
			<div class="form-group" ng-class="{'has-error': client_billing.billto_email.$invalid}">
				<label>Billing Email</label>
				<input type="email" name="billto_email" class="form-control" placeholder="" ng-model="formStep2.billto_email" ng-init="formStep2.billto_email='{{ $billing_info->billto_email or ''}}'">
				<span class="help-block" ng-show="client_billing.billto_email.$invalid">
					<span ng-show="client_billing.billto_email.$error.email">Email is invalid.</span>
				</span>
			</div>
			<div class="form-group" ng-class="{'has-error': client_billing.billto_phone.$invalid}">
				<label>Billing Phone</label>
				<input type="text" name="billto_phone" class="form-control" placeholder="" ng-model="formStep2.billto_phone" ng-init="formStep2.billto_phone={{ $billing_info->billto_phone or ''}}" ng-pattern="/^([0-9]{5,12})$/">
				<span class="help-block" ng-show="client_billing.billto_phone.$invalid">
					<span ng-show="client_billing.billto_phone.$error.pattern">Please write a valid phone number</span>
				</span>
			</div>
            </div>
                  </div>
                  <div class="btn-cont">
                      <button  type="button" class="btn btn-default" ui-sref="form.profile">Back</button>
                      <button  type="button" class="btn btn-primary" ng-click="saveAndExit(client_billing)">Continue Later</button>
                     <!-- <button  type="button" class="btn btn-primary">Next</button> -->
					 <button  type="button" class="btn btn-primary" ng-disabled="client_billing.$invalid" ng-click="submitStep2(client_billing)" ui-sref="form.carrierSelection">@{{ formStep2.saveButtonText }}</button>
		  <img src="{{ asset('images/loading.gif') }}" alt="Loading.." height="32" width="32" ng-show="processing.status" />
		  <div class="alert alert-danger" ng-show="error.status">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong><span ng-bind="error.message"></span></strong>
			</div>
                  </div>