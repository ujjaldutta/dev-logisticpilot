<div class="reg_top">
	<h2>{{ Lang::get('registrationsteps.processName') }}</h2>
	<ul class="reg_steps">
		<li class="step_1" ui-sref-active="active"><a ui-sref=".profile"><span>1</span> {{ Lang::get('registrationsteps.companyStep') }}</a></li>
		<li class="step_2" ui-sref-active="active"><a ui-sref=".billing"><span>2</span> {{ Lang::get('registrationsteps.billingStep') }}</a></li>
		<li class="step_3" ui-sref-active="active"><a ui-sref=".carrierSelection"><span>3</span> {{ Lang::get('registrationsteps.carrierStep') }}</a></li>
		<li class="step_4" ui-sref-active="active"><a ui-sref=".carrierSetup"><span>4</span> {{ Lang::get('registrationsteps.carrierApiStep') }}</a></li>
		<li class="step_5" ui-sref-active="active" ng-if="validateAllSteps"><a ui-sref=".finish"><span>5</span> {{ Lang::get('registrationsteps.finish') }}</a></li>
	</ul>
</div>
  <div class="reg_bot"id="form-views" ui-view></div>