<form id="signup-form" name="client_profile" enctype="multipart/form-data" novalidate>
<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}" ng-init="formData._token='{{ csrf_token() }}'"> -->
<input type="hidden" name="step" value="1" ng-init="formData.step=1" required>
<input type="hidden" name="clientID" value="{{ Auth::user()->clientID }}" ng-init="formData.clientID='{{ Auth::user()->clientID }}'" required>
<input type="hidden" name="clientWlID" value="" ng-init="formData.clientWLID='{{ $profile->wishList->id or 0}}'; formData.client_country = '{{ $profile->client_country }}';" required>
<h3>{{ Lang::get('registrationsteps.step1Heading') }}</h3>
	  <div class="row">
		  <div class="col-lg-3">
			  <div class="form-group" ng-class="{'has-error': client_profile.client_compname.$invalid}">
					<label>{{ Lang::get('registrationsteps.step1CompanyName') }}*</label>
					<input type="text" name="client_compname" class="form-control" placeholder="" ng-model="formData.client_compname" required ng-init="formData.client_compname='{{ $profile->client_compname or '' }}'">
					<span class="help-block" ng-show="client_profile.client_compname.$invalid">
						<span ng-show="client_profile.client_compname.$error.required">{{ Lang::get('registrationsteps.step1ValidationCompanyRequired') }}</span>
					</span>
			  </div>
			  <div class="form-group">
				  <label>{{ Lang::get('registrationsteps.step1CompanyAdd1') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="formData.client_adr1" ng-init="formData.client_adr1='{{ $profile->client_adr1 or '' }}'">
			  </div>
			  <div class="form-group">
				  <label>{{ Lang::get('registrationsteps.step1CompanyAdd2') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="formData.client_adr2" ng-init="formData.client_adr2='{{ $profile->client_adr2 or '' }}'">
			  </div>
			  <div class="form-group" ng-class="{'has-error': client_profile.client_location.$invalid}">
			   <label>{{ Lang::get('registrationsteps.step1CompanyLocation') }}*</label>
				<input class="form-control" name="client_location" g-places-autocomplete ng-model="formData.location" force-selection="true" options="autocompleteOptionsProfile" required/ >
			  </div>
		  </div>
		  <div class="col-lg-3">
			  <div class="form-group">
				  <label>{{ Lang::get('registrationsteps.step1CompanyFname') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="formData.client_firstname" ng-init="formData.client_firstname='{{ $profile->client_firstname or '' }}'" >
			  </div>
			   <div class="form-group">
				  <label>{{ Lang::get('registrationsteps.step1CompanyLname') }}</label>
				  <input type="text" class="form-control" placeholder="" ng-model="formData.client_lastname" ng-init="formData.client_lastname='{{ $profile->client_lastname or '' }}'" >
			  </div>
			  <div class="form-group" ng-class="{'has-error': client_profile.client_email.$invalid}">
				  <label>{{ Lang::get('registrationsteps.step1CompanyEmail') }}</label>
				  <input type="email" name="client_email" class="form-control" placeholder="" ng-model="formData.client_email" ng-init="formData.client_email='{{ $profile->client_email or '' }}'" >
				  <span class="help-block" ng-show="client_profile.client_email.$invalid">
					<span ng-show="client_profile.client_email.$error.email">{{ Lang::get('registrationsteps.step1ValidationEmailInvalid') }}</span>
				  </span>
			  </div>
			  <div class="form-group" ng-class="{'has-error': client_profile.client_phone.$invalid}">
				  <label>{{ Lang::get('registrationsteps.step1CompanyPhone') }}</label>
				  <input type="text" name="client_phone" class="form-control" placeholder="" ng-model="formData.client_phone" ng-init="formData.client_phone='{{ $profile->client_phone or '' }}'" ng-pattern="/^([0-9]{5,12})$/">
				  <span class="help-block" ng-show="client_profile.client_phone.$invalid">
					<span ng-show="client_profile.client_phone.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
				  </span>
			  </div>
			  <div class="form-group" ng-class="{'has-error': client_profile.client_fax.$invalid}">
				  <label>{{ Lang::get('registrationsteps.step1CompanyFax') }}</label>
				  <input type="text" name="client_fax" class="form-control" placeholder="" ng-model="formData.client_fax" ng-init="formData.client_fax='{{ $profile->client_fax or '' }}'" ng-pattern="/^([0-9]{5,12})$/">
				  <span class="help-block" ng-show="client_profile.client_fax.$invalid">
					<span ng-show="client_profile.client_fax.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationFaxInvalid') }}</span>
				  </span>
			  </div>
			  <label>{{ Lang::get('registrationsteps.step1CompanyLogo') }}</label>
			  <div class="file_upload">
				  <button class="btn btn-primary" type="button" ngf-select ngf-change="upload($files)" accept="image/*" >{{ Lang::get('registrationsteps.step1UploadLogoButton') }}</button>
				  <img ngf-src="files[0]" ng-show="files[0].type.indexOf('image') > -1" class="thumb">
				<p>@{{formData.log}}</p>
				<p>( {{ Lang::get('registrationsteps.step1CompanyLogoMessage') }})</p>
			  </div>
			  <label>{{ Lang::get('registrationsteps.step1CompanyBMessage') }}</label>
			  <textarea class="form-control" rows="3" ng-model="formData.wl_message" ng-init="formData.wl_message='{{ $profile->wishList->wl_message or '' }}'"></textarea>
		  </div>
	  </div>
	  <div class="btn-cont">
		  <button  type="button" class="btn btn-primary" ng-click="saveAndExit(client_profile)">{{ Lang::get('registrationsteps.ContinueMessage') }}</button>
		  <!-- <button  type="button" class="btn btn-primary" ui-sref="form.billing" ng-disabled="formData.nextNotAllowed">Next</button> -->
		  <button  type="button" class="btn btn-primary" ui-sref="form.billing" ng-disabled="client_profile.$invalid" ng-click="submitStep1(client_profile)">@{{ formData.saveButtonText }}</button>
		  <img src="{{ asset('images/loading.gif') }}" alt="Loading.." height="32" width="32" ng-show="processing.status" />
		  <div class="alert alert-danger" ng-show="error.status">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong><span ng-bind="error.message"></span></strong>
			</div>
	  </div>
</form>