 <form id="signup-form-step3" name="carrier_selection" novalidate>
 <input type="hidden" name="step" value="3" ng-init="formStep3.step=3" required>
 </form>
	<div class="reg_bot">
                  <h3>Select your carrier partners</h3>
                  <div class="row map_carrier_top">
                      <div class="col-lg-6">
                          <div class="map_carrier_top_box">
                              <div class="form-group">
                                  <label>Start Entering Carrier Name</label>
								  <div class="row carrier-name-row">
									  <div class="col-sm-8">
										<div angucomplete-alt id="list-carriers" placeholder="Type Carrier Name e.g. UPS" pause="200" selected-object="currentSelectedProvider" remote-url="/list-carriers?selected=@{{ formStep3.carriers.join(',') }}&q=" search-fields="name" title-field="name" image-field="logo" minlength="1" input-class="form-control form-control-small" match-class="highlight" input-name="list-carriers" auto-match="true" clear-selected="true"></div>
									  </div>
									  <div class="col-sm-4 file_upload">
										<button class="btn btn-primary" type="button" ng-click="addToExsitingList(carrier_selection)" ng-disabled="!currentSelectedProvider">Add to my Carrier List</button>
									  </div>
								  </div>
							  </div>
							  <div class="form-group" ng-show="currentSelectedProvider">
								You selected <img src="" ng-src="@{{ currentSelectedProvider.originalObject.logo }}" alt="@{{currentSelectedProvider.originalObject.name}}"> <strong>@{{currentSelectedProvider.originalObject.name}}</strong>. Click Add button to add to your list</span>
							  </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="file_upload text-center">
                              <p>You didn't find carrier you are looking for? don't worry click below to add carrier or request support to add carrier</p>
                              <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-frame" ng-click="loadAddCarrierContent()">Add new Carrier</button>
                          </div>
                      </div>
                  </div>
                  <div class="row map_carrier_bottom">
						<selected-carrier-list></selected-carrier-list>
                  </div>
                  <div class="btn-cont">
                      <button  type="button" class="btn btn-default" ui-sref="form.billing">Back</button>
                      <button  type="button" class="btn btn-primary" ng-click="saveAndExit()">Continue Later</button>
                     <!-- <button  type="button" class="btn btn-primary">Next</button> -->
					 <button  type="button" class="btn btn-primary" ng-disabled="carrier_selection.$invalid" ng-click="submitStep3(carrier_selection)" ui-sref="form.carrierSetup">@{{ formStep3.saveButtonText }}</button>
		  <img src="{{ asset('images/loading.gif') }}" alt="Loading.." height="32" width="32" ng-show="processing.status" />
		  <div class="alert alert-danger" ng-show="error.status">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong><span ng-bind="error.message"></span></strong>
			</div>
                  </div>
    </div>

                 
                  