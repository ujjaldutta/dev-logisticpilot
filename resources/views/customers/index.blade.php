@extends('layouts.user_home')
@section('angular_controller')
    CustomerManagerController
@stop
@section('page_class')
    contract-page
@stop
@section('page_title')
    Welcome!
@stop
@section('breadcrumbs')
	{!! Breadcrumbs::render('setup_customer') !!}
@stop
@section('styles')
    <link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css') }}" rel="stylesheet">
@stop
@section('sidebar')
  @include('_partials/customer_management_sidebar')
@stop
@section('top_content')
    
@stop
@section('content')
<!-- content section-->
  <section class="content-wrapper">
    <div class="content-subcontainer">
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="contract_table" style="min-height: 797px;">
                <div style="height: 32px"><div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div></div>
                <div class="clearfix"></div>
                <div class="row contract_buttons">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 right_buttons pull-right">
                       <a href="{{url(\Config::get('app.frontPrefix'). '/customers/download')}}" class="btn btn-primary download_excel"><span></span>Download to Excel</a>
                       <a href="{{url(\Config::get('app.frontPrefix'). '/customers/add')}}" class="btn btn-primary download_excel">Add Customer</a>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 pull-left">
                      <a href="javascript:;" class="btn btn-default" ng-click="deleteAllCustomer()"><i class="fa fa-trash"></i></a>
                  </div>
                </div>
                <div class="table-responsive clearfix customer-table-transit">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">                    				
					<tr>
						<th align="center" valign="middle" scope="col" width="25px">
                            <div class="checkbox" style="width:15px;">
                                <label>
                                    <input type="checkbox" ng-click="checkAll()" ng-model="customerData.selectAll">
                                </label>
                            </div>
                        </th>	
						@foreach($allFields as $field)
							<th align="left" valign="middle" scope="col"><a href="javascript:;" ng-click="sortColumn('{{$field->displayField}}')">{{ $field->displayName }} <i class="fa " ng-show="predicate === '{{$field->displayField}}'" ng-class="{'fa-sort-asc' : !reverse, 'fa-sort-desc': reverse}"></i></a></th>					
						@endforeach
						<th align="left" valign="middle" scope="col">&nbsp;</th>
					</tr>					
					<tr ng-repeat="customer in customerData.list|filter:filterCustomerListing">
						<td align="center" valign="middle" width="25px">
							<div class="checkbox" style="width:15px;">
								<label>
									<input type="checkbox" ng-model="customer.selected" ng-change="checkSelection($index)">
								</label>
							</div>
						</td>
						
						@foreach($allFields as $field)
							<td align="left" valign="middle"><a href="{{ url(\config::get('app.frontPrefix'). '/customers/edit')}}/@{{customer.id}}" ng-click="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
								<?php echo "{{ customer.". $field->displayField ."}}"; ?></a></td>						
						@endforeach
						<td align="center" valign="middle">
							<a style="width:67px;margin-bottom:3px;" href="{{ url(\config::get('app.frontPrefix'). '/customers/edit')}}/@{{customer.id}}" class="" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;
							<a class="" href="javascript:;" ng-click="deleteCustomer(customer.id, true)" title="Delete"><i class="fa fa-trash"></i></a>
						</td>						
						
					</tr>
					<tr ng-hide="customerData.list.length">
						<td colspan="11" align="center"><strong>No Record Found!</strong></td>
					</tr>
              	  </table>
              </div>
				<div class="row">
					<div class="col-sm-4 col-md-4">
						<div class="row" style="margin-top:20px;">
							<div class="col-sm-7">
								<select ng-options="pageno.id as pageno.val for pageno in pageNos" ng-model="recordLimit" id="record_limit_selecter" name="record_limit_selecter" class="form-control"></select>
							</div>
						</div>
					</div>
					
					<div class="col-sm-8 col-md-8">
							<div class="row">
								<!--<div class="col-sm-7">
										
								</div>-->
								<div class="col-sm-12">
									<div class="text-right">
										<span style="vertical-align: top; display: inline-block; margin: 26px 10px 0px 0px;">Showing record @{{(recordLimit * currentPage) > customerData.count ? customerData.count : recordLimit * currentPage}} of @{{customerData.count}}</span>
										<pagination total-items="customerData.count" items-per-page="recordLimit" ng-model="currentPage" ng-change="pageChanged()"></pagination>
									</div>
								</div>
							</div>
					</div>
				</div>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CustomerManager.js') }}"> </script>
@endsection
