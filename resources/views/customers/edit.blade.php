@extends('layouts.user_home')
@section('angular_controller')
CustomerEditController
@stop
@section('page_class')
contract-page
@stop
@section('page_title')
Welcome!
@stop
@section('breadcrumbs')
{!! Breadcrumbs::render('edit_customer') !!}
@stop
@section('styles')
<link href="{{ asset('bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/angularjs-slider/dist/rzslider.min.css')}}" rel="stylesheet">
@stop
@section('sidebar')
@include('_partials/customer_management_sidebar')
@stop
@section('top_content')

@stop
@section('content')
<!-- content section-->
<section class="content-wrapper">
    <div class="contract_table">
        <div class="loading hidden pull-right"><img src="{{asset('images/ajax-loader.gif')}}" alt="Loading..."/></div>
        <div class="clearfix"></div>
        <div id="horizontalTab1" class="tab-hld easytabs">
            <ul class="resp-tabs-list">
                <li>Customer Setup</li>
                <li>Customer Default</li>
            </ul>
            <div class="resp-tabs-container">
                <div class="tab-content">
                    <div class="row">
                        <form id="customerProfileForm" name="client_profile" enctype="multipart/form-data" novalidate ng-init="custID ='{{$customer_id}}'">
                            <div class="col-lg-3">
                                <div class="form-group" ng-class="{'has-error': client_profile.client_compname.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyName') }}*</label>
                                    <input type="text" name="client_compname" class="form-control" placeholder="" ng-model="clientData.client_compname" required>
                                    <span class="help-block" ng-show="client_profile.client_compname.$invalid">
                                        <span ng-show="client_profile.client_compname.$error.required">{{ Lang::get('registrationsteps.step1ValidationCompanyRequired') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.client_firstname.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyFname') }}</label>
                                    <input type="text" class="form-control" placeholder="" ng-model="clientData.client_firstname" name="client_firstname" required>
                                    <span class="help-block" ng-show="client_profile.client_firstname.$invalid">
                                        <span ng-show="client_profile.client_firstname.$error.required">{{ Lang::get('registrationsteps.step1ValidationCompanyRequired') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.client_lastname.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyLname') }}</label>
                                    <input type="text" class="form-control" placeholder="" ng-model="clientData.client_lastname" name="client_lastname" required>
                                    <span class="help-block" ng-show="client_profile.client_lastname.$invalid">
                                        <span ng-show="client_profile.client_lastname.$error.required">{{ Lang::get('registrationsteps.step1ValidationCompanyRequired') }}</span>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyAdd1') }}</label>
                                    <input type="text" class="form-control" placeholder="" ng-model="clientData.client_adr1">
                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyAdd2') }}</label>
                                    <input type="text" class="form-control" placeholder="" ng-model="clientData.client_adr2">
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.client_phone.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyPhone') }}</label>
                                    <input type="text" name="client_phone" class="form-control" placeholder="" ng-model="clientData.client_phone" ng-pattern="/^([0-9]{5,12})$/">
                                    <span class="help-block" ng-show="client_profile.client_phone.$invalid">
                                        <span ng-show="client_profile.client_phone.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationPhoneInvalid') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.client_fax.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyFax') }}</label>
                                    <input type="text" name="client_fax" class="form-control" placeholder="" ng-model="clientData.client_fax" ng-pattern="/^([0-9]{5,12})$/">
                                    <span class="help-block" ng-show="client_profile.client_fax.$invalid">
                                        <span ng-show="client_profile.client_fax.$error.pattern">{{ Lang::get('registrationsteps.step1ValidationFaxInvalid') }}</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.client_location.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyLocation') }}*</label>
                                    <input class="form-control" name="client_location" g-places-autocomplete ng-model="clientData.location" force-selection="true" options="autocompleteOption" required />
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group" ng-class="{'has-error': client_profile.client_email.$invalid}">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyEmail') }}</label>
                                    <input type="email"  ng-model="clientData.client_email" class="form-control" placeholder="" required>
                                    <span class="help-block" ng-show="client_profile.client_email.$invalid">
                                        <span ng-show="client_profile.client_email.$error.email">{{ Lang::get('registrationsteps.step1ValidationEmailInvalid') }}</span>
                                    </span>
                                </div>
                                <div class="form-group file_upload">
                                    <label>Reset and Send Password</label>
                                    <button class="btn btn-primary" ng-disabled="loginform.email.$invalid || loginform.password.$invalid" ng-click="updatePassword()">@{{buttonMailText}}</button>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.language.$invalid}">
                                    <label>language</label>
                                    <select class="form-control" ng-options="lng.id as lng.name for lng in languages" ng-model="clientData.wish_list.wl_culture" id="language" name="language" required></select>
                                    <span class="help-block" ng-show="client_profile.language.$invalid">
                                        <span ng-show="client_profile.language.$error.required">Language is required</span>
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{'has-error': client_profile.theme.$invalid}">
                                    <label>Theme</label>
                                    <select class="form-control" ng-options="theme.id as theme.theme_name for theme in themes" ng-model="clientData.wish_list.wl_theme" id="theme" name="theme" required></select>
                                    <span class="help-block" ng-show="client_profile.theme.$invalid">
                                        <span ng-show="client_profile.theme.$error.required">Theme is required</span>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyLogo') }}</label>
                                    <div class="file_upload">
                                        <button class="btn btn-primary" type="button" ngf-select ngf-change="upload($files)" accept="image/*" >{{ Lang::get('registrationsteps.step1UploadLogoButton') }}</button>
                                        <img ngf-src="files[0]" ng-show="files[0].type.indexOf('image') > -1" class="thumb">
                                        <p>@{{clientData.log}}</p>
                                        <p>( {{ Lang::get('registrationsteps.step1CompanyLogoMessage') }})</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ Lang::get('registrationsteps.step1CompanyBMessage') }}</label>
                                    <textarea class="form-control" rows="3" ng-model="clientData.wish_list.wl_message"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-cont" style="padding-top:20px;">
                                <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
                                <button  type="button" class="btn btn-primary" ng-disabled="client_profile.$invalid" ng-click="saveProfileData(client_profile)">Save / Update</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content">
                    <form name="frmClientDefaultSettings" id="client-default-settings" novalidate>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="box-round">
                                    <h2>Default Shipping Location</h2>
                                    <div class="content">
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.pickup_locname.$invalid}">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.pickup_locname" name="pickup_locname">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.pickup_locname.$invalid">
                                                <span ng-show="frmClientDefaultSettings.pickup_locname.$error.required">Pick-up Location Name is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.pickup_locadr1.$invalid}">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.pickup_locadr1" name="pickup_locadr1">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.pickup_locadr1.$invalid">
                                                <span ng-show="frmClientDefaultSettings.pickup_locadr1.$error.required">Pick-up Address1 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.pickup_locadr2.$invalid}">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.pickup_locadr2" name="pickup_locadr2">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.pickup_locadr2.$invalid">
                                                <span ng-show="frmClientDefaultSettings.pickup_locadr2.$error.required">Pick-up Address2 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.pickup_location.$invalid}">
                                            <label>Pickup Location</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.pickup_location" name="pickup_location"  g-places-autocomplete force-selection="true" options="autocompleteOption">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.pickup_location.$invalid">
                                                <span ng-show="frmClientDefaultSettings.pickup_location.$error.required">Pick-up Location is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.pickup_notes.$invalid}">
                                            <label>Default Shipping Notes</label>
                                            <textarea class="form-control" rows="4" placeholder="" ng-model="clientDefaultSettings.pickup_notes" name="pickup_notes"></textarea>
                                            <span class="help-block" ng-show="frmClientDefaultSettings.pickup_notes.$invalid">
                                                <span ng-show="frmClientDefaultSettings.pickup_notes.$error.required">Pick-up Location is required.</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box-round">
                                    <h2>Default Receiving Location</h2>
                                    <div class="content">
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_locname.$invalid}">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.delivery_locname" name="delivery_locname">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.delivery_locname.$invalid">
                                                <span ng-show="frmClientDefaultSettings.delivery_locname.$error.required">Delivery Location Name is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_locadr1.$invalid}">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.delivery_locadr1" name="delivery_locadr1">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.delivery_locadr1.$invalid">
                                                <span ng-show="frmClientDefaultSettings.delivery_locadr1.$error.required">Delivery Address1 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_locadr2.$invalid}">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.delivery_locadr2" name="delivery_locadr2">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.delivery_locadr2.$invalid">
                                                <span ng-show="frmClientDefaultSettings.delivery_locadr2.$error.required">Delivery Address2 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_location.$invalid}">
                                            <label>Delivery Location</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.delivery_location" name="delivery_location"  g-places-autocomplete force-selection="true" options="autocompleteOption">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.delivery_location.$invalid">
                                                <span ng-show="frmClientDefaultSettings.delivery_location.$error.required">Delivery Location is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_notes.$invalid}">
                                            <label>Default Shipping Notes</label>
                                            <textarea class="form-control" rows="4" placeholder="" ng-model="clientDefaultSettings.delivery_notes" name="delivery_notes"></textarea>
                                            <span class="help-block" ng-show="frmClientDefaultSettings.delivery_notes.$invalid">
                                                <span ng-show="frmClientDefaultSettings.delivery_notes.$error.required">Pick-up Location is required.</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box-round">
                                    <h2>Default Bill to Location</h2>
                                    <div class="content">
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.billto_locname.$invalid}">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.billto_locname" name="billto_locname">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.billto_locname.$invalid">
                                                <span ng-show="frmClientDefaultSettings.billto_locname.$error.required">Billing Location Name is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.billto_locadr1.$invalid}">
                                            <label>Address 1</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.billto_locadr1" name="billto_locadr1">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.billto_locadr1.$invalid">
                                                <span ng-show="frmClientDefaultSettings.billto_locadr1.$error.required">Billing Address1 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.billto_locadr2.$invalid}">
                                            <label>Address 2</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.billto_locadr2" name="billto_locadr2">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.billto_locadr2.$invalid">
                                                <span ng-show="frmClientDefaultSettings.billto_locadr2.$error.required">Billing Address2 is required.</span>
                                            </span>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.billto_location.$invalid}">
                                            <label>Billing Location</label>
                                            <input type="text" class="form-control" placeholder="" ng-model="clientDefaultSettings.billto_location" name="billto_location"  g-places-autocomplete force-selection="true" options="autocompleteOption">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.billto_location.$invalid">
                                                <span ng-show="frmClientDefaultSettings.billto_location.$error.required">Delivery Location is required.</span>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box-round">
                                    <h2>Shipping Product Default</h2>
                                    <div class="content">
                                        <div class="form-group">
                                            <label>Commodity</label>
                                            <select ng-options="item.id as item.label for item in products" ng-model="clientDefaultSettings.product_name" class="form-control"></select>
                                        </div>
                                        <div class="form-group">
                                            <label>Freight Class</label>
                                            <select ng-options="item.id as item.label for item in productClasses" ng-model="clientDefaultSettings.product_class" class="form-control"></select>
                                        </div>
                                        <div class="form-group">
                                            <label>Payment Terms</label>
                                            <select ng-options="item.id as item.label for item in shipmentTerms" ng-model="clientDefaultSettings.shipment_terms" class="form-control"></select>
                                        </div>
                                        <div class="form-group">
                                            <label>Use BOL template as <a href="#">Show Sample</a></label>
                                            <select ng-options="item.id as item.label for item in bolTemplates" ng-model="clientDefaultSettings.bol_templateId" class="form-control"></select>
                                        </div>
                                        <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.email_bolto.$invalid}">
                                            <label>Email BOL to</label>
                                            <input type="email" class="form-control"  placeholder="username@gmail.com" ng-model="clientDefaultSettings.email_bolto" name="email_bolto">
                                            <span class="help-block" ng-show="frmClientDefaultSettings.email_bolto.$invalid">
                                                <span ng-show="frmClientDefaultSettings.email_bolto.$error.required">BOL email is required.</span>
                                                <span ng-show="frmClientDefaultSettings.email_bolto.$error.email">BOL email is Invalid.</span>
                                            </span>
                                            <p>Multiple e-mail addresses must be separated</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row topPad40">
                            <div class="col-lg-6">
                                <h2>Invoice</h2>
                                <div class="invoices_cont">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row noPad">
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <label>Invoice Term</label>
                                                        <select ng-options="item.id as item.label for item in invoiceTerms" ng-model="clientDefaultSettings.invoice_term" class="form-control"></select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="form-group">
                                                        <label>Email invoice on</label>
                                                        <select ng-options="item.id as item.label for item in invoiceDays" ng-model="clientDefaultSettings.email_invoiceon" class="form-control"></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Use Invoice Template as</label>
                                                <select ng-options="item.id as item.label for item in invoiceTemplates" ng-model="clientDefaultSettings.invoice_templateId" class="form-control"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.email_invoiceto.$invalid}">
                                                <label>Email BOL to</label>
                                                <input type="email" class="form-control"  placeholder="username@gmail.com" ng-model="clientDefaultSettings.email_invoiceto" name="email_invoiceto">
                                                <span class="help-block" ng-show="frmClientDefaultSettings.email_invoiceto.$invalid">
                                                    <span ng-show="frmClientDefaultSettings.email_invoiceto.$error.required">BOL email is required.</span>
                                                    <span ng-show="frmClientDefaultSettings.email_invoiceto.$error.email">BOL email is Invalid.</span>
                                                </span>
                                                <p>Multiple e-mail addresses must be separated</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.invoice_postal.$invalid}">
                                                <label>Postal</label>
                                                <input type="text" class="form-control"  placeholder="" ng-model="clientDefaultSettings.invoice_postal" name="invoice_postal">
                                                <span class="help-block" ng-show="frmClientDefaultSettings.invoice_postal.$invalid">
                                                    <span ng-show="frmClientDefaultSettings.invoice_postal.$error.required">Invoice postal is required.</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" ng-class="{'has-error': frmClientDefaultSettings.delivery_notes.$invalid}">
                                                <label>Default Shipping Notes</label>
                                                <textarea class="form-control" rows="4" placeholder="" ng-model="clientDefaultSettings.delivery_notes" name="delivery_notes"></textarea>
                                                <span class="help-block" ng-show="frmClientDefaultSettings.delivery_notes.$invalid">
                                                    <span ng-show="frmClientDefaultSettings.delivery_notes.$error.required">Pick-up note is required.</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box-round topMar40">
                                    <h2>3PL Only Setup</h2>
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="semiBold">Received EDI</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.receive_edi204">204
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.receive_edi210">210
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.receive_edi214">214
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.receive_edi216">216
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="semiBold">Send EDI</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.send_edi204">204
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.send_edi210">210
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="clientDefaultSettings.send_edi214">214
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="box-round topMar40">
                                    <h2>Alert and Information</h2>
                                    <div class="content">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">Stop over credit limit
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="">Alert message
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="btn-cont" style="padding-top:20px;">
                            <button  type="button" class="btn btn-default" ng-click="cancelEditing()" style="color:black">Cancel</button>
                            <button  type="button" class="btn btn-primary" ng-disabled="frmClientDefaultSettings.$invalid" ng-click="saveSettingsData(frmClientDefaultSettings)">Save / Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="alert" ng-show="error.status || success.status" ng-class="{'alert-danger': error.status, 'alert-success': success.status}">
            <button aria-label="Close" class="close" type="button" ng-click="hideMessage()"><span aria-hidden="true">×</span></button>
            <strong><span ng-show="error.status" ng-bind="error.message"></span><span ng-show="success.status" ng-bind="success.message"></span></strong>
        </div>
    </div>
</section>
<!-- /content section-->
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('js/CustomerManager.js')}}"></script>
@endsection
