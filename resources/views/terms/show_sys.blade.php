@extends('layouts.master')
@section('page_title')
	Logistics Pilot | Terms and Conditions
@stop
@section('page_heading')
	Terms and Conditions
@stop
@section('content')
	<section class="blog-wrapper">
    	<div class="container">
        	<div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                   	<div class="col-md-10">
 						<div class="widget">
                        	<div class="title">
                            	<h3>Terms and Conditions</h3>
                            </div><!-- end title -->
							@yield('message')
								@if(Session::has('success'))
									<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<p> {{ Session::get('success') }}	</p>	
									</div>
								@endif
								@if(Session::has('error'))
									<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<p> {{ Session::get('error') }}	</p>	
									</div>
								@endif
								@if($errors->has())
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert">
											<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
										</button>
									@foreach ($errors->all() as $error)
											<p> {{ $error }}</p>
									@endforeach
									</div>
								@endif
							<form action="{{ url(Config::get('app.frontPrefix'). '/terms/accept-sys-terms') }}" method="POST" name="site_terms" id="site_terms">
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
								<div class="form-group">
									<textarea class="form-control" readonly="readonly" cols="80" rows="8">{{{ $term->ts_message }}}</textarea>
								</div>
								<div class="form-group">
									<label><input type="checkbox" class="" ng-model="terms.accept" name="accepted" /> I accept the terms and conditions </label>
								</div>
								<div class="form-group">
									<input type="submit" value="Accept" class="btn btn-primary" data-ng-disabled="!terms.accept">
									<button type="button" class="btn btn-default" onclick="window.location.href='/auth/logout'">Decline</button>
								</div>
							</form>
                        </div><!-- end widget -->
					</div><!-- end col-md-4 -->
                    <div class="col-md-2">
						<div class="widget">
							&nbsp;
						</div>
						
					<div class="form-group">
						<img src="{{ asset('images/lock.png') }}" class="img-responsive" alt="Terms and conditions"></div>
					</div>
            		</div><!-- end col-md-4 -->
            	</div><!-- end row --> 
            </div><!-- end content -->
    	</div><!-- end container -->
    </section><!-- end white-wrapper -->
@stop
@section('scripts')
	<script src="{{ asset('js/login.js') }}"></script>
@stop