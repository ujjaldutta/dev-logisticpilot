@extends('layouts.account_home')
@section('page_title')
	Welcome!
@stop
@section('angular_controller')
    CustomerDashabordSetupController
@stop
@section('styles')
	<link rel="stylesheet" href="{{ asset('css/additional_cssrules.css') }}" />
@stop
@section('sidebar')
	 
@stop
@section('content')
	<div class="row clearfix">
        	<div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-users fa-2x"></i>
                            <h3>Client List</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:;" ng-click="resetCustomerSelection(0)" class="details_link">Reset Selected Customer</a></li>
                                    <li><a href="javascript:;" ng-click="reloadPanel('ccl')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/customers') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-customer-section" style="position: absolute; width: 100%; height: 100%; background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="customer in customerData.list"><a href="javascript:;" ng-click="changeCustomer(customer.id, $event)">@{{ customer.client_code + '-' }} @{{customer. client_compname}}</a></li>
                               <li ng-show="customerData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="customerData.count >= 0 ">Total: <span class="summary-count">@{{ customerData.count }} Record(s)</span></p>
						<p ng-show="customerData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="yellow">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-truck fa-2x"></i>
                            <h3>Carriers</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('crr')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/setup-connectivity') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-customer-carriers-section" style="position: absolute; width: 100%; height: 100%; background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                                <li ng-repeat="carrier in customerCarrierData.list">@{{ carrier.name }}</li>
								<li ng-show="customerCarrierData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="customerCarrierData.count >= 0 ">Total: <span class="summary-count">@{{ customerCarrierData.count }} Record(s)</span></p>
						<p ng-show="customerCarrierData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="green">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                           <i class="fa fa-phone fa-2x"></i>
                            <h3>Client Contact</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
                            <ul>
                               <li>No Data Available</li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p>Total: 0 Record(s)</p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p class="purple">Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-gears fa-2x"></i>
                            <h3>Default Settings</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries">
                            <ul>
                                <li>No Data Available</li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p>Total: 0 Record(s)</p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                           <i class="fa fa-connectdevelop fa-2x"></i>
                            <h3>EDI Setup</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
                            <ul>
                               <li>No Data Available</li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p>Total: 0 Record(s)</p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-map-marker fa-2x"></i>
                            <h3>Location</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('lcn')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/customer-locations') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);" id="loading-customer-location-section">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                              <li ng-repeat="location in customerLocationData.list">@{{ location.location_code + '-' }} @{{location. location_name}}</li>
                               <li ng-show="customerLocationData.count == 0"><strong>No Data Available</strong></li>  
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="customerLocationData.count >= 0 ">Total: <span class="summary-count">@{{ customerLocationData.count }} Record(s)</span></p>
                    	<p ng-show="customerLocationData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-users fa-2x"></i>
                            <h3>Users</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('usr')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/users') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                      	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);" id="loading-user-section">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                               <li ng-repeat="user in userData.list">@{{ user.usr_firstname }} @{{user.usr_lastname}}</li>
                               <li ng-show="userData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="userData.count >= 0">Total:  <span class="summary-count">@{{ userData.count }} Record(s)</span></p>
						<p ng-show="userData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>Top Accessorials: Liftgate</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            	<div class="career_box">
                	<div class="career_box_top">
                        <div class="career_box_head clearfix">
                            <i class="fa fa-cubes fa-2x"></i>
                            <h3>Products</h3>
                            <div class="dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{!! asset("images/open_button.png") !!}" alt class="open_button"></a>
                                <ul class="dropdown-menu">
									<li><a href="javascript:;" ng-click="reloadPanel('prd')" class="details_link">Reload</a></li>
                                    <li><a href="{!! url(Config::get('app.frontPrefix'). '/customer-products') !!}" class="details_link">Details</a></li>
                                </ul>
                            </div>
                     	</div>
                        <div class="entries" style="position:relative">
							<div class="loading-dashboard-item" id="loading-customer-product-section" style="position: absolute; width: 100%; height: 100%;background-color: rgba(255,255,255, 0.9);">
								<img src="{!! asset('images/loading.gif') !!}" width="32" style="top: 50%; left: 50%; position: relative; margin: -16px 0px 0px -16px;">
							</div>
                            <ul>
                              <li ng-repeat="product in customerProductData.list">@{{ product.item_code }} @{{product.item_name}}</li>
                              <li ng-show="customerProductData.count == 0"><strong>No Data Available</strong></li>
                            </ul>
                        </div>
                	</div>
                    <div class="career_box_bottom">
                    	<p ng-show="customerProductData.count >= 0">Total:  <span class="summary-count">@{{ customerProductData.count }} Record(s)</span></p>
						<p ng-show="customerProductData.count < 0 ">Total: <span class="summary-count">Refreshing...</span></p>
                    </div>
                    <div class="career_box_details">
                    	<a href="#" class="close"></a>
                    	<p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
<script src="{{ asset('js/CustomerDashabordSetup.js') }}"> </script>
@endsection