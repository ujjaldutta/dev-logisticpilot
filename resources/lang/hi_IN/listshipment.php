<?php
return[
	'originbox_heading' => 'कंपनी विवरण दर्ज करें',
	'destinationbox_heading' => 'कंपनी विवरण दर्ज करें',
	
	'purchaseOrderNumber' => 'कंपनी विवरण दर्ज करें',
	'expectedPickupDate' => 'कंपनी विवरण दर्ज करें',
	'pickupDate' => 'कंपनी विवरण दर्ज करें',
	'openTime1' => 'कंपनी विवरण दर्ज करें',
	'closeTime1' => 'कंपनी विवरण दर्ज करें',
	'proNumber1' => 'कंपनी विवरण दर्ज करें',
	'expectedDeliveryDate' => 'कंपनी विवरण दर्ज करें',
	'deliveredDate' => 'कंपनी विवरण दर्ज करें',
	'openTime2' => 'कंपनी विवरण दर्ज करें',
	'closeTime2' => 'कंपनी विवरण दर्ज करें',
	'carrierQuotenumber' => 'कंपनी विवरण दर्ज करें',
	'billableweight' => 'कंपनी विवरण दर्ज करें',
	'costperpound' => 'कंपनी विवरण दर्ज करें',
	'proNumber2' => 'कंपनी विवरण दर्ज करें',
	'shipmentvalue' => 'कंपनी विवरण दर्ज करें',
	'shp_distance' => 'कंपनी विवरण दर्ज करें',
	
	'invoiceStatus' => 'कंपनी विवरण दर्ज करें',
	'paymentStatus' => 'कंपनी विवरण दर्ज करें',
	'billofLanding' => 'कंपनी विवरण दर्ज करें',
	'carrierQuote' => 'कंपनी विवरण दर्ज करें',
	'customerQuote' => 'कंपनी विवरण दर्ज करें',
	'customerInvoice' => 'कंपनी विवरण दर्ज करें',	
	
	'tab_title1' => 'कंपनी विवरण दर्ज करें',
	'tab_title2' => 'कंपनी विवरण दर्ज करें',
	'tab_title3' => 'कंपनी विवरण दर्ज करें',
	'tab_title4' => 'कंपनी विवरण दर्ज करें',
	'tab_title5' => 'कंपनी विवरण दर्ज करें',
	'tab_title6' => 'कंपनी विवरण दर्ज करें',
	'tab_title7' => 'कंपनी विवरण दर्ज करें',
	'tab_title8' => 'कंपनी विवरण दर्ज करें',
	
	'loadBoardStatistics' => 'कंपनी विवरण दर्ज करें', 
	'emailStatistics' => 'कंपनी विवरण दर्ज करें',
	'userAuditTrail' => 'कंपनी विवरण दर्ज करें',
	'scanDocumentViewDocument' => 'कंपनी विवरण दर्ज करें',	
	
	'productHeading' => 'कंपनी विवरण दर्ज करें',
	'stopHeading' => 'कंपनी विवरण दर्ज करें',
	
	
	'carrierRespondedforSPOTQuote' => 'कंपनी विवरण दर्ज करें',
	'tlPosting' => 'कंपनी विवरण दर्ज करें',
	'customerCost' => 'कंपनी विवरण दर्ज करें',
	'carrierCost' => 'कंपनी विवरण दर्ज करें',
	
	'customerNotes' => 'कंपनी विवरण दर्ज करें',
	'carrierNotes' => 'कंपनी विवरण दर्ज करें',
	'internalNotes' => 'कंपनी विवरण दर्ज करें',
	
	'originTerminal' => 'कंपनी विवरण दर्ज करें',
	'destinationTerminal' => 'कंपनी विवरण दर्ज करें',
	
	'trackTrace' => 'कंपनी विवरण दर्ज करें',	
	
];