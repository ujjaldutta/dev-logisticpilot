<?php

return[
	'bookingNumberId' => 'कंपनी विवरण दर्ज करें',
	'equipmentType' => 'बिलिंग विवरण दर्ज करें',
	'payTerms' => 'आपका वाहक चुने',
	'mode' => 'बाहक अनुबंध करें',
	'purchaseOrder' => 'पंजीकरण की प्रक्रिया',
	'pickupLocation_box' => 'कंपनी विवरण भरें',
	'deliveryLocation_box' => 'आपके संगठन का नाम',
	'billToAddress_box' => 'कंपनी का पता पंक्ति 1',
	'pick_addressLine1' => 'कंपनी का पता पंक्ति 2',
	'pick_addressLine2' => 'देश',
	'pick_location' => 'स्थान',
	'pick_pickupDate' => 'डाक-सम्बन्धी',
	'pick_openTime' => 'राज्य',
	'pick_closeTime' => 'शहर',
	'pick_contactPhone' => 'संपर्क प्रथम नाम',
	'pick_contactName' => 'संपर्क अंतिम नाम',
	'pick_note' => 'संपर्क ईमेल',
	
	'del_addressLine1' => 'संपर्क प्रथम नाम',
	'del_addressLine2' => 'संपर्क प्रथम नाम',
	'del_location' => 'संपर्क प्रथम नाम',
	'del_delDate' => 'संपर्क प्रथम नाम',
	'del_openTime' => 'संपर्क प्रथम नाम',
	'del_closeTime' => 'संपर्क प्रथम नाम',
	'del_contactPhone' => 'संपर्क प्रथम नाम',
	'del_contactName' => 'संपर्क प्रथम नाम',
	'del_note' => 'संपर्क प्रथम नाम',
	
	'bill_addressLine1' => 'संपर्क प्रथम नाम',
	'bill_addressLine2' => 'संपर्क प्रथम नाम',
	'bill_location' => 'संपर्क प्रथम नाम',	
	
	'creditLimit' => 'संपर्क प्रथम नाम',
	'similarShipments' => 'संपर्क प्रथम नाम',
	'suggestedCarrier' => 'संपर्क प्रथम नाम',
	'tlLoadBoard' => 'संपर्क प्रथम नाम',
	
	'stopHeading' => 'संपर्क प्रथम नाम',
	'searchName' => 'संपर्क प्रथम नाम',
	'stop_addressLine1' => 'संपर्क प्रथम नाम',
	'stop_addressLine2' => 'संपर्क प्रथम नाम',
	'stop_city' => 'संपर्क प्रथम नाम',
	'stop_state' => 'संपर्क प्रथम नाम',
	'stop_postal' => 'संपर्क प्रथम नाम',
	'stop_type' => 'संपर्क प्रथम नाम',
	
	'productHeading' => 'संपर्क प्रथम नाम',
	
	
	'accessorialHeading' => 'संपर्क प्रथम नाम',
	'shipmentDetailsHeading' => 'संपर्क प्रथम नाम',
	'row_heading1' => 'संपर्क प्रथम नाम',
	'row_heading2' => 'संपर्क प्रथम नाम',
	'row_heading3' => 'संपर्क प्रथम नाम',
	'row_heading4' => 'संपर्क प्रथम नाम',
	'row_heading5' => 'संपर्क प्रथम नाम',
	'row_heading6' => 'संपर्क प्रथम नाम',
	'row_heading7' => 'संपर्क प्रथम नाम',
	'row_heading8' => 'संपर्क प्रथम नाम',
	
	
	'shipment_notesHeading' => 'संपर्क प्रथम नाम',
	'customerNote' => 'संपर्क प्रथम नाम',
	'carrierNote' => 'संपर्क प्रथम नाम',
	'internalNote' => 'संपर्क प्रथम नाम',	
		
];