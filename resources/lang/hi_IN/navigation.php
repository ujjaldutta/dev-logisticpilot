<?php

return[
	'logout' => 'लॉग आउट',
	'welcome' => 'स्वागत',
	'updateProfile' => 'प्रोफ़ाइल अपडेट करें',
	'changePassword' => 'पासवर्ड बदलें',
	'supportMessage' => 'आपका सहायक दल',
	'call' => 'कॉल',
];