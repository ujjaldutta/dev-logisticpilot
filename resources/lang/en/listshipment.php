<?php

return[
	'originbox_heading' => 'PICKUP LOCATION',
	'destinationbox_heading' => 'DESTINATION ADDRESS',
	
	'purchaseOrderNumber' => 'Purchase Order Number',
	'expectedPickupDate' => 'Expected Pickup Date',
	'pickupDate' => 'Pickup Date',
	'openTime1' => 'Open Time',
	'closeTime1' => 'Close Time',
	'proNumber1' => 'Pro Number',
	'expectedDeliveryDate' => 'Expected Delivery Date',
	'deliveredDate' => 'Delivered Date',
	'openTime2' => 'Open Time',
	'closeTime2' => 'Close Time',
	'carrierQuotenumber' => 'Carrier Quote number',
	'billableweight' => 'Billable weight',
	'costperpound' => 'Cost per pound',
	'proNumber2' => 'Pro Number',
	'shipmentvalue' => 'Shipment value',
	'shp_distance' => 'Distance',
	
	'invoiceStatus' => 'Invoice Status',
	'paymentStatus' => 'Payment Status',
	'billofLanding' => 'Bill of Landing',
	'carrierQuote' => 'Carrier Quote',
	'customerQuote' => 'Customer Quote',
	'customerInvoice' => 'Customer Invoice',
	
	'tab_title1' => 'Shipment',
	'tab_title2' => 'Stops',
	'tab_title3' => 'Products',
	'tab_title4' => 'Carrier Communication',
	'tab_title5' => 'Cost and Commission',
	'tab_title6' => 'Notes',
	'tab_title7' => 'Terminal and Contact',
	'tab_title8' => 'Track & Trace',
	
	'loadBoardStatistics' => 'Load Board Statistics', 
	'emailStatistics' => 'Email Statistics',
	'userAuditTrail' => 'User Audit Trail',
	'scanDocumentViewDocument' => 'Scan Document & View Document',	
	
	'productHeading' => 'Shipping Product',
	'stopHeading' => 'Shipping Stops',
	
	'carrierRespondedforSPOTQuote' => 'Carrier Responded for SPOT Quote',
	'tlPosting' => 'TL POSTING',
	'customerCost' => 'Customer Cost',
	'carrierCost' => 'Carrier Cost',
	
	'customerNotes' => 'Customer Notes',
	'carrierNotes' => 'Carrier Notes',
	'internalNotes' => 'Internal Notes',
	
	'originTerminal' => 'Origin Terminal',
	'destinationTerminal' => 'Destination Terminal',
	
	'trackTrace' => 'Track & Trace',
	
];