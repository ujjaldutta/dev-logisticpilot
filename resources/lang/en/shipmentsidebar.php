<?php

return[
	'sidebarHeading1' => 'Shipment',
    'sidebarHeading2' => 'shipment status',	
	'sidebarHeading3' => 'Customer',
	'sidebarHeading4' => 'Carrier',
	'sidebarHeading5' => 'Transportation Mode',
	'sidebarHeading6' => 'Equipment Type',
	'sidebarHeading7_fromstate' => 'State',
	'sidebarHeading8_tostate' => 'State',
	'sidebarHeading9' => 'Pricing',
	'sidebarHeading10' => 'EDI',
	'sidebarHeading11' => 'Date Section',
];