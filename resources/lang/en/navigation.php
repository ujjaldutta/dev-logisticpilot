<?php

return[
	'logout' => 'Logout',
	'welcome' => 'Welcome',
	'updateProfile' => 'Update Profile',
	'changePassword' => 'Change Password',
	'supportMessage' => 'Your Support Team',
	'call' => 'Call',
];