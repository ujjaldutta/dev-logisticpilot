<?php

return[
    'bookingNumberId' => 'Booking Number/ID',
	'equipmentType' => 'Equipment Type',
	'payTerms' => 'Pay Terms',
	'mode' => 'Mode',
	'purchaseOrder' => 'Purchase Order#',
	'pickupLocation_box' => 'Pickup Location',
	'deliveryLocation_box' => 'Delivery Location',
	'billToAddress_box' => 'Bill to Address',
	'pick_addressLine1' => 'Address line 1',
	'pick_addressLine2' => 'Address line 2',
	'pick_location' => 'Location',
	'pick_pickupDate' => 'Pickup Date',
	'pick_openTime' => 'Open Time',
	'pick_closeTime' => 'Close Time',
	'pick_contactPhone' => 'Contact Phone',
	'pick_contactName' => 'Contact Name',
	'pick_note' => 'Add your pickup location notes',
	
	'del_addressLine1' => 'Address line 1',
	'del_addressLine2' => 'Address line 2',
	'del_location' => 'Location',
	'del_delDate' => 'Exp. Delivery Date',
	'del_openTime' => 'Open Time',
	'del_closeTime' => 'Close Time',
	'del_contactPhone' => 'Contact Phone',
	'del_contactName' => 'Contact Name',
	'del_note' => 'Add your delivery location notes',
	
	'bill_addressLine1' => 'Address line 1',
	'bill_addressLine2' => 'Address line 2',
	'bill_location' => 'Location',	
	
	'creditLimit' => 'Credit Limit',
	'similarShipments' => 'Similar Shipments',
	'suggestedCarrier' => 'Suggested Carrier',
	'tlLoadBoard' => 'TL Load Board(s)',
	
	'stopHeading' => 'Add Stop',
	'searchName' => 'Name',
	'stop_addressLine1' => 'Address line 1',
	'stop_addressLine2' => 'Address line 2',
	'stop_city' => 'City',
	'stop_state' => 'State',
	'stop_postal' => 'Postal',
	'stop_type' => 'Stop Type',
	
	'productHeading' => 'Shipping Product',
	
	
	'accessorialHeading' => 'Accessorial',
	'shipmentDetailsHeading' => 'Additional Shipment Details',
	'row_heading1' => 'Heading 1',
	'row_heading2' => 'Heading 2',
	'row_heading3' => 'Heading 3',
	'row_heading4' => 'Heading 4',
	'row_heading5' => 'Carrier Safety',
	'row_heading6' => 'Carrier Safety',
	'row_heading7' => 'Carrier Safety',
	'row_heading8' => 'Carrier Safety',
	
	
	'shipment_notesHeading' => 'Shipment Notes',
	'customerNote' => 'Customer Notes',
	'carrierNote' => 'Carrier Notes',
	'internalNote' => 'Internal Notes',
];