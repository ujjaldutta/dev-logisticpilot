var Calendar = function (data) {
    //function to initiate Full CAlendar
    var runCalendar = function () {
        var $modal = $('#event-management');
		
		// modal manager spinner fix for bs3
		/* $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
		'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
			'<div class="progress progress-striped active">' +
				'<div class="progress-bar" style="width: 100%;"></div>' +
			'</div>' +
		'</div>';
		*/
        
		/* $('#event-categories div.event-category').each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 50 //  original position after the drag
            });
        }); */
        /* initialize the calendar
				 -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var calendar = $('#calendar').fullCalendar({
			theme: false,
            buttonIcons: {
                prev: 'left-single-arrow',
                next: 'right-single-arrow'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
			events: {
				url: '/get-shifts',
				type: 'POST',
				data: function(){
					return {
						sorting: $('#shifts_sorting').serialize()
					};
				},
			}, 
			loading: function(isLoading, view){
				if(isLoading)
					$('.manager-overlay').removeClass('hidden');
				else
					$('.manager-overlay').addClass('hidden');
			},
			/*events: [{
                title: 'Meeting with Boss',
                start: new Date(y, m, 1),
                className: 'label-default'
            }, {
                title: 'Bootstrap Seminar',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2),
                className: 'label-teal'
            }, {
                title: 'Lunch with Nicole',
                start: new Date(y, m, d - 3, 12, 0),
                className: 'label-green',
                allDay: false
            }],
			*/
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            /* drop: function (date, allDay) { // this function is called when something is dropped
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                var $categoryClass = $(this).attr('data-class');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                if ($categoryClass)
                    copiedEventObject['className'] = [$categoryClass];
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            }, */
            selectable: true,
            selectHelper: true,
            select: function (start, end, jsEvent, view) {
				var check = start.format('YYYY-MM-DD');
				var today = $.fullCalendar.moment(new Date()).format('YYYY-MM-DD');
				//console.log(check);
				//console.log(today);
				if(check >= today){
					var allDay = start.hasTime() && end.hasTime();
					var start = start.format('YYYY-M-DD HH:mm:ss');
					var end = end.format('YYYY-M-DD HH:mm:ss');
					var params = escape(start + '/' + start + '/' + allDay) ;	
									
					//var params = escape(start + '/' + end + '/' + allDay) ;
					
					//console.log(params);
					//console.log(start.format('YYYY-M-DD HH:mm:ss'));
					//console.log(end.format('YYYY-M-DD HH:mm:ss'));
					// create the backdrop and wait for next modal to be triggered
					
					/* $('body').modalmanager('loading');
					setTimeout(function(){
						 $modal.load('/admin/shifts/add/' + params, '', function(){
						  $modal.modal();
						});
					}, 1000);
					
					*/

					$modal.modal({
						backdrop: 'static',
						keyboard: false,
						remote: '/admin/shifts/add/' + params,
					});
					
					
					calendar.fullCalendar('unselect');
				}
				
            },
            eventClick: function (calEvent, jsEvent, view) {
				var eventId = calEvent._id;
				var parentId = calEvent.parent_id;
				//console.log(start.format('YYYY-M-DD HH:mm:ss'));
				//console.log(end.format('YYYY-M-DD HH:mm:ss'));
				// create the backdrop and wait for next modal to be triggered
				if(calEvent.recurrence){
					BootstrapDialog.show({
						message: 'Do you want to edit the whole set of repeated shifts?',
						closable: false,
						buttons: [{
							label: 'Edit Occurrence',
							cssClass: 'btn-primary',
							action: function(dialogItself){
								$modal.modal({
									backdrop: 'static',
									keyboard: false,
									remote: '/admin/shifts/edit/' + eventId,
								});
								dialogItself.close();
							}
						}, {
							//icon: 'glyphicon glyphicon-ban-circle',
							label: 'Edit Series',
							cssClass: 'btn-warning',
							action: function(dialogItself){
								$modal.modal({
									backdrop: 'static',
									keyboard: false,
									remote: parentId ? '/admin/shifts/edit/' + parentId + '/all' : 
										'/admin/shifts/edit/' + eventId + '/all',
								});
								dialogItself.close();
							}
						}, {
							label: 'Close',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				}else{
					$modal.modal({
						backdrop: 'static',
						keyboard: false,
						remote: '/admin/shifts/edit/' + eventId,
					});
				}
				
				
				/* $('body').modalmanager('loading');
				setTimeout(function(){
					 $modal.load('/admin/shifts/edit/' + eventId, '', function(){
					  $modal.modal();
					});
				}, 1000);
				
				*/
				
				
               /* var form = $("<form></form>");
                form.append("<label>Change event name</label>");
                form.append("<div class='input-group'><input class='form-control' type=text value='" + calEvent.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success'><i class='fa fa-check'></i> Save</button></span></div>");
                $modal.modal({
                    backdrop: 'static'
                });
                $modal.find('.remove-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.remove-event').unbind('click').click(function () {
                    calendar.fullCalendar('removeEvents', function (ev) {
                        return (ev._id == calEvent._id);
                    });
                    $modal.modal('hide');
                });
                $modal.find('form').on('submit', function () {
                    calEvent.title = form.find("input[type=text]").val();
                    calendar.fullCalendar('updateEvent', calEvent);
                    $modal.modal('hide');
                    return false;
                }); */
            },
			eventMouseover: function(event, jsEvent, view){
				//console.log(event.vacancies);
				var content = '<div class="row">' +
					'<div class="col-sm-12"><strong>' + 
						event.start.format('MMMM Do YYYY, h:mm:ss a') + ' ' + 
						event.title +
					 '</strong></div>' +
				'</div>';
				if(event.vacancies){
					for(item in event.vacancies){
						content += '<div class="row">' +
							'<div class="col-xs-6">' + 
								event.vacancies[item]['name'] +
							'</div>'+
							'<div class="col-xs-1"><strong>' + 
								eval(event.vacancies[item]['qty'] - event.vacancies[item]['registered']) +
							'</strong></div>'+
							'<div class="col-xs-2">' + 
								'Available' +
							'</div>'+
						'</div>';
					}
				}
				//show pop-over for only previously added event 
				if($(this).hasClass('edit-event')){
					//console.log(content);
					$(this).popover({
						container: "body",
						title: "Availability",
						html: true,
						content: content,
						trigger: 'hover',
						placement: 'auto',
					});
					$(this).popover('show');
				}
			}
        });
    };
    return {
        init: function () {
            runCalendar();
        }
    };
}();