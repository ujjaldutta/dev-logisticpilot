var app = app || {};
(function($){
	'use strict';
	app.AdminDashboardNew = {
		MemberView:{
			activateHandlers: function(){
				var _super = this;							
				try{					
					$('body').on('hidden.bs.modal', '.modal', function () {
							$('#event-management, #pending-shift-modal, #confirmed-shift-modal, #user-management').find('.modal-content').html(
								'<div class="manager-overlay"><div class="loadtext"><img src="' + app.Config.getScriptBaseUrl() + 'images/preloader_white.gif' + '" alt="Loading..."></div></div>'
							);
							$(this).removeData('bs.modal');
					});	
					//Start pending shift data table function		
					var oTable = _super.oTable = $('#user-management').DataTable ( {
						"ajax": app.Config.getScriptBaseUrl() + app.Config.adminPrefix + "/registrations/list",
						 "deferRender": true,
						  "order": [[ 4, "desc" ]],
						"columns": [
							{ "data": "name" },
							{ "data": "email" },
							{ "data": "plan" },
							{ "data": "status"},
							{ "data": "created_on", className: 'hidden-xs',},
							{ "data": "actions" }
						],
						 "columnDefs": [ 
							{
								"targets": -1,
								"data": null,
								"defaultContent": "<span class=\"links record-view\" title=\"View\"><i class=\"fa fa-eye\"></i></span>"
									+ "&nbsp;"
									//+ "<span class=\"links record-delete\" title=\"Delete\"><i class=\"fa fa-times\"></i></span>"
									//+ "&nbsp;",
							},
						]
					} );
					$('#user-management_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#user-management_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#user-management_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#user-management_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
					$('#user-management_wrapper').on('click', '.activate', function(e){ 
						var data = oTable.row( $(this).parents('tr') ).data();				
						var status = data.status;
						var id = data.id;
						var _token = data.token;
						var _super = $(this);
						alertify.confirm('Are you sure you want to active this user?', function(e){
						if(e){
							$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/registrations/set-action',
									{ _token: _token, id: id, status: status}
								).done(function(result){
									if(result.success)
									{							
										alertify.alert('The user has been successfully activated.');
										//reloading data table
										oTable.ajax.reload();
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
								});
							}
						});								
					});		
					// manage view
					$('#user-management_wrapper').on('click', '.record-view', function(e){
						var data = oTable.row( $(this).parents('tr') ).data();
						var id = data.id;
						var _token = data.token;
						var _super = $(this);						
						$('#user-details-modal').modal({
							backdrop: 'static', keyboard: false
						}).load(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/registrations/view/' + id);					});			
					// end
				}catch(error){
					alertify.alert(error.message);
				}				
			},			
			init: function(){				
				//activating all event listeners
				this.activateHandlers();
			}
		},
	};
})(jQuery);