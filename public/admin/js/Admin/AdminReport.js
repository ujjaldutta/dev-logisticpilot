var app = app || {};
(function($){
	'use strict';
	app.AdminReport = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			
			$.validator.addMethod("valid_start_end", function(value, element) {
				if(!this.optional(element)){
					var end = moment(value);
					var start = moment($('#start-date').val());
					//console.log(expiry > currentDate);
					return end >= start;
				}else
					return true;		
			},	 	"End date cannot be past than start date");
			
			
		},
		
		//report section controller for scheduled report 
		Schedule: {
			activateHandlers: function(){
				var _super = this;
				
				$('.date-picker').datepicker({autoclose: true});
				
				$('#make-print').on('click', function(event){
					//Print ele4 with custom options
					$("#report_area").print({
						//Use Global styles
						globalStyles : true,

						//Print in a hidden iframe
						iframe : true,

						//Don't print this
						noPrintSelector : ".actions",
					});
				});
				
				$('#make-pdf').on('click', function(event){
					var roleId = $(this).data('role-id') || 0;
					var startDate = $(this).data('start-date') || 0;
					var endDate = $(this).data('end-date') || 0;
					
					var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/report-schedule-pdf/' + roleId + '/' + startDate + '/' + endDate, '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );
				});
			},
			
					
			runValidation: function(){
				$('#report-generate').validate();
			},
			
			init: function(){
				app.AdminReport.runDefaultValidation();
				this.activateHandlers();
				this.runValidation();
			},
		},
		
		Assignment: {
			getShifts: function(data, target, eventInfo){
				//showing ajax loading
				$('div[data-loader=' + target + ']').removeClass('hidden');
				
				$.post(
					app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-get-shifts-by-dates',
					data
				).done(function(result){
					if(result['shifts']){
						var $html = '<option value="">Please select</option>';
						if(result['count'] !== undefined && result['count'])
							if(eventInfo !== undefined && 'all' == eventInfo)					 
								$html += '<option value="all" selected="selected">All</option>';
							else
								$html += '<option value="all">All</option>';
						for(var key in result['shifts']){
								if(eventInfo !== undefined && result['shifts'][key]['id'] == eventInfo)
									$html += '<option value="' + result['shifts'][key]['id'] +'" selected="selected">' + result['shifts'][key]['name'] + '</option>';
								else
									$html += '<option value="' + result['shifts'][key]['id'] +'">' + result['shifts'][key]['name'] + '</option>';
						}
						
						$('.' + target).html($html);
						//hiding ajax loading
						$('div[data-loader=' + target + ']').addClass('hidden');
					}else
						alertify.alert('Error in response');
					
				}).fail(function(jqXHR, textStatus, errorThrown){
					alert('Error: ' + textStatus + ' -- ' + errorThrown);
				});
			},
			activateHandlers: function(){
				var _super = this;
				$('.date-picker').datepicker({autoclose: true});
				
				$('#start-date, #end-date').on('blur change', function(event, eventInfo){
					if($('#start-date').val().trim() && 
						$('#end-date').val().trim() != ''
					){
						var startDate = $('#start-date').val().trim();
						var endDate = $('#end-date').val().trim();
						
						startDate = moment(startDate, 'YYYY-MM-DD');
						endDate = moment(endDate, 'YYYY-MM-DD');
						
						if(startDate.format('YYYY-MM-DD') != "Invalid date" &&
							endDate.format('YYYY-MM-DD') != "Invalid date"
						){
							if(endDate >= startDate)
								_super.getShifts(
									{ startDate: startDate.format('YYYY-MM-DD'), endDate: endDate.format('YYYY-MM-DD')},
									'shift',
									eventInfo
								);
						}else{
							var $html = '<option value="">Please select</option>';
								//$html += '<option value="all">All</option>';
								$('#shift').html($html);	
						}
					}
				});
				
				//triggering change event to fetch the user list for this role 				
				var selectedId = $('#shift').data('selected-id') || 0;
				$('#end-date').trigger('blur', selectedId);
				
				$('#make-print').on('click', function(event){
					//Print ele4 with custom options
					$("#report_area").print({
						//Use Global styles
						globalStyles : true,

						//Print in a hidden iframe
						iframe : true,

						//Don't print this
						noPrintSelector : ".actions",
					});
				});
				
				$('#make-pdf').on('click', function(event){
					var shiftId = $(this).data('shift-id') || 0;
					var startDate = $(this).data('start-date') || 0;
					var endDate = $(this).data('end-date') || 0;
					
					var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/assignment-pdf/' + shiftId + '/' + startDate + '/' + endDate, '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );					
				});
				
			},
			
			runValidation: function(){
				$('#assignment-generate').validate();
			},
			
			init: function(){
				app.AdminReport.runDefaultValidation();
				this.activateHandlers();
				this.runValidation();
			},
		},
		
		UserHistory: {
			activateHandlers: function(){
				var _super = this;
				
				$('.date-picker').datepicker({autoclose: true});
				
				$('#make-print').on('click', function(event){
					//Print ele4 with custom options
					$("#report_area").print({
						//Use Global styles
						globalStyles : true,

						//Print in a hidden iframe
						iframe : true,

						//Don't print this
						noPrintSelector : ".actions",
					});
				});
				
					$('#make-pdf').on('click', function(event){
				
					var userId = $(this).data('user-id') || 0;
					var startDate = $(this).data('start-date') || 0;
					var endDate = $(this).data('end-date') || 0;
					
					var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/report-user-history-pdf/' + userId + '/' + startDate + '/' + endDate, '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );
					});
			},
			
			runValidation: function(){
				$('#report-user-history-generate').validate();
			},
			
			init: function(){
				app.AdminReport.runDefaultValidation();
				this.activateHandlers();
				this.runValidation();
			},
		},
		
		ShiftHistory: {
			activateHandlers: function(){
				var _super = this;
				
				$('.date-picker').datepicker({autoclose: true});
				
				$('#make-print').on('click', function(event){
					//Print ele4 with custom options
					$("#report_area").print({
						//Use Global styles
						globalStyles : true,

						//Print in a hidden iframe
						iframe : true,

						//Don't print this
						noPrintSelector : ".actions",
					});
				});
				
				$('#make-pdf').on('click', function(event){
				
					var roleId = $(this).data('role-id') || 0;
					var startDate = $(this).data('start-date') || 0;
					var endDate = $(this).data('end-date') || 0;
					
					var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/report-shift-history-pdf/' + roleId + '/' + startDate + '/' + endDate, '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );
					});
				
			},

			runValidation: function(){
				$('#report-shift-history-generate').validate();
			},
				
			init: function(){
				app.AdminReport.runDefaultValidation();
				this.activateHandlers();
				this.runValidation();
			},
				
		},
		
	}
})(jQuery);