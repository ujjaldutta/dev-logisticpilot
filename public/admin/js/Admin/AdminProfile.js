var app = app || {};
(function($){
	'use strict';
	app.AdminProfile = {
		getStates: function(data, target){
			//showing ajax loading
			$('div[data-loader=' + target + ']').removeClass('hidden');
			
			$.post(
				app.Admin.globalScriptBaseUrl + 'states/ajaxGetStates',
				data
			).done(function(result){
				$('.' + target).html(result);
				
				//hiding ajax loading
				$('div[data-loader=' + target + ']').addClass('hidden');
			}).fail(function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' -- ' + errorThrown);
			});
		},
		activateHandlers: function(){
			var _super = this;
			$('.country').on('change', function(){
				var data = {country_id: $('#' + $(this).attr('id') + ' option:selected').val()};
				console.log($('#' + $(this).attr('id')));
				
				_super.getStates(data, 'states');
			});
			
			$('.change_state_label').on('change', function(){
					var chnageLabel = $(this).val();
					
					if(chnageLabel==1){
						$('.new_label').html('State <span aria-required="true" class="symbol required"></span>');
						$('.new_label2').html('Zip	<span aria-required="true" class="symbol required"></span>');
						$('#zip_postal_code').removeClass('ca_zip_validation').addClass('us_zip_validation');						
					}
					
					if(chnageLabel==2){
						$('.new_label').html('Province <span aria-required="true" class="symbol required"></span>');	
						$('.new_label2').html('Postal Code <span aria-required="true" class="symbol required"></span>');
						$('#zip_postal_code').removeClass('us_zip_validation').addClass('ca_zip_validation');			
					}	
				});
			
				$('#dob').datepicker({
					autoclose: true	
				});	
					
				var changeFlag = false;
				
				$('input').on('change', function(event){
						changeFlag = true;
				});			
			
				/*$('.skill-li-tab').click(function(){
					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/skill/';
				});*/	
				
				/*$('.certification-li-tab').click(function(){
					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/certification/';
				});*/			
				
				/*$('.recognition-li-tab').click(function(){
					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/recognition/';
					
				});*/
				
				/*$('.time-off-li-tab').click(function(){
					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/time-off/';
				});*/
				
				$('#effective_date').datepicker({ 
					//startDate: new Date(), 
					autoclose: true, 
				}).on('changeDate changeYear changeMonth', function(ev) {
						var endDate = new Date(ev.date);
        				endDate.setDate(endDate.getDate() + 1);
						$('#expiry_date').datepicker('setStartDate', endDate);
						$('#expiry_date').val('');
				});
				
				$('#expiry_date').datepicker({ 
					//startDate: new Date(), 
					autoclose: true, 
				});
				
				$('input[id ^= effective-date-]').datepicker({
					autoclose: true,
				}).on('changeDate changeYear changeMonth', function(ev) {
						var currentId = $(ev.currentTarget).attr('id').split('-');
						currentId = currentId[currentId.length-1];
						
						var endDate = new Date(ev.date);
        				endDate.setDate(endDate.getDate() + 1);
						
						$('#expiry-date-' + currentId).datepicker('setStartDate', endDate);
						$('#expiry-date-' + currentId).val('');
				});
				
				$('[id ^= expiry-date-]').datepicker({
					autoclose: true,
				});
				
				$(document).on('ready', function(){
					var form = $('.form-admin-user-certification-update');
					form.valid();
					
					var expiredCertificationCounter = 0;
					$('.expired-certification').each(function(i){
						expiredCertificationCounter = $(this).hasClass('has-error') ? expiredCertificationCounter + 1 : expiredCertificationCounter;
					});
					
					expiredCertificationCounter ? $('span#invalid-certification-counter').text(expiredCertificationCounter).parent('p.red-text').removeClass('hidden') : void(0);
					
					//console.log(expiredCertificationCounter);
				});
				
				//handles to auto select row checkbox when any of its fields are changed
				$('.effective_date_rows').on('change click', function(event){
					var $id = $(this).attr('id') || 0;
					$id = $id.split('-');
					$id = $id[$id.length -1];
					$('[id=' + $id + ']').iCheck('check');
					
					//checking all checkbox when all check boxes are checked
					if($('input.user-certification').length == $('input.user-certification:checked').length)
						$('#user-certification-select-all').iCheck('check');
				});
								
				//check all
				$('#user-certification-select-all').on('ifChecked', function(event){
					$('input.user-certification').iCheck('check');
				});
				
				//uncheck all
				$('#user-certification-select-all').on('ifUnchecked', function(event){
						$('input.user-certification').iCheck('uncheck');
				});
				
				//delete all handler
				$('#certification-delete-all').on('click', function(event){
					
					if($('input.user-certification:checked').length){
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure you wish to delete selected certifications ?', function (e) {
							if(e){
								
								//getting all selected ids
								var $ids = '';
								$('input.user-certification:checked').each(function(i){
									$ids += $(this).val() + ',';
								});
								
								if($ids.length > 1)
									$ids = $ids.substring(0, $ids.length-1);
								
								$('#certification-ids').val($ids);
								$('#user-certification-multi-delete').submit();
							}
						});
					}else
						alertify.alert('Please select at least one item');
				});
				
				
				$('#start_date').datepicker({ 
					startDate: new Date(),
					autoclose: true
				
				}).on('changeDate', function(ev) {
						var endDate = new Date(ev.date);
        				//endDate.setDate(endDate.getDate() + 1);
        				endDate.setDate(endDate.getDate());
						$('#end_date').datepicker('setStartDate', endDate);
						$('#end_date').val('');
				});
				
				$('#end_date').datepicker({ 
					//startDate: new Date(), 
					autoclose: true, 
				});
				
				$('.date-picker').datepicker({ 
					//startDate: new Date(), 
					autoclose: true, 
				});

				//check all
				$('#user-recognition-select-all').on('ifChecked', function(event){
					$('input.user-recognition').iCheck('check');
				});
				
				//uncheck all
				$('#user-recognition-select-all').on('ifUnchecked', function(event){
						$('input.user-recognition').iCheck('uncheck');
				});
			
				//delete all handler
				$('#recognition-delete-all').on('click', function(event){
					
					if($('input.user-recognition:checked').length){
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure you wish to delete all recognitions?', function (e) {
							if(e){
								
								//getting all selected ids
								var $ids = '';
								$('input.user-recognition:checked').each(function(i){
									$ids += $(this).val() + ',';
								});
								
								if($ids.length > 1)
									$ids = $ids.substring(0, $ids.length-1);
								
								$('#rec-ids').val($ids);
								$('#user-recognition-multi-delete').submit();
							}
						});
					}else
						alertify.alert('Please select at least one item');
				});				
				
				$('.certification-date-sync a').click(function(){
					var syncValue = $(this).attr('href');
					var splitValue = syncValue.split("/");
					var countValue = splitValue.length;
					var orderValue = splitValue[countValue-2];

					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/certification/'+orderValue;
				});
				
				$('.date-sync a').click(function(){
					var syncValue = $(this).attr('href');
					var splitValue = syncValue.split("/");
					var countValue = splitValue.length;
					var orderValue = splitValue[countValue-2];

					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/recognition/'+orderValue;
				});
				
				$('.timeoff-date-sync a').click(function(){
					var syncValue = $(this).attr('href');
					var splitValue = syncValue.split("/");
					var countValue = splitValue.length;
					var orderValue = splitValue[countValue-2];
					window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/profile/time-off/'+orderValue;
				});
				
				//check all
				$('#user-timeoff-select-all').on('ifChecked', function(event){
					$('input.user-timeoff').iCheck('check');
				});
				
				//uncheck all
				$('#user-timeoff-select-all').on('ifUnchecked', function(event){
						$('input.user-timeoff').iCheck('uncheck');
				});
				
				//delete all handler
				$('#timeoff-delete-all').on('click', function(event){
					
					if($('input.user-timeoff:checked').length){
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure you wish to delete all time-off periods?', function (e) {
							if(e){
								
								//getting all selected ids
								var $ids = '';
								$('input.user-timeoff:checked').each(function(i){
									$ids += $(this).val() + ',';
								});
								
								if($ids.length > 1)
									$ids = $ids.substring(0, $ids.length-1);
								
								$('#timeoff-ids').val($ids);
								$('#user-timeoff-multi-delete').submit();
							}
						});
					}else
						alertify.alert('Please select at least one item');
				});
				
				
				
		$(document).on('ready', function(){
			
					/*
						* triggering blur events and already selected
						* contact type when page refreshes 
						* for the already entered contact numbers
					*/
				
				$('.phone').trigger('blur');
				$('#default_cno_type').val($('input.preferred:checked').data('default-cno-type') || '');					
				});
				
				$('.phone').on('blur', function(e){
				var regExp = /^[2-9]\d{2}-\d{3}-\d{4}$/;
				//console.log(regExp.test($(this).val()));
				var validated = regExp.test($(this).val());
				var targetRadioId = $(this).data('default-radio-id') || null;
				
				try{
					if(validated){
						$('#' + targetRadioId).val($(this).val());
						$('#' + targetRadioId).iCheck('enable');
					}else{
						$('#' + targetRadioId).val('');
						$('#' + targetRadioId).iCheck('uncheck');
						$('#' + targetRadioId).iCheck('disable');
					}
				}catch(error){
					alert(error.message);
				}
				
				/* 
				* again checking at least one cno is selected 
				* otherwise default home cno is being selected
				*/
				
				if(!$('.preferred:checked').length){
					$('#default-home').iCheck('enable');
					$('#default-home').iCheck('check');
				}
			});
			
			$('.preferred').on('ifChecked', function(){
				var value = $(this).data('default-cno-type') || '';
				$('#default_cno_type').val(value);
			});
					 				
		
		},
		
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			//adding phone number rule
			$.validator.addMethod("phone_validation", function(value, element) {
				return this.optional(element) || /^[2-9]\d{2}-\d{3}-\d{4}$/.test(value);
			}, "Please enter valid number (###-###-####).");
			
			//adding zip validation rule
			$.validator.addMethod("zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Postal code / Zip should be like A1A 1A1");
			
			$.validator.addMethod("alphabetic", function(value, element) {
        		return this.optional(element) || /^[a-z]+$/i.test(value); 
      	}, "This field may only contains alphabetic characters");
			
			//adding zip validation rule for CANADA & US (both)
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Please enter valid postal code (no spaces)");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Please enter valid ZIP code (no spaces)");
			
			$.validator.addMethod("alpha_space", function(value, element) {
       		return this.optional(element) || /^[a-zA-Z ][a-zA-Z\\s ]+$/.test(value); 
      	}, "This field may only contains alphabetic characters and spaces");
			
			//adding password nomenclature rule
			$.validator.addMethod("password_strength", function(value, element) {
				return this.optional(element) || /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/.test(value);
			}, "Password must follow requirements.");
			
			$.validator.addMethod("valid_certification_expiry", function(value, element) {
				if(!this.optional(element)){
					var expiry = moment(value);
					var currentDate = moment();
					//console.log(expiry > currentDate);
					return expiry > currentDate;
				}else
					return true;		
			},	 	"Certification expired");
			
			
		},
		
		runProfileValidator : function () {
			var form = $('.form-admin-profile');
			var errorHandler3 = $('.errorHandler', form);
			form.validate({
				rules: {
					"fname": {
						required: true,
						alphabetic: true
					},
					"lname": {
						required: true,
						alphabetic: true
					},
					"gender_id": {
						required: true
					},
					"profile[home_phone]": {
						required: true,
						phone_validation: true,
						minlength: 10,
						maxlength: 12
					},
					"profile[work_phone]": {
						phone_validation: true			
					},
					"profile[mobile]": {
						phone_validation: true					
					},
					"profile[pager]": {
						phone_validation: true					
					},
					"dob":{
						required: true	
					},
					"profile[emergency_contact_name]": {
						required: true,
						alpha_space: true					
					},
					"profile[emergency_relation_id]": {
						required: true					
					},
					"profile[role_id]": {
						required: true	
					},
					"profile[emergency_phone_number]": {
						required: true,
						phone_validation: true					
					},
					"profile[occupation]": {
						required: true					
					},
					"profile[address]": {
						required: true,
					},
					"profile[country_id]": {
						required: true
					},
					"profile[state_id]": {
						required: true
					},
					"profile[zip]": {
						required: true,
						zip_validation: true
					},
				},
				submitHandler: function (form) {
					errorHandler3.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler3.show();
				}
			});
		},
			
			
		runSkillValidator : function () {
			var form = $('.form-admin-profile-skill');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				
				rules: {
					"skill_id[]": {
						required: true
					},
				},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});
		},
		
		runCertificationValidator : function() {
			var form = $('.form-admin-user-certification');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
						"name": {
							required: true							
						},
						"effective_date": {
							required: true						
						},
						"expiry_date": {
							required: true						
						},					
					},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}			
			});
			
			var form = $('.form-admin-user-certification-update');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
						"effective_date_row[]": {
							required: true						
						},
						"expiry_date_row[]": {
							required: true						
						},					
					},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}			
			});	
		},
		
		runRecognitionValidator : function () {
			var form = $('.form-admin-user-recognition');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
					"name": {
						required: true
					},
					"completion_date": {
						required: true				
					},
				},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});
		},
		
		runTimeoffValidator : function () {
			var form = $('.form-admin-user-timeoff');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
					"description": {
						required: true
					},
					"start_date": {
						required: true				
					},
					"end_date": {
						required: true	
					},
				},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});
		},
		
		runPasswordValidator : function () {
			var form = $('.form-admin-profile-email');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
					"email": {
						required: true,
						email: true
					},
					"conf_email": {
						required: true,
						email: true,
						equalTo: '#email'
					},
				},
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});
		},
		
		runLoginEmailValidator : function () {
			var form = $('.form-admin-profile-password');
			var errorHandler = $('.errorHandler', form);
			form.validate({
				rules: {
					"current_password": {
						minlength: 2,
						required: true
					},
					"password": {
						required: true,
						password_strength: true,
					},
					"conf_password": {
						required: true,
						equalTo: '#UserPassword'
					}
				},
				messages: {
					"password": 'Please enter correct password'
				},
				
				submitHandler: function (form) {
					errorHandler.hide();
					form.submit();
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					errorHandler.show();
				}
			});
		},
		
		/*highLightCurrentTab: function(tabName){
			$('#myTab4 a[rel=' + tabName + ']').tab('show')
			//$('li[rel=' + tabName + ']').click();
		},
		
		highLightleftMenu: function(){
			$('.main-navigation-menu li[rel=leftmenu-root-dashboard]').addClass('open active');
			//$('li[rel=' + tabName + ']').click();
		},*/
		
		highLightCurrentTab: function(tabName){
			tabName = (tabName === undefined || !tabName.length) ? 'profile' : tabName;
			//console.log(tabName);
			$('#myTab4').find('li').removeClass('active');
			$('.tab-pane').removeClass('active');
			$('[rel=' + tabName + ']').addClass('active');
		},
		
		highLightleftMenu: function(){
			$('.main-navigation-menu li[rel=leftmenu-root-dashboard]').addClass('open active');
			//$('li[rel=' + tabName + ']').click();
		},
		
				
		init: function(){
			//activating all event listeners
			this.activateHandlers();
			//executing default form validation pre-requisite
			this.runDefaultValidation();
			//attaching profile form validation
			this.runProfileValidator();
			//attaching skills validation
			this.runSkillValidator();
			//attaching certificatons validation
			this.runCertificationValidator();
			//attaching recognition part validation
			this.runRecognitionValidator();
			//attaching time-off periods validation
			this.runTimeoffValidator();
			//attaching password update form validation
			this.runPasswordValidator();
			//attaching login email validation
			this.runLoginEmailValidator();
			//highlight current section on the left menu
			//this.highLightleftMenu();
		}
		
	};
	//app.AdminProfile.showLocation();
	
	//initializing app
	app.AdminProfile.init();
	
})(jQuery);