var app = app || {};
(function($){
	'use strict';
	app.AdminSettings = {
		runDefaultValidation : function () {
			$.validator.addMethod('valid_phone', function(value, element){
				return this.optional(element) || /^[2-9]\d{2}-\d{3}-\d{4}$/.test(value);
			}, 'Please enter valid number (###-###-####).');
		
			$.validator.addMethod('valid_zip', function(value, element){
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 'Please write a valid USA / Canada zip code');
			
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Please enter valid postal code (no spaces)");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Please enter valid ZIP code (no spaces)");
			
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		Address : {
			getStates: function(data, target){
				//showing ajax loading
				$('div[data-loader=' + target + ']').removeClass('hidden');
				
				$.post(
					app.Admin.globalScriptBaseUrl + 'states/ajaxGetStates',
					data
				).done(function(result){
					$('.' + target).html(result);
					
					//hiding ajax loading
					$('div[data-loader=' + target + ']').addClass('hidden');
				}).fail(function(jqXHR, textStatus, errorThrown){
					alert('Error: ' + textStatus + ' -- ' + errorThrown);
				});
			},
			
			activateHandlers: function(){
				var _super = this;
				$('.country').on('change', function(){
					var data = {country_id: $('#' + $(this).attr('id') + ' option:selected').val()};
					_super.getStates(data, 'states');
				});
				
				$('.change_state_label').on('change', function(){
					var chnageLabel = $(this).val();

					if(chnageLabel==1){
						$('.new_label').html('State <span aria-required="true" class="symbol required"></span>');
						$('.new_label2').html('Zip	<span aria-required="true" class="symbol required"></span>');
						$('#zip_postal_code').removeClass('ca_zip_validation').addClass('us_zip_validation');						
					}
					
					if(chnageLabel==2){
						$('.new_label').html('Province <span aria-required="true" class="symbol required"></span>');	
						$('.new_label2').html('Postal Code <span aria-required="true" class="symbol required"></span>');
						$('#zip_postal_code').removeClass('us_zip_validation').addClass('ca_zip_validation');			
					}	
				});
			
				
				$(document).on('ready', function(){
					/*
					* handler to load previously selected country and state when 
					* server side validation fails or page is refreshed. Applicable only
					* on edit mode
					*/
					if(!$('.country').hasClass('edit')){
						var data = {country_id: $('.country option:selected').val()};
						_super.getStates(data, 'states');
					}
				});
			},
			
			runAddressValidator : function () {
				var form = $('#settings-address');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"organization_name": {
							required: true
						},
						"primary_contact_name": {
							required: true
						},
						"position": {
							required: true
						},
						"address": {
							required: true
						},
						"admin_email": {
							required: true,
							email: true
						},
						"admin_email_confirmation": {
							required: true,
							email: true,
							equalTo: '#admin_email'
						},
						"state_id": {
							required: true,
						},
						"city": {
							required: true,
						},
						"primary_phone": {
							required: true,
							valid_phone: true
						},
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//running default validation rules
				app.AdminSettings.runDefaultValidation();
				//running address validation
				this.runAddressValidator();
			}
		},
		
		Branding: {
			activateHandlers: function(){
				var _super = this;
			
				
				$(document).on('ready', function(){
					
				});
				
			},
			
			runBrandingValidator : function () {
				var form = $('#settings-branding');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"account_name": {
							required: true
						},
						/* "logo": {
							required: true
						} */
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//running default validation rules
				app.AdminSettings.runDefaultValidation();
				//running branding validation
				this.runBrandingValidator();
			}
		},
		
		//js codes for Terms and Condition pages. 
		TermsAndConditions: {
			activateHandlers: function(){
				var _super = this;
			
				
				$(document).on('ready', function(){
					
				});
				
			},
			
			runTncValidator : function () {
				var form = $('#settings-terms');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"tnc": {
							required: true
						},
						 "other_info": {
							required: true
						} 
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//running default validation rules
				app.AdminSettings.runDefaultValidation();
				//running branding validation
				this.runTncValidator();
			}
		},
		
		Localization: {
			activateHandlers: function(){
				var _super = this;
			
				
				$(document).on('ready', function(){
					
				});
				
			},
			
			runLocalizationValidator : function () {
				var form = $('#settings-timezone');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"timezone_id": {
							required: true
						},
						 "time_format_12hrs": {
							required: true
						} 
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//running default validation rules
				app.AdminSettings.runDefaultValidation();
				//running branding validation
				this.runLocalizationValidator();
			}
		},
		
		Languages:{
			activateHandlers: function(){
				var _super = this;
			
				
				$(document).on('ready', function(){
					
				});
				
			},
			
			runLanguageValidator : function () {
				var form = $('#terms-list');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//running default validation rules
				app.AdminSettings.runDefaultValidation();
				//running branding validation
				this.runLanguageValidator();
			}
		},
		
		Approvals:{
			activateHandlers: function(){
				var _super = this;
				try{
					if($('.bt-switch').length){
						//$('.bt-switch').bootstrapSwitch('setSizeClass', '');
					}
				}catch(error){
					alert(error.message);
				}
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
			}
		}
	};
	
})(jQuery);
