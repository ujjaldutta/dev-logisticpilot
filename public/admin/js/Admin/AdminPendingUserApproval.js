var app = app || {};
(function($){
	'use strict';
	app.AdminPendingUserApproval = {	
	 activateHandlers: function(){
	 	
				//for modal
				$('body').on('hidden.bs.modal', '.modal', function () {
						$('#user-management').find('.modal-content').html(
							'<div class="manager-overlay"><div class="loadtext"><img src="' + app.Config.getScriptBaseUrl() + 'images/preloader_white.gif' + '" alt="Loading..."></div></div>'
						);
						$(this).removeData('bs.modal');
				});	 	
	 				
				//Start pending users data table
				var oTable = $('#pending-user-approval-dedicated').dataTable({
						"aoColumns": [
							null,
							null,
							null,
							null,
							null,
							{ "bSortable": false },
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[0, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#pending-user-approval-dedicated_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#pending-user-approval-dedicated_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#pending-user-approval-dedicated_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#pending-user-approval-dedicated_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
					$('#pending-user-approval-dedicated_wrapper').on('click', '.userApprove', function(e){
					//declared variables					
					//var userActive = $(this).data('user-active') || 0;
					var userId = $(this).data('user-id') || 0;
					var userName = $(this).data('user-name') || 0;
					var _super = $(this);
					
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure to approve this user - '+userName+' ?', function(e){
					if(e){
						$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/userapprovalupdate',
								{ 
									userId: userId, 
									//userActive: userActive,
								}
							).done(function(result){
								if(result['active']==1)
								{							
									alertify.alert(userName + ' has been successfully approved.');
									var $tbody = _super.parents('tbody.pending-user-list');
									_super.parent().parent().remove();
									
									//inserting a informative row when there is no data available
									if(!$tbody.find('tr').length){
										$tbody.html('<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>');
									}
								}
							}).error(function(jqXHR, textStatus, errorThrown){
								alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
							});
						}
					}); 		
					});
					
					$('#pending-user-approval-dedicated_wrapper').on('click', '.userDeny', function(){
							var userId = $(this).data('deny-user-id') || 0;
							var userDenystatus = $(this).data('deny-status') || 0;
							var userName = $(this).data('user-name') || 0;
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure to deny this user - '+userName+'?', function(e){
							if(e){	
								$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/userdeny',
									{ userId: userId, userDenystatus: userDenystatus}
								).done(function(result){
									if(result['denied']==1)
									{							
										alertify.alert(userName + ' has been successfully denied.');
										window.location.reload();
									} else
									{
										alertify.alert(result['error']);	
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
								});
							}
						}); 	
					});
					//end pending users data table
		},
			init: function(){
				//executing default form validation pre-requisite
				//app.AdminShift.runDefaultValidation();
				//attaching profile form validation
				//this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
					
};
	//initializing app
	app.AdminPendingUserApproval.init();
})(jQuery);