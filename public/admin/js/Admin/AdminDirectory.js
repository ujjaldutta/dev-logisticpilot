var app = app || {};
(function($){
	'use strict';
	app.AdminDirectory = {
		activateHandlers: function(){
			var _super = this;
			
			//code to handle pagination limit
			$('#pagination-per_page').on('change', function(){
				var value = $(this).val();
				window.location.href= app.Config.getScriptBaseUrl() + 
					app.Config.adminPrefix + 
					'/users/list?limit=' + value;
			});
			
			$('#clear-search').on('click', function(){
				window.location.href = app.Config.getScriptBaseUrl() + 'admin/users/list/';
			});
			
			//function to handle confirmation when soft deleting
			$('.user-delete').on('click', function(){
				var $userId = $(this).data('id') || 0;
				var $userName = $(this).data('name') || 0;
				try{
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure to delete user - ' + $userName + '?', function(e){
						if(e){
							$('#form-user-delete-' + $userId).submit();
						}
					});
				}catch(error){
					alert(error.message);
				}
			});
			
			//function to handle confirmation when restore
			$('.user-restore').on('click', function(){
				var $userId = $(this).data('id') || 0;
				var $userName = $(this).data('user-name') || 0;
				try{
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure you wish to restore user - ' + $userName + '?', function(e){
						if(e){
							$('#form-user-restore-' + $userId).submit();
						}
					});
				}catch(error){
					alert(error.message);
				}
			});
			
			//Start historical user directory data table function		
					var oTable = $('#historical-user-table').dataTable({
						"aoColumns": [
							{ "bSortable": false },
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							{ "bSortable": false },
							{ "bSortable": false },
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						//'oSearch' : false,
						//"bPaginate": false,
						 "aaSorting": [
							[6, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#historical-user-table_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#historical-user-table_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#historical-user-table_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#historical-user-table_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
				
					
					
		},
		
		init: function(){
			//activating all event listeners
			this.activateHandlers();
		},
		
		loadDirectoryDataTable: function(columns){
			//Start active user directory data table function		
					var oTable = $('#active-user-table').dataTable({
						"aoColumns": columns,
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						//'oSearch' : false,
						//"bPaginate": false,
						 "aaSorting": [
							[8, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#active-user-table_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#active-user-table_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#active-user-table_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#active-user-table_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});	
		},
	};
	
	//initializing app
	app.AdminDirectory.init();
	
})(jQuery);