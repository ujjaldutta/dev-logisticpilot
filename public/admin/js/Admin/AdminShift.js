var app = app || {};
(function($){
	'use strict';
	app.AdminShift = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				//ignore: ':hidden',
				ignore : [],
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Please enter valid postal code (no spaces)");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Please enter valid ZIP code (no spaces)");
			
			$.validator.addMethod("recurrenceValid", function(value, element){
				if($('[name=recurrence] option:selected').length && 
					$('[name=recurrence] option:selected').val() != 'none'
				)
					return /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
				else
					return true;
			}, "Invalid recurrence validity date");
			
			$.validator.addMethod('endDateNotBeforeStartDate', function(value, element){
				if(!this.optional(element) && $('#start_date').val().trim() != ''){
					var start = moment($('#start_date').val(), 'YYYY-MM-DD'); //.format('YYYY-DD-MM');
					var end = moment(value, 'YYYY-MM-DD'); //.format('YYYY-DD-MM')
					return end >= start;
				}else
					return false;
			}, 'Your end date should be same or past than start date');
			
			 $.validator.addMethod('endTimeNotBeforeStartTime', function(value, element){
				if(!this.optional(element)){
					var startDate = $('#start_date').val().trim();
					var startTime = $('#start_time').val().trim();
					var endDate = $('#end_date').val().trim();
					var endTime = $('#end_time').val().trim();
					
					try{
						var startDateTime = moment(startDate + ' ' + startTime + ':00');
						var endDateTime = moment(endDate + ' ' + endTime + ':00');
						if(startDateTime.format('HH:mm') != "Invalid date" && endDateTime.format('HH:mm') != "Invalid date"){
							return endDateTime > startDateTime;
						}else
							return false;
					}catch(ex){
						alert(ex.message);
						return false;
					}
				}else
					return true;
			}, 'Your end date/time should be past than start date/time');
			
			$.validator.addMethod('checkQuantity', function(value, element){
				if(!this.optional(element)){
					var registered = $(element).data('registered') || 0;
					return value && value >= registered;
				}else
					return true;
			}, 'The quantity cannot be lowered');
			
		},
		
		initializeMap: function(id, lat, lng, locations) {
		  var i, infowindow, locations, map, mapOptions, marker, _results;

		  /* Settings */
		  // Custom image marker
		  // image = "assets/img/custom_icon.png"

		  // Map Options
		  mapOptions = {
			zoom: 10,
			center: new google.maps.LatLng(lat, lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		  };
		  
		  // Initialize the map with options (inside #map element)
		  map = new google.maps.Map(document.getElementById(id), mapOptions);

		  // Initialize the pop up
		  infowindow = new google.maps.InfoWindow();

		  // Add a marker and a popup for each locations
		  marker = void 0;
		  i = 0;
		  _results = [];
		  while (i < locations.length) {
			// Marker
			marker = new google.maps.Marker({
			  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			  zIndex: locations[i][3],
			  map: map,
			  // icon: image
			});
			// Pop Up
			google.maps.event.addListener(marker, "click", (function(marker, i) {
			  return function() {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			  };
			})(marker, i));
			_results.push(i++);
		  }
		  return map;
		},
		
		Categories: {
			activateHandlers: function(){
				var _super = this;
				
				//code to handle bootstrap select picker for the specified class group
				try{
					$('.selectpicker').selectpicker();
				}catch(error){
					alert(error.message);
				}				
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{
					
					//role update form submission handler
					$('#category-update-submit').on('click', function(){
						$('#category-update-form').submit();
					});
					
					//check
					$('#shift-category-select-all').on('ifChecked', function(event){
						$('input.shift-categories').iCheck('check');
					});
					
					//uncheck
					$('#shift-category-select-all').on('ifUnchecked', function(event){
						$('input.shift-categories').iCheck('uncheck');
					});
					
					//delete all handler
					$('#shift-category-delete-all').on('click', function(event){
						
						if($('input.shift-categories:checked').length){
							alertify.set({ buttonFocus: "cancel" });
							alertify.confirm('Are you sure to delete?', function (e) {
								if(e){
									
									//getting all selected ids
									var $ids = '';
									$('input.shift-categories:checked').each(function(i){
										$ids += $(this).val() + ',';
									});
									
									if($ids.length > 1)
										$ids = $ids.substring(0, $ids.length-1);
									
									$('#shift-cat-ids').val($ids);
									$('#shift-category-multi-delete').submit();
								}
							});
						}else
							alertify.alert('Please select at least one item');
					});
					
					//handles to auto select row checkbox when any of its fields are changed
					$('.data-list select, .data-list input').on('change ifChecked ifUnchecked', function(event){
						var $id = $(this).data('id') || 0;
						$('[id=' + $id + ']').iCheck('check');
						
						//checking all checkbox when all check boxes are checked
						if($('input.shift-categories').length == $('input.shift-categories:checked').length)
							$('#shift-category-select-all').iCheck('check');
					});
					
					//handles auto select/deselect all check boxes
					$('input.shift-categories').on('ifCheck ifUnchecked', function(event){
						if($('input.shift-categories').length == $('input.shift-categories:checked').length)
							$('#shift-category-select-all').iCheck('check');
						else
							$('#shift-category-select-all').iCheck('indeterminate');
					});
					
					//code to handle pagination limit
					$('#pagination-per_page').on('change', function(){
						var value = $(this).val();
						window.location.href= app.Config.getScriptBaseUrl() + 
							app.Config.adminPrefix + 
							'/shifts/category-list?limit=' + value;
					});
				}catch(error){
					alert(error.message);
				}
				
				
			},
			
			runCategoryAddValidator : function () {
				var form = $('#form-admin-shiftcategory-add');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"name": {
							required: true
						},
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//executing default form validation pre-requisite
				app.AdminShift.runDefaultValidation();
				//attaching profile form validation
				this.runCategoryAddValidator();
			}
		},
		
		Skill: {
			activateHandlers: function(){	
				var _super = this;
    
   			//editables 
    			$('.inline-edit').editable({
           		url: app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-update-skill-category-name',
           		validate: function(value) {
           			if($.trim(value) == '') return 'This field is required';
        			},
           		success: function(response, newValue){
           			if(response['error'])
           				return response['error'];
           		},
    			});
    			
    			//editables 
    			$('.inline-skill-edit').editable({
           		url: app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-update-skill-name',
           		validate: function(value) {
           			if($.trim(value) == '') return 'This field is required';
        			},
           		success: function(response, newValue){
           			if(response['error'])
           				return response['error'];
           		},
    			});
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{
					//role update form submission handler
					$('#submit-skill-updates').on('click', function(){
						$('#form-admin-user-update-skills').submit();
					});
					
					//check
					$('#shift-category-select-all').on('ifChecked', function(event){
						$('input.shift-categories').iCheck('check');
					});
					
					//uncheck
					$('#shift-category-select-all').on('ifUnchecked', function(event){
						$('input.shift-categories').iCheck('uncheck');
					});
					
					//delete all handler
					$('#shift-category-delete-all').on('click', function(event){
						
						if($('input.shift-categories:checked').length){
							alertify.set({ buttonFocus: "cancel" });
							alertify.confirm('Are you sure to delete?', function (e) {
								if(e){
									
									//getting all selected ids
									var $ids = '';
									$('input.shift-categories:checked').each(function(i){
										$ids += $(this).val() + ',';
									});
									
									if($ids.length > 1)
										$ids = $ids.substring(0, $ids.length-1);
									
									$('#shift-cat-ids').val($ids);
									$('#shift-category-multi-delete').submit();
								}
							});
						}else
							alertify.alert('Please select at least one item');
					});
				}catch(error){
					console.log(error.message);
				}
				
				//handles to auto select row checkbox when any of its fields are changed
				$('.data-list select, .data-list input').on('change ifChecked ifUnchecked', function(event){
					var $id = $(this).data('id') || 0;
					$('[id=' + $id + ']').iCheck('check');
					
					//checking all checkbox when all check boxes are checked
					if($('input.shift-categories').length == $('input.shift-categories:checked').length)
						$('#shift-category-select-all').iCheck('check');
				});
				
				//handles auto select/deselect all check boxes
				$('input.shift-categories').on('ifCheck ifUnchecked', function(event){
					if($('input.shift-categories').length == $('input.shift-categories:checked').length)
						$('#shift-category-select-all').iCheck('check');
					else
						$('#shift-category-select-all').iCheck('indeterminate');
				});
				
				//code to handle pagination limit
				$('#pagination-per_page').on('change', function(){
					var value = $(this).val();
					window.location.href= app.Config.getScriptBaseUrl() + 
						app.Config.adminPrefix + 
						'/shifts/skill-categories?limit=' + value;
				});
				
				$('#addSkill').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var category_id = button.data('category-id') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('#skill-category-id').val(category_id)
				});
				
				$('#submit-skill').on('click', function(){
					var $skill = $('#skill-name').val().trim();
					var $skill_category_id = $('#skill-category-id').val();
					var $skills = [];
					
					//getting already added skills so that duplication does not occur
					$('a.skill-remove').each(function(i){
						var item = $(this).clone();
						$(item).find('i').remove();
						$skills.push($(item).text().trim());
					});
					 
					
					if(!/^[a-zA-Z\s]+$/.test($skill))
						alertify.alert('The skill name is invalid');					
					else if($.inArray($skill, $skills) >= 0)
						alertify.alert('The skill name is already exists');						
					else{
						$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/add-skill', {
							name: $skill,
							skill_category_id: $skill_category_id
						}).done(function(result){
							if(result['success'])
								window.location.href= app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/skill-categories';
							else if(result['error'])
								alertify.alert(result['error'])
							else {}
						}).fail(function(jqXHR, textStatus, errorThrown){
							alertify.alert("Error: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
						});
						//console.log($skill);
						//console.log($skill_category_id);
					}
				});
				
				$('.skill-remove').on('click', function(){
					var $id = $(this).data('id') || 0;
					var $skill_category_name = $(this).data('category-name') || 0;
					var $skill_category_id = $(this).data('category-id') || 0;
					
					$('[id=' + $skill_category_id + ']').iCheck('check');
					
					//checking all checkbox when all check boxes are checked
					if($('input.shift-categories').length == $('input.shift-categories:checked').length)
						$('#shift-category-select-all').iCheck('check');
					
					var _super = $(this);
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure you wish to delete the skill - '+ $skill_category_name +' ?', function(e){
						if(e){
							console.log($id);
							var $skill_ids = $('#skill_to_be_removed_' + $skill_category_id).val().split(',');
							$skill_ids.push($id);
							$('#skill_to_be_removed_' + $skill_category_id).val($skill_ids.join(','));
							_super.remove();
						}
					});
				});
				
			},
			runSkillCategoryAddValidator : function () {
				var form = $('#form-admin-skill-category');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"name": {
							required: true
						},
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//executing default form validation pre-requisite
				app.AdminShift.runDefaultValidation();
				//attaching profile form validation
				this.runSkillCategoryAddValidator();
			}
		},
		ScheduleFlyerCommon: {
			activateHandlers: function(){
				var _super = this;
				
				try{ 
					var startDate = $('#start_date').datepicker({
						startDate: new Date(),
						autoclose: true
					}).on('changeDate', function(ev) {
						$('#end_date').datepicker('setStartDate', ev.date);
						$('#end_date').val('');
					});
					
					$('#end_date').datepicker({
						startDate: new Date(),
						autoclose: true
					}).on('changeDate', function(ev) {
						$('#recurrence_until').datepicker('setStartDate', ev.date);
						$('#recurrence_until').val('');
					});
					
					$('#recurrence_until').datepicker({
						startDate: new Date(),
						autoclose: true
					});
					
					$('#end_time').datetimepicker({
						format: 'HH:mm',
						useCurrent: false,
						widgetPositioning:{
							horizontal: 'left',
							vertical: 'bottom'
						},
						widgetParent: $('.end-datetime')
					});
					
					//end time widget
					$('#start_time').datetimepicker({
						format: 'HH:mm',
						useCurrent: false,
						widgetPositioning:{
							horizontal: 'left',
							vertical: 'bottom'
						},
						widgetParent: $('.start-datetime')
						
					});
					
					//code to show pop overs for events on the calendar
					$('.popovers').popover();
					
					//adding icheck support
					$('input.popup[type="checkbox"].grey, input.popup[type="radio"].grey, #role_type_single, #role_type_selective').iCheck({
						checkboxClass: 'icheckbox_minimal-grey',
						radioClass: 'iradio_minimal-grey',
						increaseArea: '10%' // optional
					});
					
					//code to manage role type selection
					
					$('#role_type_single').on('ifChecked', function(event){
						if($(this).hasClass('confirmation')){
							var _super = $(this);
							alertify.confirm('Are you sure you wish to change the Available Space method?', function(e){
								if(!e){
									$('#role_type_selective').removeClass('confirmation').iCheck('check').addClass('confirmation');
									_super.next('.old-role-id').remove();
									return;
								}else{
									$('select.exisitng-roles').each(function(i){
										var $val = $(this).find('option:selected').val();
										_super.after('<input type="hidden" class="old-role-id" name="changed_role_ids[]" value="'+ $val + '"/>');
									});
								}
							});
						}
						$('.item-selective input, .item-selective select, .item-selective button').prop('disabled', true);
						$('a.remove-roles-edit').attr('disabled', true);
						$('.item-single input, .item-single select, .item-single button').prop('disabled', false);
						$('.print-selective').iCheck('disable');
						$('.print-all').iCheck('check');
					});
					
					$('#role_type_selective').on('ifChecked', function(event){
						if($(this).hasClass('confirmation')){
							var _super = $(this);
							alertify.confirm('Are you sure you wish to change the Available Space method? This will remove all registered users and notify them by email?', function(e){
								if(!e){
									$('#role_type_single').removeClass('confirmation').iCheck('check').addClass('confirmation');
									_super.next('.old-role-id').remove();
									return;
								}else{
									var $oldDefaultRoleId = $('#role_id_single').val();
									$('input.old-role-id').remove();
									_super.after('<input type="hidden" class="old-role-id" name="changed_role_ids[]" value="'+ $oldDefaultRoleId + '"/>');
								}
							});
						}
						$('.item-selective input, .item-selective select, .item-selective button').prop('disabled', false);
						$('a.remove-roles-edit').removeAttr('disabled');
						$('.item-single input, .item-single select, .item-single button').prop('disabled', true);
						$('.print-selective').iCheck('enable');	
					});
					
					//checking if existing roles has been changed without deleting. 
					$('select.exisitng-roles').on('change', function(){
						var exisitingRoleId = $(this).data('role-id') || 0;
						var selectedRoleId = $(this).find('option:selected').val();
						//if newly selected role is same as before then dont fire anything
						var _super = $(this);
						if(exisitingRoleId != selectedRoleId){
							alertify.confirm('Are you sure you wish to change the role? This will remove all registered users and notify them by email when you finally save this event?', function(e){
								if(!e){
									_super.find('option[value=' + exisitingRoleId + ']').prop('selected', true);
									_super.next('.old-role-id').remove();
								}else{
									_super.next('.old-role-id').remove();
									_super.after('<input type="hidden" class="old-role-id" name="changed_role_ids[]" value="'+ exisitingRoleId + '"/>');
								}
							});
						}else
							_super.next('.old-role-id').remove();
					});
					
					
					$('body').on('hidden.bs.modal', '.modal', function () {
						$(this).removeData('bs.modal');
						
						$('#event-management').find('.modal-content').html(
							'<div class="manager-overlay"><div class="loadtext"><img src="' + app.Config.getScriptBaseUrl() + 'images/preloader_white.gif' + '" alt="Loading..."></div></div>'
						);
						//refetching calendar items when sorting parameters change.
						$('#calendar').fullCalendar('refetchEvents');
					});
					
					//code to add multiple rows of selective roles when add more button
					$('#add-roles').on('click', function(){
						var randomTimeStamp = Math.floor(Date.now() / 1000);
						
						var html = $('.selective-roles').clone(true);
						html.removeClass('selective-roles').addClass('generated').attr('id', randomTimeStamp);
						html.find('.radio').html('');
						html.find('select.exisitng-roles').removeClass('exisitng-roles').off('change');
						html.find('#add-roles').removeClass('roles');
						html.find('div.buttons')
							.html('<a href="#" class="blue-text" style="padding-top:8px;">Remove</a>')
							.on('click', function(){ 
								$('#'+ randomTimeStamp).remove();
							});
						html.appendTo('#role-management');
					});
					
					//code to remove already added roles on edit mode
					$('#role-management').on('click', '.remove-roles-edit', function(event){
						var divId = $(this).data('remove-id') || 0;
						var roleId = $(this).data('role-id') || 0;
						var shiftId = $(this).data('shift-id') || 0;
						
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Warning: Removing this role will result in the removal of registered users. Do you wish to continue?', function(e){
							if(e){
								//firing ajax to send cancellation notices and removing the role instantly
								
								$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-remove-role',
									{shiftId: shiftId, roleId: roleId}
								).done(function(result){
									if(result['success'])
										$('div#'+divId).remove();
									else if(result['error'])
										alertify.alert(result['error'])
									else {}
								}).fail(function(jqXHR, textStatus, errorThrown){
									alertify.alert("Error: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
								});
							}
						});
					});
					
					//code to handle toggle map visibility
					$('#toggle-gmaps').on('ifUnchecked', function(){
						$('#map-area').addClass('hidden');
					});
					
					//code to handle submit button functionality
					$('.submit').on('click', function(){
						if($('#event_creation').valid()){
							$('#event_creation').submit();
						}else{
							alertify.alert('There are some errors on this form');
						}
					});
					
				}catch(ex){
					alert(ex.message);
				}
				
			},
			runValidator : function () {
				var form = $('#event_creation');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"name": {
							required: true
						},
						"category_id": {
							required: true
						},
						"start_date": {
							required: true,
							date: true,
						},
						"end_date": {
							required: true,
							date: true,
							endDateNotBeforeStartDate: true,
						},
						"start_time": {
							required: true,
						},
						"end_time": {
							required: true,
							endTimeNotBeforeStartTime: true,
						},
						"recurrence": {
							required: true,
						},
						"description": {
							required: true,
						},
						"street_address": {
							required: true,
						},
						"city": {
							required: true,
						},
						"recurrence_until": {
							//required: true,
							recurrenceValid: true
						},
						"role_qty[]": {
							required: true,
							digits: true,
						}
						/* "zip": {
							required: true,
							zip_validation: true
						}, */
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//executing default form validation pre-requisite
				app.AdminShift.runDefaultValidation();
				//code to run validation
				this.runValidator();
				//activation handlers
				this.activateHandlers();
			}
		},
		
		ScheduleAddFlyer:{
			activateHandlers: function(){
				var _super = this;		
				
				try{
					//code to handle gmaps toggle with specified address
					var map = null;
					var lat = 0;
					var lng = 0;
				 
					$('#toggle-gmaps').on('ifChecked', function(){
						$('#map-area').removeClass('hidden');
						map = map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
						});
						/* GMaps.geolocate({
						  success: function(position) {
							lat = position.coords.latitude;
							lng = position.coords.longitude;
							//map construction when geolocation is supported
							map = map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  error: function(error) {
							alert(error);
							map = map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  not_supported: function() {
							alert('You browser doesn not support geo location facility');
							map = map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  always: function() {
							_super.map = _super.map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  }
						}); */
						
						$('#street_address').trigger('blur');
					});
					
					//function to handle map search based on the location given
					$('#street_address, #city, #zip, #state_id').on('blur', function(){
						var address = $('#street_address').val().trim();
						var city = $('#city').val().trim();
						var zip = $('#zip').val().trim();
						var state = $('#state_id option:selected').text();
						
						address = (address.length && city.length) ? address + ', ' + city : address;
						address = (address.length && state.length) ? address + ' ' + state : address;
						address = (address.length && zip.length) ? address + ' ' + zip : address;
						//console.log(address);
						
						if(address.length && map){
							GMaps.geocode({
								address: address,
								callback: function (results, status) {
									if (status == 'OK') {
										var latlng = results[0].geometry.location;
										//setting lat and long to save in the database
										$('#lat').val(latlng.lat());
										$('#lng').val(latlng.lng());
										
										map.removeMarkers(); //removing any marker already added previously
										map.setCenter(latlng.lat(), latlng.lng());
										map.addMarker({
											lat: latlng.lat(),
											lng: latlng.lng(),
											infoWindow: {
												content: address,
											}
										});
									}
								}
							});
						}
					});
					
					$('#event-management').on('loaded.bs.modal', function () {
						$('.manager-overlay').addClass('hidden');
					});
				
				}catch(error){
					alert(error.message);
				}
				
				//next tab switcher code after current tab form validation
				$('.next').on('click', function(){	
					//if($('#event_creation').valid()){
						var nextTab = $(this).data('tab-next') || null;
						var nextTabNo = nextTab ? parseInt(nextTab.charAt(3)) : nextTab;
						$('.nav li a[href=#' + nextTab + ']').tab('show');
						
						if(nextTabNo > 1 && nextTabNo < 3){
							$(this).data('tab-next', 'tab' + eval(nextTabNo + 1));
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo -1));
							$('.prev').removeClass('hidden');
						}else{
							nextTabNo == 3 ? $('button.submit').removeClass('hidden') 
								: $('button.submit').addClass('hidden');
							
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo - 1));
							$(this).addClass('hidden');
						}
						
					//}
				});
				
				//prev tab switcher code after current tab form validation
				$('.prev').on('click', function(){
					//if($('#event_creation').valid()){
						//prev step should disable add event button
						$('button.submit').addClass('hidden');
						
						var prevTab = $(this).data('tab-prev') || null;
						var prevTabNo = prevTab ? parseInt(prevTab.charAt(3)) : prevTab;
						$('.nav li a[href=#' + prevTab + ']').tab('show');
						
						if(prevTabNo > 1 && prevTabNo < 3){
							$(this).data('tab-prev', 'tab' + eval(prevTabNo - 1));
							$('.next').data('tab-next', 'tab' + eval(prevTabNo + 1));
							$('.next').removeClass('hidden');
						}else{
							$(this).addClass('hidden');
							$('.next').data('tab-next', 'tab' + eval(prevTabNo + 1));
						}
					//}
				});
				
				//code to validate and handle next previous button when changed directly using tabs
				$('a[data-toggle="tab"]').each(function(){
					$(this).on('show.bs.tab', function (e) {
						var nextTabNo = parseInt($(e.target).attr('href').charAt(4));
						//$('.nav li a[href=#tab' + nextTabNo + ']').tab('show');
						if(nextTabNo == 1){
							$('.submit').addClass('hidden');
							$('.prev').addClass('hidden');
							$('.next').removeClass('hidden');
							$('.next').data('tab-next', 'tab2');
						}else if(nextTabNo > 1 && nextTabNo < 3){
							$('.submit').addClass('hidden');
							$('.next').data('tab-next', 'tab' + eval(nextTabNo + 1));
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo - 1));
							$('.next').removeClass('hidden');
							$('.prev').removeClass('hidden');
						}else if(nextTabNo == 3){
							$('.submit').removeClass('hidden');
							$('.next').addClass('hidden');
							$('.prev').removeClass('hidden');
							$('.prev').data('tab-prev', 'tab2');
						}
					});
				});
			},
			
			init: function(){
				//running common functionlity
				app.AdminShift.ScheduleFlyerCommon.init();
				//activating all event listeners
				this.activateHandlers();
			}
		},
		
		ScheduleEditFlyer:{
			map: null,
			activateHandlers: function(){
				var _super = this;
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{
					//code to handle gmaps toggle with specified address
					var map = null;
					//var lat = 0;
					//var lng = 0;
					
					/* var map = null; */
					
					var lat = $('#lat').val().length ? parseFloat($('#lat').val()) : 0;
					var lng = $('#lng').val().length ? parseFloat($('#lng').val()) : 0;
					
					//console.log('lat: '+ lat);
					//console.log('lng: '+ lng);
					
					_super.map = new GMaps({
					  div: '#map-search-edit',
					  lat: lat,
					  lng: lng
					});
					
					_super.map.addMarker({
					  lat: lat,
					  lng: lng,
					  title: $('#name').val().trim()
					});
					
					$('#event-management').on('loaded.bs.modal', function () {
						$('.manager-overlay').addClass('hidden');
						
						//triggering the GEO CODE search functionality to auto point and zoom the desired location
						$('#street_address').trigger('blur');
						
						//_super.map.refresh();
						
						 $('#map-search-edit').removeAttr('style').css({
							'background-color': '#e5e3df',
							overflow: 'hidden',
							position: 'relative',
						});
					});
					
					$('#toggle-gmaps').on('ifChecked', function(){
						$('#map-area').removeClass('hidden');
						
						_super.map =  _super.map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
						});
						
						/* GMaps.geolocate({
						  success: function(position) {
							lat = position.coords.latitude;
							lng = position.coords.longitude;
							//map construction when geolocation is supported
							//console.log(_super.map);
							_super.map =  _super.map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  error: function(error) {
							alert(error);
							_super.map = _super.map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  not_supported: function() {
							alert('You browser doesn not support geo location facility');
							_super.map = _super.map || new GMaps({
								div: '#map-search',
								lat: lat,
								lng: lng
							});
						  },
						  always: function() {
							
						  }
						}); */
						
						//triggering the GEO CODE search functionality to auto point and zoom the desired location
						$('#street_address').trigger('blur');
					});
					
					//function to handle map search based on the location given
					$('#street_address, #city, #zip, #state_id').on('blur', function(){
						//console.log('triggered');
						
						var address = $('#street_address').val().trim();
						var city = $('#city').val().trim();
						var zip = $('#zip').val().trim();
						var state = $('#state_id option:selected').text();
						
						address = (address.length && city.length) ? address + ', ' + city : address;
						address = (address.length && state.length) ? address + ' ' + state : address;
						address = (address.length && zip.length) ? address + ' ' + zip : address;
						
						if(address.length && _super.map){
							GMaps.geocode({
								address: address,
								callback: function (results, status) {
									if (status == 'OK') {
										var latlng = results[0].geometry.location;
										//console.log('address: ' + address);
										//console.log('lat: ' + latlng.lat());
										//console.log('lng: ' + latlng.lng());
										
										//setting lat and long to save in the database
										$('#lat').val(latlng.lat());
										$('#lng').val(latlng.lng());
										
										_super.map.setCenter(latlng.lat(), latlng.lng());
										_super.map.removeMarkers();
										_super.map.addMarker({
											lat: latlng.lat(),
											lng: latlng.lng(),
											infoWindow: {
												content: address,
											}
										});
										
										_super.map.refresh();
									}
								}
							});
						}
					});
					
					//triggering the GEO CODE search functionality to auto point and zoom the desired location
					//$('#street_address').trigger('blur');
					
				}catch(error){
					alert(error.message);
				}
				
				/* code to handle registration for a shift on its various roles */
				$('.shift-registration').on('click', function(event){
					var roleId = $(this).data('role-id') || null;
					
					//getting selected user from the respective dropdown
					var userId = roleId ? $('#shift_user_id_' + roleId + ' option:selected').val() : null
					var shiftId = $('#shift_user_id_' + roleId).data('shift-id') || null;
					var vacancy = $('#shift_user_id_' + roleId).data('vacancy') || 0;
					
					//checking for a valid user
					if(userId == '' || isNaN(userId)){
						$('#shift_user_id_' + roleId).parent('div.form-group').addClass('has-error');
						$('#shift_user_id_' + roleId).next('label').length ? void(0)  
							: $('#shift_user_id_' + roleId).after('<label class="help-block">Please select a valid user</label>');
						return;
					}else{
						$('#shift_user_id_' + roleId).next('label').remove();
						$('#shift_user_id_' + roleId).parent('div.form-group').removeClass('has-error');
					}
					
					var _super = $(this);
					//changing button text
					_super.prop('disabled', true).text('Registering...');
					
					//posting AJAX request to register the selected user on the shift for the role
					$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-register', {
						shiftId: shiftId,
						userId: userId,
						roleId: roleId,
						vacancy: vacancy,
					}).done(function(result){
						if(result['success'] && result['registration']){
							//console.log(result['registration']);
							$('#shift_user_id_' + roleId).parent().parent().parent().find('.registered').text(result['registration']);
							$('#shift_user_id_' + roleId + ' option:selected').remove();
						}else if(result['error']){
							alertify.alert(result['error']);
							//console.log(result['error']);
						}else{}
					}).error(function(jqXHR, textStatus, errorThrown){
						alert(textStatus + ' ' + jqXHR.status + ': ' + errorThrown);
						_super.prop('disabled', true).text('Register');
					}).always(function(result){
						//changing button text
						if(result['registration'] && result['registration'] < vacancy){
							_super.prop('disabled', false).text('Register');
						}else{
							_super.prop('disabled', false).text('Register');
							$('#shift_user_id_' + roleId).prop('disabled', false);
						}
					});
				});
				
				//code to handle view and print roasters
				$('#view-print-roaster').on('click', function(event){
					var shiftId = $(this).data('shift-id') || 0;
					var shiftName = $(this).data('shift-name') || 'Unknown';
					var shiftDate = $(this).data('shift-date') || 'Unknown';
					
					var date = moment(shiftDate); //constructing moment js functionlity for formatting
					//console.log(date.format('MMMM D, YYYY'));
					
					
					BootstrapDialog.show({
						title: "Roster information for '" + shiftName + "' on " + date.format('MMMM D, YYYY'),
						message: function(dialog) {
							var $message = $('<div></div>');
							var pageToLoad = dialog.getData('pageToLoad');
							$message.load(pageToLoad);
							return $message;
						},
						data: {
							'pageToLoad': app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-view-roasters/' + shiftId 
						},
						buttons: [
							{
								label: 'Pdf',
								icon: 'clip-file-pdf',
								cssClass: 'btn-default',
								action: function(dialogItself){
									//dialogItself.close();
									var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-view-roasters/' + shiftId + '/pdf', '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );
								}
							},
							{
								label: 'Print',
								icon: 'fa fa-print',
								cssClass: 'btn-default',
								action: function(dialogItself){
									//dialogItself.close();
									//Print ele4 with custom options
									$("#roasters").print({
										//Use Global styles
										globalStyles : true,

										//Print in a hidden iframe
										iframe : true,

										//Don't print this
										noPrintSelector : ".actions",
									});
								}
							},
							{
							label: 'Close',
							cssClass: 'btn-default pull-right',
							action: function(dialogItself){
								dialogItself.close();
							},
						},
						],
					});
				});
				
				//handle approve deny delete etc from roaster dialog
				$('body').on('click', '#roasters a.inactive, #roasters a.active', function(event){
					var regId = $(this).data('reg-id') || 0;
					var action = $(this).hasClass('active') ? 'active' : 'inactive'
					var _super = $(this);
					alertify.confirm('Are you sure to ' + action + ' this user?', function(e){
						if(e){
							$.post(
								app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-manage-registration-status/' + action, {id: regId}
							).done(function(result){
								if(result['success'] && result['success'] == 'inactive'){
									_super.addClass('inactive').removeClass('active').text('Deny');
									//console.log(_super.parent('td').siblings('td.status'));
									_super.parent('td').siblings('td.status').text('No');
								}else if(result['success'] && result['success'] == 'active'){
									_super.addClass('active').removeClass('inactive').text('Approve');
									//console.log(_super.parent('td').siblings('td.status'));
									_super.parent('td').siblings('td.status').text('Yes');
								}else if(result['error']){
									alertify.alert(result['error']);
								}
							}).error(function(jqXHR, textStatus, errorThrown){
								alertify.alert(textStatus + ' ' + jqXHR.status + ': ' + errorThrown);
							});
						}
					})
					//alert('here');
				})
				
				//handle approve deny delete etc from roaster dialog
				$('body').on('click', '#roasters a.remove', function(event){
					var regId = $(this).data('reg-id') || 0;
					var legendId = $(this).data('legend-key') || 0;
					var _super = $(this);
					console.log($('#roaster-table-' + legendId + ' tr').length);
					alertify.confirm('Are you sure to remove this user?', function(e){
						if(e){
							$.post(
								app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/ajax-delete-registration', {id: regId}
							).done(function(result){
								if(result['success']){
									//console.log(_super.parent('td').siblings('td.status'));
									$('span.registered_count_' + legendId).text(result['vacancy']);
									_super.parents('tr.roster-row').remove();
									if($('#roaster-table-' + legendId + ' tr').length <= 1){
										$('<tr><td colspan="5" class="center"><span class="label label-sm label-inverse">No User Found!</span></td></tr>').appendTo('#roaster-table-' + legendId + ' tbody');
									}
								}else if(result['error']){
									alertify.alert(result['error']);
								}
							}).error(function(jqXHR, textStatus, errorThrown){
								alertify.alert(textStatus + ' ' + jqXHR.status + ': ' + errorThrown);
							});
						}
					})
					//alert('here');
				})
				
				//next tab switcher code after current tab form validation
				$('.next').on('click', function(){
					//if($('#event_creation').valid()){
						var nextTab = $(this).data('tab-next') || null;
						var nextTabNo = nextTab ? parseInt(nextTab.charAt(3)) : nextTab;
						$('.nav li a[href=#' + nextTab + ']').tab('show');
						
						if(nextTabNo > 1 && nextTabNo < 4){
							$(this).data('tab-next', 'tab' + eval(nextTabNo + 1));
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo -1));
							$('.prev').removeClass('hidden');
						}else{
							nextTabNo == 4 ? $('button.submit').removeClass('hidden') 
								: $('button.submit').addClass('hidden');
							
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo - 1));
							$(this).addClass('hidden');
						}
						
					//}
				});
				
				//prev tab switcher code after current tab form validation
				$('.prev').on('click', function(){
					//if($('#event_creation').valid()){
						//prev step should disable add event button
						$('button.submit').addClass('hidden');
						
						var prevTab = $(this).data('tab-prev') || null;
						var prevTabNo = prevTab ? parseInt(prevTab.charAt(3)) : prevTab;
						$('.nav li a[href=#' + prevTab + ']').tab('show');
						
						if(prevTabNo > 1 && prevTabNo < 4){
							$(this).data('tab-prev', 'tab' + eval(prevTabNo - 1));
							$('.next').data('tab-next', 'tab' + eval(prevTabNo + 1));
							$('.next').removeClass('hidden');
						}else{
							$(this).addClass('hidden');
							$('.next').data('tab-next', 'tab' + eval(prevTabNo + 1));
						}
					//}
				});
				
				//code to validate and handle next previous button when changed directly using tabs
				$('a[data-toggle="tab"]').each(function(){
					$(this).on('show.bs.tab', function (e) {
						var nextTabNo = parseInt($(e.target).attr('href').charAt(4));
						//$('.nav li a[href=#tab' + nextTabNo + ']').tab('show');
						if(nextTabNo == 1){
							$('.submit').addClass('hidden');
							$('.prev').addClass('hidden');
							$('.next').removeClass('hidden');
							$('.next').data('tab-next', 'tab2');
						}else if(nextTabNo > 1 && nextTabNo < 4){
							$('.submit').addClass('hidden');
							$('.next').data('tab-next', 'tab' + eval(nextTabNo + 1));
							$('.prev').data('tab-prev', 'tab' + eval(nextTabNo - 1));
							$('.next').removeClass('hidden');
							$('.prev').removeClass('hidden');
						}else if(nextTabNo == 4){
							$('.submit').removeClass('hidden');
							$('.next').addClass('hidden');
							$('.prev').removeClass('hidden');
							$('.prev').data('tab-prev', 'tab3');
							
							var tmOut = setTimeout(function(){
								_super.map.refresh();
								_super.map.setCenter($('#lat').val().trim(), $('#lng').val().trim());
								//console.log('reloaded2');
							}, 4000);
						}
				  
					});
				});
			},
			
			init: function(){
				//running common functionlity
				app.AdminShift.ScheduleFlyerCommon.init();
				//activating all event listeners
				this.activateHandlers();
			}
		},
		
		AddShift: {
			map: null,
			activateHandlers: function(){
				var _super = this;
				try{
					//initializing the calendar app included from form-calendar.js
					Calendar.init();
					
					$('.shift-category-item').on('ifUnchecked ifChecked', function(){
						//refetching calendar items when sorting parameters change.
						$('#calendar').fullCalendar('refetchEvents');
					});
				}catch(error){
					alert(error.message);
				}
			},
			
			switchView: function(startDay){
				if(startDay !== undefined && startDay.length){
					try{
						startDay = moment(startDay);
						if(startDay.format('YYYY-MM-DD') != "Invalid date"){
							$('#calendar').fullCalendar('gotoDate', startDay );
							$('#calendar').fullCalendar('changeView', 'agendaWeek');
						}
					}catch(error){
						alert('Change View Error: ' + error.message);
					}
				}
			},
			
			init: function(startDay){
				//activating all event listeners
				this.activateHandlers();
				//if specific date / week requested change the calendar view
				this.switchView(startDay);
			}
		},
		
		PendingShift: {
			activateHandlers: function(){
				var _super = this;	
					
				try{
					//Start pending shift data table function		
					var oTable = $('#pending-shift-approval-dedicated').dataTable({
						"aoColumns": [
							null,
							null,
							null,
							null,
							null,
							null,
							{ "bSortable": false },
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[5, 'desc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#pending-shift-approval-dedicated_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#pending-shift-approval-dedicated_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#pending-shift-approval-dedicated_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#pending-shift-approval-dedicated_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
					
					$('#pending-shift-approval-dedicated_wrapper').on('click', '.shiftApprove', function(e){
					var shiftRegistrationstatus = $(this).data('shift-registration-status') || 0;
					var shiftRegistrationid = $(this).data('shift-registration-id') || 0;
					var userName = $(this).data('user-name') || 0;
					var shiftLang = $(this).data('shift-lang') || 0;
					var _super = $(this);
					
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure to approve this pending '+shiftLang+' approval from - '+userName+'?', function(e){
					if(e){
						$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/shiftapprovalupdate',
								{ shiftRegistrationstatus: shiftRegistrationstatus, shiftRegistrationid: shiftRegistrationid}
							).done(function(result){
								if(result['status']==1)
								{							
									alertify.alert('The shift has been successfully approved.');
									var $tbody = _super.parents('tbody.pending-shift-list');
									_super.parent().parent().remove();
									
									//inserting a informative row when there is no data available
									if(!$tbody.find('tr').length){
										$tbody.html('<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table</td></tr>');
									}
								}
							}).error(function(jqXHR, textStatus, errorThrown){
								alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
							});
						}
					});
							
				});
				
				$('#pending-shift-approval-dedicated_wrapper').on('click', '.shiftDeny', function(e){
					var shiftRegistrationid = $(this).data('shift-registration-id') || 0;
					var userName = $(this).data('user-name') || 0;
					var shiftLang = $(this).data('shift-lang') || 0;
					var _super = $(this);
										
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure to deny this pending '+shiftLang+' approval from - '+userName+'?', function(e){
					if(e){
						$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/shifts/shiftdeny',
								{ shiftRegistrationid: shiftRegistrationid}
							).done(function(result){
								if(result['status']==1)
								{							
									alertify.alert('The shift registration has successfully been denied.');
									var $tbody = _super.parents('tbody.pending-shift-list');
									_super.parent().parent().remove();
									
									//inserting a informative row when there is no data available
									if(!$tbody.find('tr').length){
										$tbody.html('<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table</td></tr>');
									}
								}
							}).error(function(jqXHR, textStatus, errorThrown){
								alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
							});
						}
					});
					
				});				
				// end pending shift data table function
				
				}catch(error){
					alert(error.message);
				}		
			},
			init: function(){
				//executing default form validation pre-requisite
				//app.AdminShift.runDefaultValidation();
				//attaching profile form validation
				//this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
		},
		
	};
})(jQuery);