var app = app || {};
(function($){
	'use strict';
	app.AdminContactSheet = {
		runDefaultValidation: function(){
		$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		getUsers: function(data, target, eventInfo){
			//showing ajax loading
			$('div[data-loader=' + target + ']').removeClass('hidden');
			
			$.post(
				app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/ajax-contact-sheet-get-users',
				data
			).done(function(result){
				if(result['users']){
					var $html = '<option value="">Please select</option>';
					if(result['count'] !== undefined && result['count'])
						if(eventInfo !== undefined && 'all' == eventInfo)					 
						 	$html += '<option value="all" selected="selected">All</option>';
						else
							$html += '<option value="all">All</option>';
					for(var key in result['users']){
							if(eventInfo !== undefined && key == eventInfo)
								$html += '<option value="' + key +'" selected="selected">' + result['users'][key] + '</option>';
							else
								$html += '<option value="' + key +'">' + result['users'][key] + '</option>';
					}
					
					$('.' + target).html($html);
					//hiding ajax loading
					$('div[data-loader=' + target + ']').addClass('hidden');
				}else
					alertify.alert('Error in response');
				
			}).fail(function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' -- ' + errorThrown);
			});
		},
		
		runValidator: function(){
				var form = $('#contact-sheet-report-generate');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
		},
			
	 activateHandlers: function(){
	 			var _super = this;
	 			$('.role').on('change', function(event, eventInfo){
	 			var selectedRole = $('#' + $(this).attr('id') + ' option:selected').val();
				if(selectedRole.length){
					var data = {role_id: selectedRole};
					_super.getUsers(data, 'user', eventInfo);
				
				}else{
					var $html = '<option value="">Please select</option>';
					//$html += '<option value="all">All</option>';
					$('.user').html($html);	
				}				
				});
				
				$('#make-print').on('click', function(event){
					//Print ele4 with custom options
					$("#report_area").print({
						//Use Global styles
						globalStyles : true,

						//Print in a hidden iframe
						iframe : true,

						//Don't print this
						noPrintSelector : ".actions",
					});
				});
				
				
				$('#make-pdf').on('click', function(event){
					var roleId = $(this).data('role-id') || 0;
					var userId = $(this).data('user-id') || 0;
					
					var win = window.open(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/users/contact-sheet-pdf/' + roleId + '/' + userId, '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100 );
				});
				
				//triggering change event to fetch the user list for this role 				
				var selectedId = $('#user-id').data('selected-id') || 0;
				$('.role').trigger('change', selectedId);
					
		},
			init: function(){
				//executing default form validation pre-requisite
				this.runDefaultValidation();
				//attaching profile form validation
				this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
					
};
	//initializing app
	app.AdminContactSheet.init();
})(jQuery);