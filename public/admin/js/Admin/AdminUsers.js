var app = app || {};
(function($){
	'use strict';
	app.AdminUsers = {
		getStates: function(data, target){
			//showing ajax loading
			$('div[data-loader=' + target + ']').removeClass('hidden');
			
			$.post(
				app.Admin.globalScriptBaseUrl + 'states/ajaxGetStates',
				data
			).done(function(result){
				$('.' + target).html(result);
				
				//hiding ajax loading
				$('div[data-loader=' + target + ']').addClass('hidden');
			}).fail(function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + textStatus + ' -- ' + errorThrown);
			});
		},
		
		runDefaultValidation : function () {
			$.validator.addMethod('valid_phone', function(value, element){
				return this.optional(element) || /^[2-9]\d{2}-\d{3}-\d{4}$/.test(value);
			}, 'Please enter valid number (###-###-####).');
		
			$.validator.addMethod('valid_zip', function(value, element){
				return this.optional(element) || /^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z]( )?\d[a-zA-Z]\d)$/.test(value);
			}, 'Please write a valid USA / Canada zip code');
			
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Please enter valid postal code (no spaces)");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Please enter valid ZIP code (no spaces)");
			
			$.validator.addMethod("alphabetic", function(value, element) {
        		return this.optional(element) || /^[a-z]+$/i.test(value); 
      	}, "This field may only contains alphabetic characters");
      	
      	$.validator.addMethod("alpha_space", function(value, element) {
        		return this.optional(element) || /^[a-zA-Z ][a-zA-Z\\s ]+$/.test(value); 
      	}, "This field may only contains alphabetic characters and spaces");
      
      	$.validator.addMethod("valid_email", function(value, element) {
       		return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
      	}, "Email Address is invalid: Please enter a valid email address.");
			
			
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		PasswordChange: {
			activateHandlers: function(){
				$('#password_change').validate({
					rules: {
						old_password: {
							required: true,
						},
						password: {
							required: true,
						},
						password_confirmation: {
							required: true,
							equalTo: '#password',
						}
					}
				});
			},
			
			init: function(){
				app.AdminUsers.runDefaultValidation();
				this.activateHandlers();
			}
		}
		
	};
	
})(jQuery);