var app = app || {};
(function($){
	'use strict';
	app.AdminInboxMessages = {

		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		Compose: {
			activateHandlers: function(){
				var _super = this;
				
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{
					 CKEDITOR.replace( 'message' );
					
				}catch(error){
					console.log(error.message);
				}
				
				
			},
			
			runInboxValidator : function () {
				var form = $('.message-form');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"select_user[]": {
							required: true
						},
						"subject": {
							required: true
						},
						"attachment":{
							required: true
						},
						"message":{
                         required: true                   
						},
                    
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//executing default form validation pre-requisite
				app.AdminInboxMessages.runDefaultValidation();
				//attaching profile form validation
				this.runSentItemsValidator();
			}
		
		},
		
		Inbox: {
			activateHandlers: function(){
				var _super = this;
				try{	
					
					/*$('#urlChange').change(function() {
     					window.location = $(this).val()
					});*/
					
					/*$(document).ready(function(){
						$("#urlChange").change(function(){
						var orderValue = $(this).val();
						window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/messages/inbox/'+orderValue;
						});
					});*/	
					
													
					
					$(".recvmsg").on('click', function(e){
						var $this = $(this);								
						var msgUserId = $(this).data('message-userid') || 0;
						var readStatus = $(this).data('message-read') || 0;
						var messageId = $(this).data('message-id') || 0;
						
						//showing the respective content
						$('.current').removeClass('current');
						$this.addClass('current');
						$(".messages-content").css('display','none');
						$("#data-inboxmessages-body-" + messageId).fadeIn('slow');
						
						if(readStatus) //when message is already read dont fire ajax call
							return;
						
						$.post(
							app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/messages/updatereadstatus',
							{ msgUserId: msgUserId}						
						).done(function(result){
							if(result['success'] && result['readStatus'])
							{
								$this.removeClass('active');
								$this.data('message-read', isNaN(result['readStatus']) ? 0 : result['readStatus']);
							}else if(result['error']){
								alertify.alert(result['error']);
							}else{}
						}).error(function(jqXHR, textStatus, errorThrown){
							alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
						});
					});
					
					$('.important').on('click', function(event){
					var $this = $(this);
					var messageUserId = $(this).parent('li').data('message-userid') || 0;
					var status = $(this).data('important-status') || 0;
					$.post(
						app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/messages/updateimportantstatus',
						{ messageUserId: messageUserId, status: status}
					).done(function(result){
						if(result['status'] && result['status'] == 1){
							$this.parent('li').addClass('starred'); //({color: 'blue'});
							$this.data('important-status', 0);
							$this.attr('title','Starred');
						}else if(result['status'] && result['status'] == 0){
							$this.parent('li').removeClass('starred');
							$this.data('important-status', 1);
							$this.attr('title','Masrk as Starred');
						}else{}
					}).error(function(jqXHR, textStatus, errorThrown){
						alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
					});
				});
				
				
				//function to handle confirmation when deleting
				$('.msg-delete').on('click', function(){
					var $msgId = $(this).data('id') || 0;
				
					try{
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure to delete this message ?', function(e){
							if(e){
							$('#form-msg-delete-' + $msgId).submit();
							}
						});
					}catch(error){
					alert(error.message);
					}
				});
				
				$("#urlChange").change(function(){
						var orderValue = $(this).val();
						window.location.href = app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/messages/inbox/'+orderValue;
					});
				
					
				}catch(error){
					alert(error.message);
				}
				
				
			},
			
			init: function(){
				this.activateHandlers();
			}	
		}
	};
})(jQuery);