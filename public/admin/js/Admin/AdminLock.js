var app = app || {};
(function($){
	'use strict';
	app.AdminLock = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					/* if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					} */
				},
				ignore: ':hidden',
				highlight: function (element) {
					//$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					//label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Postal code should be like A1A 1A1");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Invalid Zip Code");
			
			$.validator.addMethod("recurrenceValid", function(value, element){
				if($('[name=recurrence] option:selected').length && 
					$('[name=recurrence] option:selected').val() != 'none'
				)
					return /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
				else
					return true;
			}, "Invalid recurrence validity date");
		},
		
		Lock:{
			activateHandlers: function(){
				var _super = this;
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{ 
					
				}catch(ex){
					alert(ex.message);
				}
			},
			
			runValidator : function () {
				var form = $('#form-admin-lock');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"password": {
							required: true
						},
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//executing default form validation pre-requisite
				app.AdminLock.runDefaultValidation();
				//attaching profile form validation
				this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
		},
	};
})(jQuery);