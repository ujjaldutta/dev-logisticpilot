var app = app || {};
(function($){
	'use strict';
	app.AdminMessages = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		Compose: {
			activateHandlers: function(){
				var _super = this;
				
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{
					 CKEDITOR.replace( 'message' );
					 
	var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    
    $('#select-all-users').on('click', function(){
    	$('#user_id option').prop('selected', true).trigger('chosen:updated');    	
    });
    
    $('#clearall').on('click', function(e){
    		$('#user_id option').prop('selected', false).trigger('chosen:updated');
    });
    
    /*$('.add-attach').hide();
    $('.remove-attach').on('click', function(e){
    		$('.remove-attachment').hide();
    		$('.add-attach').show();
    });
    $('.add-attach').on('click', function(e){
    		$('.remove-attachment').show();
    		$('.add-attach').hide();
    });*/
    
    var input = $("#fileopen");
    $("#removeatt").on('click', function(){
    		$("#removespan").html('<input type="file" name="attachment">');
	 });
    
    /*$('.remove_close').ready(function(){
    	$("#user_id_chosen ul li").each(function(){
			$(this).find('a.search-choice-close').remove();
		}); 	    	
    });*/
					
				}catch(error){
					console.log(error.message);
				}
				
				
			},
			
			runComposeValidator : function () {
				var form = $('.message-form');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"subject": {
							required: true
						},
						"message":{
                         required: true                   
                  },
                    
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//executing default form validation pre-requisite
				app.AdminMessages.runDefaultValidation();
				//attaching profile form validation
				this.runComposeValidator();
			}
		
		}
	};
})(jQuery);