var app = app || {};
(function($){
	'use strict';
	app.AdminDashboard = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Postal code should be like A1A 1A1");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Invalid Zip Code");
			
			$.validator.addMethod("recurrenceValid", function(value, element){
				if($('[name=recurrence] option:selected').length && 
					$('[name=recurrence] option:selected').val() != 'none'
				)
					return /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
				else
					return true;
			}, "Invalid recurrence validity date");
		},
		
		PendingShift:{
			oTable: {},
			activateHandlers: function(){
				var _super = this;
							
				try{
					
					$('body').on('hidden.bs.modal', '.modal', function () {
							$('#event-management, #pending-shift-modal, #confirmed-shift-modal, #user-management').find('.modal-content').html(
								'<div class="manager-overlay"><div class="loadtext"><img src="' + app.Config.getScriptBaseUrl() + 'images/preloader_white.gif' + '" alt="Loading..."></div></div>'
							);
							$(this).removeData('bs.modal');
					});	
					//Start pending shift data table function		
					var oTable = _super.oTable = $('#user-management').DataTable ( {
						"ajax": app.Config.getScriptBaseUrl() + app.Config.adminPrefix + "/registrations/list",
						 "deferRender": true,
						  "order": [[ 4, "desc" ]],
						"columns": [
							{ "data": "name" },
							{ "data": "email" },
							{ "data": "plan" },
							{ "data": "status"},
							{ "data": "created_on", className: 'hidden-xs',},
							{ "data": "actions" }
						],
						 "columnDefs": [ 
							{
								"targets": -1,
								"data": null,
								"defaultContent": "<span class=\"links record-view\" title=\"View\"><i class=\"fa fa-eye\"></i></span>"
									+ "&nbsp;"
									//+ "<span class=\"links record-delete\" title=\"Delete\"><i class=\"fa fa-times\"></i></span>"
									//+ "&nbsp;",
							},
						]
					} );
					
					/*var oTable = $('#user-management').dataTable({
							"aoColumns": [
								null,
								null,
								null,
								null,
								null,
								null,
								{ "bSortable": false },
							],
							"oLanguage": {
								"sLengthMenu": "Show _MENU_ Rows",
								"sSearch": "",
								"oPaginate": {
									"sPrevious": "",
									"sNext": ""
								}
							},
							 "aaSorting": [
								[5, 'desc']
							],
							"aLengthMenu": [
								[5, 10, 15, 20, -1],
								[5, 10, 15, 20, "All"] // change per page values here
							],
							// set the initial value
							"iDisplayLength": 5,
							
						}); */
						$('#user-management_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
						// modify table search input
						$('#user-management_wrapper .dataTables_length select').addClass("m-wrap small");
						// modify table per page dropdown
						$('#user-management_wrapper .dataTables_length select').select2();
						// initialzie select2 dropdown
						 $('#user-management_column_toggler input[type="checkbox"]').change(function () {
							// Get the DataTables object again - this is not a recreation, just a get of the object 
							var iCol = parseInt($(this).attr("data-column"));
							var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
							oTable.fnSetColumnVis(iCol, (bVis ? false : true));
						});
					
					$('#user-management_wrapper').on('click', '.activate', function(e){ 
						var data = oTable.row( $(this).parents('tr') ).data();
						
						var status = data.status;
						var id = data.id;
						var _token = data.token;
						var _super = $(this);
						
						//alertify.set({ buttonFocus: "cancel" });

						alertify.confirm('Are you sure you want to active this user?', function(e){

						if(e){
							$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/registrations/set-action',
									{ _token: _token, id: id, status: status}
								).done(function(result){
									if(result.success)
									{							
										alertify.alert('The user has been successfully activated.');
										//reloading data table
										oTable.ajax.reload();
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
								});
							}
						});
								
					});
					
					$('#user-management_wrapper').on('click', '.record-delete', function(e){
						var data = oTable.row( $(this).parents('tr') ).data();
						var id = data.id;
						var _token = data.token;
						var _super = $(this);
						
						//alertify.set({ buttonFocus: "cancel" });

						alertify.confirm('Are you sure you want to delete this user?', function(e){
							if(e){
								$.post(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/registrations/delete',
									{ _token: _token, id: id}
								).done(function(result){
									if(result.success)
									{
										alertify.alert('The user has been deleted successfully!');
										//reloading data table
										oTable.ajax.reload();
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
								});
							}
						});
					});
					$('#user-management_wrapper').on('click', '.record-view', function(e){
						var data = oTable.row( $(this).parents('tr') ).data();
						var id = data.id;
						var _token = data.token;
						var _super = $(this);
						
						$('#user-details-modal').modal({
							backdrop: 'static', keyboard: false
						}).load(app.Config.getScriptBaseUrl() + app.Config.adminPrefix + '/registrations/view/' + id);
					});			
					// end
				}catch(error){
					alertify.alert(error.message);
				}
				
			},
			
			init: function(){
				//executing default form validation pre-requisite
				//app.AdminDashboard.runDefaultValidation();
				//attaching profile form validation
				//this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
		},
	};
})(jQuery);