var Calendar = function (data) {
    //function to initiate Full CAlendar
    var runCalendar = function () {
        var $modal = $('#event-management');
		
		// modal manager spinner fix for bs3
		/* $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
		'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
			'<div class="progress progress-striped active">' +
				'<div class="progress-bar" style="width: 100%;"></div>' +
			'</div>' +
		'</div>';
		*/
        
		/* $('#event-categories div.event-category').each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 50 //  original position after the drag
            });
        }); */
        /* initialize the calendar
				 -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var calendar = $('#calendar').fullCalendar({
			theme: false,
			
            buttonIcons: {
                prev: 'left-single-arrow',
                next: 'right-single-arrow'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
			events: {
				url: '/get-shifts-front',
				type: 'POST',
				data: function(){
					return {
						sorting: $('#shifts_sorting').serialize()
					};
				},
			},
			loading: function(isLoading, view){
				if(isLoading)
					$('.manager-overlay').removeClass('hidden');
				else
					$('.manager-overlay').addClass('hidden');
			},
			eventRender: function(event, element) {
				var el = element.find('.fc-time');
				if(event.registration_status == 0)
					$('<span class="fc-shift-status-pending">&nbsp;&nbsp;</span>').insertBefore(el);
				else if(event.registration_status == 1)
					$('<span class="fc-shift-status-success">&nbsp;&nbsp;</span>').insertBefore(el);
				
				if(event.registration_status == -1 && event.availability == false)
					$('<span class="fc-shift-status-full">&nbsp;&nbsp;</span>').insertBefore(el);
			},
			dayRender: function(date, element, view){
				element.bind('dblclick', function() {
					$('#calendar').fullCalendar('gotoDate', date );
					$('#calendar').fullCalendar('changeView', 'agendaDay');
				});
			},
            editable: false,
            droppable: false,
            selectable: false,
            selectHelper: false,
            eventClick: function (calEvent, jsEvent, view) {
				var eventId = calEvent._id;
				$modal.modal({
					backdrop: 'static',
					keyboard: false,
					remote: '/front/shifts/ajax-show/' + eventId,
				});
            },
			eventMouseover: function(event, jsEvent, view){
				//console.log(event.vacancies);
				var content = '<div class="row">' +
					'<div class="col-sm-12"><strong>' + 
						event.start.format('MMMM Do YYYY, h:mm:ss a') + ' ' + 
						event.title +
					 '</strong></div>' +
				'</div>';
				if(event.vacancies){
					for(item in event.vacancies){
						content += '<div class="row">' +
							'<div class="col-xs-6">' + 
								event.vacancies[item]['name'] +
							'</div>'+
							'<div class="col-xs-1"><strong>' + 
								eval(event.vacancies[item]['qty'] - event.vacancies[item]['registered']) +
							'</strong></div>'+
							'<div class="col-xs-2">' + 
								'Available' +
							'</div>'+
						'</div>';
					}
				}
				//show pop-over for only previously added event 
				if($(this).hasClass('edit-event')){
					//console.log(content);
					$(this).popover({
						container: "body",
						title: "Availability",
						html: true,
						content: content,
						trigger: 'hover',
						placement: 'auto',
					});
					$(this).popover('show');
				}
			}
        });
    };
    return {
        init: function () {
            runCalendar();
        }
    };
}();