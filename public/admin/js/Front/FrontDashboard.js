var app = app || {};
(function($){
	'use strict';
	app.FrontDashboard = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Postal code should be like A1A 1A1");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Invalid Zip Code");
			
			$.validator.addMethod("recurrenceValid", function(value, element){
				if($('[name=recurrence] option:selected').length && 
					$('[name=recurrence] option:selected').val() != 'none'
				)
					return /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
				else
					return true;
			}, "Invalid recurrence validity date");
			
			$.validator.addMethod('endTimeNotBeforeStartTime', function(value, element){
							var startTime = $('#start_time').val().trim();
					
							var endTime = $('#end_time').val().trim();
						try{	
							if(endTime > startTime){
								return endTime > startTime;							
							}else{
								return false;
							}
						}catch(ex){
							alert(ex.message);
							return false;
						}		
							
					},'End Time should be greater than Start Time.');
				
		},
		
		PendingShift:{
			activateHandlers: function(){
				var _super = this;
							
				try{ 
					
					//Start pending shift data table function		
					var oTable = $('#pending-shift').dataTable({
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[1, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#pending-shift_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#pending-shift_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#pending-shift_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#pending-shift_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
				
				$('#dashboard-modal').on('click', '.cancel-registration', function(e){
					alert('here');
				});


				$('#pending-shift_wrapper').on('click', '.pendingShifts', function(e){
					var shiftId = $(this).data('shift-id') || 0;
					var shiftRegistrtationId = $(this).data('shift-registration-id') || 0;
						$('#dashboard-modal').modal({
							backdrop: 'static',
							keyboard: false,
							remote: app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-view-pending-shift/' +shiftId +'/'+shiftRegistrtationId,
						});			
				});
				
				$('body').on('hidden.bs.modal', '.modal', function () {
						$(this).removeData('bs.modal');
				});
				
				//cancel registration of pending shifts & confirmed shifts (both) within the same modal
				$('#dashboard-modal').on('click', '.pending-shifts-cancel-registration', function(e){
					var shiftRegistrationId = $(this).data('registration-id') || 0; 
					var shiftLang = $(this).data('shift-lang') || 0;
					alertify.set({ buttonFocus: "cancel" });

					alertify.confirm('Are you sure you want to cancel this '+shiftLang+' registration ?', function(e){

						if(e){
							window.location.href = app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/cancel-shift-registration/'+shiftRegistrationId;
						}
					});
				}); 
				
				$('#dashboard-modal').on('loaded.bs.modal', function(){
					$('.manager-overlay').addClass('hidden');
				});
				
				// end pending shift data table function
				
				//Start confirmed shift data table
				var oTable = $('#confirmed-shift').dataTable({
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[1, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#confirmed-shift_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#confirmed-shift_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#confirmed-shift_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#confirmed-shift_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
					$('.confirmedShifts').on('click', function(e){
					e.preventDefault();
					var shiftId = $(this).data('shift-id') || 0;
					var shiftRegistrtationId = $(this).data('shift-registration-id') || 0;
						$('#dashboard-modal').modal({
							backdrop: 'static',
							keyboard: false,
							remote: app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-view-confirmed-shift/' + shiftId +'/'+shiftRegistrtationId,
						});		
					});
					
					//Start actualhours shift data table
					var oTable = $('#actual-hours').dataTable({
						"aoColumns": [
							{ "bSortable": false },
							null,
							{ "bSortable": false },
							null,
							{ "bSortable": false },
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[1, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#actual-hours_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#actual-hours_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#actual-hours_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#actual-hours_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
					
					$('#actual-hours_wrapper').on('click', '.submit-hours', function(){
						var relatedForm = $(this).attr('rel');
						var form = $('#' + relatedForm);
						var action = form.attr('action'); 
						var _super = $(this);
						if(form.valid()){
							$.post(action, $('#' + relatedForm).serialize())
								.done(function(result){
									//JSON.parse(result);
									if(result.success){
										$(form).find('input').prop('disabled', true);
										_super.remove();
									}else if(result.error){
										alertify.alert(error.message);
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert(jqXHR.status + " - " + textStatus + " " + errorThrown);
								});
						}
					});	
					
					$('#actual-hours_wrapper').on('click', '.actual-hours-dashboard', function(e){
					var shiftId = $(this).data('shift-id') || 0;
					var shiftRegistrtationId = $(this).data('shift-registration-id') || 0;
						$('#dashboard-modal').modal({
							backdrop: 'static',
							keyboard: false,
							remote: app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-actual-hours-shift/' +shiftId +'/'+shiftRegistrtationId,
						});			
					});			
					
					
					var oTable = $('#actual-hours-history').dataTable({
						"aoColumns": [
							{ "bSortable": false },
							null,
							{ "bSortable": false },
							{ "bSortable": false },
							null,
							{ "bSortable": false },
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						 "aaSorting": [
							[1, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#actual-hours-history_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#actual-hours-history_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#actual-hours-history_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#actual-hours-history_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
				
				$('body').on('hidden.bs.modal', '.modal', function () {
						$(this).removeData('bs.modal');
				});
					
					$('#actual-hours-history_wrapper').on('click', '.actual-hours', function(e){
					var shiftId = $(this).data('shift-id') || 0;
					var shiftRegistrtationId = $(this).data('shift-registration-id') || 0;
						$('#dashboard-modal').modal({
							backdrop: 'static',
							keyboard: false,
							remote: app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-actual-hours-shift/' +shiftId +'/'+shiftRegistrtationId,
						});			
					});
					
					$('.enable-btn').on('click',function(){
							var relId = $(this).attr('rel') || 0;
							
							$('span[data-relation =' +  relId + ']').addClass('hidden'); 
							$('input[data-relation =' +  relId + ']').removeClass('hidden'); 
							$('form#' + relId).find('a.update-hours, a.update-cancel').removeClass('hidden');		
					});
				
					
					$('#actual-hours-history_wrapper').on('click', '.update-hours', function(){
						var relatedForm = $(this).attr('rel');
						var form = $('#' + relatedForm);
						var action = form.attr('action'); 
						var _super = $(this);
						
						if(form.valid()){
							
							$.post(action, $('#' + relatedForm).serialize())
								.done(function(result){
									//JSON.parse(result);
									if(result.success){
										alertify.alert('Record Updated Successfully');
										var value = form.find('input[id ^= hours]').val().trim();
										$('span[data-relation =' +  relatedForm + ']').text(value).removeClass('hidden'); 
										$('input[data-relation =' +  relatedForm + ']').addClass('hidden');
										$(form).find('a.update-hours, a.update-cancel').addClass('hidden');
										//_super.remove();
									}else if(result.error){
										alertify.alert(error.message);
									}
								}).error(function(jqXHR, textStatus, errorThrown){
									alertify.alert(jqXHR.status + " - " + textStatus + " " + errorThrown);
								});
						}
					});
					
					$('#actual-hours-history_wrapper').on('click', '.update-cancel', function(){
						var relId = $(this).attr('rel') || 0;
						
						$('span[data-relation =' +  relId + ']').removeClass('hidden'); 
						$('input[data-relation =' +  relId + ']').addClass('hidden'); 
						$('form#' + relId).find('a.update-hours, a.update-cancel').addClass('hidden');
					});
					
					$('#add-non-scheduled-hour').on('click', function(){
						$('#non-schedule-hours').modal({
							keyboard: false,
						});
					});
					
					$('#actual-hours-history_wrapper').on('click', '.edit-non-scheduled-hours', function(e){
					var actualHourId = $(this).data('actual-hours-id') || 0;
						$('#update-non-schedule-hours').modal({
							backdrop: 'static',
							keyboard: false,
							remote: app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-update-non-scheduled-hours/' +actualHourId,
						});			
					});
					
					$('#end_time').datetimepicker({
						format: 'HH:mm',
						useCurrent: false,
						widgetPositioning:{
							horizontal: 'left',
							vertical: 'bottom'
						},
						widgetParent: $('.end-datetime')
					});
					
					//end time widget
					$('#start_time').datetimepicker({
						format: 'HH:mm',
						useCurrent: false,
						widgetPositioning:{
							horizontal: 'left',
							vertical: 'bottom'
						},
						widgetParent: $('.start-datetime')
						
					});
					
					$('#start_date').datepicker({
						//startDate: new Date(),
						autoclose: true
					});
					
					
					$('#non-scheduled-hour-add').validate({
						
					});
					
					$('#non-scheduled-hour-edit').validate({
						
					});
					
					//handling dashboard recent activity notification read/unread
					$('ul.dash-board_notifacation a').on('click', function(){
						//prevent ajax being fired when an already read message is clicked
						if(!$(this).hasClass('unread'))
							return;
						
						var $that = $(this);
						var $notificationId = $that.data('notification-id') || 0;
						$.post(app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/users/read-notifications', 
								{id: $notificationId}
						).done(function(result){
							if(result.success !== undefined){
								//$that.removeClass('unread');
								$that.parent('li').removeClass('unread');
								$('[data-notification-id=' + $notificationId + ']').removeClass('unread');
								$('.noti-counter').text(result.success);
							}else if(result.error){
								alertify.alert(result.error)
							}else{}
						}).error(function(jqXHR, textStatus, errorThrown){
							alertify.alert(jqXHR.status + ' ' + textStatus + ' ' + errorThrown);
						});
					});
						
				}catch(error){
					alert(error.message);
				}
				
			},
			
			init: function(){
				//executing default form validation pre-requisite
				app.FrontDashboard.runDefaultValidation();
				//attaching profile form validation
				//this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
		},
	};
	$("#dashboard-notifaction-box-close").click(function(){
    	$(".dashboard-notifaction-box").hide();
	});
	$("#dashboard-notifaction-box-message-close").click(function(){
    	$("#dashboard-notifaction-box-message").hide();
	});
})(jQuery);