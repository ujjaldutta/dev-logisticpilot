var app = app || {};
(function($){
	'use strict';
	app.FrontShift = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
			$.validator.addMethod("ca_zip_validation", function(value, element) {
				return this.optional(element) || /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/.test(value);
			}, 	"Postal code should be like A1A 1A1");
			
			$.validator.addMethod("us_zip_validation", function(value, element) {
				return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
			}, 	"Invalid Zip Code");
			
			$.validator.addMethod("recurrenceValid", function(value, element){
				if($('[name=recurrence] option:selected').length && 
					$('[name=recurrence] option:selected').val() != 'none'
				)
					return /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
				else
					return true;
			}, "Invalid recurrence validity date");
		},
		
		ScheduleViewFlyer:{
			activateHandlers: function(){
				var _super = this;
				
				//code to select and deselect all items by one checkbox
				//required jquery iCheck plugin
				try{ 
					$('body').on('hidden.bs.modal', '.modal', function () {
						$(this).removeData('bs.modal');
						
						$('#event-management').find('.modal-content').html(
							'<div class="manager-overlay"><div class="loadtext"><img src="' + app.Config.getScriptBaseUrl() + 'images/preloader_white.gif' + '" alt="Loading..."></div></div>'
						);
					});
					
					//adding icheck support
					$('input.role-shift').iCheck({
						checkboxClass: 'icheckbox_minimal-grey',
						radioClass: 'iradio_minimal-grey',
						increaseArea: '10%' // optional
					});
					
					$('.role-shift').on('ifChecked', function(event){
						var availability = $(this).data('available-space') || 0;
						if(availability > 0)
							$('#register-shift').prop('disabled', false);
						else
							$('#register-shift').prop('disabled', true);
					});
					
					$('#register-shift').on('click', function(){
						var _super = $(this);
						var roleId = $('input.role-shift:checked').val();
						var shiftId = $('input.role-shift:checked').data('shift-id') || 0;
						var userId = $('input.role-shift:checked').data('user-id') || 0;
						var vacancy = $('input.role-shift:checked').data('vacancy') || 0
						
						//now registering
						$.post(app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-register' ,
							{
								shiftId: shiftId,
								userId: userId,
								roleId: roleId,
								vacancy: vacancy,
							}
						).done(function(result){
						if(result['success'] && result['registration']){
							//console.log(result['registration']);
							$('#registered_' + roleId).text(result['registration']);
							_super.addClass('hidden');
							$('#btn-close').removeClass('hidden');
							$('#event-management').modal('hide');
							 BootstrapDialog.show({
								title: 'Registration Successful',
								message: 'Thank You <br /> Your shift has been registered and is <strong>pending approval</strong>. You will be notified by email upon acceptance of this request or you can visit your <a href="' + app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/users/dashboard"> Dasboard </a> for the up-to-date shift information',
								buttons: [
									{
										label: 'Close',
										cssClass: 'btn-default',
										action: function(dialogItself){
											$('#calendar').fullCalendar('refetchEvents');
											dialogItself.close();
										},
									},
								],
							});
						}else if(result['error']){
							alertify.alert(result['error']);
							//console.log(result['error']);
						}else{}
						}).error(function(jqXHR, textStatus, errorThrown){
							 alert(textStatus + ' ' + jqXHR.status + ': ' + errorThrown);
							//_super.prop('disabled', true).text('Register');
						});
					});
					
					
					//code to handle view and print roasters
				$('#view-print-roaster').on('click', function(event){
					var shiftId = $(this).data('shift-id') || 0;
					var shiftName = $(this).data('shift-name') || 'Unknown';
					var shiftDate = $(this).data('shift-date') || 'Unknown';
					
					var date = moment(shiftDate); //constructing moment js functionlity for formatting
					//console.log(date.format('MMMM D, YYYY'));
					
					
						BootstrapDialog.show({
							title: "Roster information for '" + shiftName + "' on " + date.format('MMMM D, YYYY'),
							message: function(dialog) {
								var $message = $('<div></div>');
								var pageToLoad = dialog.getData('pageToLoad');
								$message.load(pageToLoad);
								return $message;
							},
							data: {
								'pageToLoad': app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-view-roasters/' + shiftId 
							},
							buttons: [
								{
									label: 'Pdf',
									icon: 'clip-file-pdf',
									cssClass: 'btn-default',
									action: function(dialogItself){
										//dialogItself.close();
										var win = window.open(app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/shifts/ajax-view-roasters/' + shiftId  + '/pdf', '', 'toolbar=no, resizable=no, width=' + screen.width-100 + ', height=' + screen.height-100);
									}
								},
								{
									label: 'Print',
									icon: 'fa fa-print',
									cssClass: 'btn-default',
									action: function(dialogItself){
										//dialogItself.close();
										//Print ele4 with custom options
										$("#roasters").print({
											//Use Global styles
											globalStyles : true,

											//Print in a hidden iframe
											iframe : true,

											//Don't print this
											noPrintSelector : ".actions",
										});
									}
								},
								{
									label: 'Close',
									cssClass: 'btn-default pull-right',
									action: function(dialogItself){
										dialogItself.close();
									},
								},
							],
						});
					});
					
					$('#event-management').on('loaded.bs.modal', function () {
						$('.manager-overlay').addClass('hidden');
					});
					
				}catch(error){
					alert(error.message);
				}
			},
			runValidator : function () {
				var form = $('#shift-front-register');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"role_id": {
							required: true
						},
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//executing default form validation pre-requisite
				app.FrontShift.runDefaultValidation();
				//attaching profile form validation
				this.runValidator();
				//activating all event listeners
				this.activateHandlers();
			}
		},
		
		drawLocationMap: {
			map: null,
			
			init: function(lat, lng){
				var _super = this;
				
				_super.map = new GMaps({
					div: '#shift-location-map',
					lat: lat,
					lng: lng
				});
				
				_super.map.setCenter(lat, lng);
				_super.map.addMarker({
					lat: lat,
					lng: lng
				});
				
				//assigning large Goggle map url
				var tmOut = setTimeout(function(){
					$('#map-url').attr('href', _super.map.map.mapUrl + '&q=' + lat + '+' + lng).attr('target', '_blank');
					$('#map-url').parent('p').removeClass('hidden');
				}, 4000);
				
				
			}
		},
		
		AddShift: {
			activateHandlers: function(){
				var _super = this;
				try{
					//initializing the calendar app included from form-calendar.js
					Calendar.init();
					
					$('.shift-category-item').on('ifUnchecked ifChecked', function(){
						//refetching calendar items when sorting parameters change.
						$('#calendar').fullCalendar('refetchEvents');
					});
					
				}catch(error){
					alert(error.message);
				}
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
			}
		}
	};
})(jQuery);