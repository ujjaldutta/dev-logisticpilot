var app = app || {};
(function($){
	'use strict';
	app.FrontSentMessages = {
		runDefaultValidation : function () {
			$.validator.setDefaults({
				errorElement: "span", // contain the error msg in a small tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
						error.insertAfter($(element).closest('.form-group').children('div').children().last());
					} else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
						error.appendTo($(element).closest('.form-group').children('div'));
					} else {
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					}
				},
				ignore: ':hidden',
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error');
				},
			});
		},
		
		SentItems: {
			activateHandlers: function(){
				var _super = this;
				
				
				try{
					
					/*$('#urlChange').change(function() {
     					window.location = $(this).val()
					});*/
					
					$('.message-delete').on('click', function(){
					var $msgId = $(this).data('id') || 0;
					
					try{
						alertify.set({ buttonFocus: "cancel" });
						alertify.confirm('Are you sure to delete this message ?', function(e){
							if(e){
							$('#form-message-delete-' + $msgId).submit();
							}
						});
					}catch(error){
					alert(error.message);
					}
				});
					
					
					
					$(document).ready(function(){
						
						$("#urlChange").change(function(){
						var orderValue = $(this).val();
						window.location.href = app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/messages/sentitems/'+orderValue;
						});
					});
					
					$(document).ready(function(){
						$('li.messages-item:first').addClass('current');
					});
					
					
				/*$('.important').on('click', function(event){
					var $this = $(this);
					var messageUserId = $(this).data('message-user-id') || 0;
					var status = $(this).data('important-status') || 0;
					$.post(
						app.Config.getScriptBaseUrl() + app.Config.frontPrefix + '/messages/updateimportantstatus',
						{ messageUserId: messageUserId, status: status}
					).done(function(result){
						if(result['status'] && result['status'] == 1){
							$this.css({color: 'blue'});
							$this.data('important-status', 0);
							$this.attr('title','Starred');
						}else if(result['status'] && result['status'] == 0){
							$this.css({color: 'black'});
							$this.data('important-status', 1);
							$this.attr('title','Masrk as Starred');
						}else{}
					}).error(function(jqXHR, textStatus, errorThrown){
						alertify.alert('Error: ' + jqXHR.status + " - " + errorThrown);
					});
				}); */
					
				}catch(error){
					alert(error.message);
				}
				
				
			},
			
			runSentItemsValidator : function () {
				var form = $('.message-form');
				var errorHandler = $('.errorHandler', form);
				form.validate({
					rules: {
						"subject": {
							required: true
						},
						"attachment":{
							required: true
						},
						"message":{
                         required: true                   
                  },
                    
					},
					submitHandler: function (form) {
						errorHandler.hide();
						form.submit();
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						errorHandler.show();
					}
				});
			},
			
			init: function(){
				//activating all event listeners
				this.activateHandlers();
				//executing default form validation pre-requisite
				app.FrontSentMessages.runDefaultValidation();
				//attaching profile form validation
				this.runSentItemsValidator();
			}
		
		}
	};
})(jQuery);