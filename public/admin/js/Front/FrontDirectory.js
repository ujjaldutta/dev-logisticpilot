var app = app || {};
(function($){
	'use strict';
	app.FrontDirectory = {
		activateHandlers: function(){
			var _super = this;
			
			//code to handle pagination limit
			$('#pagination-per_page').on('change', function(){
				var value = $(this).val();
				window.location.href= app.Config.getScriptBaseUrl() + 
					app.Config.frontPrefix + 
					'/users/list?limit=' + value;
			});
			
			$('#clear-search').on('click', function(){
				window.location.href = app.Config.getScriptBaseUrl() + 'front/users/list/';
			});
			
					//Start front user directory data table function		
					var oTable = $('#sample-table-1').dataTable({
						"aoColumns": [
							{ "bSortable": false },
							null,
							null,
							null,
							null,
						],
						"oLanguage": {
							"sLengthMenu": "Show _MENU_ Rows",
							"sSearch": "",
							"oPaginate": {
								"sPrevious": "",
								"sNext": ""
							}
						},
						//'oSearch' : false,
						//"bPaginate": false,
						 "aaSorting": [
							[1, 'asc']
						],
						"aLengthMenu": [
							[5, 10, 15, 20, -1],
							[5, 10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"iDisplayLength": 5,
						
					});
					$('#sample-table-1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
					// modify table search input
					$('#sample-table-1_wrapper .dataTables_length select').addClass("m-wrap small");
					// modify table per page dropdown
					$('#sample-table-1_wrapper .dataTables_length select').select2();
					// initialzie select2 dropdown
					 $('#sample-table-1_column_toggler input[type="checkbox"]').change(function () {
						// Get the DataTables object again - this is not a recreation, just a get of the object 
						var iCol = parseInt($(this).attr("data-column"));
						var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
						oTable.fnSetColumnVis(iCol, (bVis ? false : true));
					});
			
			//function to handle confirmation when soft deleting
			$('.user-delete').on('click', function(){
				var $userId = $(this).data('id') || 0;
				try{
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure?', function(e){
						if(e){
							$('#form-user-delete-' + $userId).submit();
						}
					});
				}catch(error){
					alert(error.message);
				}
			});
			
			//function to handle confirmation when restore
			$('.user-restore').on('click', function(){
				var $userId = $(this).data('id') || 0;
				try{
					alertify.set({ buttonFocus: "cancel" });
					alertify.confirm('Are you sure?', function(e){
						if(e){
							$('#form-user-restore-' + $userId).submit();
						}
					});
				}catch(error){
					alert(error.message);
				}
			});
		},
		
		init: function(){
			//activating all event listeners
			this.activateHandlers();
		}
	};
	
	//initializing app
	app.FrontDirectory.init();
	
})(jQuery);