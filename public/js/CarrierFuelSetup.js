require([
	'angular', 
	'angular_modules/CarrierFuelManagerApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CarrierFuelManagerApp']);
});