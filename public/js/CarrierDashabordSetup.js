require([
	'angular', 
	'angular_modules/CarrierDashabordSetupApp',
	'angular_modules/CommonApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CommonApp',  'customerDashabordSetupApp', ] );
});