require([
	'angular', 
	'angular_modules/UserManagerApp',
	'angular_modules/CommonApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['UserManagerApp', 'CommonApp']);
});