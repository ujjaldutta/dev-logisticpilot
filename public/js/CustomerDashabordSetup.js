require([
	'angular', 
	'angular_modules/CustomerDashabordSetupApp',
	'angular_modules/CommonApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CommonApp',  'CustomerDashabordSetupApp', ] );
});