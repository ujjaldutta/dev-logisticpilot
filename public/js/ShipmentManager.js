require(['angular', 'angular_modules/ShipmentManagerApp', 'angular_modules/CarrierManagerApp', 'angular_modules/CustomerManagerApp', 'angular_modules/CustomerLocationManagerApp'], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['ShipmentManagerApp', 'CarrierManagerApp', 'CustomerManagerApp', 'CustomerLocationManagerApp']);
});