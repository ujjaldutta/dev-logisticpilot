require.config({
	waitSeconds: 0, 
    baseUrl: '/js/lib',
    paths: {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        jquery: '../../bower_components/jquery/dist/jquery.min',
        
        "profile":"profile",
        "jquery-ui":"jquery-ui",
        "jquery.multiselect":"jquery.multiselect",
        "ion.rangeSlider":"ion.rangeSlider",
        "common-ui":"common-ui",
        'bootstrap-multiselect':'bootstrap-multiselect',
        "bootstrap-timepicker.min":"bootstrap-timepicker.min",
        
		bootstrap :  "../../bower_components/bootstrap/dist/js/bootstrap.min",
		"bootstrap-tooltip":"bootstrap-tooltip",
		angular:  "../../bower_components/angular/angular.min",
		"angular-ui-router": "../../bower_components/angular-ui-router/release/angular-ui-router.min",
		"angular-animate": "../../bower_components/angular-animate/angular-animate.min",
        "ng-file-upload": "../../bower_components/ng-file-upload/ng-file-upload.min",
		"angucomplete-alt": "angular_modules/angucomplete-alt", 
        "angular-jquery-timepicker": '../../bower_components/angular-jquery-timepicker/src/timepickerdirective',
		"angular-touch": "../../bower_components/angular-touch/angular-touch.min",
        "easyResponsiveTabs": "easyResponsiveTabs",
        "ng-tags-input": '../../bower_components/ng-tags-input/ng-tags-input.min',
        "angular-google-places-autocomplete": '../../bower_components/angular-google-places-autocomplete/dist/autocomplete.min',
        "angularjs-datepicker": '../../bower_components/angularjs-datepicker/dist/angular-datepicker.min',
        "angularjs-dropdown-multiselect": '../../bower_components/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min',
        "ui-bootstrap": 'ui-bootstrap-tpls-0.14.2.min',
        "rzslider": '../../bower_components/angularjs-slider/dist/rzslider.min',
		"ng-grid":'../../bower_components/ui-grid/ng-grid',
		"angular-endless-scroll.min":'../../bower_components/infinite-scroll/angular-endless-scroll.min',
		"csv":"http://ui-grid.info/docs/grunt-scripts/csv",
		"pdfmake":"http://ui-grid.info/docs/grunt-scripts/pdfmake",
		"vfsfonts":"http://ui-grid.info/docs/grunt-scripts/vfs_fonts",
		"uigrid":"../../bower_components/angular-ui-grid/ui-grid.min",
		//"checkList-autocomplete":"jquery.ui.checkList",
		
		
    },
	shim: {
			'bootstrap': {deps: ['jquery']},
			"angular-ui-router" : {deps: ['angular']},
			"angular-animate" : {deps: ['angular']},
            "ng-file-upload" : {deps: ['angular']},
			"angucomplete-alt": {deps: ['angular', /* 'angular-touch' */]},
            "easyResponsiveTabs": {deps: ['jquery']}, 
            "angular": {deps: ['easyResponsiveTabs', ]},
            "ng-tags-input": {deps: ['angular', ]},
            "angular-google-places-autocomplete": {deps: ['angular', ]},
            "angularjs-datepicker": {deps: ['angular', ]},
			"angularjs-dropdown-multiselect": {deps: ['angular', 'lodash.min']},
            "ui-bootstrap": {deps: ['angular']},
            "rzslider": {deps: ['angular']},
			"ng-grid": {deps: ['angular']},
			"angular-endless-scroll.min": {deps: ['angular']},
			
			"csv": {deps: ['angular']},
			"pdfmake": {deps: ['angular']},
			"vfsfonts": {deps: ['angular']},
			"uigrid": {deps: ['angular']},
			"angular-touch": {deps: ['angular']},
			"bootstrap-tooltip": {deps: ['jquery']},
			 
			 "jquery-ui": {deps: ['jquery']},
			 "jquery.multiselect": {deps: ['jquery']},
			 "bootstrap-timepicker.min": {deps: ['jquery']},
			 "ion.rangeSlider": {deps: ['jquery']},
			 "profile": {deps: ['jquery']},
			  "common-ui": {deps: ['jquery']}, 
			 "bootstrap-multiselect": {deps: ['jquery']}, 
			// "checkList-autocomplete": {deps: ['jquery-ui']}, 
	     
		}
});
