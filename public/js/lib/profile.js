(function ($) {
  "use strict";
 
		


$(window).load(function() {
	
	$( "#datepicker" ).datepicker({
		 onClose: function( selectedDate ) {
			$( "#datepicker3" ).datepicker( "option", "minDate", selectedDate );
		  }
      
      });
	$( "#datepicker3" ).datepicker({
		      onClose: function( selectedDate ) {
				$( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
			  }
		});
	
	
	$( "#datepicker2" ).datepicker();
		
	$("#datepicker4").datepicker();
	
	$("#datepicker5").datepicker({
		 onClose: function( selectedDate ) {
			$( "#datepicker6" ).datepicker( "option", "minDate", selectedDate );
		  }
      
      });
	$("#datepicker6").datepicker({
		      onClose: function( selectedDate ) {
				$( "#datepicker5" ).datepicker( "option", "maxDate", selectedDate );
			  }
		});
  
  $("#datepickerassoc").datepicker({
		onClose: function( selectedDate ) {
			$( "#datepickerassoc1" ).datepicker( "option", "minDate", selectedDate );
		  }
	  });
	$("#datepickerassoc1").datepicker({
		onClose: function( selectedDate ) {
			$( "#datepickerassoc" ).datepicker( "option", "minDate", selectedDate );
		  }
		});

$("#datepickerfuel").datepicker({
		onClose: function( selectedDate ) {
			$( "#datepickerfuel1" ).datepicker( "option", "minDate", selectedDate );
		  }
	  });
	$("#datepickerfuel1").datepicker({
		onClose: function( selectedDate ) {
			$( "#datepickerfuel" ).datepicker( "option", "minDate", selectedDate );
		  }
		});		
		

$('[data-toggle="popover"]').popover({
			placement : 'top',
			trigger : 'hover'
		});
	
	$( ".tooltip_cont a" ).mouseover(
		function() {
			$( this ).parent().find( ".tooltip_div" ).show();
		}
	);
	$( ".tooltip_cont a" ).mouseout(
		function() {
			$( this ).parent().find( ".tooltip_div" ).hide();
		}
	);
	
	
$('#horizontalTab1, #assocTab1,#horizontalMarkupTab1,#ParentTab,#ChildTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        },
        tabidentify: 'tab_identifier_child',
    });
    
 	
 
    	
$("ul.nav-tabs a").click(function (e) {
	  	e.preventDefault();  
		$(this).tab('show');
	});	

	
	$(document).mouseup(function (e)
{
			var container = $(".mainprofile");

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				jQuery(".mainprofile").removeClass("profilemenu");
			}
		});	
		

	$(".t-infolnk").hover(function(){
				$(this).next(".t-info").slideToggle("slow");
				});	
	$(".add-assulnk").hover(function(){
		$(this).next(".add-assu").slideDown("slow");
		});
	$(".assu-close").click(function(){
		$(".add-assu").hide();
		});
						
						
});	


$(document).on('click',".client-list",function(){
						;
		$(this).next('.client-listdrop').slideToggle("slow");
								   
	});
	
$(document).on('click',".laneroute",function(){
						;
		$(this).next('.lanerange').slideToggle("slow");
								   
	});

function doStuff(){
    var landscape = window.orientation? window.orientation=='landscape' : true;
	var portrate = window.orientation? window.orientation=='portrate' : true;
	
	if(landscape && window.innerWidth > 767){
		$("div.hover_div .text").mouseenter(function () {
			$(this).parent().find(".ship_cost").show();
		});
	}
	if(portrate && window.innerWidth > 767){
		$("div.hover_div .text").mouseleave(function () {
			$(this).parent().find(".ship_cost").hide();
		});
	}
}
window.onload=window.onresize=doStuff;
if(window.onorientationchange){
    window.onorientationchange=doStuff;
}


		
})(jQuery);
