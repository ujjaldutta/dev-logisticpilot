'use strict';
function User(UserAPIEndpoint, $http, $q){
	this.UserAPIEndpoint = UserAPIEndpoint;
	this.http = $http;
	this.q = $q;

	this.makeAsyncRequest = function(method){
		try{
			//console.log(typeof method);
			if(!method instanceof  Object)
				throw new Error('Invalid method given');

			var defer = this.q.defer();
			defer.resolve(method);
			
			return defer.promise;
			
		}catch(ex){
			console.log(ex.message);
		}
	}

	this.processGoogleLocation = function(locationObj){
		var processedLocation = {};
		if(locationObj && locationObj.address_components && locationObj.address_components.length){
			for(var i = 0; i < locationObj.address_components.length; i ++){
				if(locationObj.address_components[i].types[0] !== undefined && 
					locationObj.address_components[i].types[0])
				{
					var addressType = locationObj.address_components[i].types[0];
					switch(addressType){
						case 'postal_code':
							processedLocation.postal_code = locationObj.address_components[i].long_name !== undefined ? 
								locationObj.address_components[i].long_name : null;
							break;
						case 'locality':
							processedLocation.city = locationObj.address_components[i].long_name !== undefined ? 
									locationObj.address_components[i].long_name : null;
							break;
						case 'administrative_area_level_1':
							processedLocation.state = locationObj.address_components[i].short_name !== undefined ? 
										locationObj.address_components[i].short_name : null;
							break;
						case 'country':
							processedLocation.country = locationObj.address_components[i].short_name !== undefined ? 
										locationObj.address_components[i].short_name : null;
							break;
					}
				}
			}
		}

		return processedLocation;
	}
}

User.prototype.listUser = function(params){
	try{
		//console.log(params);
		var url = __buildUserUrl(params, this.UserAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.sendPassword = function(email,password){
	try{	
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.sendPassword;
		//alert(url);
		//return false;
		return this.http.post(url, {email: email, password: password})
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.sendUpdatePassword = function(email,userId){
	try{	
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.sendUpdatePassword;		
		return this.http.post(url, {email: email, userId: userId})
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getUserData = function(userID){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.edit + 'userID=' + userID;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.syncClientMapping = function(clientList, userID, all){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.clientMapping + 'userID=' + userID + '&all=' + all;
		var payload = {clientList: clientList};
		
		//if all customer requested then send the special param
		if(all !== undefined && all === true)
			payload = angular.extend({all: true}, payload);
		
		return this.http.post(url, payload)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.getClientMapping = function(userID){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.clientMapping + 'userID=' + userID;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.detachClientMapping = function(clientID, userID){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.clientMapping + 'userID=' + userID + '&clientID=' + clientID;
		
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.detachLayoutMapping = function(layoutTypeId, userId){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.allLayoutMappings + 'userId=' + userId + '&layoutTypeId=' + layoutTypeId;
		
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

User.prototype.deleteUser = function(userID){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.deleteUrl + 'userID=' + userID;
		
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}	
}

User.prototype.saveUser = function(custID, userID, data){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.edit;
		
		return this.http.post(url, angular.extend({clientID: custID, userID: userID}, data))
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}	
}

User.prototype.formatValidationErrors = function(msgObj){
	if(msgObj instanceof Object){
		var messages = [];
		for(var key in msgObj){
			messages.push(msgObj[key]);
		}
		return messages.join(',');	
	}else
		return msgObj;
}

User.prototype.getuserTypes = function(){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.userTypes;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getAllLayoutTypes = function(){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.layoutTypes;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getDisplayAttributes = function(layoutId){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.layoutFields + 'layoutID=' + layoutId;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getAllLayoutMappings = function(userId){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.allLayoutMappings + 'userId=' + userId;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.loadAttributes = function(userId, layoutId){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.savedLayouts + 'layoutID=' + layoutId + '&userId=' + userId;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.saveLayoutAttributeAssignment = function(userId, layoutId, data){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.saveLayoutAssignment + 'userId=' + userId + '&layoutId=' + layoutId;
		return this.http.post(url, data)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getConstraintOperators = function(){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.constraintOperatos;
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.getAllScreenFilters = function(userId){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.screenFilter + 'userId=' + userId;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.saveScreenFilter = function(data){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.screenFilter;
		return this.http.post(url, data)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

User.prototype.detachScreenFilter = function(layoutId, id){
	try{
		var url = this.UserAPIEndpoint.base +  this.UserAPIEndpoint.screenFilter + 'layoutId=' + layoutId;

		if(id !== undefined)
			url += '&id=' + id;
		
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildUserUrl(params, apiBaseUrl){

	var clientID = 0, filters = {}, sorting = [], limit = 20, page = 1, orderBy= '', orderType='', clientId = '';
	
	if(typeof params === 'object'){
		if(params.filters)
			filters = params.filters;
		if(params.limit)
			limit = params.limit;
		if(params.sorting)
			sorting = params.sorting;
		if(params.page)
			page = params.page;
		if(params.clientID)
			clientID = params.clientID
		if(params.orderBy)
			orderBy = params.orderBy;
		if(params.orderType)
			orderType = params.orderType;
		if(params.clientId)
			clientId = params.clientId;
	}

	//console.log(params);
	
	return apiBaseUrl.base +  apiBaseUrl.list + 'clientID=' + clientID + '&filters=' 
		+ angular.toJson(filters, true) + '&limit=' + limit + '&page=' + page 
		+ '&orderBy=' + orderBy + '&orderType=' + orderType + '&clientID=' + clientId;
}