'use strict';
function Shipment(ShipmentAPIEndpoint, $http, $q){
	this.ShipmentAPIEndpoint = ShipmentAPIEndpoint;
	this.http = $http;
	this.q = $q;

	this.makeAsyncRequest = function(method){
		try{
			//console.log(typeof method);
			if(!method instanceof  Object)
				throw new Error('Invalid method given');

			var defer = this.q.defer();
			defer.resolve(method);
			
			return defer.promise;
			
		}catch(ex){
			console.log(ex.message);
		}
	}

	this.processGoogleLocation = function(locationObj){
		var processedLocation = {};
		if(locationObj && locationObj.address_components && locationObj.address_components.length){
			for(var i = 0; i < locationObj.address_components.length; i ++){
				if(locationObj.address_components[i].types[0] !== undefined && 
					locationObj.address_components[i].types[0])
				{
					var addressType = locationObj.address_components[i].types[0];
					switch(addressType){
						case 'postal_code':
							processedLocation.postal_code = locationObj.address_components[i].long_name !== undefined ? 
								locationObj.address_components[i].long_name : null;
							break;
						case 'locality':
							processedLocation.city = locationObj.address_components[i].long_name !== undefined ? 
									locationObj.address_components[i].long_name : null;
							break;
						case 'administrative_area_level_1':
							processedLocation.state = locationObj.address_components[i].short_name !== undefined ? 
										locationObj.address_components[i].short_name : null;
							break;
						case 'country':
							processedLocation.country = locationObj.address_components[i].short_name !== undefined ? 
										locationObj.address_components[i].short_name : null;
							break;
					}
				}
			}
		}

		return processedLocation;
	}
}

Shipment.prototype.getShipmentDataStatus = function(status){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.edit + 'status=' + status;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Shipment.prototype.listShipment = function(params){
	try{
		var url = __buildUrl(params, this.ShipmentAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Shipment.prototype.sendPassword = function(email,password){
	try{	
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.sendPassword;
		//alert(url);
		return this.http.post(url, {email: email, password: password})
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.sendUpdatePassword = function(email,custId){
	try{	
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.sendUpdatePassword;
		//alert(url);
		return this.http.post(url, {email: email, custId: custId})
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getShipmentData = function(custID){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.edit + 'custID=' + custID;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Shipment.prototype.getShipmentDefaultSettings = function(custID){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.defaultSettings + 'custID=' + custID;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Shipment.prototype.postShipmentData = function(custID, data){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.edit + 'custID=' + custID;
		
		return this.http.post(url, data)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Shipment.prototype.postShipmentDefaultSettings = function(custID, data){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.defaultSettings + 'custID=' + custID;
		
		return this.http.post(url, data)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}	
}

Shipment.prototype.deleteShipment = function(custID){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.deleteUrl + 'custID=' + custID;
		
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}	
}

Shipment.prototype.createUser = function(custID, data){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.addUser;
		
		return this.http.post(url, angular.extend({clientID: custID}, data))
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}	
}

Shipment.prototype.formatValidationErrors = function(msgObj){
	if(msgObj instanceof Object){
		var messages = [];
		for(var key in msgObj){
			messages.push(msgObj[key]);
		}
		return messages.join(',');	
	}else
		return msgObj;
}

Shipment.prototype.getAllThemes = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.themes;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllLanguages = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.languages;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllCommodities = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.commodity;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllClasses = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.classes;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllShipmentTerms = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.shipment;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getBolTemplates = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.bolTemp;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getInvoiceTerms = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.invoiceTerms;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllInvoiceDays = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.invoiceDays;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

Shipment.prototype.getAllInvoiceTemplates = function(){
	try{
		var url = this.ShipmentAPIEndpoint.base +  this.ShipmentAPIEndpoint.invoiceTemp;
		
		return this.http.get(url)
			.error(function(data, status, config, headers){
				console.warn(status);
			});
	}catch(ex){
		console.log(ex.message);
	}
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildUrl(params, apiBaseUrl){

	var filters = {}, sorting = [], limit = 20, page = 1, orderBy= '', orderType='';
	
	if(typeof params === 'object'){
		if(params.filters)
			filters = params.filters;
		if(params.limit)
			limit = params.limit;
		if(params.sorting)
			sorting = params.sorting;
		if(params.page)
			page = params.page;
		if(params.orderBy)
			orderBy = params.orderBy;
		if(params.orderType)
			orderType = params.orderType;

	}
	return apiBaseUrl.base +  apiBaseUrl.list + 'filters=' + angular.toJson(filters, true) + 
		'&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType;
}