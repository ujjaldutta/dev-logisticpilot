'use strict';
function Carrier(CarrierAPIEndpoint, $http, $q) {
    this.CarrierAPIEndpoint = CarrierAPIEndpoint;
    this.http = $http;
    this.q = $q;

    this.makeAsyncRequest = function (method) {
        try {
            //console.log(typeof method);
            if (!method instanceof  Object)
                throw new Error('Invalid method given');

            var defer = this.q.defer();
            defer.resolve(method);

            return defer.promise;

        } catch (ex) {
            console.log(ex.message);
        }
    }

    this.processGoogleLocation = function (locationObj) {
        var processedLocation = {};
        if (locationObj && locationObj.address_components && locationObj.address_components.length) {
            for (var i = 0; i < locationObj.address_components.length; i++) {
                if (locationObj.address_components[i].types[0] !== undefined &&
                        locationObj.address_components[i].types[0])
                {
                    var addressType = locationObj.address_components[i].types[0];
                    switch (addressType) {
                        case 'postal_code':
                            processedLocation.postal_code = locationObj.address_components[i].long_name !== undefined ?
                                    locationObj.address_components[i].long_name : null;
                            break;
                        case 'locality':
                            processedLocation.city = locationObj.address_components[i].long_name !== undefined ?
                                    locationObj.address_components[i].long_name : null;
                            break;
                        case 'administrative_area_level_1':
                            processedLocation.state = locationObj.address_components[i].short_name !== undefined ?
                                    locationObj.address_components[i].short_name : null;
                            break;
                        case 'country':
                            processedLocation.country = locationObj.address_components[i].short_name !== undefined ?
                                    locationObj.address_components[i].short_name : null;
                            break;
                    }
                }
            }
        }

        return processedLocation;
    }
    
    this.processedComboBox = function (itemObj) {
        var processedComboBox = {};       
        
        if (itemObj) {
            processedComboBox = itemObj['id'];
        }

        return processedComboBox;
    }
}

Carrier.prototype.listCarrier = function (params) {
    try {
        var url = __buildUrl(params, this.CarrierAPIEndpoint);

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.getCarrierData = function (carrID) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.edit + 'carrID=' + carrID;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.getCarrierInsurances = function(carrID, params){
	try{
		var url = this.CarrierAPIEndpoint.base +  this.CarrierAPIEndpoint.carrierInsurances + 'carrID=' + carrID;

        url = _appendQueryParams(url, params);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

Carrier.prototype.postCarrierData = function (carrID, data) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.edit + 'carrID=' + carrID;

        return this.http.post(url, data)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.deleteCarrier = function (carrID) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.deleteUrl + 'carrID=' + carrID;

        return this.http.delete(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.formatValidationErrors = function (msgObj) {
    if (msgObj instanceof Object) {
        var messages = [];
        for (var key in msgObj) {
            messages.push(msgObj[key]);
        }
        return messages.join(',');
    } else
        return msgObj;
}

Carrier.prototype.getAllInsurancesTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.insuranceTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.saveInsurances = function (carrID, data) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.saveInsurances + 'carrID=' + carrID;

        return this.http.post(url, data)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllCarrierTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.carrierTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllCurrencyTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.currencyTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllModesTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.modeTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.saveModes = function (carrID, data) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.saveModes + 'carrID=' + carrID;

        return this.http.post(url, data)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getCarrierModes = function(carrID, params){
	try{
		var url = this.CarrierAPIEndpoint.base +  this.CarrierAPIEndpoint.carrierModes + 'carrID=' + carrID;

        url = _appendQueryParams(url, params);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}	
}

Carrier.getAllFields = function() {
    try {
		var url = this.CarrierAPIEndpoint.base +  this.CarrierAPIEndpoint.carrierAllFields;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

Carrier.prototype.listEquipment = function (carrID, params) {
    try {
        var url = __buildUrlEquipment(params, carrID, this.CarrierAPIEndpoint);

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.deleteEquipment = function (equipId) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.deleteEquipUrl + 'equipID=' + equipId;

        return this.http.delete(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getEquipmentData = function (equipID) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.equipmentEdit + 'equipID=' + equipID;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.getAllEquipmentTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.equipmentTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllOwnerTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.ownerTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.postEquipmentData = function (equipID, carrierId, data) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.equipmentEdit + 'equipID=' + equipID + '&carrierId=' + carrierId;

        return this.http.post(url, data)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.getAllEquipTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.equipTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllInsTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.insTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getAllModTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.modTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.listRemit = function (carrID, params) {
    try {
        var url = __buildUrlRemit(params, carrID, this.CarrierAPIEndpoint);

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.deleteRemit = function (remitId) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.deleteRemitUrl + 'remitID=' + remitId;

        return this.http.delete(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

Carrier.prototype.getRemitData = function (remitID) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.remitEdit + 'remitID=' + remitID;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.postRemitData = function (remitID, carrierId, data) {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.remitEdit + 'remitID=' + remitID + '&carrierId=' + carrierId;

        return this.http.post(url, data)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

Carrier.prototype.getAllCurrTypes = function () {
    try {
        var url = this.CarrierAPIEndpoint.base + this.CarrierAPIEndpoint.currTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}


//prtivate method to handle filter params
function __buildUrl(params, apiBaseUrl) {

    var filters = {}, sorting = [], limit = 20, page = 1, orderBy = '', orderType = '';

    if (typeof params === 'object') {
        if (params.filters)
            filters = params.filters;
        if (params.limit)
            limit = params.limit;
        if (params.sorting)
            sorting = params.sorting;
        if (params.page)
            page = params.page;
        if (params.orderBy)
            orderBy = params.orderBy;
        if (params.orderType)
            orderType = params.orderType;
    }

    return apiBaseUrl.base + apiBaseUrl.list + 'filters=' + angular.toJson(filters, true) + '&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType;
}

//prtivate method to handle filter params
function __buildUrlEquipment(params, carrID, apiBaseUrl) {
    var sorting = [], limit = 20, page = 1, orderBy = '', orderType = '', groupBy = null;

    if (params && typeof params === 'object') {
        if (params.limit)
            limit = params.limit;
        if (params.sorting)
            sorting = params.sorting;
        if (params.page)
            page = params.page;
        if (params.orderBy)
            orderBy = params.orderBy;
        if (params.orderType)
            orderType = params.orderType;
        if (params.groupBy)
            groupBy = params.groupBy;
    }

    return apiBaseUrl.base + apiBaseUrl.listEquipments + 'carrID=' + carrID + '&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType + '&groupBy=' + groupBy;
}

function __buildUrlRemit(params, carrID, apiBaseUrl) {
    var sorting = [], limit = 20, page = 1, orderBy = '', orderType = '', groupBy = null;

    if (params && typeof params === 'object') {
        if (params.limit)
            limit = params.limit;
        if (params.sorting)
            sorting = params.sorting;
        if (params.page)
            page = params.page;
        if (params.orderBy)
            orderBy = params.orderBy;
        if (params.orderType)
            orderType = params.orderType;
        if (params.groupBy)
            groupBy = params.groupBy;
    }

    return apiBaseUrl.base + apiBaseUrl.listRemits + 'carrID=' + carrID + '&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType + '&groupBy=' + groupBy;
}

function _appendQueryParams(url, params){
    var limit = 20, page = 1, orderBy = '', orderType = '', groupBy = '';

    if (params && typeof params === 'object') {
        if (params.limit)
            limit = params.limit;
        if (params.sorting)
            sorting = params.sorting;
        if (params.page)
            page = params.page;
        if (params.orderBy)
            orderBy = params.orderBy;
        if (params.orderType)
            orderType = params.orderType;
        if (params.groupBy)
            groupBy = params.groupBy;
    }
    return url + '&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType + '&groupBy=' + groupBy;
}
