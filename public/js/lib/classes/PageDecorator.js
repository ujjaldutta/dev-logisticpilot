//constructor
function PageDecorator($){
	this.$ = $;
}
//prototyped loadPage method
PageDecorator.prototype.loadPage = function(){
	var $ = this.$;
	$(document).ready(function() {
		$(".loader").delay(300).fadeOut();
		$(".animationload").delay(600).fadeOut("slow");

		$(".toggle-menu").on("click", function() {
			$(".body-container").toggleClass("nav-small")
		});
		
		$('.main-menu .open_dashboard').click(function(e){
			e.preventDefault();
			$('.dashboard_top').slideToggle();
			$(this).toggleClass('open');
		});
	});
}

PageDecorator.prototype.headerFix = function(obj){
	var $ = this.$;
	
	obj.affix({
		offset: {
			top: 100, 
			bottom: function () {
			return (this.bottom = $('#copyrights').outerHeight(true))
			}
		}
	});
}

PageDecorator.prototype.gotoTop = function(obj){
	var $ = this.$;
	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 1) {
			obj.css({bottom:"25px"});
		} else {
			obj.css({bottom:"-100px"});
		}
	});
	
	obj.click(function(){
		$('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});
}

PageDecorator.prototype.autoHide = function(selector, timeOut, doMakeAngularCompatible){
	if(isNaN(timeOut))
		throw new Error('Not a valid timeout');
	
	var $ = this.$;
	setTimeout(function(){
		$(selector).length ?
			$(selector).fadeOut(800, function(){
				if(typeof doMakeAngularCompatible !== undefined && doMakeAngularCompatible)
					$(selector).addClass('ng-hide').removeAttr('style');
			}) :
			void(0);

			
	}, timeOut);
}

PageDecorator.prototype.themeSelecter = function(){
	var $ = this.$;
	$('#default_theme').click(function(){
		$('link[rel=stylesheet][href~="css/style-green.css"]').remove();
		$('link[rel=stylesheet][href~="css/style-orange.css"]').remove();
		$('link[rel=stylesheet][href~="css/style-red.css"]').remove();
	});
	$('#green_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-green.css"
		});
	});
	$('#orange_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-orange.css"
		});
	});
	$('#red_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-red.css"
		});
	});
}

PageDecorator.prototype.activateBSTooltip = function(){
	var $ = this.$;
	try{
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();
	}catch(ex){
		alert(ex.message);
	}
}


PageDecorator.prototype.runMagicMenuLine = function(){
	var $ = this.$;
	try{
		$(window).load(function(){
			//Magic arrow markup
			var $el, leftPos, newWidth;
			/* Add Magic Line markup via JavaScript, because it ain't gonna work without */
			$("#magicArrow").append("<li id='magic-line'></li>");
    
			/* Cache it */
			var $magicLine = $("#magic-line");
    
			$magicLine
				.width($(".current_page_item").width())
			   // .css("left", $(".current_page_item a").position().left)
				.data("origLeft", $magicLine.position().left)
				.data("origWidth", $magicLine.width());
				
				$("#magicArrow > li.dropdown > a").click(function() {
				 $('.current_page_item').removeClass('current_page_item');
					$(this).parent('li').addClass('current_page_item');
					$el = $(this);
					leftPos = $el.position().left;
					newWidth = $el.parent().width();
					
					$magicLine.stop().animate({
						left: leftPos,
						width: newWidth
					});
				});
		});
	}catch(ex){
		alert(ex.message);
	}
}

/*PageDecorator.prototype.activateEasyTab = function(idName, like){
	var $ = this.$;
	var elm = null;
	
	if(like !== undefined && like === true)
		elm = $('[id ^= ' + idName + ']');
	else
		elm = $('#' + idName);
	try{
		$(window).on('document', 'load', function(){
			console.log(elm);
			elm.easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion           
			width: 'auto', //auto or any width like 600px
			fit: true,   // 100% fit in a container
			closed: 'accordion', // Start closed if in accordion view
			activate: function(event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
    }catch(ex){
    	alert(ex.message);
    }
}*/