function CarrierApiService(carrierStatusAPIEndpoint, SyncCarriers, $http){
	
	
	this.fecthSelectedCarriers = function(withDetails, searchString, onlyApiEnabled, limit, clientId,ProfileId){
		return SyncCarriers.fetch(withDetails, searchString, onlyApiEnabled, limit, clientId,ProfileId);
	};
	
	//ujjal : fetch unassigned carrier
	this.getUnassignedCarriers = function(withDetails, searchString, onlyApiEnabled, limit, clientID){
		console.log(searchString);
		var url = '/front/client-register?action=getUnassignedCarriers';
					url = (typeof withDetails !== undefined &&  withDetails == true) ? url + '&withCarrierSetup=true' : url;
					url = (typeof searchString !== undefined) ? url + '&q='+ searchString : url;
					url = (typeof onlyApiEnabled !== undefined &&  onlyApiEnabled == true) ? url + '&apiEnabled=true' : url;
					url = (typeof  limit!== undefined &&  !isNaN(limit) && limit) ? url + '&limit=' + limit : url;
					url = (typeof clientID !== undefined &&  !isNaN(clientID) && clientID) ? url + '&clientID=' + clientID : url;
					
		return $http.get(url)
		.success(function(result){
							return result;
						})
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	

	this.getCarrierApiDetails = function(carrierId){
		return $http.get('/front/client-register?action=getCarrierApiDetails&carrierId=' + carrierId)
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	
	//@param int carrierId
	//@param optional object filter
	this.getCarrierAccTariffDetails = function(carrierId, filter){
		var url = '/front/client-register?action=getCarrierAccTariffDetails&carrierId=' + carrierId;
		
		url = _handleCarrierFilterParams(url, filter);

		return $http.get(url)
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	
	//@param int carrierId
	//@param optional object filter
	this.getCarrierFuelTariffDetails = function(carrierId, filter){
		var url = '/front/client-register?action=getCarrierFuelTariffDetails&carrierId=' + carrierId;
		
		url = _handleCarrierFilterParams(url, filter);
		
		return $http.get(url)
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	this.postCarrierAccTariffCustomDetails = function(data){
		return $http.post('/front/client-register?action=postCarrierAccTariffCustomDetails', data)
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	this.deleteCarrierAccTariffCustomDetails = function(id){
		return $http.delete('/front/client-register/' + id + '?type=deleteCarrierAccTariffCustomDetails')
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	this.postCarrierFuelTariffCustomDetails = function(data){
		return $http.post('/front/client-register?action=postCarrierFuelTariffCustomDetails', data)
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	this.deleteCarrierFuelTariffCustomDetails = function(id){
		return $http.delete('/front/client-register/' + id + '?type=deleteCarrierFuelTariffCustomDetails')
			.error(function(data, status, config, headers){
				alert(status);
			});
	};
	this.checkApiStatus = function(username, password, account, scac){
		return $http.jsonp(carrierStatusAPIEndpoint + 'callback=JSON_CALLBACK&username=' + username
				+ '&password=' + password + '&accountnumber=' + account + '&scaccode=' + scac
			) 
			.error(function(data, status, config, headers){
				alert(status);
			});
	};

	this.postApiDetails = function(dataObj){
		return $http.post('/front/client-register', dataObj)
			.error(function(data, status, config, headers){
				console.log(status);
			});
	}
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function _handleCarrierFilterParams(url, filter){
	if(filter !== undefined){
		var filterParams = '';
		
		for(var key in filter){
			filterParams += 'filter[' + key + ']=' + filter[key] + '&';
		}
		
		if(filterParams.length){
			filterParams = filterParams.substring(0, filterParams.length -1);
			return url += '&' + filterParams;
		}else
			return url;
	}else
		return url;
}
