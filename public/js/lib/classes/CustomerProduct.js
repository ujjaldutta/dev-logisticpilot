'use strict';
function CustomerProduct(CustomerProductAPIEndpoint, $http, $q, GoogleLocationFactory) {
    this.CustomerProductAPIEndpoint = CustomerProductAPIEndpoint;
    this.http = $http;
    this.q = $q;
    this.GoogleLocationFactory = GoogleLocationFactory;

    this.makeAsyncRequest = function (method) {
        try {
            //console.log(typeof method);
            if (!method instanceof  Object)
                throw new Error('Invalid method given');

            var defer = this.q.defer();
            defer.resolve(method);

            return defer.promise;

        } catch (ex) {
            console.log(ex.message);
        }
    }

    this.processGoogleLocation = function (locationObj) {
        return this.GoogleLocationFactory.processGoogleLocation(locationObj);
    }

    this.fillGoogleLocation = function (locationObj) {
        return this.GoogleLocationFactory.fillLocationDetails(locationObj, 'location');
    }

    this.processedComboBox = function (itemObj) {
        var processedComboBox = {};

        if (itemObj) {
            processedComboBox = itemObj['id'];
        }

        return processedComboBox;
    }
}

CustomerProduct.prototype.listProducts = function (params) {
    try {
        var url = __buildProductUrl(params, this.CustomerProductAPIEndpoint);

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getWeightTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.weightTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getCommodityTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.commodityTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getDimmensionsTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.dimmensionsTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getTemperatureTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.temperatureTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getFreeClassTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.freeClassTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getClassTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.classTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getPackageTypes = function () {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.packageTypes;

        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }

}

CustomerProduct.prototype.getCustomerData = function (id) {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.edit + 'id=' + id;
        return this.http.get(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

CustomerProduct.prototype.postProductData = function (clientId, payload) {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.edit;
        return this.http.post(url, angular.extend({clientId: clientId}, payload))
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

CustomerProduct.prototype.deleteProduct = function (id) {
    try {
        var url = this.CustomerProductAPIEndpoint.base + this.CustomerProductAPIEndpoint.deleteUrl + 'id=' + id;
        ;
        return this.http.delete(url)
                .error(function (data, status, config, headers) {
                    console.warn(status);
                });
    } catch (ex) {
        console.log(ex.message);
    }
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildProductUrl(params, apiBaseUrl) {

    var filters = {}, sorting = [], limit = 20, page = 1, orderBy = '', orderType = '', clientId = '';

    if (typeof params === 'object') {
        if (params.filters)
            filters = params.filters;
        if (params.limit)
            limit = params.limit;
        if (params.sorting)
            sorting = params.sorting;
        if (params.page)
            page = params.page;
        if (params.orderBy)
            orderBy = params.orderBy;
        if (params.orderType)
            orderType = params.orderType;
        if(params.clientId)
            clientId = params.clientId;

    }
    return apiBaseUrl.base + apiBaseUrl.list + 'filters=' + angular.toJson(filters, true) +
            '&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType + '&clientID=' + clientId;
}