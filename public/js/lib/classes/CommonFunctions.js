'use strict';

function CommonFunctions(){
 var factory = {};
 
 factory.getRandNumber = function(){
 return this.randomNumber = Math.floor(Math.random()*90000) + 10000;
}

factory.tConvert = function(time){
 	  // Check correct time format and split into components
	  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

	  if (time.length > 1) { // If time format correct
		time = time.slice (1);  // Remove full string match value
		time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
		time[0] = +time[0] % 12 || 12; // Adjust hours
	  }
	  return this.convertedTime = time.join (''); // return adjusted time or original string
}

factory.dformat = function(inputDate){
		var date = new Date(inputDate);
		if (!isNaN(date.getTime())) {
			// Months use 0 index.
			return this.convertedDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
		}
		
}

return factory;
}

