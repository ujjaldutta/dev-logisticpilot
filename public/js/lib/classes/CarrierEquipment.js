'use strict';
function CarrierEquipment(CarrierEquipmentAPIEndpoint, $http, $q){
	this.CarrierEquipmentAPIEndpoint = CarrierEquipmentAPIEndpoint;
	this.http = $http;
	this.q = $q;

	this.makeAsyncRequest = function(method){
		try{
			//console.log(typeof method);
			if(!method instanceof  Object)
				throw new Error('Invalid method given');

			var defer = this.q.defer();
			defer.resolve(method);
			
			return defer.promise;
			
		}catch(ex){
			console.log(ex.message);
		}
	}
}

CarrierEquipment.prototype.listAccessorial = function(params){
	try{
		var url = __buildUrl(params, this.CarrierEquipmentAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CarrierEquipment.prototype.listFuel = function(params){
	try{
		var url = __buildUrl(params, this.CarrierEquipmentAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CarrierEquipment.prototype.getCalculationTypes = function(carrierId){
	try{
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.calcTypes + 'carrierId=' + carrierId;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CarrierEquipment.prototype.getFuelTypes = function(carrierId){
	try{
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.fuelTypes + 'carrierId=' + carrierId;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CarrierEquipment.prototype.getAccessorialTypes = function(carrierId){
	try{
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.accessorials + 'carrierId=' + carrierId;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CarrierEquipment.prototype.getAccessorialData = function(id){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.get + 'id=' + id;
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CarrierEquipment.prototype.getFuelData = function(id){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.get + 'id=' + id;
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CarrierEquipment.prototype.postAccessorialData = function(carrierId, payload){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.edit;
		return this.http.post(url, angular.extend({carrierID: carrierId}, payload))
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CarrierEquipment.prototype.postFuelData = function(carrierId, payload){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base +  this.CarrierEquipmentAPIEndpoint.edit;
		return this.http.post(url, angular.extend({carrierID: carrierId}, payload))
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CarrierEquipment.prototype.deleteAccessorialData = function(id){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base.substring(0,  this.CarrierEquipmentAPIEndpoint.base.length -1) + '/' + id + '/' + this.CarrierEquipmentAPIEndpoint.deleteUrl;
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CarrierEquipment.prototype.deleteFuelData = function(id){
	try{	
		var url = this.CarrierEquipmentAPIEndpoint.base.substring(0,  this.CarrierEquipmentAPIEndpoint.base.length -1) + '/' + id + '/' + this.CarrierEquipmentAPIEndpoint.deleteUrl;
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildUrl(params, apiBaseUrl){

	var carrierId = 0, filters = {}, sorting = [], limit = 20, page = 1, orderBy= '', orderType='';

	//console.log(params);
	
	if(typeof params === 'object'){
		if(params.carrierId && !isNaN(params.carrierId))
			carrierId = params.carrierId;
		else
			throw new Error('Carrier should be present');
		
		if(params.filters)
			filters = params.filters;
		if(params.limit)
			limit = params.limit;
		if(params.sorting)
			sorting = params.sorting;
		if(params.page)
			page = params.page;
		if(params.orderBy)
			orderBy = params.orderBy;
		if(params.orderType)
			orderType = params.orderType;

	}
	return apiBaseUrl.base +  apiBaseUrl.list + 'carrierId=' +  carrierId + '&filterJson=' + angular.toJson(filters, true) + 
		'&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType;
}