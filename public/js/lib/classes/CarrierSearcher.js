'use strict';
function CarrierSearcher(searchEndPoint, quoteEndPoint, $http, $q){
			this.searchEndPoint = searchEndPoint;
			this.quoteEndPoint = quoteEndPoint;
			this.http = $http;
			this.q = $q;

			this.carriersFound = 0;

			this.makeSearchRequest = function(url){
				return this.http.jsonp(url)
					.error(function(data, status, config, headers){
						console.warn(status);
					});
			}

			this.searchForCarrier = function(data){
				try{
					//console.log(data);
					var url = __buildUrl(data, this.searchEndPoint);
					var defer = this.q.defer();
					
					if(url){
						//console.log(url);
						defer.resolve(this.makeSearchRequest(url));
					}
					
					return defer.promise;
					
				}catch(ex){
					console.log(ex.message);
				}
			}

			this.saveQuote = function(data){
				return $http.post(__builQuoteActionUrl('saveQuote', this.quoteEndPoint), data)
					.error(function(data, status, config, headers){
						console.warn(status);
					});

			}

			this.makeAsyncRequest = function(func){
				try{
					var defer = this.q.defer();
					
					defer.resolve(func);
					
					return defer.promise;
					
				}catch(ex){
					console.log(ex.message);
				}
			}

			//public method to process the google generated location from the autocomplete and have formatted information
			this.processGoogleLocationInformation = function (location){
				var formattedLocation = {};
				if(location && location.address_components){
					for(var item in location.address_components){
						switch(location.address_components[item]['types'][0]){
							case 'postal_code': 
								angular.extend(formattedLocation, {postal_code: location.address_components[item]['long_name']});
								break;
							case 'locality':
								angular.extend(formattedLocation, {city: location.address_components[item]['long_name']});
								break;
							case 'administrative_area_level_1':
								angular.extend(formattedLocation, {state: location.address_components[item]['short_name']});
								break;
							case 'country':
								angular.extend(formattedLocation, {country: location.address_components[item]['short_name']});
								break;
						}
					}
				}

				return formattedLocation;
			}

			//private method to build quote related api url as per the given params
			function __builQuoteActionUrl(params, apiBaseUrl){
				switch(params){
					case 'saveQuote':
						return apiBaseUrl + 'action=saveQuote';
				}
			}
			
			//prtivate method to handle carrier accs or fuel tariff filter parmas
			function __buildUrl(data, apiBaseUrl){
				//return 'http://192.168.1.12/teamD/dibakar/lp-test-jsonp/jsonp.php?callback=JSON_CALLBACK';
				
				return apiBaseUrl + 'UserID=ltlsupport%40uship.com&DeliveryCity=PITTSBURGH&ServiceLevel=&Password=D%40%21%21a75201&DeliveryState=PA&Accs1=&meterno=&DeliveryPostal=15201&type=&Accountnumber=&DeliveryCountry=USA&Clientaddress=&PickupCity=CLEVELAND&ScacCode=PITD&Clientcity=&PickupState=OH&PaymentTerm=Prepaid&Clientstate=&PickupPostal=44101&ShipDate=&ClientPostal=&PickupCountry=USA&Accounttype=&clientcountry=&accountname=&isdebugmode=&IsTestMode=&products=&carrierrateserviceproduct=&usebrokersrv=&Class1=70&Weight1=1000&Height1=&Length1=&NMFC1=123456&Pallet1=&Width1=&Class2=&Weight2=&Height2=&Length2=&NMFC2=&Pallet2=&Width2=&Class3=&Height3=&Length3=&NMFC3=&Pallet3=&Weight3=&Width3=&Class4=&Height4=&Length4=&NMFC4=&Pallet4=&Weight4=&Width4=&Class5=&Height5=&Length5=&NMFC5=&Pallet5=&Weight5=&Width5=&callback=JSON_CALLBACK';
				
				var url = '';
				//forming url for api call
				//account security and connectivity if the info does not exist exit and return false
				if(data.apiUserName !== undefined && data.apiPassword !== undefined && data.accountNumber !== undefined && data.scac !== undefined){
					url += 'UserID=' + data.apiUserName + '&Password=' + data.apiPassword + '&Accountnumber=' 
						+ data.accountNumber + '&ScacCode=' + data.scac 
						+ '&accountname=&meterno=&isdebugmode=1&IsTestMode=1&ServiceLevel=STD&usebrokersrv=1&products=&type=';
				}

				//building accs for each product
				var accs = [];
				if(data.addlAccessorial){
					for(var i=0; i<data.addlAccessorial.length; i++){
						accs.push(data.addlAccessorial[i].id);
					}
				}

				//additional config for the selected carrier
				if(data.apiShipType && data.apiPayType)
					url += '&Accounttype=' + data.apiShipType + '&PaymentTerm=Prepaid';

				if(data.clientInfo){
					var clientInfo = '';
					for(var key in data.clientInfo){
						clientInfo += '&' + key + '=' + escape(data.clientInfo[key]);
					}
					url += clientInfo;
				}

				//shipment date
				if(data.shipDate)
					url += '&ShipDate=' + data.shipDate; 
				
				//pickup location
				if(data.pickup){
					var pickup = this.processGoogleLocationInformation(data.pickup);
					url += '&PickupCity=' + pickup.city;
					url += '&PickupState=' + pickup.state;
					url += '&PickupPostal=' + pickup.postal_code;
					url += '&PickupCountry=' + pickup.country;

					/* for(var item in pickup){
						//console.log(item);
						//console.log(pickup[item]);
						if(pickup[item].city)
							url += '&PickupCity=' + pickup[item].city;
						else if(pickup[item].state)
							url += '&PickupState=' + pickup[item].state;
						else if(pickup[item].postal_code)
							url += '&PickupPostal=' + pickup[item].postal_code;
						else if(pickup[item].country)
							url += '&PickupCountry=' + pickup[item].country;
					} */
					
				}
				//delivery location
				if(data.delivery){
					var delivery = this.processGoogleLocationInformation(data.delivery);
					url += '&DeliveryCity=' + delivery.city;
					url += '&DeliveryState=' + delivery.state;
					url += '&DeliveryPostal=' + delivery.postal_code;
					url += '&DeliveryCountry=' + delivery.country;
					
					/* for(var item in delivery){
						//console.log(item);
						//console.log(pickup[item]);
						if(delivery[item].city)
							url += '&DeliveryCity=' + delivery[item].city;
						else if(delivery[item].state)
							url += '&DeliveryState=' + delivery[item].state;
						else if(delivery[item].postal_code)
							url += '&DeliveryPostal=' + delivery[item].postal_code;
						else if(delivery[item].country)
							url += '&DeliveryCountry=' + delivery[item].country;
					} */
				}
				//adding multiple prodcuts
				if(data.products && data.products.length){
					var productUrl = '';
					for(var i=0; i<data.products.length; i++){
						productUrl += '&Description' + (i+1) + '=';
						productUrl += '&NMFC' + (i+1) + '=';
						productUrl += '&Class' + (i+1) + '=' + data.products[i].class;
						productUrl += '&Weight' + (i+1) + '=' + data.products[i].weight;
						productUrl += '&Height' + (i+1) + '=' + data.products[i].height;
						productUrl += '&Length' + (i+1) + '=' + data.products[i].len;
						productUrl += '&Width' + (i+1) + '=' + data.products[i].width;
						productUrl += '&Pieces' + (i+1) + '=' + data.products[i].qty;
						productUrl += '&Pallet' + (i+1) + '=' + data.products[i].qty;
						productUrl += '&Accs' + (i+1) + '=' + accs.join(',');
					}

					url += productUrl;
				}

				return apiBaseUrl + url + '&callback=JSON_CALLBACK';
			}
		}