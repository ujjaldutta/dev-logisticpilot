'use strict';
function CustomerLocation(CustomerLocationAPIEndpoint, $http, $q, GoogleLocationFactory){
	this.CustomerLocationAPIEndpoint = CustomerLocationAPIEndpoint;
	this.http = $http;
	this.q = $q;
	this.GoogleLocationFactory = GoogleLocationFactory;

	this.makeAsyncRequest = function(method){
		try{
			//console.log(typeof method);
			if(!method instanceof  Object)
				throw new Error('Invalid method given');

			var defer = this.q.defer();
			defer.resolve(method);
			
			return defer.promise;
			
		}catch(ex){
			console.log(ex.message);
		}
	}

	this.processGoogleLocation = function(locationObj){
		return this.GoogleLocationFactory.processGoogleLocation(locationObj);
	}

	this.fillGoogleLocation = function(locationObj){
		return this.GoogleLocationFactory.fillLocationDetails(locationObj, 'location');
	}
}

CustomerLocation.prototype.listLocations = function(params){
	try{
		var url = __buildLocationUrl(params, this.CustomerLocationAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CustomerLocation.prototype.getLocationTypes = function(){
	try{
		var url = this.CustomerLocationAPIEndpoint.base +  this.CustomerLocationAPIEndpoint.types;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CustomerLocation.prototype.geTimeZones = function(){
	try{
		var url = this.CustomerLocationAPIEndpoint.base +  this.CustomerLocationAPIEndpoint.timeZone;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CustomerLocation.prototype.getCustomerData = function(id){
	try{	
		var url = this.CustomerLocationAPIEndpoint.base +  this.CustomerLocationAPIEndpoint.edit + 'id=' + id;
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CustomerLocation.prototype.postLocationData = function(clientId, payload){
	try{	
		var url = this.CustomerLocationAPIEndpoint.base +  this.CustomerLocationAPIEndpoint.edit;
		return this.http.post(url, angular.extend({clientId: clientId}, payload))
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CustomerLocation.prototype.deleteLocation = function(id){
	try{	
		var url = this.CustomerLocationAPIEndpoint.base +  this.CustomerLocationAPIEndpoint.deleteUrl + 'id=' + id; ;
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildLocationUrl(params, apiBaseUrl){

	var filters = {}, sorting = [], limit = 20, page = 1, orderBy= '', orderType='', clientId = '';
	
	if(typeof params === 'object'){
		if(params.filters)
			filters = params.filters;
		if(params.limit)
			limit = params.limit;
		if(params.sorting)
			sorting = params.sorting;
		if(params.page)
			page = params.page;
		if(params.orderBy)
			orderBy = params.orderBy;
		if(params.orderType)
			orderType = params.orderType;
		if(params.clientId)
			clientId = params.clientId;

	}
	return apiBaseUrl.base +  apiBaseUrl.list + 'filters=' + angular.toJson(filters, true) + 
		'&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType + '&clientID=' + clientId;
}