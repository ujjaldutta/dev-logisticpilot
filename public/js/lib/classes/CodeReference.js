'use strict';
function CodeReference(CodeReferenceAPIEndpoint, $http, $q, GoogleLocationFactory){
	this.CodeReferenceAPIEndpoint = CodeReferenceAPIEndpoint;
	this.http = $http;
	this.q = $q;
	this.GoogleLocationFactory = GoogleLocationFactory;

	this.makeAsyncRequest = function(method){
		try{
			//console.log(typeof method);
			if(!method instanceof  Object)
				throw new Error('Invalid method given');

			var defer = this.q.defer();
			defer.resolve(method);
			
			return defer.promise;
			
		}catch(ex){
			console.log(ex.message);
		}
	}

	this.processGoogleLocation = function(locationObj){
		return this.GoogleLocationFactory.processGoogleLocation(locationObj);
	}

	this.fillGoogleLocation = function(locationObj){
		return this.GoogleLocationFactory.fillLocationDetails(locationObj, 'location');
	}
}

CodeReference.prototype.listCodeReferences = function(params){
	try{
		var url = __buildUrl(params, this.CodeReferenceAPIEndpoint);
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

CodeReference.prototype.getCodeData = function(id){
	try{	
		var url = this.CodeReferenceAPIEndpoint.base +  this.CodeReferenceAPIEndpoint.edit + 'id=' + id;
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CodeReference.prototype.postCodeData = function(id, data){
	try{	
		var url = this.CodeReferenceAPIEndpoint.base +  this.CodeReferenceAPIEndpoint.edit;
		return this.http.post(url, angular.extend({id: id}, data))
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CodeReference.prototype.deleteCodeReference = function(id){
	try{	
		var url = this.CodeReferenceAPIEndpoint.base +  this.CodeReferenceAPIEndpoint.deleteUrl + 'id=' + id;
		return this.http.delete(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
}

CodeReference.prototype.getTypes= function(){
	try{
		var url = this.CodeReferenceAPIEndpoint.base +  this.CodeReferenceAPIEndpoint.codetypes;
		
		return this.http.get(url)
		.error(function(data, status, config, headers){
			console.warn(status);
		});
	}catch(ex){
		console.log(ex.message);
	}
		
}

//prtivate method to handle carrier accs or fuel tariff filter parmas
function __buildUrl(params, apiBaseUrl){

	var filters = {}, sorting = [], limit = 20, page = 1, orderBy= '', orderType='';
	
	if(typeof params === 'object'){
		if(params.filters)
			filters = params.filters;
		if(params.limit)
			limit = params.limit;
		if(params.sorting)
			sorting = params.sorting;
		if(params.page)
			page = params.page;
		if(params.orderBy)
			orderBy = params.orderBy;
		if(params.orderType)
			orderType = params.orderType;

	}
	return apiBaseUrl.base +  apiBaseUrl.list + 'filters=' + angular.toJson(filters, true) + 
		'&limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderType=' + orderType;
}