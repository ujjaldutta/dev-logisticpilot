'use strict';

function UserManagerController(User, $scope, $window){
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	$scope.userData = {count: 0, list: []};
	$scope.TotalCustomer = [];
	
	//$scope.pageNos = [{id: 3, val: 3}, {id: 5, val: 5}, {id: 10, val: 10}, {id: 15, val: 15}, {id: 20, val: 20}, {id: 25, val: 25}, ];
	$scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
	$scope.recordLimit = 3;
	$scope.currentPage = 1;
	$scope.filters = [];
	$scope.filter = {};

	$scope.predicate = 'id';
    $scope.reverse = false;
	
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	   
	   fetchUsers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	};


	$scope.pageChanged = function(){
		//fetchUsers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage});
		
		fetchUsers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	}

	$scope.$watch('recordLimit', function(value, oldValue){
    	if(oldValue != value){
    		fetchUsers({clientID: value, filters: $scope.filters, limit: $scope.recordLimit, 
					page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}
    });
	
	//intially fetching records of the current page i.e. page 1 with current conditions
	//fetchUsers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage});

	$scope.$watch('clinetID', function(value){
		if(!isNaN(value) && value){
			//intially fetching records of the current page i.e. page 1 with current conditions
			fetchUsers({clientID: value, filters: $scope.filters, limit: $scope.recordLimit, 
					page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	})

	

	//filtering customer result if any by watching sorting option changes
	 $scope.$watch('filter.userName', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: value}, {location: User.processGoogleLocation($scope.filter.userLoc)}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
			fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, 
				page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	
	$scope.$watch('filter.userLoc', function(value, oldValue){
		//console.log(User.processGoogleLocation(value));
		if(angular.isObject(value)){
			$scope.filters = [{location: User.processGoogleLocation(value)}, {name: $scope.filter.userName}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];

			fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, 
				page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}else if(oldValue !== undefined){
			$scope.filters = [{location: null}, {name: $scope.filter.userName}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
			fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
			
	});

	$scope.$watch('filter.userActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: User.processGoogleLocation($scope.filter.userLoc)}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
			fetchUsers({
				clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}

	});

	$scope.$watch('filter.userInActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: User.processGoogleLocation($scope.filter.userLoc)}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
			fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, 
				page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});

	$scope.resetFilter = function(type){
		switch(type){
			case 'userLoc':
				if($scope.filter.userLoc !== undefined){
					$scope.filter.userLoc = null;
					$scope.filters = [{name: $scope.filter.userName}, {location: $scope.filter.userLoc}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
					fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
				}
				break;
			case 'userName':
				if($scope.filter.userName !== undefined){
					$scope.filter.userName = null;
					$scope.filters = [{name: $scope.filter.userName}, {location: User.processGoogleLocation($scope.filter.userLoc)}, {status: {active: $scope.filter.userActive, inactive: $scope.filter.userInActive}}];
					fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
				}
				break;
		}
	}


	$scope.checkAll = function(){
		angular.forEach($scope.userData.list, function(value, key){
			if($scope.userData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){
		if($scope.userData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.userData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.userData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.userData.selectAll = true;

		}else
			$scope.userData.selectAll = false;
	}

	$scope.deleteAllUser = function(){
		//scanning all items for checking
		if($window.confirm('Are you sure ?')){
			angular.forEach($scope.userData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteUser(value.id, false);
				}
			});
		}	
	}

	$scope.deleteUser = function(userID, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = User.makeAsyncRequest(User.deleteUser(userID));
		}else if(showAlert === false)
			promiseObj = User.makeAsyncRequest(User.deleteUser(userID));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
					fetchUsers({clientID: $scope.clinetID, limit: $scope.recordLimit, filters: $scope.filters});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}

	function fetchUsers(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		User.makeAsyncRequest(User.listUser(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.userData.count = response.data.count;
					$scope.userData.list = response.data.results ? response.data.results : [];
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

}

