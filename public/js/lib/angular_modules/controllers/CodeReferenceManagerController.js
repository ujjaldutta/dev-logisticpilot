'use strict';

function CodeReferenceManagerController(CodeReference, $scope, $window) {

    $scope.codeReferenceData = {count: 0, list: []};
    $scope.TotalCodeReferences = [];
    //$scope.pageNos = [{id: 3, val: 3}, {id: 5, val: 5}, {id: 10, val: 10}, {id: 15, val: 15}, {id: 20, val: 20}, {id: 25, val: 25}, ];
    $scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
    $scope.recordLimit = 10;
    $scope.currentPage = 1;
    $scope.filters = [];
    $scope.filter = {};
    $scope.types  = [];

    $scope.predicate = 'id';
    $scope.reverse = false;

    $scope.$watch('recordLimit', function (value, oldValue) {
        if (oldValue != value) {
            fetchCodeReferences({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });

    $scope.sortColumn = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
        //alert("A");
        fetchCodeReferences({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
            orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
        });
    };


    $scope.pageChanged = function () {
        fetchCodeReferences({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
            orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
        });
    }

    getTypes();
    //intially fetching records of the current page i.e. page 1 with current conditions
    fetchCodeReferences({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});


    //filtering customer result if any by watching sorting option changes
    $scope.$watch('filter.itemDesc', function (value, oldValue) {
        if (value != oldValue) {
            $scope.filters = [{code:  $scope.filter.itemCode}, {desc:value}, {classTypes: $scope.filter.classId}];
            fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });
    
    $scope.$watch('filter.itemCode', function (value, oldValue) {
        if (value != oldValue) {
            $scope.filters = [{code: value}, {desc:$scope.filter.itemDesc}, {classTypes: $scope.filter.classId}];
            fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });
    
    $scope.$watch('filter.codeType', function (value, oldValue) {
        if (value != oldValue) {
            $scope.filters = [{code:  $scope.filter.itemCode}, {desc:$scope.filter.itemDesc}, {codetype: value}];
            fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });

    $scope.resetFilter = function (type) {
        switch (type) {
            case 'itemDesc':
                if ($scope.filter.itemDesc !== undefined) {
                    $scope.filter.itemDesc = null;
                    $scope.filters = [{code: $scope.filter.itemCode}, {desc:$scope.filter.itemDesc}];
                    fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                        orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
                    });
                }
                break;
            case 'itemCode':
                if ($scope.filter.itemCode !== undefined) {
                    $scope.filter.itemCode = null;
                    $scope.filters = [{code: $scope.filter.itemCode}, {desc:$scope.filter.itemDesc}];
                    fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                        orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
                    });
                }
                break;
        }
    }

    $scope.checkAll = function () {
        angular.forEach($scope.codeReferenceData.list, function (value, key) {
            if ($scope.codeReferenceData.selectAll === true)
                value.selected = true;
            else
                value.selected = false;
        });
    }

    $scope.checkSelection = function (index) {
        if ($scope.codeReferenceData.list[index].selected === true) {
            //now checking whether

            //total no of items on this current page
            var totalItems = $scope.codeReferenceData.list.length,
                    selectedItems = 0;

            //scanning all items for checking
            angular.forEach($scope.codeReferenceData.list, function (value, key) {
                if (value.selected === true)
                    selectedItems++;
            });

            if (totalItems == selectedItems)
                $scope.codeReferenceData.selectAll = true;

        } else
            $scope.codeReferenceData.selectAll = false;
    }

    $scope.deleteAllCodeReferences = function () {
        //scanning all items for checking
        if ($window.confirm('Are you sure ?')) {
            angular.forEach($scope.codeReferenceData.list, function (value, key) {
                if (value.selected === true) {
                    $scope.deleteCodeReference(value.Id, false);
                }
            });
        }
    }

    $scope.deleteCodeReference = function (id, showAlert) {
        var promiseObj = null;

        if (showAlert === true && $window.confirm('Are you sure to delete?')) {
            promiseObj = CodeReference.makeAsyncRequest(CodeReference.deleteCodeReference(id));
        } else if (showAlert === false)
            promiseObj = CodeReference.makeAsyncRequest(CodeReference.deleteCodeReference(id));

        //processing current list after successfull delete
        if (promiseObj) {
            promiseObj.then(function (response) {
                if (response.data.success) {
                    fetchCodeReferences({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc' });
                }
            }, function (reason) {
                console.warn(reason);
            });
        }
    }
    
    function getTypes() {
        CodeReference.makeAsyncRequest(CodeReference.getTypes())
                .then(function (response) {
                    if (response.data.success && response.data.types) {
                        $scope.filter.types = response.data.types;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function fetchCodeReferences(params) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');
        CodeReference.makeAsyncRequest(CodeReference.listCodeReferences(params))
                .then(function (response) {
                    if (response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.codeReferenceData.count = response.data.count;
                        $scope.codeReferenceData.list = response.data.results ? response.data.results : [];
                    }
                    //console.log(result);
                }, function (reason) {
                    console.warn(reason);
                }).finally(function (data) {
            //making the loader invisible for user
            angular.element('.loading').addClass('hidden');
        });
    }

}

