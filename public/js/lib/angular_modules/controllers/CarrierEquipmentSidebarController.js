'use strict';

function CarrierEquipmentSidebarController(CarrierApiService, CarrierEquipmentEndPoint, $scope, $window){
	$scope.carrierList = [];
	$scope.dtPickerstatus = {fromOpened: false, toOpened: false};
	$scope.carrierRecordLimit = 5;

	fecthSelectedCarriers();

	$scope.gotoAccessorialSetup = function(carrierId){
		$window.location.href = CarrierEquipmentEndPoint.base + CarrierEquipmentEndPoint.listAccessorial + carrierId;	
	}

	$scope.gotoFuelSetup = function(carrierId){
		$window.location.href = CarrierEquipmentEndPoint.base + CarrierEquipmentEndPoint.listFuel + carrierId;	
	}
	
	$scope.openSortableFrom = function($event) {
		//alert('here');
    	$scope.dtPickerstatus.fromOpened = true;
  	};

  	$scope.openSortableTo = function($event) {
    	$scope.dtPickerstatus.toOpened = true;
  	};

  	$scope.dateOptions = {
  		//formatYear: 'yy',
    	startingDay: 1
  	};


	//private method to get selected carriers for this customer
	function fecthSelectedCarriers(){
		angular.element('.loading-carrier').removeClass('hidden');
		CarrierApiService.fecthSelectedCarriers(false) //calling factory to make ajax request
			.success(function(result){
				 if(result.success && result.carriers && result.carrierlist){
					//$scope.selectedCarrierList = result.carriers;
					$scope.carrierList = result.carriers;
					angular.element('.loading-carrier').addClass('hidden');
				}
			});
	}
}