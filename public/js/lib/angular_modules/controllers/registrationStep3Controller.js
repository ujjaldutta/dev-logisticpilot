function registrationStep3Controller(SyncCarriers, $scope, $http, $compile, $rootScope, Upload){
	//we will store all of our form data in this object
	$scope.formStep3 = {saveButtonText: 'Next', carriers: []};
	
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOptionsAddCarrier = {
        types: ["(regions)"]
    }

	//intializing data from the server to list all previously selected carriers
	fetchDatafromServer();

	/* $scope.selectedCarrierList = [{
		id: 2,
		name: 'Demo',
		logo: 'http://logisticspilot.com.in/uploads/carriers/ups-logo.png',
		address1: 'test',
		address2: 'test',
		city: 'test',
		state: 'test',
		postal: 'test',
		country: 'test',
	}]; */

	$scope.addToExsitingList = function(form){
		if($scope.currentSelectedProvider && $scope.currentSelectedProvider.originalObject){
			//inserting into the model so that the template compile service can immeadiately display it
			$scope.selectedCarrierList.push({
				id: $scope.currentSelectedProvider.originalObject.id,
				name: $scope.currentSelectedProvider.originalObject.name,
				logo: $scope.currentSelectedProvider.originalObject.logo,
				address1: $scope.currentSelectedProvider.originalObject.address1,
				address2: $scope.currentSelectedProvider.originalObject.address2,
				city: $scope.currentSelectedProvider.originalObject.city,
				state: $scope.currentSelectedProvider.originalObject.state,
				postal: $scope.currentSelectedProvider.originalObject.postal,
				country: $scope.currentSelectedProvider.originalObject.country,
			});
			
			//inserting into list of selected carriers for server side processing
			$scope.formStep3.carriers.push($scope.currentSelectedProvider.originalObject.id);
			
			//clearing the selected item after selecting
			$scope.currentSelectedProvider = $scope.initial;

			//calling automatic saving to assign assignment id for added carrier otherwise instant contact saving wont work
			$scope.submitStep3(form);
		}
	}


	//function to handle and submit step 3
	$scope.submitStep3 = function(form){
		if(form && form.$valid){
			//flag to enable loader for registration process
			$scope.processing.status = true;
			
			$scope.formStep3.saveButtonText = 'Saving...';
			
			$http.post('/front/client-register', $scope.formStep3)
				.success(function (data){
					$scope.formStep3.saveButtonText = 'Next';
					if(data.success){
						//now saving all contact info by broadcasting to the event listeners
						$scope.$broadcast('saveContactInfo', {});
						
						//intializing data from the server to list all previously selected carriers
						fetchDatafromServer();
						
						//disabling loader after complete
						$scope.processing.status = false;
					}else if(data.error){
						$scope.error.status = true;
						$scope.error.message = data.error;

						//disabling loader after complete
						$scope.processing.status = false;
					}
				})
				.error(function(data, status){
					alert(status);
					if(status == 302 || status == 301 || status == 401)
						window.location.href = '/'
				});
		}
	};

	/* section to handle add carrier option
		************************************
	*/

	/* $scope.addCarrier = {id: 0, };
	$scope.addCarrierMessage = {
		error_status: 0,
		success_status: 0,
		error_message: '',
		success_message:'',
		saveButtonText: 'Save',
	}; */

	$scope.loadAddCarrierContent = function(){
		$.get('/front/ajaxStep3AddCarrier').
			done(function(result){
				//resetting the form everytime when its open again
				$scope.addCarrier = {id: 0, };
				$scope.addCarrierMessage = {
					error_status: 0,
					success_status: 0,
					error_message: '',
					success_message:'',
					saveButtonText: 'Save',
				};
			   
			    var elem = $compile(result)($scope);
			    // ...and link it to the scope we're interested in.
			    // Here we'll use the $rootScope.
			   
			    
			    $('#modal-frame').html(elem);

			    // Now that the content has been compiled, linked,
			    // and added to the DOM, we must trigger a digest cycle
			    // on the scope we used in order to update bindings.
			    $scope.$digest();
			})
			.error(function(jqXHR, textStatus, errorThrown){
				alert('Error: ' + jqXHR.status + " " + textStatus + " " + errorThrown);
					
				if(jqXHR.status == 302 || jqXHR.status == 301 || jqXHR.status == 401)
					window.location.href = '/'
			});
		
		/* $('#modal-frame').load('/front/ajaxStep3AddCarrier', function(data){

		}); */
	}

		$scope.addCarrierCountrySelected = function(selected){
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.addCarrier.carrier_country = selected.title;

		//clearing all the fields
		$scope.$broadcast('setOutsideValue', {id: 'addCarriercity', value: '' });
		$scope.$broadcast('setOutsideValue', {id: 'addCarrierPostal', value: '' });
		$scope.$broadcast('setOutsideValue', {id: 'addCarrierstate', value: '' });

		$scope.addCarrier.carrier_city = '';
		$scope.addCarrier.carrier_state = '';
		$scope.addCarrier.carrier_postal = '';
	};

	//method to handle selected postcodes
	$scope.addCarrierPostcodeSelected = function(selected) {
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.addCarrier.carrier_postal = selected.title;
		$scope.addCarrier.carrier_city = selected.originalObject.city_name;
		$scope.addCarrier.carrier_state = selected.originalObject.state_code;
		$scope.addCarrier.carrier_country = selected.originalObject.country_code;

		$scope.$broadcast('setOutsideValue', {id: 'addCarriercity', value: selected.originalObject.city_name});
		$scope.$broadcast('setOutsideValue', {id: 'addCarriercountry', value: selected.originalObject.country_code});
		$scope.$broadcast('setOutsideValue', {id: 'addCarrierstate', value: selected.originalObject.state_code});
		
    };

    $scope.addCarrierCitySelected = function(selected){
    	if(selected === undefined)
			return;

		//console.log(selected);
		$scope.addCarrier.carrier_city = selected.title;
    };
	
	$scope.addCarrierStateSelected = function(selected){
		if(selected === undefined)
			return;
		
    	//console.log(selected);
    	$scope.addCarrier.carrier_state = selected.title;
    };

    $scope.addCarrierCountryChanged = function(str){
    	$scope.addCarrier.carrier_country = str;
    };
    
    $scope.addCarrierPostcodeChanged = function(str){
    	$scope.addCarrier.carrier_postal = str;
    };
    $scope.addCarrierStateChanged = function(str){
    	$scope.addCarrier.carrier_state = str;
    };
    $scope.addCarrierCityChanged = function(str){
    	$scope.addCarrier.carrier_city = str;
    };

	//method to submit the form
	$scope.submitNewCarrier = function(form){
		//$scope.abForm.$valid
		if(form && form.$valid){
			$scope.addCarrierMessage.saveButtonText = 'Saving...';
			$http.post('/front/ajaxStep3AddCarrier', $scope.addCarrier)
				.success(function (data){
					
					$scope.addCarrierMessage.saveButtonText = 'Save';
					if(data.success){
						$scope.addCarrierMessage.error_status = false;
						
						$scope.addCarrierMessage.success_message = data.success;
						$scope.addCarrierMessage.success_status = true;
						
						$scope.addCarrier.id = data.id ? data.id : 0;
						//$scope.formData.billInfoID = data.billInfoID;
					}else if(data.error){
						$scope.addCarrierMessage.success_status = false;
						
						$scope.addCarrierMessage.error_message = data.error;
						$scope.addCarrierMessage.error_status = true;
					}
				})
				.error(function(data, status){
					alert('Error: ' + status);
					if(status == 302 || status == 301 || status == 401)
						window.location.href = '/'
					
				});
		}
	}

	$scope.resetAddNewCarrier = function(){
		
	}

	/* Ends here
		************************************
	*/

	$scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/ajaxStep3AddCarrierImage',
                    fields: {
                        'id': $scope.addCarrier.id
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.addCarrier.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                //evt.config.file.name + '\n' + $scope.formData.log;
                }).success(function (data, status, headers, config) {
                	if(data.error){
                		$scope.error.status = true;
						$scope.error.message = data.error;
                	}else if(data.success){
                		$scope.addCarrier.id = data.id ? data.id : null;
                		
                		$scope.addCarrier.log = 'file ' + config.file.name + 'uploaded successfully!'
                	}
                  
                   //$scope.$apply();
                });
            }
        }
    };


	$scope.$on('refetch', function(e, args){
		//console.log('here in events');
		fetchDatafromServer();
	});

	$scope.selectedSearchCarrier = function(selected){
		//getting scac code
		var scac = selected.originalObject.scac;
		//getting the carrier ID for the selected carrier
		var carrierId = selected.originalObject.id;
		console.log(selected);
		
		if(scac && carrierId)
			window.location.href="/front/setup-connectivity#/setup?id="+ carrierId + "&scac=" + scac;
	}

	
	function fetchDatafromServer(){
		SyncCarriers.fetch(true) //calling factory to make ajax request
			.success(function(result){
				 if(result.success && result.carriers && result.carrierlist){
					$scope.selectedCarrierList = result.carriers;
					$scope.formStep3.carriers = result.carrierlist;
				}
			});
	}
}