'use strict';

function CarrierEquipmentEditController(CarrierAPIEndpoint, Carrier, Upload, $scope, $location, $window) {
    //model attribute to show/hide error message notification
    $scope.error = {
        status: false,
        message: '',
    };

    $scope.success = {
        status: false,
        message: '',
    };

    //collection  of carrier type objects
    //$scope.carrierId = '';
    $scope.equipmentData = {};    
    
    getEquipmentTypes();
    getOwnerTypes();
    
     // Settings combox sidebar
    $scope.filters = [];
    $scope.filter = {};
    $scope.filter.insId = {};
    $scope.filter.insurancetTypes = [];
    $scope.insSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.modId = {};
    $scope.filter.equipmentTypes = [];
    $scope.modSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.equipId = {};
    $scope.filter.modeTypes = [];
    $scope.equipmentSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };
    
    $scope.equipmentData.equipId = {};
    $scope.equipTypes = [];
    $scope.equipSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
    $scope.equipmentData.ownerId = {};
    $scope.ownerTypes = [];
    $scope.ownerSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };                      
    
    
    /* method to dismiss the alert */
    $scope.hideMessage = function () {
        $scope.error = {
            status: false,
            message: '',
        };

        $scope.success = {
            status: false,
            message: '',
        };
    }

    $scope.cancelEditing = function (carrierId) {        
        $window.location.href = CarrierAPIEndpoint.base + 'edit/' + carrierId + '#horizontalTab14';        
    }

    /*
     * when valid carr id is loaded or assigned 
     * after dom rendered make a aysnc req to the server 
     * to get the information
     */
    $scope.$watch('equipID', function (val) {
        if (!isNaN(val) && val > 0) {
            //fetching the selected carrier data
            getEquipmentData(val);
        }

    });

    function getEquipmentData(equipID) {
        if (!equipID)
            equipID = $location.search().equipID || 0;

        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');

        Carrier.makeAsyncRequest(Carrier.getEquipmentData(equipID))
                .then(function (response) {
                    if (response.data.success && response.data.equipment) {
                        $scope.equipmentData = response.data.equipment;
                        $scope.carrierId = response.data.carrierId;
                        $scope.equipmentData.equipId = {id: $scope.equipmentData.equipId};
                        $scope.equipmentData.ownerId = {id: $scope.equipmentData.ownerId};
                        $scope.equipmentData.location = __fillLocationDetails($scope.equipmentData, 'equip');                        
                        $scope.equipmentData.equip_refridgerate = !!parseInt($scope.equipmentData.equip_refridgerate);   
                        
                        return;
                    }
                    //console.log(response);}, function(reason){
                    console.warn(reason);
                })                     
                .finally(function (data) {
                    //making the loader invisible for user
                    angular.element('.loading').addClass('hidden');
                });
    }

    function getEquipmentTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllEquipmentTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.equipmentTypes !== undefined) {
                        $scope.equipTypes = response.data.equipmentTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getOwnerTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllOwnerTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.ownerTypes !== undefined) {
                        $scope.ownerTypes = response.data.ownerTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    /*
     * Saving the carrier fields
     */
    $scope.saveEquipmentData = function (form) {
        if (form && form.$valid ) {
            
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');

            Carrier.postEquipmentData($scope.equipID, $scope.carrierId, $scope.equipmentData)
                    .then(function (response) {
                        
                        if (response.data.success && response.data.equipId && response.data.carrierId) {
                            
                            $scope.equipID = response.data.equipId;
                            $scope.carrierId = response.data.carrierId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';
                        } else if (response.data.error !== undefined) {
                            
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = Carrier.formatValidationErrors(response.data.error);
                            } else
                                $scope.error.message = response.data.error;
                        }
                        //console.log(response);
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }
    
    function __fillLocationDetails(collection, prf) {

        var location = '';
        var city = prf + '_ownercity';
        var state = prf + '_ownerstate';
        var postal = prf + '_ownerpostal';
        var country = prf + '_ownercountry';

        if (collection[city] !== undefined && collection[city])
            location += collection[city] + ', ';
        if (collection[state] !== undefined && collection[state])
            location += collection[state] + ', ';
        if (collection[postal] !== undefined && collection[postal])
            location += collection[postal] + ', ';
        if (collection[country] !== undefined && collection[country])
            location += collection[country];

        return location;
    }
 
}
