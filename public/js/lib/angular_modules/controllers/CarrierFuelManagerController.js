'use strict';

function CarrierFuelManagerController(CarrierEquipment, $scope, $window){
	$scope.fuelData = {count: 0, list: []};
	$scope.fuel = [];
	//$scope.pageNos = [{id: 3, val: 3}, {id: 5, val: 5}, {id: 10, val: 10}, {id: 15, val: 15}, {id: 20, val: 20}, {id: 25, val: 25}, ];
	$scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
	$scope.recordLimit = 3;
	$scope.currentPage = 1;
	$scope.filters = [];
	$scope.filter = {};
	$scope.maxSize = 5;


	$scope.predicate = 'id';
    $scope.reverse = false;

    $scope.$watch('recordLimit', function(value, oldValue){
    	if(oldValue != value){
    		fetchFuels({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}
    });
	
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	   //alert("A");
	   fetchFuels({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	};
	

	$scope.pageChanged = function(){
		fetchFuels({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	}

	$scope.$watch('carrierId', function(value){
		if(value && !isNaN(value)){
			//intially fetching records of the current page i.e. page 1 with current conditions
			fetchFuels({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
	});

	//filtering customer result if any by watching sorting option changes
	$scope.$watch('filter.effectiveFrom', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{fromDate: value}, {toDate: $scope.filter.effectiveTo}];
			
			fetchFuels({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	
	$scope.$watch('filter.effectiveTo', function(value, oldValue){
		if(value != oldValue){
			//console.log(value);
			$scope.filters = [{fromDate: $scope.filter.effectiveFrom}, {toDate: value}];

			fetchFuels({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});


	$scope.checkAll = function(){
		angular.forEach($scope.fuelData.list, function(value, key){
			if($scope.fuelData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){
		if($scope.fuelData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.fuelData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.fuelData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.fuelData.selectAll = true;

		}else
			$scope.fuelData.selectAll = false;
	}

	$scope.deleteAllFuels = function(){
		//scanning all items for checking
		if($window.confirm('Are you sure ?')){
			angular.forEach($scope.fuelData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteFuel(value.id, false);
				}
			});
		}	
	}

	$scope.deleteFuel = function(id, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = CarrierEquipment.makeAsyncRequest(CarrierEquipment.deleteFuelData(id));
		}else if(showAlert === false)
			promiseObj = CarrierEquipment.makeAsyncRequest(CarrierEquipment.deleteFuelData(id));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
					//fetchFuels({limit: $scope.recordLimit});
					fetchFuels({limit: $scope.recordLimit, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}

	function fetchFuels(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.listFuel(angular.extend({carrierId: $scope.carrierId}, params)))
			.then(function(response){
				if(response.data.success && response.data.rates && response.data.rates.totalCustomRateCount !== undefined && response.data.rates.tariffsCustom !== undefined){
					$scope.fuelData.count = response.data.rates.totalCustomRateCount;
					$scope.fuelData.list = response.data.rates.tariffsCustom;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

}