'use strict';

function CustomerProductEditController(CustomerProductAPIEndpoint, CustomerProduct, $scope, $location, $window) {
    $scope.saveBtnText = 'Save';

    //model attribute to show/hide error message notification
    $scope.error = {
        status: false,
        message: '',
    };

    $scope.success = {
        status: false,
        message: '',
    };

    //collection  of customer type objects
    $scope.clientProductData = {}; 
    $scope.productId = null;
    
    // Settings combox fix error
    $scope.filters = [];
    $scope.filter = {};
    $scope.filter.classId = {};
    $scope.filter.classTypes = [];
    $scope.classSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,                            
                           };

    
    // Settings combox
    $scope.clientProductData.item_class = {};
    $scope.freeClassTypes = [];
    $scope.freeclassSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                        };
                           
    $scope.clientProductData.item_weightType = {};
    $scope.weightTypes = [];
    $scope.weightSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };                           
                           
    $scope.clientProductData.item_packagetypeid = {};
    $scope.packageTypes = [];
    $scope.packageSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
    $scope.clientProductData.item_commodity = {};
    $scope.temperatureTypes = [];    
    $scope.comSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
    $scope.clientProductData.item_temperaturetype = {};
    $scope.commodityTypes = [];
    $scope.tempSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
    $scope.clientProductData.item_dimuomtype = {};    
    $scope.dimmensionsTypes = [];
    $scope.dimSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };                       
                           
    
    /* method to dismiss the alert */
    $scope.hideMessage = function () {
        $scope.error = {
            status: false,
            message: '',
        };

        $scope.success = {
            status: false,
            message: '',
        };
    }

    getWeightTypes();
    getCommodityTypes();
    getDimmensionsTypes();
    getTemperatureTypes();
    getFreeClassTypes();
    getPackageTypes();

    $scope.cancelEditing = function () {
        $window.location.href = CustomerProductAPIEndpoint.base ? CustomerProductAPIEndpoint.base : '/';
    }

    /*
     * when valid cust id is loaded or assigned 
     * after dom rendered make a aysnc req to the server 
     * to get the information
     */
    $scope.$watch('custID', function (val) {
        if (!isNaN(val) && val > 0) {
            if (!$scope.newProduct && $scope.productId) {
                //fetching the selected customer data
                getCustomerProductData($scope.productId);
            }

        }

    });


    /*
     * Saving the profile fields
     */

    $scope.saveProductData = function (form) {
        if (form && form.$valid) {
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');

            CustomerProduct.postProductData($scope.custID, $scope.clientProductData)
                    .then(function (response) {
                        if (response.data.success && response.data.productId) {
                            $scope.clientProductData.id = response.data.productId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';
                        } else if (response.data.error !== undefined) {
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = 'Unhandled Exception occurred';
                            } else
                                $scope.error.message = response.data.error;
                        }
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }


    function getCustomerProductData(id) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');

        CustomerProduct.makeAsyncRequest(CustomerProduct.getCustomerData(id))
                .then(function (response) {
                    if (response.data.success && response.data.clientProduct) {
                        $scope.clientProductData = response.data.clientProduct;
                        $scope.clientProductData.item_class = {id: $scope.clientProductData.item_class};
                        $scope.clientProductData.item_weightType = {id: $scope.clientProductData.item_weightType};
                        $scope.clientProductData.item_packagetypeid = {id: $scope.clientProductData.item_packagetypeid};
                        $scope.clientProductData.item_commodity = {id: $scope.clientProductData.item_commodity};
                        $scope.clientProductData.item_temperaturetype = {id: $scope.clientProductData.item_temperaturetype};
                        $scope.clientProductData.item_dimuomtype = {id: $scope.clientProductData.item_dimuomtype};
                        
                        //now converting data type to either true or false for checkboxes
                        $scope.clientProductData.item_returnable = !!parseInt($scope.clientProductData.item_returnable);
                        $scope.clientProductData.item_stackability = !!parseInt($scope.clientProductData.item_stackability);
                        $scope.clientProductData.item_hazmat = !!parseInt($scope.clientProductData.item_hazmat);
                    }
                    //console.log(response);
                }, function (reason) {
                    console.warn(reason);
                })
                .finally(function (data) {
                    //making the loader invisible for user
                    angular.element('.loading').addClass('hidden');
                });
    }

    function getWeightTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getWeightTypes())
                .then(function (response) {
                    if (response.data.success && response.data.weightTypes) {
                        $scope.weightTypes = response.data.weightTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getCommodityTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getCommodityTypes())
                .then(function (response) {
                    if (response.data.success && response.data.commodityTypes) {
                        $scope.commodityTypes = response.data.commodityTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getDimmensionsTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getDimmensionsTypes())
                .then(function (response) {
                    if (response.data.success && response.data.dimmensionsTypes) {
                        $scope.dimmensionsTypes = response.data.dimmensionsTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getTemperatureTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getTemperatureTypes())
                .then(function (response) {
                    if (response.data.success && response.data.temperatureTypes) {
                        $scope.temperatureTypes = response.data.temperatureTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getFreeClassTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getFreeClassTypes())
                .then(function (response) {
                    if (response.data.success && response.data.freeClassTypes) {
                        $scope.freeClassTypes = response.data.freeClassTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getPackageTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getPackageTypes())
                .then(function (response) {
                    if (response.data.success && response.data.packageTypes) {
                        $scope.packageTypes = response.data.packageTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }


}




