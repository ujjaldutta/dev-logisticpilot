'use strict';


function ContractAssocarialController($scope, $http, $timeout, dataService){	
	  
	
	$scope.assocarial=[{id: 'assoc1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:''}];
	$scope.SCACassocarial= [{id: 'SCACassoc1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:''}];
	
	
	
	$scope.assocRecordLimit = 2;
	$scope.assocCurrentPage = 1;
	$scope.currentPagescac = 1;
	$scope.calctype = ['FLAT','PERCENT','MULTIPLY'];
	$scope.SCACassocPagination={current_page:1};
	$scope.assocPagination={current_page:1};
	$scope.currentTab='assoc';
	$scope.assocfilter=[];
	$scope.assocsearchfilter=[];
	$scope.assocfilter.searchAssoc='';
	
	
	
	$scope.saveAssoc = function(params) {
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
					 $scope.assocarial = [];
					 $scope.assocPagination={current_page:1};
				      $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				    });
	
	};
	
	
	$scope.deleteAssoc = function(params,index) {
	
	var lastItem = index;
	//console.log($scope.assocarial);
	

	$scope.assocarial.splice(lastItem);
	
	// $scope.assocarial.push.apply($scope.assocarial);
	 //console.log($scope.assocarial);
						if( typeof $scope.assocarial.accessorial!='undefined')$scope.assocarial.accessorial.splice(lastItem);
						if( typeof $scope.assocarial.rate_calculation!='undefined')$scope.assocarial.rate_calculation.splice(lastItem);
						if( typeof $scope.assocarial.min_rate!='undefined')$scope.assocarial.min_rate.splice(lastItem);
						if( typeof $scope.assocarial.max_rate!='undefined')$scope.assocarial.max_rate.splice(lastItem);
						if( typeof $scope.assocarial.rate_percentage!='undefined')$scope.assocarial.rate_percentage.splice(lastItem);
						if( typeof $scope.assocarial.effective_from!='undefined')$scope.assocarial.effective_from.splice(lastItem);
						if( typeof $scope.assocarial.effective_to!='undefined')$scope.assocarial.effective_to.splice(lastItem);
						if( typeof $scope.assocarial.cid!='undefined')$scope.assocarial.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
		$scope.assocPagination={current_page:1};
		$scope.assocarial = [];
		$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
		
		return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						$scope.assocPagination={current_page:1};
						$scope.assocarial = [];
				      $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				    });
	
	};
	
	
	//this section is for grid in add ratelane
	$scope.addNewAssoc = function() {
	
	
		var newItemNo = $scope.assocarial.length+1;
		$scope.assocarial.push({'id':'assoc'+newItemNo});
		
	  };



/************************* Broadcast function call ********************************/

	
		
		$scope.$on('loadAssocevt', function(e) {
			
		$scope.assocarial = [];
		$scope.SCACassocarial=[];  
		$scope.assocPagination={current_page:1};
		$scope.currentPagescac = 1;
        $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
        $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: 1});
        
		});
	  
	  $scope.resetRow=function(){
		  $scope.assocarial = [];
		  $scope.assocPagination={current_page:1};
		  $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
	  }
	   //load assoc autocomplete
	    $scope.getassoc = function(textval){
				$scope.assoctype = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/assoc-type'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.assoctype.push(item);
						});

					});
					
					
			  };
			  
	$scope.onSelect = function ($item, $model, $label) {
			$scope.$item = $item;
			$scope.$model = $model;
			$scope.$label = $label;
			if($label=='assoc'){
				jQuery("#assoccode").val($scope.$item.id);
				
			}
			
			
		};
	$scope.onSelectAssoc = function ($item, $model, $label) {
			
			$scope.$item = $item;
			$scope.assocfilter.searchAssoc=$scope.$item.id
			$scope.assocPagination.current_page=1
			$scope.assocarial = [];
			//$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
			
			$scope.loadAccessorialsection(dataService.dataObj.profile_id);
			
		};
		
		
		
		
/************************************ infinite Accessorial scroll *********************************************/	
	
$scope.loadAssoc = function(params) {
	if($scope.rightpanel!='Accessorial') return;
if($scope.currentTab!='assoc') return

$scope.assocPagination.current_page=params.page;

var afilter=[{EffectiveFrom: $scope.assocfilter.EffectiveFrom,EffectiveTo:$scope.assocfilter.EffectiveTo,searchAssoc:$scope.assocfilter.searchAssoc}];


      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.assocPagination.current_page > $scope.assocPagination.total_pages;
 
      if (!isTerminal) {
		  
		  

        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-assocarial', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.assocarial =$scope.assocarial;
            
            
			if(typeof $scope.assocarial!='undefined'){
				$scope.assocPagination.total_pages=data.assoc_count;
				$scope.assocarial.push.apply($scope.assocarial, data.Accessorial);
				
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.currentTab!='assoc') return
     
      var page = $scope.assocPagination ? $scope.assocPagination.current_page + 1 : 1;
		$scope.assocPagination.current_page=page;
     
      $scope.loadAssoc({page:page,limit:$scope.assocRecordLimit});
    });
    
    $scope.resetAccRow=function(date){
		
		$scope.assocPagination.current_page=1
			$scope.assocarial = [];
			$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
		}		
	
			
		
	$scope.$watch('assocfilter.EffectiveFrom', function(value, oldValue){
				
				if(value != oldValue){
				
				//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
				//$scope.resetRow();
				//console.log(value);
				//dataService.dataObj.EffectiveFrom=value;
				$scope.loadAccessorialsection(dataService.dataObj.profile_id);
				      
				}
			});	
				
	
	$scope.$watch('assocfilter.EffectiveTo', function(value, oldValue){
				
				if(value != oldValue){
					
				
					//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
					//$scope.resetRow();
					//console.log(value);
					//dataService.dataObj.EffectiveTo=value;
					$scope.loadAccessorialsection(dataService.dataObj.profile_id);
				}
			});	
		
		
		
		
		
		
		
		
		
		
		
/*************************************** SCAC Part ************************************/


	
	$scope.saveSCACAssoc = function(params) {
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
						$scope.SCACassocPagination={current_page:1};
						$scope.SCACassocarial = [];
				      $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
				    });
	
	};
	
	
	$scope.deleteSCACAssoc = function(params,index) {
	
	var lastItem = index;
						$scope.SCACassocarial.splice(lastItem);
	
				if( typeof $scope.SCACassocarial.accessorial!='undefined')$scope.SCACassocarial.accessorial.splice(lastItem);
				if( typeof $scope.SCACassocarial.rate_calculation!='undefined')$scope.SCACassocarial.rate_calculation.splice(lastItem);
				if( typeof $scope.SCACassocarial.min_rate!='undefined')$scope.SCACassocarial.min_rate.splice(lastItem);
				if( typeof $scope.SCACassocarial.max_rate!='undefined')$scope.SCACassocarial.max_rate.splice(lastItem);
				if( typeof $scope.SCACassocarial.rate_percentage!='undefined')$scope.SCACassocarial.rate_percentage.splice(lastItem);
				if( typeof $scope.SCACassocarial.effective_from!='undefined')$scope.SCACassocarial.effective_from.splice(lastItem);
				if( typeof $scope.SCACassocarial.effective_to!='undefined')$scope.SCACassocarial.effective_to.splice(lastItem);
				if( typeof $scope.SCACassocarial.cid!='undefined')$scope.SCACassocarial.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
			$scope.SCACassocPagination={current_page:1};
			$scope.SCACassocarial = [];
			$scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
			return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						
						$scope.SCACassocPagination={current_page:1};
						$scope.SCACassocarial = [];
				      $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
				    });
	
	};
	
	
	//this section is for grid in add ratelane
	$scope.addNewSCACAssoc = function() {
	
		var newItemNo = $scope.SCACassocarial.length+1;
		$scope.SCACassocarial.push({'id':'SCACassoc'+newItemNo});
		
	  };
	  
	  
	  
	  
	  
	  
	  $scope.loadSCACAssoc = function(params) {
	if($scope.rightpanel!='Accessorial') return;
	if($scope.currentTab!='scac') return
	
	$scope.SCACassocPagination.current_page=params.page;
	
	$scope.lfilters=[{Statecode:$scope.Statename,Citycode:$scope.Citycode}];
	
      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:$scope.lfilters},
          isTerminal = $scope.SCACassocPagination &&
                       $scope.SCACassocPagination.current_page >= $scope.SCACassocPagination.total_pages_scac &&
                       $scope.SCACassocPagination.current_page <= 1;
    
      // Determine if there is a need to load a new page
      if (!isTerminal) {
        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-assocarial', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.SCACassocarial =$scope.SCACassocarial;
			if(typeof $scope.SCACassocarial!='undefined'){
           $scope.SCACassocPagination.total_pages_scac=data.scac_count;
            $scope.SCACassocarial.push.apply($scope.SCACassocarial, data.SCAC_Assoc);
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.currentTab!='scac') return
    
      var page = $scope.SCACassocPagination ? $scope.SCACassocPagination.current_page + 1 : 1;
		$scope.SCACassocPagination.current_page=page;
     
      $scope.loadSCACAssoc({page:page,limit:$scope.assocRecordLimit});
    });


}
