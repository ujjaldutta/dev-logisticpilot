function CustomerDashabordSetupController(Customer, User, CustomerLocation, CustomerProduct, CarrierApiService, $scope, $timeout){
	$scope.recordLimit = 50; 
	$scope.timeoutMS = 3000;
	
	//customer overview module
	$scope.customerData = {count: -1, list: []};
	
	//user overview module
	$scope.userData = {count: -1, list: []};
	
	//customer location overview module
	$scope.customerLocationData = {count: -1, list: []};
	
	//customer product overview module
	$scope.customerProductData = {count: -1, list: []};

	//we will store all of our form data in this object
	$scope.customerCarrierData = {count: -1, list: []};

	var defaultParams = {limit: $scope.recordLimit,  orderType: 'asc'}; 
	
	//now loading customers
	fetchCustomers(angular.extend({orderBy: 'client_firstname'}, defaultParams));
	
	//executing factory function to fetch all records at once using async calls
	loadAllData();

	$scope.reloadPanel = function(section){
		switch(section){
			case 'ccl':
				fetchCustomers(angular.extend({orderBy: 'client_firstname'}, defaultParams), 0);
				break;
			case 'prd':
				fetchProducts(angular.extend({orderBy: 'item_name'}, defaultParams), 0);
				break;
			case 'usr':
				fetchUsers(angular.extend({orderBy: 'usr_firstname' }, defaultParams), 0);
				break;
			case 'lcn':
				fetchLocations(angular.extend({orderBy: 'location_name'}, defaultParams), 0);
				break;
			case 'crr':
				fetchCarriers(defaultParams, 0);
				break;
		}
	}

	$scope.resetCustomerSelection = function(delay){
		//removing any selected customer 
		angular.element(document.querySelectorAll('.entries a.selectedCust')).removeClass('selectedCust');
		loadAllData(delay);
	}

	$scope.changeCustomer = function(custId, event){
		angular.element(document.querySelectorAll('.entries a.selectedCust')).removeClass('selectedCust');
		angular.element(event.currentTarget).addClass('selectedCust');
		
		//fetching users
		fetchUsers(angular.extend({orderBy: 'usr_firstname', clientId: custId}, defaultParams), 0);
		//fetching customer locations
		fetchLocations(angular.extend({orderBy: 'location_name', clientId: custId}, defaultParams), 0);
		//fetching customer products
		fetchProducts(angular.extend({orderBy: 'item_name', clientId: custId}), 0);
		//fetching customer carriers
		fetchCarriers(angular.extend({clientId: custId}, defaultParams), 0);
	}
	
	function loadAllData(delay){
		//fetching users
		fetchUsers(angular.extend({orderBy: 'usr_firstname' }, defaultParams), delay);
		//fetching customer locations
		fetchLocations(angular.extend({orderBy: 'location_name'}, defaultParams), delay);
		//fetching customer products
		fetchProducts(angular.extend({orderBy: 'item_name'}, defaultParams), delay);
		//fetching customer carriers
		fetchCarriers(defaultParams, delay);
	}
	
	function fetchCustomers(params, timeOutAfter){
		$timeout(function(){
			//making the loader visible for user
			angular.element(document.querySelector('#loading-customer-section')).removeClass('hidden');
			Customer.makeAsyncRequest(Customer.listCustomer(params))
				.then(function(response){
					if(response.data.success && response.data.count !== undefined && response.data.results){
						$scope.customerData.count = response.data.count;
						$scope.customerData.list = response.data.results ? response.data.results : [];
					}
					//console.log(result);
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
					//making the loader invisible for user
					angular.element(document.querySelector('#loading-customer-section')).addClass('hidden');
				});
		}, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
		
	}
	
	function fetchUsers(params, timeOutAfter){
		$timeout(function(){
			//making the loader visible for user
			angular.element(document.querySelector('#loading-user-section')).removeClass('hidden');
			User.makeAsyncRequest(User.listUser(params))
				.then(function(response){
					if(response.data.success && response.data.count !== undefined && response.data.results){
						$scope.userData.count = response.data.count;
						$scope.userData.list = response.data.results ? response.data.results : [];
					}
					//console.log(result);
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
					//making the loader invisible for user
					angular.element(document.querySelector('#loading-user-section')).addClass('hidden');
				});
		}, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
	}
	
	function fetchLocations(params, timeOutAfter){
		$timeout(function(){
			//making the loader visible for user
			angular.element(document.querySelector('#loading-customer-location-section')).removeClass('hidden');
			CustomerLocation.makeAsyncRequest(CustomerLocation.listLocations(params))
				.then(function(response){
					if(response.data.success && response.data.count !== undefined && response.data.results){
						$scope.customerLocationData.count = response.data.count;
						$scope.customerLocationData.list = response.data.results ? response.data.results : [];
					}
					//console.log(result);
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
					//making the loader invisible for user
					angular.element(document.querySelector('#loading-customer-location-section')).addClass('hidden');
				});
		}, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
	}

	function fetchProducts(params, timeOutAfter) {
		$timeout(function(){
	        //making the loader visible for user
			angular.element(document.querySelector('#loading-customer-product-section')).removeClass('hidden');
	        
	        CustomerProduct.makeAsyncRequest(CustomerProduct.listProducts(params))
	                .then(function (response) {
	                    if (response.data.success && response.data.count !== undefined && response.data.results) {
	                        $scope.customerProductData.count = response.data.count;
	                        $scope.customerProductData.list = response.data.results ? response.data.results : [];
	                    }
	                    //console.log(result);
	                }, function (reason) {
	                    console.warn(reason);
	                }).finally(function (data) {
	            //making the loader invisible for user
				angular.element(document.querySelector('#loading-customer-product-section')).addClass('hidden');
	        });
        }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
    }

    function fetchCarriers(params, timeOutAfter){
    	$timeout(function(){
    		var limit = null, clientId = null;
    		 //making the loader visible for user
			angular.element(document.querySelector('#loading-customer-carriers-section')).removeClass('hidden');

			if(params.limit)
				limit = params.limit;
			if(params.clientId)
				clientId = params.clientId;


			CarrierApiService.fecthSelectedCarriers(false, null, false, limit, clientId) //calling factory to make ajax request
				.success(function(result){
					 if(result.success && result.carriers && result.carrierlist && result.count !== undefined){
						//$scope.selectedCarrierList = result.carriers;
						$scope.customerCarrierData.list = result.carriers;
						$scope.customerCarrierData.count = result.count;
					}
				}).finally(function (data) {
		            //making the loader invisible for user
					angular.element(document.querySelector('#loading-customer-carriers-section')).addClass('hidden');
		        });
		 }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
	}
}