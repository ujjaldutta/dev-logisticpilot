function registrationStep2Controller($scope, $http, SyncCarriers){
	//we will store all of our form data in this object
	$scope.formStep2 = {saveButtonText: 'Next', location: null};
	
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOptionsBilling = {
       types: ["(regions)"]
    }

	//fetchinh already saved billing information if any
	fetchBillingDetails();

	//function to handle step 2
	$scope.submitStep2 = function(form){
		if(form && form.$valid){
			//flag to enable loader for registration process
			$scope.processing.status = true;
			
			$scope.formStep2.saveButtonText = 'Saving...';
			
			$http.post('/front/client-register', $scope.formStep2)
				.success(function (data){
					$scope.formStep2.saveButtonText = 'Next';
					if(data.success){
						//$scope.formData.status = true;
						//$scope.formData.billInfoID = data.billInfoID;
						
						//disabling loader after complete
						$scope.processing.status = false;
					}else if(data.error){
						$scope.error.status = true;
						$scope.error.message = data.error;

						//disabling loader after complete
						$scope.processing.status = false;
					}
				})
				.error(function(data, status){
					alert(status);
				});
		}
	};

	//method to handle save and continue later function
	$scope.saveAndExit = function(form){
		if(form !== undefined)
			$scope.submitStep2(form);
		
		window.location.href='/auth/logout';
	};

	$scope.billCountrySelected = function(selected){
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.formStep2.billto_country = selected.title;
	};

	//method to handle selected postcodes
	$scope.billPostcodeSelected = function(selected) {
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.formStep2.billto_postal = selected.title;
		$scope.formStep2.billto_city = selected.originalObject.city_name;
		$scope.formStep2.billto_state = selected.originalObject.state_code;
		$scope.formStep2.billto_country = selected.originalObject.country_code;

		$scope.$broadcast('setOutsideValue', {id: 'billto_city', value: selected.originalObject.city_name});
		$scope.$broadcast('setOutsideValue', {id: 'billto_country', value: selected.originalObject.country_code});
		$scope.$broadcast('setOutsideValue', {id: 'billto_state', value: selected.originalObject.state_code});
		
    };

    $scope.billCitySelected = function(selected){
    	if(selected === undefined)
			return;

		//console.log(selected);
		$scope.formStep2.billto_city = selected.title;
    };
	
	$scope.billStateSelected = function(selected){
		if(selected === undefined)
			return;
		
    	//console.log(selected);
    	$scope.formStep2.billto_state = selected.title;
    };

    $scope.billCountryChanged = function(str){
    	$scope.formStep2.billto_country = str;
    };
    
    $scope.billPostcodeChanged = function(str){
    	$scope.formStep2.billto_postal = str;
    };
    $scope.billStateChanged = function(str){
    	$scope.formStep2.billto_state = str;
    };
    $scope.billCityChanged = function(str){
    	$scope.formStep2.billto_city = str;
    };



     function fetchBillingDetails(){
    	SyncCarriers.getStepBillingDetails()
    		.success(function(result){
    			if(result.success){
    				$scope.formStep2 = angular.extend($scope.formStep2, result.billing);
    				var location = '';
    				if(result.billing.billto_city)
    					location = result.billing.billto_city;

    				if(result.billing.billto_state)
    					location += ', ' + result.billing.billto_state;

    				if(result.billing.billto_postal)
    					location += ', ' + result.billing.billto_postal;

    				if(result.billing.billto_country)
    					location += ', ' + result.billing.billto_country;

    				$scope.formStep2.location = location;
    			}
    		});
    }
}