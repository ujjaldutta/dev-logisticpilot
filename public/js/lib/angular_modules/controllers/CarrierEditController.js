'use strict';

function CarrierEditController(CarrierAPIEndpoint, Carrier, Upload, $scope, $location, $window) {
    //model attribute to show/hide error message notification
    $scope.error = {
        status: false,
        message: '',
    };

    $scope.success = {
        status: false,
        message: '',
    };

    //google autocomplete config to search specific type of information
    $scope.autocompleteOption = {
        componentRestrictions: {},
        types: ["(regions)"]
    }

    //collection  of carrier type objects
    $scope.carrierData = {};
    $scope.carrierInsurances = {};
    $scope.carrierModes = {};
    $scope.insuranceTypes = [];
    $scope.modeTypes = [];
    $scope.allFields = [];

    getCarrierTypes();
    getCurrencyTypes();

    // Settings combox sidebar
    $scope.filters = [];
    $scope.filter = {};
    $scope.filter.insId = {};
    $scope.filter.insurancetTypes = [];
    $scope.insSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.modId = {};
    $scope.filter.equipmentTypes = [];
    $scope.modSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.equipId = {};
    $scope.filter.modeTypes = [];
    $scope.equipmentSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    // Settings combox
    $scope.carrierData.carrier_type = {};
    $scope.carrierTypes = [];
    $scope.carrSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };
    $scope.carrierData.carrier_currencycode = {};
    $scope.currencyTypes = [];
    $scope.currencySettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    // 4th tab (Carrier Equipment): Set params 
    $scope.equipmentData = {count: 0, list: []};
    $scope.TotalEquipment = [];

    $scope.recordLimit = 10;
    $scope.currentPage = 1;

    $scope.predicate = 'equip_no';
    $scope.reverse = false;

    // 5th tab (Carrier Remit): Set params 
    $scope.remitData = {count: 0, list: []};
    $scope.TotalRemit = [];

    $scope.recordLimitRemit = 10;
    $scope.currentPageRemit = 1;

    $scope.predicateRemit = 'remit_name';
    $scope.reverseRemit = false;

    /* method to dismiss the alert */
    $scope.hideMessage = function () {
        $scope.error = {
            status: false,
            message: '',
        };

        $scope.success = {
            status: false,
            message: '',
        };
    }

    $scope.isExistCarrID = function () {
        if (!$scope.carrID) {
            return true;
        }
        else {
            return false;
        }
    };

    $scope.cancelEditing = function () {
        $window.location.href = CarrierAPIEndpoint.base ? CarrierAPIEndpoint.base : '/';
    }

    /*
     * when valid carr id is loaded or assigned 
     * after dom rendered make a aysnc req to the server 
     * to get the information
     */
    $scope.$watch('carrID', function (val) {
        if (!isNaN(val) && val > 0) {
            //fetching the selected carrier data
            getCarrierData(val);
        }

    });

    // TODO
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/carriers/upload',
                    fields: {
                        'carrierID': $scope.carrID
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.carrierData.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                    //evt.config.file.name + '\n' + $scope.formData.log;
                }).success(function (data, status, headers, config) {
                    if (data.error) {
                        $scope.error.status = true;
                        $scope.error.message = data.error;
                    } else if (data.success) {
                        $scope.success.status = true;
                        $scope.success.message = 'file ' + config.file.name + ' uploaded successfully!';
                    }

                    //$scope.$apply();
                });
            }
        }
    };

    function getCarrierData(carrID) {
        if (!carrID)
            carrID = $location.search().carrID || 0;

        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');

        Carrier.makeAsyncRequest(Carrier.getCarrierData(carrID))
                .then(function (response) {
                    if (response.data.success && response.data.carrier) {
                        $scope.carrierData = response.data.carrier;
                        //filling the location input with the already selected location details but saved separately
                        $scope.carrierData.location = __fillLocationDetails($scope.carrierData, 'carrier');
                        //console.log($scope.carrierData);                                                
                        $scope.carrierData.carrier_rateapi = !!parseInt($scope.carrierData.carrier_rateapi);
                        $scope.carrierData.carrier_bookingapi = !!parseInt($scope.carrierData.carrier_bookingapi);
                        $scope.carrierData.carrier_trackingapi = !!parseInt($scope.carrierData.carrier_trackingapi);
                        $scope.carrierData.carrier_imageapi = !!parseInt($scope.carrierData.carrier_imageapi);
                        $scope.carrierData.carrier_smartwaypay = !!parseInt($scope.carrierData.carrier_smartwaypay);
                        $scope.carrierData.carrier_mexicoauth = !!parseInt($scope.carrierData.carrier_mexicoauth);
                        $scope.carrierData.carrier_canadaauth = !!parseInt($scope.carrierData.carrier_canadaauth);
                        $scope.carrierData.carrier_acceptbid = !!parseInt($scope.carrierData.carrier_acceptbid);
                        $scope.carrierData.carrier_safetyrating = !!parseInt($scope.carrierData.carrier_safetyrating);
                        $scope.carrierData.carrier_hazmat = !!parseInt($scope.carrierData.carrier_hazmat);

                        $scope.carrierData.carrier_type = {id: $scope.carrierData.carrier_type};
                        $scope.carrierData.carrier_currencycode = {id: $scope.carrierData.carrier_currencycode};

                        //again making async request to get other details like insurances
                        return Carrier.makeAsyncRequest(Carrier.getCarrierInsurances(carrID));
                    }
                    //console.log(response);}, function(reason){
                    console.warn(reason);
                })
                //callback for insurance
                .then(function (response) {
                    if (response && response.data.success && response.data.carrierInsurances) {
                        if (response.data.carrierInsurances.length > 0) {
                            $scope.carriers.insurances = response.data.carrierInsurances;

                            for (var i = 0; i < $scope.carriers.insurances.length; i++) {
                                //processing locations devided into diff attributes to place it one place
                                $scope.carriers.insurances[i].insurance_location = __fillLocationDetails($scope.carriers.insurances[i], 'insurance');
                            }
                        }

                        //again making async request to get other details like modes
                        return Carrier.makeAsyncRequest(Carrier.getCarrierModes(carrID));
                    }
                }, function (reason) {
                    console.warn(reason);
                })
                .then(function (response) {
                    if (response && response.data.success && response.data.carrierModes) {
                        if (response.data.carrierModes.length > 0) {
                            $scope.carriers.modes = response.data.carrierModes;
                        }

                        var params = {limit: $scope.recordLimit, page: $scope.currentPage};
                        return Carrier.makeAsyncRequest(Carrier.listEquipment(carrID, params));
                    }
                }, function (reason) {
                    console.warn(reason);
                })
                .then(function (response) {
                    if (response && response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.equipmentData.count = response.data.count;
                        $scope.equipmentData.list = response.data.results ? response.data.results : [];

                        var params = {limit: $scope.recordLimitRemit, page: $scope.currentPageRemit};
                        return Carrier.makeAsyncRequest(Carrier.listRemit(carrID, params));
                    }
                }, function (reason) {
                    console.warn(reason);
                })
                .then(function (response) {
                    if (response && response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.remitData.count = response.data.count;
                        $scope.remitData.list = response.data.results ? response.data.results : [];
                    }
                }, function (reason) {
                    console.warn(reason);
                })
                .finally(function (data) {
                    //making the loader invisible for user
                    angular.element('.loading').addClass('hidden');
                });
    }

    function getInsuranceTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllInsurancesTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.insurancestype !== undefined) {
                        $scope.insuranceTypes = response.data.insurancestype;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getModeTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllModesTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.modestype !== undefined) {
                        $scope.modeTypes = response.data.modestype;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getCarrierTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllCarrierTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.carrierType !== undefined) {
                        $scope.carrierTypes = response.data.carrierType;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function getCurrencyTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllCurrencyTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.currencyType !== undefined) {
                        $scope.currencyTypes = response.data.currencyType;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }


    /*
     * Saving the carrier fields
     */
    $scope.saveCarrierData = function (form) {
        if (form && form.$valid) {
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');

            Carrier.postCarrierData($scope.carrID, $scope.carrierData)
                    .then(function (response) {
                        if (response.data.success && response.data.carrId) {
                            $scope.carrID = response.data.carrId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';
                        } else if (response.data.error !== undefined) {
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = Carrier.formatValidationErrors(response.data.error);
                            } else
                                $scope.error.message = response.data.error;
                        }
                        //console.log(response);
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }

    /*
     * 2nd Tab: Carrier Insurance 
     */


    $scope.carriers = {};

    $scope.carriers.insurances = [{}];

    getInsuranceTypes();

    $scope.addItem = function () {
        $scope.carriers.insurances.push({});
    }

    $scope.removeItem = function (item) {
        if (confirm('Are you sure to remove this item?'))
            $scope.carriers.insurances.splice(item, 1);
    }

    //method to save insurances
    $scope.saveInsurances = function (form) {
        if (form && form.$valid && $scope.carrID !== "") { //
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');
            Carrier.saveInsurances($scope.carrID, $scope.carriers.insurances)
                    .then(function (response) {
                        if (response.data.success && response.data.carrId) {
                            $scope.carrID = response.data.carrId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';

                            //again making async request to refresh insurances and get new ids
                            return Carrier.makeAsyncRequest(Carrier.getCarrierInsurances($scope.carrID));
                        } else if (response.data.error !== undefined) {
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = Carrier.formatValidationErrors(response.data.error);
                            } else
                                $scope.error.message = response.data.error;
                        }
                        //console.log(response);
                    })
                    //callback for insurance
                    .then(function (response) {
                        if (response && response.data.success && response.data.carrierInsurances && response.data.carrierInsurances.length > 0) {
                            $scope.carriers.insurances = response.data.carrierInsurances;

                            for (var i = 0; i < $scope.carriers.insurances.length; i++) {
                                //processing locations devided into diff attributes to place it one place
                                $scope.carriers.insurances[i].insurance_location = __fillLocationDetails($scope.carriers.insurances[i], 'insurance');
                            }
                        }
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }

    /*
     * 3rd Tab: Carrier Mode 
     */
    getModeTypes();

    $scope.carriers.modes = [{}];

    $scope.addModeItem = function () {
        $scope.carriers.modes.push({});
    }

    $scope.removeModeItem = function (item) {
        if (confirm('Are you sure to remove this item?'))
            $scope.carriers.modes.splice(item, 1);
    }

    $scope.saveModes = function (form) {
        if (form && form.$valid && $scope.carrID !== "") { //
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');
            Carrier.saveModes($scope.carrID, $scope.carriers.modes)
                    .then(function (response) {
                        if (response.data.success && response.data.carrId) {
                            $scope.carrID = response.data.carrId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';

                            //again making async request to refresh modes and get new ids
                            return Carrier.makeAsyncRequest(Carrier.getCarrierModes($scope.carrID));
                        } else if (response.data.error !== undefined) {
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = Carrier.formatValidationErrors(response.data.error);
                            } else
                                $scope.error.message = response.data.error;
                        }
                        //console.log(response);
                    })
                    //callback for insurance
                    .then(function (response) {
                        if (response && response.data.success && response.data.carrierModes && response.data.carrierModes.length > 0) {
                            $scope.carriers.modes = response.data.carrierModes;
                        }
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }

    /*
     * 4th Tab: Carrier Equipment 
     */
    $scope.sortColumn = function (predicate) {
        $scope.reverse = false;

        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.pageChanged = function () {
        fetchEquipments($scope.carrID, {limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
    }

    $scope.checkAll = function () {
        angular.forEach($scope.equipmentData.list, function (value, key) {
            if ($scope.equipmentData.selectAll === true)
                value.selected = true;
            else
                value.selected = false;
        });
    }

    $scope.checkSelection = function (index) {
        if ($scope.equipmentData.list[index].selected === true) {
            //now checking whether

            //total no of items on this current page
            var totalItems = $scope.equipmentData.list.length,
                    selectedItems = 0;

            //scanning all items for checking
            angular.forEach($scope.equipmentData.list, function (value, key) {
                if (value.selected === true)
                    selectedItems++;
            });

            if (totalItems == selectedItems)
                $scope.equipmentData.selectAll = true;

        } else
            $scope.equipmentData.selectAll = false;
    }

    $scope.deleteAllEquipment = function () {
        //scanning all items for checking
        if ($window.confirm('Are you sure ?')) {
            angular.forEach($scope.equipmentData.list, function (value, key) {
                if (value.selected === true) {
                    $scope.deleteEquipment(value.id, false);
                }
            });
        }
    }

    $scope.deleteEquipment = function (equipId, showAlert) {
        var promiseObj = null;

        if (showAlert === true && $window.confirm('Are you sure to delete?')) {
            promiseObj = Carrier.makeAsyncRequest(Carrier.deleteEquipment(equipId));
        } else if (showAlert === false)
            promiseObj = Carrier.makeAsyncRequest(Carrier.deleteEquipment(equipId));

        //processing current list after successfull delete
        if (promiseObj) {
            promiseObj.then(function (response) {
                //console.log(response);
                if (response.data.success) {
                    fetchEquipments($scope.carrID, {limit: $scope.recordLimit, page: $scope.currentPage});
                }
            }, function (reason) {
                console.warn(reason);
            });
        }
    }

    function fetchEquipments(carrID, params) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');
        Carrier.makeAsyncRequest(Carrier.listEquipment(carrID, params))
                .then(function (response) {
                    if (response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.equipmentData.count = response.data.count;
                        $scope.equipmentData.list = response.data.results ? response.data.results : [];
                    }
                    //console.log(result);
                }, function (reason) {
                    console.warn(reason);
                }).finally(function (data) {
            //making the loader invisible for user
            angular.element('.loading').addClass('hidden');
        });
    }


    /*
     * 5th Tab: Carrier Remit 
     */

    $scope.sortColumn = function (predicate) {
        $scope.reverseRemit = false;

        $scope.reverseRemit = ($scope.predicateRemit === predicate) ? !$scope.reverseRemit : false;
        $scope.predicateRemit = predicate;
    };

    $scope.pageChanged = function () {
        fetchRemits($scope.carrID, {limit: $scope.recordLimitRemit, page: $scope.currentPageRemit, orderBy: $scope.predicateRemit, orderType: $scope.reverseRemit ? 'desc' : 'asc'});
    }

    $scope.checkAll = function () {
        angular.forEach($scope.remitData.list, function (value, key) {
            if ($scope.remitData.selectAll === true)
                value.selected = true;
            else
                value.selected = false;
        });
    }

    $scope.checkSelection = function (index) {
        if ($scope.remitData.list[index].selected === true) {
            //now checking whether

            //total no of items on this current page
            var totalItems = $scope.remitData.list.length,
                    selectedItems = 0;

            //scanning all items for checking
            angular.forEach($scope.remitData.list, function (value, key) {
                if (value.selected === true)
                    selectedItems++;
            });

            if (totalItems == selectedItems)
                $scope.remitData.selectAll = true;

        } else
            $scope.remitData.selectAll = false;
    }

    $scope.deleteAllRemit = function () {
        //scanning all items for checking
        if ($window.confirm('Are you sure ?')) {
            angular.forEach($scope.remitData.list, function (value, key) {
                if (value.selected === true) {
                    $scope.deleteRemit(value.id, false);
                }
            });
        }
    }

    $scope.deleteRemit = function (remitId, showAlert) {
        var promiseObj = null;

        if (showAlert === true && $window.confirm('Are you sure to delete?')) {
            promiseObj = Carrier.makeAsyncRequest(Carrier.deleteRemit(remitId));
        } else if (showAlert === false)
            promiseObj = Carrier.makeAsyncRequest(Carrier.deleteRemit(remitId));

        //processing current list after successfull delete
        if (promiseObj) {
            promiseObj.then(function (response) {
                //console.log(response);
                if (response.data.success) {
                    fetchRemits($scope.carrID, {limit: $scope.recordLimitRemit, page: $scope.currentPageRemit});
                }
            }, function (reason) {
                console.warn(reason);
            });
        }
    }

    function fetchRemits(carrID, params) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');
        Carrier.makeAsyncRequest(Carrier.listRemit(carrID, params))
                .then(function (response) {
                    if (response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.remitData.count = response.data.count;
                        $scope.remitData.list = response.data.results ? response.data.results : [];
                    }
                    //console.log(result);
                }, function (reason) {
                    console.warn(reason);
                }).finally(function (data) {
            //making the loader invisible for user
            angular.element('.loading').addClass('hidden');
        });
    }

    function __fillLocationDetails(collection, prf) {

        var location = '';
        var city = prf + '_city';
        var state = prf + '_state';
        var postal = prf + '_postal';
        var country = prf + '_country';

        if (collection[city] !== undefined && collection[city])
            location += collection[city] + ', ';
        if (collection[state] !== undefined && collection[state])
            location += collection[state] + ', ';
        if (collection[postal] !== undefined && collection[postal])
            location += collection[postal] + ', ';
        if (collection[country] !== undefined && collection[country])
            location += collection[country];

        return location;
    }
}