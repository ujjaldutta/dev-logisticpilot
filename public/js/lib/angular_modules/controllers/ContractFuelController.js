'use strict';
function ContractFuelController ($scope, $http, $window){
	
	
	/*****************************************************************************************************************************************/



$scope.fuel=[{id: 'fuel1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:''}];
	$scope.SCACfuel= [{id: 'SCACfuel1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:''}];
	
	
	
	$scope.fuelRecordLimit = 2;
	$scope.fuelCurrentPage = 1;
	$scope.fuelcurrentPagescac = 1;
	$scope.calctype = ['FLAT','PERCENT','MULTIPLY'];
	$scope.SCACfuelPagination={current_page:1};
	$scope.fuelPagination={current_page:1};
	$scope.fuelcurrentTab='fuel';
	$scope.fuelfilter=[];
	$scope.fuelsearchfilter=[];
	$scope.fuelfilter.searchFuel='';
	$scope.fuelassoccode=''
	
	
	$scope.saveFuel = function(params) {
	params.accessorial_code=$scope.fuelassoccode;
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
					 $scope.fuel = [];
					 $scope.fuelPagination={current_page:1};
				      $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				    });
	
	};
	
/*************************************************Delete Fuel**************************************************/	
	$scope.deleteFuel = function(params,index) {
	
	var lastItem = index;
	//console.log($scope.fuel);
	

	$scope.fuel.splice(lastItem);
	
	// $scope.fuel.push.apply($scope.fuel);
	 //console.log($scope.fuel);
						if( typeof $scope.fuel.accessorial!='undefined')$scope.fuel.accessorial.splice(lastItem);
						if( typeof $scope.fuel.rate_calculation!='undefined')$scope.fuel.rate_calculation.splice(lastItem);
						if( typeof $scope.fuel.min_rate!='undefined')$scope.fuel.min_rate.splice(lastItem);
						if( typeof $scope.fuel.max_rate!='undefined')$scope.fuel.max_rate.splice(lastItem);
						if( typeof $scope.fuel.rate_percentage!='undefined')$scope.fuel.rate_percentage.splice(lastItem);
						if( typeof $scope.fuel.effective_from!='undefined')$scope.fuel.effective_from.splice(lastItem);
						if( typeof $scope.fuel.effective_to!='undefined')$scope.fuel.effective_to.splice(lastItem);
						if( typeof $scope.fuel.cid!='undefined')$scope.fuel.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
		$scope.fuelPagination={current_page:1};
		$scope.fuel = [];
		$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		
		return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						$scope.fuelPagination={current_page:1};
						$scope.fuel = [];
				      $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewFuel = function() {
	
	
		var newItemNo = $scope.fuel.length+1;
		$scope.fuel.push({'id':'fuel'+newItemNo});
		
	  };



/************************* Reset Row ********************************/


	  $scope.resetRow=function(){
		  $scope.fuel = [];
		  $scope.fuelPagination={current_page:1};
		  $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		  
		  
		  $scope.SCACfuel = [];
		  $scope.SCACfuelPagination={current_page:1};
		  $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: 1});
		  
	  }
	   //load fuel autocomplete
	    $scope.getfuel = function(textval){
				$scope.fueltype = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/assoc-type'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.fueltype.push(item);
						});

					});
					
					
			  };
			  
	$scope.onSelect = function ($item, $model, $label) {
			$scope.$item = $item;
			$scope.$model = $model;
			$scope.$label = $label;
			if($label=='fuel'){
				//jQuery("#fuelcode").val($scope.$item.id);
				$scope.fuelassoccode=$scope.$item.id
			}
			
			
		};
	$scope.onSelectFuel = function ($item, $model, $label) {
			
			$scope.$item = $item;
			$scope.fuelfilter.searchFuel=$scope.$item.id
			$scope.fuelPagination.current_page=1
			$scope.fuel = [];
			//$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
			
			//$scope.loadFuelsection(dataService.dataObj.profile_id);
			$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
			
		};
		
		
		
		
/************************************ infinite Fuel scroll *********************************************/	
	
$scope.loadFuel = function(params) {
	if($scope.rightpanel!='Fuel') return;
if($scope.fuelcurrentTab!='fuel') return

$scope.fuelPagination.current_page=params.page;

var afilter=[{EffectiveFrom: $scope.fuelfilter.EffectiveFrom,EffectiveTo:$scope.fuelfilter.EffectiveTo,searchFuel:$scope.fuelfilter.searchFuel}];


      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.fuelPagination.current_page > $scope.fuelPagination.total_pages;
 
      if (!isTerminal) {
		  
		  

        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-fuel', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.fuel =$scope.fuel;
            
            
			if(typeof $scope.fuel!='undefined'){
				$scope.fuelPagination.total_pages=data.fuel_count;
				$scope.fuel.push.apply($scope.fuel, data.Fuel);
				
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.fuelcurrentTab!='fuel') return
     
      var page = $scope.fuelPagination ? $scope.fuelPagination.current_page + 1 : 1;
		$scope.fuelPagination.current_page=page;
     
      $scope.loadFuel({page:page,limit:$scope.fuelRecordLimit});
    });
    
    $scope.resetAccRow=function(date){
		
		$scope.fuelPagination.current_page=1
			$scope.fuel = [];
			$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		}		
	
/**********************************************Filters*****************************************************/			
		
	$scope.$watch('assocfilter.EffectiveFrom', function(value, oldValue){
				
				if(value != oldValue){
				
				//$scope.loadFuelsection(dataService.dataObj.profile_id);
				//$scope.resetRow();
				//console.log(value);
				//dataService.dataObj.EffectiveFrom=value;
				//$scope.loadFuelsection(dataService.dataObj.profile_id);
				$scope.fuelPagination.current_page=1
				$scope.fuel = [];
				$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				      
				}
			});	
				
	
	$scope.$watch('assocfilter.EffectiveTo', function(value, oldValue){
				
				if(value != oldValue){
					
				
					//$scope.loadFuelsection(dataService.dataObj.profile_id);
					//$scope.resetRow();
					//console.log(value);
					//dataService.dataObj.EffectiveTo=value;
					//$scope.loadFuelsection(dataService.dataObj.profile_id);
					$scope.fuelPagination.current_page=1
					$scope.fuel = [];
					$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				}
			});	
		
		
		
		
		
		
		
		
		
		
		
/*************************************** SCAC Part ************************************/


	
	$scope.saveSCACFuel = function(params) {
	params.accessorial_code=$scope.fuelassoccode;
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
						$scope.SCACfuelPagination={current_page:1};
						$scope.SCACfuel = [];
				      $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
				    });
	
	};
	
/*******************************************DELETE SCAC********************************************************/
	$scope.deleteSCACFuel = function(params,index) {
	
	var lastItem = index;
						$scope.SCACfuel.splice(lastItem);
	
				if( typeof $scope.SCACfuel.accessorial!='undefined')$scope.SCACfuel.accessorial.splice(lastItem);
				if( typeof $scope.SCACfuel.rate_calculation!='undefined')$scope.SCACfuel.rate_calculation.splice(lastItem);
				if( typeof $scope.SCACfuel.min_rate!='undefined')$scope.SCACfuel.min_rate.splice(lastItem);
				if( typeof $scope.SCACfuel.max_rate!='undefined')$scope.SCACfuel.max_rate.splice(lastItem);
				if( typeof $scope.SCACfuel.rate_percentage!='undefined')$scope.SCACfuel.rate_percentage.splice(lastItem);
				if( typeof $scope.SCACfuel.effective_from!='undefined')$scope.SCACfuel.effective_from.splice(lastItem);
				if( typeof $scope.SCACfuel.effective_to!='undefined')$scope.SCACfuel.effective_to.splice(lastItem);
				if( typeof $scope.SCACfuel.cid!='undefined')$scope.SCACfuel.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
			$scope.SCACfuelPagination={current_page:1};
			$scope.SCACfuel = [];
			$scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
			return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						
						$scope.SCACfuelPagination={current_page:1};
						$scope.SCACfuel = [];
				      $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewSCACFuel = function() {
	
		var newItemNo = $scope.SCACfuel.length+1;
		$scope.SCACfuel.push({'id':'SCACfuel'+newItemNo});
		
	  };
	  
	  
	  
	  
/*****************************************Load SCAC**********************************************************/	  
	  
	  $scope.loadSCACFuel = function(params) {
	if($scope.rightpanel!='Fuel') return;
	if($scope.fuelcurrentTab!='scac') return
	
	$scope.SCACfuelPagination.current_page=params.page;
	
	var afilter=[{EffectiveFrom: $scope.fuelfilter.EffectiveFrom,EffectiveTo:$scope.fuelfilter.EffectiveTo,searchFuel:$scope.fuelfilter.searchFuel}];
	
      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.SCACfuelPagination &&
                       $scope.SCACfuelPagination.current_page >= $scope.SCACfuelPagination.total_pages_scac &&
                       $scope.SCACfuelPagination.current_page <= 1;
    isTerminal = $scope.SCACfuelPagination.current_page > $scope.SCACfuelPagination.total_pages;
    
          
      // Determine if there is a need to load a new page
      if (!isTerminal) {
        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-fuel', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.SCACfuel =$scope.SCACfuel;
			if(typeof $scope.SCACfuel!='undefined'){
           $scope.SCACfuelPagination.total_pages_scac=data.scac_count;
            $scope.SCACfuel.push.apply($scope.SCACfuel, data.SCAC_Fuel);
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
/***************************************************************************************************/			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.fuelcurrentTab!='scac') return
    
      var page = $scope.SCACfuelPagination ? $scope.SCACfuelPagination.current_page + 1 : 1;
		$scope.SCACfuelPagination.current_page=page;
     
      $scope.loadSCACFuel({page:page,limit:$scope.fuelRecordLimit});
    });
    
    
    
    
    $scope.setcurrentfuelTab=function(curtab){
		
		//$scope.fuelfilter.searchFuel='';
		//$scope.fuelfilter.EffectiveFrom='';
		//$scope.fuelfilter.EffectiveTo='';
		$scope.fuelcurrentTab=curtab;
		$scope.resetRow();
		
	}
	
	
}
