function CarrierDashabordSetupController(CarrierApiService, Carrier, $scope, $timeout, $q){
	$scope.recordLimit = 5;
	$scope.timeoutMS = 3000;
	
	//customer overview module
	$scope.accsRates = {totalDefaultCount: -1, totalCustomCount: -1, customRates: [], defaultRates: [],};
	$scope.fuelRates = {totalDefaultCount: -1, totalCustomCount: -1, customRates: [], defaultRates: [],};
	$scope.equipmentData = {count: -1, list: [], };
	$scope.carriers = {modes: [], };
	$scope.remits = {count: -1, list: [], };
	$scope.insurances = {list: [], };

	//we will store all of our form data in this object
	$scope.customerCarrierData = {count: -1, list: []};
	
	var defaultParams = {limit: $scope.recordLimit,  orderType: 'asc', /* groupBy: 'carrierId' */}; 

	$scope.selectedCarrierId = 0;
	
	/**
	* watcher to fetch associated items whenever the selcted carrier 
	* changes by system or via the user interaction
	*/
	$scope.$watch('selectedCarrierId', function(newValue, oldValue){
		newValue = parseInt(newValue);
		oldValue = parseInt(oldValue);
		
		if(!isNaN(newValue) && !!newValue && (!isNaN(oldValue) || newValue != oldValue)){
			fetchAccsRates(newValue, angular.extend({groupBy: 'accsID'}, defaultParams), 0);
			fetchFuelTariffs(newValue, angular.extend({groupBy: 'fuelType'}, defaultParams), 0);
			fetchEquipments(newValue, angular.extend({groupBy: 'equip_no'}, defaultParams), 0);
			fetchModes(newValue, angular.extend({groupBy: 'modeId'}, defaultParams), 0);
			fetchRemits(newValue, angular.extend({groupBy: 'remit_name'}, defaultParams), 0);
			fetchInsurances(newValue, angular.extend({groupBy: 'insurance_policyname'}, defaultParams), 0);
		};
	});

	//fetching customer carriers
	fetchCarriers(defaultParams);

	$scope.changeCarrier = function(carrierId, event){
		angular.element(document.querySelectorAll('.entries a.selectedCust')).removeClass('selectedCust');
		angular.element(event.currentTarget).addClass('selectedCust');
		
		//fetching users
		$scope.selectedCarrierId = carrierId;
		
	}

	$scope.reloadPanel = function(section){
			switch(section){
				case 'crr':
					fetchCarriers(defaultParams, 0);
					break;
				case 'fuel':
					!!parseInt($scope.selectedCarrierId) ? 
						fetchFuelTariffs($scope.selectedCarrierId, angular.extend({groupBy: 'fuelType'}, defaultParams), 0) 
							: void(0);
					break;
				case 'accs':
					!!parseInt($scope.selectedCarrierId) ? 
						fetchAccsRates($scope.selectedCarrierId, angular.extend({groupBy: 'accsID'}, defaultParams), 0) 
							: void(0);
					break;		
				case 'eqp':
					!!parseInt($scope.selectedCarrierId) ? 
						fetchEquipments($scope.selectedCarrierId, angular.extend({groupBy: 'equip_no'}, defaultParams), 0) 
							: void(0);
					break;	
				case 'rmt':
					!!parseInt($scope.selectedCarrierId) ? fetchRemits($scope.selectedCarrierId, angular.extend({groupBy: 'remit_name'}, defaultParams), 0) 
							: void(0);
					break;
				case 'mode':
					!!parseInt($scope.selectedCarrierId) ? 
						fetchModes($scope.selectedCarrierId, angular.extend({groupBy: 'modeId'}, defaultParams), 0) 
							: void(0);
					break;
				case 'ins':
					!!parseInt($scope.selectedCarrierId) ? fetchInsurances($scope.selectedCarrierId, angular.extend({groupBy: 'insurance_policyname'}, defaultParams), 0) 
						: void(0);
					break;
			}
	}
	
	function fetchCarriers(params, timeOutAfter){
    	$timeout(function(){
    		var limit = null, clientId = null;
    		//making the loader visible for user
			angular.element(document.querySelector('#loading-customer-carriers-section')).removeClass('hidden');

			if(params.limit)
				limit = params.limit;
			if(params.clientId)
				clientId = params.clientId;


			CarrierApiService.fecthSelectedCarriers(false, null, false, limit, clientId) //calling factory to make ajax request
				.success(function(result){
					 if(result.success && result.carriers && result.carrierlist && result.count !== undefined){
						//$scope.selectedCarrierList = result.carriers;
						$scope.customerCarrierData.list = result.carriers;
						$scope.customerCarrierData.count = result.count;

						//code to automatically select the first carrier after loading it from the server
						if($scope.customerCarrierData.list.length && $scope.customerCarrierData.list[0].id !== undefined){
							$scope.selectedCarrierId = $scope.customerCarrierData.list[0].id;
						}
					}
				}).finally(function (data) {
		            //making the loader invisible for user
					angular.element(document.querySelector('#loading-customer-carriers-section')).addClass('hidden');
		        });
		 }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
	}
	
	function fetchAccsRates(carrierId, params, timeOutAfter){
		$timeout(function(){
			//making the loader visible for user
			angular.element(document.querySelector('#loading-accessorial-section')).removeClass('hidden');
			
			var deffered = $q.defer();
			var promise = deffered.promise;
			promise
				.then(function(result){
					//console.log(result);
					if(result.data.success){
						if(result.data.rates.tariffsDefault)
							$scope.accsRates.defaultRates = result.data.rates.tariffsDefault
						else
							$scope.accsRates.defaultRates = [];
						
						if(result.data.rates.tariffsCustom)
							$scope.accsRates.customRates = result.data.rates.tariffsCustom;
						else
							$scope.accsRates.customRates = [];
					}
				}).finally(function(data){
					//making the loader invisible for user
					angular.element(document.querySelector('#loading-accessorial-section')).addClass('hidden');
				});
				
				deffered.resolve(CarrierApiService.getCarrierAccTariffDetails(carrierId, params));
		}, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);	
	}

	function fetchFuelTariffs(carrierId, filter, timeOutAfter){
		$timeout(function(){
			//making the loader visible for user
			angular.element(document.querySelector('#loading-fuel-section')).removeClass('hidden');
			
			var deferredFuel = $q.defer();
			var promiseFuel = deferredFuel.promise;

			promiseFuel.then(function(result){
				//console.log(result);
				if(result.data.success){
					if(result.data.rates.tariffsDefault && result.data.rates.totalDefaultCount){
						$scope.fuelRates.defaultRates = result.data.rates.tariffsDefault;
						$scope.fuelRates.totalDefaultCount = result.data.rates.totalDefaultCount;
					}else{
						$scope.fuelRates.defaultRates = [];
						$scope.fuelRates.totalDefaultCount = 0;
					}

					if(result.data.rates.tariffsCustom && result.data.rates.totalCustomRateCount){
						$scope.fuelRates.customRates = result.data.rates.tariffsCustom;
						$scope.fuelRates.totalCustomCount = result.data.rates.totalCustomRateCount;
					}else{
						$scope.fuelRates.customRates = [];
						$scope.fuelRates.totalCustomCount = 0;
					}
				}
			}, function(reason){
				console.log(reason);
			}, function(progress){
				console.log(progress);
			}).finally(function(data){
					//making the loader invisible for user
					angular.element(document.querySelector('#loading-fuel-section')).addClass('hidden');
				});;

			deferredFuel.resolve(CarrierApiService.getCarrierFuelTariffDetails(carrierId, filter));
		}, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
	}

    function fetchEquipments(carrierId, params, timeOutAfter) {
    	$timeout(function(){
	        //making the loader visible for user
			angular.element(document.querySelector('#loading-equipment-section')).removeClass('hidden');
	        Carrier.makeAsyncRequest(Carrier.listEquipment(carrierId, params))
	                .then(function (response) {
	                    if (response.data.success && $scope.equipmentData.count !== undefined && response.data.results != undefined) {
	                        $scope.equipmentData.count = response.data.count;
	                        $scope.equipmentData.list = response.data.results ? response.data.results : [];
	                    }
	                    //console.log(result);
	                }, function (reason) {
	                    console.warn(reason);
	                }).finally(function (data) {
	            	//making the loader invisible for user
					angular.element(document.querySelector('#loading-equipment-section')).addClass('hidden');
	        });
	    }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
    }

    function fetchModes(carrierId, params, timeOutAfter) {
    	$timeout(function(){
	        //making the loader visible for user
			angular.element(document.querySelector('#loading-modes-section')).removeClass('hidden');
	        Carrier.makeAsyncRequest(Carrier.getCarrierModes(carrierId, params))
	                .then(function (response) {
	                	if (response.data.success  && response.data.carrierModes != undefined) {
                            $scope.carriers.modes = response.data.carrierModes;
                        }
	                }, function (reason) {
	                    console.warn(reason);
	                }).finally(function (data) {
	            	//making the loader invisible for user
					angular.element(document.querySelector('#loading-modes-section')).addClass('hidden');
	        });
	    }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
    }

    function fetchRemits(carrierId, params, timeOutAfter) {
    	$timeout(function(){
	        //making the loader visible for user
			angular.element(document.querySelector('#loading-remits-section')).removeClass('hidden');
	        Carrier.makeAsyncRequest(Carrier.listRemit(carrierId, params))
	                .then(function (response) {
	                	if (response.data.success && response.data.count !== undefined && response.data.results !== undefined) {
                        	$scope.remits.count = response.data.count;
                        	$scope.remits.list = response.data.results ? response.data.results : [];
                    	}
	                }, function (reason) {
	                    console.warn(reason);
	                }).finally(function (data) {
	            	//making the loader invisible for user
					angular.element(document.querySelector('#loading-remits-section')).addClass('hidden');
	        });
	    }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
    } 
    
    function fetchInsurances(carrierId, params, timeOutAfter) {
    	$timeout(function(){
	        //making the loader visible for user
			angular.element(document.querySelector('#loading-insurance-section')).removeClass('hidden');
	        Carrier.makeAsyncRequest(Carrier.getCarrierInsurances(carrierId, params))
	                .then(function (response) {
	                	if (response.data.success && response.data.carrierInsurances !== undefined) {
                        	$scope.insurances.list = response.data.carrierInsurances;
                    	}
	                }, function (reason) {
	                    console.warn(reason);
	                }).finally(function (data) {
	            	//making the loader invisible for user
					angular.element(document.querySelector('#loading-insurance-section')).addClass('hidden');
	        });
	    }, timeOutAfter !== undefined ? timeOutAfter : $scope.timeoutMS);
    }
}