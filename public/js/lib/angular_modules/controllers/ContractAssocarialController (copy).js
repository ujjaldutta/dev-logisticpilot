'use strict';


function ContractAssocarialController($scope, $http, $timeout, dataService,LanguageList){	
		/* $scope.myData = [{name: "Moroni", age: 50},
                     {name: "Tiancum", age: 43},
                     {name: "Jacob", age: 27},
                     {name: "Nephi", age: 29},
                     {name: "Enos", age: 34}];
                     
          */
     $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [2, 500, 1000],
        pageSize: 2,
        currentPage: 1
    };	
     $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    }; 
      $scope.myData=[];               
	  
    	
		
   
    $scope.setPagingData = function(data, page, pageSize){	
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
      
       
        
        setTimeout(function () {
            
            var data;
          
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('/front/profiles/contract-assocarial?profileid='+dataService.dataObj.profile_id+'&callback=?').success(function (largeLoad) {		
                    data = largeLoad.filter(function(item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });
                    $scope.setPagingData(data,page,pageSize);
                });
                
            } else {
				/*
                
                $http.get('http://angular-ui.github.io/ui-grid/jsonFiles/largeLoad.json?callback=?').success(function (largeLoad) {
                 $scope.setPagingData(largeLoad,page,pageSize);
                    
                   
               });
               */
               
               var httpRequest = $http({
						method: 'POST',
						data:{profileid:dataService.dataObj.profile_id,limit:pageSize,page:page},
						url: '/front/profiles/contract-assocarial'
						
				
					}).success(function(largeLoad, status) {
						$scope.setPagingData(largeLoad,page,pageSize);
						});
                
			}
					
					
            
        }, 100);
        
        
        
        
    };
	
    
	
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    
    $scope.addrow = function(pos){
		var emptyData = {
                cid: "",
                
                _newRow: true
            };

            $scope.myData.unshift(emptyData);
            
		
		/*
		var emptyContact = $scope.myData[$scope.myData.length-1];
		var e = $scope.$on('ngGridEventData', function() {
			$scope.gridOptions.selectItem(0, true);
			window.scrollTo(0,0);
			e();
		});

		$scope.myData.unshift(emptyContact);*/
	};
$scope.deleteThisRow = function(data){
		

$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-assoc',
				    data    : {Accessorial:data.entity,cid:dataService.dataObj.profile_id}, 
				  
				   })
				    .success(function(data) {
						$scope.myData.splice(data.entity, 1);
				      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
				    });
				    
		
	};
$scope.saveThisRow	= function(data){
		
	   $scope.saveData(data);
	
 
		
	};
	
	
	$scope.saveData=function(data){
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-assoc',
				    data    : {Accessorial:data.entity,cid:dataService.dataObj.profile_id,index:data.rowIndex}, //forms user object
				  
				   })
				    .success(function(data) {
				      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
				    });
	};

    $scope.$on('ngGridEventEndCellEdit', function(evt){

   // Detect changes and send entity to REST server

   //$scope.saveData(evt.targetScope.row);

	});
	
 

        $scope.effectivefrom = function (data) {
			if(data.entity.cid=='') return;
			var date=data.entity.effective_from;
			var yyyy = date.getFullYear();
		   var mm = date.getMonth()+1; // getMonth() is zero-based
		   var dd  = date.getDate();
		  var date=dd+'/'+mm+'/'+yyyy;
			data.entity.effective_from=date;
		
             $scope.saveData(data);
        };
        
         $scope.effectiveto = function (data) {
		if(data.entity.cid=='') return;
			var date=data.entity.effective_to;
			var yyyy = date.getFullYear();
		   var mm = date.getMonth()+1; 
		   var dd  = date.getDate();
		  var date=dd+'/'+mm+'/'+yyyy;
			data.entity.effective_to=date;
		 
            $scope.saveData(data);
        };
        
            
	$scope.languages = LanguageList;
	$scope.template='<select ng-cell-input ng-options=\'l.language_code as l.language_title for l in languages\' ng-class="\'colt\' + $index" ng-model="COL_FIELD" ng-input="COL_FIELD" data-placeholder="-- Select One --" ></select>';
	$scope.dateCellTemplateEdit = '<input  ng-blur="effectivefrom(row)"  ng-class="\'colt\' + col.index" uib-datepicker-popup="MM-dd-yyyy" is-open="true" datepicker-append-to-body=true ng-model="COL_FIELD" my-input="COL_FIELD"/>';
	$scope.dateCellTemplateEdit2 = '<input  ng-blur="effectiveto(row)"  ng-class="\'colt\' + col.index" uib-datepicker-popup="MM-dd-yyyy" is-open="true" datepicker-append-to-body=true ng-model="COL_FIELD" my-input="COL_FIELD"/>';
	

            
  $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
		showFooter: true,
		enableCellSelection: true,
        enableRowSelection: false,
        jqueryUITheme:true,
        //showSelectionCheckbox:true,
         rowTemplate: '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" style="overflow: visible"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
        enableCellEdit: true,
        totalServerItems: 'totalServerItems',
		headerRowHeight:40,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
         columnDefs: [{field: 'accessorial', displayName: 'Accessorial', enableCellEdit: true}, 
                     {field:'rate_calculation', displayName:'Rate Calculation', enableFocusedCellEdit: true,
                    editableCellTemplate: $scope.template,
                    cellFilter : 'LanguageMap'},
                     {field:'min_rate', displayName:'Minimum Rate', enableCellEdit: true},
                     {field:'max_rate', displayName:'Maximum Rate', enableCellEdit: true},
                     {field:'rate_percentage', displayName:'Rate/Percentage', enableCellEdit: true},
                     {field:'effective_from', displayName:'Effective From',
					 readonly: true,
					 enableCellEditOnFocus: true,
					 editableCellTemplate: $scope.dateCellTemplateEdit
					 
					 },
                     {field:'effective_to', displayName:'Effective To', enableCellEdit: true, readonly: true,
					 editableCellTemplate: $scope.dateCellTemplateEdit2},
					  {field:'cid',visible: false},
					  {field:'assoc_code',visible: false},
             {displayName: 'Actions', cellTemplate: 
             '<div class="grid-action-cell">'+
             ' <a ng-click="$event.stopPropagation(); deleteThisRow(row);" href="javascript:void(0)"><i class="fa fa-trash-o"></i></a>'+
             ' <a ng-click="$event.stopPropagation(); saveThisRow(row);" href="javascript:void(0)"><i class="fa fa-save"></i></a></div>'
             
             , enableCellEdit: false},
             
             
                     ]
   
    };
    
     setTimeout(function () {
			$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		},4000);
	
	 		  };
