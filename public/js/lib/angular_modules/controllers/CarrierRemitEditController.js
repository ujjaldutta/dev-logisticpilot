'use strict';

function CarrierRemitEditController(CarrierAPIEndpoint, Carrier, Upload, $scope, $location, $window) {
    //model attribute to show/hide error message notification
    $scope.error = {
        status: false,
        message: '',
    };

    $scope.success = {
        status: false,
        message: '',
    };

    //collection  of carrier type objects
    //$scope.carrierId = '';
    $scope.remitData = {};
    
    getCurrencyTypes();
    
     // Settings combox sidebar
    $scope.filters = [];
    $scope.filter = {};
    $scope.filter.insId = {};
    $scope.filter.insurancetTypes = [];
    $scope.insSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.modId = {};
    $scope.filter.equipmentTypes = [];
    $scope.modSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.filter.equipId = {};
    $scope.filter.modeTypes = [];
    $scope.equipmentSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };
    
    $scope.remitData.remit_currency = {};
    $scope.currencyTypes = [];
    $scope.currencySettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };                       

    
    /* method to dismiss the alert */
    $scope.hideMessage = function () {
        $scope.error = {
            status: false,
            message: '',
        };

        $scope.success = {
            status: false,
            message: '',
        };
    }

    $scope.cancelRemitEditing = function (carrierId) {        
        $window.location.href = CarrierAPIEndpoint.base + 'edit/' + carrierId + '#horizontalTab15';        
    }

    /*
     * when valid carr id is loaded or assigned 
     * after dom rendered make a aysnc req to the server 
     * to get the information
     */
    $scope.$watch('remitID', function (val) {
        if (!isNaN(val) && val > 0) {
            //fetching the selected carrier data
            getRemitData(val);
        }

    });

    function getRemitData(remitID) {
        if (!remitID)
            remitID = $location.search().remitID || 0;

        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');

        Carrier.makeAsyncRequest(Carrier.getRemitData(remitID))
                .then(function (response) {
                    if (response.data.success && response.data.remit) {
                        $scope.remitData = response.data.remit;
                        $scope.carrierId = response.data.carrierId;
                        $scope.remitData.location = __fillLocationDetails($scope.remitData, 'remit');
                        $scope.remitData.remit_currency = {id: $scope.remitData.remit_currency};
                        
                        return;
                    }
                    //console.log(response);}, function(reason){
                    console.warn(reason);
                })                     
                .finally(function (data) {
                    //making the loader invisible for user
                    angular.element('.loading').addClass('hidden');
                });
    }

    function getCurrencyTypes() {
        Carrier.makeAsyncRequest(Carrier.getAllCurrTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.currencyTypes !== undefined) {
                        $scope.currencyTypes = response.data.currencyTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    /*
     * Saving the carrier fields
     */
    $scope.saveRemitData = function (form) {
        if (form && form.$valid ) {
            
            Carrier.postRemitData($scope.remitID, $scope.carrierId, $scope.remitData)
                    .then(function (response) {
                        
                        if (response.data.success && response.data.remitId && response.data.carrierId) {
                            
                            $scope.remitID = response.data.remitId;
                            $scope.carrierId = response.data.carrierId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';
                        } else if (response.data.error !== undefined) {
                            
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = Carrier.formatValidationErrors(response.data.error);
                            } else
                                $scope.error.message = response.data.error;
                        }
                        //console.log(response);
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }   
    
    function __fillLocationDetails(collection, prf) {

        var location = '';
        var city = prf + '_city';
        var state = prf + '_state';
        var postal = prf + '_postal';
        var country = prf + '_country';

        if (collection[city] !== undefined && collection[city])
            location += collection[city] + ', ';
        if (collection[state] !== undefined && collection[state])
            location += collection[state] + ', ';
        if (collection[postal] !== undefined && collection[postal])
            location += collection[postal] + ', ';
        if (collection[country] !== undefined && collection[country])
            location += collection[country];

        return location;
    }
 
}


