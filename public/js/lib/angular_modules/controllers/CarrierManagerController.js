'use strict';

function CarrierManagerController(Carrier, $scope, $window){
	$scope.carrierData = {count: 0, list: []};
	$scope.TotalCarrier = [];
        
	$scope.recordLimit = 10;
	$scope.currentPage = 1;
	$scope.filters = [];
	$scope.filter = {};
        
        $scope.filter.carrApi = 'all'; // default
        $scope.filter.carrEquipTC = 'allTC'; // default
        
        getEquipmentTypes();
        getInsuranceTypes();
        getModeTypes();        

         // Settings combox
        $scope.filter.insId = {};
        $scope.filter.insurancetTypes = [];
        $scope.insSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
                           
        $scope.filter.modId = {};
        $scope.filter.equipmentTypes = [];
        $scope.modSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };
                           
        $scope.filter.equipId = {};
        $scope.filter.modeTypes = [];
        $scope.equipmentSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };

	$scope.predicate = 'scac';
        $scope.reverse = false;
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	};
        
	$scope.pageChanged = function(){
            fetchCarriers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
	}

	//intially fetching records of the current page i.e. page 1 with current conditions
	fetchCarriers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage});

	//filtering carrier result if any by watching sorting option changes
	$scope.$watch('filter.carrName', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: value},
                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},
                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                          {api: $scope.filter.carrApi},
                                          {tempControl: $scope.filter.carrEquipTC},
                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                          {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                        fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
	});
        
        $scope.$watch('filter.carrLoc', function(value){
		//console.log(Carrier.processGoogleLocation(value));
                if(value !== undefined){
			$scope.filters = [{name: $scope.filter.carrName},
                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},
                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                          {tempControl: $scope.filter.carrEquipTC},
                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                          {api: $scope.filter.carrApi}, 
                                          {location: Carrier.processGoogleLocation(value)}];
                        fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}		
	});
        
       $scope.searchInsuranceEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){
                    $scope.filters = [{name: $scope.filter.carrName},
                        {insurance: Carrier.processedComboBox(item)},
                        {mode: Carrier.processedComboBox($scope.filter.modId)},
                        {tempControl: $scope.filter.carrEquipTC},
                        {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                        {api: $scope.filter.carrApi}, 
                        {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                    fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
            }                     
        };
        
        $scope.searchModeEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){
                    $scope.filters = [{name: $scope.filter.carrName},
                        {insurance: Carrier.processedComboBox($scope.filter.insId)},
                        {mode:Carrier.processedComboBox(item)},
                        {tempControl: $scope.filter.carrEquipTC},
                        {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                        {api: $scope.filter.carrApi}, 
                        {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                    fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
            }                     
        };
                
        $scope.searchEquipmentEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){
                    $scope.filters = [{name: $scope.filter.carrName},
                        {insurance: Carrier.processedComboBox($scope.filter.insId)},
                        {mode: Carrier.processedComboBox($scope.filter.modId)},
                        {tempControl: $scope.filter.carrEquipTC},
                        {equipTypeId: Carrier.processedComboBox(item)},
                        {api: $scope.filter.carrApi}, 
                        {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                    fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
            }                     
        };
        
        $scope.$watch('filter.carrEquipTC', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.carrName},
                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},
                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                          {tempControl: value}, 
                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                          {api: $scope.filter.carrApi}, 
                                          {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                        fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
	});
        
        $scope.$watch('filter.carrApi', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.carrName},
                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},
                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                          {tempControl: $scope.filter.carrEquipTC},
                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                          {api: value}, 
                                          {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
                        fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
		}
	});
	
	$scope.resetFilter = function(type){
		switch(type){
                        case 'carrLoc':
				if($scope.filter.carrLoc !== undefined){
					$scope.filter.carrLoc = null;
					$scope.filters = [{name: $scope.filter.carrName},
                                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},
                                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                                          {tempControl: $scope.filter.carrEquipTC},
                                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                                          {api: $scope.filter.carrApi}, 
                                                          {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
					fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
				}
				break;
			case 'carrName':
				if($scope.filter.carrName !== undefined){
					$scope.filter.carrName = null;
					$scope.filters = [{name: $scope.filter.carrName},
                                                          {insurance: Carrier.processedComboBox($scope.filter.insId)},                                                          
                                                          {mode: Carrier.processedComboBox($scope.filter.modId)},
                                                          {tempControl: $scope.filter.carrEquipTC},
                                                          {equipTypeId: Carrier.processedComboBox($scope.filter.equipId)},
                                                          {api: $scope.filter.carrApi}, 
                                                          {location: Carrier.processGoogleLocation($scope.filter.carrLoc)}];
					fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
				}
				break;
		}
	}

	$scope.checkAll = function(){
		angular.forEach($scope.carrierData.list, function(value, key){
			if($scope.carrierData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){
		if($scope.carrierData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.carrierData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.carrierData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.carrierData.selectAll = true;

		}else
			$scope.carrierData.selectAll = false;
	}

	$scope.deleteAllCarrier = function(){
		//scanning all items for checking
		if($window.confirm('Are you sure ?')){
			angular.forEach($scope.carrierData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteCarrier(value.id, false);
				}
			});
		}	
	}

	$scope.deleteCarrier = function(carrId, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Carrier.makeAsyncRequest(Carrier.deleteCarrier(carrId));
		}else if(showAlert === false)
			promiseObj = Carrier.makeAsyncRequest(Carrier.deleteCarrier(carrId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
                                        fetchCarriers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}
        
        function getEquipmentTypes() {
            Carrier.makeAsyncRequest(Carrier.getAllEquipTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.equipmentTypes !== undefined) {
                        $scope.filter.equipmentTypes = response.data.equipmentTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
        }
        
        function getInsuranceTypes() {
            Carrier.makeAsyncRequest(Carrier.getAllInsTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.insuranceTypes !== undefined) {
                        $scope.filter.insuranceTypes = response.data.insuranceTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
        }
        
        function getModeTypes() {
            Carrier.makeAsyncRequest(Carrier.getAllModTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.modeTypes !== undefined) {
                        $scope.filter.modeTypes = response.data.modeTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
        }
                
        function fetchCarriers(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		Carrier.makeAsyncRequest(Carrier.listCarrier(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.carrierData.count = response.data.count;
					$scope.carrierData.list = response.data.results ? response.data.results : [];
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

}

