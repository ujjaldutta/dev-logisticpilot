'use strict';

function ProfileSearchController($scope, $http, $window,$interval){
	
	$scope.search={};
	$scope.searchresult=[];
	$scope.filters = [];
	$scope.profilelist={};
	$scope.autocompleteOption = {
        componentRestrictions: { country: 'us' },
        types: ["(regions)"]
	};
	$scope.SearchId='';
	$scope.counter=0;
	$scope.resultmsg='No Result Found!';
	$scope.multisearch= [{id: 'choice1'}];
	
	
	
	
	
	
	
	
	
$scope.stopInterval=function(){
	$interval.cancel(promise);
	}	
	
	
$scope.resultList=function(){
	angular.element('.loading').removeClass('hidden');
	//console.log($scope.SearchId)
	$scope.counter++;
	
	if($scope.counter>5){
		$scope.counter=0;
		$interval.cancel(promise);
		$scope.search._token=$scope.newtoken;
		$scope.resultmsg='Please Try Again';
		angular.element('.loading').addClass('hidden');
	}
	  $http({
				    method  : 'POST',
				    url     : '/front/profilesearch/list',
				    data    : {searchId:$scope.SearchId}, //forms user object
		
				   })
				    .success(function(data) {
						if(data.success==false ){
						$scope.resultmsg=data.msg;	
							
						}
						else if(data.count<=0){
							$scope.resultmsg=data.msg;
							
							}
						else{
						
						$scope.searchresult=data.result;
						//console.log($scope.searchresult)
						angular.element('.loading').addClass('hidden');
						$scope.resultmsg='';
						$interval.cancel(promise);
						}
						
					  });	
	
}	
	
var promise;	
	
	$scope.SearchProfile=function(){
		$scope.searchresult=[];
		$scope.SearchId=$scope.search._token;
		angular.element('.loading').removeClass('hidden');
				
				  $http({
				    method  : 'POST',
				    url     : '/front/profilesearch/search',
				    data    : $scope.search, //forms user object
		
				   })
				    .success(function(data) {
				      
					
						
						$scope.newtoken=data.token;
						$scope.search._token=$scope.newtoken;
						jQuery("#searchtoken").val($scope.newtoken);
						
						
					promise=$interval( function(){ $scope.resultList(); }, 3000);
				    //angular.element('.loading').addClass('hidden');
				    });
	}
	
$scope.addNewSearch=function(){
	var newItemNo = $scope.multisearch.length+1;
		$scope.multisearch.push({'id':'choice'+newItemNo});
	};
 $scope.removeNewSearch = function() {
		var lastItem = $scope.multisearch.length-1;
		$scope.multisearch.splice(lastItem);
		
	};	
	
	
$scope.onlyNumbers = function(event){   
    var keys={
        'up': 38,'right':39,'down':40,'left':37,
        'escape':27,'backspace':8,'tab':9,'enter':13,'del':46,
        '0':48,'1':49,'2':50,'3':51,'4':52,'5':53,'6':54,'7':55,'8':56,'9':57
    };
    for(var index in keys) {
        if (!keys.hasOwnProperty(index)) continue;
        if (event.charCode==keys[index]||event.keyCode==keys[index]) {
            return; //default event
        }
    }   
    event.preventDefault();
};


	
	
	
	$scope.loadProfiles=function(params){
		angular.element('.loading').removeClass('hidden');
		
		
		
		var httpRequest = $http({
			    method: 'POST',
			    url: '/front/profiles/profile-list',
			    data:{filters:params.filters,fields:'_id,RateProfileCode,RateProfileDescription,CustomerReferenceCode'},
		
			}).success(function(data, status) {
			    $scope.profilelist = data.profilelist;
				$scope.SearchId=$scope.search._token;
				 angular.element('.loading').addClass('hidden');
				
			});
		
		    
	}
	
	$scope.loadProfiles({filters: $scope.filters});
	
}



