'use strict';

function CustomerEditController(CustomerAPIEndpoint, Customer, Upload, $scope, $location, $window){
	$scope.buttonMailText = 'Mail Password';
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOption = {
        componentRestrictions: {  },
        types: ["(regions)"]
    }

    //collection  of customer type objects
	$scope.clientData = {};
	$scope.clientDefaultSettings = {};
	$scope.themes = [];
	$scope.languages = [];

	//static collection of product types and products
	/*$scope.productClasses = [ 
		{id: 50, label: 50}, 
		{id: 55, label: 55},
		{id: 60, label: 60},
		{id: 65, label: 65},
		{id: 70, label: 70},
		{id: 77.5, label: 77.5},
		{id: 85, label: 85},
		{id: 92.5, label: 92.5},
		{id: 100, label: 100},
		{id: 110, label: 110},
		{id: 125, label: 125},
		{id: 150, label: 150},
		{id: 175, label: 175},
		{id: 200, label: 200},
		{id: 250, label: 250},
		{id: 300, label: 300},
		{id: 400, label: 400},
		{id: 500, label: 500},
	];*/

	/*$scope.products = [
		{id: 'p1', label: 'Product 1'}, 
		{id: 'p2', label: 'Product 2'}, 
		{id: 'p3', label: 'Product 3'}, 
	];*/
	
	/*$scope.shipmentTerms = [
		{id: 1, label: 'Prepaid'}, 
		{id: 2, label: 'Postpaid'}, 
	];*/
	
	/*$scope.bolTemplates = [
		{id: 1, label: 'Template 1'}, 
		{id: 2, label: 'Template 2'}, 
	];*/

	/*$scope.invoiceTerms = [
		{id: 1, label: 'Term 1'}, 
		{id: 2, label: 'Term 2'}, 
	];*/
	
	/*$scope.invoiceDays = [
		{id: 1, label: 'Sunday'}, 
		{id: 2, label: 'Monday'}, 
		{id: 3, label: 'Tuesday'}, 
		{id: 4, label: 'Wednesday'}, 
		{id: 5, label: 'Thursday'}, 
		{id: 6, label: 'Friday'}, 
		{id: 7, label: 'Saturday'}, 
	];*/

	/*$scope.invoiceTemplates = [
		{id: 1, label: 'Template 1'}, 
		{id: 2, label: 'Template 2'},  
	];*/
        
        //view tables
        $scope.products = [];
        $scope.productClasses = [];
        $scope.shipmentTerms = [];
        $scope.bolTemplates = [];
        $scope.invoiceTerms = [];
        $scope.invoiceDays = [];
        $scope.invoiceTemplates = [];

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = CustomerAPIEndpoint.base ? CustomerAPIEndpoint.base : '/';
	}


	//fetching languages
	getLanguages();
	//fetching themes
	getThemes();
        getCommodityType();
        getClassesType();
        getShipmentTermsType();
        getBolTemplatesType();
        getInvoiceTermsType();
        getInvoiceDaysType();
        getInvoiceTemplatesType();
	
	/*
	* when valid cust id is loaded or assigned 
	* after dom rendered make a aysnc req to the server 
	* to get the information
	*/
	$scope.$watch('custID', function(val){
		if(!isNaN(val) && val > 0){
			//fetching the selected customer data
			getCustomerData(val);
		}
			
	});

	$scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/registration-steps/upload',
                    fields: {
                        'clientID': $scope.custID
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.clientData.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                //evt.config.file.name + '\n' + $scope.formData.log;
                }).success(function (data, status, headers, config) {
                	if(data.error){
                		$scope.error.status = true;
						$scope.error.message = data.error;
                	}else if(data.success){
                		$scope.success.status = true;
						$scope.success.message = 'file ' + config.file.name + ' uploaded successfully!';
                	}
                  
                   //$scope.$apply();
                });
            }
        }
    };


	/*
	* Saving the profile fields
	*/
	
	$scope.saveProfileData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			Customer.postCustomerData($scope.custID, $scope.clientData)
				.then(function(response){
					if(response.data.success && response.data.custId){
						$scope.custID = response.data.custId;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
						
						if($scope.newUser === true){
							return Customer.createUser($scope.custID, $scope.clientData);
						}
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							$scope.error.message = Customer.formatValidationErrors(response.data.error);
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.then(function(response){
					if(response === undefined)
						return;

					if(response.data.success && response.data.userId){
						$scope.clientData.userId = response.data.userId || 0;
						$scope.clientData.client_password = response.data.client_password || '';
					}else{
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							$scope.error.message = "User creation error: " + Customer.formatValidationErrors(response.data.error);
						}else
							$scope.error.message = response.data.error;
					}
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}

	/*
	* Saving the profile fields
	*/
	
	$scope.saveSettingsData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			Customer.postCustomerDefaultSettings($scope.custID, $scope.clientDefaultSettings)
				.then(function(response){
					//console.log(response);
					if(response.data.success && response.data.id){
						$scope.clientDefaultSettings.id = response.data.id;
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}

	function getCustomerData(custID){
		if(!custID)
			custID = $location.search().custID || 0;
		
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		Customer.makeAsyncRequest(Customer.getCustomerData(custID))
			.then(function(response){
				if(response.data.success && response.data.profile){
					$scope.clientData = response.data.profile;
					
					//filling the location input with the already selected location details but saved separately
					$scope.clientData.location = __fillLocationDetails($scope.clientData, 'client');
					//console.log($scope.customerData);
					//again making async request to get other details like billing and shipping
					return Customer.makeAsyncRequest(Customer.getCustomerDefaultSettings(custID));
				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			//callback for billing
			.then(function(response){
				if(response && response.data.success && response.data.defaultSettings.default_settings){
					$scope.clientDefaultSettings = response.data.defaultSettings.default_settings;
					
					//now converting data type to either true or false for checkboxes
					$scope.clientDefaultSettings.receive_edi204 = !!parseInt($scope.clientDefaultSettings.receive_edi204);
					$scope.clientDefaultSettings.receive_edi210 = !!parseInt($scope.clientDefaultSettings.receive_edi210);
					$scope.clientDefaultSettings.receive_edi214 = !!parseInt($scope.clientDefaultSettings.receive_edi214);
					$scope.clientDefaultSettings.receive_edi216 = !!parseInt($scope.clientDefaultSettings.receive_edi216);
					$scope.clientDefaultSettings.send_edi204 = !!parseInt($scope.clientDefaultSettings.send_edi204);
					$scope.clientDefaultSettings.send_edi210 = !!parseInt($scope.clientDefaultSettings.send_edi210);
					$scope.clientDefaultSettings.send_edi214 = !!parseInt($scope.clientDefaultSettings.send_edi214);

					//processing locations devided into diff attributes to place it one place
					$scope.clientDefaultSettings.pickup_location = __fillLocationDetails($scope.clientDefaultSettings, 'pickup');
					$scope.clientDefaultSettings.delivery_location = __fillLocationDetails($scope.clientDefaultSettings, 'delivery');
					$scope.clientDefaultSettings.billto_location = __fillLocationDetails($scope.clientDefaultSettings, 'billto');

				} 
			}, function (reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	/* function __fillLocationDetails(collection){
		var location = '';
		
		if(collection.client_city)
			location += collection.client_city + ', ';
		if(collection.client_state)
			location += collection.client_state + ', ';
		if(collection.client_postal)
			location += collection.client_postal + ', ';
		if(collection.client_country)
			location += collection.client_country;

		return location;
	} */

	function __fillLocationDetails(collection, prf){

		var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		
		if(collection[city] !== undefined && collection[city])
			location += collection[city] + ', ';
		if(collection[state] !== undefined && collection[state])
			location += collection[state] + ', ';
		if(collection[postal] !== undefined && collection[postal])
			location += collection[postal] + ', ';
		if(collection[country] !== undefined && collection[country])
			location += collection[country];

		return location;
	}
	
	
	$scope.sendEmail = function(){
		$scope.buttonMailText = 'Please wait...';
		var email = $scope.clientData.client_email;
		var pass = $scope.clientData.client_password;		
		
		Customer.makeAsyncRequest(Customer.sendPassword(email, pass))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			})
			
		
	}
	
	$scope.updatePassword = function(){
		if(!$window.confirm('Are you sure to reset the passsword?'))
			return;

		$scope.buttonMailText = 'Please wait...';
		var email = $scope.clientData.client_email;	
		var custId = $scope.custID;

		Customer.makeAsyncRequest(Customer.sendUpdatePassword(email,custId))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function getThemes(){
		Customer.makeAsyncRequest(Customer.getAllThemes())
			.then(function(response){
				if(response.data.success !== undefined && response.data.themes !== undefined){
					$scope.themes = response.data.themes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function getLanguages(){
		Customer.makeAsyncRequest(Customer.getAllLanguages())
			.then(function(response){
				if(response.data.success !== undefined && response.data.languages !== undefined){
					$scope.languages = response.data.languages;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getCommodityType(){
		Customer.makeAsyncRequest(Customer.getAllCommodities())
			.then(function(response){
				if(response.data.success !== undefined && response.data.commodity !== undefined){
					$scope.products = response.data.commodity;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getClassesType(){
		Customer.makeAsyncRequest(Customer.getAllClasses())
			.then(function(response){
				if(response.data.success !== undefined && response.data.classes !== undefined){
					$scope.productClasses = response.data.classes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}        
        
        function getShipmentTermsType(){
		Customer.makeAsyncRequest(Customer.getAllShipmentTerms())
			.then(function(response){
				if(response.data.success !== undefined && response.data.shipment !== undefined){
					$scope.shipmentTerms = response.data.shipment;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getBolTemplatesType(){
		Customer.makeAsyncRequest(Customer.getBolTemplates())
			.then(function(response){
				if(response.data.success !== undefined && response.data.bolTemplates !== undefined){
					$scope.bolTemplates = response.data.bolTemplates;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceTermsType(){
		Customer.makeAsyncRequest(Customer.getInvoiceTerms())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceTerms !== undefined){
					$scope.invoiceTerms = response.data.invoiceTerms;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceDaysType(){
		Customer.makeAsyncRequest(Customer.getAllInvoiceDays())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceDays !== undefined){
					$scope.invoiceDays = response.data.invoiceDays;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceTemplatesType(){
		Customer.makeAsyncRequest(Customer.getAllInvoiceTemplates())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceTemplates !== undefined){
					$scope.invoiceTemplates = response.data.invoiceTemplates;
				}
			}, function(reason){
				console.warn(reason);
			});
	}  
        
        
}


