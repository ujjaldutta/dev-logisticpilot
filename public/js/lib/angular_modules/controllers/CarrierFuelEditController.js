'use strict';

function CarrierFuelEditController(CarrierEquipmentAPIEndpoint, CarrierEquipment, $scope, $location, $window){
	
	$scope.saveBtnText = 'Save';

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

    //collection  of customer type objects
	$scope.fuelData = {};
	$scope.fuelTypes = [];
	$scope.recordId = null;
	$scope.fuelDropDownSettings = {enableSearch: true, selectionLimit: 1, idProp: 'id', displayProp: 'accsname'};

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = '/front/setup-connectivity/list-carrier-fuel/' + $scope.carrierId;
	}
	
	/*
	* when valid cust id is loaded or assigned 
	* after dom rendered make a aysnc req to the server 
	* to get the information
	*/
	
	$scope.$watch('carrierId', function(val){
		if(!isNaN(val) && val > 0){
			if(!$scope.newRecord && !isNaN($scope.recordId)){
				//fetching the selected customer data
				//getFuelData($scope.recordId);
			}
			
			getFuelTypes(val);

			//getAccessorialTypes(val);
		}
			
	});

	$scope.$watch('recordId', function(val){
		if(!isNaN(val) && val > 0){
			//fetching the selected customer data
			getFuelData(val);
		}
	});


	/*
	* Saving the profile fields
	*/
	
	$scope.saveFuelData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			CarrierEquipment.postFuelData($scope.carrierId, angular.extend({step: 'postCarrierFuelTariffCustomDetails'}, $scope.fuelData))
				.then(function(response){
					if(response.data.success && response.data.id){
						$scope.fuelData.id = response.data.id;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							//$scope.error.message = CarrierEquipment.formatValidationErrors(response.data.error);
							$scope.error.message = 'Unhandled Exception occurred';
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}
	

	function getFuelData(id){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.getFuelData(id))
			.then(function(response){
				if(response.data.success && response.data.fuelDetails){
					$scope.fuelData = response.data.fuelDetails;
					$scope.fuelData.rangefrom = parseFloat($scope.fuelData.rangefrom);
					$scope.fuelData.rangeto = parseFloat($scope.fuelData.rangeto);
					$scope.fuelData.LTLRate = parseFloat($scope.fuelData.LTLRate);
					$scope.fuelData.TLRate = parseFloat($scope.fuelData.TLRate);
				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	
	function getFuelTypes(carrierId){
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.getFuelTypes(carrierId))
			.then(function(response){
				if(response.data.success && response.data.fuelTypes){
					$scope.fuelTypes = response.data.fuelTypes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
}


