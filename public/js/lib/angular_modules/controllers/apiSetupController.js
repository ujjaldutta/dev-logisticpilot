function apiSetupController(carrierStatusAPIEndpoint, CarrierApiService, $scope, $http, $compile, $q, $location){
	//we will store all of our form data in this object
	$scope.apiSetupCollection = {saveButtonText: 'Next', carriers: [],};

	//dataset to hold individual carrier api setup data
	$scope.apisetup = {rate_apiactivated: false, shp_zip: '', csn_zip: ''};

	//dataset to hold individual carrier accs tariff including default and custom set by the particular user
	$scope.accsRates = {defaultRates: null, customRates: null, accsTypes:null, calculationTypes: null, defaultRate: null, totalDefaultCount: 0, totalCustomRateCount: 0,};
	
	//dataset to hold individual carrier accs tariff including default and custom set by the particular user
	$scope.fuelRates = {defaultRates: null, customRates: null, fuelTypes: null, defaultRate: null, totalDefaultCount: 0, totalCustomRateCount: 0, };

	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('fuelTariffTypeChanged', function(e, args){
		$scope.apisetup.fuel_ruletariff = args;
	});

	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('accsTariffTypeChanged', function(e, args){
		$scope.apisetup.acc_ruletariff = args;
	});

	//listener to the event when directive to emit event for changing accs tariff type attribute
	$scope.$on('filterCustomAccsTariff', function(e, args){
		//console.log(args);
		if(args.carrierId){
			fetchAccsTariffs(args.carrierId, args);
		}
	});
	
	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('filterCustomFuelTariff', function(e, args){
		//console.log(args);
		if(args.carrierId){
			fetchFuelTariffs(args.carrierId, args);
		}
	});
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOptions = {
        types: ['locality', 'postal_code']
    }

	//intializing data from the server to list all previously selected carriers
	fetchDatafromServer();

	//sorting function to list api enabled carriers
	$scope.sortByApiEnabledCarrier = function(carrier){
		if($scope.apiEnabledCarrierOnly){
			if(carrier.apiEnabled ==1)
				return carrier;
			else
				return;
		}else
			return carrier;
	}

	//selecting and fetching the carrier details from the db when selected from the search auto complete
	$scope.selectedSearchCarrier = function(selected){
		//if this carrier is not api enabled then break otherwise load config window
		if(selected === undefined || selected.originalObject.apiEnabled === undefined || selected.originalObject.apiEnabled == 0)
			return;

		//getting scac code
		var scac = selected.originalObject.scac;
		//getting the carrier ID for the selected carrier
		var carrierId = selected.originalObject.id;
		//console.log(angular.element('div[data-scac=' + scac + ']').attr('id'));
		var carrierIndex = angular.element('div[data-scac=' + scac + ']').attr('id');
		if(carrierIndex !== undefined){
			var index = carrierIndex.split('_');
			index = index[index.length -1];
			//console.log(index);

			if(scac !== undefined && index !== undefined && carrierId !== undefined){
				$scope.visibleCarrierApiSetup(carrierId, scac);
			}
		}
	}

	$scope.visibleCarrierApiSetup = function(carrierId, scac){
		//setting scac for the selected carrier
		$scope.apisetup.scac = scac;
		$scope.apisetup.carrierID = carrierId;
		//return;
		
		//fetching the data from server for the selected carrier api details set previously if any
		CarrierApiService.getCarrierApiDetails(carrierId)
			.success(function(result){
				//console.log(result.count);

				var carrierIndex = angular.element('div[data-scac=' + scac + ']').attr('id');
				
				if(carrierIndex !== undefined){
					carrierIndex = carrierIndex.split('_');
					carrierIndex = carrierIndex[carrierIndex.length -1];
				}
				
				//dom jq manipulation to make enable disable
				$('div[id ^= carrier_]').addClass('hidden');
				$('#carrier_' + carrierIndex).removeClass('hidden');

				//binding api setup form with the already saved result from the DB or blank if creating a new record
				//console.log($scope.apiStatusImage);
				if(result.count && result.count >=1 && result.carrier){
					//resetting all error or success message for the previous carrier
					$scope.error = {status: false, message: '',};
					$scope.success = {status: false, message: '',};

					$scope.apisetup = angular.extend({}, result.carrier, {scac: scac, step: 4});
					//console.log($scope.apisetup);
					//making api status icon depending on existing record
					if(result.carrier.rate_enablepickup !== undefined && result.carrier.rate_enablepickup == 1)
						$scope.apisetup.rate_enablepickup = true;
					else if(result.carrier.rate_enablepickup !== undefined && result.carrier.rate_enablepickup == 0)
						$scope.apisetup.rate_enablepickup = false;
					
					if(result.carrier.rate_enablerate !== undefined && result.carrier.rate_enablerate == 1)
						$scope.apisetup.rate_enablerate = true;
					else if(result.carrier.rate_enablerate !== undefined && result.carrier.rate_enablerate == 0)
						$scope.apisetup.rate_enablerate = false;
					
					if(result.carrier.rate_enabletracking !== undefined && result.carrier.rate_enabletracking == 1)
						$scope.apisetup.rate_enabletracking = true;
					else if(result.carrier.rate_enabletracking !== undefined && result.carrier.rate_enabletracking == 0)
						$scope.apisetup.rate_enabletracking = false;
					
					if(result.carrier.rate_apiactivated !== undefined && result.carrier.rate_apiactivated == 1)
						$scope.apiStatusImage = $scope.apiStatusImageSuccess;
					else if(result.carrier.rate_apiactivated !== undefined && result.carrier.rate_apiactivated == 0)
						$scope.apiStatusImage = $scope.apiStatusImageError;
					else
						$scope.apiStatusImage = $scope.apiStatusImageUnknown;
				}else if(result.count !== undefined && !result.count){
					$scope.apisetup = angular.extend({}, {rate_apiactivated: false, scac: scac, step: 4, carrierID: carrierId});
					$scope.apiStatusImage = $scope.apiStatusImageUnknown;
				}

				//$scope.$digest();
			});

		//calling private method for accessorial rates
		fetchAccsTariffs(carrierId);

		//calling private method for fuel rates
		fetchFuelTariffs(carrierId);
	};



	//method to connect to the carrier api services where the credential and connectivity can be checked
	$scope.checkCarrierStatus = function(){
		//console.log($scope.apisetup);
		//checking appropriate
		var credentials = $scope.apisetup;
		/*if(credentials.rate_apiuserid && credentials.rate_apiaccount 
			&& credentials.rate_apipwd && credentials.scac
		){
		*/
			//saving the form details

			//making api active status false intially
			$scope.apisetup.rate_apiactivated = false;
			//now saving the total details to the db
			//console.log($scope.apisetup);
			CarrierApiService.postApiDetails($scope.apisetup)
				.success(function(result){
					if(result.success && result.id){
						$scope.apisetup.id = result.id;
						
						//now building deffered obejct to resolve the api status and update accordingly when promise object is returned
						var deferred = $q.defer();
						var promise = deferred.promise;

						promise.then(function(value){
							//console.log(value.data);
							if(value.data && value.data.Result) {
								if(value.data.Result == "Success"){
									//generating success message
									$scope.success.status = true;
									$scope.success.message = 'Connectivity passed successfully';
									
									//making api active status false intially
									$scope.apisetup.rate_apiactivated = true;
									$scope.apiStatusImage = $scope.apiStatusImageSuccess;
									CarrierApiService.postApiDetails($scope.apisetup); //saving the new status as success
								}else if(value.data.Result == "Error"){
									$scope.error.status = true;
									$scope.error.message = value.data.ErrorMessage ? value.data.ErrorMessage : 'Unknown Error Response';
									$scope.apiStatusImage = $scope.apiStatusImageError;
								}
									
							}
						}, function(reason){
							console.log(reason);
						}, function(update){
							console.log(update);
						});

						deferred.resolve(
							CarrierApiService.checkApiStatus(credentials.rate_apiuserid, credentials.rate_apipwd, credentials.rate_apiaccount, credentials.scac)
						);

					}else if(result.error){
						alert(result.error);
					}
				});
		//} 
	};

	$scope.addCarrier = function(){
		window.location.href="/front/setup-connectivity/add-carrier";
	};
	
	function fetchDatafromServer(){
		CarrierApiService.fecthSelectedCarriers(true) //calling factory to make ajax request
			.success(function(result){
				 if(result.success && result.carriers && result.carrierlist){
					//$scope.selectedCarrierList = result.carriers;
					$scope.apiSetupCollection.carriers = result.carriers;
					
					
					//if the page has params to load specific details of a carrier from the url search params then load that first
					//otherwise load the default one
					var urlSearchParams = $location.search()
					var scac = urlSearchParams.scac;
					var carrierID = urlSearchParams.id;
					if(scac && carrierID)
						$scope.visibleCarrierApiSetup(carrierID, scac);
					else{
						//iterating to the list to get the first api enabled carrer to load
						var keepGoing = true;
						angular.forEach($scope.apiSetupCollection.carriers, function(carrier, index){
							if(keepGoing){
								if(carrier.apiEnabled !== undefined && carrier.apiEnabled == 1){
									$scope.visibleCarrierApiSetup(carrier.id, carrier.scac);
									keepGoing = false;
								}	
							}	
						});
					}
				}
			});
	}

	function fetchAccsTariffs(carrierId, filter){
		var deferredAccs = $q.defer();
		var promiseAccs = deferredAccs.promise;

		promiseAccs.then(function(result){
			//console.log(result);
			if(result.data.success){
				if(result.data.rates.tariffsDefault)
					$scope.accsRates.defaultRates = result.data.rates.tariffsDefault
				else
					$scope.accsRates.defaultRates = [];
				
				if(result.data.rates.tariffsCustom)
					$scope.accsRates.customRates = result.data.rates.tariffsCustom;
				else
					$scope.accsRates.customRates = [];
				if(result.data.accsTypes){
					var accsTypes = result.data.accsTypes;
					accsTypes.unshift({accsname: "Not Specified", id: ""});
					$scope.accsRates.accsTypes = accsTypes;
					//console.log($scope.accsRates.accsTypes);
				}else
					$scope.accsRates.accsTypes = [];
				
				if(result.data.calculationTypes)
					$scope.accsRates.calculationTypes = result.data.calculationTypes;
				else
					$scope.accsRates.calculationTypes = [];
			}
		}, function(reason){
			console.log(reason);
		}, function(progress){
			console.log(progress);
		});

		deferredAccs.resolve(CarrierApiService.getCarrierAccTariffDetails(carrierId, filter));
	}

	function fetchFuelTariffs(carrierId, filter){
		var deferredFuel = $q.defer();
		var promiseFuel = deferredFuel.promise;

		promiseFuel.then(function(result){
			//console.log(result);
			if(result.data.success){
				if(result.data.rates.tariffsDefault && result.data.rates.totalDefaultCount){
					$scope.fuelRates.defaultRates = result.data.rates.tariffsDefault;
					$scope.fuelRates.totalDefaultCount = result.data.rates.totalDefaultCount;
				}else{
					$scope.fuelRates.defaultRates = [];
					$scope.fuelRates.totalDefaultCount = 0;
				}

				if(result.data.rates.tariffsCustom && result.data.rates.totalCustomRateCount){
					$scope.fuelRates.customRates = result.data.rates.tariffsCustom;
					$scope.fuelRates.totalCustomRateCount = result.data.rates.totalCustomRateCount;
				}
				else{
					$scope.fuelRates.customRates = [];
					$scope.fuelRates.totalCustomRateCount = 0;
				}

				if(result.data.fuelTypes){
					var fuelTypes = result.data.fuelTypes;
					fuelTypes.unshift({fuelType: "Not Specified", id: ""});
					$scope.fuelRates.fuelTypes = fuelTypes;
				}
				else
					$scope.fuelRates.fuelTypes = [];
			}
		}, function(reason){
			console.log(reason);
		}, function(progress){
			console.log(progress);
		});

		deferredFuel.resolve(CarrierApiService.getCarrierFuelTariffDetails(carrierId, filter));
	}
}