function registrationStep4Controller(SyncCarriers, $scope, $http, $compile, $rootScope, Upload){
	//we will store all of our form data in this object
	$scope.formStep4 = {saveButtonText: 'Next', carriers: []};
	
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	//intializing data from the server to list all previously selected carriers
	fetchDatafromServer();

	//function to handle and submit step 3
	$scope.submitStep4 = function(form){
		if(form && form.$valid){
			//flag to enable loader for registration process
			$scope.processing.status = true;
			
			$scope.formStep4.saveButtonText = 'Saving...';
			
			$http.post('/front/client-register', $scope.formStep3)
				.success(function (data){
					$scope.formStep4.saveButtonText = 'Next';
					if(data.success){
						//now saving all contact info by broadcasting to the event listeners
						$scope.$broadcast('saveContactInfo', {});
						
						//intializing data from the server to list all previously selected carriers
						fetchDatafromServer();
						
						//disabling loader after complete
						$scope.processing.status = false;
					}else if(data.error){
						$scope.error.status = true;
						$scope.error.message = data.error;

						//disabling loader after complete
						$scope.processing.status = false;
					}
				})
				.error(function(data, status){
					alert(status);
					if(status == 302 || status == 301 || status == 401)
						window.location.href = '/'
				});
		}
	};
	


	$scope.$on('refetch', function(e, args){
		//console.log('here in events');
		fetchDatafromServer();
	});

	
	function fetchDatafromServer(){
		SyncCarriers.fetch(true) //calling factory to make ajax request
			.success(function(result){
				 if(result.success && result.carriers && result.carrierlist){
					$scope.selectedCarrierList = result.carriers;
					$scope.formStep3.carriers = result.carrierlist;
				}
			});
	}
}