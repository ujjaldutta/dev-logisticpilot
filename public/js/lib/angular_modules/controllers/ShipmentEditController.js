'use strict';

function ShipmentEditController(ShipmentAPIEndpoint, Shipment, Customer, CustomerLocation, Carrier, Upload, $scope, $location, $window, $compile){
	$scope.buttonMailText = 'Mail Password';
	$scope.stop_counter = 3;
	$scope.product_counter = 5;
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOption = {
        componentRestrictions: {  },
        types: ["(regions)"]
    }

    //collection  of Shipment type objects
	$scope.shipment = {};
	$scope.clientDefaultSettings = {};
	$scope.themes = [];
	$scope.languages = [];
     
	//static collection of product types and products
	/*$scope.productClasses = [ 
		{id: 50, label: 50}, 
		{id: 55, label: 55},
		{id: 60, label: 60},
		{id: 65, label: 65},
		{id: 70, label: 70},
		{id: 77.5, label: 77.5},
		{id: 85, label: 85},
		{id: 92.5, label: 92.5},
		{id: 100, label: 100},
		{id: 110, label: 110},
		{id: 125, label: 125},
		{id: 150, label: 150},
		{id: 175, label: 175},
		{id: 200, label: 200},
		{id: 250, label: 250},
		{id: 300, label: 300},
		{id: 400, label: 400},
		{id: 500, label: 500},
	];*/

	/*$scope.products = [
		{id: 'p1', label: 'Product 1'}, 
		{id: 'p2', label: 'Product 2'}, 
		{id: 'p3', label: 'Product 3'}, 
	];*/
	
	/*$scope.shipmentTerms = [
		{id: 1, label: 'Prepaid'}, 
		{id: 2, label: 'Postpaid'}, 
	];*/
	
	/*$scope.bolTemplates = [
		{id: 1, label: 'Template 1'}, 
		{id: 2, label: 'Template 2'}, 
	];*/

	/*$scope.invoiceTerms = [
		{id: 1, label: 'Term 1'}, 
		{id: 2, label: 'Term 2'}, 
	];*/
	
	/*$scope.invoiceDays = [
		{id: 1, label: 'Sunday'}, 
		{id: 2, label: 'Monday'}, 
		{id: 3, label: 'Tuesday'}, 
		{id: 4, label: 'Wednesday'}, 
		{id: 5, label: 'Thursday'}, 
		{id: 6, label: 'Friday'}, 
		{id: 7, label: 'Saturday'}, 
	];*/

	/*$scope.invoiceTemplates = [
		{id: 1, label: 'Template 1'}, 
		{id: 2, label: 'Template 2'},  
	];*/
        
        //view tables
        $scope.products = [];
        $scope.productClasses = [];
        $scope.shipmentTerms = [];
        $scope.bolTemplates = [];
        $scope.invoiceTerms = [];
        $scope.invoiceDays = [];
        $scope.invoiceTemplates = [];
		$scope.equipTypes = [];
		$scope.modeTypes = [];
		$scope.generatenumber = '';
		$scope.customerLocationData = {count: 0, list: []};

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = ShipmentAPIEndpoint.base ? ShipmentAPIEndpoint.base : '/';
	}

	$scope.locationSelected = function(colobj){
	console.log(this);
	if(this.inputName == 'pickup_addr'){
	 angular.element('#pick_location_adr1').val(colobj.originalObject.location_adr1);
	 $scope.client_shipment.pick_location_adr1.$setValidity("pick_location_adr1", false);
	 angular.element('#pick_location_adr2').val(colobj.originalObject.location_adr2);
	 angular.element('#pick_location_city').val(colobj.originalObject.location_city);
	 angular.element('#pick_location_state').val(colobj.originalObject.location_state);
	 angular.element('#pick_location_postal').val(colobj.originalObject.location_postal);
	 //angular.element('#pick_location_postal').val("01.05.2012").datepicker('update');
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 }
	if(this.inputName == 'del_addr'){
	 angular.element('#del_location_adr1').val(colobj.originalObject.location_adr1);
	 angular.element('#del_location_adr2').val(colobj.originalObject.location_adr2);
	 angular.element('#del_location_city').val(colobj.originalObject.location_city);
	 angular.element('#del_location_state').val(colobj.originalObject.location_state);
	 angular.element('#del_location_postal').val(colobj.originalObject.location_postal);
	 //angular.element('#pick_location_postal').val("01.05.2012").datepicker('update');
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 }	 
	if(this.inputName == 'add_stop_name[]'){
	var fieldid = this.id;
    var fieldid_seg = fieldid.split('_'); 
    var counter = parseInt(fieldid_seg[3]);	  console.log(counter);
	 angular.element('#add_stop_addr1_'+counter+'2').val(colobj.originalObject.location_adr1);
	 angular.element('#add_stop_addr2_'+counter+'3').val(colobj.originalObject.location_adr2);
	 angular.element('#add_stop_city_'+counter+'4').val(colobj.originalObject.location_city);
	 angular.element('#add_stop_state_'+counter+'5').val(colobj.originalObject.location_state);
	 angular.element('#add_stop_postcode_'+counter+'6').val(colobj.originalObject.location_postal);
	 //angular.element('#pick_location_postal').val("01.05.2012").datepicker('update');
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 }	 
	}
	
	$scope.billtolocationSelected = function(colobj){
	console.log(colobj.originalObject);
	if(this.inputName == 'billto_addr'){
	 angular.element('#billto_adr1').val(colobj.originalObject.billto_adr1);
	 angular.element('#billto_adr2').val(colobj.originalObject.billto_adr2);
	 angular.element('#billto_city').val(colobj.originalObject.billto_city);
	 angular.element('#billto_state').val(colobj.originalObject.billto_state);
	 angular.element('#billto_postal').val(colobj.originalObject.billto_postal);
	 //angular.element('#pick_location_postal').val("01.05.2012").datepicker('update');
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 //angular.element('#pick_location_adr1').val( addr_data[0]);
	 } 
	}	
	
	
	$scope.addStopRow = function(){ 
	  var counter = $scope.stop_counter;
	  angular.element("#add_row").after($compile('<tr> <td class="first"> <div class="input-group"> <!--input type="text" name="add_stop_name[]" class="form-control" placeholder="Name Search"--> <angucomplete-alt placeholder="Name Search" selected-object="locationSelected" pause="200" remote-url="/front/customer-locations/ajax-list-locations?filters={}&limit=20&page=1&orderBy=&orderType=&clientID=&autocomplete=true&q=" search-fields="location_name" title-field="location_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="add_stop_name[]" id="add_stop_name_'+counter+'_1" /> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button> </span> </div> </td> <td> <input type="text" name="add_stop_addr1[]" id="add_stop_addr1_'+counter+'2" class="form-control" placeholder="Address line 1" > </td> <td> <input type="text" name="add_stop_addr2[]" id="add_stop_addr2_'+counter+'3" class="form-control" placeholder="Address line 2" > </td> <td> <input type="text" name="add_stop_city[]" id="add_stop_city_'+counter+'4" class="form-control" placeholder="City" > </td> <td> <input type="text" name="add_stop_state[]" id="add_stop_state_'+counter+'5" class="form-control" placeholder="State" > </td> <td> <input type="text" name="add_stop_postcode[]" id="add_stop_postcode_'+counter+'6" class="form-control" placeholder="Postal" > </td> <td> <select type="text" name="add_stop_pd[]" class="form-control" ><option value="P">Pickup - P</option><option value="D">DropOff - D</option></select> </td> <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td> </tr>')($scope));
      //expect(element.find('#ex1_value').length).toBe(1);	
	  //counter++;
	  $scope.stop_counter ++;
	} 

	$scope.addProduct = function(){
	 var counter = $scope.product_counter; alert(counter);
	 angular.element("#product_add_more").after($compile('<tr><td> <input type="text" class="form-control" placeholder="" > </td>  <td class="type">   <angucomplete-alt placeholder="" pause="200" remote-url="/front/customer-products/ajax-package-type-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_type[]" /> </td>  <td>  <angucomplete-alt placeholder="" pause="200" remote-url="/front/customer-products/ajax-package-type-list?autocomplete=true&q=" search-fields="item_name" title-field="item_name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.unit_desc[]" /></td>  <td>  <input type="text" class="form-control" placeholder="" >    </td> <td> <input type="text" class="form-control" placeholder="" >   </td> <td style="width:20%;">   <div class="diamention">   <input type="text" class="form-control" placeholder="L" >   <input type="text" class="form-control" placeholder="W" >   <input type="text" class="form-control last" placeholder="H" >  </div>  </td> <td>  <angucomplete-alt placeholder="" pause="200" remote-url="/front/customer-products/ajax-class-type-list?autocomplete=true&q=" search-fields="name" title-field="name" minlength="2" description-field="description" input-class="form-control" match-class="highlight" input-name="shipment.class[]" />  </td> <td>   <input type="text" class="form-control" placeholder="">   </td>   <td>  <input type="checkbox">                                   </td>   <td> <input type="text" class="form-control" placeholder="1"> </td> <td><button type="button" class="btn btn-default deleteRow delete"><span class="minus"></span>Delete</button></td> </tr>')($scope));
	 $scope.product_counter ++;
	} 
	
	//fetching languages
	//getLanguages();
	//fetching themes
	//getThemes();
        //getCommodityType();
        //getClassesType();
       // getShipmentTermsType();
        //getBolTemplatesType();
        getInvoiceTermsType();
        //getInvoiceDaysType();
        //getInvoiceTemplatesType();
		getAllEquipmentTypes();
		getAllModeTypes();
		getRandNumber();
		//getClientLocation(6);
	
	/*
	* when valid cust id is loaded or assigned 
	* after dom rendered make a aysnc req to the server 
	* to get the information
	*/
	$scope.$watch('custID', function(val){
		if(!isNaN(val) && val > 0){
			//fetching the selected Shipment data
			getShipmentData(val);
		}
			
	});

	$scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/registration-steps/upload',
                    fields: {
                        'clientID': $scope.custID
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.clientData.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                //evt.config.file.name + '\n' + $scope.formData.log;
                }).success(function (data, status, headers, config) {
                	if(data.error){
                		$scope.error.status = true;
						$scope.error.message = data.error;
                	}else if(data.success){
                		$scope.success.status = true;
						$scope.success.message = 'file ' + config.file.name + ' uploaded successfully!';
                	}
                  
                   //$scope.$apply();
                });
            }
        }
    };


	/*
	* Saving the profile fields
	*/
	
	$scope.saveProfileData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			Shipment.postShipmentData($scope.custID, $scope.clientData)
				.then(function(response){
					if(response.data.success && response.data.custId){
						$scope.custID = response.data.custId;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
						
						if($scope.newUser === true){
							return Shipment.createUser($scope.custID, $scope.clientData);
						}
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							$scope.error.message = Shipment.formatValidationErrors(response.data.error);
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.then(function(response){
					if(response === undefined)
						return;

					if(response.data.success && response.data.userId){
						$scope.clientData.userId = response.data.userId || 0;
						$scope.clientData.client_password = response.data.client_password || '';
					}else{
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							$scope.error.message = "User creation error: " + Shipment.formatValidationErrors(response.data.error);
						}else
							$scope.error.message = response.data.error;
					}
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}

	/*
	* Saving the profile fields
	*/
	
	$scope.saveSettingsData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			Shipment.postShipmentDefaultSettings($scope.custID, $scope.clientDefaultSettings)
				.then(function(response){
					//console.log(response);
					if(response.data.success && response.data.id){
						$scope.clientDefaultSettings.id = response.data.id;
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}

	function getShipmentData(custID){
		if(!custID)
			custID = $location.search().custID || 0;
		
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		Shipment.makeAsyncRequest(Shipment.getShipmentData(custID))
			.then(function(response){
				if(response.data.success && response.data.profile){
					$scope.clientData = response.data.profile;
					
					//filling the location input with the already selected location details but saved separately
					$scope.clientData.location = __fillLocationDetails($scope.clientData, 'client');
					//console.log($scope.ShipmentData);
					//again making async request to get other details like billing and shipping
					return Shipment.makeAsyncRequest(Shipment.getShipmentDefaultSettings(custID));
				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			//callback for billing
			.then(function(response){
				if(response && response.data.success && response.data.defaultSettings.default_settings){
					$scope.clientDefaultSettings = response.data.defaultSettings.default_settings;
					
					//now converting data type to either true or false for checkboxes
					$scope.clientDefaultSettings.receive_edi204 = !!parseInt($scope.clientDefaultSettings.receive_edi204);
					$scope.clientDefaultSettings.receive_edi210 = !!parseInt($scope.clientDefaultSettings.receive_edi210);
					$scope.clientDefaultSettings.receive_edi214 = !!parseInt($scope.clientDefaultSettings.receive_edi214);
					$scope.clientDefaultSettings.receive_edi216 = !!parseInt($scope.clientDefaultSettings.receive_edi216);
					$scope.clientDefaultSettings.send_edi204 = !!parseInt($scope.clientDefaultSettings.send_edi204);
					$scope.clientDefaultSettings.send_edi210 = !!parseInt($scope.clientDefaultSettings.send_edi210);
					$scope.clientDefaultSettings.send_edi214 = !!parseInt($scope.clientDefaultSettings.send_edi214);

					//processing locations devided into diff attributes to place it one place
					$scope.clientDefaultSettings.pickup_location = __fillLocationDetails($scope.clientDefaultSettings, 'pickup');
					$scope.clientDefaultSettings.delivery_location = __fillLocationDetails($scope.clientDefaultSettings, 'delivery');
					$scope.clientDefaultSettings.billto_location = __fillLocationDetails($scope.clientDefaultSettings, 'billto');

				} 
			}, function (reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	/* function __fillLocationDetails(collection){
		var location = '';
		
		if(collection.client_city)
			location += collection.client_city + ', ';
		if(collection.client_state)
			location += collection.client_state + ', ';
		if(collection.client_postal)
			location += collection.client_postal + ', ';
		if(collection.client_country)
			location += collection.client_country;

		return location;
	} */

	function __fillLocationDetails(collection, prf){

		var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		
		if(collection[city] !== undefined && collection[city])
			location += collection[city] + ', ';
		if(collection[state] !== undefined && collection[state])
			location += collection[state] + ', ';
		if(collection[postal] !== undefined && collection[postal])
			location += collection[postal] + ', ';
		if(collection[country] !== undefined && collection[country])
			location += collection[country];

		return location;
	}
	
	
	$scope.sendEmail = function(){
		$scope.buttonMailText = 'Please wait...';
		var email = $scope.clientData.client_email;
		var pass = $scope.clientData.client_password;		
		
		Shipment.makeAsyncRequest(Shipment.sendPassword(email, pass))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			})
			
		
	}
	
	$scope.updatePassword = function(){
		if(!$window.confirm('Are you sure to reset the passsword?'))
			return;

		$scope.buttonMailText = 'Please wait...';
		var email = $scope.clientData.client_email;	
		var custId = $scope.custID;

		Shipment.makeAsyncRequest(Shipment.sendUpdatePassword(email,custId))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function getThemes(){
		Shipment.makeAsyncRequest(Shipment.getAllThemes())
			.then(function(response){
				if(response.data.success !== undefined && response.data.themes !== undefined){
					$scope.themes = response.data.themes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function getLanguages(){
		Shipment.makeAsyncRequest(Shipment.getAllLanguages())
			.then(function(response){
				if(response.data.success !== undefined && response.data.languages !== undefined){
					$scope.languages = response.data.languages;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getCommodityType(){
		Shipment.makeAsyncRequest(Shipment.getAllCommodities())
			.then(function(response){
				if(response.data.success !== undefined && response.data.commodity !== undefined){
					$scope.products = response.data.commodity;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getClassesType(){
		Shipment.makeAsyncRequest(Shipment.getAllClasses())
			.then(function(response){
				if(response.data.success !== undefined && response.data.classes !== undefined){
					$scope.productClasses = response.data.classes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}        
        
        function getShipmentTermsType(){
		Shipment.makeAsyncRequest(Shipment.getAllShipmentTerms())
			.then(function(response){
				if(response.data.success !== undefined && response.data.shipment !== undefined){
					$scope.shipmentTerms = response.data.shipment;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getBolTemplatesType(){
		Shipment.makeAsyncRequest(Shipment.getBolTemplates())
			.then(function(response){
				if(response.data.success !== undefined && response.data.bolTemplates !== undefined){
					$scope.bolTemplates = response.data.bolTemplates;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceTermsType(){
		Customer.makeAsyncRequest(Customer.getInvoiceTerms())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceTerms !== undefined){
					$scope.invoiceTerms = response.data.invoiceTerms;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceDaysType(){
		Shipment.makeAsyncRequest(Shipment.getAllInvoiceDays())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceDays !== undefined){
					$scope.invoiceDays = response.data.invoiceDays;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
        
        function getInvoiceTemplatesType(){
		Shipment.makeAsyncRequest(Shipment.getAllInvoiceTemplates())
			.then(function(response){
				if(response.data.success !== undefined && response.data.invoiceTemplates !== undefined){
					$scope.invoiceTemplates = response.data.invoiceTemplates;
				}
			}, function(reason){
				console.warn(reason);
			});
	}  
        
    function getAllEquipmentTypes(){
        Carrier.makeAsyncRequest(Carrier.getAllEquipmentTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.equipmentTypes !== undefined) {
                        $scope.equipTypes = response.data.equipmentTypes;
						//alert($scope.equipTypes);
                    }
                }, function (reason) {
                    console.warn(reason);
                });	  
	}  

    function getAllModeTypes(){
        Carrier.makeAsyncRequest(Carrier.getAllModesTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.modestype !== undefined) {
                        $scope.modeTypes = response.data.modestype;
                    }
                }, function (reason) {
                    console.warn(reason);
                }); 
	} 	
	
	function getRandNumber(){
	  $scope.generatenumber = Math.floor(Math.random()*90000) + 10000;
	}
	
	/*function getClientLocation(params){
		CustomerLocation.makeAsyncRequest(CustomerLocation.listLocations(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.customerLocationData.count = response.data.count;
					$scope.customerLocationData.list = response.data.results ? response.data.results : [];
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				//angular.element('.loading').addClass('hidden');
			});	
	}*/
}


