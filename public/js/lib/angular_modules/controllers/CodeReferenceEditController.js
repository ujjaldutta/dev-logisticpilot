'use strict';

function CodeReferenceEditController(CodeReferenceAPIEndpoint, CodeReference, $scope, $location, $window) {
    $scope.saveBtnText = 'Save';

    //model attribute to show/hide error message notification
    $scope.error = {
        status: false,
        message: '',
    };

    $scope.success = {
        status: false,
        message: '',
    };

    //collection  of customer type objects
    $scope.codeReferenceData = {};    
    $scope.codeReferenceId = null;

    /* method to dismiss the alert */
    $scope.hideMessage = function () {
        $scope.error = {
            status: false,
            message: '',
        };

        $scope.success = {
            status: false,
            message: '',
        };
    }

    $scope.cancelEditing = function () {
        $window.location.href = CodeReferenceAPIEndpoint.base ? CodeReferenceAPIEndpoint.base : '/';
    }

    /*
     * when valid cust id is loaded or assigned 
     * after dom rendered make a aysnc req to the server 
     * to get the information
     */
    $scope.$watch('codeReferenceId', function (val) {
        if (!isNaN(val) && val > 0) {
            if (!$scope.newProduct && $scope.codeReferenceId) {
                //fetching the selected customer data
                getCodeReferenceData($scope.codeReferenceId);
            }            
        }

    });


    /*
     * Saving the profile fields
     */

    $scope.saveProductData = function (form) {
        if (form && form.$valid) {
            //making the loader visible for user
            angular.element('.loading').removeClass('hidden');

            CodeReference.postCodeData($scope.codeReferenceId, $scope.codeReferenceData)
                    .then(function (response) {
                        if (response.data.success && response.data.codeReferenceId) {
                            $scope.codeReferenceData.id = response.data.codeReferenceId;
                            $scope.success.status = true;
                            $scope.success.message = response.data.success;

                            $scope.error.status = false;
                            $scope.error.message = '';
                        } else if (response.data.error !== undefined) {
                            $scope.success.status = false;
                            $scope.success.message = '';

                            $scope.error.status = true;
                            if (response.data.error !== undefined && response.data.error instanceof Object) {
                                $scope.error.message = 'Unhandled Exception occurred';
                            } else
                                $scope.error.message = response.data.error;
                        }
                    }, function (reason) {
                        console.log(reason);
                    })
                    .finally(function (data) {
                        //making the loader invisible for user
                        angular.element('.loading').addClass('hidden');
                    });
        }
    }


    function getCodeReferenceData(id) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');

        CodeReference.makeAsyncRequest(CodeReference.getCodeData(id))
                .then(function (response) {
                    if (response.data.success && response.data.codeReference) {
                        $scope.codeReferenceData = response.data.codeReference;                        
                    }
                    //console.log(response);
                }, function (reason) {
                    console.warn(reason);
                })
                .finally(function (data) {
                    //making the loader invisible for user
                    angular.element('.loading').addClass('hidden');
                });
    }
    
}




