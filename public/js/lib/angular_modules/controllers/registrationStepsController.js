function registrationStepsController (SyncCarriers, $rootScope, $state, $scope, $http, $location, Upload){
	//checking final step validity when changing from 
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
		if(toState.name && toState.name == 'form.carrierSetup'){
			SyncCarriers.validateAllSteps()
				.success(function(result){
					if(result.success)
						$scope.validateAllSteps = true;
					else if(result.error)
						$scope.validateAllSteps = false;
				});
		}
	});

	//checking validity of final step if directly loaded this state
	if(!$scope.validateAllSteps && window.location.hash == '#/form/carrier-Setup'){
		SyncCarriers.validateAllSteps()
			.success(function(result){
				if(result.success)
					$scope.validateAllSteps = true;
				else if(result.error)
					$scope.validateAllSteps = false;
			});
	}

	//checking validity of final step if step 5 url is crawled
	if(!$scope.validateAllSteps && window.location.hash == '#/form/finish'){
		$state.go('form.profile');
	}
	//event to check all steps are valid when forwarding to the final step
	$scope.$on('ValidateAllSteps', function(e, value){
		if(value.status !== undefined && value.status){
			$scope.validateAllSteps = true;
		}else if(value.status !== undefined && !value.status){
			$scope.validateAllSteps = false;
		}
	});

	//we will store all of our form data in this object
	$scope.formData = {saveButtonText: $scope.saveButtonNormalTextLocale, location: null};
	
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOptionsProfile = {
        componentRestrictions: {  },
        types: ["(regions)"]
    }
	
	//fetching already saved results
	fetchProfileDetails();

	$scope.finishAndExit = function(){
		SyncCarriers.finishAndExit()
			.success(function(result){
				if(result.success)
					$scope.saveAndExit();
				else if(result.error)
					alert(result.error);
			});
	};

	$scope.formData.log = '';

	$scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/registration-steps/upload',
                    fields: {
                        'clientID': $scope.formData.clientID
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.formData.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                //evt.config.file.name + '\n' + $scope.formData.log;
                }).success(function (data, status, headers, config) {
                	if(data.error){
                		$scope.error.status = true;
						$scope.error.message = data.error;
                	}else if(data.success){
                		$scope.formData.log = 'file ' + config.file.name + 'uploaded successfully!'

                		//updating the image after successfull upload
                		$scope.site.image = data.url ? data.url : $scope.site.image;
                	}
                  
                   //$scope.$apply();
                });
            }
        }
    };
			
	// function to process the form step 1
	$scope.submitStep1 = function(form) {
		if(form && form.$valid){
			//$scope.user._token = form._token;
			//$scope.user.subscription_plan_id = form.subscription_plan_id;
			
			//flag to enable loader for registration process
			$scope.processing.status = true;
			
			$scope.formData.saveButtonText = $scope.saveButtonLoadingTextLocale;
			
			$http.post('/front/client-register', $scope.formData)
				.success(function (data){
					$scope.formData.saveButtonText = $scope.saveButtonNormalTextLocale;
					if(data.success){
						//$scope.formData.status = true;
						$scope.formData.clientWLID = data.clientWLID;
						
						//disabling loader after complete
						$scope.processing.status = false;

						//if redirect request
					}else if(data.error){
						$scope.error.status = true;
						$scope.error.message = data.error;

						//disabling loader after complete
						$scope.processing.status = false;
					}
				})
				.error(function(data, status){
					alert(status);
				});
		}else
			alert('The form has invalid data');
	};

	//method to handle save and continue later function
	$scope.saveAndExit = function(form){
		if(form !== undefined)
			$scope.submitStep1(form);
		
		window.location.href='/auth/logout';
	};

	$scope.selectedCountry = function(selected){
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.formData.client_country = selected.title;
	};

	//method to handle selected postcodes
	$scope.postCodeSelected = function(selected) {
		if(selected === undefined)
			return;
		
		//console.log(selected);
		
		$scope.formData.client_postal = selected.title;
		$scope.formData.client_city = selected.originalObject.city_name;
		$scope.formData.client_state = selected.originalObject.state_code;
		$scope.formData.client_country = selected.originalObject.country_code;

		$scope.$broadcast('setOutsideValue', {id: 'client_city', value: selected.originalObject.city_name});
		$scope.$broadcast('setOutsideValue', {id: 'client_country', value: selected.originalObject.country_code});
		$scope.$broadcast('setOutsideValue', {id: 'client_state', value: selected.originalObject.state_code});
		
    };

    $scope.citySelected = function(selected){
    	if(selected === undefined)
			return;

		//console.log(selected);
		$scope.formData.client_city = selected.title;
    };
	
	$scope.stateSelected = function(selected){
		if(selected === undefined)
			return;
		
    	//console.log(selected);
    	$scope.formData.client_state = selected.title;
    };

    $scope.countryChanged = function(str){
    	$scope.formData.client_country = str;
    };
    
    $scope.postcodeChanged = function(str){
    	$scope.formData.client_postal = str;
    };
    $scope.stateChanged = function(str){
    	$scope.formData.client_state = str;
    };
    $scope.cityChanged = function(str){
    	$scope.formData.client_city = str;
    };

    function fetchProfileDetails(){
    	SyncCarriers.getStepProfileDetails()
    		.success(function(result){
    			if(result.success){
    				$scope.formData = angular.extend($scope.formData, result.profile);
    				var location = '';
    				if(result.profile.client_city)
    					location = result.profile.client_city;

    				if(result.profile.client_state)
    					location += ', ' + result.profile.client_state;

    				if(result.profile.client_postal)
    					location += ', ' + result.profile.client_postal;

    				if(result.profile.client_country)
    					location += ', ' + result.profile.client_country;

    				$scope.formData.location = location;
    			}
    		});
    }
}