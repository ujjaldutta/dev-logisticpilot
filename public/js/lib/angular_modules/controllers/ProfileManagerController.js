'use strict';

function ProfileManagerController(carrierStatusAPIEndpoint, CarrierApiService,$scope, $http, $window, Upload, dataService,$compile, $q, $location, $timeout,uiGridConstants){
	
	
	
		  $scope.profile = {};
		  $scope.contract= {};
		  
		  $scope.master = {};
		  $scope.message='';
		  $scope.cmessage='';
		  $scope.profile_id='';
		  
		  $scope.profilelist={};
		  $scope.contractlist={};
		  $scope.pageNos = [{id: 2, val: '2 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, {id: 25, val: '25 Per Page'}, ];
		  $scope.recordLimit = 10;
		  $scope.currentPage = 1;
		  $scope.RateTypeOptions=[];
		  $scope.ServiceLevelOptions=[];
		  $scope.DistanceAppOptions=[];
		  
		  $scope.rightpanel='profile';
		  $scope.filters = [];
		  $scope.lanefilters = [];
		  $scope.country=[];
		  
		  $scope.ratelane= {};
		  $scope.lanerecordLimit = 4;
		  $scope.lanecurrentPage = 1;
		  $scope.laneratelist={};
		  $scope.WeightTypeOptions=[];
		  $scope.EQuipTypeOptions=[];
		  $scope.LocTypeOptions=[];
		  $scope.DistanceTypeOptions=[];
		  $scope.UnitTypeOptions=[];
		  $scope.choices = [{id: 'choice1'}];
		  $scope.choices2 = [{id: 'schoice1'}];
		  $scope.multidistance= [{id: 'dchoice1'}];
		  $scope.multicostperunit= [{id: 'cpuchoice1'}];
		  //$scope.ratelane.OriginalState=[{value: '',label:'Please Select State',disabled:true}];
		  //$scope.ratelane.OriginalCity=[{value: '',label:'Please Select City',disabled:true}];
		  //$scope.ratelane.OriginalZipcode=[{value: '',label:'Please Select Zipcode',disabled:true}];
		  
		  $scope.country=[];
		  $scope.pagination={current_page:1};
		
	$scope.getLength = function(obj) {
    return Object.keys(obj).length;
}
	  
/*************************************Save single profile**************************************/		
		//create a contract profile 
		  $scope.submitProfile = function(form) {
			
				angular.element('.loading').removeClass('hidden');
				
				  $http({
				    method  : 'POST',
				    url     : '/front/profiles/save-profile',
				    data    : $scope.profile, //forms user object
		
				   })
				    .success(function(data) {
				      if (data.errors) {
					
					$scope.message = data.message;
				      } else {
					$scope.message = data.message;
					$scope.reset();
					$scope.loadProfile({filters: $scope.filters});
					jQuery('#moreDetails1').modal('hide');
				      }
				      angular.element('.loading').addClass('hidden');
				    });
				    
			
		  };
		  
		  $scope.showdropdown=function(cprofile){
			  
			  $scope.selectedprofile=cprofile;
			  jQuery(".mainprofile").toggleClass("profilemenu");
		  }
		  
		  $scope.addProfile=function(){
			//  console.log($scope.profile);
			  $scope.pid=$scope.profile._id;
			  $scope.profile = angular.copy($scope.master);
		  };
		  
		  $scope.reloadProfile=function(){
			  $scope.editProfile($scope.pid);
		  };
/*************************************Load single profile for edit**************************************/	
		//load a profile for edit
		$scope.editProfile = function(cid){
		
				var httpRequest = $http({
						method: 'POST',
						data:{cid:cid},
						url: '/front/profiles/profile-ajaxbyid'
						
				
					}).success(function(data, status) {
						$scope.profile = data.profile;
						angular.element('.loading').addClass('hidden');
						
						
						
					});
			}
					  
/************************************Duplicate a profile***************************************/	
		//load a profile for edit
		$scope.copyProfile = function(profile){
		jQuery(".mainprofile").toggleClass("profilemenu");
				var httpRequest = $http({
						method: 'POST',
						data:{profile:profile},
						url: '/front/profiles/copy-profile'
						
				
					}).success(function(data, status) {
						$scope.loadProfile({filters: $scope.filters});
						angular.element('.loading').addClass('hidden');
						
						
						
					});
			}				
			
			
			
			
/************************************** Reset contract and ratelane form / Load default values for new**********************************/			
		  //reset contract and profile forms
		  $scope.reset = function() {
			$scope.profile = angular.copy($scope.master);
			$scope.message='';
			$scope.cmessage='';
		      };
		   $scope.resetcontract = function() {
			$scope.contract = angular.copy($scope.master);
			$scope.contract.carrierId='';
			jQuery("#countryex5_value").val('');
			$scope.message='';
			$scope.cmessage='';
		      };  
		    
			$scope.resetlane = function() {
				$scope.message='';
				$scope.cmessage='';
				var lane=$scope.ratelane
				$scope.ratelane = angular.copy($scope.master);
				$scope.ratelane.carrierId=lane.carrierId;
				$scope.ratelane.cId=lane.cId;
				$scope.ratelane.ProfileId=lane.ProfileId;
				$scope.choices = [{id: 'choice1'}];
				$scope.choices2 = [{id: 'schoice1'}];
				$scope.multidistance= [{id: 'dchoice1'}];
				$scope.multicostperunit= [{id: 'cpuchoice1'}];
				$scope.ratelane.OriginalCountrycode=$scope.ratelane.DestinationCountrycode='USA';            
               $scope.ratelane.OriginalCountry=$scope.ratelane.DestinationCountry='United States';   
				$scope.ratelane.CapacityWeightType= {id: 'Pounds',name:'Pounds'};
				$scope.ratelane.PackageWeightType= {id: 'Pounds',name:'Pounds'};
				$scope.ratelane.VolumeWeightType= {id: 'Pounds',name:'Pounds'};
				$scope.ratelane.DistanceType= {id: 'Mile',name:'Mile'};
		      };   

/***************************************Save a contract************************************/		
		
		 //save or update contract
		  $scope.submitContract = function(form) {
				
				if($scope.profile_id==''){
					alert("Please select a profile");
					return false;
				}
				
				if($scope.contract.RateType.id!='3'){
					
				$scope.contract.FAK50=$scope.contract.FAK55=$scope.contract.FAK65=$scope.contract.FAK70=$scope.contract.FAK75='';
				$scope.contract.FAK85=$scope.contract.FAK90=$scope.contract.FAK95=$scope.contract.FAK100=$scope.contract.FAK110='';
				$scope.contract.FAK125=$scope.contract.FAK150=$scope.contract.FAK175=$scope.contract.FAK200=$scope.contract.FAK250='';
				$scope.contract.FAK300=$scope.contract.FAK400=$scope.contract.FAK500='';
				
				}
				
				angular.element('.loading').removeClass('hidden');
				$scope.contract.ProfileId=$scope.profile_id;
				  $http({
				    method  : 'POST',
				    url     : '/front/profiles/save-contract',
				    data    : $scope.contract, //forms user object
				  
				   })
				    .success(function(data) {
				      if (data.errors) {
					
					$scope.message = data.message;
				      } else {
					$scope.cmessage = data.cmessage;
					$scope.loadProfile({filters: $scope.filters});
					jQuery('#moreDetails').modal('hide');
					//$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1});
					//$scope.resetcontract();
				      }
				      angular.element('.loading').addClass('hidden');
				    });
				    
			
		  };
		    
		      
		      		      
/***************************************load all profiles in sidebar************************************/
		//load profiles in left sidebar and first profile contact as default      
		$scope.loadProfile = function(params) {
			var httpRequest = $http({
			    method: 'POST',
			    url: '/front/profiles/profile-list',
			    data:{filters:params.filters,fields:'_id,RateProfileCode,RateProfileDescription,CustomerReferenceCode'},
		
			}).success(function(data, status) {
			    $scope.profilelist = data.profilelist;
			 
			   $scope.RateTypeOptions=data.RateTypeOptions;
			  $scope.ServiceLevelOptions=data.ServiceLevelOptions;
			  $scope.DistanceAppOptions=data.DistanceAppOptions;
			  
			   if($scope.profile_id=='' && data.profilelist.length>0)
			   { $scope.profile_id=data.profilelist[0].RateProfileCode;
			    $scope.profile=data.profilelist[0];
			    
					}
			    
			    if($scope.profile_id!=''){
			    $scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1})
			    //save for other controller
			    dataService.dataObj={profile_id:$scope.profile_id};
			    
			    
			    
				}
				
			  
				$scope.rightpanel='profile'	;
				
				
			});
		
		    };
		    
		    $scope.$watch('filter.profileName', function(value, oldValue){
				if(value != oldValue){
					
					$scope.filters = [{name: value}];
					$scope.loadProfile({filters: $scope.filters});
				}
			});	
			
			
/*****************************************Delete single profile**********************************/
		//delete a selected profile
		$scope.deleteProfile = function(profileid) {
			jQuery(".mainprofile").toggleClass("profilemenu");
			$scope.profile_id=profileid;
			if($window.confirm('Are you sure ?')){
					var httpRequest = $http({
						method: 'POST',
						data:{RateProfileCode:$scope.profile_id},
						url: '/front/profiles/delete-profile'
						
				
					}).success(function(data, status) {
						$scope.profilelist = data.profilelist;
						$scope.profile_id='';
						$scope.loadProfile({filters: $scope.filters});
						 angular.element('.loading').addClass('hidden');
					});
				};
		    };    
		    
/************************************ Load contract list ***************************************/	

		//load all contract of selected profile
		
		$scope.loadContract = function(params) {
			angular.element('.loading').removeClass('hidden');
			$scope.profile_id=params.profileid;
		
			//display correct first default  right panel
			$scope.rightpanel='profile';
			
			var httpRequest = $http({
						method: 'POST',
						data:{RateProfileCode:$scope.profile_id,limit:params.limit,page:params.page,orderBy:params.orderBy,orderType:params.orderType,filters:params.filters,fields:'carrierId,RateType,ServiceLevel,DistanceApp,EffectiveFrom,EffectiveTo,FAK50,FAK55,FAK65,FAK70,FAK75,FAK85,FAK90,FAK95,FAK100,FAK110,FAK125,FAK150,FAK175,FAK200,FAK250,FAK300,FAK400,FAK500,file,EquipmentType'},
						url: '/front/profiles/contract-list'
						
				
					}).success(function(data, status) {
						$scope.contractlist = data;
						angular.element('.loading').addClass('hidden');
						$scope.lanes=[];
						$scope.pagination={current_page:1};
						
						
						
						if(data.contracts.length>0)
						{//load first record default
						$scope.editContract(data.contracts[0]._id);
						
						
					    }
						else
						{
						$scope.laneratelist={};
						$scope.ratelane.cId='';
						$scope.ratelane.ProfileId='';
						//$scope.contractlist={};
						
						}
						 //save for other controller
						dataService.dataObj={profile_id:$scope.profile_id};
						
						//fetch Carrier API
						 //$timeout(fetchDatafromServer(),5000);
						
					});
			
		    };
		 
		//delete a selected contract 
		  $scope.deleteContract=function(id){
						if($window.confirm('Are you sure ?')){
							var httpRequest = $http({
								method: 'POST',
								data:{cid:id},
								url: '/front/profiles/delete-contract'
								
						
							}).success(function(data, status) {
								$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1});
							});
						};
				
			  };
			  
		//copy a contract
		$scope.copyContract=function(id){
			
			var httpRequest = $http({
								method: 'POST',
								data:{cid:id},
								url: '/front/profiles/copy-contract'
								
						
							}).success(function(data, status) {
								$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1});
							});
		}

		
		//sort contract column
		$scope.sortColumn = function(predicate) {
			   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
			   $scope.predicate = predicate;
			   //alert("A");
			   $scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage, 
					orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
				});
			};
	
		//contract pagination
		$scope.pageChanged = function(){
		
				$scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage});
			}

			$scope.$watch('recordLimit', function(value, oldValue){
				if(oldValue != value){
					$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit, 	page: $scope.currentPage});
				}
			});
/***************************************load contract to modify or load single contract************************************/	
		//load a contract for edit
		$scope.editContract = function(cid){
		$scope.cmessage='';
				var httpRequest = $http({
						method: 'POST',
						data:{cid:cid},
						url: '/front/profiles/contract-ajaxbyid'
						
				
					}).success(function(data, status) {
						$scope.contract = data.contract;
						jQuery("#countryex5_value").val($scope.contract.carrierId);
						angular.element('.loading').addClass('hidden');
						$scope.ratelane.carrierId=$scope.contract.carrierId;
						$scope.ratelane.cId=$scope.contract._id;
						$scope.ratelane.ProfileId=$scope.contract.ProfileId;
						
						$scope.lanes=[];
						$scope.pagination={current_page:1};
						
						if($scope.ratelane.cId!='' && $scope.ratelane.ProfileId!=''){
						//$scope.loadRatelanes({ limit: $scope.lanerecordLimit, 	page: $scope.lanecurrentPage});
						
						$scope.Statecode='';
						$scope.Statename='';
						$scope.Citycode='';
						
						$scope.loadlanes($scope.lanecurrentPage ? parseInt($scope.lanecurrentPage, $scope.lanerecordLimit) : 1);
						
						}
						
						if($scope.contract.RateType.id=='6'){
						$scope.matrixreset();	
						 $scope.loadGridData();
						 
						}
						
						
					});
			}
		
/***********************************Filter contract based on sidebar****************************************/		
		
		$scope.$watch('filter.carrier', function(value, oldValue){
				if(value != oldValue){
					
					$scope.filters = [{carrier: value}];
					$scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
			});	
			
		$scope.$watch('filter.EffectiveFrom', function(value, oldValue){
				if(value != oldValue){
					
					$scope.filters = [{EffectiveFrom: value,EffectiveTo:$scope.filters.EffectiveTo}];
					$scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
			});
			
		$scope.$watch('filter.EffectiveTo', function(value, oldValue){
				if(value != oldValue){
					
					$scope.filters = [{EffectiveFrom:$scope.filters.EffectiveFrom,EffectiveTo: value}];
					$scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
			});	
			
	
		$scope.showExpireContracts=function(type,value){
			$scope.filters=[];
			if(type=='all' && value==true){
				$scope.filters = [{Month:'all'}];
			}else if(type=='1' && value==true){
				$scope.filters = [{Month:'1'}];
			}
			else if(type=='2' && value==true){
				$scope.filters = [{Month:'2'}];
			}
			else if(type=='3' && value==true){
				$scope.filters = [{Month:'3'}];
			}
			
					$scope.loadContract({profileid:$scope.profile_id,limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
		}	
			
/*************************************************************************/
	
/***************************************Carrier autocomplete************************************/		    
	    //load carrier autocomplete
	    /*$scope.getcarriers = function(textval){
				$scope.states = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/carrier-ajax'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.states.push(item);
						});

					});
					
					
			  };*/
	$scope.searchCarrierAPI = function(userInputString, timeoutPromise) {
		return $http.post('/front/services/carrier-api', {q: userInputString}, {timeout: timeoutPromise});
	}		  
//angu-autocomplete function to hold values after selection			  
$scope.selectedCarrier = function (selected) {
				
				  if (selected) {
					$scope.contract.carrierId = selected.originalObject.name;
					$scope.contract.carriercode = selected.originalObject.id;
				  } else {
					$scope.fueltype = null;
				  }
		
		};
 //load country autocomplete
/*	    $scope.getCountry = function(textval){
				$scope.countrylist = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/country-list'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.countrylist.push(item);
						});

					});
					
					
			  };
			  * 
* */
			  
//country autocomplete
	$scope.searchCountryAPI = function(userInputString, timeoutPromise) {
	  return $http.post('/front/services/country-list', {q: userInputString}, {timeout: timeoutPromise});
	}

//angu-autocomplete function to hold values after selection	
$scope.selectedCountryOrg = function (selected) {
				
				  if (selected) {
					$scope.ratelane.OriginalCountry = selected.originalObject.name;
					$scope.ratelane.OriginalCountrycode = selected.originalObject.id;
				  } else {
					$scope.fueltype = null;
				  }
		
		};
		
	$scope.selectedCountryDest = function (selected) {
				
				  if (selected) {
					$scope.ratelane.DestinationCountry = selected.originalObject.name;
					$scope.ratelane.DestinationCountrycode = selected.originalObject.id;
				  } else {
					$scope.ratelane.DestinationCountry = null;
					$scope.ratelane.DestinationCountrycode = null;
				  }
		
		};
		
 //load states autocomplete
	    $scope.getStates = function(textval,countrycode){
				$scope.statelist = [];
				var param={q:textval,countrycode:countrycode};
				
				//console.log(param)
				angular.element('.orgloading').removeClass('hidden');
				var httpRequest = $http({
						method: 'POST',
						data:param,
						url: '/front/services/state-list'
						
				
					}).success(function(data, status) {
						$scope.statelist = [];
						$.each(data, function(i, item) {
							
							$scope.statelist.push(item);
						});
						angular.element('.orgloading').addClass('hidden');
					return;
					});
					
					
			  };
 
 //load city autocomplete
 	 
	  $scope.getCity = function(textval,countrycode,statecode){
				$scope.citylist = [];
				 angular.element('.orgloading').removeClass('hidden');
				 
				var param={q:textval,countrycode:countrycode,statecode:statecode};
				
				var httpRequest = $http({
						method: 'POST',
						data:param,
						url: '/front/services/city-list'
						
				
					}).success(function(data, status) {
						$scope.statelist = [];
						$.each(data, function(i, item) {
							
							$scope.citylist.push(item);
						});
						angular.element('.orgloading').addClass('hidden');
					return;
					});
					
					
			  };

//load zipcode autocomplete
$scope.remoteUrlOrgZipRequestFn = function(str) {
      return {q: str,countrycode:$scope.ratelane.OriginalCountrycode,statecode:$scope.ratelane.OrgStatecode,city:$scope.ratelane.OrgCity};
    };

$scope.selectedZipcode = function (selected) {
				
			//console.log(selected);
			if (selected) {
					$scope.zipcode = selected.originalObject.id;
					//console.log($scope.zipcode);
					
				  } else {
					$scope.zipcode = null;
				  }
				  
		
		};    
$scope.orglostFocusZipcode = function (curobject) {
			//console.log($scope.ratelane);
			if($scope.zipcode){	
				//console.log(curobject);
				$scope.ratelane.OriginalZipcode=$scope.zipcode;
				//console.log($scope.zipcode);
			}
		
		}; 	
	$scope.getZipcode = function(textval,countrycode,statecode,city){
				$scope.ziplist = [];
				if(textval.length<=2) return;
				
				//var param={q:textval,countrycode:countrycode,statecode:statecode,city:city};
				var param={q:textval,countrycode:countrycode};
				angular.element('.orgloading').removeClass('hidden');
				var httpRequest = $http({
						method: 'POST',
						data:param,
						url: '/front/services/postal-list'
						
				
					}).success(function(data, status) {
						$scope.ziplist = [];
						$.each(data, function(i, item) {
							
							$scope.ziplist.push(item);
						});
						angular.element('.orgloading').addClass('hidden');
					return $scope.ziplist;
					});
					
					
			  };
			  	  
//behavior on select of autocomplete on addratelane section
			   			 
	$scope.onSelect = function ($item, $model, $label) {
		//console.log($label);
		//console.log($item);return;
		
			$scope.$item = $item;
			$scope.$model = $model;
			$scope.$label = $label;
			if($label=='carrier'){
				jQuery("#carriercode").val($scope.$item.id);
				$scope.contract.carriercode=$scope.$item.id;
			}
			if($label=='orgcountry'){
				jQuery("#OriginalCountrycode").val($scope.$item.id);
				$scope.ratelane.OriginalCountrycode=$scope.$item.id;
			}
			//console.log($scope.$item);
			if($label=='orgzipcode'){
				
				$scope.$model=$scope.$item.id;
			}
			if($label=='orgtriplezipcode'){
				
				$scope.ratelane.OriginalTripleZipcode=$scope.$item.id;
			}
			
			
			
			if($label=='orgzipcodefrom'){
				
				$scope.ratelane.OriginalZipcodeFrom=$scope.$item.id;
			}
			if($label=='orgzipcodeto'){
				
				$scope.ratelane.OriginalZipcodeTo=$scope.$item.id;
			}
			if($label=='destcountry'){
				jQuery("#DestinationCountrycode").val($scope.$item.id);
				$scope.ratelane.DestinationCountrycode=$scope.$item.id;
			}
			
			if($label=='orgstate'){
				
				$scope.ratelane.OrgStatecode=$scope.$item.id;
			}
			if($label=='orgcity'){
				
				$scope.ratelane.OrgCity=$scope.$item.id;
			}
			
			if($label=='deststate'){
				
				$scope.$model=$scope.$item.id;
			}
			if($label=='destcity'){
				
				$scope.$model=$scope.$item.id;
			}
			if($label=='destzipcode'){
				
				$scope.ratelane.DestinationZipcode=$scope.$item.id;
			}
			if($label=='destzipcodefrom'){
				
				$scope.ratelane.DestinationZipcodeFrom=$scope.$item.id;
			}
			if($label=='destzipcodeto'){
				
				$scope.ratelane.DestinationZipcodeTo=$scope.$item.id;
			}
			
			//console.log($scope.ratelane);
		};

		
		
/***************************************************************************/
/*
	$scope.loadRatelanes=function(params){
		
		
		var data={ProfileId:$scope.ratelane.ProfileId,carrierId:$scope.ratelane.cId,limit:params.limit,page:params.page,orderBy:params.orderBy,orderType:params.orderType,filters:params.filters};
		var httpRequest = $http({
						method: 'POST',
						
						url: '/front/profiles/ratelane-list',
						data:data
				
					}).success(function(data, status) {
						$scope.laneratelist = data;
						
						
						angular.element('.orgloading').addClass('hidden');
						
						
						
						
					});
	
	};*/
/***************************************************************************/	
	//sort lanerate column
	/*	$scope.lanesortColumn = function(predicate) {
			   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
			   $scope.predicate = predicate;
			   //alert("A");
			   $scope.loadRatelanes({limit: $scope.lanerecordLimit, page: $scope.lanecurrentPage, 
					orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
				});
			};
	
		//lanerate pagination
		$scope.lanepageChanged = function(){
		
				$scope.loadRatelanes({limit: $scope.lanerecordLimit, page: $scope.lanecurrentPage});
			}

		$scope.$watch('lanerecordLimit', function(value, oldValue){
				if(oldValue != value){
					$scope.loadRatelanes({ limit: $scope.lanerecordLimit, 	page: $scope.lanecurrentPage});
				}
			});
		*/	
			
/***************************************************************************/	
	$scope.submitRatelane=function(form) {
		//console.log($scope.ratelane);
				//return;
				if($scope.profile_id=='' || $scope.ratelane.ProfileId=='undefined' || $scope.ratelane.carrierId=='undefined'){
					alert("Please select a profile and carrier ");
					return false;
				}
				
				//clear previous origin selection on update
				if($scope.ratelane.OriginalLocation.id=='8' || $scope.ratelane.OriginalLocation.id=='1'){
							
							if(typeof $scope.ratelane.OriginalZipcode!='undefined')$scope.ratelane.OriginalZipcode=[];
							if(typeof $scope.ratelane.OriginalZipcodeFrom!='undefined')$scope.ratelane.OriginalZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalZipcodeTo!='undefined') $scope.ratelane.OriginalZipcodeTo=[];
							if(typeof $scope.ratelane.OriginalTripleZipcode!='undefined') $scope.ratelane.OriginalTripleZipcode=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeFrom!='undefined')$scope.ratelane.OriginalTripleZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeTo!='undefined') $scope.ratelane.OriginalTripleZipcodeTo=[];
					}
				else if($scope.ratelane.OriginalLocation.id=='6'){
							if( typeof $scope.ratelane.OriginalCity!='undefined')$scope.ratelane.OriginalCity=[];
							if(typeof $scope.ratelane.OriginalState!='undefined')$scope.ratelane.OriginalState=[];
							
							if(typeof $scope.ratelane.OriginalZipcodeFrom!='undefined')$scope.ratelane.OriginalZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalZipcodeTo!='undefined') $scope.ratelane.OriginalZipcodeTo=[];
							if(typeof $scope.ratelane.OriginalTripleZipcode!='undefined') $scope.ratelane.OriginalTripleZipcode=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeFrom!='undefined')$scope.ratelane.OriginalTripleZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeTo!='undefined') $scope.ratelane.OriginalTripleZipcodeTo=[];
							
				}
				else if($scope.ratelane.OriginalLocation.id=='3'){
							if( typeof $scope.ratelane.OriginalCity!='undefined')$scope.ratelane.OriginalCity=[];
							if(typeof $scope.ratelane.OriginalState!='undefined')$scope.ratelane.OriginalState=[];
							if(typeof $scope.ratelane.OriginalZipcode!='undefined')$scope.ratelane.OriginalZipcode=[];
							if(typeof $scope.ratelane.OriginalTripleZipcode!='undefined') $scope.ratelane.OriginalTripleZipcode=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeFrom!='undefined')$scope.ratelane.OriginalTripleZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeTo!='undefined') $scope.ratelane.OriginalTripleZipcodeTo=[];
							
				}
				
				else if($scope.ratelane.OriginalLocation.id=='4'){
							if( typeof $scope.ratelane.OriginalCity!='undefined')$scope.ratelane.OriginalCity=[];
							if(typeof $scope.ratelane.OriginalState!='undefined')$scope.ratelane.OriginalState=[];
							if(typeof $scope.ratelane.OriginalZipcode!='undefined')$scope.ratelane.OriginalZipcode=[];
							if(typeof $scope.ratelane.OriginalZipcodeFrom!='undefined')$scope.ratelane.OriginalZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalZipcodeTo!='undefined') $scope.ratelane.OriginalZipcodeTo=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeFrom!='undefined')$scope.ratelane.OriginalTripleZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalTripleZipcodeTo!='undefined') $scope.ratelane.OriginalTripleZipcodeTo=[];
							
				}
				else if($scope.ratelane.OriginalLocation.id=='5'){
							if( typeof $scope.ratelane.OriginalCity!='undefined')$scope.ratelane.OriginalCity=[];
							if(typeof $scope.ratelane.OriginalState!='undefined')$scope.ratelane.OriginalState=[];
							if(typeof $scope.ratelane.OriginalZipcode!='undefined')$scope.ratelane.OriginalZipcode=[];
							if(typeof $scope.ratelane.OriginalZipcodeFrom!='undefined')$scope.ratelane.OriginalZipcodeFrom=[];
							if(typeof $scope.ratelane.OriginalZipcodeTo!='undefined') $scope.ratelane.OriginalZipcodeTo=[];
							if(typeof $scope.ratelane.OriginalTripleZipcode!='undefined') $scope.ratelane.OriginalTripleZipcode=[];
							
				}
				
				
				
				//clear previous destination selection on update
				if($scope.ratelane.DestinationLocation.id=='8' || $scope.ratelane.DestinationLocation.id=='1'){
							
							if(typeof $scope.ratelane.DestinationZipcode!='undefined')$scope.ratelane.DestinationZipcode=[];
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined')$scope.ratelane.DestinationZipcodeTo=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							if(typeof $scope.ratelane.DestinationTripleZipcode!='undefined') $scope.ratelane.DestinationTripleZipcode=[];
					}
				else if($scope.ratelane.DestinationLocation.id=='6'){
							if( typeof $scope.ratelane.DestinationCity!='undefined')$scope.ratelane.DestinationCity=[];
							if(typeof $scope.ratelane.DestinationState!='undefined')$scope.ratelane.DestinationState=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							if(typeof $scope.ratelane.DestinationTripleZipcode!='undefined') $scope.ratelane.DestinationTripleZipcode=[];
							
				}
				else if($scope.ratelane.DestinationLocation.id=='3'){
							if( typeof $scope.ratelane.DestinationCity!='undefined')$scope.ratelane.DestinationCity=[];
							if(typeof $scope.ratelane.DestinationState!='undefined')$scope.ratelane.DestinationState=[];
							if(typeof $scope.ratelane.DestinationZipcode!='undefined')$scope.ratelane.DestinationZipcode=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							if(typeof $scope.ratelane.DestinationTripleZipcode!='undefined') $scope.ratelane.DestinationTripleZipcode=[];
							
				}
				else if($scope.ratelane.DestinationLocation.id=='4'){
							if( typeof $scope.ratelane.DestinationCity!='undefined')$scope.ratelane.DestinationCity=[];
							if(typeof $scope.ratelane.DestinationState!='undefined')$scope.ratelane.DestinationState=[];
							if(typeof $scope.ratelane.DestinationZipcode!='undefined')$scope.ratelane.DestinationZipcode=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							
							if(typeof $scope.ratelane.DestinationTripleZipcodeFrom!='undefined')$scope.ratelane.DestinationTripleZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationTripleZipcodeTo!='undefined') $scope.ratelane.DestinationTripleZipcodeTo=[];
							
				}
				else if($scope.ratelane.DestinationLocation.id=='5'){
							if( typeof $scope.ratelane.DestinationCity!='undefined')$scope.ratelane.DestinationCity=[];
							if(typeof $scope.ratelane.DestinationState!='undefined')$scope.ratelane.DestinationState=[];
							if(typeof $scope.ratelane.DestinationZipcode!='undefined')$scope.ratelane.DestinationZipcode=[];
							
							if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom=[];
							if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined') $scope.ratelane.DestinationZipcodeTo=[];
							if(typeof $scope.ratelane.DestinationTripleZipcode!='undefined') $scope.ratelane.DestinationTripleZipcode=[];
							
				}
				
				
				
				angular.element('.loading').removeClass('hidden');
				
				  $http({
				    method  : 'POST',
				    url     : '/front/profiles/save-lane',
				    data    : $scope.ratelane, //forms user object
				  
				   })
				    .success(function(data) {
						$scope.resetlane();
				      if (data.errors) {
					
					$scope.cmessage = data.cmessage;
				      } else {
					
					$scope.cmessage = data.cmessage;
					jQuery('#ratemoreDetails').modal('hide');
					//$scope.loadRatelanes({ limit: $scope.lanerecordLimit, 	page: $scope.lanecurrentPage});
					$scope.lanes=[];
					$scope.pagination={current_page:1};
					$scope.loadlanes($scope.lanecurrentPage ? parseInt($scope.lanecurrentPage, $scope.lanerecordLimit) : 1);
				      }
				      angular.element('.loading').addClass('hidden');
				    });
				    
		
	};
/************************************************************************/

//load a lanerate for edit
		$scope.editRatelane = function(cid){
		
				var httpRequest = $http({
						method: 'POST',
						data:{cid:cid},
						url: '/front/profiles/lanerate-ajaxbyid'
						
				
					}).success(function(data, status) {
						$scope.ratelane = data.lanerate;
						//console.log($scope.ratelane.OriginalState.length);
						//jQuery("#orgstate").multiselect('dataprovider', $scope.ratelane.OriginalState);
						//jQuery("#orgcity").multiselect('dataprovider', $scope.ratelane.OriginalCity);
						//jQuery("#orgzip").multiselect('dataprovider', $scope.ratelane.OriginalZipcode);
						
						
						if($scope.ratelane.OriginalState && $scope.ratelane.OriginalState.length>0){
						for(var i=1;i<$scope.ratelane.OriginalState.length;i++){
							$scope.addNewChoice();
						}
					
						}
						if($scope.ratelane.OriginalZipcode && $scope.ratelane.OriginalZipcode.length>0){
						for(var i=1;i<$scope.ratelane.OriginalZipcode.length;i++){
							$scope.addNewChoice();
						}
						}
						if($scope.ratelane.OriginalZipcodeFrom && $scope.ratelane.OriginalZipcodeFrom.length>0){
						for(var i=1;i<$scope.ratelane.OriginalZipcodeFrom.length;i++){
							$scope.addNewChoice();
						}
						}
						
						
					
					
						
						
						if($scope.ratelane.DestinationState && $scope.ratelane.DestinationState.length>0){
						for(i=1;i<$scope.ratelane.DestinationState.length;i++){
							$scope.addNewChoice2();
						}
						}
						if($scope.ratelane.DestinationZipcode && $scope.ratelane.DestinationZipcode.length>0){
						for(var i=1;i<$scope.ratelane.DestinationZipcode.length;i++){
							$scope.addNewChoice2();
						}
						}
						if($scope.ratelane.DestinationZipcodeFrom && $scope.ratelane.DestinationZipcodeFrom.length>0){
						for(var i=1;i<$scope.ratelane.DestinationZipcodeFrom.length;i++){
							$scope.addNewChoice2();
						}
						}
						
						
						
						if(typeof $scope.ratelane.DistanceRate!='undefined' && $scope.ratelane.DistanceRate.Rate.length>0){
							
						for(var i=1;i<$scope.ratelane.DistanceRate.Rate.length;i++){
							$scope.addNewDistance();
						}
						}
						
						
						if(typeof $scope.ratelane.CPU!='undefined' && $scope.ratelane.CPU.MaxWeight.length>0){
							
						for(var i=1;i<$scope.ratelane.CPU.MaxWeight.length;i++){
							$scope.addNewCostperUnit();
						}
						}
						
						
						angular.element('.loading').addClass('hidden');
						
						
					});
			}
		
			
/***************************************************************************/	
	//delete a selected contract 
		  $scope.deleteRatelane=function(id){
						if($window.confirm('Are you sure ?')){
							var httpRequest = $http({
								method: 'POST',
								data:{cid:id},
								url: '/front/profiles/delete-ratelane'
								
						
							}).success(function(data, status) {
								//$scope.loadRatelanes({ limit: $scope.lanerecordLimit, 	page: $scope.lanecurrentPage});
								$scope.lanes=[];
								$scope.pagination={current_page:1};
								$scope.loadlanes($scope.lanecurrentPage ? parseInt($scope.lanecurrentPage, $scope.lanerecordLimit) : 1);
							});
						};
				
			  }; 
/************************************************************************/
	//this section is for grid in add ratelane
	$scope.addNewChoice = function() {
		if(typeof $scope.ratelane.OriginalLocation==='undefined') return false;
		
		if(typeof $scope.ratelane.OriginalLocation.name==='undefined') return false;
		var newItemNo = $scope.choices.length+1;
		$scope.choices.push({'id':'choice'+newItemNo});
	  };
	$scope.resetorgloc=function(){
		 $scope.choices = [{id: 'choice1'}];
	} 
	$scope.resetdestloc=function(){
		 $scope.choices2 = [{id: 'schoice1'}];
	} 
	  
	  
	 $scope.addNewChoice2 = function() {
		 if(typeof $scope.ratelane.DestinationLocation==='undefined') return false;
		 if(typeof $scope.ratelane.DestinationLocation.name==='undefined') return false;
		 
		var newItemNo = $scope.choices2.length+1;
		$scope.choices2.push({'id':'schoice'+newItemNo});
	  };
	  
	  
	  
	  
	  $scope.addNewDistance = function() {
		 
		var newItemNo = $scope.multidistance.length+1;
		$scope.multidistance.push({'id':'dchoice'+newItemNo});
	  };
	  
	  $scope.addNewCostperUnit = function() {
		 
		var newItemNo = $scope.multicostperunit.length+1;
		$scope.multicostperunit.push({'id':'cpuchoice'+newItemNo});
	  };
		
		
		
		
	  $scope.removeChoice = function() {
		var lastItem = $scope.choices.length-1;
		$scope.choices.splice(lastItem);
		
		if( typeof $scope.ratelane.OriginalCity!='undefined')$scope.ratelane.OriginalCity.splice(lastItem);
		if(typeof $scope.ratelane.OriginalState!='undefined')$scope.ratelane.OriginalState.splice(lastItem);
		if(typeof $scope.ratelane.OriginalZipcode!='undefined')$scope.ratelane.OriginalZipcode.splice(lastItem);
		if(typeof $scope.ratelane.OriginalZipcodeFrom!='undefined')$scope.ratelane.OriginalZipcodeFrom.splice(lastItem);
		if(typeof $scope.ratelane.OriginalZipcodeTo!='undefined')$scope.ratelane.OriginalZipcodeTo.splice(lastItem);
		
		
	  };
	  
	  
	  $scope.removeChoice2 = function() {
		var lastItem = $scope.choices2.length-1;
		$scope.choices2.splice(lastItem);
		
		if(typeof $scope.ratelane.DestinationCity!='undefined')$scope.ratelane.DestinationCity.splice(lastItem);
		if(typeof $scope.ratelane.DestinationState!='undefined')$scope.ratelane.DestinationState.splice(lastItem);
		if(typeof $scope.ratelane.DestinationZipcode!='undefined')$scope.ratelane.DestinationZipcode.splice(lastItem);
		if(typeof $scope.ratelane.DestinationZipcodeFrom!='undefined')$scope.ratelane.DestinationZipcodeFrom.splice(lastItem);
		if(typeof $scope.ratelane.DestinationZipcodeTo!='undefined')$scope.ratelane.DestinationZipcodeTo.splice(lastItem);
		
		
	  };
	  
	  $scope.removeDistance = function() {
		var lastItem = $scope.multidistance.length-1;
		$scope.multidistance.splice(lastItem);
		if(typeof $scope.ratelane.DistanceRate==='undefined') return;
		if($scope.ratelane.DistanceRate.Rate.length>0)$scope.ratelane.DistanceRate.Rate.splice(lastItem);
		if($scope.ratelane.DistanceRate.OutDisc.length>0)$scope.ratelane.DistanceRate.OutDisc.splice(lastItem);
		if($scope.ratelane.DistanceRate.MinCharge.length>0)$scope.ratelane.DistanceRate.MinCharge.splice(lastItem);
		if($scope.ratelane.DistanceRate.Stop.length>0)$scope.ratelane.DistanceRate.Stop.splice(lastItem);
		
		
	  };	
	  
	  
	  $scope.removeCostperUnit = function() {
		var lastItem = $scope.multicostperunit.length-1;
		$scope.multicostperunit.splice(lastItem);
		if(typeof $scope.ratelane.CPU=='undefined') return;
		
		if($scope.ratelane.CPU.UnitType.length>0)$scope.ratelane.CPU.UnitType.splice(lastItem);
		if($scope.ratelane.CPU.MaxWeight.length>0)$scope.ratelane.CPU.MaxWeight.splice(lastItem);
		if($scope.ratelane.CPU.MinWeight.length>0)$scope.ratelane.CPU.MinWeight.splice(lastItem);
		if($scope.ratelane.CPU.MaxHeight.length>0)$scope.ratelane.CPU.MaxHeight.splice(lastItem);
		if($scope.ratelane.CPU.MaxWidth.length>0)$scope.ratelane.CPU.MaxWidth.splice(lastItem);
		if($scope.ratelane.CPU.FromUnit.length>0)$scope.ratelane.CPU.FromUnit.splice(lastItem);
		if($scope.ratelane.CPU.ToUnit.length>0)$scope.ratelane.CPU.ToUnit.splice(lastItem);
		if($scope.ratelane.CPU.Rate.length>0)$scope.ratelane.CPU.Rate.splice(lastItem);
		if($scope.ratelane.CPU.MinCharge.length>0)$scope.ratelane.CPU.MinCharge.splice(lastItem);
		
	  };		  
			  
/***************************************************************************/
		//load carrier panel at right side
		$scope.loadCarriersection = function(profile_id,_id) {
			
			jQuery(".mainprofile").toggleClass("profilemenu");
			
			$scope.rightpanel='profile';
			
			$scope.loadContract({profileid:profile_id, limit: $scope.recordLimit,page: 1});
			$scope.editProfile(_id);
			
		    };
		
		
		
		
		
		//load options for addlane form
		 $scope.loadoptions = function() {
				$http({
				    method  : 'POST',
				    url     : '/front/profiles/country',
				  
				   })
				    .success(function(data) {
					$scope.WeightTypeOptions=data.WeightTypeOptions;
				     $scope.country=data.country;
				     $scope.EQuipTypeOptions=data.EqpType;
					 $scope.LocTypeOptions=data.Loc;
					 $scope.DistanceTypeOptions=data.DistanceType;
					 $scope.UnitTypeOptions=data.UnitType;
					 $scope.mLocTypeOptions=$scope.LocTypeOptions;
					 var i=0;
						$scope.mLocTypeOptions.forEach(function(item) {
							$scope.mLocTypeOptions[i].label=item.name+"  ("+item.name.replace(/[^a-zA-Z0-9]/g, '')+")";
							
							$scope.mLocTypeOptions[i].orggrid="Origin"+item.name.replace(/[^a-zA-Z0-9]/g, '');
							$scope.mLocTypeOptions[i].destgrid="Destination"+item.name.replace(/[^a-zA-Z0-9]/g, '');
							
							
							i++;
						});
				
				
				    });
			
			
		    };
		$timeout($scope.loadoptions(),5000);    			  
		 //load default   profile at page startup...this is first call
		$scope.loadProfile({filters: $scope.filters});
		
		
		
/************************************ infinite scroll *********************************************/		



$scope.loadlanes = function(page) {

if($scope.rightpanel!='profile') return;

$scope.lfilters=[{Statecode:$scope.Statename,Citycode:$scope.Citycode}];
var fields='carrierId,cId,ProfileId,OriginalCountry,OriginalState,OrgCity,OriginalLocation,DestinationCountry,DestinationLocation,OriginalCity,DestinationCity,DestinationState,OriginalZipcode,DestinationZipcode,FromWeightRange,ToWeightRange,MoveType,CPU,Rateware,DistanceRate,RateMatrix,CostPerHundredRate';
      var params     = { page: page,limit: $scope.lanerecordLimit,ProfileId:$scope.ratelane.ProfileId,carrierId:$scope.ratelane.cId,filters:$scope.lfilters,fields:fields},
          isTerminal = $scope.pagination &&
                       $scope.pagination.current_page >= $scope.pagination.total_pages &&
                       $scope.pagination.current_page <= 1;
    isTerminal=$scope.pagination.current_page >= $scope.pagination.total_pages
   // console.log($scope.pagination.total_pages);
      // Determine if there is a need to load a new page
      if (!isTerminal) {
        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/ratelane-list', params)
          .success(function(data, status, headers) {
			
			 $scope.pagination.total_pages=data.count; 
            //console.log(data.count);
            
            $scope.lanes = $scope.lanes;
			if(typeof $scope.lanes!='undefined'){
           
            $scope.lanes.push.apply($scope.lanes, data.ratelist);
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
		if($scope.rightpanel!='profile') return;
     
      var page = $scope.pagination ? $scope.pagination.current_page + 1 : 1;
		$scope.pagination.current_page=page;
     
      $scope.loadlanes(page);
    });
    
    
    
/*********************************** state auto complete************************************************/    
    
$scope.onStateSelect = function ($item) {
			$scope.$item = $item;
			
			$scope.Statecode=$scope.$item.id;
			$scope.Statename=$scope.$item.name;
			
			$scope.lanes=[];
					$scope.pagination={current_page:1};
					//$scope.lanefilters = [{State:value}];
					//$scope.lanefilters.push.apply($scope.lanefilters, [{State:$scope.$item.id}]);
					$scope.loadlanes($scope.lanecurrentPage ? parseInt($scope.lanecurrentPage, $scope.lanerecordLimit) : 1);
					
			
		}
/*********************************** city auto complete************************************************/  
$scope.onCitySelect = function ($item) {
			$scope.$item = $item;
			
			$scope.Citycode=$scope.$item.id;
			
			$scope.lanes=[];
					$scope.pagination={current_page:1};
					//$scope.lanefilters = [{City:value}];
					//$scope.lanefilters.push.apply($scope.lanefilters, [{City:$scope.$item.id}]);
					$scope.loadlanes($scope.lanecurrentPage ? parseInt($scope.lanecurrentPage, $scope.lanerecordLimit) : 1);
			
		}
		
		
/**************************** file upload *************************************/

$scope.upload = function (files,cid) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/services/upload',
                    fields: {
                        'cid': cid
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                
                }).success(function (data, status, headers, config) {
					
                	if(data.error){
                		$scope.error.status = true;
						$scope.error.message = data.error;
                	}else if(data.success){
                		$scope.log = 'file ' + config.file.name + ' uploaded successfully!'

                		//updating the image after successfull upload
                		//$scope.site.image = data.url ? data.url : $scope.site.image;
                		$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1});
                		
                	}
                  
                   //$scope.$apply();
                });
            }
        }
    };
  $scope.viewFile=function(cid){
	  
	  $http({
				    method  : 'POST',
				    data:{cid:cid},
				    url     : '/front/services/fileurl',
				    
				  
				   })
				    .success(function(data) {
						window.location.href=data.fileurl;
					});
	  
	  }  
	  
  $scope.deleteFile=function(cid){
	  if($window.confirm('Are you sure ?')){
	  $http({
				    method  : 'POST',
				    data:{cid:cid},
				    url     : '/front/services/delfile',
				    
				  
				   })
				    .success(function(data) {
						$scope.loadContract({profileid:$scope.profile_id, limit: $scope.recordLimit,page: 1});
					});
	  
	  }  
  }
  
  
  $scope.checkAll = function(){
		
			if($scope.filter.selectAll === true)
				{$scope.filter.month1 = true;
				$scope.filter.month2= true;
				$scope.filter.month3= true;
			}
			else{
				$scope.filter.month1= false;
				$scope.filter.month2 = false;
				$scope.filter.month3 = false;
			}
		
		
	}
	
/***************************************** Load right panels ****************************************************************/
	
//load fuel panel at right side
		$scope.loadFuelsection = function(profilecode,_id) {
	
			$scope.rightpanel='Fuel';
			
			var httpRequest = $http({
								method: 'POST',
								data:{q:'CALCTYPE'},
								url: '/front/services/code-type'
								
						
							}).success(function(data, status) {
								$scope.calctype=data;
							});
							
		
          var httpRequest = $http({
								method: 'POST',
								
								url: '/front/services/fuel-type'
								
						
							}).success(function(data, status) {
								$scope.fueltype=data;
							});
							
          
			$scope.fuel = [];
		  $scope.fuelPagination={current_page:1};
		  $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		  
		  
		  $scope.SCACfuel = [];
		  $scope.SCACfuelPagination={current_page:1};
		  $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: 1});
		  
			
		    };
		    
		//load accessorial panel at right side
		 $scope.loadAccessorialsection = function(profile_id,_id) {
			 
			$scope.editProfile(_id); 
			dataService.dataObj={profile_id:profile_id};
			//$scope.loadContract({profileid:profile_id, limit: $scope.recordLimit,page: 1});
			$scope.rightpanel='Accessorial';
			//$scope.$broadcast ('loadAssocevt');
			
			$scope.assocarial = [];
			$scope.SCACassocarial=[];  
			$scope.assocPagination={current_page:1};
			$scope.currentPagescac = 1;
			
			
			
			var httpRequest = $http({
								method: 'POST',
								data:{q:'CALCTYPE'},
								url: '/front/services/code-type'
								
						
							}).success(function(data, status) {
								$scope.calctype=data;
							});
							
			$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
			$scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: 1});
			
		    };   
		//load markup at right side panel
		  $scope.loadMarkupsection = function(profilecode,_id) {
			
			$scope.rightpanel='Markup';
			
			$scope.chargeMethod();
			$scope.loadNewMarkup();
			$scope.profileCarrier();
			
		    }; 
		    
		    
		    $scope.loadCarrier_Api=function (){
				
				$scope.rightpanel='Carrier_Api';
				
				
			}
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/********************************************************* Assocarial Tab ********************************************************************************/






















$scope.assocarial=[{id: 'assoc1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:''}];
	$scope.SCACassocarial= [{id: 'SCACassoc1',accessorial:null,rate_calculation:null,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,cid:null,scac:null,carrier:null}];
	
	
	
	$scope.assocRecordLimit = 2;
	$scope.assocCurrentPage = 1;
	$scope.currentPagescac = 1;
	$scope.calctype = ['FLAT','PERCENT','MULTIPLY'];
	$scope.SCACassocPagination={current_page:1};
	$scope.assocPagination={current_page:1};
	$scope.currentTab='assoc';
	$scope.assocfilter=[];
	$scope.assocsearchfilter=[];
	$scope.assocfilter.searchAssoc='';
	$scope.assoccode=''
	$scope.accessorial_code='';
	




	$scope.saveAssoc = function(params) {
	params.accessorial_code=$scope.assoccode;
	params.accessorial=$scope.assocname
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
					 $scope.assocarial = [];
					 $scope.assocPagination={current_page:1};
				      $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				    });
	
	};
	
/*************************************************Delete Assoc**************************************************/	
	$scope.deleteAssoc = function(params,index) {
	
	var lastItem = index;
	//console.log($scope.assocarial);
	

	$scope.assocarial.splice(lastItem);
	
	// $scope.assocarial.push.apply($scope.assocarial);
	 //console.log($scope.assocarial);
						if( typeof $scope.assocarial.accessorial!='undefined')$scope.assocarial.accessorial.splice(lastItem);
						if( typeof $scope.assocarial.rate_calculation!='undefined')$scope.assocarial.rate_calculation.splice(lastItem);
						if( typeof $scope.assocarial.min_rate!='undefined')$scope.assocarial.min_rate.splice(lastItem);
						if( typeof $scope.assocarial.max_rate!='undefined')$scope.assocarial.max_rate.splice(lastItem);
						if( typeof $scope.assocarial.rate_percentage!='undefined')$scope.assocarial.rate_percentage.splice(lastItem);
						if( typeof $scope.assocarial.effective_from!='undefined')$scope.assocarial.effective_from.splice(lastItem);
						if( typeof $scope.assocarial.effective_to!='undefined')$scope.assocarial.effective_to.splice(lastItem);
						if( typeof $scope.assocarial.cid!='undefined')$scope.assocarial.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
		$scope.assocPagination={current_page:1};
		$scope.assocarial = [];
		$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
		
		return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						$scope.assocPagination={current_page:1};
						$scope.assocarial = [];
				      $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewAssoc = function() {
	
	
		var newItemNo = $scope.assocarial.length+1;
		$scope.assocarial.push({'id':'assoc'+newItemNo});
		
	  };



/************************* Reset Row ********************************/


	  $scope.resetRow=function(){
		  $scope.assocarial = [];
		  $scope.assocPagination={current_page:1};
		  $scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
		  $scope.assocfilter=[];
		  
		  $scope.SCACassocarial = [];
		  $scope.SCACassocPagination={current_page:1};
		  $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: 1});
		  
	  }
	   //load assoc autocomplete
	    $scope.getassoc = function(textval){
				$scope.assoctype = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/assoc-type'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.assoctype.push(item);
						});

					});
					
					
			  };

	$scope.assocAPI = function(userInputString, timeoutPromise) {
		return $http.post('/front/services/assoc-type', {q: userInputString}, {timeout: timeoutPromise});
	}
			  
	$scope.onSelectautocomplete = function (selected) {
						
		
				  if (selected) {
					$scope.assoccode = selected.originalObject.id;
					$scope.assocname = selected.originalObject.name;
				  } else {
					$scope.assoccode = null;
				  }
				 // console.log($scope.assoccode);
				
		};
		
$scope.carrierAPI = function(userInputString, timeoutPromise) {
		return $http.post('/front/services/carrier-api', {q: userInputString}, {timeout: timeoutPromise});
	}
			
$scope.onCarrierselect= function (selected) {
						
		
				  if (selected) {
					$scope.scac = selected.originalObject.id;
					$scope.scacname = selected.originalObject.name;
				  } else {
					$scope.scac = null;
				  }
				
			
		};
		
	$scope.onSelectAssoc = function ($item, $model, $label) {
			
			$scope.$item = $item;
			$scope.assocfilter.searchAssoc=$scope.$item.id
			$scope.assocPagination.current_page=1
			$scope.assocarial = [];
			//$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
			
			//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
			$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
			
		};
		
		
		
		
/************************************ infinite Accessorial scroll *********************************************/	
	
$scope.loadAssoc = function(params) {
	if($scope.rightpanel!='Accessorial') return;
if($scope.currentTab!='assoc') return

$scope.assocPagination.current_page=params.page;

var afilter=[{EffectiveFrom: $scope.assocfilter.EffectiveFrom,EffectiveTo:$scope.assocfilter.EffectiveTo,searchAssoc:$scope.assocfilter.searchAssoc}];


      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.assocPagination.current_page > $scope.assocPagination.total_pages;
 
      if (!isTerminal) {
		  
		  

        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-assocarial', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.assocarial =$scope.assocarial;
            
            
			if(typeof $scope.assocarial!='undefined'){
				$scope.assocPagination.total_pages=data.assoc_count;
				$scope.assocarial.push.apply($scope.assocarial, data.Accessorial);
				
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.currentTab!='assoc') return
     
      var page = $scope.assocPagination ? $scope.assocPagination.current_page + 1 : 1;
		$scope.assocPagination.current_page=page;
     
      $scope.loadAssoc({page:page,limit:$scope.assocRecordLimit});
    });
    
    $scope.resetAccRow=function(date){
		
		$scope.assocPagination.current_page=1
			$scope.assocarial = [];
			$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
		}		
	
/**********************************************Filters*****************************************************/			
		
	$scope.$watch('assocfilter.EffectiveFrom', function(value, oldValue){
				
				if(value != oldValue){
				
				//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
				//$scope.resetRow();
				//console.log(value);
				//dataService.dataObj.EffectiveFrom=value;
				//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
				$scope.assocPagination.current_page=1
				$scope.assocarial = [];
				$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				      
				}
			});	
				
	
	$scope.$watch('assocfilter.EffectiveTo', function(value, oldValue){
				
				if(value != oldValue){
					
				
					//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
					//$scope.resetRow();
					//console.log(value);
					//dataService.dataObj.EffectiveTo=value;
					//$scope.loadAccessorialsection(dataService.dataObj.profile_id);
					$scope.assocPagination.current_page=1
					$scope.assocarial = [];
					$scope.loadAssoc({limit: $scope.assocRecordLimit,page: 1});
				}
			});	
		
		
		
		
		
		
		
		
		
		
		
/*************************************** SCAC Part ************************************/


	
	$scope.saveSCACAssoc = function(params) {
	if($scope.assoccode){
	params.accessorial_code=$scope.assoccode;
	params.accessorial=$scope.assocname
	}
	if($scope.scac){
		params.scac=$scope.scac;
		params.carrier=$scope.scacname;
	}
	
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
						$scope.SCACassocPagination={current_page:1};
						$scope.SCACassocarial = [];
				      $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
				    });
	
	};
	
/*******************************************DELETE SCAC********************************************************/
	$scope.deleteSCACAssoc = function(params,index) {
	
	var lastItem = index;
						$scope.SCACassocarial.splice(lastItem);
	
				if( typeof $scope.SCACassocarial.accessorial!='undefined')$scope.SCACassocarial.accessorial.splice(lastItem);
				if( typeof $scope.SCACassocarial.rate_calculation!='undefined')$scope.SCACassocarial.rate_calculation.splice(lastItem);
				if( typeof $scope.SCACassocarial.min_rate!='undefined')$scope.SCACassocarial.min_rate.splice(lastItem);
				if( typeof $scope.SCACassocarial.max_rate!='undefined')$scope.SCACassocarial.max_rate.splice(lastItem);
				if( typeof $scope.SCACassocarial.rate_percentage!='undefined')$scope.SCACassocarial.rate_percentage.splice(lastItem);
				if( typeof $scope.SCACassocarial.effective_from!='undefined')$scope.SCACassocarial.effective_from.splice(lastItem);
				if( typeof $scope.SCACassocarial.effective_to!='undefined')$scope.SCACassocarial.effective_to.splice(lastItem);
				if( typeof $scope.SCACassocarial.cid!='undefined')$scope.SCACassocarial.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
			$scope.SCACassocPagination={current_page:1};
			$scope.SCACassocarial = [];
			$scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
			return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-assoc',
				    data    : {Accessorial:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						
						$scope.SCACassocPagination={current_page:1};
						$scope.SCACassocarial = [];
				      $scope.loadSCACAssoc({limit: $scope.assocRecordLimit,page: $scope.currentPagescac});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewSCACAssoc = function() {
	
		var newItemNo = $scope.SCACassocarial.length+1;
		$scope.SCACassocarial.push({'id':'SCACassoc'+newItemNo});
		
	  };
	  
	  
	  
	  
/*****************************************Load SCAC**********************************************************/	  
	  
	  $scope.loadSCACAssoc = function(params) {
	if($scope.rightpanel!='Accessorial') return;
	if($scope.currentTab!='scac') return
	
	$scope.SCACassocPagination.current_page=params.page;
	
	var afilter=[{EffectiveFrom: $scope.assocfilter.EffectiveFrom,EffectiveTo:$scope.assocfilter.EffectiveTo,searchAssoc:$scope.assocfilter.searchAssoc}];
	
      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.SCACassocPagination &&
                       $scope.SCACassocPagination.current_page >= $scope.SCACassocPagination.total_pages_scac &&
                       $scope.SCACassocPagination.current_page <= 1;
    isTerminal = $scope.SCACassocPagination.current_page > $scope.SCACassocPagination.total_pages_scac;
    
          
      // Determine if there is a need to load a new page
      if (!isTerminal) {
        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-assocarial', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.SCACassocarial =$scope.SCACassocarial;
			if(typeof $scope.SCACassocarial!='undefined'){
           $scope.SCACassocPagination.total_pages_scac=data.scac_count;
            $scope.SCACassocarial.push.apply($scope.SCACassocarial, data.SCAC_Assoc);
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
/***************************************************************************************************/			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.currentTab!='scac') return
    
      var page = $scope.SCACassocPagination ? $scope.SCACassocPagination.current_page + 1 : 1;
		$scope.SCACassocPagination.current_page=page;
     
      $scope.loadSCACAssoc({page:page,limit:$scope.assocRecordLimit});
    });
    
    
    
    
    $scope.setcurrentTab=function(curtab){
		
		//$scope.assocfilter.searchAssoc='';
		//$scope.assocfilter.EffectiveFrom='';
		//$scope.assocfilter.EffectiveTo='';
		$scope.currentTab=curtab;
		$scope.resetRow();
		
	}
	
	
	
	
	



















	
		
/*************************************************************Fuel Functionality*******************************************************************/
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

$scope.fuel=[{id: 'fuel1',min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,fuel_type:null,fuel_calculation:null,cid:null,scac:''}];
	$scope.SCACfuel= [{id: 'SCACfuel1',min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,fuel_type:null,fuel_calculation:null,cid:null,scac:''}];
	
	
	
	$scope.fuelRecordLimit = 2;
	$scope.fuelCurrentPage = 1;
	$scope.fuelcurrentPagescac = 1;
	$scope.calctype = ['FLAT','PERCENT','MULTIPLY'];
	$scope.SCACfuelPagination={current_page:1};
	$scope.fuelPagination={current_page:1};
	$scope.fuelcurrentTab='fuel';
	$scope.fuelfilter=[];
	$scope.fuelsearchfilter=[];
	$scope.fuelfilter.searchFuel='';
	$scope.fuelassoccode=''
	$scope.fueltype=[];
	
	$scope.saveFuel = function(params) {
	
	
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
					 $scope.fuel = [];
					 $scope.fuelPagination={current_page:1};
				      $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				    });
	
	};
	
/*************************************************Delete Fuel**************************************************/	
	$scope.deleteFuel = function(params,index) {
	
	var lastItem = index;
	//console.log($scope.fuel);
	

	$scope.fuel.splice(lastItem);
	
	// $scope.fuel.push.apply($scope.fuel);
	 //console.log($scope.fuel);
						//if( typeof $scope.fuel.accessorial!='undefined')$scope.fuel.accessorial.splice(lastItem);
						//if( typeof $scope.fuel.rate_calculation!='undefined')$scope.fuel.rate_calculation.splice(lastItem);
						if( typeof $scope.fuel.min_rate!='undefined')$scope.fuel.min_rate.splice(lastItem);
						if( typeof $scope.fuel.max_rate!='undefined')$scope.fuel.max_rate.splice(lastItem);
						if( typeof $scope.fuel.rate_percentage!='undefined')$scope.fuel.rate_percentage.splice(lastItem);
						if( typeof $scope.fuel.effective_from!='undefined')$scope.fuel.effective_from.splice(lastItem);
						if( typeof $scope.fuel.effective_to!='undefined')$scope.fuel.effective_to.splice(lastItem);
						if( typeof $scope.fuel.fuel_type!='undefined')$scope.fuel.fuel_type.splice(lastItem);
						if( typeof $scope.fuel.fuel_calculation!='undefined')$scope.fuel.fuel_calculation.splice(lastItem);
						if( typeof $scope.fuel.cid!='undefined')$scope.fuel.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
		$scope.fuelPagination={current_page:1};
		$scope.fuel = [];
		$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		
		return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						$scope.fuelPagination={current_page:1};
						$scope.fuel = [];
				      $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewFuel = function() {
	
	
		var newItemNo = $scope.fuel.length+1;
		$scope.fuel.push({'id':'fuel'+newItemNo});
		
	  };



/************************* Reset Row ********************************/


	  $scope.resetfuelRow=function(){
		  $scope.fuel = [];
		  $scope.fuelPagination={current_page:1};
		  $scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		  $scope.fuelfilter=[];
		  
		  $scope.SCACfuel = [];
		  $scope.SCACfuelPagination={current_page:1};
		  $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: 1});
		  
	  }
	   //load fuel autocomplete
	    $scope.getfuel = function(textval){
				$scope.fueltype = [];
				
				var httpRequest = $http({
						method: 'POST',
						data:{q:textval},
						url: '/front/services/assoc-type'
						
				
					}).success(function(data, status) {
						
						$.each(data, function(i, item) {
							
							$scope.fueltype.push(item);
						});

					});
					
					
			  };
			  
	/*$scope.onSelect = function ($item, $model, $label) {
			$scope.$item = $item;
			$scope.$model = $model;
			$scope.$label = $label;
			if($label=='fuel'){
				//jQuery("#fuelcode").val($scope.$item.id);
				$scope.fuelassoccode=$scope.$item.id
			}
			
			
		};*/
	$scope.onSelectFuel = function ($item, $model, $label) {
			
			$scope.$item = $item;
			$scope.fuelfilter.searchFuel=$scope.$item.id
			$scope.fuelPagination.current_page=1
			$scope.fuel = [];
			
			$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
			
		};
	
	
	
	
		
	$scope.fueltypeAPI = function(userInputString, timeoutPromise) {
	  return $http.post('/front/services/fuel-type', {q: userInputString}, {timeout: timeoutPromise});
	}	
	
	$scope.onSelectFueltype = function (selected) {
						
		
				  if (selected) {
					$scope.fueltype = selected.originalObject.name;
				  } else {
					$scope.fueltype = null;
				  }
			
				
		};
		
		
/************************************ infinite Fuel scroll *********************************************/	
	
$scope.loadFuel = function(params) {
	if($scope.rightpanel!='Fuel') return;
if($scope.fuelcurrentTab!='fuel') return

$scope.fuelPagination.current_page=params.page;

var afilter=[{EffectiveFrom: $scope.fuelfilter.EffectiveFrom,EffectiveTo:$scope.fuelfilter.EffectiveTo,searchFuel:$scope.fuelfilter.searchFuel}];


      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.fuelPagination.current_page > $scope.fuelPagination.total_pages;
 
      if (!isTerminal) {
		  
		  

        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-fuel', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.fuel =$scope.fuel;
            
            
			if(typeof $scope.fuel!='undefined'){
				$scope.fuelPagination.total_pages=data.fuel_count;
				$scope.fuel.push.apply($scope.fuel, data.Fuel);
				
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.fuelcurrentTab!='fuel') return
     
      var page = $scope.fuelPagination ? $scope.fuelPagination.current_page + 1 : 1;
		$scope.fuelPagination.current_page=page;
     
      $scope.loadFuel({page:page,limit:$scope.fuelRecordLimit});
    });
    
    $scope.resetAccRow=function(date){
		
		$scope.fuelPagination.current_page=1
			$scope.fuel = [];
			$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
		}		
	
/**********************************************Filters*****************************************************/			
		
	$scope.$watch('fuelfilter.EffectiveFrom', function(value, oldValue){
				
				if(value != oldValue){
				
				//$scope.loadFuelsection(dataService.dataObj.profile_id);
				//$scope.resetRow();
				//console.log(value);
				//dataService.dataObj.EffectiveFrom=value;
				//$scope.loadFuelsection(dataService.dataObj.profile_id);
				$scope.fuelPagination.current_page=1
				$scope.fuel = [];
				$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				      
				}
			});	
				
	
	$scope.$watch('fuelfilter.EffectiveTo', function(value, oldValue){
				
				if(value != oldValue){
					
				
					//$scope.loadFuelsection(dataService.dataObj.profile_id);
					//$scope.resetRow();
					//console.log(value);
					//dataService.dataObj.EffectiveTo=value;
					//$scope.loadFuelsection(dataService.dataObj.profile_id);
					$scope.fuelPagination.current_page=1
					$scope.fuel = [];
					$scope.loadFuel({limit: $scope.fuelRecordLimit,page: 1});
				}
			});	
		
		
		
		
		
		
		
		
		
		
		
/*************************************** SCAC Part ************************************/


	
	$scope.saveSCACFuel = function(params) {
	//params.accessorial_code=$scope.fuelassoccode;
	if($scope.scac){
		params.scac=$scope.scac;
		params.carrier=$scope.scacname;
	}
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id}, //forms user object
				  
				   })
				    .success(function(data) {
						$scope.SCACfuelPagination={current_page:1};
						$scope.SCACfuel = [];
				      $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
				    });
	
	};
	
/*******************************************DELETE SCAC********************************************************/
	$scope.deleteSCACFuel = function(params,index) {
	
	var lastItem = index;
						$scope.SCACfuel.splice(lastItem);
	
				//if( typeof $scope.SCACfuel.accessorial!='undefined')$scope.SCACfuel.accessorial.splice(lastItem);
				//if( typeof $scope.SCACfuel.rate_calculation!='undefined')$scope.SCACfuel.rate_calculation.splice(lastItem);
				if( typeof $scope.SCACfuel.min_rate!='undefined')$scope.SCACfuel.min_rate.splice(lastItem);
				if( typeof $scope.SCACfuel.max_rate!='undefined')$scope.SCACfuel.max_rate.splice(lastItem);
				if( typeof $scope.SCACfuel.rate_percentage!='undefined')$scope.SCACfuel.rate_percentage.splice(lastItem);
				if( typeof $scope.SCACfuel.effective_from!='undefined')$scope.SCACfuel.effective_from.splice(lastItem);
				if( typeof $scope.SCACfuel.effective_to!='undefined')$scope.SCACfuel.effective_to.splice(lastItem);
				
				if( typeof $scope.SCACfuel.fuel_type!='undefined')$scope.SCACfuel.fuel_type.splice(lastItem);
				if( typeof $scope.SCACfuel.fuel_calculation!='undefined')$scope.SCACfuel.fuel_calculation.splice(lastItem);
				if( typeof $scope.SCACfuel.cid!='undefined')$scope.SCACfuel.cid.splice(lastItem);
	
	if(typeof params.cid=='undefined')
		{
			$scope.SCACfuelPagination={current_page:1};
			$scope.SCACfuel = [];
			$scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
			return;
		}
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-fuel',
				    data    : {Fuel:params,cid:dataService.dataObj.profile_id},
				  
				   })
				    .success(function(data) {
						
						$scope.SCACfuelPagination={current_page:1};
						$scope.SCACfuel = [];
				      $scope.loadSCACFuel({limit: $scope.fuelRecordLimit,page: $scope.fuelcurrentPagescac});
				    });
	
	};
	
/***************************************************************************************************/	
	//this section is for grid in add ratelane
	$scope.addNewSCACFuel = function() {
	
		var newItemNo = $scope.SCACfuel.length+1;
		
		var newitem={id: 'SCACfuel'+newItemNo,min_rate:null,max_rate:null,rate_percentage:null,effective_from:null,effective_to:null,fuel_type:null,fuel_calculation:null,cid:null,scac:''};
		$scope.SCACfuel.push(newitem);
		
		
	  };
	  
	  
	  
	  
/*****************************************Load SCAC**********************************************************/	  
	  
	  $scope.loadSCACFuel = function(params) {
	if($scope.rightpanel!='Fuel') return;
	if($scope.fuelcurrentTab!='scac') return
	
	$scope.SCACfuelPagination.current_page=params.page;
	
	var afilter=[{EffectiveFrom: $scope.fuelfilter.EffectiveFrom,EffectiveTo:$scope.fuelfilter.EffectiveTo,searchFuel:$scope.fuelfilter.searchFuel}];
	
      var paramdata     ={profileid:dataService.dataObj.profile_id,limit:params.limit,page:params.page,filters:afilter},
          isTerminal = $scope.SCACfuelPagination &&
                       $scope.SCACfuelPagination.current_page >= $scope.SCACfuelPagination.total_pages_scac &&
                       $scope.SCACfuelPagination.current_page <= 1;
    isTerminal = $scope.SCACfuelPagination.current_page > $scope.SCACfuelPagination.total_pages_scac;
    
        
      // Determine if there is a need to load a new page
      if (!isTerminal) {
        // Flag loading as started
        $scope.loading = true;

        // Make an API request
        $http.post('/front/profiles/contract-fuel', paramdata)
          .success(function(data, status, headers) {
			
         
            $scope.SCACfuel =$scope.SCACfuel;
			if(typeof $scope.SCACfuel!='undefined'){
           $scope.SCACfuelPagination.total_pages_scac=data.scac_count;
            $scope.SCACfuel.push.apply($scope.SCACfuel, data.SCAC_Fuel);
			}
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
      }
      
    }

	
/***************************************************************************************************/			
    
    $scope.$on('endlessScroll:next', function() {
	if($scope.fuelcurrentTab!='scac') return
    
      var page = $scope.SCACfuelPagination ? $scope.SCACfuelPagination.current_page + 1 : 1;
		$scope.SCACfuelPagination.current_page=page;
     
      $scope.loadSCACFuel({page:page,limit:$scope.fuelRecordLimit});
    });
    
    
    
    
    $scope.setcurrentfuelTab=function(curtab){
		
		//$scope.fuelfilter.searchFuel='';
		//$scope.fuelfilter.EffectiveFrom='';
		//$scope.fuelfilter.EffectiveTo='';
		$scope.fuelcurrentTab=curtab;
		$scope.resetfuelRow();
		
		
		
		
		
	}
	
 /***********************************************************************************************************************************/
 
 $scope.localSearch=function(userInputString){
	 var data={q:userInputString};
	 $http.post('/front/services/country-list',data )
          .success(function(data, status, headers) {
			
         
           return data;
            
          })
          .finally(function() {
           
            $scope.loading = false;
          });
 } 
 
 
 
 
 
 
 
 
 
 /****************************************************************Markup *************************************************************/
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 $scope.markup=[];
 $scope.mprofile={};
 $scope.mprofile.TopLevelUpCharge={};
 
 $scope.markuplist=[];
 $scope.$watch('markupsearch', function(value, oldValue){
				
				if(value != oldValue){
			
				$scope.loadMarkup(value);
				      
				}
			});	

$scope.loadMarkup = function(params) {
	
	
			var httpRequest = $http({
			    method: 'POST',
			    url: '/front/profiles/markup-list',
			    data:{markup:params},
		
			}).success(function(data, status) {
			    $scope.markuplist = data.markuplist;
			 				
			});
		
		    };
		    
$scope.loadNewMarkup = function() {
	
	
			var httpRequest = $http({
			    method: 'POST',
			    url: '/front/profiles/newmarkup-list',
			   
		
			}).success(function(data, status) {
			    $scope.newmarkuplist = data.markuplist;
			 				
			});
		
		    };
		    
$scope.chargeMethod=function(){
	
	var httpRequest = $http({
								method: 'POST',
								data:{q:'CHARGETYPE'},
								url: '/front/services/code-type'
								
						
							}).success(function(data, status) {
								$scope.chargemethodtype=data;
							});
							
}


	$scope.addMarkup = function(params) {
	
	
	
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/save-markup',
				    data    : {Markup:params,profile:$scope.profile_id}, 
				  
				   })
				    .success(function(data) {
						jQuery('#markupDetails').modal('hide');
						
				      $scope.loadNewMarkup('');
				    });
	
	};
	
	$scope.deleteMarkup = function(params) {
	
	
	if($window.confirm('Are you sure ?')){
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/delete-markup',
				    data    : {_id:params,profile:$scope.profile_id}, 
				  
				   })
				    .success(function(data) {
						
				      $scope.loadMarkup('');
				      $scope.loadNewMarkup('');
				    });
				    
		}
	
	};
	
	$scope.fetchCarrier=function(markup,pos){
		
		if(markup=='no'){
			$scope.newmarkuplist[pos].UpchargeByCarrier=[];
			$scope.newmarkuplist[pos].setCarrier='no';
			return;
		}else{
			$scope.newmarkuplist[pos].setCarrier='yes';
		
		}
		return;
	};
	
	$scope.updateMarkup=function(markup){
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/update-markup',
				    data    : {Markup:markup,profile:$scope.profile_id}, 
				  
				   })
				    .success(function(data) {
						
				     $scope.loadMarkup('');
				    });
	};
	
	$scope.addToMapping=function(pos){
		
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/addprofile-markup',
				    data    : {ProfileId:$scope.markuplist[pos].ProfileId,_id:$scope.markuplist[pos]._id}, 
				  
				   })
				    .success(function(data) {
						$scope.markuplist[pos].MappedProfile=data;
				     
				    });
				    
	}
	
	$scope.addCarrier=function(carrier,pos){
		$scope.mcarrier={id:carrier.originalObject.id,CarrierName:carrier.originalObject.name};
		
		$scope.newmarkuplist[pos].UpchargeByCarrier.push($scope.mcarrier);
		//$rootScope.$broadcast('angucomplete-alt:clearSearch');
		
	}
	
	$scope.removeCarrierMarkup=function(parentpos,pos){

		$scope.newmarkuplist[parentpos].UpchargeByCarrier.splice(pos,1);
		
	}
	
	$scope.excludeProfile=function(parentpos,pos,ProfileId){
		
		$http({
				    method  : 'POST',
				    url     : '/front/profiles/excludeprofile-markup',
				    data    : {ProfileId:ProfileId,_id:$scope.markuplist[parentpos]._id}, 
				  
				   })
				    .success(function(data) {
						$scope.markuplist[pos].MappedProfile=data;
						$scope.loadMarkup('');
				    });
	}
	
	$scope.profileCarrier=function(){
		
		$http({
			 method  : 'POST',
			 url     : '/front/profiles/carrier-markup',
			 data    : {profile:$scope.profile_id,fields:'carrierId,ProfileId,clientID,carriercode'},
			 })
			.success(function(data) {
			 if(typeof data.profile.Markup!='undefined'){
						 
					 if(data.profile.Markup.UpchargeByCarrier){
						 $scope.mprofile.UpchargeByCarrier=data.profile.Markup.UpchargeByCarrier;
					 }else{
					 $scope.mprofile.UpchargeByCarrier=data.carrier;
						}
					 
					 if(typeof data.profile.Markup.TopLevelUpCharge!='undefined'){
					 $scope.mprofile.TopLevelUpCharge.profileChargePercent=data.profile.Markup.TopLevelUpCharge.ChargePercent;
					 $scope.mprofile.TopLevelUpCharge.profilecarrierMinCharge=data.profile.Markup.TopLevelUpCharge.MinCharge;
					 $scope.mprofile.TopLevelUpCharge.profileTopChargeType=data.profile.Markup.TopLevelUpCharge.TopChargeType;
					 
						}
			  }else{
				 $scope.mprofile.UpchargeByCarrier=data.carrier;  
			  }
			 //console.log($scope.mprofile);
			 

 				    });
 				    
	}
	
	$scope.delProfileMarkup=function(){
		
		$http({
			 method  : 'POST',
			 url     : '/front/profiles/delprofile-markup',
			 data    : {profileId:$scope.profile_id},
			 })
			.success(function(data) {
			
					 $scope.mprofile={};
					$scope.mprofile.TopLevelUpCharge={};
			 

 				    });
		
	}
	
	$scope.updateProfileMarkup=function(){
		
		$http({
			 method  : 'POST',
			 url     : '/front/profiles/updateprofile-markup',
			 data    : {profileId:$scope.profile_id,profile:$scope.mprofile},
			 })
			.success(function(data) {
			// $scope.profile.UpchargeByCarrier=data;
			$scope.mprofile.TopLevelUpCharge.profileChargePercent=data.Markup.TopLevelUpCharge.ChargePercent;
			 $scope.mprofile.TopLevelUpCharge.profilecarrierMinCharge=data.Markup.TopLevelUpCharge.MinCharge;
			 $scope.mprofile.TopLevelUpCharge.profileTopChargeType=data.Markup.TopLevelUpCharge.TopChargeType;
			  
			 $scope.mprofile.UpchargeByCarrier=data.Markup.UpchargeByCarrier;
		
 				    });
	}
	
















/****************************************Carrier API*******************************************************************************/







//we will store all of our form data in this object
	$scope.apiSetupCollection = {saveButtonText: 'Next', carriers: [],unassignedcarriers:[]};

	//dataset to hold individual carrier api setup data
	$scope.apisetup = {rate_apiactivated: false, shp_zip: '', csn_zip: ''};

	//dataset to hold individual carrier accs tariff including default and custom set by the particular user
	$scope.accsRates = {defaultRates: null, customRates: null, accsTypes:null, calculationTypes: null, defaultRate: null, totalDefaultCount: 0, totalCustomRateCount: 0,};
	
	//dataset to hold individual carrier accs tariff including default and custom set by the particular user
	$scope.fuelRates = {defaultRates: null, customRates: null, fuelTypes: null, defaultRate: null, totalDefaultCount: 0, totalCustomRateCount: 0, };

	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('fuelTariffTypeChanged', function(e, args){
		$scope.apisetup.fuel_ruletariff = args;
	});

	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('accsTariffTypeChanged', function(e, args){
		$scope.apisetup.acc_ruletariff = args;
	});

	//listener to the event when directive to emit event for changing accs tariff type attribute
	$scope.$on('filterCustomAccsTariff', function(e, args){
		//console.log(args);
		if(args.carrierId){
			fetchAccsTariffs(args.carrierId, args);
		}
	});
	
	//listener to the event when directive to emit event for changing fuel tariff type attribute
	$scope.$on('filterCustomFuelTariff', function(e, args){
		//console.log(args);
		if(args.carrierId){
			fetchFuelTariffs(args.carrierId, args);
		}
	});
	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOptions = {
        types: ['locality', 'postal_code']
    }

	//intializing data from the server to list all previously selected carriers
	

	//sorting function to list api enabled carriers
	$scope.sortByApiEnabledCarrier = function(carrier){
		if($scope.apiEnabledCarrierOnly){
			if(carrier.apiEnabled ==1)
				return carrier;
			else
				return;
		}else
			return carrier;
	}

	//selecting and fetching the carrier details from the db when selected from the search auto complete
	$scope.selectedSearchCarrier = function(selected){
		//if this carrier is not api enabled then break otherwise load config window
		if(selected === undefined || selected.originalObject.apiEnabled === undefined || selected.originalObject.apiEnabled == 0)
			return;

		//getting scac code
		var scac = selected.originalObject.scac;
		//getting the carrier ID for the selected carrier
		var carrierId = selected.originalObject.id;
		//console.log(angular.element('div[data-scac=' + scac + ']').attr('id'));
		var carrierIndex = angular.element('div[data-scac=' + scac + ']').attr('id');
		if(carrierIndex !== undefined){
			var index = carrierIndex.split('_');
			index = index[index.length -1];
			//console.log(index);

			if(scac !== undefined && index !== undefined && carrierId !== undefined){
				$scope.visibleCarrierApiSetup(carrierId, scac,'withoutsetup');
			}
		}
	}

	$scope.visibleCarrierApiSetup = function(carrierId, scac,type){
		//setting scac for the selected carrier
		$scope.apisetup.scac = scac;
		$scope.apisetup.carrierID = carrierId;
		//return;
		
		//fetching the data from server for the selected carrier api details set previously if any
		CarrierApiService.getCarrierApiDetails(carrierId)
			.success(function(result){
				//console.log(result.count);

				var carrierIndex = angular.element('div[data-scac=' + scac + ']').attr('id');
				
				if(carrierIndex !== undefined){
					carrierIndex = carrierIndex.split('_');
					carrierIndex = carrierIndex[carrierIndex.length -1];
				}
				
				//dom jq manipulation to make enable disable
				jQuery('div[id ^= carrier2_]').addClass('hidden');
				jQuery('div[id ^= carrier_]').addClass('hidden');
				if(type=='withoutsetup'){
				
				jQuery('#carrier2_' + carrierIndex).removeClass('hidden');
				}else{
					
				jQuery('#carrier_' + carrierIndex).removeClass('hidden');	
				}

				//binding api setup form with the already saved result from the DB or blank if creating a new record
				//console.log($scope.apiStatusImage);
				if(result.count && result.count >=1 && result.carrier){
					//resetting all error or success message for the previous carrier
					$scope.error = {status: false, message: '',};
					$scope.success = {status: false, message: '',};

					$scope.apisetup = angular.extend({}, result.carrier, {scac: scac, step: 4});
					//console.log($scope.apisetup);
					//making api status icon depending on existing record
					if(result.carrier.rate_enablepickup !== undefined && result.carrier.rate_enablepickup == 1)
						$scope.apisetup.rate_enablepickup = true;
					else if(result.carrier.rate_enablepickup !== undefined && result.carrier.rate_enablepickup == 0)
						$scope.apisetup.rate_enablepickup = false;
					
					if(result.carrier.rate_enablerate !== undefined && result.carrier.rate_enablerate == 1)
						$scope.apisetup.rate_enablerate = true;
					else if(result.carrier.rate_enablerate !== undefined && result.carrier.rate_enablerate == 0)
						$scope.apisetup.rate_enablerate = false;
					
					if(result.carrier.rate_enabletracking !== undefined && result.carrier.rate_enabletracking == 1)
						$scope.apisetup.rate_enabletracking = true;
					else if(result.carrier.rate_enabletracking !== undefined && result.carrier.rate_enabletracking == 0)
						$scope.apisetup.rate_enabletracking = false;
					
					if(result.carrier.rate_apiactivated !== undefined && result.carrier.rate_apiactivated == 1)
						$scope.apiStatusImage = $scope.apiStatusImageSuccess;
					else if(result.carrier.rate_apiactivated !== undefined && result.carrier.rate_apiactivated == 0)
						$scope.apiStatusImage = $scope.apiStatusImageError;
					else
						$scope.apiStatusImage = $scope.apiStatusImageUnknown;
				}else if(result.count !== undefined && !result.count){
					$scope.apisetup = angular.extend({}, {rate_apiactivated: false, scac: scac, step: 4, carrierID: carrierId});
					$scope.apiStatusImage = $scope.apiStatusImageUnknown;
				}
				$scope.apisetup.contract_profilecode=$scope.profile_id;

				//$scope.$digest();
			});

		//calling private method for accessorial rates
		fetchAccsTariffs(carrierId);

		//calling private method for fuel rates
		fetchFuelTariffs(carrierId);
	};



	//method to connect to the carrier api services where the credential and connectivity can be checked
	$scope.checkCarrierStatus = function(){
		//console.log($scope.apisetup);
		//return;
		//checking appropriate
		var credentials = $scope.apisetup;
		/*if(credentials.rate_apiuserid && credentials.rate_apiaccount 
			&& credentials.rate_apipwd && credentials.scac
		){
		*/
			//saving the form details

			//making api active status false intially
			$scope.apisetup.rate_apiactivated = false;
			//now saving the total details to the db
			//console.log($scope.apisetup);
			CarrierApiService.postApiDetails($scope.apisetup)
				.success(function(result){
					if(result.success && result.id){
						$scope.apisetup.id = result.id;
						
						
						$scope.loadCarriersection($scope.profile.RateProfileCode,$scope.profile._id)
						
						
						
						//now building deffered obejct to resolve the api status and update accordingly when promise object is returned
						var deferred = $q.defer();
						var promise = deferred.promise;

						promise.then(function(value){
							//console.log(value.data);
							if(value.data && value.data.Result) {
								if(value.data.Result == "Success"){
									//generating success message
									$scope.success.status = true;
									$scope.success.message = 'Connectivity passed successfully';
									
									//making api active status false intially
									$scope.apisetup.rate_apiactivated = true;
									$scope.apiStatusImage = $scope.apiStatusImageSuccess;
									CarrierApiService.postApiDetails($scope.apisetup); //saving the new status as success
									
								}else if(value.data.Result == "Error"){
									$scope.error.status = true;
									$scope.error.message = value.data.ErrorMessage ? value.data.ErrorMessage : 'Unknown Error Response';
									$scope.apiStatusImage = $scope.apiStatusImageError;
								}
									
							}
						}, function(reason){
							console.log(reason);
						}, function(update){
							console.log(update);
						});

						deferred.resolve(
							CarrierApiService.checkApiStatus(credentials.rate_apiuserid, credentials.rate_apipwd, credentials.rate_apiaccount, credentials.scac)
						);

					}else if(result.error){
						alert(result.error);
					}
				});
		//} 
	};

	$scope.addCarrier = function(){
		window.location.href="/front/setup-connectivity/add-carrier";
	};
	
	$scope.CarrierWebservice=function(){
		
		
		if($scope.rightpanel!='profile')
		return;
		
		CarrierApiService.fecthSelectedCarriers(true,'',false,false,false,$scope.profile_id) //calling factory to make ajax request
			.success(function(result){
				 if(result.success && result.carriers && result.carrierlist){
					//$scope.selectedCarrierList = result.carriers;
					$scope.apiSetupCollection.carriers = result.carriers;
					
					
					//if the page has params to load specific details of a carrier from the url search params then load that first
					//otherwise load the default one
					var urlSearchParams = $location.search()
					var scac = urlSearchParams.scac;
					var carrierID = urlSearchParams.id;
					
					if(scac && carrierID)
						$scope.visibleCarrierApiSetup(carrierID, scac,'setup');
					else{
						//iterating to the list to get the first api enabled carrer to load
						var keepGoing = true;
						angular.forEach($scope.apiSetupCollection.carriers, function(carrier, index){
							if(keepGoing){
								
								if(carrier.apiEnabled !== undefined && carrier.apiEnabled == 1){
									
									$scope.visibleCarrierApiSetup(carrier.id, carrier.scac,'setup');
									keepGoing = false;
								}	
							}	
						});
					}
				}
			});
			
			CarrierApiService.getUnassignedCarriers(true,'',true,false,false) //calling factory to make ajax request
			.success(function(result){
				$scope.apiSetupCollection.unassignedcarriers=result.carriers;
				

				
			});
		
	}
	
	function fetchDatafromServer(){
	}

	function fetchAccsTariffs(carrierId, filter){
		var deferredAccs = $q.defer();
		var promiseAccs = deferredAccs.promise;

		promiseAccs.then(function(result){
			//console.log(result);
			if(result.data.success){
				if(result.data.rates.tariffsDefault)
					$scope.accsRates.defaultRates = result.data.rates.tariffsDefault
				else
					$scope.accsRates.defaultRates = [];
				
				if(result.data.rates.tariffsCustom)
					$scope.accsRates.customRates = result.data.rates.tariffsCustom;
				else
					$scope.accsRates.customRates = [];
				if(result.data.accsTypes){
					var accsTypes = result.data.accsTypes;
					accsTypes.unshift({accsname: "Not Specified", id: ""});
					$scope.accsRates.accsTypes = accsTypes;
					//console.log($scope.accsRates.accsTypes);
				}else
					$scope.accsRates.accsTypes = [];
				
				if(result.data.calculationTypes)
					$scope.accsRates.calculationTypes = result.data.calculationTypes;
				else
					$scope.accsRates.calculationTypes = [];
			}
		}, function(reason){
			console.log(reason);
		}, function(progress){
			console.log(progress);
		});

		deferredAccs.resolve(CarrierApiService.getCarrierAccTariffDetails(carrierId, filter));
	}

	function fetchFuelTariffs(carrierId, filter){
		var deferredFuel = $q.defer();
		var promiseFuel = deferredFuel.promise;

		promiseFuel.then(function(result){
			//console.log(result);
			if(result.data.success){
				if(result.data.rates.tariffsDefault && result.data.rates.totalDefaultCount){
					$scope.fuelRates.defaultRates = result.data.rates.tariffsDefault;
					$scope.fuelRates.totalDefaultCount = result.data.rates.totalDefaultCount;
				}else{
					$scope.fuelRates.defaultRates = [];
					$scope.fuelRates.totalDefaultCount = 0;
				}

				if(result.data.rates.tariffsCustom && result.data.rates.totalCustomRateCount){
					$scope.fuelRates.customRates = result.data.rates.tariffsCustom;
					$scope.fuelRates.totalCustomRateCount = result.data.rates.totalCustomRateCount;
				}
				else{
					$scope.fuelRates.customRates = [];
					$scope.fuelRates.totalCustomRateCount = 0;
				}

				if(result.data.fuelTypes){
					var fuelTypes = result.data.fuelTypes;
					fuelTypes.unshift({fuelType: "Not Specified", id: ""});
					$scope.fuelRates.fuelTypes = fuelTypes;
				}
				else
					$scope.fuelRates.fuelTypes = [];
			}
		}, function(reason){
			console.log(reason);
		}, function(progress){
			console.log(progress);
		});

		deferredFuel.resolve(CarrierApiService.getCarrierFuelTariffDetails(carrierId, filter));
	}
	$scope.stext='';
	$scope.inputChanged=function(text){
		$scope.stext=text;
		
		
		
	}
	$scope.getCarrierSearch=function(){
		
		CarrierApiService.getUnassignedCarriers(true,$scope.stext,true,false,false) //calling factory to make ajax request
			.success(function(result){
				$scope.apiSetupCollection.unassignedcarriers=result.carriers;
				

				
			});
			
		
	}
	
	
	
	
	
	
	
	
	
/*************************************grid **************************************/











  $scope.gridOptions = {
    enableRowSelection: true,
    enableSelectAll: false,
    selectionRowHeaderWidth: 35,
    selectionRowHeaderHeight: 35,
    rowHeight: 35,
    multiSelect:false
    
  };
 $scope.ratematrix={};
 $scope.Matrixheads=[]; 
 $scope.rateaddnew=false;
 $scope.gridOptions.data=[];
 $scope.existingmatrix={};
 $scope.ratelanematrix={};
 $scope.gridcount=0;
 $scope.currentgrid={};
 
 $scope.matrixreset=function(){

 $scope.ratematrix={};
 $scope.Matrixheads=[]; 
 $scope.rateaddnew=false;
 $scope.gridOptions.data=[];
 $scope.existingmatrix={};
 $scope.ratelanematrix={};
 $scope.gridcount=0; 
 }
      $scope.storeFile = function( gridRow, gridCol, files ) {
        // ignore all but the first file, it can only select one anyway
        // set the filename into this column
        gridRow.entity.filename = files[0].name;
     
        // read the file and set it into a hidden column, which we may do stuff with later
        var setFile = function(fileContent){
          gridRow.entity.file = fileContent.currentTarget.result;
          // put it on scope so we can display it - you'd probably do something else with it
          $scope.lastFile = fileContent.currentTarget.result;
          $scope.$apply();
        };
        var reader = new FileReader();
        reader.onload = setFile;
        reader.readAsText( files[0] );
      };
     
      $scope.gridOptions.columnDefs = [
        { name: 'rowstate', displayName: 'States', enableCellEdit: false, width: '10%' },
       
        
      ];
     
     $scope.msg = {};
     
     $scope.gridOptions.onRegisterApi = function(gridApi){
              //set gridApi on scope
              $scope.gridApi = gridApi;
              gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue ;
                $scope.$apply();
                console.log($scope.msg);
              });
              
             
                  gridApi.selection.on.rowSelectionChanged($scope,function(row){
					//var msg = 'rows changed ' + rows.length;
					
					if(row.isSelected){
						
					$scope.gridselection=row.entity;
					//console.log($scope.gridselection)
					}else{
					$scope.gridselection={};	
					}
					
					
					
				  });
             
            };
 $scope.loadGridData=function(){
	 
	 if($scope.rightpanel!='profile')
		return;
	     $scope.gridselection={};
           // var  data =[];
         //$scope.gridOptions.data = data;
          //$scope.gridOptions.data=[];
          angular.element('.loading').removeClass('hidden');
          
         $http({
			 method  : 'POST',
			 url     : '/front/profiles/fetch-matrix',
			 data:{matrixform:$scope.ratematrix,cid:$scope.contract._id}
			 })
			.success(function(data) {
				
				
				
				
				$scope.MatrixOptions=data.matrixoptions; 
				
				
				$scope.ColHeadingOptions=data.headingoptions;
				var i=0;
				$scope.ColHeadingOptions.forEach(function(item) {
					$scope.ColHeadingOptions[i].label=item.name+"  ("+item.name.replace(/[^a-zA-Z0-9]/g, '')+")";
					
					
					i++;
				});
			
				
				
				if(typeof data.existingmatrix._id!='undefined' ){
				$scope.existingmatrix=data.existingmatrix;
				
				
				$scope.gridOptions.columnDefs=[];
						
						
				$scope.ratelanematrix.OriginLocation=$scope.existingmatrix.OriginLocation
				$scope.ratelanematrix.DestinationLocation=$scope.existingmatrix.DestinationLocation
				
				
				var col1=$scope.existingmatrix.OriginLocation.orggrid;
				
				var col2=$scope.existingmatrix.DestinationLocation.destgrid;
				
		
				
				
			
				var cdata={ field:col1,name:col1 , displayName: $scope.existingmatrix.OriginLocation.name, enableCellEdit: false, width: '20%' };
				$scope.gridOptions.columnDefs.push(cdata)
					
				var cdata={ field:col2,name: col2, displayName: $scope.existingmatrix.DestinationLocation.name, enableCellEdit: false, width: '20%' };
				$scope.gridOptions.columnDefs.push(cdata)  
				
				$scope.existingmatrix.matrix.forEach(function(item) {
					var cdata={ field:item.name.replace(/[^a-zA-Z0-9]/g, ''),name: item.name.replace(/[^a-zA-Z0-9]/g, ''), displayName: item.name, enableCellEdit: true, width: '10%' };
					  $scope.gridOptions.columnDefs.push(cdata);
					  
					  
				});
				
				
				   
						  if(data.griddata.length>0){
							  
							  $scope.gridOptions.data=data.griddata;
							  $scope.ratelanematrix=data.matrixdata;
							  $scope.gridcount=data.griddata.length;
							 
							  
						  }else{
						  
						  var griditem={};
						  griditem[col1]=" ";
						  griditem[col2]=" ";
						  
						  $scope.existingmatrix.matrix.forEach(function(item) {
						  griditem[item.name.replace(/[^a-zA-Z0-9]/g, '')]=" ";
						  
							});
						  
						  
						  $scope.gridOptions.data.push(griditem);
						  
					   }        
                   
                  
				  
					}
					
					
					var cdata={ field:'Action',name: 'Action', displayName: 'Action', enableCellEdit: false, width: '10%' ,
						
						cellTemplate: '<div class="text-center"><a href="javascript:void(0)" ng-click="grid.appScope.DeleteMatrix(row)"><i class="fa fa-trash-o"></i></a></div>'
						};
					  $scope.gridOptions.columnDefs.push(cdata);
				
						angular.element('.loading').addClass('hidden');
 				    });
         
         
       
	}




 
 $scope.populateHead=function(){
		if($scope.ratematrix.colhead.id!=''){
		var item=$scope.ratematrix.colhead;
		$scope.Matrixheads.push(item);
		
		}
 }
 
 $scope.matrixaddnew=function(){
	 $scope.rateaddnew=true;
 }
 $scope.matrixclosenew=function(){
	 $scope.rateaddnew=false;
 }
 
 $scope.deleteHead=function(){
	 
	
     
	if($scope.ratematrix.selectedcolhead){	
		for (var i =0; i < $scope.Matrixheads.length; i++)
		   if ($scope.Matrixheads[i].name === $scope.ratematrix.selectedcolhead[0].name) {
			  $scope.Matrixheads.splice(i,1);
			  
			  break;
		   }
	$scope.ratematrix.colhead=$scope.ratematrix.selectedcolhead[0].name;
}
     
 }
 
 $scope.addMatrix=function(form){
	 
	 $http({
			 method  : 'POST',
			 url     : '/front/profiles/save-matrix',
			 data    : {matrix:$scope.Matrixheads,name:$scope.ratematrix.matrix_name,OriginLocation:$scope.ratematrix.OriginalLocation,DestinationLocation:$scope.ratematrix.DestinationLocation,_token:$scope.ratematrix._token,
				 nomatrix:$scope.nomatrix,mfile:$scope.mfile,cId:$scope.contract._id
				 },
			 })
			.success(function(data) {
				$scope.rateaddnew=false;
				$scope.Matrixheads=[];
				$scope.MatrixOptions=data.moptions;
				$scope.ratematrix.matrix=data.selected;
				 $scope.loadGridData();
 				    });
	 
 }
$scope.hidenewrate=function(){
	if($scope.ratematrix.matrix.id!=""){
		$scope.rateaddnew=false;
	}
	
}

$scope.updateMatrixlane=function(){
	
	//console.log( $scope.ratelanematrix);
	//console.log( $scope.gridOptions.data);
	$scope.ratelanematrix.cId=$scope.contract._id;
	if(typeof $scope.ratematrix.matrix!='undefined'){
	$scope.ratelanematrix.gridId=$scope.ratematrix.matrix.id;
	}
	
	
	$http({
			 method  : 'POST',
			 url     : '/front/profiles/save-lanematrix',
			 data    : {grid:$scope.gridOptions.data,matrixlane:$scope.ratelanematrix},
			 })
			.success(function(data) {
				$scope.rateaddnew=false;
				$scope.Matrixheads=[];
				$scope.loadGridData();
 				    });
 				    
 				    
}

$scope.AddRow=function(){
	
				var col1=$scope.existingmatrix.OriginLocation.orggrid;
				var col2=$scope.existingmatrix.DestinationLocation.destgrid;
	var griditem={};
				  griditem[col1]=" ";
				  griditem[col2]=" ";
				  
				  $scope.existingmatrix.matrix.forEach(function(item) {
				  griditem[item.name.replace(/[^a-zA-Z0-9]/g, '')]=" ";
				  
					});
				  
				  
				  $scope.gridOptions.data.push(griditem);
				  $scope.gridcount=$scope.gridOptions.data.length;
				  $scope.gridApi.selection.selectRow($scope.gridOptions.data[$scope.gridcount-1]);
				
				 
				 
};

$scope.DeleteMatrix=function(row){
	if($window.confirm('Are you sure ?')){
	var index = $scope.gridOptions.data.indexOf(row.entity);
      
      if(row.entity._id){
            $http({
			 method  : 'POST',
			 url     : '/front/profiles/delete-lanematrix',
			 data    : {_id:row.entity._id,cId:$scope.ratelanematrix.cId},
			 })
			.success(function(data) {
				      $scope.gridOptions.data.splice(index, 1);
 				    });
 				    
			}else{
				 $scope.gridOptions.data.splice(index, 1);
				
			}
 				    
 	}
};

$scope.deleteRatematrix=function(ratematrix){
	
if($window.confirm('Are you sure ?')){	
	
	$http({
			 method  : 'POST',
			 url     : '/front/profiles/delete-matrix',
			 data    : {_id:ratematrix.id},
			 })
			.success(function(data) {
				      
				      if(data.success==false){
						  alert("Data already exists for this matrix.")
					  }else{
						$scope.rateaddnew=false;
						$scope.Matrixheads=[];
						$scope.MatrixOptions=data.moptions;
						$scope.ratematrix.matrix={};
					 }
 				    });
 				    
 				    
	};
	
}
$scope.nomatrix=false;
$scope.mfile={};
$scope.UploadRate = function (files,cid) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: '/front/profiles/upload-matrix',
                    fields: {
                        'cid': cid
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.log = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
                                
                }).success(function (data, status, headers, config) {
					
                	if(data.success==false){
                		//$scope.error.status = true;
						//$scope.error.message = data.error;
						
						if(data.nomatrix==true){
							console.log('create matrix to upload file.')
							$scope.nomatrix=data.nomatrix;
							$scope.mfile=data.result;
							$scope.rateaddnew=true;
							jQuery("#ratematrixDetails").modal('show');
						}else{
							alert(data.msg);
						}
                	}else if(data.success==true){
                		//$scope.log = 'file ' + config.file.name + ' uploaded successfully!'
						alert(data.msg);
                		$scope.loadGridData();
                		
                	}
                  
                   
                });
            }
        }
    };

$scope.handleBlurEvent=function(model,key,element){
	
	model[key]=jQuery(element).val();
	
}
	
}



