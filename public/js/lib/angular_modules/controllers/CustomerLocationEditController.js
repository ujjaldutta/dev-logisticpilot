'use strict';

function CustomerLocationEditController(CustomerLocationAPIEndpoint, CustomerLocation, $scope, $location, $window){
	$scope.saveBtnText = 'Save';
	
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOption = {
        componentRestrictions: {  },
        types: ["(regions)"]
    }

    //collection  of customer type objects
	$scope.clientLocationData = {};
	$scope.locationTypes = [];
	$scope.timeZones = [];
	$scope.locationId = null;

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = CustomerLocationAPIEndpoint.base ? CustomerLocationAPIEndpoint.base : '/';
	}
	
	/*
	* when valid cust id is loaded or assigned 
	* after dom rendered make a aysnc req to the server 
	* to get the information
	*/
	$scope.$watch('custID', function(val){
		if(!isNaN(val) && val > 0){
			if(!$scope.newLocation && $scope.locationId){
				//fetching the selected customer data
				getCustomerLocationData($scope.locationId);
			}
			
			getLocationTypes();

			geTimeZones();
		}
			
	});


	/*
	* Saving the profile fields
	*/
	
	$scope.saveLocationData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			CustomerLocation.postLocationData($scope.custID, $scope.clientLocationData)
				.then(function(response){
					if(response.data.success && response.data.locationId){
						$scope.clientLocationData.id = response.data.locationId;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							//$scope.error.message = CustomerLocation.formatValidationErrors(response.data.error);
							$scope.error.message = 'Unhandled Exception occurred';
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}
	

	function getCustomerLocationData(id){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		CustomerLocation.makeAsyncRequest(CustomerLocation.getCustomerData(id))
			.then(function(response){
				if(response.data.success && response.data.clientLocation){
					$scope.clientLocationData = response.data.clientLocation;
					
					//filling the location input with the already selected location details but saved separately
					$scope.clientLocationData.location_location = CustomerLocation.fillGoogleLocation($scope.clientLocationData, 'location');

					//now converting data type to either true or false for checkboxes
					$scope.clientLocationData.location_liftgate = !!parseInt($scope.clientLocationData.location_liftgate);
					$scope.clientLocationData.location_apptrequired = !!parseInt($scope.clientLocationData.location_apptrequired);
					$scope.clientLocationData.location_other = !!parseInt($scope.clientLocationData.location_other);
				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	/* function __fillLocationDetails(collection){
		var location = '';
		
		if(collection.client_city)
			location += collection.client_city + ', ';
		if(collection.client_state)
			location += collection.client_state + ', ';
		if(collection.client_postal)
			location += collection.client_postal + ', ';
		if(collection.client_country)
			location += collection.client_country;

		return location;
	} */

	function __fillLocationDetails(collection, prf){

		var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		
		if(collection[city] !== undefined && collection[city])
			location += collection[city] + ', ';
		if(collection[state] !== undefined && collection[state])
			location += collection[state] + ', ';
		if(collection[postal] !== undefined && collection[postal])
			location += collection[postal] + ', ';
		if(collection[country] !== undefined && collection[country])
			location += collection[country];

		return location;
	}

	function getLocationTypes(){
		CustomerLocation.makeAsyncRequest(CustomerLocation.getLocationTypes())
			.then(function(response){
				if(response.data.success && response.data.locationTypes){
					$scope.locationTypes = response.data.locationTypes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function geTimeZones(){
		CustomerLocation.makeAsyncRequest(CustomerLocation.geTimeZones())
			.then(function(response){
				if(response.data.success && response.data.timeZones){
					$scope.timeZones = response.data.timeZones;
				}
			}, function(reason){
				console.warn(reason);
			});
	}
}


