'use strict';

function CarrierAccessorialEditController(CarrierEquipmentAPIEndpoint, CarrierEquipment, $scope, $location, $window){
	$scope.saveBtnText = 'Save';

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

    //collection  of customer type objects
	$scope.accessorialData = {};
	$scope.accessorialTypes = [];
	$scope.calculationTypes = [];
	$scope.recordId = null;
	$scope.accsDropDownSettings = {enableSearch: true, selectionLimit: 1, idProp: 'id', displayProp: 'accsname'};

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = '/front/setup-connectivity/list-carrier-accessorials/' + $scope.carrierId;
	}
	
	/*
	* when valid cust id is loaded or assigned 
	* after dom rendered make a aysnc req to the server 
	* to get the information
	*/
	
	$scope.$watch('carrierId', function(val){
		if(!isNaN(val) && val > 0){
			if(!$scope.newRecord && !isNaN($scope.recordId)){
				//fetching the selected customer data
				getAccessorialData($scope.recordId);
			}
			
			getCalculationAccessorialTypes(val);

			//getAccessorialTypes(val);
		}
			
	});

	$scope.$watch('recordId', function(val){
		if(!isNaN(val) && val > 0){
			//fetching the selected customer data
			getAccessorialData(val);
		}
	});


	/*
	* Saving the profile fields
	*/
	
	$scope.saveAccessorialData = function(form){
		if(form && form.$valid){
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');
			
			CarrierEquipment.postAccessorialData($scope.carrierId, angular.extend({step: 'postCarrierAccTariffCustomDetails'}, $scope.accessorialData))
				.then(function(response){
					if(response.data.success && response.data.id){
						$scope.accessorialData.id = response.data.id;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							//$scope.error.message = CarrierEquipment.formatValidationErrors(response.data.error);
							$scope.error.message = 'Unhandled Exception occurred';
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}
	

	function getAccessorialData(id){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.getAccessorialData(id))
			.then(function(response){
				if(response.data.success && response.data.accsDetails){
					$scope.accessorialData = response.data.accsDetails;
					$scope.accessorialData.crraccs_minrate = parseFloat($scope.accessorialData.crraccs_minrate);
					$scope.accessorialData.crraccs_rate = parseFloat($scope.accessorialData.crraccs_rate);
				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	function getCalculationAccessorialTypes(carrierId){
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.getCalculationTypes(carrierId))
			.then(function(response){
				if(response.data.success && response.data.calculationTypes){
					$scope.calculationTypes = response.data.calculationTypes;
				}
				if(response.data.success && response.data.accsTypes){
					$scope.accessorialTypes = response.data.accsTypes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	/* function getAccessorialTypes(carrierId){
		CarrierEquipment.makeAsyncRequest(CarrierEquipment.getAccessorialTypes(carrierId))
			.then(function(response){
				if(response.data.success && response.data.timeZones){
					$scope.timeZones = response.data.timeZones;
				}
			}, function(reason){
				console.warn(reason);
			});
	} */
}


