function rateCalculationController($scope, SyncCarriers, carrierCalculation, carrierRateSearch, $q, $timeout){
	$scope.searchBtnText = "Get my Rates";
	$scope.showSearchProgress = false;
	$scope.searchProgress = 0.0;
	$scope.searchStarted = false;
	
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	//flag to use filter or not when displaying result
	$scope.usePriceFilter = false;
	$scope.useTransitFilter = false;
	
	//search class type
	$scope.productClassTypes = [ {id: 1, label: "Yes"}, {id: 2, label: "No"},];

	
	//search products
	$scope.itemTypes = null;
	
	//search classes if type = 1
	$scope.productClasses = [ 
		{id: 50, label: 50}, 
		{id: 55, label: 55},
		{id: 60, label: 60},
		{id: 65, label: 65},
		{id: 70, label: 70},
		{id: 77.5, label: 77.5},
		{id: 85, label: 85},
		{id: 92.5, label: 92.5},
		{id: 100, label: 100},
		{id: 110, label: 110},
		{id: 125, label: 125},
		{id: 150, label: 150},
		{id: 175, label: 175},
		{id: 200, label: 200},
		{id: 250, label: 250},
		{id: 300, label: 300},
		{id: 400, label: 400},
		{id: 500, label: 500},
	];
	
	$scope.carriers = [];
	
	$scope.accessorials = [];

	$scope.rates = {addlAccessorial: [],}; //container for which all the patameters will be sent to the api

	$scope.rates.searchResults = [];

	$scope.rates.products = [{
		qty: 1,
		classType: 1,
		len: 1,
		width: 1,
		height: 1,
		weight: 1,

	}];

	$scope.onPriceFilter = function(){
		$scope.usePriceFilter = true;
	}

	$scope.onTransitFilter = function(){
		$scope.useTransitFilter = true;
	}

	//resetting the filter to show all search result
	$scope.resetFilter = function(type){
		switch(type){
			case 'price':
				$scope.usePriceFilter = false;
				break;
			case 'transit':
				$scope.useTransitFilter = false;
				break;
		}
	}

	//filter method to filter the serach result using vairous params
	$scope.sortByPriceAndTransit = function(searchResults){
		//filtering price when pricefilter is on
		if($scope.usePriceFilter && $scope.useTransitFilter){
			if(
				(!searchResults.TotalShipmentCharge.length ||
					(Math.ceil(searchResults.TotalShipmentCharge) >= $scope.priceFilter.min && Math.ceil(searchResults.TotalShipmentCharge) <= $scope.priceFilter.max)) &&
				(!searchResults.TransitTime.length ||  
					(searchResults.TransitTime >= $scope.transitFilter.min && searchResults.TransitTime <= $scope.transitFilter.max))
			){
				return searchResults;
			}else
				return;
		}else if($scope.usePriceFilter){
			if(
				!searchResults.TotalShipmentCharge.length ||
					(Math.ceil(searchResults.TotalShipmentCharge) >= $scope.priceFilter.min && Math.ceil(searchResults.TotalShipmentCharge) <= $scope.priceFilter.max)
			){
				return searchResults;
			}else
				return;
		}else if($scope.useTransitFilter){
			if(
				!searchResults.TransitTime.length ||  
					(searchResults.TransitTime >= $scope.transitFilter.min && searchResults.TransitTime <= $scope.transitFilter.max)
			){
				return searchResults;
			}else
				return;
		}else
			return searchResults;
	}

	
	//fetching some important types and data
	fetchDataConfig();
	
	$scope.addItem = function(){
		$scope.rates.products.push({
			qty: 1,
			classType: 1,
			len: 1,
			width: 1,
			height: 1,
			weight: 1,
		});
	}
	
	$scope.removeItem = function(item){
		if(confirm('Are you sure to remove this item'))
			$scope.rates.products.splice(item, 1);
	}

	$scope.calculateDensity = function(l, w, h, wt){
		return carrierCalculation.calculateDensity(l, w, h, wt);
	}

	$scope.calculateTotalWeight =  function(){
		var  wights = new Array();
		for(var i = 0; i < $scope.rates.products.length; i ++){
			var item = $scope.rates.products[i];
			wights.push(item.weight);
		}
		return carrierCalculation.calculateTotalweight(wights);
	}
	
	$scope.searchForFilter = function(){
		var promises = [];
		//changing button text to refect the search procedure is going
		$scope.searchBtnText = 'Searching ...';
		$scope.searchStarted = true; //indicator that the search at least intiated once
		//activating the search progress bar
		$scope.showSearchProgress = true;
		//clearing existing result for search
		$scope.rates.searchResults = [];

		//filtering carriers using _ which have only api enabled as they only to be regarded for searching
		var searchSuccess = 0;
		var totalSearches = _.where($scope.carriers, {apiEnabled: 1});
		
		for(var i = 0; i < $scope.carriers.length; i ++){
			var data = $scope.carriers[i];
			//calling the api service with only those carriers are API enabled
			if(data.apiEnabled && data.apiEnabled == 1){
				var promise = carrierRateSearch.searchForCarrier(angular.extend(data, $scope.rates))
						.then(function(result){
							searchSuccess ++;
							$scope.searchProgress = (searchSuccess / (totalSearches.length)) * 100;
							//console.log(result.data);
							if(result.data.Result && result.data.Result == 'Success' && result.data.GetRate){
								//getting current index which the method is inserting. 
								currentIndex = $scope.rates.searchResults.length;
								$scope.rates.searchResults.push(result.data.GetRate);

								//getting carrier performance and insurance details
								return SyncCarriers.getInsuranceForCarrierLiability(result.data.GetRate.SCAC, currentIndex);
							}

							return; 
						})
						.then(function(result){
							if(result.data && result.data.success && result.data.carrierInfo){
								
								if(result.data.carrierInfo){
									var insuranceLiability = 'Unknown', rating = 0.0; 
									
									//getting insurance info
									if(result.data.carrierInfo.insurances && result.data.carrierInfo.insurances.length){
										insuranceLiability = result.data.carrierInfo.insurances[0].pivot.insurance_amount ? 
										result.data.carrierInfo.insurances[0].pivot.insurance_amount : 'Unknown';
									}

									//getting rating info for this carrier
									if(result.data.carrierInfo.carrier_performance){
										rating = result.data.carrierInfo.carrier_performance ? result.data.carrierInfo.carrier_performance : 0;
									}
									
									
									//updating both
									if(result.data.currentIndex){
										$timeout(function(){ //delaying for smooth display and effect
											$scope.rates.searchResults[parseInt(result.data.currentIndex)].insuranceLiabilityAmt = insuranceLiability;
											$scope.rates.searchResults[parseInt(result.data.currentIndex)].rating = rating;
										}, 1000);
									}
								}
								
							}
							//console.log(result);
						}, function(reason){
							console.log(reason);
						});
				promises.push(promise);
			}	
		}

		$q.all(promises)
			.then(function(data){
				//console.log(data);
			}, function(reason){
				console.log(reason);
			})
			.finally(function(){
				//making the search progress stopped after 2 sec of actual finish
				$timeout(function(){ 
					$scope.searchBtnText = 'Get my Rates';
					$scope.showSearchProgress = false;
					$scope.searchProgress = 0.0;
				}, 2000);
				
				//calculating the max and min values depending on the search for both price and transit days
				maxPrice = _.result(_.max($scope.rates.searchResults, 'TotalShipmentCharge'), 'TotalShipmentCharge');
				maxTransit = _.result(_.max($scope.rates.searchResults, 'TransitTime'), 'TransitTime');
				
				//setting for price range
				$scope.priceFilter.ceil = !isNaN(maxPrice) ? Math.ceil(maxPrice) : $scope.priceFilter.ceil;
				$scope.priceFilter.max = !isNaN(maxPrice) ? Math.ceil(maxPrice) : $scope.priceFilter.max;
				//setting for transit days
				$scope.transitFilter.ceil = !isNaN(maxTransit) ? Math.ceil(maxTransit) : $scope.transitFilter.ceil;
				$scope.transitFilter.max = !isNaN(maxTransit) ? Math.ceil(maxTransit) : $scope.transitFilter.max;

			});
		
	}
	//method to get different items plucked from the already selected carrier list
	$scope.getCarrierInfoBySCAC = function(infoKey, scac){
		try{
			var info = _.result(_.findWhere($scope.carriers, {scac: scac}), infoKey);
			//console.log(info);
			return info;
		}catch(ex){
			console.error(ex.message);
		}
	}

	//method to save quote
	$scope.saveQuote = function (key){
		var row = null;
		row = $scope.rates.searchResults[key] ? $scope.rates.searchResults[key] : null;

		if(row && !row.saved){
			//getting carrier details
			var carrierInfo = _.findWhere($scope.carriers, {scac: row.SCAC});

			var pickupInfo = carrierRateSearch.processGoogleLocationInformation($scope.rates.pickup);
			var deliveryInfo = carrierRateSearch.processGoogleLocationInformation($scope.rates.delivery);

			//building the data blocks to post the api and saved by the backend server
			var dataQuoteMaster = {
				shp_pickupdate: $scope.rates.shipDate,
				
				shp_pickupcity: pickupInfo.city ? pickupInfo.city : null,
				shp_pickupstate: pickupInfo.state ? pickupInfo.state : null,
				shp_pickuppostal: pickupInfo.postal_code ? pickupInfo.postal_code : null,
				shp_pickupcountry: pickupInfo.country ? pickupInfo.country : null,
				
				shp_deliverycity: deliveryInfo.city ? deliveryInfo.city : null,
				shp_deliverystate: deliveryInfo.state ? deliveryInfo.state : null,
				shp_deliverypostal: deliveryInfo.postal_code ? deliveryInfo.postal_code : null,
				shp_deliverycountry: deliveryInfo.country ? deliveryInfo.country : null,

				shp_quotenumber: row.QuoteNumber ? row.QuoteNumber : null,
				shp_quotedate: new Date(),
				shp_transitdays: row.TransitTime ? row.TransitTime : null,
				shp_servicelevelId: row.ServiceLevel ? row.ServiceLevel : null,
				//shp_distance: row.ServiceLevel ? row.ServiceLevel : null,
			};

			var dataCost = {'costs': []};

			//calculating different cost
			//frieght
			dataCost.costs.push({
				cost_srlno: 1,
				cost_code: 'FGT',
				cost_quotedcost: row.GrossCharges,
				cost_carrierid: carrierInfo.id,
				cost_ratetype: 1,
			});
			
			//fuel
			dataCost.costs.push({
				cost_srlno: 2,
				cost_code: 'FUE',
				cost_quotedcost: row.FuelSurCharge,
				cost_carrierid: carrierInfo.id,
				cost_ratetype: 1,
			});

			//discount
			dataCost.costs.push({
				cost_srlno: 3,
				cost_code: 'DSC',
				cost_quotedcost: row.Discount,
				cost_carrierid: carrierInfo.id,
				cost_ratetype: 1,
			});

			var dataProducts = {'products': []};

			//populating all added products and their details
			for(var i = 0; i < $scope.rates.products.length; i ++){
				dataProducts.products.push({
					prd_srlno: i+1, 
					prd_itempkgtype: $scope.rates.products[i].itemType, 
					prd_itemqty: $scope.rates.products[i].qty,
					prd_itemclass: $scope.rates.products[i].classType,
					prd_itemwidth: $scope.rates.products[i].width,
					prd_itemheight: $scope.rates.products[i].height,
					prd_itemlength: $scope.rates.products[i].len,
					prd_itemweight: $scope.rates.products[i].weight,
					prd_itemhazmat: $scope.rates.products[i].hazmat,
					prd_stackable: $scope.rates.products[i].stackable,
				});
			}

			//console.log(dataProducts);

			//now making service call to make async request to save quote related data
			//noting the save procedure is in progress
			row.saveText = 'Saving...';

			carrierRateSearch.makeAsyncRequest(carrierRateSearch.saveQuote(
				angular.extend(
					dataQuoteMaster, dataCost, dataProducts
				)
			)).then(function(result){
				//console.log(result);
				if(result.data && result.data.success){
					//marking this quote as saved so for this search that cannot be saved again
					row.saved = true; 
					row.saveText = 'Saved';
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}
	
	//console.log($scope.addionalAccessorial);

	function fetchDataConfig(){
		//requesting all the selected carrier list
		SyncCarriers.fetch(true)
			.success(function(result){
				if(result.success && result.carriers && result.carriers.length){

					var carriers = result.carriers;

					for(var i=0; i< result.carriers.length; i++){
						if(result.carriers[i].apiEnabled == 1 || result.carriers[i].apiEnabled == '1')
							result.carriers[i].apiEnabled = 1;
					}

					$scope.carriers = carriers;		
				}
			});
		//requesting all available accessorial list of carriers
		SyncCarriers.fetchAccessorial()
			.success(function(result){
				if(result.success){
					$scope.accessorials = result.accessorials;
				}
			});

		//requesting all products
		SyncCarriers.fetchItemTypes()
			.success(function(result){
				if(result.success){
					$scope.itemTypes = result.itemTypes;
				}
			});
	}
}