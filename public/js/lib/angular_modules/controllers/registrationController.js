function registrationController ($scope, UIService, $http){
	$scope.user = {};
	
	//model attribute to show/hide error message notification
	$scope.success = {
		status: false,
		message: '',
	};


	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	//model attribute to show/hide ajax loader
	$scope.processing = {
		status: false,
	};
	
	$scope.handleRegistration = function(form){
		if(form && form.$valid){
			//$scope.user._token = form._token;
			//$scope.user.subscription_plan_id = form.subscription_plan_id;
			
			//flag to enable loader for registration process
			$scope.processing.status = true;
			
			$http.post('/register', $scope.user)
				.success(function (data){
					if(data.success){
						$scope.success.status = true;
						$scope.success.message = data.success;

						//clearing the model
						$scope.user.reg_usrfirstname 
							= $scope.user.reg_usrlastname 
							= $scope.user.reg_usremail
							= $scope.user.reg_company 
							= $scope.user.reg_cno
							= $scope.user.captcha = '';
							
						if(data.captcha)
							$scope.user.captchasrc = data.captcha;

						//resetting to Pristine stage
						form.$setPristine();
						
						//disabling loader after complete
						$scope.processing.status = false;

						//calling factory to autohide the notification
						UIService.hideNotification();

						//after the autohide period making angular notification set to false again
						setTimeout(function(){
							$scope.success.status = false;
						}, 4000);
						
					}else if(data.error){
						$scope.error.status = true;
						$scope.error.message = data.error;

						//disabling loader after complete
						$scope.processing.status = false;

						//calling factory to autohide the notification
						UIService.hideNotification();
						
						//after the autohide period making angular notification set to false again
						setTimeout(function(){
							$scope.error.status = false;
						}, 4000);
						
					}

				})
				.error(function(data, status){
					console.log(status);
				});
		}else
			alert('The form has invalid data');
	}
	
	$scope.refreshCaptcha = function(){
		$http.get('/get-captcha')
			.success(function(data){
				if(data.captcha){
					$scope.user.captchasrc = data.captcha;
				}else{
					$scope.error.status = true;
					$scope.error.message = 'captcha Refresh Error';
					
					//calling factory to autohide the notification
					UIService.hideNotification();
					
					//after the autohide period making angular notification set to false again
					setTimeout(function(){
						$scope.error.status = false;
					}, 4000);
				}
			}).error(function(data, status){
				console.log(status);
			});
	}
}