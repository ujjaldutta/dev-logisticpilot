'use strict';

function CustomerManagerController(Customer, $scope, $window){
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	$scope.customerData = {count: 0, list: []};
	$scope.TotalCustomer = [];
	$scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
	$scope.recordLimit = 3;
	$scope.currentPage = 1;
	$scope.filters = [];
	$scope.filter = {};

	$scope.predicate = 'id';
    $scope.reverse = false;

    $scope.$watch('recordLimit', function(value, oldValue){
    	if(oldValue != value){
    		fetchCustomers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}
    });
	
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	   //alert("A");
	   fetchCustomers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	};
	

	$scope.pageChanged = function(){
		fetchCustomers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	}

	//intially fetching records of the current page i.e. page 1 with current conditions
	fetchCustomers({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
	

	//filtering customer result if any by watching sorting option changes
	$scope.$watch('filter.custName', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: value}, {location: Customer.processGoogleLocation($scope.filter.custLoc)}];
			fetchCustomers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	$scope.$watch('filter.custLoc', function(value, oldValue){
		//console.log(Customer.processGoogleLocation(value));
		if(angular.isObject(value)){
			$scope.filters = [{location: Customer.processGoogleLocation(value)}, {name: $scope.filter.custName}];

			fetchCustomers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}else if(oldValue !== undefined){
			$scope.filters = [{location: null}, {name: $scope.filter.custName}];
			fetchCustomers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	
	$scope.$watch('filter.custActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Customer.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			fetchCustomers({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}

	});

	$scope.$watch('filter.custInActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Customer.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			fetchCustomers({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}
	});

	$scope.resetFilter = function(type){
		switch(type){
			case 'userLoc':
				if($scope.filter.custLoc !== undefined){
					$scope.filter.custLoc = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: $scope.filter.custLoc}];
					fetchCustomers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
				break;
			case 'userName':
				if($scope.filter.custName !== undefined){
					$scope.filter.custName = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: Customer.processGoogleLocation($scope.filter.custLoc)}];
					fetchCustomers({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
				break;
		}
	}


	$scope.checkAll = function(){
		angular.forEach($scope.customerData.list, function(value, key){
			if($scope.customerData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){
		if($scope.customerData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.customerData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.customerData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.customerData.selectAll = true;

		}else
			$scope.customerData.selectAll = false;
	}

	$scope.deleteAllCustomer = function(){
		//scanning all items for checking
		if($window.confirm('Are you sure ?')){
			angular.forEach($scope.customerData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteCustomer(value.id, false);
				}
			});
		}	
	}

	$scope.deleteCustomer = function(custId, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Customer.makeAsyncRequest(Customer.deleteCustomer(custId));
		}else if(showAlert === false)
			promiseObj = Customer.makeAsyncRequest(Customer.deleteCustomer(custId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
					fetchCustomers({limit: $scope.recordLimit});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}

	function fetchCustomers(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		Customer.makeAsyncRequest(Customer.listCustomer(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.customerData.count = response.data.count;
					$scope.customerData.list = response.data.results ? response.data.results : [];
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

}

