'use strict';

function ShipmentInvoiceManagerController(Shipment, Carrier, Customer, CommonFunctions, $scope, $window, $compile, $parse, $uibModal){
	// pricefilter after search
	$scope.isSummaryView = false;
	$scope.statuscolors = ['booked','dispatched','transit','out-delivery','delivered','quote','waiting'];
	
	$scope.mapdistance = 0;
	$scope.colorclass = ['green', 'blue', 'red'];
	$scope.colorclasslist = [];
	$scope.getrandindex = function(){ 
		 return $scope.colorclass[Math.floor(Math.random()*$scope.colorclass.length)];
		};		
		
	
	$scope.animationsEnabled = true;
	$scope.modal_title = '';
  $scope.open = function (size) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'printmodal.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        },
        update_val: function () {
          return '';
        }
      }
    });	
	}
	
	
  $scope.openforemail = function (size) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: '/templates/emailmodal.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        },
        update_val: function () {
          return '';
        }
      }
    });	
	}	
	
	
  $scope.openfordelete = function (update_val) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'printmodaldelete.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        },
        update_val: function () {
          return '';
        }
      }
    });	
	}	
	
  $scope.openforupdate = function (update_val) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'printmodalupdate.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        },
        update_val: function () {
          return update_val;
        }		
      }
    });	
	}	
	
	
  $scope.openforInvoiceupdate = function (update_val) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'invoiceupdate.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        },
        update_val: function () {
          return update_val;
        }		
      }
    });	
	}	

  $scope.openforreload = function (size) {	
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'reloadlist.html',
	  backdrop: false,
      controller: 'ModalInstanceCtrl',
      /*size: size,*/
      resolve: {
        items: function () {
          return $scope;
        }
      }
    });	
	}	
	
	
	$scope.stateList = [];
    $scope.b2btypes = [];
	$scope.carrierList = [];
	$scope.modeTypes = [];
	$scope.equipTypes = [];
	$scope.customernotes = [];
	$scope.carriernotes = [];
	$scope.terminalnotes = [];
	$scope.clickedShipId = [];
	$scope.accessorials_cc = [];
	$scope.accessorials_crc = [];
	$scope.shipStatus = [];
	$scope.customerList = [];
	$scope.products = [];
	$scope.stops = [];
	
	$scope.cc_cost = [];
	$scope.accessorials_cc_ini = [];
	$scope.accessorials_crc_ini = [];
	$scope.accessorials_ccccr_deafult = [];
	
	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};	
	
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	$scope.shipmentData = {count: 0, list: [], shipnotes : []};
	$scope.totalShipmentCount = 0;
	$scope.TotalShipment = [];
	$scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
	$scope.recordLimit = 10;
	$scope.currentPage = 0;
	$scope.filters = [];
	$scope.filter = {};
	$scope.postStatusId = [];

	$scope.predicate = 'id';
    $scope.reverse = false;
    $scope.filter.shp_clientlist = [];
	$scope.filter.shp_carriertype = [];
	$scope.filter.shp_loadstatusId = [];
	$scope.filter.shp_equipmentId = [];
	
	getAllModeTypes();
	getAllEquipmentTypes();	
	getCarrierList();
	getStateList();
	
	
	//google autocomplete config to search specific type of information
	$scope.autocompleteOption = {
        componentRestrictions: { country: 'us' },
        types: ["(regions)"]
    }	
	
	$scope.$watch('clientID', function(v1, v2){
	  getCustomerList(); 
	  //fetchCustomers(angular.extend({orderBy: 'client_firstname'}, defaultParams), 0);
	});
   
   $scope.isBusy = false;	
   $scope.loadMore = function() {
   //alert($scope.isSummaryView);
   if(!$scope.isSummaryView){
    if($scope.isBusy === true)
	 return;
	
	$scope.isBusy = true;
    angular.element('#load_more').addClass('hidden');
	angular.element('.loading_more').removeClass('hidden');
    $scope.currentPage++;
	
	var params = {limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'};	
	
		Shipment.makeAsyncRequest(Shipment.listShipment(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					
					$scope.shipmentData.count = response.data.count;
					$scope.shipmentData.list = $scope.shipmentData.list.concat(response.data.results ? response.data.results : []);
					angular.element('#load_more').removeClass('hidden');
					angular.element('.loading_more').addClass('hidden');
					console.log(response.data.count);
					if($scope.shipmentData.count === 0){
					 $scope.isBusy = true;
					 return;
					}
					
					$scope.isBusy = false;
					
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
			 $scope.nextPageDisabledClass();
				//angular.element('.loading').addClass('hidden');
			});	
			
			}
	
   };	

	
	$scope.expandInfo = function(trid){
	  angular.element('#act'+trid).slideToggle( "slow", function(){
		  if(angular.element('#ico_'+trid).hasClass('glyphicon-plus')){
		   angular.element('#ico_'+trid).removeClass('glyphicon-plus');
		   angular.element('#ico_'+trid).addClass('glyphicon-minus');
		  } else {
		   angular.element('#ico_'+trid).removeClass('glyphicon-minus');
		   angular.element('#ico_'+trid).addClass('glyphicon-plus');	  
		  }
			
			if($scope.clickedShipId.indexOf(angular.element('#ship_id'+trid).val()) == -1){		
			 //getTabInfo(angular.element('#ship_id'+trid).val());
			 getShipmentData(angular.element('#ship_id'+trid).val());
			 $scope.clickedShipId.push(angular.element('#ship_id'+trid).val());
			 
			}	  
	  }); 

	}
			

	
    $scope.$watch('recordLimit', function(value, oldValue){
    	if(oldValue != value){
    		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}
    });
	
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	   //alert("A");
	   fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	};
	

	$scope.pageChanged = function(){
		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	}

	/*$scope.$watch('filters.shp_postatusId_booked', function(value, oldValue){ //alert('1');
    	if(oldValue != value){
		    $scope.filters = [{shp_postatusId: $scope.postStatusId} ];
    		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}	
	});
	
	$scope.$watch('filters.shp_postatusId_dispatched', function(value, oldValue){ //alert('2');
    	if(oldValue != value){
		    $scope.filters = [{shp_postatusId: $scope.postStatusId} ];
    		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}	
	});	*/
	
    $scope.insSettings = {selectionLimit: 1,
        enableSearch: true,
        showUncheckAll: false,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };	
	
       $scope.searchCustomerEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){ 
				    if(item.id){
				    $scope.filter.shp_clientlist.length = 0; 
				    $scope.filter.shp_clientlist.push(item.id);
					} else {
					$scope.filter.shp_clientlist.length = 0; 
					}
					$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist}, ];
					$scope.currentPage = 1;
					$scope.clickedShipId.length = 0;
					fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
							orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
             	});					
					
		     }
            }
        };	
	
       $scope.searchCarrierEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){ 
				    if(item.id){
				    $scope.filter.shp_carriertype.length = 0; 
				    $scope.filter.shp_carriertype.push(item.id);
					} else {
					$scope.filter.shp_carriertype.length = 0; 
					}
					$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist}, ];
					$scope.currentPage = 1;
					$scope.clickedShipId.length = 0;
					fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
							orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
             	    });					
					
		     }
            }
        };		
	
	
       $scope.searchLoadEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){ 
				    if(item.id){
				    $scope.filter.shp_loadstatusId.length = 0; 
				    $scope.filter.shp_loadstatusId.push(item.id);
					} else {
					$scope.filter.shp_loadstatusId.length = 0; 
					}
					$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist}, ];
					$scope.currentPage = 1;
					$scope.clickedShipId.length = 0;
					fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
							orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
             	    });					
					
		     }
            }
        };


       $scope.searchEquipmentEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){ 
				    if(item.id){
				    $scope.filter.shp_equipmentId.length = 0; 
				    $scope.filter.shp_equipmentId.push(item.id);
					} else {
					$scope.filter.shp_equipmentId.length = 0; 
					}
					$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist}, ];
					$scope.currentPage = 1;
					$scope.clickedShipId.length = 0;
					fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
							orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
             	    });					
					
		     }
            }
        };		
	
	
	$scope.checkStatus = function(liid){
	console.log(liid);
	$scope.totalShipmentCount = 0;
	$scope.currentPage = 1;
    $scope.clickedShipId = [];	
	// alert(angular.element('#li_'+liid).hasClass('.select-multi'));
	 if(!angular.element('#li_'+liid).hasClass('select-multi')){
	   angular.element('#li_'+liid).addClass('select-multi');
	   angular.element('#chk_'+liid).checked = true;
	   $scope.postStatusId.push(angular.element('#chk_'+liid).val());
	   }
	 else {  
	   angular.element('#li_'+liid).removeClass('select-multi');
	   angular.element('#chk_'+liid).checked = false;
	   var index = $scope.postStatusId.indexOf(angular.element('#chk_'+liid).val());
	   $scope.postStatusId.splice(index, 1);
	   }
	   
		//$scope.filters = [{shp_postatusId: $scope.postStatusId} ];
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
        fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});	
       }			
	   console.log($scope.postStatusId);
	}
	
	
	
	//intially fetching records of the current page i.e. page 1 with current conditions
	/*fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});*/
	

	//filtering Shipment result if any by watching sorting option changes
	getStatusType();
	$scope.$watch('filter.shipText', function(value, oldValue){
		if(value != oldValue){
		console.log($scope.filter.shp_clientlist);
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
        fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});	
			}
		}
	});
	$scope.$watch('filter.custLoc', function(value, oldValue){
		//console.log(Shipment.processGoogleLocation(value));
		if(angular.isObject(value)){
			$scope.filters = [{location: Shipment.processGoogleLocation(value)}, {name: $scope.filter.custName}];
            $scope.clickedShipId.length = 0;
			$scope.currentPage = 1;
			fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}else if(oldValue !== undefined){
			$scope.filters = [{location: null}, {name: $scope.filter.custName}];
			$scope.clickedShipId.length = 0;
			$scope.currentPage = 1;
			fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	
	$scope.$watch('filter.custActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			$scope.clickedShipId.length = 0;
			$scope.currentPage = 1;
			fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}

	});

	$scope.$watch('filter.custInActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}
	});

	$scope.$watch('filter.shp_carriertype', function(val1, val2){
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	   
		}
	  }
	});
	
	$scope.$watch('filter.shp_loadstatusId', function(val1, val2){
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});
        }		
	  }
	});	
	
	$scope.$watch('filter.shp_equipmentId', function(val1, val2){
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
	
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {	
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	
        }		
	  }
	});		
	
	$scope.$watch('filter.shp_clientlist', function(val1, val2){ 
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	 
        }		
	  }
	});	


	$scope.$watch('filter.shp_from_state', function(val1, val2){ 
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	
       }		
	  }
	});	


	$scope.$watch('filter.shp_to_state', function(val1, val2){ 
      if(val1 !== val2){
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		
		if($scope.isSummaryView){
		 $scope.summaryList();
		} else {		
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	
      }		
	  }
	});		
	
	
	
	$scope.resetFilter = function(type){
		switch(type){
			case 'userLoc':
				if($scope.filter.custLoc !== undefined){
					$scope.filter.custLoc = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: $scope.filter.custLoc}];
					$scope.clickedShipId.length = 0;
					$scope.currentPage = 1;
					fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
				break;
			case 'shipText':
				/*if($scope.filter.custName !== undefined){
					$scope.filter.custName = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}];
					fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}*/
				$scope.filter.shipText = null;
				//$scope.postStatusId = null;
				//$scope.shipStatusitem.shp_postatusId = null;
				$scope.filter.shp_carriertype = null;
				$scope.filter.shp_loadstatusId = null;
				$scope.filter.shp_equipmentId = null;
				$scope.filter.shp_clientlist = null;
				$scope.filter.shp_from_state = null;
				$scope.filter.shp_to_state = null;
				
               for(var id in $scope.postStatusId){
			     //alert($scope.postStatusId[id]);
			     //$scope.checkStatus($scope.postStatusId[id]);
				 
				 if(angular.element('#li_'+$scope.postStatusId[id]).hasClass('select-multi')){
				    angular.element('#li_'+$scope.postStatusId[id]).removeClass('select-multi');
				    angular.element('#chk_'+$scope.postStatusId[id]).checked = false;
				   }				 
			   }
				$scope.postStatusId = [];
				$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist}, ];
				$scope.clickedShipId.length = 0;
				$scope.currentPage = 1;
				
				if($scope.isSummaryView){
				 $scope.summaryList();
				} else {
				fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});	
                }				
				break;
		}
	}

	
	
	$scope.checkAll = function(){
		angular.forEach($scope.shipmentData.list, function(value, key){ //console.log(value);
			if($scope.shipmentData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){ 
		if($scope.shipmentData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.shipmentData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.shipmentData.selectAll = true;

		}else
			$scope.shipmentData.selectAll = false;
	}
   $scope.seletedShipList = []; 
   $scope.printAction = function(update_value){ //alert(action);
		//scanning all items for checking
		//if($window.confirm('Are you sure ?')){
		    $scope.seletedShipList.length = 0;
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					//$scope.printShipmentStatus(value.id, false, update_value);
					$scope.seletedShipList.push($scope.shipmentData.list[key]);
				}
			});
			
		   if($scope.seletedShipList.length){
				  if(update_value == 1){
				   $scope.modal_title = 'Bill Of Lading';
				  }
				  if(update_value == 2){
				   $scope.modal_title = 'Print Shipping Label';
				  }	
				  if(update_value == 3){
				   $scope.modal_title = 'Print Customer Quote';
				  }
				  if(update_value == 4){
				   $scope.modal_title = 'Print Pickup Confirmation';
				  }	
                  if($scope.seletedShipList.length === 1){
				   //console.log($scope.seletedShipList.pop(0).id); return;
				   
					$window.open(
					  '/front/shipments/ajax-print-shipment/'+$scope.seletedShipList.pop(0).id+'/'+$scope.modal_title,
					  '_blank' // <- This is what makes it open in a new window.
					);				   
				   
                  } else {				  
                  $scope.open();
                  }				  
			  }
		//}
   }		
   	
	$scope.printShipmentStatus = function(shipId, showAlert, value){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Shipment.makeAsyncRequest(Shipment.printUpdateStatus(value, shipId));
		}else if(showAlert === false)
			promiseObj = Shipment.makeAsyncRequest(Shipment.printUpdateStatus(value, shipId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
				    //getStatusType(); 
					//$scope.clickedShipId.length = 0;
					//fetchShipments({limit: $scope.recordLimit});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}	
	
	
   $scope.emailAction = function(update_value){ //alert(action);
		    $scope.seletedShipList.length = 0;
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					//$scope.printShipmentStatus(value.id, false, update_value);
					$scope.seletedShipList.push($scope.shipmentData.list[key]);
				}
			});
			
		   if($scope.seletedShipList.length){
				  if(update_value == 1){
				   $scope.modal_title = 'Bill Of Lading';
				  }
				  if(update_value == 2){
				   $scope.modal_title = 'Print Shipping Label';
				  }	
				  if(update_value == 3){
				   $scope.modal_title = 'Print Customer Quote';
				  }
				  if(update_value == 4){
				   $scope.modal_title = 'Print Pickup Confirmation';
				  }	
				  
                 $scope.openforemail();		  
			  }
   }	
	
	
	
	
	
   $scope.resolveAction = function(action, value){ //alert(action);
    if(action == 'delete'){ 
	  $scope.deleteAllShipment();
	}
	if(action == 'update'){ 
	 $scope.updateAllShipmentStatus(value);
	}
	
	if(action == 'invoice'){ 
	 $scope.invoiceShipment(value);
	}	
   }	
	
	
	$scope.invoicelist = [];
	$scope.invoiceShipment = function(update_value){
		//scanning all items for checking
		//if($window.confirm('Are you sure ?')){
			/*angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					$scope.updateInvoiceShipment(value.id, false);
					$scope.invoicelist.push(value.id); 
				}
			});*/
		//}
		
		$scope.openforInvoiceupdate(update_value);
	}	
	
	$scope.invreqcount = 1;
	$scope.updateInvoiceShipment = function(shipId, showAlert){
	    angular.element('.loading').removeClass('hidden');
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Shipment.makeAsyncRequest(Shipment.sendUpdateInvoiceStatus(shipId));
		}else if(showAlert === false)
			promiseObj = Shipment.makeAsyncRequest(Shipment.sendUpdateInvoiceStatus(shipId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){ 
				    /*getStatusType(); */
					$scope.clickedShipId.length = 0;
					$scope.currentPage = 1;
					fetchShipments({limit: $scope.recordLimit});
					/*if($scope.invoicelist.length == $scope.invreqcount){
					$window.location.href = '/front/shipmentinvoice/invoice';
                    }*/
                 $scope.invreqcount++;					
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}			
	
	$scope.updateAllShipmentStatus = function(update_value){
		//scanning all items for checking
		/*if($window.confirm('Are you sure ?')){
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					$scope.updateShipmentStatus(value.id, false, update_value);
				}
			});
		}*/	
		
		$scope.openforupdate(update_value);
	}

	$scope.updateShipmentStatus = function(shipId, showAlert, value){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Shipment.makeAsyncRequest(Shipment.sendUpdateStatus(value, shipId));
		}else if(showAlert === false)
			promiseObj = Shipment.makeAsyncRequest(Shipment.sendUpdateStatus(value, shipId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
				    getStatusType(); 
					$scope.clickedShipId.length = 0;
					$scope.currentPage = 1;
					fetchShipments({limit: $scope.recordLimit});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}


	
	$scope.deleteAllShipment = function(){
		//scanning all items for checking
		/*if($window.confirm('Are you sure ?')){
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteShipment(value.id, false);
				}
			});
		}*/	
		
		$scope.openfordelete();
	}

	$scope.deleteShipment = function(shipId, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Shipment.makeAsyncRequest(Shipment.deleteShipment(shipId));
		}else if(showAlert === false)
			promiseObj = Shipment.makeAsyncRequest(Shipment.deleteShipment(shipId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
				    getStatusType();
					$scope.clickedShipId.length = 0;
					$scope.currentPage = 1;
					fetchShipments({limit: $scope.recordLimit});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}

	$scope.savePartialShipmentData = function(form){ 
	// if(!form.$valid)
	  // throw new Error("Invalid Request");
	
	
	 console.log($scope);
	}
	
	$scope.summaryListData = [];
    $scope.summaryList = function(){
	    //$scope.resetFilter('shipText');
	    $scope.isSummaryView = true;
	    angular.element('.loading').removeClass('hidden');
		angular.element('.shiplist').hide(500);
		//alert($scope.filter.shipText);
				
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		
		var params = {limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'};	
		
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;		 
		
		Shipment.makeAsyncRequest(Shipment.getAllShipmentSummary(params))
				.then(function(response){
					if(response.data.success !== undefined && response.data.results !== undefined){
						$scope.summaryListData = response.data.results.shipment_summary;
					}
				}, function(reason){
					console.warn(reason);
		}).finally(function(){
		angular.element('.summarylist').show(500);
		 angular.element('.loading').addClass('hidden');
		});
	}
	
	$scope.fetchDataWithClient = function(clientid_val, carrier_val){ 
	    angular.element('.summarylist').hide(500);
		$scope.isSummaryView = false;
		if(carrier_val)
		$scope.filter.shp_carriertype = [carrier_val];
		else
		$scope.filter.shp_carriertype = [];
		
		$scope.filter.shp_clientlist = [clientid_val];
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});
        angular.element('.shiplist').show(500);		
	
	}

	$scope.viewAll = function(){
	 $scope.isSummaryView = false;
	 angular.element('.summarylist').hide(500);
	 //$scope.resetFilter('shipText');
		$scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
		$scope.clickedShipId.length = 0;
		$scope.currentPage = 1;
		fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: 1, filters: $scope.filters});
	 angular.element('.shiplist').show(500);	
	}
	
	function fetchShipments(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		Shipment.makeAsyncRequest(Shipment.listShipment(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.shipmentData.count = response.data.count;
					$scope.shipmentData.list = response.data.results ? response.data.results : [];
                    $scope.totalShipmentCount = $scope.shipmentData.count;
					
					if($scope.totalShipmentCount === 0){
					 $scope.isBusy = true;
					 return;
					} else {
					 $scope.isBusy = false;
					}
					//console.log($scope.shipmentData.list.shp_reqpickupdate);
					//$scope.shipmentData.list.shp_reqpickupdate = CommonFunctions.dformat($scope.shipmentData.list.shp_reqpickupdate.split(' ')[0]);
					
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				//angular.element('#script').after($compile('<loadjsfiles></loadjsfiles>')($scope));
				angular.element('.loading').addClass('hidden');
			});
	}
	
	
	function getShipmentData(shipID){
		if(!shipID)
			shipID = $location.search().shipID || 0;
		
		//making the loader visible for user
		angular.element('#loading_shipment'+shipID).removeClass('hidden');
		
		Shipment.makeAsyncRequest(Shipment.getShipmentData(shipID, 'list'))
			.then(function(response){
				if(response.data.success && response.data.shipmentdata){
					$scope.shipment = response.data.shipmentdata;
					
					$scope.customernotes = response.data.notedata_customer;
					$scope.carriernotes = response.data.notedata_carrier;
					$scope.terminalnotes = response.data.notedata_internal;	
                    $scope.accessorials_cc = response.data.accessorials_cc;	
                    $scope.accessorials_crc = response.data.accessorials_crc;
					$scope.accessorials_cc_ini = response.data.accessorials_cc_ini;
					$scope.accessorials_crc_ini = response.data.accessorials_crc_ini;					
					$scope.accessorials_ccccr_deafult = response.data.accessorials_ccccr_deafult;
					$scope.products = response.data.productdata;
                    $scope.stops = response.data.stops;
                    $scope.b2btypes = response.data.b2btypes;					
					
					for(var t=0;t<$scope.b2btypes.length;t++){
					  $scope.colorclasslist[t] = $scope.getrandindex();
					}
					
					$scope.shipment.pick_location = __fillLocationDetails($scope.shipment, 'pickup');
					$scope.shipment.del_location = __fillLocationDetails($scope.shipment, 'del');
					$scope.shipment.billto_location = __fillLocationDetails($scope.shipment, 'billto');					
					var origin_addr = $scope.shipment.shp_pickupcity+', '+$scope.shipment.shp_pickupstate+', '+$scope.shipment.shp_pickuppostal+', '+$scope.shipment.shp_pickupcountry; 

					var des_addr = $scope.shipment.shp_deliverycity+', '+$scope.shipment.shp_deliverystate+', '+$scope.shipment.shp_deliverypostal+', '+$scope.shipment.shp_deliverycountry; 					
					
					//console.log(response.data.accessorials_cc);
					
					//angular.element('#s_d_loc'+shipID).html($compile('<source-location picklocation="'+origin_addr+'" autofillpick="'+$scope.shipment.shp_pickupname+'" id="'+$scope.shipment.id+'" autofilldel="'+$scope.shipment.shp_deliveryname+'" dellocation="'+des_addr+'" elmshipid="'+$scope.shipment.id+'" phone="'+$scope.shipment.client_phone+'" shppickphone="'+$scope.shipment.shp_pickupphone+'" shppickcontact="'+$scope.shipment.shp_pickupcontact+'" shpdelphone="'+$scope.shipment.shp_deliveryphone+'" shpdelcontact="'+$scope.shipment.shp_deliverycontact+'" pickaddr1="'+$scope.shipment.shp_pickupadr1+'" pickaddr2="'+$scope.shipment.shp_pickupadr2+'" deladdr1="'+$scope.shipment.shp_deliveryadr1+'" deladdr2="'+$scope.shipment.shp_deliveryadr2+'"></source-location>')($scope));
					
					//angular.element('#direction_'+shipID).html($compile('<gmap-direction origin="'+origin_addr+'" destination="'+des_addr+'" shipidval="'+shipID+'"></gmap-direction>')($scope));
					angular.element('#direct_in_'+shipID).html($compile('<parent-dir></parent-dir>')($scope));
					/*$scope.shpproducts = response.data.productdata;
					$scope.accessorialsModel = response.data.accessorials;
					$scope.stops = response.data.stops;
					$scope.suggestedHeadingslist = response.data.suggestedHeadings; 
					
					
					$scope.shipment.shp_pickuptimefrom = CommonFunctions.tConvert($scope.shipment.shp_pickuptimefrom);
					$scope.shipment.shp_pickuptimeto = CommonFunctions.tConvert($scope.shipment.shp_pickuptimeto);
					$scope.shipment.shp_deliverytimefrom = CommonFunctions.tConvert($scope.shipment.shp_deliverytimefrom);
					$scope.shipment.shp_deliverytimeto = CommonFunctions.tConvert($scope.shipment.shp_deliverytimeto);
					
					$scope.shipment.shp_reqpickupdate = CommonFunctions.dformat($scope.shipment.shp_reqpickupdate.split(' ')[0]);
					$scope.shipment.shp_expdeliverydate = CommonFunctions.dformat($scope.shipment.shp_expdeliverydate.split(' ')[0]);
					
					
					  if($scope.shipment.shp_loadstatusId == '2'){
						$scope.isTruckLoad = true;
					  } else {
						$scope.isTruckLoad = false;
					  }		*/				

				}
				//console.log(response);
			}, function(reason){
				console.warn(reason);
			})
			//callback for billing
			.then(function(response){
				if(response && response.data.success && response.data.defaultSettings.default_settings){

				} 
			}, function (reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('#direct_in_'+shipID+' parent-dir').html($compile('<tabinfo id="'+shipID+'"></tabinfo>')($scope)); 
				//angular.element('.loading_shipment').addClass('hidden');
			});
	}	
	
	function __fillLocationDetails(collection, sec){
	var location = '';
     if(sec == 'pickup'){
		/*var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		*/
		console.log(collection);
		if(collection.shp_pickupcity !== undefined && collection.shp_pickupcity)
			location += collection.shp_pickupcity + ', ';
		if(collection.shp_pickupstate !== undefined && collection.shp_pickupstate)
			location += collection.shp_pickupstate + ', ';
		if(collection.shp_pickuppostal !== undefined && collection.shp_pickuppostal)
			location += collection.shp_pickuppostal + ', ';
		if(collection.shp_pickupcountry !== undefined && collection.shp_pickupcountry)
			location += collection.shp_pickupcountry;
     
	 }
	  
     if(sec == 'del'){
		/*var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		*/
		console.log(collection);
		if(collection.shp_deliverycity !== undefined && collection.shp_deliverycity)
			location += collection.shp_deliverycity + ', ';
		if(collection.shp_deliverystate !== undefined && collection.shp_deliverystate)
			location += collection.shp_deliverystate + ', ';
		if(collection.shp_deliverypostal !== undefined && collection.shp_deliverypostal)
			location += collection.shp_deliverypostal + ', ';
		if(collection.shp_deliverycountry !== undefined && collection.shp_deliverycountry)
			location += collection.shp_deliverycountry;
      }

     if(sec == 'billto'){
		/*var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		*/
		console.log(collection);
		if(collection.shp_carrierbilltocity !== undefined && collection.shp_carrierbilltocity)
			location += collection.shp_carrierbilltocity + ', ';
		if(collection.shp_carrierbilltostate !== undefined && collection.shp_carrierbilltostate)
			location += collection.shp_carrierbilltostate + ', ';
		if(collection.shp_carrierbilltopostal !== undefined && collection.shp_carrierbilltopostal)
			location += collection.shp_carrierbilltopostal + ', ';
		if(collection.shp_carrierbilltocountry !== undefined && collection.shp_carrierbilltocountry)
			location += collection.shp_carrierbilltocountry;
      }	  
		return location;
	}	
	
	
	function getStatusType(){
		Shipment.makeAsyncRequest(Shipment.getAjaxShipStatus())
				.then(function(response){
					if(response.data.success !== undefined && response.data.shipstatus !== undefined){
						$scope.shipStatus = response.data.shipstatus;
					}
				}, function(reason){
					console.warn(reason);
		});	
	}
	
	
    function getAllEquipmentTypes(){
        Carrier.makeAsyncRequest(Carrier.getAllEquipmentTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.equipmentTypes !== undefined) {
                        $scope.equipTypes = response.data.equipmentTypes;
						//alert($scope.equipTypes);
                    }
                }, function (reason) {
                    console.warn(reason);
                });	  
	}  

    function getAllModeTypes(){
        Carrier.makeAsyncRequest(Carrier.getAllModesTypes())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.modestype !== undefined) {
                        $scope.modeTypes = response.data.modestype;
                    }
                }, function (reason) {
                    console.warn(reason);
                }); 
	} 	
	
    function getCarrierList(){
        Carrier.makeAsyncRequest(Carrier.getAllCarriers())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.carriers !== undefined) {
                        $scope.carrierList = response.data.carriers;
                    }
                }, function (reason) {
                    console.warn(reason);
                }); 
	} 	


    function getStateList(){
		Shipment.makeAsyncRequest(Shipment.getAllState())
				.then(function(response){
					if(response.data.success !== undefined && response.data.state_list !== undefined){
						$scope.stateList = response.data.state_list;
					}
				}, function(reason){
					console.warn(reason);
		});
	}	
	
	
    function getCustomerList(){
	//console.log($scope.login_user_name);
        Customer.makeAsyncRequest(Customer.getAllCustomerList())
                .then(function (response) {
                    if (response.data.success !== undefined && response.data.clientlist !== undefined) {
                        $scope.customerList = response.data.clientlist;
                    }
                }, function (reason) {
                    console.warn(reason);
                }); 
				
				
			/*Customer.makeAsyncRequest(Customer.listCustomer(angular.extend({orderBy: 'client_firstname'})))
				.then(function(response){
					if(response.data.success && response.data.count !== undefined && response.data.results){
						//$scope.customerData.count = response.data.count;
						//$scope.customerData.list = response.data.results ? response.data.results : [];
						$scope.customerList = response.data.results ? response.data.results : [];
					}
					//console.log(result);
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
				});	*/			
				
	} 	
	


}

