'use strict';

function CommonAppController($scope, $window){
	//callback to the auto complete to search child customers
	$scope.customerSelected = function(selected) {
		console.log(selected);
		
		if(selected.originalObject.clientID !== undefined && selected.originalObject.clientID)
     		$window.location.href='/front/set-customer/' + selected.originalObject.clientID;
    }

    $scope.resetLoggedInClient = function(){
    	$window.location.href='/front/reset-customer/';
    }
}