'use strict';

function UserRegistrationController(UserAPIEndpoint, User, $scope, $location, $window){
	$scope.UserAPIEndpoint = UserAPIEndpoint;
	$scope.buttonMailText = 'Mail Password';
	
	// pricefilter after search
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	//model attribute to show/hide error message notification
	$scope.error = {
		status: false,
		message: '',
	};

	$scope.success = {
		status: false,
		message: '',
	};

	//google autocomplete config to search specific type of information
	$scope.autocompleteOption = {
        componentRestrictions: {  },
        types: ["(regions)"]
    }

    //collection  of user type objects
	$scope.clientID = null;
	$scope.userID = null;
	$scope.userData = {};
	$scope.userLayoutSettings = {};
	$scope.userTypes = [];
	$scope.layoutTypes = [];
	$scope.layoutMappings = [];
	$scope.selectedClient= null;
	$scope.selectedClients = [];
	$scope.clientMappings = [];
	$scope.screenFilter = [];
	$scope.screenFilters = [];
	$scope.screenAttributes = [];
	$scope.screenConstraintOperators = [];


	$scope.$watch('clientID', function(value){
		if(!isNaN(value) && value){
			//fetching languages
			getUserTypes();
		}
	});

	$scope.$watch('userID', function(value){
		if(!isNaN(value) && value){
			//fetching user details only when editing
			if(!$scope.newUser)
				getUserDetails(value);
			
			getClientMappings(value);

			getLayoutTypes();

			getAllLayoutMappings(value);

			getAllScreenFilters(value);
		}
	});
	
	$scope.$watch('screenFilter.layoutId', function(value){
		if(value && !isNaN(value)){
			User.makeAsyncRequest(User.getDisplayAttributes(value))
				.then(function(response){
					if(response !== undefined && response.data && response.data instanceof Array){
						$scope.screenAttributes = response.data;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				});
		}
	});

	$scope.$watch('screenFilter.attributeId', function(value){
		if(value && !isNaN(value)){
			User.makeAsyncRequest(User.getConstraintOperators())
				.then(function(response){
					if(response !== undefined && response.data.constraintOperators){
						$scope.screenConstraintOperators = response.data.constraintOperators;
					}
					
				}, function(reason){
					console.log(reason);
				});
		}
	});

	/* method to dismiss the alert */
	$scope.hideMessage = function(){
		$scope.error = {
			status: false,
			message: '',
		};

		$scope.success = {
			status: false,
			message: '',
		};
	}

	$scope.cancelEditing = function(){
		$window.location.href = UserAPIEndpoint.base ? UserAPIEndpoint.base : '/';
	}
	
	$scope.saveRegistredData = function(form){
		
		if(form && form.$valid){
			//making the loader visible for user
			//angular.element('.loading').removeClass('hidden');
			
			User.saveUser($scope.clientID, $scope.userID, $scope.userData)
				.then(function(response){
					if(response.data.success && response.data.userID){
						//$scope.userData.userID = response.data.userID;
						$scope.userID = response.data.userID;
						$scope.success.status = true;
						$scope.success.message = response.data.success;

						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error !== undefined){
						$scope.success.status = false;
						$scope.success.message = '';

						$scope.error.status = true;
						if(response.data.error !== undefined && response.data.error instanceof Object){
							$scope.error.message = User.formatValidationErrors(response.data.error);
						}else
							$scope.error.message = response.data.error;
					}
					//console.log(response);
				}, function(reason){
					console.log(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
		}
	}

	$scope.addToExsitingClientList = function(selectedObj, all){
		if(all === undefined)
			all = false;

		if(all){
			if(!$window.confirm('Are you sure to add all customers at once to this user?'))
				return
		}
		
		if(selectedObj && selectedObj.originalObject.id){
			$scope.selectedClients.push(selectedObj.originalObject.id);
		}
		
		if($scope.userID){
			User.makeAsyncRequest(User.syncClientMapping($scope.selectedClients, $scope.userID, all))
			.then(function(response){
				if(response.data.success){
					getClientMappings($scope.userID);
					$scope.selectedClient = null;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
		}
			
	}

	$scope.removeMapping = function(clientID){
		if(!$window.confirm('Are you sure to delete?'))
			return;

		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');

		User.makeAsyncRequest(User.detachClientMapping(clientID, $scope.userID))
			.then(function(response){
				if(response.data.success){
					getClientMappings($scope.userID);
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}
	
	$scope.removeLayoutMapping = function(layoutTypeId, userId){
		if(!$window.confirm('Are you sure to delete?'))
			return;
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		User.makeAsyncRequest(User.detachLayoutMapping(layoutTypeId, userId))
			.then(function(response){
				if(response.data.success){
					getAllLayoutMappings($scope.userID);
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}
	
	$scope.removeScreenFiltering = function(layoutId, id){
			if(!$window.confirm('Are you sure to delete?'))
				return;
			//making the loader visible for user
			angular.element('.loading').removeClass('hidden');

			User.makeAsyncRequest(User.detachScreenFilter(layoutId, id))
				.then(function(response){
					if(response.data.success){
						getAllScreenFilters($scope.userID);
					}
					//console.log(result);
				}, function(reason){
					console.warn(reason);
				})
				.finally(function(data){
					//making the loader invisible for user
					angular.element('.loading').addClass('hidden');
				});
	}
	
	$scope.saveScreenFilter = function(){
		User.makeAsyncRequest(User.saveScreenFilter(angular.extend({userId: $scope.userID}, $scope.screenFilter)))
			.then(function(response){
				if(response.data.success){
					$scope.screenFilter = [];
					getAllScreenFilters($scope.userID);
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			})
			.finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});	
	}

	function getUserTypes(){
		User.makeAsyncRequest(User.getuserTypes())
			.then(function(response){
				if(response.data.success !== undefined && response.data.userTypes !== undefined){
					$scope.userTypes = response.data.userTypes;
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	function getLayoutTypes(){
		User.makeAsyncRequest(User.getAllLayoutTypes())
			.then(function(response){
				if(response.data.success && response.data.layoutTypes){
					$scope.layoutTypes = response.data.layoutTypes;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			});
	}

	function getUserDetails(userID){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		
		User.makeAsyncRequest(User.getUserData(userID))
			.then(function(response){
				if(response.data.success && response.data.userDetails){
					$scope.userData = response.data.userDetails;
					//filling the location input with the already selected location details but saved separately
					$scope.userData.usr_location = __fillLocationDetails($scope.userData, 'usr');
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	function getAllLayoutMappings(userID){
		User.makeAsyncRequest(User.getAllLayoutMappings(userID))
			.then(function(response){
				if(response.data.success && response.data.layoutMappings){
					$scope.layoutMappings = response.data.layoutMappings;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			});
	}
	
	function getAllScreenFilters(userID){
		User.makeAsyncRequest(User.getAllScreenFilters(userID))
			.then(function(response){
				if(response.data.success && response.data.screenFilters){
					$scope.screenFilters = response.data.screenFilters;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			});
	}
	
	function getClientMappings(userID){
		//making the loader invisible for user
		angular.element('.loading').addClass('hidden');
		
		User.makeAsyncRequest(User.getClientMapping($scope.userID))
			.then(function(response){
				if(response.data.success && response.data.userMappings){
					$scope.clientMappings = response.data.userMappings;
					
					//iterating mapped client id to filter future search result
					var selectedClients = [];
					angular.forEach($scope.clientMappings, function(value, key){
						if(value.id !== undefined && value.id)
							selectedClients.push(value.id);
					});

					$scope.selectedClients = selectedClients;
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				angular.element('.loading').addClass('hidden');
			});
	}

	function __fillLocationDetails(collection, prf){

		var location = '';
		var city = prf + '_city';
		var state = prf + '_state';
		var postal = prf + '_postal';
		var country = prf + '_country';
		
		if(collection[city] !== undefined && collection[city])
			location += collection[city] + ', ';
		if(collection[state] !== undefined && collection[state])
			location += collection[state] + ', ';
		if(collection[postal] !== undefined && collection[postal])
			location += collection[postal] + ', ';
		if(collection[country] !== undefined && collection[country])
			location += collection[country];

		return location;
	}
	
	$scope.sendEmail = function(){
		$scope.buttonMailText = 'Please wait...';
		var email = $scope.userData.email;
		var pass = $scope.userData.password;		
		//alert(email+'--'+pass);
		//return false;
		User.makeAsyncRequest(User.sendPassword(email, pass))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			})
			
		
	}
	
	$scope.updatePassword = function(){		
		if(!$window.confirm('Are you sure to reset the passsword?'))
			return;

		$scope.buttonMailText = 'Please wait...';
		var email = $scope.userData.email;	
		var userId = $scope.userId;
		//alert(email+'---'+userId);
		//return false;
		User.makeAsyncRequest(User.sendUpdatePassword(email,userId))
			.then(function(response){
				if(response.data !== undefined){
					$scope.buttonMailText = 'Mail Password';

					if(response.data.success){
						$scope.success.status = true;
						$scope.success.message = response.data.success;
						$scope.error.status = false;
						$scope.error.message = '';
					}else if(response.data.error){
						$scope.success.status = false;
						$scope.success.message = '';
						$scope.error.status = true;
						$scope.error.message = response.data.error;
					}
				}
			}, function(reason){
				console.warn(reason);
			});
	}

	$scope.loadTags = function(query){
		//console.log(query);
		return User.makeAsyncRequest(User.getDisplayAttributes($scope.userLayoutSettings.layoutId))
	}

	//monitoring layout change using dropdown or other to save the unsaved changes
	$scope.$watch('userLayoutSettings.layoutId', function(newValue, oldValue){
		if(oldValue !== undefined && oldValue)
			$scope.saveCurrentLayoutSettings(oldValue);
	});

	$scope.saveCurrentLayoutSettings = function(layoutId){
		//firstly checking for data added/edited to save, after that fetching the attributes for the new one
		if(layoutId !== undefined && $scope.userLayoutSettings.layoutDetails !== undefined && $scope.userLayoutSettings.layoutDetails.length){
			User.makeAsyncRequest(User.saveLayoutAttributeAssignment($scope.userID, layoutId, {selectedAttributes: $scope.userLayoutSettings.layoutDetails}))
				.then(function(response){
					if(response.data !== undefined && response.data.success){
						return User.loadAttributes($scope.userID, $scope.userLayoutSettings.layoutId)
					}
				}, function(reason){
					console.warn(reason);
				}).then(function(response){
					if(response !== undefined && response.data.layoutAttributes){
						$scope.userLayoutSettings.layoutDetails = response.data.layoutAttributes;
						getAllLayoutMappings($scope.userID);
					}
				}, function(reason){
					console.warn(reason);
				});
		}else{
			User.makeAsyncRequest(User.loadAttributes($scope.userID, $scope.userLayoutSettings.layoutId))
				.then(function(response){
					if(response !== undefined && response.data.layoutAttributes){
						$scope.userLayoutSettings.layoutDetails = response.data.layoutAttributes;
						getAllLayoutMappings($scope.userID);
					}
				}, function(reason){
					console.warn(reason);
				});
		}
	}
}