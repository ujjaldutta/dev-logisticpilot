'use strict';

function CustomerProductManagerController(CustomerProduct, $scope, $window) {

    $scope.customerProductData = {count: 0, list: []};
    $scope.TotalCustomerProducts = [];
    //$scope.pageNos = [{id: 3, val: 3}, {id: 5, val: 5}, {id: 10, val: 10}, {id: 15, val: 15}, {id: 20, val: 20}, {id: 25, val: 25}, ];
    $scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
    $scope.recordLimit = 3;
    $scope.currentPage = 1;
    $scope.filters = [];
    $scope.filter = {};
    //$scope.classTypes  = [];
    
    getClassTypes();
     // Settings combox
    $scope.filter.classId = {};
    $scope.filter.classTypes = [];
    $scope.classSettings = {selectionLimit: 1, 
                            enableSearch: true, 
                            showUncheckAll: false,
                            smartButtonMaxItems: 1,
                            smartButtonTextConverter: function(itemText, originalItem) {
                                return itemText; }
                           };

    $scope.predicate = 'id';
    $scope.reverse = false;

    $scope.$watch('recordLimit', function (value, oldValue) {
        if (oldValue != value) {
            fetchProducts({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });

    $scope.sortColumn = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
        //alert("A");
        fetchProducts({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
            orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
        });
    };


    $scope.pageChanged = function () {
        fetchProducts({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage,
            orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
        });
    }
    
    //intially fetching records of the current page i.e. page 1 with current conditions
    fetchProducts({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});


    //filtering customer result if any by watching sorting option changes
    $scope.$watch('filter.itemName', function (value, oldValue) {
        if (value != oldValue) {
            $scope.filters = [{name: value}, {nmfc: $scope.filter.itemNMFC}, {classTypes: CustomerProduct.processedComboBox($scope.filter.classId)}];
            fetchProducts({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });
    
    $scope.$watch('filter.itemNMFC', function (value, oldValue) {
        if (value != oldValue) {
            $scope.filters = [{name: $scope.filter.itemName}, {nmfc: value}, {classTypes: CustomerProduct.processedComboBox($scope.filter.classId)}];
            fetchProducts({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });
        }
    });
    
    $scope.searchClassEvent = {
            onItemSelect: function(item) {
                if(item !== undefined){
                    $scope.filters = [{name: $scope.filter.itemName}, {nmfc:  $scope.filter.itemNMFC}, {classTypes: CustomerProduct.processedComboBox($scope.filter.classId)}];
                    fetchProducts({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                    orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
            });    
                }
            }                     
        };

    $scope.resetFilter = function (type) {
        switch (type) {
            case 'itemName':
                if ($scope.filter.itemName !== undefined) {
                    $scope.filter.itemName = null;
                    $scope.filters = [{name: $scope.filter.itemName}, {nmfc: $scope.filter.itemNMFC}, {classTypes: CustomerProduct.processedComboBox($scope.filter.classId)}];
                    fetchProducts({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                        orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
                    });
                }
                break;
            case 'itemNMFC':
                if ($scope.filter.itemNMFC !== undefined) {
                    $scope.filter.itemNMFC = null;
                    $scope.filters = [{name: $scope.filter.itemName}, {nmfc: $scope.filter.itemNMFC}, {classTypes: CustomerProduct.processedComboBox(item)}];
                    fetchProducts({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
                        orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
                    });
                }
                break;
        }
    }


    $scope.checkAll = function () {
        angular.forEach($scope.customerProductData.list, function (value, key) {
            if ($scope.customerProductData.selectAll === true)
                value.selected = true;
            else
                value.selected = false;
        });
    }

    $scope.checkSelection = function (index) {
        if ($scope.customerProductData.list[index].selected === true) {
            //now checking whether

            //total no of items on this current page
            var totalItems = $scope.customerProductData.list.length,
                    selectedItems = 0;

            //scanning all items for checking
            angular.forEach($scope.customerProductData.list, function (value, key) {
                if (value.selected === true)
                    selectedItems++;
            });

            if (totalItems == selectedItems)
                $scope.customerProductData.selectAll = true;

        } else
            $scope.customerProductData.selectAll = false;
    }

    $scope.deleteAllCustomerProducts = function () {
        //scanning all items for checking
        if ($window.confirm('Are you sure ?')) {
            angular.forEach($scope.customerProductData.list, function (value, key) {
                if (value.selected === true) {
                    $scope.deleteCustomerProduct(value.id, false);
                }
            });
        }
    }

    $scope.deleteCustomerProduct = function (id, showAlert) {
        var promiseObj = null;

        if (showAlert === true && $window.confirm('Are you sure to delete?')) {
            promiseObj = CustomerProduct.makeAsyncRequest(CustomerProduct.deleteProduct(id));
        } else if (showAlert === false)
            promiseObj = CustomerProduct.makeAsyncRequest(CustomerProduct.deleteProduct(id));

        //processing current list after successfull delete
        if (promiseObj) {
            promiseObj.then(function (response) {
                //console.log(response);
                if (response.data.success) {
                    //fetchProducts({limit: $scope.recordLimit});
                    fetchProducts({limit: $scope.recordLimit, filters: $scope.filters,
                        orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
                    });
                }
            }, function (reason) {
                console.warn(reason);
            });
        }
    }
    
    function getClassTypes() {
        CustomerProduct.makeAsyncRequest(CustomerProduct.getClassTypes())
                .then(function (response) {
                    if (response.data.success && response.data.classTypes) {
                        $scope.filter.classTypes = response.data.classTypes;
                    }
                }, function (reason) {
                    console.warn(reason);
                });
    }

    function fetchProducts(params) {
        //making the loader visible for user
        angular.element('.loading').removeClass('hidden');
        CustomerProduct.makeAsyncRequest(CustomerProduct.listProducts(params))
                .then(function (response) {
                    if (response.data.success && response.data.count !== undefined && response.data.results) {
                        $scope.customerProductData.count = response.data.count;
                        $scope.customerProductData.list = response.data.results ? response.data.results : [];
                    }
                    //console.log(result);
                }, function (reason) {
                    console.warn(reason);
                }).finally(function (data) {
            //making the loader invisible for user
            angular.element('.loading').addClass('hidden');
        });
    }

}

