'use strict';

function ShipmentManagerController(Shipment, CommonFunctions, $scope, $window, $compile, $parse){
	// pricefilter after search
	$scope.customernotes = [];
	$scope.carriernotes = [];
	$scope.terminalnotes = [];
	$scope.clickedShipId = [];
	
	$scope.cc_cost = [];
	
	$scope.priceFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};
	
	// transit filter after search
	$scope.transitFilter = {
	    min: 10,
	    max: 180,
	    ceil: 500,
	    floor: 0
	};

	$scope.shipmentData = {count: 0, list: [], shipnotes : []};
	$scope.totalShipmentCount = 0;
	$scope.TotalShipment = [];
	$scope.pageNos = [{id: 3, val: '3 Per Page'}, {id: 5, val: '5 Per Page'}, {id: 10, val: '10 Per Page'}, /* {id: 15, val: '15 per Page'}, {id: 20, val: '20 Per Page'}, */ {id: 25, val: '25 Per Page'}, ];
	$scope.recordLimit = 2;
	$scope.currentPage = 1;
	$scope.filters = [];
	$scope.filter = {};

	$scope.predicate = 'id';
    $scope.reverse = false;

	
	$scope.nextPageDisabledClass = function() { //console.log($scope.currentPage); console.log($scope.pageCount());
	  return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	};

	$scope.pageCount = function() {
	  return Math.ceil($scope.totalShipmentCount/$scope.recordLimit);
	};	
	
	
   $scope.loadMore = function() {
    angular.element('#load_more').addClass('hidden');
	angular.element('.loading').removeClass('hidden');
    $scope.currentPage++;
	
	var params = {limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'};	
	
		Shipment.makeAsyncRequest(Shipment.listShipment(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					//$scope.newShipmentData.count = response.data.count;
					//$scope.newShipmentData.list = response.data.results ? response.data.results : [];	
					
					$scope.shipmentData.count = response.data.count;
					$scope.shipmentData.list = $scope.shipmentData.list.concat(response.data.results ? response.data.results : []);
					angular.element('#load_more').removeClass('hidden');
					angular.element('.loading').addClass('hidden');
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//angular.element('.loading').addClass('hidden');
			});	
	
    //var newItems = Item.get($scope.currentPage*$scope.itemsPerPage, $scope.itemsPerPage);
    //$scope.shipmentData = $scope.shipmentData.concat(newItems);
   };	
	
	
	$scope.expandInfo = function(trid){
	  angular.element('#act'+trid).slideToggle( "slow" ); 
	  $('#horizontalTab'+trid).easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion           
			width: 'auto', //auto or any width like 600px
			fit: true,   // 100% fit in a container
			closed: 'accordion', // Start closed if in accordion view
			activate: function(event) { // Callback function if tab is switched
				var $tab = $(this);
				var $info = $('#tabInfo');
				var $name = $('span', $info);
				$name.text($tab.text());
				$info.show();
			}
		});
		
        if($scope.clickedShipId.indexOf(angular.element('#ship_id'+trid).val()) == -1){		
		 getTabInfo(angular.element('#ship_id'+trid).val());
		 $scope.clickedShipId.push(angular.element('#ship_id'+trid).val());
		}
	}
	
    $scope.$watch('recordLimit', function(value, oldValue){
    	if(oldValue != value){
    		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
    	}
    });
	
	$scope.sortColumn = function(predicate) {
	   $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	   $scope.predicate = predicate;
	   //alert("A");
	   fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	};
	

	$scope.pageChanged = function(){
		fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, 
			orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
		});
	}

	//intially fetching records of the current page i.e. page 1 with current conditions
	fetchShipments({filters: $scope.filters, limit: $scope.recordLimit, page: $scope.currentPage, orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'});
	

	//filtering Shipment result if any by watching sorting option changes
	$scope.$watch('filter.custName', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: value}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}];
			fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	$scope.$watch('filter.custLoc', function(value, oldValue){
		//console.log(Shipment.processGoogleLocation(value));
		if(angular.isObject(value)){
			$scope.filters = [{location: Shipment.processGoogleLocation(value)}, {name: $scope.filter.custName}];

			fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}else if(oldValue !== undefined){
			$scope.filters = [{location: null}, {name: $scope.filter.custName}];
			fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			});
		}
	});
	
	$scope.$watch('filter.custActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}

	});

	$scope.$watch('filter.custInActive', function(value, oldValue){
		if(value != oldValue){
			$scope.filters = [{name: $scope.filter.userName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}, {status: {active: $scope.filter.custActive, inactive: $scope.filter.custInActive}}];
			fetchShipments({clientID: $scope.clinetID, limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters});
		}
	});

	$scope.resetFilter = function(type){
		switch(type){
			case 'userLoc':
				if($scope.filter.custLoc !== undefined){
					$scope.filter.custLoc = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: $scope.filter.custLoc}];
					fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters,
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
				break;
			case 'userName':
				if($scope.filter.custName !== undefined){
					$scope.filter.custName = null;
					$scope.filters = [{name: $scope.filter.custName}, {location: Shipment.processGoogleLocation($scope.filter.custLoc)}];
					fetchShipments({limit: $scope.recordLimit, page: $scope.currentPage, filters: $scope.filters, 
						orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
					});
				}
				break;
		}
	}


	$scope.checkAll = function(){
		angular.forEach($scope.shipmentData.list, function(value, key){ //console.log(value);
			if($scope.shipmentData.selectAll === true)
				value.selected = true;
			else
				value.selected = false;
		});
	}

	$scope.checkSelection = function(index){ 
		if($scope.shipmentData.list[index].selected === true){
			//now checking whether

			//total no of items on this current page
			var totalItems = $scope.shipmentData.list.length,
				selectedItems = 0;

			//scanning all items for checking
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true)
					selectedItems ++;
			});

			if(totalItems == selectedItems)
				$scope.shipmentData.selectAll = true;

		}else
			$scope.shipmentData.selectAll = false;
	}

	$scope.deleteAllShipment = function(){
		//scanning all items for checking
		if($window.confirm('Are you sure ?')){
			angular.forEach($scope.shipmentData.list, function(value, key){
				if(value.selected === true){
					$scope.deleteShipment(value.id, false);
				}
			});
		}	
	}

	$scope.deleteShipment = function(custId, showAlert){
		var promiseObj = null;
		
		if(showAlert === true && $window.confirm('Are you sure to delete?')){
			promiseObj = Shipment.makeAsyncRequest(Shipment.deleteShipment(custId));
		}else if(showAlert === false)
			promiseObj = Shipment.makeAsyncRequest(Shipment.deleteShipment(custId));

		//processing current list after successfull delete
		if(promiseObj){
			promiseObj.then(function(response){
				//console.log(response);
				if(response.data.success){
					fetchShipments({limit: $scope.recordLimit});
				}
			}, function(reason){
				console.warn(reason);
			});
		}
	}

	$scope.savePartialShipmentData = function(form){ 
	// if(!form.$valid)
	  // throw new Error("Invalid Request");
	
	
	 console.log($scope);
	}
	
	function fetchShipments(params){
		//making the loader visible for user
		angular.element('.loading').removeClass('hidden');
		Shipment.makeAsyncRequest(Shipment.listShipment(params))
			.then(function(response){
				if(response.data.success && response.data.count !== undefined && response.data.results){
					$scope.shipmentData.count = response.data.count;
					$scope.shipmentData.list = response.data.results ? response.data.results : [];
                    $scope.totalShipmentCount = $scope.shipmentData.count;
					//console.log($scope.shipmentData.list.shp_reqpickupdate);
					//$scope.shipmentData.list.shp_reqpickupdate = CommonFunctions.dformat($scope.shipmentData.list.shp_reqpickupdate.split(' ')[0]);
					
				}
				//console.log(result);
			}, function(reason){
				console.warn(reason);
			}).finally(function(data){
				//making the loader invisible for user
				//angular.element('#script').after($compile('<loadjsfiles></loadjsfiles>')($scope));
				angular.element('.loading').addClass('hidden');
			});
	}
	
	function getTabInfo(shipID){
	  angular.element('.tabinfo').removeClass('hidden');
	  Shipment.makeAsyncRequest(Shipment.getAjaxShipmentTab(shipID)).then(function(response){
	   if(response.data.success && response.data.count !== undefined && response.data.results){
		 angular.element('#customer_cost_'+shipID).html($compile('<customercost></customercost>')($scope)); 
		 angular.element('#carrier_cost_'+shipID).html($compile('<carriercost></carriercost>')($scope));
	   }
	  }, function(reason){
	   console.warn(reason);
	  }).finally(function(data){
	   getNotes(shipID);
	  });
	}
	
	

	function getNotes(shipID){
	  Shipment.makeAsyncRequest(Shipment.getAjaxShipmentNotes(shipID)).then(function(response){
	   if(response.data.success && response.data.results){
		$scope.customernotes = response.data.results.notedata_customer;
		$scope.carriernotes = response.data.results.notedata_carrier;
		$scope.terminalnotes = response.data.results.notedata_internal;
		//console.log($scope.customernotes); 
	   }
	  }, function(reason){
	   console.warn(reason);
	  }).finally(function(data){
        angular.element('#customer_note_'+shipID).html($compile('<customernotes></customernotes>')($scope));
        angular.element('#carrier_note_'+shipID).html($compile('<carriernotes></carriernotes>')($scope));
        angular.element('#terminal_note_'+shipID).html($compile('<terminalnotes></terminalnotes>')($scope));
        angular.element('.tabinfo').addClass('hidden');		
	  });
	}	
	
}

