define(['angular', 'angular_modules/CommonApp',], function(){
	var app = angular.module('passwordChangeApp', ['CommonApp',])
		.controller('passwordChangeController', function($scope){
			$scope.btnText = 'Go';
			
			$scope.requestChange = function(form){
				if(form && form.$valid){
					$scope.btnText = 'Please Wait...';
					return true;
				}
			}
		}).directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});
});