define(['modules/PageDecorator', 'angular', 'angular_modules/controllers/registrationController'], function(PageDecorator){
	var app = angular.module('registrationApp', [])
		.factory('UIService', function(){
			var factory = {};

			//factory metod to autohide the notification
			factory.hideNotification = function(){
				var decorator = PageDecorator.load();
				decorator.autoHide('.alert', 4000, true);
			}

			factory.loginRedirect = function(){
				
			}

			return factory;
		})
		.controller('registrationController', registrationController);
});