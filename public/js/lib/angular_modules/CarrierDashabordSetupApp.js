define([
	'classes/CarrierApiService',
	'classes/Carrier',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/factories/GoogleLocation',
	'angular_modules/controllers/CarrierDashabordSetupController',
], function(){
	var app = angular.module('customerDashabordSetupApp', ['SyncCarriers', 'GoogleLocationFormatter', ])
		.value('carrierStatusAPIEndpoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRate?')

		.value('CarrierAPIEndpoint', {
			base: '/front/carriers/', list: 'ajax-sub-carrier-lists?',
			listEquipments: 'ajax-sub-carrier-equipments-lists?', 
			carrierModes: 'ajax-carrier-modes?',
			listRemits: 'ajax-sub-carrier-remits-lists?',
			carrierInsurances: 'ajax-carrier-insurances?',
		})

		
		.service('Carrier', ['CarrierAPIEndpoint', '$http', '$q', Carrier])
		
		//service to fetch process and maintains carrier details and api connectivity
		.service('CarrierApiService', ['carrierStatusAPIEndpoint','SyncCarriers', '$http', CarrierApiService])
		
		// our controller for the form
		// =============================================================================
		.controller('CarrierDashabordSetupController', CarrierDashabordSetupController);
});