define([
	'jquery',
	'ng-file-upload',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/controllers/registrationStep3Controller',
	'angucomplete-alt',
	'angular-google-places-autocomplete',
], function($){
	var app = angular.module('addCarrierApp', ['SyncCarriers', 'ngFileUpload', "angucomplete-alt", 'google.places',])
		//service to fetch process and maintains carrier details and api connectivity
		.service('CarrierApiService', function(carrierStatusAPIEndpoint, SyncCarriers, $http, $q, $timeout){
			this.fecthSelectedCarriers = function(){
				return SyncCarriers.fetch();
			};

			this.getCarrierApiDetails = function(carrierId){
				return $http.get('/front/client-register?action=getCarrierApiDetails&carrierId=' + carrierId)
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.checkApiStatus = function(username, password, account, scac){
				return $http.jsonp(carrierStatusAPIEndpoint + 'callback=JSON_CALLBACK&username=' + username
						+ '&password=' + password + '&accountnumber=' + account + '&scaccode=' + scac
					) 
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.postApiDetails = function(dataObj){
				return $http.post('/front/client-register', dataObj)
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.asyncCheckValidateSteps = function(){
					//whether all steps are valid before proceeding to the next step
					var deferred = $q.defer();
					$timeout(function(){
						deferred.resolve(SyncCarriers.validateAllSteps())
					}, 2000);

					return deferred.promise;
				}
		})
		// our controller for the form
		// =============================================================================
		.controller('registrationStep3Controller', registrationStep3Controller)
		.directive('carrierListSetup', function(){
			return {
				restrict: 'E',
				scope: true,
				templateUrl: '/templates/selectedCarrierSetup.html',
				controller: function($scope, $window){
					$scope.visibleCarrierApiSetup = function(carrierId, scac){
						if(scac && carrierId)
							$window.location.href="/front/setup-connectivity#/setup?id="+ carrierId + "&scac=" + scac;
					}
				},
				link: function(scope, elm, attr){
					//sorting function to list api enabled carriers
					scope.sortByApiEnabledCarrier = function(carrier){
						if(scope.apiEnabledCarrierOnly){
							if(carrier.apiEnabled ==1)
								return carrier;
							else
								return;
						}else
							return carrier;
					}
				}
			};
		})
		.directive('selectedCarrierList', function(SyncCarriers, $http){
			return {
				restrict: 'E',
				templateUrl: '/templates/selectedCarrierList.html',
				link: function(scope, element, attrs){
					
					//code to submit individual carrier contact
					scope.submitCarrierInfo = function(index){
						if(index === undefined) //cancelling process if invalid index is given
							return;

						//extracting information using jquery lib
						var name = $('#name_' + index).val().trim();
						var email = $('#email_' + index).val().trim();
						var phone = $('#phone_' + index).val().trim();

						//flag to determine data validity
						var flag = false;
						
						if(name.length || email.length || phone.length){
							var emailRegExp = /^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$/;
							var phoneRegExp = /^(?:[\+]{1})?([0-9]{8,12})$/;
							var nameRegExp = /^([a-zA-Z\s]{2,30})$/;

							if(emailRegExp.test(email) || phoneRegExp.test(phone))
								flag = true;
							else if(nameRegExp.test(name))
								flag = true;
						}
						
						//cancelling processing or submission if valid data flag is not true
						if(!flag)
							return;

						//running spinner for processing status
						var currentButtonContent = $('#submit_'  + index).html();
						$('#submit_'  + index).prop('disabled', true).html('<i class="fa fa-spinner"></i>');

						//preparing the data to post
						var data = {
							step: $('#step_' + index).val(),
							assignmentId: $('#assignmentId_' + index).val(),
							name: $('#name_' + index).val().trim(),
							email: $('#email_' + index).val().trim(),
							phone: $('#phone_' + index).val().trim(),
						};

						//disabling all textboxes when processing save request
						$('#carrier_' + index).find('input').prop('disabled', true);

						$http.post('/front/client-register', data)
							.success(function(result){
								if(result.success){
									//re-enabling the input fields
									$('#carrier_' + index).find('input').prop('disabled', false);
									//restoring submit button text
									$('#submit_'  + index).prop('disabled', false).html(currentButtonContent);
								}
							});
					};


					scope.deleteCarrierProvider = function(index){
						if(confirm('Are you sure to delete')){
							//console.log($('#assignmentId_' + index).val());
							var id = $('#carrier_id_' + index).val();
							//posting to the server endpoint to delete
							$http.delete('/front/client-register/' + id + '/?type=carrierAssignment')
								.success(function(result){
									if(result.success)
										scope.$emit('refetch', [result]);
								})
								.error(function(data, status){
									alert(status);
								});
						}
					};
					
					scope.redirect = function(){						
						window.location = "/front/setup-connectivity";
					};

					scope.$on('saveContactInfo', function(event, params){
						//finding the form
						$(element).find('form').each(function(i){
							var index = $(this).data('index');
							//console.log();
							scope.submitCarrierInfo(index);
						});

						
					});
				}
			};
		})
});
