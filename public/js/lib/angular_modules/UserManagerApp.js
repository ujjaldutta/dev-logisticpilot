define([
	'classes/User',	
	'angular_modules/controllers/UserManagerController',	
	'angular_modules/controllers/UserRegistrationController',	
	'angular_modules/directives/jqEasyTabs',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',	
	'angular-google-places-autocomplete',
	'ng-tags-input',
], function(){	
	var app = angular.module('UserManagerApp', [
			'jq.easytabs', 'rzModule', 'ui.bootstrap', 'google.places', 'ngTagsInput',
		])
		.value('UserAPIEndpoint', {			
				base: '/front/users/', 
				list: 'ajax-sub-user-lists?', 
				edit: 'ajax-manage-sub-user?', 
				deleteUrl: 'ajax-sub-user-delete?',				
				//addUser: 'ajax-manage-sub-user?',	
				userTypes: 'ajax-get-user-types?',
				clientList: 'ajax-get-client-lists?',			
				clientMapping: 'ajax-client-mapping?',
				sendPassword: 'ajax-send-password?',
				sendUpdatePassword: 'ajax-send-update-password',
				layoutTypes: 'ajax-layout-types?',
				layoutFields: 'ajax-layout-attributes?',
				savedLayouts: 'ajax-saved-attributes?',
				saveLayoutAssignment: 'ajax-save-attributes?',
				allLayoutMappings: 'ajax-all-layout-mappings?',
				constraintOperatos: 'ajax-constraint-operators?',
				screenFilter: 'ajax-screen-filters?',
				//screenFilter: 'ajax-all-screen-filters?',
		})		
		
		//service to handle carrier search and other its results
		.service('User', ['UserAPIEndpoint', '$http', '$q', User])		

		//factory to handle calculation and other things		
		
		.controller('UserManagerController', UserManagerController)
		.controller('UserRegistrationController', UserRegistrationController)
		
		
});