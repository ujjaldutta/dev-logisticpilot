define([
	'ng-file-upload',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/controllers/apiSetupController',
	'angular_modules/directives/jqEasyTabs',
	'angucomplete-alt',
	'ng-tags-input',
	'angularjs-datepicker',
	'classes/CarrierApiService',
], function(){
	var app = angular.module('apiSetupApp', ['SyncCarriers', 'ngFileUpload', "angucomplete-alt", 'ngTagsInput', '720kb.datepicker', 'jq.easytabs', ])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('carrierStatusAPIEndpoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRate?')
		
		//service to fetch process and maintains carrier details and api connectivity
		.service('CarrierApiService', ['carrierStatusAPIEndpoint','SyncCarriers', '$http', CarrierApiService])
		// our controller for the form
		// =============================================================================
		.controller('apiSetupController', apiSetupController)
		.directive('accsTariffs', ['Upload', function(Upload){
			return {
				restrict: 'AC',
				scope:{
					defaultRates: '=',
					customRates: '=',
					accsTypes: '=',
					calcTypes: '=',
					defaultRateAccs: '=',
					carrierId: '@',
				},
				templateUrl: '/templates/accsTariffDefault.html',
				//require: '^ngFileUpload',
				link: function(scope, elm, attr){
					//attributes to show hide and detrmine status of upload csv
					scope.uploadProgress =  false;
					scope.uploadSuccess = false;
					scope.uploadError = false;

					scope.filter = {type:1, fromDate: null, toDate: null},
					scope.itemsLength = scope.customRates ? scope.customRates.length : 0;

					//emitting the accs tarif type to parent scope when changed by a user
					scope.$watch('defaultRateAccs', function(newValue, oldValue){
						if(angular.isDefined(oldValue) && newValue != oldValue){
							scope.$emit('accsTariffTypeChanged', newValue);
						}
					});

					//listening to event when item is deleted from the child directive
					scope.$on('updateCustomAccsCollection', function(e, args){
						//console.log(args);
						scope.customRates.splice(args, 1);
					});

					//adding new item in the collection
					scope.addNewRate = function(){
						/* var obj = {};
						console.log(scope.itemsLength);
						obj[scope.itemsLength] = {editEnabled: 1, carrierID: scope.carrierId, accsID: 1, crraccs_minrate: 0.00, crraccs_rate: 0.00, crraccs_type: 1, crraccs_addbydefault: 0, effectivefrom: null};
						
						//adding a blank row to insert new object
						scope.customRates = angular.extend(obj, scope.customRates);

						console.log(scope.customRates);

						scope.itemsLength++;
						*/

						scope.customRates.push({editEnabled: 1, carrierID: scope.carrierId, accsID: 1, crraccs_minrate: null, crraccs_rate: null, crraccs_type: 1, crraccs_addbydefault: 0, effectivefrom: null});
					}
					
					//emitting events to parent scope to refetch the values as per the filter params
					scope.filterTariff = function(){
						//console.log(scope.filter);
						scope.$emit('filterCustomAccsTariff', angular.extend({carrierId: scope.carrierId}, scope.filter));
					}

					//upload custom accessorial tarif csv
					scope.uploadAccessorial = function (files) {
				        if (files && files.length) {
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i];
				                Upload.upload({
				                    url: '/front/carrier-service?action=uploadAccesCsv',
				                    fields: {
				                        'carrierId': scope.carrierId
				                    },
				                    file: file
				                }).progress(function (evt) {
				                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				                    scope.uploadProgress = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
				                                //evt.config.file.name + '\n' + $scope.formData.log;
				                }).success(function (data, status, headers, config) {
				                	scope.uploadProgress = false;
				                	
				                	if(data.error){
				                		scope.uploadSuccess = false;
				                		scope.uploadError = data.error;

				                	}else if(data.success){
				                		scope.uploadError = false;
				                		scope.uploadSuccess = 'file ' + config.file.name + 'uploaded successfully!';
				                		
				                		//emitting event to sync data after csv upload success
				                		scope.$emit('filterCustomAccsTariff', {carrierId: scope.carrierId});
				                	}
				                });
				            }
				        }
				    };

				    //method to handle upload notification toogle on or off
				    scope.resetUploadNotification = function(){
				    	scope.uploadProgress =  false;
						scope.uploadSuccess = false;
						scope.uploadError = false;
				    }
				}
			};
		}])
		.directive('fuelTariffs', ['Upload', function(Upload){
			return {
				restrict: 'AC',
				scope:{
					defaultRates: '=',
					customRates: '=',
					defaultRateFuel: '=',
					fuelTypes: '=',
					carrierId: '@',
					defaultRateCount: '@',
					customRateCount: '@',
				},
				templateUrl: '/templates/fuelTariffDefault.html',
				link: function(scope, elm, attr){
					//attributes to show hide and detrmine status of upload csv
					scope.uploadProgress =  false;
					scope.uploadSuccess = false;
					scope.uploadError = false;

					scope.filter = {type:1, fromDate: null, toDate: null, amountFrom: null, amountTo: null},

					//emitting the fuel tarif type to parent scope when changed by a user
					scope.$watch('defaultRateFuel', function(newValue, oldValue){
						if(angular.isDefined(oldValue) && newValue != oldValue){
							scope.$emit('fuelTariffTypeChanged', newValue);
						}
					});

					//listening to event when item is deleted from the child directive
					scope.$on('updateCustomFuelCollection', function(e, args){
						//console.log(args);
						scope.customRates.splice(args, 1);
					});
					
					//adding new item in the collection
					scope.addNewRate = function(){
						/* var obj = {};

						//console.log(scope.customRateCount);
						
						//obj[scope.customRateCount] = {editEnabled: 1, carrierID: scope.carrierId, fuelType: 1, rangefrom: 0.00, rangeto: 0.00, LTLRate: 1, TLRate: 0, effectivefrom: null};
						
						//adding a blank row to insert new object
						//scope.customRates = angular.extend(obj, scope.customRates);

						//console.log(scope.customRates);

						//scope.customRateCount++;

						*/
						//adding new item in the collection
						scope.customRates.push({editEnabled: 1, carrierID: scope.carrierId, fuelType: 1, rangefrom: null, rangeto: null, LTLRate: null, TLRate: null, effectivefrom: null});
						
					}

					//emitting events to parent scope to refetch the values as per the filter params
					scope.filterTariff = function(){
						//console.log(scope.filter);
						scope.$emit('filterCustomFuelTariff', angular.extend({carrierId: scope.carrierId}, scope.filter));
					}

					//upload custom fuel tarif csv
					scope.uploadFuel = function (files) {
				        if (files && files.length) {
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i];
				                Upload.upload({
				                    url: '/front/carrier-service?action=uploadFuelCsv',
				                    fields: {
				                        'carrierId': scope.carrierId
				                    },
				                    file: file
				                }).progress(function (evt) {
				                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				                    scope.uploadProgress = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
				                                //evt.config.file.name + '\n' + $scope.formData.log;
				                }).success(function (data, status, headers, config) {
				                	scope.uploadProgress = false;
				                	
				                	if(data.error){
				                		scope.uploadSuccess = false;
				                		scope.uploadError = data.error;

				                	}else if(data.success){
				                		scope.uploadError = false;
				                		scope.uploadSuccess = 'file ' + config.file.name + 'uploaded successfully!';
				                		
				                		//emitting event to sync data after csv upload success
				                		scope.$emit('filterCustomFuelTariff', {carrierId: scope.carrierId});
				                	}
				                });
				            }
				        }
				    };

				    //method to handle upload notification toogle on or off
				    scope.resetUploadNotification = function(){
				    	scope.uploadProgress =  false;
						scope.uploadSuccess = false;
						scope.uploadError = false;
				    }
				}
			};
		}])
		.directive('customAccsTariffEditable', ['CarrierApiService', function(CarrierApiService){
			var TEMPLATE_URL = '/templates/accsCustomEditForm.html';
			return {
				restrict: 'AC',
				templateUrl: TEMPLATE_URL,
				scope: {
					savedValue: '=',
					accsTypes: '=',
					calcTypes: '=',
					editEnabled: '=',
					indexKey: '@',
				},
				link: function(scope, elm, attr){
					scope.rates = {};
					scope.editEnabled = scope.editEnabled === undefined ? 0 : scope.editEnabled;
					
					//code fix for angular js checkbox directive
					if(scope.savedValue.crraccs_addbydefault !== undefined && scope.savedValue.crraccs_addbydefault == 1)
						scope.savedValue.crraccs_addbydefault = true;
					else if(scope.savedValue.crraccs_addbydefault !== undefined && scope.savedValue.crraccs_addbydefault == 0)
						scope.savedValue.crraccs_addbydefault = false;

					angular.copy(scope.savedValue, scope.rates);

					scope.updateInfo = function (){
						CarrierApiService.postCarrierAccTariffCustomDetails(angular.extend({step:'postCarrierAccTariffCustomDetails'}, scope.rates))
						.success(function(result){
							if(result.success && result.id){
								scope.rates.id = result.id;
								scope.editEnabled = 0;
							}
						});
					};
					
					scope.deleteInfo = function (id){
						if(confirm('are you sure ?')){
							if(id == undefined){
								elm.remove();
								scope.$emit('updateCustomAccsCollection', scope.indexKey);
								return;
							}
							
							CarrierApiService.deleteCarrierAccTariffCustomDetails(id)
								.success(function(result){
									if(result.success){
										elm.remove();
										scope.$emit('updateCustomAccsCollection', scope.indexKey);
									}	
								});
						}
					};
				}
			}
		}])
		.directive('customFuelTariffEditable', ['CarrierApiService', function(CarrierApiService){
			var TEMPLATE_URL = '/templates/fuelCustomEditForm.html';
			return {
				restrict: 'AC',
				templateUrl: TEMPLATE_URL,
				scope: {
					savedValue: '=',
					fuelTypes: '=',
					editEnabled: '=',
					indexKey: '@',
				},
				link: function(scope, elm, attr){
					scope.ftrates = {};
					scope.editEnabled = scope.editEnabled === undefined ? 0 : scope.editEnabled;

					angular.copy(scope.savedValue, scope.ftrates);

					scope.updateInfo = function (){
						CarrierApiService.postCarrierFuelTariffCustomDetails(angular.extend({step:'postCarrierFuelTariffCustomDetails'}, scope.ftrates))
						.success(function(result){
							if(result.success && result.id){
								scope.ftrates.id = result.id;
								scope.editEnabled = 0;
							}
						});
					};
					scope.deleteInfo = function (id){
						if(confirm('are you sure ?')){
							if(id == undefined){
								elm.remove();
								scope.$emit('updateCustomFuelCollection', scope.indexKey);
								return;
							}	

							CarrierApiService.deleteCarrierFuelTariffCustomDetails(id)
								.success(function(result){
									if(result.success){
										elm.remove();
										scope.$emit('updateCustomFuelCollection', scope.indexKey);
									}
										
								});
						}
					};
				}
			}
	}]);	
});