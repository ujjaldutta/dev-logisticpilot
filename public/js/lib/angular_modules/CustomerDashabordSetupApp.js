define([
	'classes/Customer',
	'classes/CustomerLocation',
	'classes/CustomerProduct',
	'classes/CarrierApiService',
	'classes/User',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/factories/GoogleLocation',
	'angular_modules/controllers/CustomerDashabordSetupController',
], function(){
	var app = angular.module('CustomerDashabordSetupApp', ['SyncCarriers', 'GoogleLocationFormatter', ])
		//constants or base object for Customer Api
		.value('CustomerAPIEndpoint', {
				base: '/front/customers/', list: 'ajax-sub-client-lists?',
		})
		
		//constants or base object for User Api
		.value('UserAPIEndpoint', {			
				base: '/front/users/', 
				list: 'ajax-sub-user-lists?',
		})
		
		//constants or base object for Customer Location Api
		.value('CustomerLocationAPIEndpoint', {
			base: '/front/customer-locations/', list: 'ajax-list-locations?',
		})
		
		//constants or base object for Customer product Api
		.value('CustomerProductAPIEndpoint', {
				base: '/front/customer-products/', list: 'ajax-list-products?',
		})

		.value('carrierStatusAPIEndpoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRate?')
		
		//service to fetch process and maintains carrier details and api connectivity
		.service('CarrierApiService', ['carrierStatusAPIEndpoint','SyncCarriers', '$http', CarrierApiService])

		//service to handle customer product component
		.service('CustomerProduct', ['CustomerProductAPIEndpoint', '$http', '$q', 'GoogleLocationFactory', CustomerProduct])
		
		//service to handle customer dashboard component
		.service('Customer', ['CustomerAPIEndpoint', '$http', '$q', Customer])
		
		//service to handle user dashboard component
		.service('User', ['UserAPIEndpoint', '$http', '$q', User])
		
		//service to handle customer location dashboard component
		.service('CustomerLocation', ['CustomerLocationAPIEndpoint', '$http', '$q', 'GoogleLocationFactory', CustomerLocation])

		
		// our controller for the form
		// =============================================================================
		.controller('CustomerDashabordSetupController', CustomerDashabordSetupController);
});