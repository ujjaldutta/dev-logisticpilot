define([
	'classes/CarrierEquipment',
	'angular_modules/CommonApp',
	'angular_modules/CarrierEquipmentSidebarApp',
	'angular_modules/controllers/CarrierFuelManagerController',
	'angular_modules/controllers/CarrierFuelEditController',
	'ui-bootstrap',
], function(){
	var app = angular.module('CarrierFuelManagerApp', [
			'ui.bootstrap', 'CommonApp', 'CarrierEquipmentSidebarApp',
		])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('CarrierEquipmentAPIEndpoint', {
				base: '/front/client-register?', list: 'action=getCarrierFuelTariffDetails&withNoDefault=1&withNoExtraParams=1&', 
				get: 'action=getCarrierFuelTariffData&', edit: 'action=postCarrierFuelTariffCustomDetails', deleteUrl: '?type=deleteCarrierFuelTariffCustomDetails',
				fuelTypes: 'action=getCarrierFuelTariffDetails&withNoDefault=1&withNoCustom=1&',
		})

		
		.service('CarrierEquipment', ['CarrierEquipmentAPIEndpoint', '$http', '$q', CarrierEquipment])
		// our controller for the form
		// =============================================================================
		.controller('CarrierFuelManagerController', CarrierFuelManagerController)
		.controller('CarrierFuelEditController', CarrierFuelEditController);
});