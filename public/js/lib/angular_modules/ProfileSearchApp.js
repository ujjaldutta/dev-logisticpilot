define([
	'angular',
	'angular_modules/CommonApp',
	'angular_modules/controllers/ProfileSearchController',
	'angular_modules/factories/GoogleLocation',
	'angular_modules/directives/jqEasyTabs',
	'ui-bootstrap',
	'profile',
	'bootstrap-tooltip',
	'angular-google-places-autocomplete',
	'jquery-ui',
	'bootstrap-timepicker.min',
	'angular-endless-scroll.min',
	], function(){
	var app = angular.module('ProfileSearchApp', ['CommonApp',  'ui.bootstrap', 'angucomplete-alt','google.places', 'GoogleLocationFormatter','dc.endlessScroll'])
	
	.controller('ProfileSearchController', ProfileSearchController)
	
	;
	
	
		app.directive('assocdate', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
							jQuery(".fromdate").datepicker();
							jQuery(".todate").datepicker();
        
				  }
			   };
			}]);
			
			

	/*app.directive('googleplace', function() {
					return {
						require: 'ngModel',
						scope: {
							ngModel: '=',
							details: '=?'
						},
						link: function(scope, element, attrs, model) {
							var options = {
								types: [],
								componentRestrictions: {}
							};
							scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

							google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
								scope.$apply(function() {
									scope.details = scope.gPlace.getPlace();
									model.$setViewValue(element.val());                
								});
							});
						}
					};
				});
*/


})
