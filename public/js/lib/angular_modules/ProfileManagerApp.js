define([
	'angular',
	'angular_modules/CommonApp',
	'angular_modules/controllers/ProfileManagerController',
	//'angular_modules/controllers/ContractFuelController',
	//'angular_modules/controllers/ContractAssocarialController',
	'angular_modules/directives/jqEasyTabs',
	'angularjs-datepicker',
	'ui-bootstrap',
	'ng-file-upload',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
	'bootstrap-tooltip',
	'jquery-ui',
	'bootstrap-timepicker.min',
	'profile',
	
	'angular-endless-scroll.min',
	'angular_modules/factories/SyncCarriers',
	'ng-tags-input',
	'bootstrap-multiselect',
	'classes/CarrierApiService',
	'csv',
	'pdfmake',
	'vfsfonts',
	'uigrid',
	'angular-touch',
	//'checkList-autocomplete',
			
	], function(){
	var app = angular.module('ProfileManagerApp', ['CommonApp', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', '720kb.datepicker','ngFileUpload', 'jq.easytabs', 'google.places','dc.endlessScroll', 'ngTouch', 'ui.grid', 'ui.grid.edit','ui.grid.selection', 'addressFormatter','ngTagsInput','SyncCarriers'])
	
	.controller('ProfileManagerController', ProfileManagerController)
	//.controller('ContractFuelController', ContractFuelController)
	//.controller('ContractAssocarialController', ContractAssocarialController)
	//.controller('ContractMarkupController', ContractMarkupController)
	;
	
	
	      
/***************************************************************************/	      
	      //fix tooltips issue on ajax pagination
	      app.directive('tooltipshow', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
					jQuery( ".tooltip_cont a" ).mouseover(
							function() {
								jQuery( this ).parent().find( ".tooltip_div" ).show();
							}
						);
						jQuery( ".tooltip_cont a" ).mouseout(
							function() {
								jQuery( this ).parent().find( ".tooltip_div" ).hide();
							}
						);
						
						
					
					
						
        
				  }
			   };
			}]);
			
			
			
			
					
			
		app.directive('assocdate', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
							jQuery(".fromdate").datepicker();
							jQuery(".todate").datepicker();
        
				  }
			   };
			}]);
			
		app.directive('zipauto', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs,ngModel) {
					  
								jQuery(".sourcepost").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/postal-list',
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.OriginCountryCode
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2
							});
							
							jQuery(".destpost").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/postal-list',
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.DestinationCountryCode
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2
							});
							
							
        
				  }
			   };
			}]);
		

		app.directive('statecity', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
					  
								jQuery(".sourcestatecity").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/statecity-list',
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.OriginCountryCode
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2
							});
							
							
							jQuery(".deststatecity").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/statecity-list',
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.DestinationCountryCode
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2
							});

        
				  }
			   };
			}]);
		
		
		app.directive('state', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
					  
								jQuery(".sourcestate").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/state-list',
										
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.OriginCountryCode
										},
										success: function (data)
										{
											response(data);
										}
										
									});
								},
								minLength: 2
							});
							
							
							jQuery(".deststate").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/state-list',
										
										dataType: "json",
										data:
										{
											q:request.term,countrycode:scope.ratelanematrix.DestinationCountryCode
										},
										success: function (data)
										{
											response(data);
										}
									});
								},
								minLength: 2
							});


        
				  }
			   };
			}]);	
			
			app.directive('country', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
					  
								jQuery(".sourcecountry").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/country-list',
										type     : 'POST',
										dataType: "json",
										data:
										{
											q:request.term,
											_token:jQuery("#_token").val()
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2,
								select: function( event, ui ) {
										
										 scope.ratelanematrix.OriginCountry=ui.item.value;
										 scope.ratelanematrix.OriginCountryCode=ui.item.id;
										
										  
									  }
							});
							
							
							
							jQuery(".destcountry").autocomplete({
		
							source: function (request, response)
								{
									jQuery.ajax(
									{
										url: '/front/services/country-list',
										type     : 'POST',
										dataType: "json",
										data:
										{
											q:request.term,
											_token:jQuery("#_token").val()
										},
										success: function (data)
										{
											response(data.items);
										}
									});
								},
								minLength: 2,
								select: function( event, ui ) {
										
										 scope.ratelanematrix.DestinationCountry=ui.item.value;
										scope.ratelanematrix.DestinationCountryCode=ui.item.id;
										  
									  }
							});

        
				  }
			   };
			}]);		
			
		app.directive('datapicker', [function() {
			   return {
				  restrict: 'A',
				  link: function(scope, elem, attrs) {
							
							
							
							
							jQuery('#example-dataprovider').multiselect({
								            enableFiltering: true,
											filterBehavior: 'value',
											onChange: function(option, checked, select) {
												//console.log('Changed option ' + jQuery(option).val() + '.');
												//console.log(this.lastToggledInput);
											},
											templates: {
												
												filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search filtertext1" type="text"></div></li>',
												
											},
											onDropdownHide:function(event){
												//jQuery('#example-dataprovider').multiselect('refresh');
											},
											dataprovider:function(){
												
														jQuery.ajax({
														url      : '/front/services/postal-list',
														data     : post,
														type     : 'POST',
														dataType : 'json',
														success: function(data) {
															  
															 return data
														}      
												  }); 
											}
								});



							var options = [
								{label: 'Select', title: 'Select', value: '', selected: false},
								
							];
						
  
    
        
				  }
			   };
			}]);	
			
			
				
	
			
	angular.module('addressFormatter', []).filter('address', function () {
	  return function (input) {
		  return input.street + ', ' + input.city + ', ' + input.state + ', ' + input.zip;
	  };
	});		
			
		app.factory('dataService', function() {

			  // private variable
			  var _dataObj = {};

			  // public API
			  return {
				dataObj: _dataObj
			  };
			});	
		
	
/********************************************************************************/		

app.directive('accsTariffs', ['Upload', function(Upload){
			return {
				restrict: 'AC',
				scope:{
					defaultRates: '=',
					customRates: '=',
					accsTypes: '=',
					calcTypes: '=',
					defaultRateAccs: '=',
					carrierId: '@',
				},
				templateUrl: '/templates/accsTariffDefault.html',
				//require: '^ngFileUpload',
				link: function(scope, elm, attr){
					//attributes to show hide and detrmine status of upload csv
					scope.uploadProgress =  false;
					scope.uploadSuccess = false;
					scope.uploadError = false;

					scope.filter = {type:1, fromDate: null, toDate: null},
					scope.itemsLength = scope.customRates ? scope.customRates.length : 0;

					//emitting the accs tarif type to parent scope when changed by a user
					scope.$watch('defaultRateAccs', function(newValue, oldValue){
						if(angular.isDefined(oldValue) && newValue != oldValue){
							scope.$emit('accsTariffTypeChanged', newValue);
						}
					});

					//listening to event when item is deleted from the child directive
					scope.$on('updateCustomAccsCollection', function(e, args){
						//console.log(args);
						scope.customRates.splice(args, 1);
					});

					//adding new item in the collection
					scope.addNewRate = function(){
						/* var obj = {};
						console.log(scope.itemsLength);
						obj[scope.itemsLength] = {editEnabled: 1, carrierID: scope.carrierId, accsID: 1, crraccs_minrate: 0.00, crraccs_rate: 0.00, crraccs_type: 1, crraccs_addbydefault: 0, effectivefrom: null};
						
						//adding a blank row to insert new object
						scope.customRates = angular.extend(obj, scope.customRates);

						console.log(scope.customRates);

						scope.itemsLength++;
						*/

						scope.customRates.push({editEnabled: 1, carrierID: scope.carrierId, accsID: 1, crraccs_minrate: null, crraccs_rate: null, crraccs_type: 1, crraccs_addbydefault: 0, effectivefrom: null});
					}
					
					//emitting events to parent scope to refetch the values as per the filter params
					scope.filterTariff = function(){
						//console.log(scope.filter);
						scope.$emit('filterCustomAccsTariff', angular.extend({carrierId: scope.carrierId}, scope.filter));
					}

					//upload custom accessorial tarif csv
					scope.uploadAccessorial = function (files) {
				        if (files && files.length) {
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i];
				                Upload.upload({
				                    url: '/front/carrier-service?action=uploadAccesCsv',
				                    fields: {
				                        'carrierId': scope.carrierId
				                    },
				                    file: file
				                }).progress(function (evt) {
				                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				                    scope.uploadProgress = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
				                                //evt.config.file.name + '\n' + $scope.formData.log;
				                }).success(function (data, status, headers, config) {
				                	scope.uploadProgress = false;
				                	
				                	if(data.error){
				                		scope.uploadSuccess = false;
				                		scope.uploadError = data.error;

				                	}else if(data.success){
				                		scope.uploadError = false;
				                		scope.uploadSuccess = 'file ' + config.file.name + 'uploaded successfully!';
				                		
				                		//emitting event to sync data after csv upload success
				                		scope.$emit('filterCustomAccsTariff', {carrierId: scope.carrierId});
				                	}
				                });
				            }
				        }
				    };

				    //method to handle upload notification toogle on or off
				    scope.resetUploadNotification = function(){
				    	scope.uploadProgress =  false;
						scope.uploadSuccess = false;
						scope.uploadError = false;
				    }
				}
			};
		}]);
		
		
app.directive('fuelTariffs', ['Upload', function(Upload){
			return {
				restrict: 'AC',
				scope:{
					defaultRates: '=',
					customRates: '=',
					defaultRateFuel: '=',
					fuelTypes: '=',
					carrierId: '@',
					defaultRateCount: '@',
					customRateCount: '@',
				},
				templateUrl: '/templates/fuelTariffDefault.html',
				link: function(scope, elm, attr){
					//attributes to show hide and detrmine status of upload csv
					scope.uploadProgress =  false;
					scope.uploadSuccess = false;
					scope.uploadError = false;

					scope.filter = {type:1, fromDate: null, toDate: null, amountFrom: null, amountTo: null},

					//emitting the fuel tarif type to parent scope when changed by a user
					scope.$watch('defaultRateFuel', function(newValue, oldValue){
						if(angular.isDefined(oldValue) && newValue != oldValue){
							scope.$emit('fuelTariffTypeChanged', newValue);
						}
					});

					//listening to event when item is deleted from the child directive
					scope.$on('updateCustomFuelCollection', function(e, args){
						//console.log(args);
						scope.customRates.splice(args, 1);
					});
					
					//adding new item in the collection
					scope.addNewRate = function(){
						/* var obj = {};

						//console.log(scope.customRateCount);
						
						//obj[scope.customRateCount] = {editEnabled: 1, carrierID: scope.carrierId, fuelType: 1, rangefrom: 0.00, rangeto: 0.00, LTLRate: 1, TLRate: 0, effectivefrom: null};
						
						//adding a blank row to insert new object
						//scope.customRates = angular.extend(obj, scope.customRates);

						//console.log(scope.customRates);

						//scope.customRateCount++;

						*/
						//adding new item in the collection
						scope.customRates.push({editEnabled: 1, carrierID: scope.carrierId, fuelType: 1, rangefrom: null, rangeto: null, LTLRate: null, TLRate: null, effectivefrom: null});
						
					}

					//emitting events to parent scope to refetch the values as per the filter params
					scope.filterTariff = function(){
						//console.log(scope.filter);
						scope.$emit('filterCustomFuelTariff', angular.extend({carrierId: scope.carrierId}, scope.filter));
					}

					//upload custom fuel tarif csv
					scope.uploadFuel = function (files) {
				        if (files && files.length) {
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i];
				                Upload.upload({
				                    url: '/front/carrier-service?action=uploadFuelCsv',
				                    fields: {
				                        'carrierId': scope.carrierId
				                    },
				                    file: file
				                }).progress(function (evt) {
				                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				                    scope.uploadProgress = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
				                                //evt.config.file.name + '\n' + $scope.formData.log;
				                }).success(function (data, status, headers, config) {
				                	scope.uploadProgress = false;
				                	
				                	if(data.error){
				                		scope.uploadSuccess = false;
				                		scope.uploadError = data.error;

				                	}else if(data.success){
				                		scope.uploadError = false;
				                		scope.uploadSuccess = 'file ' + config.file.name + 'uploaded successfully!';
				                		
				                		//emitting event to sync data after csv upload success
				                		scope.$emit('filterCustomFuelTariff', {carrierId: scope.carrierId});
				                	}
				                });
				            }
				        }
				    };

				    //method to handle upload notification toogle on or off
				    scope.resetUploadNotification = function(){
				    	scope.uploadProgress =  false;
						scope.uploadSuccess = false;
						scope.uploadError = false;
				    }
				}
			};
		}]);		
		
app.directive('customAccsTariffEditable', ['CarrierApiService', function(CarrierApiService){
			var TEMPLATE_URL = '/templates/accsCustomEditForm.html';
			return {
				restrict: 'AC',
				templateUrl: TEMPLATE_URL,
				scope: {
					savedValue: '=',
					accsTypes: '=',
					calcTypes: '=',
					editEnabled: '=',
					indexKey: '@',
				},
				link: function(scope, elm, attr){
					scope.rates = {};
					scope.editEnabled = scope.editEnabled === undefined ? 0 : scope.editEnabled;
					
					//code fix for angular js checkbox directive
					if(scope.savedValue.crraccs_addbydefault !== undefined && scope.savedValue.crraccs_addbydefault == 1)
						scope.savedValue.crraccs_addbydefault = true;
					else if(scope.savedValue.crraccs_addbydefault !== undefined && scope.savedValue.crraccs_addbydefault == 0)
						scope.savedValue.crraccs_addbydefault = false;

					angular.copy(scope.savedValue, scope.rates);

					scope.updateInfo = function (){
						CarrierApiService.postCarrierAccTariffCustomDetails(angular.extend({step:'postCarrierAccTariffCustomDetails'}, scope.rates))
						.success(function(result){
							if(result.success && result.id){
								scope.rates.id = result.id;
								scope.editEnabled = 0;
							}
						});
					};
					
					scope.deleteInfo = function (id){
						if(confirm('are you sure ?')){
							if(id == undefined){
								elm.remove();
								scope.$emit('updateCustomAccsCollection', scope.indexKey);
								return;
							}
							
							CarrierApiService.deleteCarrierAccTariffCustomDetails(id)
								.success(function(result){
									if(result.success){
										elm.remove();
										scope.$emit('updateCustomAccsCollection', scope.indexKey);
									}	
								});
						}
					};
				}
			}
		}]);
		
app.directive('customFuelTariffEditable', ['CarrierApiService', function(CarrierApiService){
			var TEMPLATE_URL = '/templates/fuelCustomEditForm.html';
			return {
				restrict: 'AC',
				templateUrl: TEMPLATE_URL,
				scope: {
					savedValue: '=',
					fuelTypes: '=',
					editEnabled: '=',
					indexKey: '@',
				},
				link: function(scope, elm, attr){
					scope.ftrates = {};
					scope.editEnabled = scope.editEnabled === undefined ? 0 : scope.editEnabled;

					angular.copy(scope.savedValue, scope.ftrates);

					scope.updateInfo = function (){
						CarrierApiService.postCarrierFuelTariffCustomDetails(angular.extend({step:'postCarrierFuelTariffCustomDetails'}, scope.ftrates))
						.success(function(result){
							if(result.success && result.id){
								scope.ftrates.id = result.id;
								scope.editEnabled = 0;
							}
						});
					};
					scope.deleteInfo = function (id){
						if(confirm('are you sure ?')){
							if(id == undefined){
								elm.remove();
								scope.$emit('updateCustomFuelCollection', scope.indexKey);
								return;
							}	

							CarrierApiService.deleteCarrierFuelTariffCustomDetails(id)
								.success(function(result){
									if(result.success){
										elm.remove();
										scope.$emit('updateCustomFuelCollection', scope.indexKey);
									}
										
								});
						}
					};
				}
			}
	}]);		
app.value('carrierStatusAPIEndpoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRate?');

app.service('CarrierApiService', ['carrierStatusAPIEndpoint','SyncCarriers', '$http', CarrierApiService]);



app.filter('mapGender', function() {
      var genderHash = {
        1: 'male',
        2: 'female'
      };
     
      return function(input) {
        if (!input){
          return '';
        } else {
          return genderHash[input];
        }
      };
    });
     
    app.filter('mapStatus', function() {
      var genderHash = {
        1: 'Bachelor',
        2: 'Nubile',
        3: 'Married'
      };
     
      return function(input) {
        if (!input){
          return '';
        } else {
          return genderHash[input];
        }
      };
    }) ;


})
