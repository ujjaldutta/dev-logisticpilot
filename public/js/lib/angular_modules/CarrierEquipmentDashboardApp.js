define([
	'angular_modules/CommonApp',
	'angular_modules/CarrierEquipmentSidebarApp',
	'angular_modules/controllers/CarrierEquipmentDashboardController',
], function(){
	var app = angular.module('CarrierEquipmentDashboardApp', [
			'CommonApp', 
			'CarrierEquipmentSidebarApp',
		])

		// our controller for the form
		// =============================================================================
		.controller('CarrierEquipmentDashboardController', CarrierEquipmentDashboardController)
});