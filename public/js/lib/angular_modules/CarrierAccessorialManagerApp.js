define([
	'classes/CarrierEquipment',
	'angular_modules/CommonApp',
	'angular_modules/CarrierEquipmentSidebarApp',
	'angular_modules/controllers/CarrierAccessorialManagerController',
	'angular_modules/controllers/CarrierAccessorialEditController',
	'angularjs-datepicker',
	'ui-bootstrap',
], function(){
	var app = angular.module('CarrierAccessorialManagerApp', [
			'CommonApp', 'CarrierEquipmentSidebarApp',
			'ui.bootstrap', '720kb.datepicker',
		])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('CarrierEquipmentAPIEndpoint', {
				base: '/front/client-register?', list: 'action=getCarrierAccTariffDetails&withNoDefault=1&withNoExtraParams=1&', 
				get: 'action=getCarrierAccsTariffData&', edit: 'action=postCarrierAccTariffCustomDetails', deleteUrl: '?type=deleteCarrierAccTariffCustomDetails',
				calcTypes: 'action=getCarrierAccTariffDetails&withNoDefault=1&withNoCustom=1&', accessorials: 'ajax-accessorial-types?'
		})

		
		.service('CarrierEquipment', ['CarrierEquipmentAPIEndpoint', '$http', '$q', CarrierEquipment])
		// our controller for the form
		// =============================================================================
		.controller('CarrierAccessorialManagerController', CarrierAccessorialManagerController)
		.controller('CarrierAccessorialEditController', CarrierAccessorialEditController)	
});