define(['angular'], function(){
	'use strict';
	
	angular.module('GoogleLocationFormatter', [])
	//factory to work on carriers
	.factory('GoogleLocationFactory', function(){
		return {
			processGoogleLocation : function(locationObj){
				var processedLocation = {};
				if(locationObj !== undefined && locationObj.address_components && locationObj.address_components.length){
					for(var i = 0; i < locationObj.address_components.length; i ++){
						if(locationObj.address_components[i].types[0] !== undefined && 
							locationObj.address_components[i].types[0])
						{
							var addressType = locationObj.address_components[i].types[0];
							switch(addressType){
								case 'postal_code':
									processedLocation.postal_code = locationObj.address_components[i].long_name !== undefined ? 
										locationObj.address_components[i].long_name : null;
									break;
								case 'locality':
									processedLocation.city = locationObj.address_components[i].long_name !== undefined ? 
											locationObj.address_components[i].long_name : null;
									break;
								case 'administrative_area_level_1':
									processedLocation.state = locationObj.address_components[i].short_name !== undefined ? 
												locationObj.address_components[i].short_name : null;
									break;
								case 'country':
									processedLocation.country = locationObj.address_components[i].short_name !== undefined ? 
												locationObj.address_components[i].short_name : null;
									break;
							}
						}
					}
				}
				return processedLocation;
			},

			fillLocationDetails: function (collection, prf){
				var location = '';
				var city = prf + '_city';
				var state = prf + '_state';
				var postal = prf + '_postal';
				var country = prf + '_country';
				
				if(collection[city] !== undefined && collection[city])
					location += collection[city] + ', ';
				if(collection[state] !== undefined && collection[state])
					location += collection[state] + ', ';
				if(collection[postal] !== undefined && collection[postal])
					location += collection[postal] + ', ';
				if(collection[country] !== undefined && collection[country])
					location += collection[country];

				return location;
			}
		}
	})
});
