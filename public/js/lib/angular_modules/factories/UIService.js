define(['jquery', 'jquery', 'classes/PageDecorator', 'easyResponsiveTabs'], function($, jQuery){

	function UIService(){
		this.decorator = new PageDecorator($);

		this.hideNotification = function(){
				this.decorator.autoHide('.alert', 4000, true);
		}

		this.loginRedirect = function(){
				
		}

		this.setupTabs = function (elm){
			this.decorator.activateEasyTab('horizontalTab1', false);
		}

	}

	return {
		UIService : new UIService()
	};
});