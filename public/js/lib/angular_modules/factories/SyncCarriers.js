define(['angular'], function(){
	'use strict';
	
	angular.module('SyncCarriers', [])
	//factory to work on carriers
	.factory('SyncCarriers', function($http){
		return {
			getStepProfileDetails: function(){
				return $http.get('/front/client-register?action=getProfileDetails')
					.error(function(data, status, config, headers){
						alert(status);
					})
			},
			getStepBillingDetails: function(){
				return $http.get('/front/client-register?action=getBillingDetails')
					.error(function(data, status, config, headers){
						alert(status);
					})
			},
			getInsuranceForCarrierLiability: function(scac, currentIndex){
				var url = '/front/carrier-service?action=getCarrierLiablityInsurance&scac=' + scac;
				url = currentIndex !== undefined ? url + '&currentIndex=' + currentIndex : url;
				
				return $http.get(url)
					.error(function(data, status, config, headers){
						alert(status);
					})
			},
			
			/* fetch: function(withSteup){
				if(withSteup === undefined)
					withSteup = false;

				var url = '/front/client-register?action=getSelectedCarriers';
				
				if(withSteup)
					url += '&withCarrierSetup=true';
				
				return $http.get(url)
					.success(function(result){
						return result;
					})
					.error(function(data, status){
						alert(status);
					});
			}, */
			
			fetch: function(withDetails, searchString, onlyApiEnabled, limit, clientID,ProfileId){
					var url = '/front/client-register?action=getSelectedCarriers';
					url = (withDetails !== undefined &&  withDetails == true) ? url + '&withCarrierSetup=true' : url;
					url = (searchString !== undefined &&  withDetails.length) ? url + '&q='+ searchString : url;
					url = (onlyApiEnabled !== undefined &&  onlyApiEnabled == true) ? url + '&apiEnabled=true' : url;
					url = (limit !== undefined &&  !isNaN(limit) && limit) ? url + '&limit=' + limit : url;
					url = (clientID !== undefined &&  !isNaN(clientID) && clientID) ? url + '&clientID=' + clientID : url;
					url = (ProfileId !== undefined &&  (ProfileId!='')) ? url + '&ProfileId=' + ProfileId : url;
					
					return $http.get(url)
						.success(function(result){
							return result;
						})
						.error(function(data, status){
							alert(status);
						});
				},
				
			fetchAccessorial: function(filter){
				var url = '/front/client-register?action=getCarrierAccessorials';
				return $http.get(url)
					.success(function(result){
						return result;
					})
					.error(function(data, status){
						alert(status);
					});
			},
			fetchItemTypes: function(filter){
				var url = '/front/client-register?action=getItemTypes';
				return $http.get(url)
					.success(function(result){
						return result;
					})
					.error(function(data, status){
						alert(status);
					});
			},
			
			validateAllSteps: function(){
				return $http.get('/front/client-register?action=validateAllSteps')
					.error(function(data, status, config, headers){
						alert(status);
					})
			},

			finishAndExit: function(){
				return $http.get('/front/client-register?action=completeRegSteps')
					.error(function(data, status, config, headers){
						alert(status);
					})
			}
		}
	})
});
