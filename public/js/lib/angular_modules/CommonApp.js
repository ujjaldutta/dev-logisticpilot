define([
	'angucomplete-alt',
	'angular-google-places-autocomplete',
	'angular_modules/controllers/CommonAppController',
], function(){
	var app = angular.module('CommonApp', ['angucomplete-alt', 'google.places', ])
		
		// our controller for the form
		// =============================================================================
		.controller('CommonAppController', CommonAppController);
		
});