define([
	'angular_modules/apiSetupApp',
	'angucomplete-alt',
	'angularjs-dropdown-multiselect',
	'angular-google-places-autocomplete',
	'angular_modules/controllers/CarrierEquipmentSidebarController',
	'angularjs-datepicker',
	'ui-bootstrap',
], function(){
	var app = angular.module('CarrierEquipmentSidebarApp', ['apiSetupApp', 'angucomplete-alt', 
			'google.places',  'angularjs-dropdown-multiselect','720kb.datepicker', 
		])
		.value('CarrierEquipmentEndPoint', {
			base: '/front/setup-connectivity/',
			listAccessorial: 'list-carrier-accessorials/',
			listFuel: 'list-carrier-fuel/',
		})
		
		// our controller for the form
		// =============================================================================
		.controller('CarrierEquipmentSidebarController', CarrierEquipmentSidebarController);
});