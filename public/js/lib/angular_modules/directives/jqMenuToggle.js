define(
	['jquery', 'angular', ], 
	function($){
	var module = angular.module('jqMenuToggle', [])
		.directive('menuToggle', function(){
			return {
				restrict: 'AC',
				link: function(scope, element, attrs){
					//console.log($(element));						
                    element.bind('click', function(event){
					 $(element).find('ul').slideToggle();
					 event.stopPropagation();
					});					
				}
			};
		})
});