'use strict'

define(['angular',], function(){
 var gmapdirection = angular.module('gmapdirection', []);
     gmapdirection.directive('gmapDirection', function(){
	   var directive = {};
	       directive.restrict = 'E';
		   //directive.require = '^?tabinfo';
		   directive.scope = {origin:'@', destination:'@',shipidval:'@',};
		   directive.templateUrl = '/templates/direction.html';
		   directive.controller = function($scope){ 
		        $scope.distance = 0;
				$scope.calculateAndDisplayRoute = function(directionsService, directionsDisplay, origin, destination, shipid) {
						/*var waypts = [];
						var checkboxArray = document.getElementById('waypoints');
						for (var i = 0; i < checkboxArray.length; i++) {
						  if (checkboxArray.options[i].selected) {
							waypts.push({
							  location: checkboxArray[i].value,
							  stopover: true
							});
						  }
						}*/

						directionsService.route({
						  origin: origin,
						  destination: destination,
						  //waypoints: waypts,
						  //optimizeWaypoints: true,
						  travelMode: google.maps.TravelMode.DRIVING
						}, function(response, status) {
						  if (status === google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(response); 
							
							var route = response.routes[0];
							$scope.distance = route.legs[0].distance.text;
							angular.element('#dis_'+shipid).html('<h2>DISTANCE : '+$scope.distance+'</h2>');
							console.log('i am here'); console.log($scope.$parent.shipment);
							var dis = $scope.distance.split(' ')[0];
							dis = dis.replace(',','').trim();
							$scope.$parent.shipment.shp_distance = dis;
							/*if(!angular.element('#'+shipid+'_distance').val()){
							 var dislist = parseDouble($scope.distance.split(' ')[0]);
							 angular.element('#'+shipid+'_distance').val(dislist).trigger('input');
							 }*/
							//alert($scope.distance);
							/*var summaryPanel = angular.element('#directions-panel');
							summaryPanel.innerHTML = '';
							// For each route, display summary information.
							for (var i = 0; i < route.legs.length; i++) {
							  var routeSegment = i + 1;
							  summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
								  '</b><br>';
							  summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
							  summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
							  summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
							}*/
						  } else {
							window.alert('Directions request failed due to ' + status);
						  }
						});
					  }		    
		   };
		   directive.compile = function(elem, attr){
		     return{
                 pre: function preLink(scope, iElement, iAttrs, ctrl) { 
				  elem.find('div.boston').html('<div id="map_direction'+attr.shipidval+'" style="width:100%; height:300px;" class="img-responsive"></div><div class="filter-head" style="padding:0; text-align:right;" id="dis_'+attr.shipidval+'"><h2>DISTANCE : '+scope.distance+'</h2></div>');
				 },
                 post: function postLink(scope, iElement, iAttrs, ctrl) { 
				 //scope.$emit('distance_value','abc');
				 //console.log('hiiiiiiiiiii');

                   var directionsService = new google.maps.DirectionsService;
                    var directionsDisplay = new google.maps.DirectionsRenderer;
					var map = new google.maps.Map(document.getElementById('map_direction'+attr.shipidval), {
					  zoom: 6,
					  scrollwheel: false,
					  center: {lat: 41.850033, lng: -87.6500523}
					}); 
					directionsDisplay.setMap(map);
					scope.calculateAndDisplayRoute(directionsService, directionsDisplay, attr.origin, attr.destination, attr.shipidval);
					
					//var dislist = parseInt(scope.distance.split(' ')[0]);
					
					//console.log('here1');
					//console.log(scope);
				 }
			 }
		   };
		   
		   return directive;
	 });
});