define(
	['angular'], 
	function($){
	var module = angular.module('shipmentsDirs', [])
		.directive('iCheck', function($timeout) {
			return {
			require:'ngModel',
			  link: function(scope, element, attrs, ngModel, ctrl) {
				  element.bind('click', function() { 
						console.log(attrs);
						var elid = attrs.id;
						var elids = elid.split('_');  
					if (element[0].checked) {
					   //scope.$apply(function() { 
					   console.log(elids);
					   scope.shpproducts[elids[2]-1].prod_itemhazmat = true;
					   console.log(scope.shpproducts[elids[2]-1]);
					  //});
					} else {
					 console.log(elids);
					 scope.shpproducts[elids[2]-1].prod_itemhazmat = false;
					 console.log(scope.shpproducts[elids[2]-1]);
					}
				  });
			  }
			};
        })
        .directive('loadjsfiles', function(ShipmentAPIEndpoint){
		  var directive = {};
		  
		  directive.restrict = 'E';
		  directive.link = function(scope, elem, attrs){
		   elem.bind('click', function(){ 
				/*$('.timepicker-box').timepicker({
					minuteStep: 5,
					showInputs: false,
					disableFocus: true
				
				}).on('changeTime.timepicker', function(e) {
					$(this).trigger('input');
				});	    */
				
		   });
		  }
		  directive.templateUrl = ShipmentAPIEndpoint.base+'jscripts-after-dom-load';
		  return directive;
		})	
        .directive('customercost', function(ShipmentAPIEndpoint){
		 var directive = {};
		 directive.restrict = 'E';
         directive.scope = {};
		 directive.require = '^parentDir';
		 
 		 directive.templateUrl=ShipmentAPIEndpoint.base+'customer-cost-directory-view';
		 directive.link = function(scope, elem, attr, parentDirCtrl){ 
		 console.log(elem);
		  scope.listc = {customernc:[{},{}]}; 
		  elem.find('button.b1').bind('click',function(){ alert('b1');
		   scope.listc.customernc.push({});
		   console.log(scope);	
		   /*p_scope = parentDirCtrl.getParentScope();
		   p_scope.cc_cost.push(scope.listc);
		   console.log(p_scope.cc_cost);*/
		   scope.$apply();
		  });
		  
		 elem.find('button.b2').bind('click',function(){
		  alert('b2');
		 }); 
		 
		 elem.find('a.a1').bind('click',function(){
		  alert('a1');
		 }); 		 
		 };
		 return directive;
		})
		.directive('removeCustomerCostItem', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.listc.customernc.splice(attr.id, 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})
		.directive('removeCarrierCostItem', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.listcr.carriernc.splice(attr.id, 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})		
        .directive('carriercost', function(ShipmentAPIEndpoint){
		 var directive = {};
		 directive.restrict = 'E';
         directive.scope = {};
		 
 		 directive.templateUrl=ShipmentAPIEndpoint.base+'carrier-cost-directory-view';
		 directive.link = function(scope, elem, attr){ 
		  scope.listcr = {carriernc:[{},{}]}; 
		  elem.find('button').bind('click',function(){
		   scope.listcr.carriernc.push({});
		   console.log(scope.listcr);	
		   scope.$apply();
		  });
		 };
		 return directive;
		})
		.directive('parentDir', function(){
		 var directive = {};
		 directive.restrict = 'E';
		 directive.scope = true;
		 directive.controller = function($scope){
		  this.getParentScope = function(){
		    console.log($scope);
		   return $scope;
		  }
		 }
		 //directive.template = '<customernotes></customernotes>';
		 return directive;
		})
        .directive('customernotes', function(ShipmentAPIEndpoint, $templateCache){
		 var directive = {};
		 directive.restrict = 'E';
		 directive.require = '^parentDir';
		 directive.scope = {};
 		 directive.templateUrl=ShipmentAPIEndpoint.base+'customer-notes-view';
		 directive.link = function(scope, elem, attr, parentDirCtrl){
         //console.log(parentDirCtrl.getData());	
         var local_scope = parentDirCtrl.getParentScope();	
         if(local_scope.customernotes.length) 		 
		  scope.customernotes = {notes:local_scope.customernotes};
		 else
          scope.customernotes = {notes:[]};	 
		  
		  console.log(scope.customernotes.notes);
		  elem.find('button').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' ');
		   if(scope.customer_notes.note_desc.trim() != ''){	
			   scope.customernotes.notes.push({
			   'ship_notes':scope.customer_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':1, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   scope.customer_notes.note_desc = '';
			   $templateCache.remove(directive.templateUrl);
			   scope.$apply();
		   }
		  });
		 };
		 return directive;
		})	
        .directive('carriernotes', function(ShipmentAPIEndpoint, $templateCache){
		 var directive = {};
		 directive.restrict = 'E';
		 directive.require = '^parentDir';
		 directive.scope = {};
 		 directive.templateUrl=ShipmentAPIEndpoint.base+'carrier-notes-view';
		 directive.link = function(scope, elem, attr, parentDirCtrl){
         //console.log(parentDirCtrl.getData());	
         var local_scope = parentDirCtrl.getParentScope();	
         if(local_scope.carriernotes.length)   		 
		  scope.carriernotes = {notes:local_scope.carriernotes};
		 else
          scope.carriernotes = {notes:[]}
		 
		  console.log(scope.carriernotes.notes);
		  elem.find('button').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' ');
		   if(scope.carrier_notes.note_desc.trim() != ''){	
			   scope.carriernotes.notes.push({
			   'ship_notes':scope.carrier_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':2, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   $templateCache.remove(directive.templateUrl);
			   scope.carrier_notes.note_desc = '';
			   scope.$apply();
		   }
		  });
		 };
		 return directive;
		})	
        .directive('terminalnotes', function(ShipmentAPIEndpoint, $templateCache){
		 var directive = {};
		 directive.restrict = 'E';
		 directive.require = '^parentDir';
		 directive.scope = {};
 		 directive.templateUrl=ShipmentAPIEndpoint.base+'terminal-notes-view';
		 directive.link = function(scope, elem, attr, parentDirCtrl){ 
         //console.log(parentDirCtrl.getData());	
         var local_scope = parentDirCtrl.getParentScope();	
         if(local_scope.terminalnotes.length) 		 
		  scope.terminalnotes = {notes:local_scope.terminalnotes};
		 else
 		  scope.terminalnotes = {notes:[]};
		  
		  console.log(scope.terminalnotes.notes);
		  elem.find('button').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' '); 
           if(scope.terminal_notes.note_desc.trim() != ''){		   
			   scope.terminalnotes.notes.push({
			   'ship_notes':scope.terminal_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':3, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   console.log(scope.terminalnotes.notes);
			   scope.terminal_notes.note_desc = '';
			   $templateCache.remove(directive.templateUrl);
			   scope.$apply();
		   }
		  });
		 };
		 return directive;
		})		
		.directive('removeCustomerNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.customernotes.notes.splice(scope.customernotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})
		.directive('removeCarrierNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.carriernotes.notes.splice(scope.carriernotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})		
		.directive('removeTerminalNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.terminalnotes.notes.splice(scope.terminalnotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})	
});