define(
	['jquery', 'angular', 'jqTimePicker', 'jqDatePicker',], 
	function($){
	var module = angular.module('jq.dateTimePicker', [])
		.directive('jqdatetimepicker', function(){
			return {
				restrict: 'AC',
				link: function(scope, element, attrs){
					//console.log($(element));						
						$(element).timepicker({
							minuteStep: 5,
							showInputs: false,
							disableFocus: true
						
						}).on('changeTime.timepicker', function(e) {
							$(this).trigger('input');
						});						
				}
			};
		})
		.directive('jqdatepicker', function(){
			return {
				restrict: 'AC',
				link: function(scope, element, attrs){ 
					//console.log($(element));							
                     $('#'+attrs.id).datepicker({onSelect:function(){$(this).trigger('input'); }});
		
				}
			};
		});		
});