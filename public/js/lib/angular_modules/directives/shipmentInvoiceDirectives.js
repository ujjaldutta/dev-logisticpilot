define(
	['jquery','angular'], 
	function($){
	var module = angular.module('shipmentsDirs', [])
		.directive('iCheck', function($timeout) {
			return {
			require:'ngModel',
			  link: function(scope, element, attrs, ngModel, ctrl) {
				  element.bind('click', function() { 
						console.log(attrs);
						var elid = attrs.id;
						var elids = elid.split('_');  
					if (element[0].checked) {
					   //scope.$apply(function() { 
					   console.log(elids);
					   scope.shpproducts[elids[2]-1].prod_itemhazmat = true;
					   console.log(scope.shpproducts[elids[2]-1]);
					  //});
					} else {
					 console.log(elids);
					 scope.shpproducts[elids[2]-1].prod_itemhazmat = false;
					 console.log(scope.shpproducts[elids[2]-1]);
					}
				  });
			  }
			};
        })
        .directive('iChecktab', function($timeout) {
			return {
			require:'ngModel',
			  link: function(scope, element, attrs, ngModel, ctrl) {
				  element.bind('click', function() { 
						console.log(attrs);
						var elid = attrs.id;
						var elids = elid.split('_');  
					if (element[0].checked) {
					   //scope.$apply(function() { 
					   console.log(elids);
					   scope.products[elids[3]].prod_itemhazmat = true;
					   console.log(scope.products[elids[3]]);
					  //});
					} else {
					 console.log(elids);
					 scope.products[elids[3]].prod_itemhazmat = false;
					 console.log(scope.products[elids[3]]);
					}
				  });
			  }
			};
        })		
        .directive('tabinfo', function(ShipmentAPIEndpoint, $templateCache, CustomerProduct, Shipment){
		 var directive = {};
		 directive.scope = {id : '@'};

		 directive.restrict = 'E';
		 directive.require = '^parentDir';
		 directive.templateUrl = ShipmentAPIEndpoint.base+'jscripts-invoice-after-dom-load';		 
		 
		 directive.link = function(scope, elem, attr, parentDirCtrl){
		 
		  Shipment.makeAsyncRequest(Shipment.getShipmentDataStatusUser(attr.id))
			.then(function(response){
				if(response.data.success !== undefined && response.data.shipment_status_user !== undefined){
					scope.shipStatusUser = response.data.shipment_status_user;
				}
			}, function(reason){
				console.warn(reason);
			});

		  Shipment.makeAsyncRequest(Shipment.getShipmentFiles(attr.id))
			.then(function(response){
				if(response.data.success !== undefined && response.data.shipment_files !== undefined){
					//scope.shipStatusUser = response.data.shipment_files;
					scope.shipFiles = response.data.shipment_files;
			        //scope.shipDocs = response.data.shipment_files;
				}
			}, function(reason){
				console.warn(reason);
			});	

		  Shipment.makeAsyncRequest(Shipment.getShipTrackingList(attr.id))
			.then(function(response){
				if(response.data.success !== undefined && response.data.track_list !== undefined){
					scope.shipTracking = response.data.track_list;
				}
			}, function(reason){
				console.warn(reason);
			});	

		  Shipment.makeAsyncRequest(Shipment.getShipmentTerminalData(attr.id))
			.then(function(response){
				if(response.data.success !== undefined && response.data.terminal_data !== undefined){
					scope.terminalData = response.data.terminal_data[0];
					//alert(scope.terminalData.trm_orginname);
					//console.log(scope.terminalData);
					//return;
				}
			}, function(reason){
				console.warn(reason);
			});			
		 
		  CustomerProduct.makeAsyncRequest(CustomerProduct.getPackageTypes())
			.then(function(response){
				if(response.data.success !== undefined && response.data.packageTypes !== undefined){
					scope.packageTypes = response.data.packageTypes;
				}
			}, function(reason){
				console.warn(reason);
			});		

		   CustomerProduct.makeAsyncRequest(CustomerProduct.getClassTypes())
			.then(function(response){
				if(response.data.success !== undefined && response.data.classTypes !== undefined){
					scope.classTypes = response.data.classTypes;
				}
			}, function(reason){
				console.warn(reason);
			});			
		 
		 	scope.error = {
				status: false,
				message: '',
			};

			scope.success = {
				status: false,
				message: '',
			};		 
				 
         var local_scope = parentDirCtrl.getParentScope();
         //console.log(local_scope);
          //scope.stop = [];		 
		  scope.shipmentData = local_scope.shipment;
		  scope.products = local_scope.products;
		  scope.stops = local_scope.stops;
		  scope.b2btypes = local_scope.b2btypes;
		  scope.colorclasslist = local_scope.colorclasslist;
		  scope.isdisableproduct_list = true;
		  scope.loginuserid = local_scope.login_user_id;
		  scope.shipStatus = local_scope.shipStatus;
		  
			  scope.hideStop = false;

			  if(scope.shipmentData.shp_modeId == '2')
			   scope.hideStop = false;
			  else
               scope.hideStop = true;		  
		  
		  
			scope.addStopRow_list = function(){
			 scope.stops.push({"id":"","shipId":"","stopId"
				:"","stop_prodId":'',"stop_name":"","stop_adr1":"","stop_adr2":"","stop_city":"","stop_state":"","stop_postal"
				:"","stop_country":"","stop_email":"","stop_contactname":"","stop_contactphone":"","stop_prodqty":0,"stop_ponumber"
				:0,"stop_type":"","created_at":"2016-03-04 11:44:02","updated_at":"2016-03-04 11:44:02"
               });
			}

			scope.checkedBox = function(index){
			 //console.log(scope.stop);
			 if(scope.stops[index].chk_list == true){
			  angular.element('#s_'+attr.id+'_'+index+'_1_value').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'1').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'2').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'3').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'4').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'5').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'6').prop('disabled', false);
			  angular.element('#s'+attr.id+index+'7').prop('disabled', false);
			 } else {
			  angular.element('#s_'+attr.id+'_'+index+'_1_value').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'1').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'2').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'3').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'4').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'5').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'6').prop('disabled', true);
			  angular.element('#s'+attr.id+index+'7').prop('disabled', true);
			 }
			}				
				
			scope.locationSelected = function(colobj){ //alert('ddd');
			console.log(colobj);	 
			if(this.inputName == 'stop_name_auto[]'){
				if(typeof colobj !== 'undefined'){
					var fieldid = this.id;
					var fieldid_seg = fieldid.split('_'); 
					var counter = parseInt(fieldid_seg[2]);	  
					var shipid = parseInt(fieldid_seg[1]);

					 angular.element('#s'+shipid+counter+'2').val(colobj.originalObject.location_adr1).trigger('input');
					 angular.element('#s'+shipid+counter+'3').val(colobj.originalObject.location_adr2).trigger('input');
					 angular.element('#s'+shipid+counter+'4').val(colobj.originalObject.location_city).trigger('input');
					 angular.element('#s'+shipid+counter+'5').val(colobj.originalObject.location_state).trigger('input');
					 angular.element('#s'+shipid+counter+'6').val(colobj.originalObject.location_postal).trigger('input');
					 angular.element('#s'+shipid+counter+'1').val(colobj.title).trigger('input');
					 //angular.element('#pick_location_postal').val("01.05.2012").datepicker('update');
					 //angular.element('#pick_location_adr1').val( addr_data[0]);
					 //angular.element('#pick_location_adr1').val( addr_data[0]);
				 }	 
			 }
			}

			scope.getInput = function(txt){
			 console.log(this.id);
			var fieldid = this.id;
			var fieldid_seg = fieldid.split('_'); 
			var counter = parseInt(fieldid_seg[2]);	  
            var shipid = parseInt(fieldid_seg[1]);			
			angular.element('#s'+shipid+counter+'1').val(txt).trigger('input');
			}				
				
				
		   scope.removeStopList = function(index){
		    scope.stops.splice(index, 1);
		   }
		   
		   
			scope.addProductRow_list = function(){
			 scope.products.push({"id":"","shipId":"","prod_srlno"
				:"","prod_itemcode":"","prod_itemname":"","prod_itempieces":"","prod_itemqty":"","prod_itempkgtypeId"
				:"","prod_itemclass":"0","prod_itemweight":"","prod_itemwidth":"","prod_itemheight":"","prod_itemlength"
				:"","prod_stopId":"","prod_itemhazmat":"","prod_itemnmfc":"","prod_cubefeet":"","prod_orderId":"","prod_accountcode"
				:"","prod_ponumber":"","created_at":"2016-03-11 08:40:46","updated_at":"2016-03-11 08:40:46"
               });
			}

			scope.checkedBoxProduct = function(index){
			 if(scope.products[index].chk_list_p == true){
			  //scope.isdisableproduct_list = false;
			  angular.element('#p_'+attr.id+'_'+index+'_3_value').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'1').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'2').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'3').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'4').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'5').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'6').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'7').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'8').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'9').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'10').prop('disabled', false);
			  angular.element('#prod_itemhazmat_'+attr.id+'_'+index+'_11').prop('disabled', false);
			  angular.element('#p'+attr.id+index+'12').prop('disabled', false);
			 } else {
			  angular.element('#p_'+attr.id+'_'+index+'_3_value').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'1').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'2').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'3').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'4').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'5').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'6').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'7').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'8').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'9').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'10').prop('disabled', true);
			  angular.element('#prod_itemhazmat_'+attr.id+'_'+index+'_11').prop('disabled', true);
			  angular.element('#p'+attr.id+index+'12').prop('disabled', true);
			 }
			}				

			 scope.itemSelected = function(colobj){
					//console.log(colobj.originalObject);
					var fieldid = this.id;
					var fieldid_seg = fieldid.split('_'); 
					var counter = parseInt(fieldid_seg[2]);	 
					var shipid = parseInt(fieldid_seg[1]);
					
					if(this.inputName == 'product.unit_desc[]'){ //alert('dd');
						if(typeof colobj !== 'undefined'){
							 angular.element('#p'+shipid+counter+'6').val(colobj.originalObject.item_length).trigger('input');
							 angular.element('#p'+shipid+counter+'7').val(colobj.originalObject.item_width).trigger('input');
							 angular.element('#p'+shipid+counter+'8').val(colobj.originalObject.item_height).trigger('input');
							 angular.element('#p'+shipid+counter+'10').val(colobj.originalObject.item_weight).trigger('input');
							 angular.element('#p'+shipid+counter+'5').val(colobj.originalObject.item_qty).trigger('input');
							 angular.element('#p'+shipid+counter+'13').val(colobj.title).trigger('input');
							 
						 }
			 } 
			}			

			scope.getProductInput = function(txt){
			console.log(this.id);
			var fieldid = this.id;
			var fieldid_seg = fieldid.split('_'); 
			var counter = parseInt(fieldid_seg[2]);	  
			var shipid = parseInt(fieldid_seg[1]);
			
			 angular.element('#p'+shipid+counter+'13').val(txt).trigger('input');
			}	
			
		   scope.removeProductList = function(index){
		    scope.products.splice(index, 1);
		   }		   
		  
		  //console.log('here');
	      console.log(scope.products); 
         if(local_scope.accessorials_cc.customernc.length)
		  scope.listc = {customernc:local_scope.accessorials_cc.customernc};
		 else
		  scope.listc = {customernc:[{id:'',accs_scac:'',accs_code:'',accs_ratetype:2},{accs_scac:'',accs_code:'',accs_ratetype:2}]}; 
		
		 if(local_scope.accessorials_crc.carriernc.length) 
		  scope.listcr = {carriernc:local_scope.accessorials_crc.carriernc}; 
		 else
		  scope.listcr = {carriernc:[{id:'', accs_scac:'',accs_code:'',accs_ratetype:3},{accs_scac:'',accs_code:'',accs_ratetype:3}]};
           		
		scope.getTotalcc_val = 0;	
        scope.getTotalcr_val = 0;		
		scope.getTotalcc = function(){ 
			var total = 0;
			for(var i = 0; i < scope.listc.customernc.length; i++){
				//var product = scope.listc.accs_ratedcost;
				var store_val = 0;
				if(isNaN(parseFloat(scope.listc.customernc[i].accs_customercost))){
				  scope.listc.customernc[i].accs_customercost = '';
				  store_val = 0;
				  } else {
				  store_val = scope.listc.customernc[i].accs_customercost;
				  }
				
				total += parseFloat(store_val); 
			}
			scope.getTotalcc_val = isNaN(total.toFixed(2))?0:total.toFixed(2);
		}	
		
		scope.getTotalcr = function(){
			var total = 0;
			for(var i = 0; i < scope.listcr.carriernc.length; i++){
				//var product = scope.listcr.accs_ratedcost;
				var store_val = 0;
				if(isNaN(parseFloat(scope.listcr.carriernc[i].accs_ratedcost))){
				  scope.listcr.carriernc[i].accs_ratedcost = '';
				  store_val = 0;
                  } else {
				  store_val = scope.listcr.carriernc[i].accs_ratedcost;
				  }				  
				
				total += parseFloat(store_val); 
			}
			scope.getTotalcr_val = isNaN(total.toFixed(2))?0:total.toFixed(2);
		}
		scope.getTotalcc();
        scope.getTotalcr(); 		
				
         if(local_scope.accessorials_cc.customernc.length)
		  scope.ini_cc = local_scope.accessorials_cc_ini[0];
		 else
		  scope.ini_cc = local_scope.accessorials_ccccr_deafult[0]; 
		
		 if(local_scope.accessorials_crc.carriernc.length) 
		  scope.ini_cr = local_scope.accessorials_crc_ini[0]; 
		 else
		  scope.ini_cr = local_scope.accessorials_ccccr_deafult[0];
				
         if(local_scope.customernotes.length) 		 
		  scope.customernotes = {notes:local_scope.customernotes};
		 else
          scope.customernotes = {notes:[]};	 		 
		 
         if(local_scope.carriernotes.length)   		 
		  scope.carriernotes = {notes:local_scope.carriernotes};
		 else
          scope.carriernotes = {notes:[]}		 
		 
         if(local_scope.terminalnotes.length) 		 
		  scope.terminalnotes = {notes:local_scope.terminalnotes};
		 else
 		  scope.terminalnotes = {notes:[]};			  
		  
		  elem.find('button.add_cc').bind('click',function(){
		   if(scope.listc.customernc.length){
		   var pcv = scope.listc.customernc[scope.listc.customernc.length-1].accs_scac;
		   var crname = angular.element('#'+elem.attr('id')+'_'+(scope.listc.customernc.length-1)+'_cus_1_value').val();
		   }else{
		   var pcv = '';
		   var crname = '';
		   }
		   
		   scope.listc.customernc.push({accs_scac:pcv,accs_code:'',accs_ratetype:2,carrier_name:crname});
		   
		   scope.$apply();
		  });
		  elem.find('button.add_cr').bind('click',function(){
		   if(scope.listcr.carriernc.length){
		   var pcv = scope.listcr.carriernc[scope.listcr.carriernc.length-1].accs_scac;
		   var crname = angular.element('#'+elem.attr('id')+'_'+(scope.listcr.carriernc.length-1)+'_cr_1_value').val();
		   }else{
		   var pcv = '';
		   var crname = '';
		   }
		   
		   scope.listcr.carriernc.push({accs_scac:pcv,accs_code:'',accs_ratetype:3,carrier_name:crname});
		   scope.$apply();
		  });
		  
		  elem.find('button.btn_cnote').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' ');
		   if(scope.customer_notes.note_desc.trim() != ''){	
			   scope.customernotes.notes.push({
			   'ship_notes':scope.customer_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':1, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   scope.customer_notes.note_desc = '';
			   $templateCache.remove(directive.templateUrl);
			   scope.$apply();
		   }
		  });	
		 
		  elem.find('button.btn_crnote').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' ');
		   if(scope.carrier_notes.note_desc.trim() != ''){	
			   scope.carriernotes.notes.push({
			   'ship_notes':scope.carrier_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':2, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   $templateCache.remove(directive.templateUrl);
			   scope.carrier_notes.note_desc = '';
			   scope.$apply();
		   }
		  });
		  
		  console.log(scope.terminalnotes.notes);
		  elem.find('button.btn_trnote').bind('click',function(){
		   var user_name = local_scope.login_user_name.split(' '); 
           if(scope.terminal_notes.note_desc.trim() != ''){		   
			   scope.terminalnotes.notes.push({
			   'ship_notes':scope.terminal_notes.note_desc.replace(/(?:\r\n|\r|\n)/g, '<br />'),  
			   'ship_notestypeId':3, 
			   'ship_createdbyId':local_scope.login_user_id,
			   'note_post_by' : local_scope.login_user_name,
			   'ship_createddate' : 'Today',
			   'usr_firstname':user_name[0],
			   'usr_lastname':user_name[1],		   
			   });
			   console.log(scope.terminalnotes.notes);
			   scope.terminal_notes.note_desc = '';
			   $templateCache.remove(directive.templateUrl);
			   scope.$apply();
		   }
		  });		  
		  
		  elem.find('button.submit_save').bind('click', function(){ 
			//console.log(scope); return
			var data = {
				shipment: scope.shipmentData,
				customer_cost: scope.listc,
				carrier_cost: scope.listcr,
				customer_notes : scope.customernotes.notes,
				carrier_notes : scope.carriernotes.notes,
				internal_notes : scope.terminalnotes.notes,
				products : scope.products,
				stops : scope.stops,
				ship_status_user : scope.shipStatusUser,
				ship_doc : scope.shipFiles,
				ship_track : scope.shipTracking,
			};			
			
			parentDirCtrl.saveData(scope.id, data, elem, scope);
		  });
		  
		 }
		 directive.controller = function($scope){
           // $scope.b2bcolor = $scope.getrandindex();				
		      $scope.shipStatusUser = [];
			  $scope.shipFiles = [];
			  $scope.shipTracking = [];
			  $scope.terminalData = [];
			  //$scope.shipDocs = [];
			  $scope.upl_disable = true;
			  //$scope.shipmentData.file = [];
			  $scope.loginuserid = '';
			  	
			$scope.getInputStop = function(txt){
			 console.log('gggggg'+this.id);
			var fieldid = this.id;
			var fieldid_seg = fieldid.split('_'); 
			//var counter = parseInt(fieldid_seg[3]);	  console.log(counter);		 
			 angular.element('#'+fieldid_seg[1]+fieldid_seg[2]+'1').val(txt).trigger('input');
			}
				
					  
			  $scope.$on("distance_value", function (evt, data) {
               alert('here'+data);
              });		  
					  
			  $scope.addTrack = function(){
			  //console.log($scope);
			   var ymd = new Date();
			   var month = ymd.getUTCMonth() + 1; //months from 1-12
			   	   if(month.toString().length == 1)
			        month = '0'+month;
               var day = ymd.getUTCDate()-1;
			       if(day.toString().length == 1)
			        day = '0'+day;
               var year = ymd.getUTCFullYear();

			   var h = ymd.getHours();
			   if(h.toString().length == 1)
			    h = '0'+h;
               var m = ymd.getMinutes();
			   if(m.toString().length == 1)
			    m = '0'+m;			   
               var s = ymd.getSeconds();
			   if(s.toString().length == 1)
			    s = '0'+s;		
				
			   var trk = {id:'',
			    shipId:$scope.shipmentData.id,
				track_date:'',
				track_time:'',
				track_statusId:$scope.shipmentData.track_statusId,
				shipstatus:{track_statusText:document.getElementById('track_statusId'+$scope.shipmentData.id).options[document.getElementById('track_statusId'+$scope.shipmentData.id).selectedIndex].text},
				track_text:$scope.shipmentData.track_text,
				track_city:'test',
				track_state:'test',
				track_trailer:'test',
				track_createdbyId:$scope.loginuserid,
				track_createdDate:year + '-' + month + '-' + day + ' ' + h + ':' + m + ':' + s
			   };
			   //alert($scope.shipmentData.track_statusId);
			   if(typeof $scope.shipmentData.track_statusId != 'undefined' && $scope.shipmentData.track_statusId != ''){
			    $scope.shipTracking.push(trk);
				$scope.shipmentData.track_statusId = '';
				$scope.shipmentData.track_text = '';
				}
			  }
			  
			  $scope.removeTrack = function(key){
			   $scope.shipTracking.splice(key, 1);
			  }
			  
              $scope.removeStatusNote = function(entryid){
			    //alert($scope.shipStatusUser.length);
				$scope.shipStatusUser.splice(entryid, 1);
			  }  
			  $scope.getInput1 = function(txt){ 
			   console.log(txt);
			   $('#carrier_name_'+this.id).val(txt).trigger('input');
			  }
			  $scope.getInput2 = function(txt){ 
			   $('#cost_desc_'+this.id).val(txt).trigger('input');
			  }
			  $scope.getInput3 = function(txt){ 
			   console.log(txt);
				$('#carrier_name_'+this.id).val(txt).trigger('input');
			  }		
			  $scope.getInput4 = function(txt){ 
			   console.log(txt);
			   $('#cost_desc_'+this.id).val(txt).trigger('input');
			  }	

			  
 
			  
		  $scope.valueSelected = function(colobj){
			console.log(colobj);
			if(this.inputName == 'cnameauto[]'){
			  if(typeof colobj !== 'undefined'){
               $('#carrier_name_'+this.id).val(colobj.originalObject.scac).trigger('input');
			  }
			}
			if(this.inputName == 'ccost_descauto[]'){
			  if(typeof colobj !== 'undefined'){
                 $('#cost_desc_'+this.id).val(colobj.originalObject.accscode).trigger('input');
			  }
			}
			if(this.inputName == 'crnameauto[]'){
			  if(typeof colobj !== 'undefined'){
                 $('#carrier_name_'+this.id).val(colobj.originalObject.scac).trigger('input');
			  }
			}
			if(this.inputName == 'crcost_descauto[]'){
			  if(typeof colobj !== 'undefined'){
                 $('#cost_desc_'+this.id).val(colobj.originalObject.accscode).trigger('input');
			  }
			}			
			}
		  
		 };
		 return directive;
		})	
		.directive('fileUpload', function($parse, Shipment){
		  var directive = {};
		  directive.restrict = 'A';
		  
		  directive.controller = function($scope){
		    $scope.removeFile = function(key){
			 $scope.shipFiles.splice(key, 1);
			}
		    $scope.uploadfile = function(){
			  console.log($scope);
			  angular.element('.loading_upl'+$scope.shipmentData.id).removeClass('hidden');
			  angular.element('.upl'+$scope.shipmentData.id).addClass('hidden');
			  var fd = new FormData();
		      fd.append('file', $scope.file);
		      fd.append("shipID", $scope.shipmentData.id);
			  Shipment.makeAsyncRequest(Shipment.uploadFiles(fd))
				.then(function(response){
					if(response.data.success !== undefined && response.data.fileName !== undefined){
						//$scope.shipDocs.push({doc_name:response.data.fileName});
						$scope.shipFiles.push({id:'',shipId:$scope.shipmentData.id,doc_typeId:'',doc_location:response.data.fileName,doc_createdbyId:'',doc_createddate:''});
						//$scope.$apply();
					}
				}, function(reason){
					console.warn(reason);
				}).finally(function(data){
				 			  angular.element('.loading_upl'+$scope.shipmentData.id).addClass('hidden');
			                  angular.element('.upl'+$scope.shipmentData.id).removeClass('hidden');
				});	
			}
		   	
		  };
		  
		  directive.link = function(scope, elem, attr){
            var model = $parse(attr.fileUpload);
            var modelSetter = model.assign;		  
			
		    elem.bind('change', function(){ console.log(elem[0].files[0]);
			scope.$emit('filedata', elem[0].files[0]);
			scope.upl_disable = false;
			 scope.$apply(function(){
				modelSetter(scope, elem[0].files[0]);
			 });
		   });
		   
		    
		  };
		  return directive;
		})
        .directive('sourceLocation', function(ShipmentAPIEndpoint, Shipment){
		 var directive = {};
		 //directive.scope = false;
		 directive.scope = {id:'@', picklocation:'@',autofillpick:'@', autofilldel:'@', dellocation:'@',phone:'@',shppickphone:'@',shppickcontact:'@',shpdelphone:'@',shpdelcontact:'@',pickaddr1:'@',pickaddr2:'@',deladdr1:'@',deladdr2:'@', };
         directive.strict = 'E';
         directive.templateUrl = ShipmentAPIEndpoint.base+'ajax-source-location'; 
		 
         directive.link = function(scope, elem, attr){ console.log('here'); console.log(attr);
		 
			//google autocomplete config to search specific type of information
			scope.autocompleteOption = {
				componentRestrictions: { country: 'us' },
				types: ["(regions)"]
			}			 
		 
		    scope.location = [];
			
			scope.openBox_s = function(panel, box){ 
			  angular.element('#source'+box).show('slow');
			  angular.element('#s_box'+box).hide('slow');
			}
			
			scope.closeBox_s = function(panel, box){  
			  angular.element('#source'+box).hide('slow');
			  angular.element('#s_box'+box).show('slow');	
			}
			
			scope.openBox_d = function(panel, box){
			  angular.element('#destination'+box).show('slow');
			  angular.element('#d_box'+box).hide('slow');
			}
			
			scope.closeBox_d = function(panel, box){  
			  angular.element('#destination'+box).hide('slow');
			  angular.element('#d_box'+box).show('slow');	
			}			  
		  
		  
		  scope.location.shp_pickupadr1 = attr.pickaddr1;
		  scope.location.shp_pickupadr2 = attr.pickaddr2;
		  
		  scope.location.shp_pickupname = attr.autofillpick;
		  scope.location.shp_deliveryname = attr.autofilldel;
		  scope.location.pick_location = attr.picklocation;
		  scope.location.del_location = attr.dellocation;		  
		  
		  
		  scope.location.shp_pickupphone = attr.shppickphone;
		  scope.location.shp_pickupcontact = attr.shppickcontact;
		  scope.location.shp_deliveryphone = attr.shpdelphone;
		  scope.location.shp_deliverycontact = attr.shpdelcontact;	
		  
		  scope.location.shp_deliveryadr1 = attr.deladdr1;
		  scope.location.shp_deliveryadr2 = attr.deladdr2;		  
		  //scope.location.id = attr.id;
		  
		scope.getPickInput = function(txt){	 
		 angular.element('#shp_pickupname'+attr.id).val(txt).trigger('input');
		}		 
		scope.getDelInput = function(txt){ 
		 angular.element('#shp_deliveryname'+attr.id).val(txt).trigger('input');
		}		
		scope.locationSelected = function(colobj){
		console.log(colobj);
		if(this.inputName == 'shp_pickupnameauto'){
		  if(typeof colobj !== 'undefined'){
			 angular.element('#pick_location'+attr.id).val(colobj.originalObject.location_city+', '+colobj.originalObject.location_state+', '+colobj.originalObject.location_postal).trigger('input').focus();
			 angular.element('#shp_pickupname'+attr.id).val(colobj.title).trigger('input');
		  }
		}
		if(this.inputName == 'shp_deliverynameauto'){
		  if(typeof colobj !== 'undefined'){
			 angular.element('#del_location'+attr.id).val(colobj.originalObject.location_city+', '+colobj.originalObject.location_state+', '+colobj.originalObject.location_postal).trigger('input').focus();
			 angular.element('#shp_deliveryname'+attr.id).val(colobj.title).trigger('input');
		  }	 
		 }	 
		
		}	  
			  
			scope.saveSource = function(form){
			  if(!form.$valid)
			   throw Error('Invalid request');
			   
			      $('.s_save').addClass('hidden'); 
				  $('.s_loading').removeClass('hidden');
				  console.log(scope.location);
				  scope.locationData = {
				  id:attr.elmshipid, 
				  shp_pickupname:scope.location.shp_pickupname,
				  shp_pickupadr1:scope.location.shp_pickupadr1,
				  shp_pickupadr2:scope.location.shp_pickupadr2,
				  pick_location:scope.location.pick_location,
				  
				  shp_pickupcontact:scope.location.shp_pickupcontact,
				  shp_pickupphone:scope.location.shp_pickupphone,			  
				  };
				var data = {
					shipment: scope.locationData,
					customer_cost: [],
					carrier_cost: [],
					customer_notes : [],
					carrier_notes : [],
					internal_notes : [],
					products : [],
					stops : [],
					ship_status_user : [],
				};			
				
				   Shipment.postPartialShipmentData(attr.elmshipid, data, 1)
				   .then(function(response){
					console.log(response);
				   }, function(reason){ 
					console.log(reason);
					}).finally(function(data){				
					 scope.picklocation = angular.element('#pick_location'+attr.id).val();
					 scope.autofillpick = angular.element('#shp_pickupname'+attr.id).val();
					 scope.shppickphone = angular.element('#shp_pickupphone'+attr.id).val();
					 scope.shppickcontact = angular.element('#shp_pickupcontact'+attr.id).val();				 
					 //scope.autofilldel = angular.element('#shp_deliveryname'+attr.id).val();
					 //scope.dellocation = angular.element('#del_location'+attr.id).val();
					 
					 angular.element('#source'+attr.id).hide('slow');
					 angular.element('#s_box'+attr.id).show('slow');
			      $('.s_save').removeClass('hidden'); 
				  $('.s_loading').addClass('hidden');					 
				   });			   
			   
			}  

			scope.saveDestination = function(form){
				 if(!form.$valid)
				  throw Error('Invalid request');
			
			      $('.d_save').addClass('hidden'); 
				  $('.d_loading').removeClass('hidden');			
				  console.log(scope.location);
				  scope.locationData = {
				  id:attr.elmshipid, 
				  shp_deliveryname:scope.location.shp_deliveryname,
				  shp_deliveryadr1:scope.location.shp_deliveryadr1,
				  shp_deliveryadr2:scope.location.shp_deliveryadr2,
				  del_location:scope.location.del_location,
				  
				  shp_deliverycontact:scope.location.shp_deliverycontact,
				  shp_deliveryphone:scope.location.shp_deliveryphone,				  
				  };
				var data = {
					shipment: scope.locationData,
					/*customer_cost: scope.listc,
					carrier_cost: scope.listcr,
					customer_notes : scope.customernotes.notes,
					carrier_notes : scope.carriernotes.notes,
					internal_notes : scope.terminalnotes.notes,
					products : scope.products,
					stops : scope.stops,*/
				};			
				
				   Shipment.postPartialShipmentData(attr.elmshipid, data, 1)
				   .then(function(response){
					console.log(response);
				   }, function(reason){ 
					console.log(reason);
					}).finally(function(data){				
					 //scope.picklocation = angular.element('#pick_location'+attr.id).val();
					 //scope.autofillpick = angular.element('#shp_pickupname'+attr.id).val();
					 scope.autofilldel = angular.element('#shp_deliveryname'+attr.id).val();
					 scope.dellocation = angular.element('#del_location'+attr.id).val();
					 scope.shpdelphone = angular.element('#shp_deliveryphone'+attr.id).val();
					 scope.shpdelcontact = angular.element('#shp_deliverycontact'+attr.id).val();				 
					 
					 angular.element('#destination'+attr.id).hide('slow');
					 angular.element('#d_box'+attr.id).show('slow');
			      $('.d_save').removeClass('hidden'); 
				  $('.d_loading').addClass('hidden');					 
				   });			
			}	
				
		  
		 }; 
         return directive;		 
		})		
		.directive('removeCustomerCostItem', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ 
		   console.log(attr.id);
		   scope.listc.customernc.splice(attr.id, 1);
		   scope.getTotalcc();
		   scope.$apply();

		  });
		 };		

        return directive;	 
		})
		.directive('removeCarrierCostItem', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.listcr.carriernc.splice(attr.id, 1);
		   scope.getTotalcr();
		   scope.$apply();

		  });
		 };		

        return directive;	 
		})		
		.directive('parentDir', function(Shipment){
		 var directive = {};
		 directive.restrict = 'E';
		 directive.scope = true;
		 directive.controller = function($scope){ 
			  this.getParentScope = function(){
				console.log($scope); //alert(bi);
			   return $scope;
			  }
			  this.saveData = function(shipID, data, elem, scope_obj){ 
                angular.element('.resp-tab-content-active').find('.alert-success').remove();			  
			    elem.find('div.btn-cont').addClass('hidden');
			    elem.find('.loading').removeClass('hidden');
			    scope_obj.success.status = false;
				scope_obj.success.message = '';		
			    scope_obj.error.status = false;
				scope_obj.error.message = '';				
			   Shipment.postPartialShipmentData(shipID, data, 0)
			   .then(function(response){
			    scope_obj.success.status = true;
				scope_obj.success.message = response.data.success;
				console.log(response);
				angular.element('.resp-tab-content-active').append('<div class="alert alert-success" ng-show="success.status" style="text-align:center;"><strong>'+scope_obj.success.message+'</strong></div>');
			   }, function(reason){ 
			    scope_obj.error.status = true;
				scope_obj.error.message = reason;			   
			    console.log(reason);
				}).finally(function(data){
						//making the loader invisible for user
						//console.log($scope.success);
						elem.find('.loading').addClass('hidden');
						elem.find('div.btn-cont').removeClass('hidden');
						
					    $scope.clickedShipId.length = 0;
					    $scope.currentPage = 1;
					    	
                    $scope.filters = [{name : $scope.filter.shipText}, {shp_postatusId : $scope.postStatusId}, {carriers : $scope.filter.shp_carriertype}, {shp_loadstatusId : $scope.filter.shp_loadstatusId}, {shp_equipmentId : $scope.filter.shp_equipmentId}, {shp_clientlist : $scope.filter.shp_clientlist},{shp_pickupstate:$scope.filter.shp_from_state},{shp_deliverystate:$scope.filter.shp_to_state}, ];
							
					angular.element('.loading').removeClass('hidden'); 
					Shipment.makeAsyncRequest(Shipment.listShipment({filters: $scope.filters, limit: $scope.recordLimit, page: 1, 
				orderBy: $scope.predicate, orderType: $scope.reverse ? 'desc' : 'asc'
			}))
						.then(function(response){ 
							if(response.data.success && response.data.count !== undefined && response.data.results){
								$scope.shipmentData.count = response.data.count;
								$scope.shipmentData.list = response.data.results ? response.data.results : [];
								$scope.totalShipmentCount = $scope.shipmentData.count;
								
								if($scope.totalShipmentCount === 0){
								 $scope.isBusy = true;
								 return;
								} else {
								 $scope.isBusy = false;
								}
								//console.log($scope.shipmentData.list.shp_reqpickupdate);
								//$scope.shipmentData.list.shp_reqpickupdate = CommonFunctions.dformat($scope.shipmentData.list.shp_reqpickupdate.split(' ')[0]);
								
							}
							//console.log(result);
						}, function(reason){
							console.warn(reason);
						}).finally(function(data){
							//making the loader invisible for user
							//angular.element('#script').after($compile('<loadjsfiles></loadjsfiles>')($scope));
							angular.element('.loading').addClass('hidden');
						});
									
						   });
						  }
					 };
		 
		 return directive;
		})
 	
		.directive('removeCustomerNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.customernotes.notes.splice(scope.customernotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})
		.directive('removeCarrierNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.carriernotes.notes.splice(scope.carriernotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})		
		.directive('removeTerminalNote', function(){
		 var directive = {};
		 directive.restrict = 'A';
         
         directive.link = function(scope, elem, attr){
		  elem.bind('click', function(){ //alert('sss');
		   console.log(attr.id);
		   scope.terminalnotes.notes.splice(scope.terminalnotes.notes.indexOf(attr.id), 1);
		   scope.$apply();
		  });
		 };		

        return directive;	 
		})	
});