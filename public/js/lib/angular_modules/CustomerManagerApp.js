define([
	'classes/Customer',
	'angular_modules/CommonApp',
	'angular_modules/controllers/CustomerManagerController',
	'angular_modules/controllers/CustomerEditController',
	'angular_modules/directives/jqEasyTabs',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('CustomerManagerApp', ['CommonApp','ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', '720kb.datepicker', 'jq.easytabs', 'google.places', ])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('CustomerAPIEndpoint', {
				base: '/front/customers/', list: 'ajax-sub-client-lists?', 
				edit: 'ajax-sub-client-edit?', deleteUrl: 'ajax-sub-client-delete?',
				defaultSettings: 'ajax-default-settings?',	sendPassword: 'ajax-send-password?',
				addUser: 'ajax-create-new-user-with-client-data',
				sendUpdatePassword: 'ajax-send-update-password',
				themes: 'ajax-get-themes',
				languages: 'ajax-get-languages',
                                commodity: 'ajax-get-commodity',
                                classes: 'ajax-get-classes',                                
                                shipment: 'ajax-get-shipment',
                                bolTemp: 'ajax-get-bol-temp',
                                invoiceTerms: 'ajax-get-invoice-terms',
                                invoiceDays: 'ajax-get-invoice-days',
                                invoiceTemp: 'ajax-get-invoice-temp',
		})

		
		.service('Customer', ['CustomerAPIEndpoint', '$http', '$q', Customer])
		// our controller for the form
		// =============================================================================
		.controller('CustomerManagerController', CustomerManagerController)
		.controller('CustomerEditController', CustomerEditController)
		
});