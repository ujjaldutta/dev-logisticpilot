define([
	'classes/CustomerLocation',
	'angular_modules/CommonApp',
	'angular_modules/controllers/CustomerLocationManagerController',
	'angular_modules/controllers/CustomerLocationEditController',
	'angular_modules/directives/jqEasyTabs',
	'angular_modules/factories/GoogleLocation',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('CustomerLocationManagerApp', ['CommonApp',
			'ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', 
			'720kb.datepicker', 'jq.easytabs', 'google.places', 'GoogleLocationFormatter'
		])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('CustomerLocationAPIEndpoint', {
				base: '/front/customer-locations/', list: 'ajax-list-locations?', 
				edit: 'ajax-location-edit?', deleteUrl: 'ajax-location-delete?',
				types: 'ajax-location-types?', timeZone: 'ajax-timezones?'
		})

		
		.service('CustomerLocation', ['CustomerLocationAPIEndpoint', '$http', '$q', 'GoogleLocationFactory', CustomerLocation])
		// our controller for the form
		// =============================================================================
		.controller('CustomerLocationManagerController', CustomerLocationManagerController)
		.controller('CustomerLocationEditController', CustomerLocationEditController)
		
});