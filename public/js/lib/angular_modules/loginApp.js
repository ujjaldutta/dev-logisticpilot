define(['angular', 'angular_modules/controllers/loginController'], function(){
	var app = angular.module('loginApp', []);
	app.controller('loginController', loginController);
});