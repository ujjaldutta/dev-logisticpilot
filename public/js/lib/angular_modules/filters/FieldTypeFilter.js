define(
        ['jquery', 'angular'],
        function ($) {
            var module = angular.module('FieldTypeFilter', [])
                    .filter('formatByType', function () {
                        return function (input, type) {
                            switch (type) {
                                case 'C':
                                    break;
                                case 'B':
                                    input = input ? 'Enabled' : 'Not Available';
                                    break;
                                case 'Y':
                                    input = input ? 'Yes' : 'No';
                                    break;
                                default:
                                    break;
                            }

                            return input;
                        };
                    });
        });