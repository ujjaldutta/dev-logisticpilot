define([
	'classes/CodeReference',
	'angular_modules/CommonApp',
	'angular_modules/controllers/CodeReferenceManagerController',
	'angular_modules/controllers/CodeReferenceEditController',
	'angular_modules/directives/jqEasyTabs',        
        'angular_modules/filters/FieldTypeFilter',
	'angular_modules/factories/GoogleLocation',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('CodeReferenceManagerApp', ['CommonApp',
			'ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', 
			'720kb.datepicker', 'jq.easytabs', 'FieldTypeFilter', 'google.places', 'GoogleLocationFormatter',])
		.value('CodeReferenceAPIEndpoint', {
				base: '/front/code-references/', list: 'ajax-list-codes?', 
				edit: 'ajax-code-edit?', deleteUrl: 'ajax-code-delete?',
				codetypes: 'ajax-code-types?',
		})

		
		.service('CodeReference', ['CodeReferenceAPIEndpoint', '$http', '$q', 'GoogleLocationFactory', CodeReference])
		// our controller for the form
		// =============================================================================
		.controller('CodeReferenceManagerController', CodeReferenceManagerController)
		.controller('CodeReferenceEditController', CodeReferenceEditController)
		
});