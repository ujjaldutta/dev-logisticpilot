define([
	'jquery',
	'angular-ui-router',
	'ng-file-upload',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/controllers/registrationStepsController',
	'angular_modules/controllers/registrationStep2Controller',
	'angular_modules/controllers/registrationStep3Controller',
	'angular_modules/controllers/registrationStep4Controller',
	'angucomplete-alt',
	'angular-google-places-autocomplete',
], function($){
	var app = angular.module('registrationStepsApp', ['SyncCarriers', 'ui.router', 'ngFileUpload', /* "ngTouch",*/ "angucomplete-alt", /* "ui.timepicker", */ 'google.places', ])
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('carrierStatusAPIEndpoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRate?')
		// configuring our routes 
		// =============================================================================
		.config(function($stateProvider, $urlRouterProvider) {
			$stateProvider
				// route to show our basic form (/form)
				.state('form', {
					url: '/form',
					templateUrl: '/front/registration-steps/form',
					controller: 'registrationStepController',
				})
				
				// nested states 
				// each of these sections will have their own view
				// url will be nested (/form/profile)
				.state('form.profile', {
					url: '/profile',
					templateUrl: '/front/registration-steps/1'
				})
				
				// nested states 
				// each of these sections will have their own view
				// url will be nested (/form/profile)
				.state('form.billing', {
					url: '/billing',
					templateUrl: '/front/registration-steps/2',
					controller: 'registrationStep2Controller'
				})

				.state('form.carrierSelection', {
					url: '/select-carriers',
					templateUrl: '/front/registration-steps/3',
					controller: 'registrationStep3Controller',
				})
				.state('form.carrierSetup', {
					url: '/carrier-Setup',
					templateUrl: '/front/registration-steps/4',
					controller: 'registrationStep4Controller',
				})
				.state('form.finish', {
					url: '/finish',
					templateUrl: '/front/registration-steps/5',
					//controller: 'registrationStep4Controller',
				})
				
			// catch all route
			// send users to the form page 
			$urlRouterProvider.otherwise('/form/profile');
		})
		//service to fetch process and maintains carrier details and api connectivity
		.service('CarrierApiService', function(carrierStatusAPIEndpoint, SyncCarriers, $http, $q, $timeout){
			this.fecthSelectedCarriers = function(){
				return SyncCarriers.fetch();
			};

			this.getCarrierApiDetails = function(carrierId){
				return $http.get('/front/client-register?action=getCarrierApiDetails&carrierId=' + carrierId)
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.checkApiStatus = function(username, password, account, scac){
				return $http.jsonp(carrierStatusAPIEndpoint + 'callback=JSON_CALLBACK&username=' + username
						+ '&password=' + password + '&accountnumber=' + account + '&scaccode=' + scac
					) 
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.postApiDetails = function(dataObj){
				return $http.post('/front/client-register', dataObj)
					.error(function(data, status, config, headers){
						alert(status);
					});
			};

			this.asyncCheckValidateSteps = function(){
					//whether all steps are valid before proceeding to the next step
					var deferred = $q.defer();
					$timeout(function(){
						deferred.resolve(SyncCarriers.validateAllSteps())
					}, 2000);

					return deferred.promise;
				}
		})
		// our controller for the form
		// =============================================================================
		.controller('registrationStepController', registrationStepsController)
		.controller('registrationStep2Controller', registrationStep2Controller)
		.controller('registrationStep3Controller', registrationStep3Controller)
		.controller('registrationStep4Controller', registrationStep4Controller)
		.directive('notemptyphone', function(){
			return {
				require: 'ngModel',
				link: function(scope, elm, attrs, ctrl){
					ctrl.$validators.notemptyphone = function(modelValue, viewValue){
						if(ctrl.$isEmpty(modelValue)){
							return true;
						}

						if(/^\+(?:[0-9] ?){6,14}[0-9]$/.test(viewValue)){
							return true;
						}

						return false;
					}
				}
			};
		})
		.directive('selectedCarrierList', function(SyncCarriers, $http){
			return {
				restrict: 'E',
				templateUrl: 'templates/selectedCarrierList.html',
				link: function(scope, element, attrs){
					
					//code to submit individual carrier contact
					scope.submitCarrierInfo = function(index){
						if(index === undefined) //cancelling process if invalid index is given
							return;

						//extracting information using jquery lib
						var name = $('#name_' + index).val().trim();
						var email = $('#email_' + index).val().trim();
						var phone = $('#phone_' + index).val().trim();

						//flag to determine data validity
						var flag = false;
						
						if(name.length || email.length || phone.length){
							var emailRegExp = /^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$/;
							var phoneRegExp = /^(?:[\+]{1})?([0-9]{8,12})$/;
							var nameRegExp = /^([a-zA-Z\s]{2,30})$/;

							if(emailRegExp.test(email) || phoneRegExp.test(phone))
								flag = true;
							else if(nameRegExp.test(name))
								flag = true;
						}
						
						//cancelling processing or submission if valid data flag is not true
						if(!flag)
							return;

						//running spinner for processing status
						var currentButtonContent = $('#submit_'  + index).html();
						$('#submit_'  + index).prop('disabled', true).html('<i class="fa fa-spinner"></i>');

						//preparing the data to post
						var data = {
							step: $('#step_' + index).val(),
							assignmentId: $('#assignmentId_' + index).val(),
							name: $('#name_' + index).val().trim(),
							email: $('#email_' + index).val().trim(),
							phone: $('#phone_' + index).val().trim(),
						};

						//disabling all textboxes when processing save request
						$('#carrier_' + index).find('input').prop('disabled', true);

						$http.post('/front/client-register', data)
							.success(function(result){
								if(result.success){
									//re-enabling the input fields
									$('#carrier_' + index).find('input').prop('disabled', false);
									//restoring submit button text
									$('#submit_'  + index).prop('disabled', false).html(currentButtonContent);
								}
							});
					};


					scope.deleteCarrierProvider = function(index){
						if(confirm('Are you sure to delete')){
							//console.log($('#assignmentId_' + index).val());
							var id = $('#carrier_id_' + index).val();
							//posting to the server endpoint to delete
							$http.delete('/front/client-register/' + id + '/?type=carrierAssignment')
								.success(function(result){
									if(result.success)
										scope.$emit('refetch', [result]);
								})
								.error(function(data, status){
									alert(status);
								});
						}
					};

					scope.$on('saveContactInfo', function(event, params){
						//finding the form
						$(element).find('form').each(function(i){
							var index = $(this).data('index');
							//console.log();
							scope.submitCarrierInfo(index);
						});

						
					});
				}
			};
		})
		.directive('selectedCarrierSetup', function(SyncCarriers, CarrierApiService, $http){
			return {
				restrict: 'E',
				scope: true,
				controller: function($scope, Upload){
					//making scopes which
					$scope.carrierData = new Array();
					
					/* $scope.$watch('carrierData', function(value){
						console.log(value);
					}); */

					$scope.deleteContractFile = function(carrierId, contractFileID){
						if(confirm('Are you sure to delete')){
							//posting to the server endpoint to delete
							$http.delete('/front/client-register/' + contractFileID + '/?type=carrierContractFile')
								.success(function(result){
									if(result.success)
										$scope.carrierData[carrierId].contractFileId = null;
								})
								.error(function(data, status){
									alert(status);
								});
						}
					};
					
					$scope.uploadContract = function (files, carrierId, steupId, docId) {
				        if (files && files.length) {
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i];
				                Upload.upload({
				                    url: '/front/ajaxUploadApiContract',
				                    fields: {
				                    	'carrierID': carrierId,
				                        'id': steupId,
				                        'docId': docId
				                    },
				                    file: file
				                }).progress(function (evt) {
				                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				                    $scope.carrierData[carrierId].message = 'progress: ' + progressPercentage + '% ' + evt.config.file.name;
				                               // evt.config.file.name + '\n' + $scope.formData.log;
				                }).success(function (data, status, headers, config) {
				                	if(data.error){
				                		$scope.carrierData[carrierId].alert = true;
				                		$scope.carrierData[carrierId].error = true;
										$scope.carrierData[carrierId].message = data.error;
				                	}else if(data.success){
				                		$scope.carrierData[carrierId].contractFileId = data.docId ? data.docId : null;
				                		$scope.carrierData[carrierId].setupId = data.contractId ? data.contractId : null;
				                		$scope.carrierData[carrierId].url = data.docUrl ? data.docUrl : null;
				                		$scope.carrierData[carrierId].alert = true;
				                		$scope.carrierData[carrierId].error = false;
				                		$scope.carrierData[carrierId].message = 'file ' + config.file.name + ' uploaded successfully!'
				                	}
				                  
				                   //$scope.$apply();
				                });
				            }
				        }
				    };
				},
				templateUrl: 'templates/selectedCarrierSetupList.html',
				link: function(scope, element, attrs){
					//code to submit individual carrier contact
					scope.submitCarrierContractInfo = function(index, setupId, docId){
						if(index === undefined) //cancelling process if invalid index is given
							return;

						//extracting information using jquery lib
						var carrierID = $('#carrier_id_' + index).val().trim();
						var username = $('#userid_' + index).val().trim();
						var password = $('#password_' + index).val().trim();

						//flag to determine data validity
						var flag = false;
						
						if(username.length && password.length)
							flag = true;
						
						//cancelling processing or submission if valid data flag is not true
						if(!flag)
							return;

						//running spinner for processing status
						var currentButtonContent = $('#submit_'  + index).html();
						$('#submit_'  + index).prop('disabled', true).html('<i class="fa fa-spinner"></i>');

						//preparing the data to post
						var data = {
							step: $('#step_' + index).val(),
							carrierID: carrierID,
							id: setupId,
							docId: docId,
							username: username,
							password: password,
						};

						//disabling all textboxes when processing save request
						$('#carrier_' + index).find('input').prop('disabled', true);

						$http.post('/front/client-register', data)
							.success(function(result){
								if(result.success){
									//re-enabling the input fields
									$('#carrier_' + index).find('input').prop('disabled', false);
									//restoring submit button text
									$('#submit_'  + index).prop('disabled', false).html(currentButtonContent);
								}
							});
					};

					scope.deleteCarrierProvider = function(index){
						if(confirm('Are you sure to delete')){
							//console.log($('#assignmentId_' + index).val());
							var id = $('#carrier_id_' + index).val();
							//posting to the server endpoint to delete
							$http.delete('/front/client-register/' + id + '/?type=carrierAssignment')
								.success(function(result){
									if(result.success)
										scope.$emit('refetch', [result]);
								})
								.error(function(data, status){
									alert(status);
								});
						}
					};

					scope.$on('saveContactInfo', function(event, params){
						//finding the form
						$(element).find('form').each(function(i){
							var index = $(this).data('index');
							var id = $(this).data('setupid');
							var docId = $(this).data('docid');
							//console.log();
							scope.submitCarrierContractInfo(index, id, docId);
						});

						//setting deffered function using $q service to check asynchornously 
						CarrierApiService.asyncCheckValidateSteps()
							.then(function(result){
								if(result.data.success)
									scope.$emit('ValidateAllSteps', {status: true})
								else if(result.data.error)
									scope.$emit('ValidateAllSteps', {status: false})
							});
					});
				}
			};
		});
});