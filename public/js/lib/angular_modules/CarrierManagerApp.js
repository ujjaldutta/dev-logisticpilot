define([
	'classes/Carrier',
	'angular_modules/CommonApp',
	'angular_modules/controllers/CarrierManagerController',
	'angular_modules/controllers/CarrierEditController',
        'angular_modules/controllers/CarrierEquipmentEditController',
        'angular_modules/controllers/CarrierRemitEditController',
	'angular_modules/directives/jqEasyTabs',
        'angular_modules/filters/FieldTypeFilter',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
        'angularjs-dropdown-multiselect',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('CarrierManagerApp', ['CommonApp','ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', '720kb.datepicker', 'jq.easytabs', 'google.places', 'FieldTypeFilter', "angularjs-dropdown-multiselect"])
		.value('CarrierAPIEndpoint', {
				base: '/front/carriers/', list: 'ajax-sub-carrier-lists?', 
				edit: 'ajax-sub-carrier-edit?', deleteUrl: 'ajax-sub-carrier-delete?',
                                insuranceTypes: 'ajax-get-insurance-types',
                                currencyTypes: 'ajax-get-currency-types',
                                carrierTypes: 'ajax-get-carrier-types',
                                modeTypes: 'ajax-get-mode-types',
                                carrierInsurances: 'ajax-carrier-insurances?',
                                saveInsurances: 'ajax-save-insurances?', 
                                saveModes: 'ajax-save-modes?',
                                carrierModes: 'ajax-carrier-modes?',
                                listEquipments: 'ajax-sub-carrier-equipments-lists?', 
                                deleteEquipUrl: 'ajax-sub-equipment-delete?',
                                carrierAllFields: 'ajax-get-all-fields',
                                equipmentTypes: 'ajax-get-equipment-types',
                                ownerTypes: 'ajax-get-owner-types',
                                equipmentEdit: 'ajax-carrier-equipment-edit?',
                                equipTypes: 'ajax-get-equip-types',
                                insTypes: 'ajax-get-ins-types',
                                modTypes: 'ajax-get-mod-types',
                                listRemits: 'ajax-sub-carrier-remits-lists?', 
                                deleteRemitUrl: 'ajax-sub-remit-delete?',
                                remitEdit: 'ajax-carrier-remit-edit?',
                                currTypes: 'ajax-get-curr-types',
		})

		
		.service('Carrier', ['CarrierAPIEndpoint', '$http', '$q', Carrier])
		// our controller for the form
		// =============================================================================
		.controller('CarrierManagerController', CarrierManagerController)
		.controller('CarrierEditController', CarrierEditController)
                .controller('CarrierEquipmentEditController', CarrierEquipmentEditController)
                .controller('CarrierRemitEditController', CarrierRemitEditController)
		
});

