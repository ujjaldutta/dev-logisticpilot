define([
	'classes/CustomerProduct',
	'angular_modules/CommonApp',
	'angular_modules/controllers/CustomerProductManagerController',
	'angular_modules/controllers/CustomerProductEditController',
	'angular_modules/directives/jqEasyTabs',        
        'angular_modules/filters/FieldTypeFilter',
	'angular_modules/factories/GoogleLocation',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
        'angularjs-dropdown-multiselect',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('CustomerProductManagerApp', ['CommonApp',
			'ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', 
			'720kb.datepicker', 'jq.easytabs', 'FieldTypeFilter', 'google.places', 'GoogleLocationFormatter', "angularjs-dropdown-multiselect",])
		.value('CustomerProductAPIEndpoint', {
				base: '/front/customer-products/', list: 'ajax-list-products?', 
				edit: 'ajax-product-edit?', deleteUrl: 'ajax-product-delete?',
				weightTypes: 'ajax-weight-types?', commodityTypes: 'ajax-commodity-types?',
                                dimmensionsTypes: 'ajax-dimmensions-types?', temperatureTypes: 'ajax-temperature-types?',
                                freeClassTypes: 'ajax-class-types?', packageTypes: 'ajax-package-types?',
                                classTypes: 'ajax-freight-types?'
		})

		
		.service('CustomerProduct', ['CustomerProductAPIEndpoint', '$http', '$q', 'GoogleLocationFactory', CustomerProduct])
		// our controller for the form
		// =============================================================================
		.controller('CustomerProductManagerController', CustomerProductManagerController)
		.controller('CustomerProductEditController', CustomerProductEditController)
		
});