define([
	'classes/CommonFunctions',
	'classes/Shipment',
	'angular_modules/CommonApp',
	'angular_modules/controllers/ShipmentInvoiceManagerController',
	'angular_modules/controllers/ShipmentEditController',
	'angular_modules/directives/shipmentInvoiceDirectives',
	'angular_modules/directives/jqEasyTabs',
	'angular_modules/directives/jqDateTimePicker',
	'angular_modules/directives/jqMenuToggle',
	'angular_modules/directives/gMapDirection',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
	'angular-sanitize',
	'angularjs-dropdown-multiselect',
	'ngInfiniteScroll',
	'angularbootstrap',
], function(){
	var app = angular.module('ShipmentInvoiceManagerApp', ['CommonApp','ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', '720kb.datepicker', 'jq.easytabs', 'google.places', 'ngSanitize', 'jq.dateTimePicker', 'shipmentsDirs','infinite-scroll', 'jqMenuToggle', 'ui.bootstrap', 'gmapdirection', ])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
        .controller('ModalInstanceCtrl', function($scope, $uibModalInstance, items, update_val){
		 $scope.items = items; 
		 console.log('herrreeee....'); console.log($scope.items);
		 	  $scope.ok = function () {
				$uibModalInstance.close();
			  };

			  $scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			  };
			  
			  $scope.deletedata = function(){
			    $uibModalInstance.dismiss('cancel');
			  };
			  
			  $scope.deletedata_remove = function(){
				angular.forEach(items.shipmentData.list, function(value, key){
					if(value.selected === true){
						items.deleteShipment(value.id, false);
					}
				});
			   $uibModalInstance.close();
			  };
			  
			  $scope.updatestatus = function(){
				angular.forEach(items.shipmentData.list, function(value, key){
					if(value.selected === true){
						items.updateShipmentStatus(value.id, false, update_val);
					}
				});
			   $uibModalInstance.close();
			  };

            $scope.updateinvoicestatus = function(){
			angular.forEach(items.shipmentData.list, function(value, key){
				if(value.selected === true){
					items.updateInvoiceShipment(value.id, false);
					//$scope.invoicelist.push(value.id); 
				}
			});
			   $uibModalInstance.close();
			  };
			  
		}) 
		.value('ShipmentAPIEndpoint', {
				base: '/front/shipmentinvoice/', list: 'ajax-shipment-lists?', 
				edit: 'ajax-sub-shipment-edit?', deleteUrl: 'ajax-sub-shipment-delete?', tabRequest: 'ajax-shipment-tab?', 
				defaultSettings: 'ajax-default-settings?',	sendPassword: 'ajax-send-password?',
				addUser: 'ajax-create-new-user-with-shipment-data',
				sendUpdatePassword: 'ajax-send-update-password',
				themes: 'ajax-get-themes',
				languages: 'ajax-get-languages',
                                commodity: 'ajax-get-commodity',
                                classes: 'ajax-get-classes',                                
                                shipment: 'ajax-get-shipment',
                                bolTemp: 'ajax-get-bol-temp',
                                invoiceTerms: 'ajax-get-invoice-terms',
                                invoiceDays: 'ajax-get-invoice-days',
                                invoiceTemp: 'ajax-get-invoice-temp',
								shipNotes: 'ajax-shipment-notes?',
								suggestedHeadings: 'ajax-suggested-headings?',
								partialdata: 'ajax-partial-shipment-edit?',
								shipStatausType: 'ajax-ship-status',
								updateStatausType: 'ajax-shipment-status',
								shipStatausUser: 'ajax-sub-ship-status-for-user?',
								shipUploadFile: 'ajax-upload-file?',
								shipFiles: 'ship-files-info?',
								clientLoadID: 'last-load-number?',
								shipTracking: 'ship-track-list?',
								shipTerminal: 'ship-terminal-list?',
								stateList: 'state-list?',
								summaryList: 'ajax-shipment-summary-lists?',
								updateInvoiceStatausType: 'ajax-shipment-invoice-status',
		})
	
        .factory('CommonFunctions', CommonFunctions)		
		.service('Shipment', ['ShipmentAPIEndpoint', '$http', '$q', Shipment])
		// our controller for the form
		// =============================================================================
		.controller('ShipmentInvoiceManagerController', ShipmentInvoiceManagerController)
		.controller('ShipmentEditController', ShipmentEditController)
		
});