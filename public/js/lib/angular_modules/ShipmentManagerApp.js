define([
	'classes/Shipment',
	'angular_modules/CommonApp',
	'angular_modules/controllers/ShipmentManagerController',
	'angular_modules/controllers/ShipmentEditController',
	'angular_modules/directives/jqEasyTabs',
	'angularjs-datepicker',
	'ui-bootstrap',
	'rzslider',
	'easyResponsiveTabs',
	'ng-file-upload',
	'angular-google-places-autocomplete',
], function(){
	var app = angular.module('ShipmentManagerApp', ['CommonApp','ngFileUpload', 'rzModule', 'ui.bootstrap', 'angucomplete-alt', '720kb.datepicker', 'jq.easytabs', 'google.places', ])
		//.constant('baseHref', '/front/setup-connectivity')
		//constant config for carrier api setup checker endpoint
		//.value('carrierStatusAPIEndpoint', '/front/client-register?action=getCarrierApiConnectivity')
		.value('ShipmentAPIEndpoint', {
				base: '/front/shipments/', list: 'ajax-sub-shipment-lists?', 
				edit: 'ajax-sub-shipment-edit?', deleteUrl: 'ajax-sub-shipment-delete?',
				defaultSettings: 'ajax-default-settings?',	sendPassword: 'ajax-send-password?',
				addUser: 'ajax-create-new-user-with-shipment-data',
				sendUpdatePassword: 'ajax-send-update-password',
				themes: 'ajax-get-themes',
				languages: 'ajax-get-languages',
                                commodity: 'ajax-get-commodity',
                                classes: 'ajax-get-classes',                                
                                shipment: 'ajax-get-shipment',
                                bolTemp: 'ajax-get-bol-temp',
                                invoiceTerms: 'ajax-get-invoice-terms',
                                invoiceDays: 'ajax-get-invoice-days',
                                invoiceTemp: 'ajax-get-invoice-temp',
		})

		
		.service('Shipment', ['ShipmentAPIEndpoint', '$http', '$q', Shipment])
		// our controller for the form
		// =============================================================================
		.controller('ShipmentManagerController', ShipmentManagerController)
		.controller('ShipmentEditController', ShipmentEditController)
		
});