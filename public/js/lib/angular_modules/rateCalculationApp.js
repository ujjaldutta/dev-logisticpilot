define([
	'classes/CarrierSearcher',
	'angular_modules/factories/SyncCarriers',
	'angular_modules/controllers/rateCalculationController',
	'angularjs-datepicker',
	'angucomplete-alt',
	'angular-google-places-autocomplete',
	'angularjs-dropdown-multiselect',
	'ui-bootstrap',
	'rzslider',
], function(){	
	var app = angular.module('rateCalculationApp', [
			'SyncCarriers', 'rzModule', 'ui.bootstrap', '720kb.datepicker', 'google.places', "angucomplete-alt", "angularjs-dropdown-multiselect",
		])
		//api endpoint to call async jsonp and the get the results after the search
		.value('carrierSearchEndPoint', 'http://23.253.41.5:8080/testingapiservicesv2/index.php/testingapiservices/getRateData?')

		//api endpoint local to get selected carrier list and other data
		.value('carrierConfigEndPoint', '/front/client-register?action=getSelectedCarriers')

		//api endpoint to search quote related services
		.value('quoteEndPoint', '/front/quote-services?')

		//service to handle carrier search and other its results
		.service("carrierRateSearch", ['carrierSearchEndPoint', 'quoteEndPoint', '$http', '$q', CarrierSearcher])

		//factory to handle calculation and other things
		.factory("carrierCalculation", function(){
			return {
				//method to calculate the total density against the given params
				calculateDensity: function(l, w, h, wt){
					var d = l*w*h;
					d = (!isNaN(wt) && !isNaN(d) && wt > 0) ? d/wt : d
					return isNaN(d) ? 0 : d.toFixed(2);
				},

				//method to calculate the total weight of the entire collection
				calculateTotalweight: function (collection){
					var total = 0;

					for(var i = 0; i < collection.length; i++){
						if(!isNaN(collection[i]))
							total += parseFloat(collection[i]);
					}

					return total;
				}
			};
		})
		
		//controller
		.controller('rateCalculationController', rateCalculationController)
		.directive('carrierCarousel', [function(){
			return {
				restrict: 'E',
				
				scope: {
					results: '=',
					carriers: '=',
					carouselId: '@',
				},
				
				template: '<div class="flexslider" id="{{carouselId}}">'
						+  '<ul class="slides">'
						+    '<li ng-repeat="result in results">'
      					+		"<img src=\"/images/no_image.png\" ng-src=\"{{getCarrierLogoBySCAC('logo', result.SCAC)}}\" />"
    					+ 	 '</li>'
						+ '</ul>'
						+ '</div>',
				
				link: function(scope, elm, attrs){
					scope.renderCarousel = function(items){
						try{
							$('.flexslider').flexslider({
							    animation: "slide",
							    animationLoop: false,
							    itemWidth: 210,
							    itemMargin: 5
							});
						}catch(ex){
							console.warn(ex.message);
						}
					}

					scope.$watch('results', function(value){
						console.log(value);
						scope.renderCarousel();
					});

					scope.$on('updateCarousel', function(e, args){
						console.log('here');
						scope.renderCarousel();
					})

					//method to get different items plucked from the already selected carrier list
					scope.getCarrierLogoBySCAC = function(infoKey, scac){
						try{
							var info = _.result(_.findWhere(scope.carriers, {scac: scac}), infoKey);
							//console.log(info);
							return info;
						}catch(ex){
							console.error(ex.message);
						}
					}
				}
			}
		}]);
});