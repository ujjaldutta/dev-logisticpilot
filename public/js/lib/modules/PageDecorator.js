define(['jquery', 'bootstrap', 'classes/PageDecorator'], function($){
	return {
		load: function(){
			return new PageDecorator($);
		},
		init: function(){
			//initiating page decorator for loader
			var pageDecorator = new PageDecorator($);
			pageDecorator.loadPage();
			pageDecorator.headerFix($("#header-style-1"));
			pageDecorator.gotoTop($('.dmtop'));
			//pageDecorator.themeSelecter();
			//magic line menu indicator
			pageDecorator.runMagicMenuLine();
			//pageDecorator.activateEasyTab('horizontalTab1', false);
			pageDecorator.activateBSTooltip();
			
		}
	}
});