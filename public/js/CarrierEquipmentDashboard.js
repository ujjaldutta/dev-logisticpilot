require([
	'angular', 
	'angular_modules/CarrierEquipmentDashboardApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CarrierEquipmentDashboardApp']);
});