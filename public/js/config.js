require.config({
	waitSeconds: 0, 
    baseUrl: '/js/lib',
    paths: {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        jquery: '../../bower_components/jquery/dist/jquery.min',
		bootstrap :  "../../bower_components/bootstrap/dist/js/bootstrap.min",
		angular:  "../../bower_components/angular/angular.min",
		"angular-ui-router": "../../bower_components/angular-ui-router/release/angular-ui-router.min",
		"angular-animate": "../../bower_components/angular-animate/angular-animate.min",
        "ng-file-upload": "../../bower_components/ng-file-upload/ng-file-upload.min",
		"angucomplete-alt": "angular_modules/angucomplete-alt",
		//"angular-touch": "../../bower_components/angular-touch/angular-touch.min",
        "ng-tags-input": '../../bower_components/ng-tags-input/ng-tags-input.min',
        "angular-google-places-autocomplete": '../../bower_components/angular-google-places-autocomplete/dist/autocomplete.min',
		"angularjs-datepicker": '../../bower_components/angularjs-datepicker/dist/angular-datepicker.min',
        "angularjs-dropdown-multiselect": '../../bower_components/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min',
        "ui-bootstrap": 'ui-bootstrap-tpls-0.14.2.min',
        "rzslider": '../../bower_components/angularjs-slider/dist/rzslider.min',
    },
	shim: {
			'bootstrap': {deps: ['jquery']},
			"angular-ui-router" : {deps: ['angular']},
			"angular-animate" : {deps: ['angular']},
            "ng-file-upload" : {deps: ['angular']},
			"angucomplete-alt": {deps: ['angular', /* 'angular-touch' */]},
			//'modules/login': {deps: ['jquery', 'classes/PageDecorator',]},
            "ng-tags-input": {deps: ['angular', ]},
            "angular-google-places-autocomplete": {deps: ['angular', ]},
			"angularjs-datepicker": {deps: ['angular', ]},
            "angularjs-dropdown-multiselect": {deps: ['angular', 'lodash.min']},
            "ui-bootstrap": {deps: ['angular']},
            "rzslider": {deps: ['angular']},
		}
});