require([
	'angular', 
	'angular_modules/CarrierAccessorialManagerApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CarrierAccessorialManagerApp']);
});