require(['angular', 'angular_modules/ShipmentInvoiceManagerApp', 'angular_modules/CarrierManagerApp', 'angular_modules/CustomerManagerApp', 'angular_modules/CustomerLocationManagerApp', 'angular_modules/CustomerProductManagerApp',/*'angular_modules/CustomerDashabordSetupApp'*/], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['ShipmentInvoiceManagerApp', 'CarrierManagerApp', 'CustomerManagerApp', 'CustomerLocationManagerApp', 'CustomerProductManagerApp', /*'CustomerDashabordSetupApp'*/]);
});