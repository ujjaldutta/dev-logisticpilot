require([
	'angular', 
	'angular_modules/apiSetupApp',
	'angular_modules/CommonApp',
], function(){
	//bootstrapping angular app
	angular.bootstrap(document, ['CommonApp',  'apiSetupApp', ] );
});